<html>
    <style type="text/css">
        .tg {
            border-collapse: collapse;
            border-color: #ccc;
            border-spacing: 0;
            border-color: black;
            border-style: none;
            border-width: 0px;
            width: 100%;
        }

        .tg2 {
            border-collapse: collapse;
            border-color: #ccc;
            border-spacing: 0;
        }

        .tg3 {
            border-collapse: collapse;
            border-color: #ccc;
            border-spacing: 0;
            border-color: black;
            border-style: none;
            border-width: 0px;
        }

        .tg td {
            background-color: #fff;
            border-color: black;
            border-style: none;
            border-width: 0px;
            color: #333;
            font-family: Arial, sans-serif;
            font-size: 14px;
            overflow: hidden;
            padding: 7px 5px;
            word-break: normal;
        }

        .tg th {
            background-color: #f0f0f0;
            border-color: black;
            border-style: none;
            border-width: 0px;
            color: #333;
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            overflow: hidden;
            padding: 7px 5px;
            word-break: normal;
        }

        .tg .tg-abx8 {
            background-color: #c0c0c0;
            border-color: #c0c0c0;
            font-weight: bold;
            text-align: left;
            vertical-align: top
        }

        .tg .tg-1wig {
            font-weight: bold;
            text-align: left;
            vertical-align: top;
            /* font-size: 10px; */
        }

        .tg .tg-k4px {
            background-color: #f0f0f0;
            font-weight: bold;
            text-align: left;
            vertical-align: top
        }

        .tg .tg-baqh {
            text-align: center;
            vertical-align: top
        }

        .tg .tg-8rcp {
            background-color: #FFF;
            font-weight: bold;
            text-align: left;
            vertical-align: middle
        }

        .tg .tg-kftd {
            background-color: #efefef;
            text-align: right;
            vertical-align: top
        }

        .tg .tg-mft3 {
            background-color: #f0f0f0;
            border-color: #cccccc;
            text-align: left;
            vertical-align: top
        }

        .tg .tg-mv9k {
            border-color: #f0f0f0;
            text-align: left;
            vertical-align: top
        }

        .tg .tg-mv9k2 {
            background-color: #f0f0f0;
            text-align: left;
            vertical-align: top;
            border-spacing: 0;
            border-color: black;
            border-style: solid;
            border-width: 1px;
            text-align: center;
        }

        .tg .tg-mv9k3 {
            text-align: left;
            vertical-align: top;
            border-spacing: 0;
            border-color: black;
            border-style: solid;
            border-width: 1px;
            text-align: center;
        }

        .tg .tg-mv9k3Text {
            text-align: left;
            vertical-align: top;
            border-spacing: 0;
            border-color: black;
            border-style: solid;
            border-width: 1px;
            text-align: left;
        }

        .tg .tg-mv9k3Int {
            text-align: left;
            vertical-align: top;
            border-spacing: 0;
            border-color: black;
            border-style: solid;
            border-width: 1px;
            text-align: right;
        }

        .tg .tg-0pky {
            border-color: inherit;
            text-align: left;
            vertical-align: top
        }

        .tg .tg-0lax {
            text-align: left;
            vertical-align: top
        }

        .tg .tg-z3qe {
            background-color: #f0f0f0;
            border-color: inherit;
            font-size: 20px;
            text-align: center;
            vertical-align: top;
            font-weight: bold
        }



        .tg .tg-odmy {
            background-color: #f0f0f0;
            text-align: left;
            vertical-align: top
        }

        .tg .tg-y6fn {
            background-color: #c0c0c0;
            margin: 0;
            text-align: left;
            vertical-align: top
        }

        .tg .tg-y6fn-title {
            background-color: #c0c0c0;
            margin: 0;
            text-align: center;
            vertical-align: top
        }

        .tg .tg-y6fn-amount {
            background-color: #c0c0c0;
            margin: 0;
            text-align: right;
            vertical-align: top
        }

        .tg .tg-73oq {
            border-color: #000000;
            text-align: center;
            vertical-align: top
        }

        .tg .tg-0lax {
            text-align: left;
            vertical-align: top
        }

        .tg .tg-mqa1 {
            border-color: #000000;
            font-weight: bold;
            text-align: center;
            vertical-align: top
        }

        .tg .tg-0lax {
            text-align: left;
            vertical-align: top
        }

        .tg .tg-amwm {
            font-weight: bold;
            text-align: center;
            vertical-align: top
        }

        .tg .tg-baqh {
            text-align: center;
            vertical-align: top;
        }


        .tg2 {
            border-collapse: collapse;
            border-spacing: 0;
        }

        .tg2 td {
            border-color: black;
            border-style: solid;
            border-width: 1px;
            font-family: Arial, sans-serif;
            font-size: 14px;
            overflow: hidden;
            padding: 7px 5px;
            word-break: normal;
        }

        .tg2 th {
            border-color: black;
            border-style: solid;
            border-width: 1px;
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            overflow: hidden;
            padding: 7px 5px;
            word-break: normal;
        }

        .tg2 .tg2-0lax {
            text-align: left;
            vertical-align: top
        }

        .ttdAkhir {
            position: relative;
            bottom: 0px;
            padding-top: 10;
            right:0px;
            float: right;
            display: inline;
        }

        .ttdTengah {
            position: relative;
            bottom: 0px;
            padding-top: 30;
            right:0px;
            float: center;
            display: inline;
        }
        @media print {
            .ttdAkhir {
                position: relative;
                bottom: 0px;
                padding-top: 10;
                right:0px;
                float: right;
                display: inline;
            }
        }
    </style>

    <div>
        <table class="tg">
            <thead>
                <tr>
                    <th class="tg-mv9k">PT SMT Indonesia</th>
                    <th class="tg-0pky" style="text-align: right;"></th>
                </tr>
                <tr>
                    <th class="tg-z3qe" colspan="2">Dispose DN Picking List</th>
                </tr>
            </thead>
        </table>

        @if(isset($data['FINGOOD']))
            <table class="tg">
                <thead>
                    <tr>
                        <td class="tg-1wig">DN ID :</td>
                        <td class="tg-1wig" colspan=2>
                            {!!DNS1D::getBarcodeHTML($data['RDL_PICK_ID'], 'C128', 1.5,23)!!}
                            <div>{{$data['RDL_PICK_ID']}}</div>
                        </td>
                    </tr>
                    <tr>
                        <td class="tg-1wig"></td>
                        <td class="tg-1wig"></td>
                        <td class="tg-1wig"></td>
                    </tr>
                    <tr>
                        <td class="tg-mv9k2">NO</td>
                        <td class="tg-mv9k2">FINISH GOOD</td>
                        <td class="tg-mv9k2">QTY</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data['FINGOOD'] as $keyData => $valueData)
                        <tr>
                            <td class="tg-mv9k3">{{$keyData + 1}}</td>
                            <td class="tg-mv9k3Text">
                                <div>
                                    {!!DNS1D::getBarcodeHTML($valueData->RDL_ITMCD, 'C128', 1.5,23)!!}
                                </div>
                                {{$valueData->RDL_ITMCD}}
                            </td>
                            <td class="tg-mv9k3Text"><b>{{$valueData->RDL_QTY}}</b></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif

        @if(isset($data['MATERIAL']))
        <table class="tg">
            <thead>
                <tr>
                    <td class="tg-1wig">DN ID :</td>
                    <td class="tg-1wig" colspan=2>
                        {!!DNS1D::getBarcodeHTML($data['RDL_PICK_ID'], 'C128', 1.5,23)!!}
                        <div>{{$data['RDL_PICK_ID']}}</div>
                    </td>
                </tr>
                <tr>
                    <td class="tg-1wig"></td>
                    <td class="tg-1wig"></td>
                    <td class="tg-1wig"></td>
                </tr>
                <tr>
                    <td class="tg-mv9k2">NO</td>
                    <td class="tg-mv9k2">MATERIAL</td>
                    <td class="tg-mv9k2">QTY</td>
                </tr>
            </thead>
            <tbody>
                @foreach($data['MATERIAL'] as $keyData => $valueData)
                    <tr>
                        <td class="tg-mv9k3">{{$keyData + 1}}</td>
                        <td class="tg-mv9k3Text">
                            <div>
                                {!!DNS1D::getBarcodeHTML($valueData->RDL_ITMCD, 'C128', 1.5,23)!!}
                            </div>
                            {{$valueData->RDL_ITMCD}}
                        </td>
                        <td class="tg-mv9k3Text"><b>{{$valueData->RDL_QTY}}</b></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @endif
    </div>
</html>
