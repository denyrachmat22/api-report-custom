<!-- Start hitung subtotal Material -->
<?php
$cekPart = array_filter($data, function ($f) {
    return $f['MDL_FLAG'] == 0;
});

$cekFinGoodReturn = array_filter($data, function ($f) {
    if ($f['MENU_ID'] === 'fg_return') {
        return $f;
    }
});

$cekFinGoodFresh = array_filter($data, function ($f) {
    if ($f['MENU_ID'] === 'warehouse' && $f['MDL_FLAG'] == 1) {
        return $f;
    }
});

$cekFinGoodPending = array_filter($data, function ($f) {
    if ($f['MENU_ID'] === 'pending' && $f['MDL_FLAG'] == "1") {
        return $f;
    }
});

$cekMchReturn = array_filter($data, function ($f) {
    if ($f['MENU_ID'] === 'mchequip' && empty($f['SCRAP_HIST_ID_PARTOF'])) {
        return $f;
    }
});

$cekMchReturnPartOf = array_filter($data, function ($f) {
    if ($f['MENU_ID'] === 'mchequip' && !empty($f['SCRAP_HIST_ID_PARTOF'])) {
        return $f;
    }
});

$subTotPart =
$subTotHargaPart =
$subTotFinGoodFresh =
$subTotFinGoodPending =
$subTotFinGoodReturn =
$subTotFinGoodReturnA =
$subTotFinGoodReturnNA =
$subTotHargaFinGoodFresh =
$subTotHargaFinGoodPending =
$subTotHargaFinGoodReturn =
$subTotFinGoodReturnHargaPart =
$subTotFinGoodReturnHargaPartA =
$subTotFinGoodReturnHargaPartNA =
$subTotMch =
$subTotMchPartOf =
$subTotMchHargaPart =
$subTotMchHargaPartPartOf =
$subTotOthersPrice =
$subTotOthers = 0;

foreach ($cekPart as $keyPart => $valuePart) {
    $subTotPart += $valuePart['QTY'];
    // foreach ($valuePart['hist_det'] as $keyPartDet => $valuePartDet) {
    //     $subTotPart += $valuePartDet['QTY'];
    // }

    if ($valuePart['REASON'] === 'LOSS') {
        $subtothargadet = 0;
        foreach ($valuePart['hist_det'] as $keyFifo => $valueFifo) {
            $subtothargadet += $valueFifo['SCR_QTY'] * $valueFifo['SCR_ITMPRC'];
        }

        $subTotHargaPart += $subtothargadet;
    } else {
        foreach ($valuePart['hist_det'] as $keyDet => $valueDet) {
            if ($valueDet['SCR_OTHER'] == 0) { 
                $subTotHargaPart += $valueDet['SCR_QTY'] * $valueDet['SCR_ITMPRC'];
            }
        }
        // $subTotHargaPart += $valuePart['QTY'] * (isset($valuePart['hist_det'][0]['SCR_ITMPRC'])
        //     ? $valuePart['hist_det'][0]['SCR_ITMPRC']
        //     : 0);
    }
}

foreach ($cekFinGoodFresh as $keyFGFresh => $valueFGFresh) {
    $subTotFinGoodFresh += $valueFGFresh['QTY'];

    $subtotFinGoodFreshhargadet = 0;
    foreach ($valueFGFresh['hist_det'] as $keyFifo => $valueFifo) {
        $subtotFinGoodFreshhargadet += $valueFifo['SCR_QTY'] * $valueFifo['SCR_ITMPRC'];
    }

    $subTotHargaFinGoodFresh += $subtotFinGoodFreshhargadet;
}

foreach ($cekFinGoodPending as $keyFGPending => $valueFGPending) {
    $subTotFinGoodPending += $valueFGPending['QTY'];

    $subtotFinGoodPendinghargadet = 0;
    foreach ($valueFGPending['hist_det'] as $keyFifo => $valueFifo) {
        $subtotFinGoodPendinghargadet += $valueFifo['SCR_QTY'] * $valueFifo['SCR_ITMPRC'];
    }

    $subTotHargaFinGoodPending += $subtotFinGoodPendinghargadet;
}

foreach ($cekFinGoodReturn as $keyFGRet => $valueFGRet) {
    if ($valueFGRet['ITH_WH'] === 'AFWH9SC') {
        $subTotFinGoodReturnA += $valueFGRet['QTY'];

        $subtotFinGoodReturnhargadetA = 0;
        foreach ($valueFGRet['hist_det'] as $keyFifo => $valueFifo) {
            $subtotFinGoodReturnhargadetA += $valueFifo['SCR_QTY'] * $valueFifo['SCR_ITMPRC'];
        }

        $subTotFinGoodReturnHargaPartA += $subtotFinGoodReturnhargadetA;
    }

    if ($valueFGRet['ITH_WH'] === 'NFWH9SC') {
        $subTotFinGoodReturnNA += $valueFGRet['QTY'];

        $subtotFinGoodReturnhargadetNA = 0;
        foreach ($valueFGRet['hist_det'] as $keyFifo => $valueFifo) {
            $subtotFinGoodReturnhargadetNA += $valueFifo['SCR_QTY'] * $valueFifo['SCR_ITMPRC'];
        }

        $subTotFinGoodReturnHargaPartNA += $subtotFinGoodReturnhargadetNA;
    }

    $subTotFinGoodReturn += $valueFGRet['QTY'];

    $subtotFinGoodReturnhargadet = 0;
    foreach ($valueFGRet['hist_det'] as $keyFifo => $valueFifo) {
        $subtotFinGoodReturnhargadet += $valueFifo['SCR_QTY'] * $valueFifo['SCR_ITMPRC'];
    }

    $subTotFinGoodReturnHargaPart += $subtotFinGoodReturnhargadet;
}

foreach ($cekMchReturn as $keyMch => $valueMch) {
    $subTotMch += $valueMch['QTY'];

    $subtotMchhargadet = 0;
    foreach ($valueMch['hist_det'] as $keyFifo => $valueFifo) {
        $subtotMchhargadet += $valueFifo['SCR_QTY'] * $valueFifo['SCR_ITMPRC'];
    }

    $subTotMchHargaPart += $subtotMchhargadet;
}

foreach ($cekMchReturnPartOf as $keyMchPartOf => $valueMchPartOf) {
    $subTotMchPartOf += $valueMchPartOf['QTY'];

    $subtotMchhargadetPartOf = 0;
    foreach ($valueMchPartOf['hist_det'] as $keyFifo => $valueFifo) {
        $subtotMchhargadetPartOf += $valueFifo['SCR_QTY'] * $valueFifo['SCR_ITMPRC'];
    }

    $subTotMchHargaPartPartOf += $subtotMchhargadetPartOf;
}
foreach ($data as $keyFifo => $valueOther) {
    foreach ($valueOther['hist_det'] as $keyFifo => $valueFifo) {
        if ($valueFifo['SCR_OTHER'] == 1) {
            $subTotOthers += $valueFifo['SCR_QTY'];
            $subTotOthersPrice += $valueFifo['SCR_QTY'] * $valueFifo['SCR_ITMPRC'];
        }
    }
}
?>
<html>
<style type="text/css">
    .tg {
        border-collapse: collapse;
        border-color: #ccc;
        border-spacing: 0;
        border-color: black;
        border-style: none;
        border-width: 0px;
    }

    .tg2 {
        border-collapse: collapse;
        border-color: #ccc;
        border-spacing: 0;
    }

    .tg td {
        background-color: #fff;
        border-color: black;
        border-style: none;
        border-width: 0px;
        color: #333;
        font-family: Arial, sans-serif;
        font-size: 14px;
        overflow: hidden;
        padding: 7px 5px;
        word-break: normal;
    }

    .tg th {
        background-color: #f0f0f0;
        border-color: black;
        border-style: none;
        border-width: 0px;
        color: #333;
        font-family: Arial, sans-serif;
        font-size: 14px;
        font-weight: normal;
        overflow: hidden;
        padding: 7px 5px;
        word-break: normal;
    }

    .tg .tg-abx8 {
        background-color: #c0c0c0;
        border-color: #c0c0c0;
        font-weight: bold;
        text-align: left;
        vertical-align: top
    }

    .tg .tg-1wig {
        font-weight: bold;
        text-align: left;
        vertical-align: top;
         /* font-size: 10px; */
    }

    .tg .tg-k4px {
        background-color: #f0f0f0;
        font-weight: bold;
        text-align: left;
        vertical-align: top
    }

    .tg .tg-baqh {
        text-align: center;
        vertical-align: top
    }

    .tg .tg-8rcp {
        background-color: #FFF;
        font-weight: bold;
        text-align: left;
        vertical-align: middle
    }

    .tg .tg-kftd {
        background-color: #efefef;
        text-align: right;
        vertical-align: top
    }

    .tg .tg-mft3 {
        background-color: #f0f0f0;
        border-color: #cccccc;
        text-align: left;
        vertical-align: top
    }

    .tg .tg-mv9k {
        border-color: #f0f0f0;
        text-align: left;
        vertical-align: top
    }

    .tg .tg-0pky {
        border-color: inherit;
        text-align: left;
        vertical-align: top
    }

    .tg .tg-0lax {
        text-align: left;
        vertical-align: top
    }

    .tg .tg-z3qe {
        background-color: #f0f0f0;
        border-color: inherit;
        font-size: 20px;
        text-align: center;
        vertical-align: top
    }

    .tg .tg-odmy {
        background-color: #f0f0f0;
        text-align: left;
        vertical-align: top
    }

    .tg .tg-y6fn {
        background-color: #c0c0c0;
        margin: 0;
        text-align: left;
        vertical-align: top
    }

    .tg .tg-y6fn-title {
        background-color: #c0c0c0;
        margin: 0;
        text-align: center;
        vertical-align: top
    }

    .tg .tg-y6fn-amount {
        background-color: #c0c0c0;
        margin: 0;
        text-align: right;
        vertical-align: top
    }

    .tg .tg-73oq {
        border-color: #000000;
        text-align: center;
        vertical-align: top
    }

    .tg .tg-0lax {
        text-align: left;
        vertical-align: top
    }

    .tg .tg-mqa1 {
        border-color: #000000;
        font-weight: bold;
        text-align: center;
        vertical-align: top
    }

    .tg .tg-0lax {
        text-align: left;
        vertical-align: top
    }

    .tg .tg-amwm {
        font-weight: bold;
        text-align: center;
        vertical-align: top
    }

    .tg .tg-baqh {
        text-align: center;
        vertical-align: top;
    }


    .tg2 {
        border-collapse: collapse;
        border-spacing: 0;
    }

    .tg2 td {
        border-color: black;
        border-style: solid;
        border-width: 1px;
        font-family: Arial, sans-serif;
        font-size: 14px;
        overflow: hidden;
        padding: 7px 5px;
        word-break: normal;
    }

    .tg2 th {
        border-color: black;
        border-style: solid;
        border-width: 1px;
        font-family: Arial, sans-serif;
        font-size: 14px;
        font-weight: normal;
        overflow: hidden;
        padding: 7px 5px;
        word-break: normal;
    }

    .tg2 .tg2-0lax {
        text-align: left;
        vertical-align: top
    }

    .ttdAkhir {
            position: relative; 
            bottom: 0px;
            padding-top: 10;
            right:0px;
            float: right;
            display: inline;
        }
    @media print {
        .ttdAkhir {
            position: relative; 
            bottom: 0px;
            padding-top: 10;
            right:0px;
            float: right;
            display: inline;
        }
    }
</style>
<table class="tg">
    <thead>
        <tr>
            <th class="tg-mv9k" colspan="2">PT SMT Indonesia</th>
            <th class="tg-0pky"></th>
            <th class="tg-0lax"></th>
            <th class="tg-0lax"></th>
            <th class="tg-0lax"></th>
            <th class="tg-0lax"></th>
            <th class="tg-0lax"></th>
            <th class="tg-0pky" colspan="5" style="text-align: right;">Form : FSOP-13-01</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="tg-mft3"></td>
            <td class="tg-mft3"></td>
            <td class="tg-mft3"></td>
            <td class="tg-mft3"></td>
            <td class="tg-mft3"></td>
            <td class="tg-mft3"></td>
            <td class="tg-mft3"></td>
            <td class="tg-mft3"></td>
            <td class="tg-mft3" colspan="5" style="text-align: right;">Rev. 02</td>
        </tr>
        <tr>
            <td class="tg-z3qe" colspan="13">
                @if($data[0]['REASON'] === 'LOSS')
                <span style="font-weight:bold">SCRAP REPORT (LOSS PRODUCTION)</span>
                @else
                <span style="font-weight:bold">SCRAP REPORT</span>
                @endif
            </td>
        </tr>
        <tr>
            <td class="tg-odmy" colspan="2"><span style="font-weight:bold">DOC. NUMBER</span></td>
            <td class="tg-k4px">: {{$data[0]['ID_TRANS']}}</td>
            <td class="tg-odmy"></td>
            <td class="tg-odmy"></td>
            <td class="tg-odmy"></td>
            <td class="tg-odmy"></td>
            <td class="tg-odmy"><b>DATE</b></td>
            <td class="tg-odmy" style="min-width: 200px">: {{(date('Y-m-d H:i:s', strtotime($data[0]['created_at'])))}}</td>
            <td class="tg-odmy"></td>
            <td class="tg-odmy"></td>
            <td class="tg-odmy"></td>
            <td class="tg-odmy"></td>
        </tr>
        <tr>
            <td class="tg-1wig" colspan="2">MODEL</td>
            <td class="tg-1wig">: ATTACHED</td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"><b>FOUND AT</b></td>
            <td class="tg-0lax" style="min-width: 200px">: ATTACHED</td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
        <tr>
            <td class="tg-1wig" colspan="2">PART CODE</td>
            <td class="tg-1wig">: {{count($data) === 1 ? $data[0]['ITEMNUM'] : 'ATTACHED'}}</td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-8rcp">ISSUED BY</td>
            <td class="tg-0lax">: {{$data[0]['users']['MSTEMP_FNM']}} {{$data[0]['users']['MSTEMP_LNM']}}</td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
        <tr>
            <td class="tg-1wig" colspan="2">PART NAME</td>
            <td class="tg-1wig" style="min-width: 250px;">: {{count($data) === 1 ? $data[0]['item_det']['MITM_ITMD1'] : 'ATTACHED'}}</td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-8rcp">DEPARTMENT</td>
            <td class="tg-0lax">: {{$data[0]['DEPT']}}</td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
        <tr>
            <td class="tg-1wig" colspan="2" style="min-width: 200px;">JOB NO / DO NO</td>
            <td class="tg-1wig">: {{'ATTACHED'}}</td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-8rcp">SCRAP REMARK</td>
            <td class="tg-0lax" colspan="3">: {{$data[0]['MAIN_REASON']}}</td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
        <tr>
            <td class="tg-abx8" colspan="3">PROCESS SCRAP :</td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
        </tr>
        <tr>
            <td class="tg-y6fn" colspan="5"><b>{{date('l')}}, {{date('d M Y')}}</b></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-title"><b>QTY</b></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-title" colspan="5"><b>AMOUNT</b></td>
        </tr>
        <!-- End hitung subtotal Model -->
        <tr>
            <td class="tg-y6fn" colspan="4"><b>RM</b></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-title"></td>
            <td class="tg-y6fn-title" colspan="6"></td>
        </tr>
        <tr>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn" colspan="4">Part Level</td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-amount">{{$subTotPart === 0 ? '-' : $subTotPart}}</td>
            <td class="tg-y6fn-amount" colspan="6">$ {{$subTotHargaPart === 0 ? 0 : number_format($subTotHargaPart, 4)}}</td>
            <!-- <td class="tg-y6fn" colspan="4">$ {{json_encode($cekPart)}}</td> -->
        </tr>
        <tr>
            <td class="tg-y6fn" colspan="4"><b>WIP</b></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-title"></td>
            <td class="tg-y6fn-title" colspan="6"></td>
        </tr>
        <?php
        $totqty = $totharga = 0;
        ?>

        <!-- Start hitung subtotal Model -->
        @foreach($processmstr as $key => $val)
            <?php
            $totpartHeader = 0;

            $subtotqtynya = $subtotharga = 0;

            $test = [];
            $hasilcekDet = $totalAmountPerProcess = 0;
            $cekArr = [];
            foreach ($data as $keyDataParsing => $valueDataParsing) {
                if ($valueDataParsing['MENU_ID'] === 'wip') {
                    $cekArr[] = $valueDataParsing;

                    /* looping detail */
                    // logger($valueDataParsing['ITEMNUM']);
                    // New Code compare design code
                    if ($valueDataParsing['design_map'] && isset($valueDataParsing['design_map']['RPDSGN_PRO_ID']) && $valueDataParsing['design_map']['RPDSGN_PRO_ID'] === $val['RPDSGN_CODE']) {
                        $subtotqtynya += isset($valueDataParsing['QTY']) ? (int)$valueDataParsing['QTY'] : 0;
                    }

                    /* Sum All Part Price */

                    // New Code Sum Price per Part
                    $subtothargadet = 0;
                    if (count($valueDataParsing['hist_det']) > 0 && $valueDataParsing['design_map']) {
                        foreach ($valueDataParsing['hist_det'] as $keyFifo => $valueFifo) {
                            $subtothargadet += $val['RPDSGN_CODE'] === $valueDataParsing['design_map']['RPDSGN_PRO_ID'] ? floatval($valueFifo['SCR_ITMPRC']) * $valueFifo['SCR_QTY'] : 0;
                            $test[$keyDataParsing][] = [$valueFifo['SCR_ITMPRC'], $valueFifo['SCR_ITMPRC'] * $valueFifo['SCR_QTY']];
                        }
                    }

                    $subtotharga += $subtothargadet;
                }
                // else {
                //     if ($valueDataParsing['MDL_FLAG'] == "1" && !$valueDataParsing['IS_RETURN']) {
                //         $cekArr[] = $valueDataParsing;

                //         /* looping detail */
                //         // logger($valueDataParsing['ITEMNUM']);
                //         // New Code compare design code
                //         if ($valueDataParsing['design_map'] && isset($valueDataParsing['design_map']['RPDSGN_PRO_ID']) && $valueDataParsing['design_map']['RPDSGN_PRO_ID'] === $val['RPDSGN_CODE']) {
                //             $subtotqtynya += isset($valueDataParsing['QTY']) ? (int)$valueDataParsing['QTY'] : 0;
                //         }

                //         /* Sum All Part Price */

                //         // New Code Sum Price per Part
                //         $subtothargadet = 0;
                //         if (count($valueDataParsing['hist_det']) > 0 && $valueDataParsing['design_map']) {
                //             foreach ($valueDataParsing['hist_det'] as $keyFifo => $valueFifo) {
                //                 $subtothargadet += $val['RPDSGN_CODE'] === $valueDataParsing['design_map']['RPDSGN_PRO_ID'] ? floatval($valueFifo['SCR_ITMPRC']) * $valueFifo['SCR_QTY'] : 0;
                //                 $test[$keyDataParsing][] = [$valueFifo['SCR_ITMPRC'], $valueFifo['SCR_ITMPRC'] * $valueFifo['SCR_QTY']];
                //             }
                //         }

                //         $subtotharga += $subtothargadet;
                //     }
                // }
            }
            $totqty += $subtotqtynya;
            $totharga += $subtotharga;
            ?>
            <tr>
                <td class="tg-y6fn"></td>
                <td class="tg-y6fn" colspan="4">{{$val['RPDSGN_DESC']}}</td>
                <td class="tg-y6fn"></td>
                <td class="tg-y6fn-amount">{{$subtotqtynya === 0 ? '-' : $subtotqtynya}}</td>
                <td class="tg-y6fn-amount" colspan="6">$ {{ $subtotharga === 0 ? '0' : number_format($subtotharga, 4) }}</td>
                <!-- <td class="tg-y6fn" colspan="4">$ {{ json_encode($test) }}</td> -->
            </tr>
        @endforeach
        <tr>
            <td class="tg-y6fn" colspan="5"><b>FG</b></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-title"></td>
            <td class="tg-y6fn-title" colspan="5"></td>
        </tr>
        <tr>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn" colspan="4">Fresh</td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-amount">{{$subTotFinGoodFresh === 0 ? '-' : $subTotFinGoodFresh}}</td>
            <td class="tg-y6fn-amount" colspan="6">$ {{$subTotHargaFinGoodFresh === 0 ? '0' : number_format($subTotHargaFinGoodFresh, 4)}}</td>
            <!-- <td class="tg-y6fn" colspan="4">$ {{json_encode($cekPart)}}</td> -->
        </tr>
        <tr>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn" colspan="4">Pending</td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-amount">{{$subTotFinGoodPending === 0 ? '-' : $subTotFinGoodPending}}</td>
            <td class="tg-y6fn-amount" colspan="6">$ {{$subTotHargaFinGoodPending === 0 ? '0' : number_format($subTotHargaFinGoodPending, 4)}}</td>
            <!-- <td class="tg-y6fn" colspan="4">$ {{json_encode($cekPart)}}</td> -->
        </tr>
        <tr>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn" colspan="4">Return</td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-amount"></td>
            <td class="tg-y6fn-amount" colspan="6"></td>
            <!-- <td class="tg-y6fn" colspan="4">$ {{json_encode($cekPart)}}</td> -->
        </tr>
        <tr>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn" colspan="4">Asset</td>
            <td class="tg-y6fn-amount">{{$subTotFinGoodReturnA === 0 ? '-' : $subTotFinGoodReturnA}}</td>
            <td class="tg-y6fn-amount" colspan="6">$ {{$subTotFinGoodReturnHargaPartA === 0 ? '0' : number_format($subTotFinGoodReturnHargaPartA, 4)}}</td>
        </tr>
        <tr>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn" colspan="4">Non Asset</td>
            <td class="tg-y6fn-amount">{{$subTotFinGoodReturnNA === 0 ? '-' : $subTotFinGoodReturnNA}}</td>
            <td class="tg-y6fn-amount" colspan="6">$ {{$subTotFinGoodReturnHargaPartNA === 0 ? '0' : number_format($subTotFinGoodReturnHargaPartNA, 4)}}</td>
        </tr>
        <tr>
            <td class="tg-y6fn" colspan="5"><b>Machine & Equipment</b></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-title"></td>
            <td class="tg-y6fn-title" colspan="5"></td>
        </tr>
        <tr>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn" colspan="4">W/O Part Of</td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-amount">{{$subTotMch === 0 ? '-' : $subTotMch}}</td>
            <td class="tg-y6fn-amount" colspan="6">$ {{$subTotMchHargaPart === 0 ? '0' : number_format($subTotMchHargaPart, 4)}}</td>
            <!-- <td class="tg-y6fn" colspan="4">$ {{json_encode($cekPart)}}</td> -->
        </tr>
        <tr>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn" colspan="4">W/ Part Of</td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-amount">{{$subTotMchPartOf === 0 ? '-' : $subTotMchPartOf}}</td>
            <td class="tg-y6fn-amount" colspan="6">$ {{$subTotMchHargaPartPartOf === 0 ? '0' : number_format($subTotMchHargaPartPartOf, 4)}}</td>
            <!-- <td class="tg-y6fn" colspan="4">$ {{json_encode($cekPart)}}</td> -->
        </tr>
        <tr>
            <td class="tg-y6fn" colspan="5"><b>Other</b></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-amount">{{$subTotOthers === 0 ? '-' : $subTotOthers}}</td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-amount" colspan="5">$ {{$subTotOthersPrice === 0 ? '0' : number_format($subTotOthersPrice, 4)}}</td>
            <!-- <td class="tg-y6fn" colspan="4">$ {{json_encode($cekPart)}}</td> -->
        </tr>
        <!-- End hitung subtotal Material -->

        <tr>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-1wig">TOTAL</td>
            <td class="tg-y6fn-amount">{{
                $totqty +
                $subTotPart +
                $subTotFinGoodFresh +
                $subTotFinGoodPending +
                $subTotFinGoodReturnA +
                $subTotFinGoodReturnNA +
                $subTotMch +
                $subTotOthers
            }}</td>
            <td class="tg-y6fn-amount"></td>
            <td class="tg-y6fn-amount" colspan="5">$ {{number_format((
                $totharga +
                $subTotHargaPart +
                $subTotHargaFinGoodFresh +
                $subTotHargaFinGoodPending +
                $subTotFinGoodReturnHargaPartA +
                $subTotFinGoodReturnHargaPartNA +
                $subTotMchHargaPart +
                $subTotOthersPrice
            ), 4)}}</td>
            <td class="tg-0lax"></td>
        </tr>

        <tr>
            <td class="tg-baqh" style="border: 1px solid;" colspan="13"><span style="font-weight:bold">CHECKED AND APPROVED BY</span></td>
        </tr>
        <tr>
            <td style="border: 1px solid;" class="tg-mqa1" colspan="5">MANAGER QA</td>
            <td style="border: 1px solid;" class="tg-mqa1" colspan="3">MANAGER MFG</td>
            <td style="border: 1px solid;" class="tg-mqa1" colspan="5">MANAGER ENG</td>
        </tr>
        <tr>
            <td style="border: 1px solid;border-bottom: none;" class="tg-73oq" colspan="5"></td>
            <td style="border: 1px solid;border-bottom: none;" class="tg-73oq" colspan="3"></td>
            <td style="border: 1px solid;border-bottom: none;" class="tg-73oq" colspan="5"></td>
        </tr>
        <tr>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-73oq" colspan="5"></td>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-0lax" colspan="3"></td>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-0lax" colspan="5"></td>
        </tr>
        <tr>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-73oq" colspan="5"></td>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-0lax" colspan="3"></td>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-0lax" colspan="5"></td>
        </tr>
        <tr>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-73oq" colspan="5"></td>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-0lax" colspan="3"></td>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-0lax" colspan="5"></td>
        </tr>
        <tr>
            <td style="border: 1px solid;border-top: none;" class="tg-73oq" colspan="5"></td>
            <td style="border: 1px solid;border-top: none;" class="tg-0lax" colspan="3"></td>
            <td style="border: 1px solid;border-top: none;" class="tg-0lax" colspan="5"></td>
        </tr>
        <tr>
            <td class="tg-amwm" style="border: 1px solid;" colspan="5">PPIC</td>
            <td class="tg-amwm" style="border: 1px solid;" colspan="3">GENERAL MANAGER</td>
            <td class="tg-amwm" style="border: 1px solid;" colspan="5">PRESIDENT DIRECTOR</td>
        </tr>
        <tr>
            <td style="border: 1px solid;" class="tg-0lax" colspan="5" rowspan="5"></td>
            <td style="border: 1px solid;height: 120px;" class="tg-0lax" colspan="3" rowspan="5"></td>
            <td style="border: 1px solid;" class="tg-0lax" colspan="5" rowspan="5"></td>
        </tr>
        <tr>
            <td style="border: 1px none;" class="tg-0lax" colspan="5" rowspan="4"></td>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
        <tr>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
        <tr>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-amwm" style="border: 1px solid;" colspan="8">TRANSFER SCRAP</td>
        </tr>
        <tr>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-baqh" style="border: 1px solid;" colspan="3">Data Input</td>
            <td class="tg-baqh" style="border: 1px solid;" colspan="5">Received By Exim</td>
        </tr>
        <tr>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax" style="border: 1px solid;" colspan="3" rowspan="4"></td>
            <td class="tg-0lax" style="border: 1px solid;" colspan="5" rowspan="4"></td>
        </tr>
        <tr>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
        <tr>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
        <tr>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
    </tbody>
</table>
<!-- This is Detail -->
<div style="page-break-before:always; height: 100%">
    <table class="tg" style="width:100%">
        <thead>
            <tr>
                <th class="tg2-0lax" rowspan="2"><b>No</b></th>
                <th class="tg2-0lax" rowspan="2"><b>Model</b></th>
                <th class="tg2-0lax" rowspan="2"><b>Part Code</b></th>
                <th class="tg2-0lax" rowspan="2"><b>JOB / DO</b></th>
                <th class="tg2-0lax" rowspan="2"><b>REMARK</b></th>
                <th class="tg2-0lax" colspan="{{count($processmstr) + 4}}"><b>Proses</b></th>
            </tr>
            <tr class="tg-0lax">
                @foreach($processmstr as $key => $val)
                <th class="tg-0lax">{{$val['RPDSGN_CODE']}}</th>
                @endforeach
                <th class="tg-0lax">Part Level</th>
                <th class="tg-0lax">FG Return</th>
                <th class="tg-0lax">Machine & Equipment</th>
            </tr>
        </thead>
        <tbody>
            <?php $totalPro = [];
            $totalPart = $totalFGRet = $totalMCH = 0; ?>
            @foreach($data as $keyData => $valData)
            <tr>
                <td class="tg-0lax">{{$keyData + 1}}</td>
                <td class="tg-0lax">{{$valData['item_det']['MITM_ITMD1']}}</td>
                <td class="tg-0lax">{{$valData['ITEMNUM']}}</td>
                <td class="tg-0lax">{{$valData['DOC_NO']}}</td>
                <td class="tg-0lax">{{$valData['REASON']}}</td>
                @if($valData['MENU_ID'] === 'wip' || $valData['MENU_ID'] === 'warehouse' || $valData['MENU_ID'] === 'pending')
                    @foreach($processmstr as $keypro => $val)
                    <?php
                    $totalPerProcess = !$valData['design_map'] || $val['RPDSGN_CODE'] != $valData['design_map']['RPDSGN_PRO_ID'] ? 0 : $valData['QTY'];
                    $totalPro[$keypro] = isset($totalPro[$keypro]) ? $totalPro[$keypro] + $totalPerProcess : $totalPerProcess;
                    ?>
                    <td class="tg-0lax">{{$totalPerProcess === 0 ? '-' : $totalPerProcess}}</td>
                    @endforeach
                    @if($valData['MDL_FLAG'] == 0)
                        <?php
                        $totalPart += $valData['QTY'];
                        ?>
                        <td class="tg-0lax">{{$valData['QTY'] === 0 ? '-' : $valData['QTY']}}</td>
                    @else
                        <td class="tg-0lax">-</td>
                    @endif
                    <td class="tg-0lax">-</td>
                    <td class="tg-0lax">-</td>
                @elseif($valData['MENU_ID'] === 'fg_return')
                    @foreach($processmstr as $key => $val)
                    <td class="tg-0lax">-</td>
                    @endforeach
                    <td class="tg-0lax">-</td>
                    <?php
                    $totalFGRet += $valData['QTY'];
                    ?>
                    <td class="tg-0lax">{{$valData['QTY'] === 0 ? '-' : $valData['QTY']}}</td>
                    <td class="tg-0lax">-</td>
                    <td class="tg-0lax">-</td>
                @elseif($valData['MENU_ID'] === 'mchequip')
                    @foreach($processmstr as $key => $val)
                    <td class="tg-0lax">-</td>
                    @endforeach
                    <td class="tg-0lax">-</td>
                    <?php
                    $totalMCH += $valData['QTY'];
                    ?>
                    <td class="tg-0lax">-</td>
                    <td class="tg-0lax">{{$valData['QTY'] === 0 ? '-' : $valData['QTY']}}</td>
                @else
                @if($valData['MDL_FLAG'] == true && $valData['IS_RETURN'] == 0)
                    @foreach($processmstr as $keypro => $val)
                    <?php
                    $totalPerProcess = !$valData['design_map'] || $val['RPDSGN_CODE'] != $valData['design_map']['RPDSGN_PRO_ID'] ? 0 : $valData['QTY'];
                    $totalPro[$keypro] = isset($totalPro[$keypro]) ? $totalPro[$keypro] + $totalPerProcess : $totalPerProcess;
                    ?>
                    <td class="tg-0lax">{{$totalPerProcess === 0 ? '-' : $totalPerProcess}}</td>
                    @endforeach
                    <td class="tg-0lax">-</td>
                @else
                    @if($valData['MDL_FLAG'] == 0)
                        @foreach($processmstr as $key => $val)
                        <td class="tg-0lax">-</td>
                        @endforeach
                        <?php
                        $totalPart += $valData['QTY'];
                        ?>
                        <td class="tg-0lax">{{$valData['QTY'] === 0 ? '-' : $valData['QTY']}}</td>
                        <td class="tg-0lax">-</td>
                    @else
                        @if($valData['MENU_ID'] === 'mchequip')
                            @foreach($processmstr as $key => $val)
                            <td class="tg-0lax">-</td>
                            @endforeach
                            <td class="tg-0lax">-</td>
                            <?php
                            $totalMCH += $valData['QTY'];
                            ?>
                            <td class="tg-0lax">-</td>
                            <td class="tg-0lax">{{$valData['QTY'] === 0 ? '-' : $valData['QTY']}}</td>
                        @else
                            @foreach($processmstr as $key => $val)
                            <td class="tg-0lax">-</td>
                            @endforeach
                            <td class="tg-0lax">-</td>
                            <?php
                            $totalFGRet += $valData['QTY'];
                            ?>
                            <td class="tg-0lax">{{$valData['QTY'] === 0 ? '-' : $valData['QTY']}}</td>
                            <td class="tg-0lax">-</td>
                        @endif
                    @endif
                @endif
            @endif
            </tr>
            @endforeach
            <tr>
                <td class="tg-0lax" colspan="5">Total</td>
                @foreach($processmstr as $keypro2 => $valpro2)
                <td class="tg-0lax">{{isset($totalPro[$keypro2]) ? ($totalPro[$keypro2] === 0 ? '-' : $totalPro[$keypro2]) : '-'}}</td>
                @endforeach
                <td class="tg-0lax">{{$totalPart === 0 ? '-' : $totalPart}}</td>
                <td class="tg-0lax">{{$totalFGRet === 0 ? '-' : $totalFGRet}}</td>
                <td class="tg-0lax">{{$totalMCH === 0 ? '-' : $totalMCH}}</td>
            </tr>
        </tbody>
    </table>
    
    <div class="ttdAkhir">
        <b style="font-size: 18px;">Verification Date: .................................................</b>
        <div style="width: 400px;height: 130px;border: solid; border-width: 1px; right: 20px; bottom: 10;display: flex;-webkit-display: flex;-moz-display: flex;-ms--display: flex;
">
            <table class="tg2" style="width: 100%">
                <tr style="text-align: center">
                    <td>Sender</td>
                    <td>Receiver</td>
                </tr>
                <tr style="height: 70px">
                    <td></td>
                    <td></td>
                </tr>
                <tr style="height: 30px">
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
</div>

</html>
