<style type="text/css">
    .tg {
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
    }

    .tg td {
        border-color: black;
        border-style: solid;
        border-width: 1px;
        font-family: Arial, sans-serif;
        font-size: 14px;
        overflow: hidden;
        padding: 10px 5px;
        word-break: normal;
    }

    .tg th {
        border-color: black;
        border-style: solid;
        border-width: 1px;
        font-family: Arial, sans-serif;
        font-size: 14px;
        font-weight: normal;
        overflow: hidden;
        padding: 10px 5px;
        word-break: normal;
    }

    .tg .tg-0pky {
        border-color: inherit;
        text-align: left;
        vertical-align: top
    }
</style>
<div>
    @foreach ($data as $keyParseData => $valueParseData)
    <div style="page-break-before:always">
        <table class="tg">
            <thead>
                <tr>
                    <th>Part Number</th>
                    <th colspan="12">{{$valueParseData['ITEMNUM']}}</th>
                </tr>
                <tr>
                    <th>Part Name</th>
                    <th colspan="12">{{$valueParseData['item_det']['MITM_ITMD1']}}</th>
                </tr>
                <tr>
                    <th>{{$valueParseData['MDL_FLAG'] == 0 ? 'DO Number' : 'Job Number'}}</th>
                    <th colspan="12">{{$valueParseData['DOC_NO']}}</th>
                </tr>
                <tr>
                    <th>Scrap Qty</th>
                    <th colspan="12">{{$valueParseData['QTY']}}</th>
                </tr>
                <tr>
                    <th class="tg-0pky" rowspan="2">No</th>
                    <th class="tg-0pky" rowspan="2">Part Code</th>
                    <th class="tg-0pky" rowspan="2">Part Name</th>
                    <th class="tg-0pky" rowspan="2">PSN</th>
                    <th class="tg-0pky" rowspan="2">Category</th>
                    <th class="tg-0pky" rowspan="2">Line</th>
                    <th class="tg-0pky" rowspan="2">F/R</th>
                    <th class="tg-0pky" rowspan="2">MC Code</th>
                    <th class="tg-0pky" rowspan="2">Lot No</th>
                    <th class="tg-0pky" rowspan="2">Qty</th>
                    <th class="tg-0pky" rowspan="2">Per Use</th>
                    <th class="tg-0pky" rowspan="2">Price</th>
                    <th class="tg-0pky" rowspan="2">Total Price</th>
                </tr>
            </thead>
            <tbody>
                <?php $totalCount = 0; ?>
                @foreach($valueParseData['hist_det'] as $keyDataDet => $valDataDet)
                <tr>
                    <td>{{ $keyDataDet+1 }}</td>
                    <td>{{ $valDataDet['SCR_ITMCD'] }}</td>
                    <td>{{ $valDataDet['MITM_ITMD1'] }}</td>
                    <td>{{ $valDataDet['SCR_PSN'] }}</td>
                    <td>{{ $valDataDet['SCR_CAT'] }}</td>
                    <td>{{ $valDataDet['SCR_LINE'] }}</td>
                    <td>{{ $valDataDet['SCR_FR'] }}</td>
                    <td>{{ $valDataDet['SCR_MCZ'] }}</td>
                    <td>{{ $valDataDet['SCR_LOTNO'] }}</td>
                    <td>{{ $valDataDet['SCR_QTY'] }}</td>
                    <td>{{ floatval($valDataDet['SCR_QTPER']) }}</td>
                    <td>$ {{ floatval($valDataDet['SCR_ITMPRC']) }}</td>
                    <td>$ {{ $valDataDet['SCR_QTY'] * floatval($valDataDet['SCR_ITMPRC']) }}</td>
                </tr>

                <?php $totalCount += $valDataDet['SCR_QTY'] * floatval($valDataDet['SCR_ITMPRC']); ?>
                @endforeach
                <tr>
                    <td colspan="12">
                        Total
                    </td>
                    <td>$ {{$totalCount}}</td>
                </tr>
            </tbody>
        </table>
    </div>
    @endforeach
</div>
