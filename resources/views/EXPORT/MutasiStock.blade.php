<html>
    <table border="2">
        <thead>
            <tr>
                <th rowspan="2">NO</th>
                <th colspan="3">DOKUMEN PABEAN</th>
                <th colspan="2">SURAT JALAN</th>
                <th colspan="2">BUKTI PENDAFTARAN</th>
                <th rowspan="2">PENERIMA BARANG</th>
                <th rowspan="2">KODE BARANG</th>
                <th rowspan="2">NAMA BARANG</th>
                <th rowspan="2">JUMLAH</th>
                <th rowspan="2">SATUAN</th>
                <th rowspan="2">VALAS</th>
                <th rowspan="2">NILAI</th>
            </tr>
            <tr>
                <td>JENIS</td>
                <td>NOMOR</td>
                <td>TANGGAL</td>
                <td>NOMOR</td>
                <td>TANGGAL</td>
                <td>NOMOR</td>
                <td>TANGGAL</td>
            </tr>
        </thead>
    <tbody>
    @foreach($mutasi as $key => $hasil)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $hasil->RCV_BCTYPE }}</td>
            <td>{{ $hasil->RCV_BCNO }}</td>
            <td>{{ $hasil->RCV_BCDATE }}</td>
            <td>{{ $hasil->RCV_DONO }}</td>
            <td>{{ $hasil->RCV_RCVDATE }}</td>
            <td>{{ $hasil->RCV_RPNO }}</td>
            <td>{{ $hasil->RCV_RPDATE }}</td>
            <td>{{ $hasil->MSUP_SUPNM }}</td>
            <td>{{ $hasil->RCV_ITMCD }}</td>
            <td>{{ $hasil->MITM_ITMD1 }}</td>
            <td>{{ $hasil->RCV_QTY }}</td>
            <td>{{ $hasil->MSUP_SUPCR }}</td>
            <td>{{ $hasil->MITM_STKUOM }}</td>
            <td>{{ $hasil->QTY }}</td>
        </tr>
    @endforeach
    </tbody>
    </table>
</html>