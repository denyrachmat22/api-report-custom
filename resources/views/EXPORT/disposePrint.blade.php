<html>
    <style type="text/css">
        .tg {
            border-collapse: collapse;
            border-color: #ccc;
            border-spacing: 0;
            border-color: black;
            border-style: none;
            border-width: 0px;
            width: 100%;
        }

        .tg2 {
            border-collapse: collapse;
            border-color: #ccc;
            border-spacing: 0;
        }

        .tg3 {
            border-collapse: collapse;
            border-color: #ccc;
            border-spacing: 0;
            border-color: black;
            border-style: none;
            border-width: 0px;
        }

        .tg td {
            background-color: #fff;
            border-color: black;
            border-style: none;
            border-width: 0px;
            color: #333;
            font-family: Arial, sans-serif;
            font-size: 14px;
            overflow: hidden;
            padding: 7px 5px;
            word-break: normal;
        }

        .tg th {
            background-color: #f0f0f0;
            border-color: black;
            border-style: none;
            border-width: 0px;
            color: #333;
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            overflow: hidden;
            padding: 7px 5px;
            word-break: normal;
        }

        .tg .tg-abx8 {
            background-color: #c0c0c0;
            border-color: #c0c0c0;
            font-weight: bold;
            text-align: left;
            vertical-align: top
        }

        .tg .tg-1wig {
            font-weight: bold;
            text-align: left;
            vertical-align: top;
            /* font-size: 10px; */
        }

        .tg .tg-k4px {
            background-color: #f0f0f0;
            font-weight: bold;
            text-align: left;
            vertical-align: top
        }

        .tg .tg-baqh {
            text-align: center;
            vertical-align: top
        }

        .tg .tg-8rcp {
            background-color: #FFF;
            font-weight: bold;
            text-align: left;
            vertical-align: middle
        }

        .tg .tg-kftd {
            background-color: #efefef;
            text-align: right;
            vertical-align: top
        }

        .tg .tg-mft3 {
            background-color: #f0f0f0;
            border-color: #cccccc;
            text-align: left;
            vertical-align: top
        }

        .tg .tg-mv9k {
            border-color: #f0f0f0;
            text-align: left;
            vertical-align: top
        }

        .tg .tg-mv9k2 {
            background-color: #f0f0f0;
            text-align: left;
            vertical-align: top;
            border-spacing: 0;
            border-color: black;
            border-style: solid;
            border-width: 1px;
            text-align: center;
        }

        .tg .tg-mv9k3 {
            text-align: left;
            vertical-align: top;
            border-spacing: 0;
            border-color: black;
            border-style: solid;
            border-width: 1px;
            text-align: center;
            font-size: 11px;
        }

        .tg .tg-mv9k3Text {
            text-align: left;
            vertical-align: top;
            border-spacing: 0;
            border-color: black;
            border-style: solid;
            border-width: 1px;
            text-align: left;
        }

        .tg .tg-mv9k3Int {
            text-align: left;
            vertical-align: top;
            border-spacing: 0;
            border-color: black;
            border-style: solid;
            border-width: 1px;
            text-align: right;
            font-size: ;
        }

        .tg .tg-0pky {
            border-color: inherit;
            text-align: left;
            vertical-align: top
        }

        .tg .tg-0lax {
            text-align: left;
            vertical-align: top
        }

        .tg .tg-z3qe {
            background-color: #f0f0f0;
            border-color: inherit;
            font-size: 20px;
            text-align: center;
            vertical-align: top;
            font-weight: bold
        }



        .tg .tg-odmy {
            background-color: #f0f0f0;
            text-align: left;
            vertical-align: top
        }

        .tg .tg-y6fn {
            background-color: #c0c0c0;
            margin: 0;
            text-align: left;
            vertical-align: top
        }

        .tg .tg-y6fn-title {
            background-color: #c0c0c0;
            margin: 0;
            text-align: center;
            vertical-align: top
        }

        .tg .tg-y6fn-amount {
            background-color: #c0c0c0;
            margin: 0;
            text-align: right;
            vertical-align: top
        }

        .tg .tg-73oq {
            border-color: #000000;
            text-align: center;
            vertical-align: top
        }

        .tg .tg-0lax {
            text-align: left;
            vertical-align: top
        }

        .tg .tg-mqa1 {
            border-color: #000000;
            font-weight: bold;
            text-align: center;
            vertical-align: top
        }

        .tg .tg-0lax {
            text-align: left;
            vertical-align: top
        }

        .tg .tg-amwm {
            font-weight: bold;
            text-align: center;
            vertical-align: top
        }

        .tg .tg-baqh {
            text-align: center;
            vertical-align: top;
        }


        .tg2 {
            border-collapse: collapse;
            border-spacing: 0;
        }

        .tg2 td {
            border-color: black;
            border-style: solid;
            border-width: 1px;
            font-family: Arial, sans-serif;
            font-size: 14px;
            overflow: hidden;
            padding: 7px 5px;
            word-break: normal;
        }

        .tg2 th {
            border-color: black;
            border-style: solid;
            border-width: 1px;
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            overflow: hidden;
            padding: 7px 5px;
            word-break: normal;
        }

        .tg2 .tg2-0lax {
            text-align: left;
            vertical-align: top
        }

        .ttdAkhir {
            position: relative;
            bottom: 0px;
            padding-top: 10;
            right:0px;
            float: right;
            display: inline;
        }

        .ttdTengah {
            position: relative;
            bottom: 0px;
            padding-top: 30;
            right:0px;
            float: center;
            display: inline;
        }
        @media print {
            .ttdAkhir {
                position: relative;
                bottom: 0px;
                padding-top: 10;
                right:0px;
                float: right;
                display: inline;
            }
        }
    </style>

    <div>
        <table class="tg">
            <thead>
                <tr>
                    <th class="tg-mv9k">PT SMT Indonesia</th>
                    <th class="tg-0pky" style="text-align: right;">Form: FSOP-14-04, Rev. 02</th>
                </tr>
                <tr>
                    <th class="tg-z3qe" colspan="2">DISPOSAL APPROVAL</th>
                </tr>
            </thead>
        </table>

        <table class="tg">
            <thead>
                <tr>
                    <td class="tg-1wig"></td>
                    <td class="tg-1wig" colspan="2">PERIOD</td>
                    <td class="tg-1wig" colspan="2">: {{date('M Y', strtotime($header->RPDISP_FDT))}} - {{date('M Y', strtotime($header->RPDISP_LDT))}}</td>
                </tr>
                <tr>
                    <td class="tg-1wig"></td>
                    <td class="tg-1wig" colspan="2">DEPT. PIC / USER</td>
                    <td class="tg-1wig" colspan="2">: {{$header->RPDISP_DEPT}} / {{$header->MSTEMP_FNM}} {{$header->MSTEMP_LNM}}</td>
                </tr>
                <tr>
                    <td class="tg-1wig"></td>
                    <td class="tg-1wig"></td>
                    <td class="tg-1wig"></td>
                </tr>
                <tr>
                    <td class="tg-mv9k2">NO</td>
                    <td class="tg-mv9k2">PART NAME</td>
                    <td class="tg-mv9k2">SERIAL NO</td>
                    <td class="tg-mv9k2">MODEL / TYPE</td>
                    <td class="tg-mv9k2">QUANTITY</td>
                    <td class="tg-mv9k2">MAKER NAME</td>
                    <td class="tg-mv9k2">SUPPLIER NAME</td>
                    <td class="tg-mv9k2">ASSET NO</td>
                    <td class="tg-mv9k2">ORIGINAL AMOUNT</td>
                    <td class="tg-mv9k2">TOTAL AMOUNT</td>
                    <td class="tg-mv9k2">BOOK VALUE</td>
                    <td class="tg-mv9k2">REMARK</td>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $keyData => $valueData)
                    <tr>
                        <td class="tg-mv9k3">{{$keyData + 1}}</td>
                        <td class="tg-mv9k3">{{$valueData->SCRH_ITMD1}}</td>
                        <td class="tg-mv9k3">{{$valueData->SCRH_ITMCD}}</td>
                        <td class="tg-mv9k3">{{$valueData->SCRH_TYPMDLD}}</td>
                        <td class="tg-mv9k3">{{$valueData->SCRH_QTY}}</td>
                        <td class="tg-mv9k3">{{$valueData->SCRH_SPTNO}}</td>
                        <td class="tg-mv9k3">{{$valueData->SCRH_SUPCD}}</td>
                        <td class="tg-mv9k3"></td>
                        <td class="tg-mv9k3">$ {{number_format(round($valueData->SCRD_AMNT, 5), $valueData->SCRH_TYPMDLD !== 'FINISH GOOD' ? 2 : 2)}}</td>
                        <td class="tg-mv9k3">$ {{number_format(round($valueData->SCRD_TOTAMNT, 5), $valueData->SCRH_TYPMDLD !== 'FINISH GOOD' ? 2 : 2)}}</td>
                        <td class="tg-mv9k3"></td>
                        <td class="tg-mv9k3">{{$header->RPDISP_REMARK}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td class="tg-mv9k3" colspan="4">TOTAL QTY</td>
                    <td class="tg-mv9k3">{{$totQty}}</td>
                    <td class="tg-mv9k3" colspan="4"></td>
                    <td class="tg-mv9k3">$ {{number_format(round($totAmount, 5), 2)}}</td>
                    <td class="tg-mv9k3" colspan="2"></td>
                </tr>
                <tr>
                    <td class="tg-1wig" colspan="12"></td>
                </tr>
                <tr>
                    <td class="tg-mv9k3Text" colspan="4">* Tulis Lokasi Part yang akan di Dispose (MEGA)</td>
                    <td class="tg-mv9k3Text" colspan="8"></td>
                </tr>
                <tr>
                    <td class="tg-mv9k3Text" colspan="4">* Claim(DN/NO DN)</td>
                    <td class="tg-mv9k3" colspan="8">{{$header->RPDISP_DN}}</td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="ttdTengah">
        <br>
        <div style="width: 100%;height: 130px;border: solid; border-width: 1px; right: 20px; bottom: 10;display: flex;-webkit-display: flex;-moz-display: flex;-ms--display: flex;
">
            <table class="tg2" style="width: 100%">
                <tr style="text-align: center">
                    <td>Prepared By</td>
                    <td>Checked by Supervisor</td>
                    <td>Approved by Head Dept</td>
                    <td>Approved by FM</td>
                    <td>Approved by Director</td>
                    <td>Approved by PD</td>
                </tr>
                <tr style="height: 70px">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr style="height: 30px">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <br>
        <span>*) Untuk approval non asset sampai Head Dept</span>
    </div>

    <div class="ttdTengah">
        <br>
        <table class="tg">
            <tr>
                <td class="tg-1wig"></td>
                <td class="tg-1wig">FLOW SIGN STATUS : (BERI TANDA √ UNTUK YANG DIPILIH)</td>
            </tr>
            <tr>
                <td class="tg-mv9k3Text" style="width:30px; height: 30px"></td>
                <td class="tg-1wig">ASSET ==> Rule Pengajuan DISPOSE ==> Sign Approval Internal sampai dengan Presdir  ==> Stock Adjusment OUT ==> Part serahkan ke Exim</td>
            </tr>
            <tr>
                <td class="tg-mv9k3Text" style="width:30px; height: 30px"></td>
                <td class="tg-1wig">NON ASSET ==> Rule Pengajuan DISPOSE NON ASSET ==> Sign Approval Customer ==> Sign Approval Internal sampai dengan Presdir  ==> Stock Adjusment OUT ==> Part serahkan ke Exim</td>
            </tr>
        </table>
    </div>

    <div class="ttdAkhir">
        <br>
        <b style="font-size: 18px;">Verification Date: .................................................</b>
        <div style="width: 400px;height: 130px;border: solid; border-width: 1px; right: 20px; bottom: 10;display: flex;-webkit-display: flex;-moz-display: flex;-ms--display: flex;
">
            <table class="tg2" style="width: 100%">
                <tr style="text-align: center">
                    <td>Sender</td>
                    <td>Receiver</td>
                </tr>
                <tr style="height: 70px">
                    <td></td>
                    <td></td>
                </tr>
                <tr style="height: 30px">
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
</html>
