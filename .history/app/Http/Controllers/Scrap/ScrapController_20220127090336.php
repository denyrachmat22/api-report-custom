<?php

namespace App\Http\Controllers\Scrap;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// Models
use App\Model\MASTER\ITHMaster;
use App\Model\MASTER\ItemMaster;
use App\Model\WMS\API\RCVSCN;
use App\Model\RPCUST\processMstr;
use App\Model\MASTER\MEGAEMS\pis2Table;

// Traits
use App\Traits\traitNewInsertScrap;
class ScrapController extends Controller
{
    use traitNewInsertScrap;

    public function getListProcess()
    {
        return processMstr::get();
    }

    public function findRef(Request $req)
    {
        $cekITH = ITHMaster::from('ITH_TBL as it')->select(
            'it.ITH_ITMCD',
            DB::raw("
                CASE
                    WHEN it.ITH_WH = 'ARQA1' THEN it.ITH_DOC
                    ELSE
                    CASE
                        WHEN it.ITH_WH = 'QAFG' THEN it2.ITH_DOC
                        ELSE it.ITH_REMARK
                    END
                END AS ITH_DOC
            "),
            DB::raw("
                CASE
                    WHEN it.ITH_WH = 'ARQA1' THEN it.ITH_SER
                    ELSE
                    CASE
                        WHEN it.ITH_WH = 'QAFG' THEN it.ITH_SER
                        ELSE it.ITH_DOC
                    END
                END AS ITH_SER
            "),
            'MITM_MODEL',
            DB::raw('SUM(it.ITH_QTY) AS ITH_QTY')
        );

        if (is_array($req->from)) {
            $cekITH->whereIn('it.ITH_WH', $req->from);
        } else {
            $cekITH->where('it.ITH_WH', $req->from);
        }

        $hasil = $cekITH->where(DB::raw("
            CASE
                WHEN it.ITH_WH = 'ARQA1' THEN it.ITH_SER
                ELSE
                CASE
                    WHEN it.ITH_WH = 'QAFG' THEN it.ITH_SER
                    ELSE it.ITH_DOC
                END
            END"), $req->ref_no)
            ->whereRaw("CASE
                WHEN it.ITH_WH = 'ARQA1' THEN it.ITH_DOC
                ELSE
                CASE
                    WHEN it.ITH_WH = 'QAFG' THEN it2.ITH_DOC
                    ELSE it.ITH_REMARK
                END
            END IS NOT NULL")
            ->join('MITM_TBL', 'MITM_ITMCD', 'it.ITH_ITMCD')
            ->leftjoin(DB::raw("(
                    SELECT
                        it2.ITH_ITMCD,
                        it2.ITH_SER,
                        it2.ITH_DOC
                    FROM
                        PSI_WMS.dbo.ITH_TBL it2
                    WHERE it2.ITH_WH = 'AFWH3'
                    AND it2.ITH_FORM = 'INC-WH-FG'
                ) it2"), function ($o) {
                $o->on('it.ITH_SER', 'it2.ITH_SER');
                $o->on('it.ITH_ITMCD', 'it2.ITH_ITMCD');
            })
            ->groupBy(
                'it.ITH_ITMCD',
                'MITM_MODEL',
                'it.ITH_DOC',
                'it2.ITH_DOC',
                'it.ITH_SER',
                'it.ITH_WH',
                'it.ITH_REMARK'
            )
            ->havingRaw('SUM(it.ITH_QTY) > 0');
        
        // return $hasil->get();
        return is_array($req->from) ? $hasil->get() : $hasil->first();
    }

    public function findItem(Request $req)
    {
        $itemInit = ItemMaster::select(
            'MITM_ITMCD',
            'MITM_ITMD1',
            'MITM_MODEL',
            DB::raw('coalesce(CAST(SUM(ITH_QTY) AS INT), 0) AS ITH_QTY'),
            DB::raw("CONCAT(MITM_ITMD1, '(', MITM_ITMCD ,')') AS MODEL2")
        )->where('MITM_ITMCD', 'LIKE', $req->filter . '%')
            ->orWhere('MITM_ITMD1', 'LIKE', $req->filter . '%')
            ->join('ITH_TBL', function ($j) {
                $j->on('MITM_ITMCD', '=', 'ITH_ITMCD')
                    ->whereRaw("LEFT([ITH_WH], 4) IN ('ARWH', 'AFWH')");
            })
            ->groupBy(
                'MITM_ITMCD',
                'MITM_ITMD1',
                'MITM_MODEL'
            );

        if ($req->has('model') && !empty($req->model)) {
            return $itemInit->where('MITM_MODEL', (string)$req->model)->get();
        }

        return $itemInit->get();
    }

    public function findLot(Request $req)
    {
        $selHeader = [
            'RCVSCN_DONO',
            'RCVSCN_ITMCD',
            'RCVSCN_LOTNO'
            // 'SERD2_ITMCD',
            // 'SERD2_LOTNO'
        ];

        $DOSel = RCVSCN::select(
            array_merge(
                $selHeader,
                [DB::raw("CONCAT(RCVSCN_DONO, '-', RCVSCN_LOTNO) as KEY_LOT")],
                [DB::raw('CAST(SUM(RCVSCN_QTY) AS INT) AS QTY_TOT')],
                [DB::raw("(
                    SELECT SUM(ITH_QTY) FROM PSI_WMS.dbo.ITH_TBL IT
                    WHERE IT.ITH_ITMCD = RCVSCN_TBL.RCVSCN_ITMCD
                    AND IT.ITH_WH = 'ARWH9SC'
                ) AS QTY")]
                // [DB::raw('CAST(SUM(SERD2_QTY) AS INT) AS QTY_FG')]
            )
        )
            ->join('ITH_TBL', function ($j) {
                $j->on('RCVSCN_ITMCD', '=', 'ITH_ITMCD')
                    ->where('ITH_WH', 'LIKE', 'ARWH%');
            })
            ->where('RCVSCN_DONO', '<>', '')
            ->where('RCVSCN_LOTNO', '<>', '')
            ->where('RCVSCN_ITMCD', $req->item)
            ->groupBy(
                $selHeader
            );

        if ($req->has('filter')) {
            $DOSel->where(function ($q) use ($req) {
                $q->where('RCVSCN_DONO', 'LIKE', '%' . $req->filter . '%')
                    ->orWhere('RCVSCN_LOTNO', 'LIKE', '%' . $req->filter . '%');
            });
        }

        return $DOSel->get();
    }

    public function findJob(Request $req)
    {
        $searchJob = collect(DB::select(DB::raw("exec sr_sp_checkJobTotalScrapQty @model = '" . $req->data['item'] . "', @jobSearch = '" . $req->data['filter'] . "'")));

        return $searchJob;
    }

    public function directSaveScrap(Request $req, $type)
    {
        $data = $req->data;

        $this->type = $type;
        $this->user = $req->user;
        $this->dept = $req->dept;

        switch ($type) {
            case 'wip':
                return $this->wipTransaction($data);
                break;
            case 'pending':
                return $data;
                break;
            case 'warehouse':
                break;
            case 'return':
                break;
            default:
                return [
                    'status' => false,
                    'message' => 'No template type found !',
                    'data' => []
                ];
                break;
        }

        return $req;
    }
}
