<?php

namespace App\Http\Controllers\Scheduller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MASTER\ITHMaster;
use App\Jobs\MutasiStokDocPabeanJobs;
use App\Model\WMS\REPORT\MutasiStok;

class TarikdataheaderController extends Controller
{
    public function MutasiPabeanPerDokumen($met, $sj = null)
    {
        $insertJob = (new MutasiStokDocPabeanJobs($sj, $met));

        dispatch($insertJob)->onQueue('stock');

        return 'Request Mutasi Pabean Masuk Queued';
    }

    public function MutasiPabeanPerItem($met)
    {
        $insertJob = (new MutasiStokDocPabeanJobs($met, 'INC_ITM'));

        dispatch($insertJob)->onQueue('stock');

        return 'Request Mutasi Pabean Masuk By ITEM Queued';
    }

    public function MutasiPabeanKeluar($sj = null)
    {
        $insertJob = (new MutasiStokDocPabeanJobs($sj,'OUT'));

        dispatch($insertJob);

        return 'Request Mutasi Pabean Keluar Queued';
    }

    public function ResetMutasiMasuk()
    {
        
    }
}
