<?php

namespace App\Http\Controllers\Scheduller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MASTER\ITHMaster;
use App\Jobs\MutasiStokDocPabeanJobs;
use App\Model\WMS\REPORT\MutasiStok;

use App\Jobs\mutasiMasukRequest;
use Illuminate\Support\Facades\DB;

class TarikdataheaderController extends Controller
{
    public function MutasiPabeanPerDokumen($met, $sj = null)
    {
        $insertJob = (new MutasiStokDocPabeanJobs($sj, $met));

        dispatch($insertJob)->onQueue('stock');

        return 'Request Mutasi Pabean Masuk Queued';
    }

    public function MutasiPabeanPerItem($met)
    {
        $insertJob = (new MutasiStokDocPabeanJobs($met, 'INC_ITM'));

        dispatch($insertJob)->onQueue('stock');

        return 'Request Mutasi Pabean Masuk By ITEM Queued';
    }

    public function MutasiPabeanKeluar($sj = null)
    {
        $insertJob = (new MutasiStokDocPabeanJobs($sj,'OUT'));

        dispatch($insertJob);

        return 'Request Mutasi Pabean Keluar Queued';
    }

    public function ResetMutasiMasuk()
    {
        $getData = MutasiStok::leftjoin('MSUP_TBL', 'MSUP_SUPCD', '=', 'RCV_SUPCD')
            ->join('MITM_TBL', 'MITM_ITMCD', '=', 'RCV_ITMCD')
            ->leftjoin('PSI_RPCUST.dbo.RPSAL_BCSTOCK', function ($f) {
                $f->on('RCV_ITMCD', 'RPSTOCK_ITMNUM');
                $f->on('RCV_DONO', 'RPSTOCK_DOC');
                $f->on('RCV_BCNO', 'RPSTOCK_BCNUM');
                $f->on('RCV_RPNO', 'RPSTOCK_NOAJU');
            })
            ->where('RCV_DONO', '<>', '')
            ->where(DB::raw('
                CASE WHEN RPSTOCK_QTY IS NULL THEN 1
                ELSE
                    CASE WHEN RPSTOCK_QTY <> CAST(RCV_QTY AS int) THEN 1
                    ELSE 0
                    END
                END
            '), '1')
            ->where('RPSTOCK_TYPE', 'INC')
            ->get()
            ->toArray();

        // return $getData;
        $jum = 0;
        foreach ($getData as $key => $value) {
            $jum += 1;
            mutasiMasukRequest::dispatch($value)->onQueue('mutasiMasuk');
        }

        return 'berhasil';
    }
}
