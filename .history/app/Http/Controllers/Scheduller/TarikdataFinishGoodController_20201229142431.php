<?php

namespace App\Http\Controllers\Scheduller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\RPCUST\MutasiRaw;
use App\Helpers\CustomFunctionHelper;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MutasiBahanBakuExport;
use Illuminate\Support\Facades\DB;
use App\Model\MASTER\ItemMaster;
use App\Model\MASTER\ITHMaster;
use Illuminate\Support\Facades\Log;
use App\Model\RPCUST\MutasiDokPabean;
use App\Model\RPCUST\MutasiFinishGood;
use App\Jobs\MutasiStokDocPabeanJobs;
use App\Model\MASTER\SPLMaster;
use PhpParser\Node\Stmt\Foreach_;

class TarikdataFinishGoodController extends Controller
{
    public function index()
    {
        $select2 = [
            'ITH_ITMCD',
            'ITH_DATE',
            'ITH_DOC',
            'MITM_STKUOM'
            // 'ITH_SER'
        ];


        // Cek data finish good
        // $cekfingood = ITHMaster::select(array_merge($select2, [DB::raw('SUM(ITH_QTY) AS ITH_QTY')]))
        $cekfingood = ITHMaster::select(array_merge($select2, [DB::raw('SUM(ITH_QTY) * -1 AS ITH_QTY')]))
            ->join('MITM_TBL', 'MITM_ITMCD', 'ITH_ITMCD')
            ->with('dlv.serial.spl')
            ->with(['dlv' => function ($qdlv) {
                $qdlv->select(
                    'DLV_ID',
                    'DLV_DATE',
                    'DLV_CUSTCD',
                    'DLV_DOCREFF',
                    'DLV_SER',
                    'DLV_QTY',
                    'DLV_NOPEN',
                    'DLV_RPDATE',
                    'DLV_BCTYPE',
                    'DLV_NOAJU',
                    'DLV_BCDATE',
                    'MCUS_CUSNM',
                    'MCUS_ABBRV',
                    'MCUS_IVCUR'
                );
                $qdlv->join('MCUS_TBL', 'MCUS_CUSCD', 'DLV_CUSTCD');
                $qdlv->with(['serial' => function ($qser) {
                    $qser->select(
                        'SER_ID',
                        'SER_DOC',
                        'SER_ITMID',
                        'SER_QTY',
                        'SER_LUPDT',
                        'MITM_ITMD1',
                        'MITM_STKUOM',
                        'MSLSPRICE_TBL.*'
                    );
                    $qser->join('MSLSPRICE_TBL', 'MSLSPRICE_ITMCD', 'SER_ITMID');
                    $qser->join('MITM_TBL', 'MITM_ITMCD', 'SER_ITMID');
                    $qser->with(['spl' => function ($qspl) {
                        $qspl->select(
                            'SPL_DOC',
                            'SPL_CAT',
                            'SPL_LINE',
                            'SPL_FEDR',
                            'SPL_JOBNO',
                            'SPL_FG',
                            'SPL_ORDERNO',
                            'SPL_ITMCD',
                            'MITM_STKUOM',
                            'SPL_QTYREQ',
                            'SPL_QTYUSE',
                            DB::raw('SPL_QTYUSE * SPL_QTYREQ AS QTY_REQ_TOT'),
                            // 'SPLSCN_ID',
                            'SPLSCN_ORDERNO',
                            'SPLSCN_LOTNO',
                            'RETSCN_QTYAFT',
                            DB::raw('SUM(SPLSCN_QTY) AS SPLSCN_QTY'),
                            DB::raw('CAST(SPLSCN_LUPDT AS DATE) as SPLSCN_LUPDT'),
                            DB::raw('(
                                SELECT TOP 1 CAST(SPLSCN_LUPDT AS DATE) as SPLSCN_LUPDT
                                    FROM SPLSCN_TBL B
                                    WHERE B.SPLSCN_DOC = SPL_DOC
                                    AND B.SPLSCN_CAT = SPL_CAT
                                    AND B.SPLSCN_LINE = SPL_LINE
                                    AND B.SPLSCN_FEDR = SPL_FEDR
                                    AND B.SPLSCN_ORDERNO = SPL_ORDERNO
                                    AND B.SPLSCN_ITMCD = SPL_ITMCD
                                    AND B.SPLSCN_LOTNO = SPLSCN_LOTNO
                                    ORDER BY CAST(B.SPLSCN_LUPDT AS DATE) ASC
                            ) AS APAINISIH')
                        );
                        // $qspl->with('splscn');
                        $qspl->join('SPLSCN_TBL', function ($join) {
                            $join->on('SPLSCN_DOC', '=', 'SPL_DOC');
                            $join->on('SPLSCN_CAT', '=', 'SPL_CAT');
                            $join->on('SPLSCN_LINE', '=', 'SPL_LINE');
                            $join->on('SPLSCN_FEDR', '=', 'SPL_FEDR');
                            $join->on('SPLSCN_ITMCD', '=', 'SPL_ITMCD');
                            $join->on('SPLSCN_ORDERNO', '=', 'SPL_ORDERNO');
                        });
                        $qspl->leftjoin('RETSCN_TBL', function ($join) {
                            $join->on('RETSCN_SPLDOC', '=', 'SPL_DOC');
                            $join->on('RETSCN_CAT', '=', 'SPL_CAT');
                            $join->on('RETSCN_LINE', '=', 'SPL_LINE');
                            $join->on('RETSCN_FEDR', '=', 'SPL_FEDR');
                            $join->on('RETSCN_ITMCD', '=', 'SPL_ITMCD');
                            $join->on('RETSCN_ORDERNO', '=', 'SPL_ORDERNO');
                        });
                        $qspl->join('MITM_TBL', 'MITM_ITMCD', 'SPL_ITMCD');
                        $qspl->where('SPLSCN_SAVED', 1);
                        $qspl->groupBy([
                            'SPL_DOC',
                            'SPL_CAT',
                            'SPL_LINE',
                            'SPL_FEDR',
                            'SPL_JOBNO',
                            'SPL_FG',
                            'SPL_ORDERNO',
                            'SPL_ITMCD',
                            'MITM_STKUOM',
                            'SPL_QTYREQ',
                            'SPL_QTYUSE',
                            // 'SPLSCN_ID',
                            'SPLSCN_ORDERNO',
                            'SPLSCN_LOTNO',
                            'RETSCN_QTYAFT',
                            'SPLSCN_DOC',
                            'SPLSCN_CAT',
                            'SPLSCN_LINE',
                            'SPLSCN_FEDR',
                            'SPLSCN_ORDERNO',
                            'SPLSCN_ITMCD',
                            DB::raw('CAST(SPLSCN_LUPDT AS DATE)')
                        ]);
                    }]);
                    $qser->with(['serd2' => function ($qserd) {
                        $qserd->join('MITM_TBL', 'MITM_ITMCD', 'SERD2_ITMCD');
                    }]);
                }]);
            }])
            ->groupBy($select2)
            ->where('ITH_FORM', 'OUT-SHP-FG')
            ->where('ITH_DOC', 'SMT79348')
            ->get()
            ->toArray();

        // return $cekfingood;

        return $this->testingmaterialout2($cekfingood);

        $this->insertToMutasiFingood($cekfingood);
        $this->UsedMaterial($cekfingood);

        return $cekfingood;
    }

    public function insertToPabeanKeluar($data)
    {
        foreach ($data as $key => $value) {
            $cekdatapabeanout = MutasiDokPabean::where('RPBEA_TYPE', 'OUT')
                ->where('RPBEA_NUMSJL', $value['ITH_DOC'])
                ->where('RPBEA_ITMCOD', $value['ITH_ITMCD'])
                ->where('RPBEA_DOC', $value['ITH_SER'])
                ->get();

            if (count($cekdatapabeanout) === 0) {
                MutasiDokPabean::insert([
                    'RPBEA_JENBEA' => $value['dlv']['DLV_BCTYPE'],
                    'RPBEA_NUMBEA' => $value['dlv']['DLV_NOAJU'],
                    'RPBEA_TGLBEA' => $value['dlv']['DLV_BCDATE'],
                    'RPBEA_NUMSJL' => $value['dlv']['DLV_ID'],
                    'RPBEA_TGLSJL' => $value['dlv']['DLV_DATE'],
                    'RPBEA_NUMPEN' => $value['dlv']['DLV_NOPEN'],
                    'RPBEA_TGLPEN' => $value['dlv']['DLV_RPDATE'],
                    'RPBEA_CUSSUP' => $value['dlv']['DLV_CUSTCD'],
                    'RPBEA_ITMCOD' => $value['dlv']['serial']['SER_ITMID'],
                    'RPBEA_QTYSAT' => 1,
                    'RPBEA_QTYJUM' => (int) $value['dlv']['DLV_QTY'],
                    'RPBEA_UNITMS' => $value['dlv']['serial']['MITM_STKUOM'],
                    'RPBEA_VALAS' => $value['dlv']['MCUS_IVCUR'],
                    'RPBEA_QTYTOT' => (int) $value['dlv']['DLV_QTY'] * (int) $value['dlv']['serial']['MSLSPRICE_PRICE'],
                    'RPBEA_TYPE' => 'OUT',
                    'RPBEA_RCVEDT' => $value['ITH_DATE'],
                    'RPBEA_DOC' => $value['ITH_SER'],
                ]);
            } else {
                MutasiDokPabean::where('RPBEA_NUMSJL', $value['dlv']['DLV_ID'])
                    ->where('RPBEA_DOC', $value['ITH_SER'])->update([
                        'RPBEA_JENBEA' => $value['dlv']['DLV_BCTYPE'],
                        'RPBEA_NUMBEA' => $value['dlv']['DLV_NOAJU'],
                        'RPBEA_TGLBEA' => $value['dlv']['DLV_BCDATE'],
                        'RPBEA_TGLSJL' => $value['dlv']['DLV_DATE'],
                        'RPBEA_NUMPEN' => $value['dlv']['DLV_NOPEN'],
                        'RPBEA_TGLPEN' => $value['dlv']['DLV_RPDATE'],
                        'RPBEA_CUSSUP' => $value['dlv']['DLV_CUSTCD'],
                        'RPBEA_ITMCOD' => $value['dlv']['serial']['SER_ITMID'],
                        'RPBEA_QTYSAT' => 1,
                        'RPBEA_QTYJUM' => (int) $value['dlv']['DLV_QTY'],
                        'RPBEA_UNITMS' => $value['dlv']['serial']['MITM_STKUOM'],
                        'RPBEA_VALAS' => $value['dlv']['MCUS_IVCUR'],
                        'RPBEA_QTYTOT' => (int) $value['dlv']['DLV_QTY'] * (int) $value['dlv']['serial']['MSLSPRICE_PRICE'],
                        'RPBEA_TYPE' => 'OUT',
                        'RPBEA_RCVEDT' => $value['ITH_DATE'],
                    ]);
            }
        }
    }

    public function insertToMutasiFingood($data)
    {
        foreach ($data as $key => $value_head) {
            // Save output qty
            $cekdatafingood = MutasiFinishGood::where('RPGOOD_ITMCOD', $value_head['ITH_ITMCD'])
                ->where('RPGOOD_REF', $value_head['ITH_DOC'])
                ->first();
            if (empty($cekdatafingood)) {
                MutasiFinishGood::create([
                    'RPGOOD_ITMCOD' => $value_head['ITH_ITMCD'],
                    'RPGOOD_UNITMS' => $value_head['MITM_STKUOM'],
                    'RPGOOD_DATEIS' => $value_head['ITH_DATE'],
                    'RPGOOD_QTYINC' => 0,
                    'RPGOOD_QTYOUT' => (int) $value_head['ITH_QTY'],
                    'RPGOOD_QTYADJ' => 0,
                    'RPGOOD_QTYTOT' => (int) $value_head['ITH_QTY'] * -1,
                    'RPGOOD_QTYOPN' => 0,
                    'RPGOOD_KET' => '',
                    'RPGOOD_REF' => $value_head['ITH_DOC']
                ]);
            } else {
                $cekdatafingood->update([
                    'RPGOOD_UNITMS' => $value_head['MITM_STKUOM'],
                    'RPGOOD_DATEIS' => $value_head['ITH_DATE'],
                    'RPGOOD_QTYINC' => 0,
                    'RPGOOD_QTYOUT' => (int) $value_head['ITH_QTY'],
                    'RPGOOD_QTYADJ' => 0,
                    'RPGOOD_QTYTOT' => (int) $value_head['ITH_QTY'] * -1,
                    'RPGOOD_QTYOPN' => 0,
                    'RPGOOD_KET' => '',
                ]);
            }

            //Save input qty
            foreach ($value_head['dlv'] as $key_deep => $value_deep) {
                if (!empty($value_deep['serial'])) {
                    $cekdatafingood = MutasiFinishGood::where('RPGOOD_ITMCOD', $value_deep['serial']['SER_ITMID'])
                        ->where('RPGOOD_REF', $value_deep['serial']['SER_ID'])
                        ->first();

                    if (empty($cekdatafingood)) {
                        MutasiFinishGood::create([
                            'RPGOOD_ITMCOD' => $value_deep['serial']['SER_ITMID'],
                            'RPGOOD_UNITMS' => $value_deep['serial']['MITM_STKUOM'],
                            'RPGOOD_DATEIS' => $value_deep['serial']['SER_LUPDT'],
                            'RPGOOD_QTYINC' => (int) $value_deep['serial']['SER_QTY'],
                            'RPGOOD_QTYOUT' => 0,
                            'RPGOOD_QTYADJ' => 0,
                            'RPGOOD_QTYTOT' => (int) $value_deep['serial']['SER_QTY'],
                            'RPGOOD_QTYOPN' => 0,
                            'RPGOOD_KET' => '',
                            'RPGOOD_REF' => $value_deep['serial']['SER_ID']
                        ]);
                    } else {
                        $cekdatafingood->update([
                            'RPGOOD_UNITMS' => $value_deep['serial']['MITM_STKUOM'],
                            'RPGOOD_DATEIS' => $value_deep['serial']['SER_LUPDT'],
                            'RPGOOD_QTYINC' => (int) $value_deep['serial']['SER_QTY'],
                            'RPGOOD_QTYOUT' => 0,
                            'RPGOOD_QTYADJ' => 0,
                            'RPGOOD_QTYTOT' => (int) $value_deep['serial']['SER_QTY'],
                            'RPGOOD_QTYOPN' => 0,
                            'RPGOOD_KET' => '',
                        ]);
                    }
                }
            }
        }
    }

    public function MutasiPabeanKeluar($sj = null)
    {
        $insertJob = (new MutasiStokDocPabeanJobs($sj, 'OUT'));

        dispatch($insertJob);

        return 'Request Mutasi Pabean Keluar Queued';
    }

    public function UsedMaterial($data)
    {
        foreach ($data as $key => $value_head) { // parse list by ith dan surat jalan keluar
            foreach ($value_head['dlv'] as $val_det_head) { // parse by serial list
                $value = $val_det_head;
                $hasiltotalscan = [];
                if (isset($val_det_head['serial']['spl'])) {
                    foreach ($val_det_head['serial']['spl'] as $key => $val_det) { // parse by spl list by job order serial

                        // kombinasi PSL / PSN
                        $explodedcombination = trim($val_det['SPL_DOC']) . '|' . trim($val_det['SPL_LINE']) . '|' . trim($val_det['SPL_CAT']) . '|' . trim($val_det['SPL_FEDR']);

                        // cari keseluruhan item di table RPSAL_RAW berdasarkan kombinasi PSL / PSN
                        $cekbedawoqty = MutasiRaw::where('RPRAW_ITMCOD', trim($val_det['SPL_ITMCD']))
                            ->where('RPRAW_REF', 'like', $explodedcombination . '%')
                            ->get()
                            ->toArray();

                        // Get list job order di SPL
                        $cekwodispl = SPLMaster::select('SPL_JOBNO')
                            ->where('SPL_DOC', $val_det['SPL_DOC'])
                            ->where('SPL_LINE', $val_det['SPL_LINE'])
                            ->where('SPL_CAT', $val_det['SPL_CAT'])
                            ->where('SPL_FEDR', $val_det['SPL_FEDR'])
                            ->groupBy('SPL_JOBNO')
                            ->orderBy('SPL_JOBNO')
                            ->get();

                        $hasilspl = []; // INIT SPL
                        $countspl = 0; // INIT JUMLAH JOB ORDER
                        foreach ($cekwodispl as $keyspl => $valspl) { // Parsing list job order
                            $countspl++;
                            $hasilspl[$valspl['SPL_JOBNO']] = $countspl;
                        }

                        // Jumlah Finish good per serial di kali dengan QTY Use di SPL_TBL untik mengetahui pemakaian bahan baku
                        $qtyserial = $value['serial']['SER_QTY'] * $val_det['SPL_QTYUSE'];
                        $qtyserial = $val_det['SPLSCN_LUPDT'] === $val_det['APAINISIH'] && !empty($val_det['RETSCN_QTYAFT']) ? $qtyserial - $val_det['RETSCN_QTYAFT'] : $qtyserial;

                        if ($val_det['SPLSCN_LUPDT'] === $val_det['APAINISIH'] && !empty($val_det['RETSCN_QTYAFT']))
                            $hasiltotalscan[$value['DLV_SER']][$val_det['SPL_ORDERNO']][$val_det['SPL_ITMCD']] = [
                                $val_det['SPL_ORDERNO'],
                                $val_det['SPLSCN_LOTNO'],
                                $val_det['SPLSCN_QTY'],
                                $qtyserial,
                                $val_det['RETSCN_QTYAFT'],
                            ];

                        // Jumlah Finish good yang di kirim di kali dengan QTY Use di SPL_TBL untik mengetahui pemakaian bahan baku
                        $qtytot = intval($value_head['ITH_QTY']) * $val_det['SPL_QTYUSE'];

                        if (count($cekbedawoqty) === 0) { // jika di table RPSAL_RAW tidak ditemukan data PSN / PSL Tersebut

                            // Kombinasikan PSN / SPL + Job No + Order / MCZ / No Mesin + Lot Material
                            $combinesplorder = trim($val_det['SPL_DOC']) . '|' . trim($val_det['SPL_LINE']) . '|' . trim($val_det['SPL_CAT']) . '|' . trim($val_det['SPL_FEDR']) . '|' . trim($val_det['SPL_JOBNO']) . '|' . trim($val_det['SPLSCN_ORDERNO'] . '|' . trim($val_det['SPLSCN_LOTNO']));

                            $hasilout = intval($qtyserial) >= intval($val_det['SPL_QTYREQ']) // Jika qty scan lebih besar daripada require maka 
                                ? intval($val_det['SPL_QTYREQ']) // insert qty req
                                : intval($qtyserial); // insert qty scan

                            // Insert ke table RPSAL_RAW
                            MutasiRaw::create([
                                'RPRAW_ITMCOD' => trim($val_det['SPL_ITMCD']),
                                'RPRAW_UNITMS' => trim($val_det['MITM_STKUOM']),
                                'RPRAW_DATEIS' => trim($val_det['SPLSCN_LUPDT']),
                                'RPRAW_QTYINC' => 0,
                                'RPRAW_QTYOUT' => $hasilout,
                                'RPRAW_QTYADJ' => 0,
                                'RPRAW_QTYTOT' => $hasilout * -1,
                                'RPRAW_QTYOPN' => 0,
                                'RPRAW_KET' => '',
                                'RPRAW_REF' => $combinesplorder,
                            ]);
                        } else { // jika di table RPSAL_RAW ditemukan data PSN / PSL Tersebut

                            // Kombinasikan PSN / SPL + Job No + Order / MCZ / No Mesin + Lot Material
                            $combinesplorder = trim($val_det['SPL_DOC']) . '|' . trim($val_det['SPL_LINE']) . '|' . trim($val_det['SPL_CAT']) . '|' . trim($val_det['SPL_FEDR']) . '|' . trim($val_det['SPL_JOBNO']) . '|' . trim($val_det['SPLSCN_ORDERNO'] . '|' . trim($val_det['SPLSCN_LOTNO']));

                            $assignedqty2 = 0; // INIT assign qty yang mempunyai kesamaan dengan variable $combinesplorder di table RPSAL_RAW
                            $assignedqtyall = 0; // INIT assign qty keseluruhan SPL / PSN di table RPSAL_RAW
                            $cekoriginalref = []; // INIT variable $combinesplorder found di table RPSAL_RAW
                            foreach ($cekbedawoqty as $key_prevqty => $value_prevqty1) {
                                $assignedqtyall += intval($value_prevqty1['RPRAW_QTYOUT']); // Total keseluruhan item di SPL / PSN
                            }

                            foreach ($cekbedawoqty as $key_prevqty => $value_prevqty) {
                                if ($value_prevqty['RPRAW_REF'] === $combinesplorder) { // Jika variable $combinesplorder ditemukan di table RPSAL_RAW
                                    $assignedqty2 += intval($value_prevqty['RPRAW_QTYOUT']);
                                    $cekoriginalref[] = $value_prevqty['RPRAW_REF'];
                                }
                            }

                            // Hasil total yang akan di inser ke RPSAL_RAW
                            $hasilouts = $qtyserial >= intval($val_det['SPL_QTYREQ']) // Jika qty scan lebih besar daripada require maka 
                                ? $hasilspl[$val_det['SPL_JOBNO']] == count($cekwodispl) ? $qtyserial : intval($val_det['SPL_QTYREQ']) // insert qty req
                                : $qtyserial;
                            $hasilout = $hasilouts < 0 ? $hasilouts * -1 : $hasilouts;

                            if (count($cekoriginalref) > 0) { // Jika kombinasi variable $combinesplorder ditemukan
                                $tot = $assignedqty2 + $qtyserial;
                                if (($assignedqtyall + $hasilout) <= $qtytot) {
                                    MutasiRaw::whereIn('RPRAW_REF', $cekoriginalref)->update([
                                        'RPRAW_DATEIS' => trim($val_det['SPLSCN_LUPDT']),
                                        'RPRAW_QTYOUT' => $tot,
                                        'RPRAW_QTYTOT' => $tot * -1,
                                    ]);
                                }
                            } else {
                                if ($hasilout > 0) {
                                    MutasiRaw::create([
                                        'RPRAW_ITMCOD' => trim($val_det['SPL_ITMCD']),
                                        'RPRAW_UNITMS' => trim($val_det['MITM_STKUOM']),
                                        'RPRAW_DATEIS' => trim($val_det['SPLSCN_LUPDT']),
                                        'RPRAW_QTYINC' => 0,
                                        'RPRAW_QTYOUT' => $hasilout,
                                        'RPRAW_QTYADJ' => 0,
                                        'RPRAW_QTYTOT' => $hasilout * -1,
                                        'RPRAW_QTYOPN' => 0,
                                        'RPRAW_KET' => '',
                                        'RPRAW_REF' => $combinesplorder,
                                    ]);
                                }
                            }
                        }
                    }
                }

                logger(json_encode($hasiltotalscan));
            }
        }
    }

    public function testingmaterialout($data)
    {
        $fingood_material = [];
        foreach ($data as $key => $value) {
            $fingood_material[$value['ITH_ITMCD']]['DOC'] = $value['ITH_DOC'];
            $fingood_material[$value['ITH_ITMCD']]['SJ_QTY'] = $value['ITH_QTY'];
            $fingood_material[$value['ITH_ITMCD']]['SJ_DATE'] = $value['ITH_DATE'];

            $countarrdlv = 0;
            foreach ($value['dlv'] as $key_dlv => $value_dlv) {
                if ($value['ITH_ITMCD'] == $value_dlv['serial']['SER_ITMID']) {
                    $fingood_material[$value['ITH_ITMCD']]['JOB'] = $value_dlv['serial']['SER_DOC'];
                    $fingood_material[$value['ITH_ITMCD']]['LIST_SERIAL'][] = [
                        'SERIAL' => $value_dlv['DLV_SER'],
                        'QTY_SERIAL' => $value_dlv['DLV_QTY']
                    ];

                    $countspl = 0;
                    foreach ($value_dlv['serial']['spl'] as $key_spl => $value_spl) {
                        if (
                            $key_spl !== 0 && ($value_spl['SPL_DOC'] !== $value_dlv['serial']['spl'][$key_spl - 1]['SPL_DOC'] ||
                                $value_spl['SPL_CAT'] !== $value_dlv['serial']['spl'][$key_spl - 1]['SPL_CAT'] ||
                                $value_spl['SPL_LINE'] !== $value_dlv['serial']['spl'][$key_spl - 1]['SPL_LINE'] ||
                                $value_spl['SPL_FEDR'] !== $value_dlv['serial']['spl'][$key_spl - 1]['SPL_FEDR'])
                        ) {
                            $countspl++;
                        }

                        $fingood_material[$value['ITH_ITMCD']]['LIST_SERIAL'][$countarrdlv]['DATA_SPL'][$countspl] = [
                            'PSN_NO' => $value_spl['SPL_DOC'],
                            'PSN_CAT' => $value_spl['SPL_CAT'],
                            'PSN_LINE' => $value_spl['SPL_LINE'],
                            'PSN_FEEDER' => $value_spl['SPL_FEDR']
                        ];

                        $countspldet = 0;
                        foreach ($value_dlv['serial']['spl'] as $key_spl_det => $value_spl_det) {
                            if (
                                $value_spl['SPL_DOC'] == $value_spl_det['SPL_DOC'] &&
                                $value_spl['SPL_CAT'] == $value_spl_det['SPL_CAT'] &&
                                $value_spl['SPL_LINE'] == $value_spl_det['SPL_LINE'] &&
                                $value_spl['SPL_FEDR'] == $value_spl_det['SPL_FEDR']
                            ) {
                                if ($key_spl_det !== 0 && ($value_spl_det['SPL_ORDERNO'] !== $value_dlv['serial']['spl'][$key_spl_det - 1]['SPL_ORDERNO'] ||
                                    $value_spl_det['SPL_ITMCD'] !== $value_dlv['serial']['spl'][$key_spl_det - 1]['SPL_ITMCD'])) {
                                    $countspldet++;
                                }

                                $fingood_material[$value['ITH_ITMCD']]['LIST_SERIAL'][$countarrdlv]['DATA_SPL'][$countspl]['PSN_DET'][] = [
                                    'PSN_CAT' => $value_spl_det['SPL_CAT'],
                                    'MCH_NO' => $value_spl_det['SPL_ORDERNO'],
                                    'PART_NUM' => $value_spl_det['SPL_ITMCD'],
                                    'UM' => $value_spl_det['MITM_STKUOM'],
                                    'QTY_USE' => $value_spl_det['SPL_QTYUSE'],
                                    'QTY_REQ_PER_WO' => $value_spl_det['SPL_QTYREQ'],
                                    'SCAN_QTY' => $value_spl_det['SPLSCN_QTY'],
                                    'SCAN_LOT' => $value_spl_det['SPLSCN_LOTNO'],
                                    'RETURN_QTY' => $value_spl_det['RETSCN_QTYAFT'],
                                ];
                            }
                        }
                    }

                    $countarrdlv++;
                }
            }
        }

        $hasiloutmaterial = [];
        foreach ($fingood_material as $key_mat => $value_mat) {
            foreach ($value_mat['LIST_SERIAL'] as $key_ser => $value_ser) {
                foreach ($value_ser['DATA_SPL'] as $key_spl => $value_spl) {
                    foreach ($value_spl['PSN_DET'] as $key_scn => $value_scn) {
                        $combinedocs = $value_ser['SERIAL'] . '|' . $value_spl['PSN_NO'] . "|" . $value_spl['PSN_CAT'] . "|" . $value_spl['PSN_LINE'] . "|" . $value_spl['PSN_FEEDER'] . "|" . $value_scn['MCH_NO'] . "|" . $value_scn['SCAN_LOT'];
                        $totalout = $key_scn !== 0 && $value_scn['PART_NUM'] == $value_spl['PSN_DET'][$key_scn - 1]['PART_NUM'] ? 0 : ($value_ser['QTY_SERIAL'] * $value_scn['QTY_USE']);
                        MutasiRaw::updateOrCreate([
                            'RPRAW_REF' => $combinedocs,
                            'RPRAW_ITMCOD' => trim($value_scn['PART_NUM'])
                        ], [
                            'RPRAW_ITMCOD' => trim($value_scn['PART_NUM']),
                            'RPRAW_UNITMS' => trim($value_scn['UM']),
                            'RPRAW_DATEIS' => trim($value_mat['SJ_DATE']),
                            'RPRAW_QTYINC' => 0,
                            'RPRAW_QTYOUT' => $totalout,
                            'RPRAW_QTYADJ' => 0,
                            'RPRAW_QTYTOT' => $totalout * -1,
                            'RPRAW_QTYOPN' => 0,
                            'RPRAW_KET' => '',
                            'RPRAW_REF' => $combinedocs,
                        ]);

                        $hasiloutmaterial[] = [
                            'DOC' => $value_spl['PSN_NO'] . "|" . $value_spl['PSN_CAT'] . "|" . $value_spl['PSN_LINE'] . "|" . $value_spl['PSN_FEEDER'] . "|" . $value_scn['MCH_NO'] . "|" . $value_scn['SCAN_LOT'],
                            'ITEM_MAT' => $value_scn['PART_NUM'],
                            'UM' => $value_scn['UM'],
                            'QTY_OUT' => $key_scn !== 0 && $value_scn['PART_NUM'] === $value_spl['PSN_DET'][$key_scn - 1]['PART_NUM'] ? 0 :   $value_ser['QTY_SERIAL'],
                            'QTY_USE' => $value_scn['QTY_USE'],
                            'LOT' => $value_scn['SCAN_LOT'],
                        ];
                    }
                }
            }
        }

        return $hasiloutmaterial;
    }

    public function testingmaterialout2($data)
    {
        $fingood_material = [];
        foreach ($data as $key => $value) {
            $fingood_material[$value['ITH_ITMCD']]['DOC'] = $value['ITH_DOC'];
            $fingood_material[$value['ITH_ITMCD']]['SJ_QTY'] = $value['ITH_QTY'];
            $fingood_material[$value['ITH_ITMCD']]['SJ_DATE'] = $value['ITH_DATE'];

            $countarrdlv = 0;
            foreach ($value['dlv'] as $key_dlv => $value_dlv) {
                if ($value['ITH_ITMCD'] == $value_dlv['serial']['SER_ITMID']) {
                    $fingood_material[$value['ITH_ITMCD']]['JOB'] = $value_dlv['serial']['SER_DOC'];
                    $fingood_material[$value['ITH_ITMCD']]['LIST_SERIAL'][] = [
                        'SERIAL' => $value_dlv['DLV_SER'],
                        'QTY_SERIAL' => $value_dlv['DLV_QTY']
                    ];

                    $countspl = 0;
                    foreach ($value_dlv['serial']['serd2'] as $key_spl => $value_spl) {
                        if (
                            $key_spl !== 0 && ($value_spl['SPL_DOC'] !== $value_dlv['serial']['serd2'][$key_spl - 1]['SERD2_PSNNO'] ||
                                $value_spl['SPL_CAT'] !== $value_dlv['serial']['serd2'][$key_spl - 1]['SERD2_CAT'] ||
                                $value_spl['SPL_LINE'] !== $value_dlv['serial']['serd2'][$key_spl - 1]['SERD2_LINENO'] ||
                                $value_spl['SPL_FEDR'] !== $value_dlv['serial']['serd2'][$key_spl - 1]['SERD2_FR'])
                        ) {
                            $countspl++;
                        }

                        $fingood_material[$value['ITH_ITMCD']]['LIST_SERIAL'][$countarrdlv]['DATA_SPL'][$countspl] = [
                            'PSN_NO' => $value_spl['SERD2_PSNNO'],
                            'PSN_CAT' => $value_spl['SERD2_CAT'],
                            'PSN_LINE' => $value_spl['SERD2_LINENO'],
                            'PSN_FEEDER' => $value_spl['SERD2_FR']
                        ];

                        $countspldet = 0;
                        foreach ($value_dlv['serial']['serd2'] as $key_spl_det => $value_spl_det) {
                            if (
                                $value_spl['SERD2_PSNNO'] == $value_spl_det['SERD2_PSNNO'] &&
                                $value_spl['SERD2_CAT'] == $value_spl_det['SERD2_CAT'] &&
                                $value_spl['SERD2_LINENO'] == $value_spl_det['SERD2_LINENO'] &&
                                $value_spl['SERD2_FR'] == $value_spl_det['SERD2_FR']
                            ) {
                                if ($key_spl_det !== 0 && (
                                    // $value_spl_det['SPL_ORDERNO'] !== $value_dlv['serial']['spl'][$key_spl_det - 1]['SPL_ORDERNO'] ||
                                    $value_spl_det['SPL_ITMCD'] !== $value_dlv['serial']['spl'][$key_spl_det - 1]['SPL_ITMCD'])) {
                                    $countspldet++;
                                }

                                $fingood_material[$value['ITH_ITMCD']]['LIST_SERIAL'][$countarrdlv]['DATA_SPL'][$countspl]['PSN_DET'][] = [
                                    'PSN_CAT' => $value_spl_det['SERD2_CAT'],
                                    // 'MCH_NO' => $value_spl_det['SPL_ORDERNO'],
                                    'PART_NUM' => $value_spl_det['SERD2_ITMCD'],
                                    'UM' => $value_spl_det['MITM_STKUOM'],
                                    // 'QTY_USE' => $value_spl_det['SPL_QTYUSE'],
                                    // 'QTY_REQ_PER_WO' => $value_spl_det['SPL_QTYREQ'],
                                    'SCAN_QTY' => $value_spl_det['SERD2_QTY'],
                                    'SCAN_LOT' => $value_spl_det['SERD2_LOTNO'],
                                    // 'RETURN_QTY' => $value_spl_det['RETSCN_QTYAFT'],
                                ];
                            }
                        }
                    }

                    $countarrdlv++;
                }
            }
        }

        // return $fingood_material;

        $hasiloutmaterial = [];
        foreach ($fingood_material as $key_mat => $value_mat) {
            foreach ($value_mat['LIST_SERIAL'] as $key_ser => $value_ser) {
                if (isset($value_ser['DATA_SPL'])) {
                    foreach ($value_ser['DATA_SPL'] as $key_spl => $value_spl) {
                        foreach ($value_spl['PSN_DET'] as $key_scn => $value_scn) {
                            $combinedocs = $value_ser['SERIAL'] . '|' . $value_spl['PSN_NO'] . "|" . $value_spl['PSN_CAT'] . "|" . $value_spl['PSN_LINE'] . "|" . $value_spl['PSN_FEEDER'] . "|" . $value_scn['SCAN_LOT'];
                            // $totalout = $key_scn !== 0 && $value_scn['PART_NUM'] == $value_spl['PSN_DET'][$key_scn -1]['PART_NUM'] ? 0 : ($value_ser['QTY_SERIAL'] * $value_scn['QTY_USE']);
                            $totalout = $value_scn['SCAN_QTY'];
                            MutasiRaw::updateOrCreate([
                                'RPRAW_REF' => $combinedocs,
                                'RPRAW_ITMCOD' => trim($value_scn['PART_NUM'])
                            ], [
                                'RPRAW_ITMCOD' => trim($value_scn['PART_NUM']),
                                'RPRAW_UNITMS' => trim($value_scn['UM']),
                                'RPRAW_DATEIS' => trim($value_mat['SJ_DATE']),
                                'RPRAW_QTYINC' => 0,
                                'RPRAW_QTYOUT' => $totalout,
                                'RPRAW_QTYADJ' => 0,
                                'RPRAW_QTYTOT' => $totalout * -1,
                                'RPRAW_QTYOPN' => 0,
                                'RPRAW_KET' => '',
                                'RPRAW_REF' => $combinedocs,
                            ]);

                            $hasiloutmaterial[] = [
                                'DOC' => $value_spl['PSN_NO'] . "|" . $value_spl['PSN_CAT'] . "|" . $value_spl['PSN_LINE'] . "|" . $value_spl['PSN_FEEDER'] . "|" . $value_scn['SCAN_LOT'],
                                'ITEM_MAT' => $value_scn['PART_NUM'],
                                'UM' => $value_scn['UM'],
                                'QTY_OUT' => $key_scn !== 0 && $value_scn['PART_NUM'] === $value_spl['PSN_DET'][$key_scn - 1]['PART_NUM'] ? 0 :   $value_ser['QTY_SERIAL'],
                                // 'QTY_USE' => $value_scn['QTY_USE'],
                                'LOT' => $value_scn['SCAN_LOT'],
                            ];
                        }
                    }
                }
            }
        }

        return $hasiloutmaterial;
    }
}
