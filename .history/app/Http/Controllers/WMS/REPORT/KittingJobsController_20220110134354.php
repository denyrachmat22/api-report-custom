<?php

namespace App\Http\Controllers\WMS\REPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\WMS\API\SPLSCN;
use App\Model\WMS\API\SPLMaster;
use App\Model\MASTER\MEGAEMS\ppsn1Table;
use Illuminate\Support\Facades\DB;
use App\Model\MASTER\ItemMaster;
use App\Model\RPCUST\processMstr;
use App\Model\RPCUST\processMapMstr;
use App\Model\RPCUST\processMapDet;
use App\Exports\mappingProcessDataExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\mappingProcessDataImport;
use App\Model\MASTER\MEGAEMS\pis2Table;
use App\Model\RPCUST\scrapHist;
use App\Model\RPCUST\scrapHistDet;
use App\Model\WMS\API\RCVSCN;
use App\Model\WMS\REPORT\MutasiStok;
use App\Model\MASTER\MEGAEMS\msppTable;
use App\Model\MASTER\SERD2TBL;
use App\Model\RPCUST\SaldoWIP;
use App\Model\MASTER\ITHMaster;
use SnappyPDF;

use App\Exports\scrapReportCompFrontExport;
use App\Traits\traitInsertScrapDetailFromJobs;

class KittingJobsController extends Controller
{
    use traitInsertScrapDetailFromJobs;

    public function index2(Request $req)
    {
        return $this->parsingDataDev($req);
    }

    public function parsingData($req)
    {

        $selHeader = [
            'PPSN1_PSNNO', //SPL_DOC
            'PPSN2_ITMCAT', //SPL_CAT
            'PPSN1_LINENO', //SPL_LINE
            'PPSN1_FR',
            'PPSN1_PROCD'
        ];

        $selHeader2 = [
            'PPSN1_WONO',
            'PPSN1_SIMQT'
        ];

        $psn1WOSel = ppsn1Table::select($selHeader)
            ->join('SRVMEGA.PSI_MEGAEMS.dbo.PPSN2_TBL', function ($q1) {
                $q1->on('PPSN1_PSNNO', 'PPSN2_PSNNO');
                $q1->on('PPSN1_LINENO', 'PPSN2_LINENO');
                $q1->on('PPSN1_FR', 'PPSN2_FR');
                $q1->on('PPSN1_DOCNO', 'PPSN2_DOCNO');
                $q1->on('PPSN1_BSGRP', 'PPSN2_BSGRP');
            });

        if (isset($req->filter)) {
            $filter = is_array($req->filter) ? $req->filter : json_decode($req->filter);
            // $filter = $req->filter;
            foreach ($filter as $keyFil => $valFil) {
                $psn1WOSel->where($valFil[0], $valFil[1], $valFil[2]);
            }

            $listFilterUser = $psn1WOSel
                ->groupBy($selHeader)
                ->get()
                ->toArray();
        } else {
            $listFilterUser = [];
        }

        // return $listFilterUser;

        $parseToFIFO = [];
        foreach ($listFilterUser as $keyVal => $valueVal) {
            $getScan = SPLSCN::select([
                'SPLSCN_DOC',
                'SPLSCN_CAT',
                'SPLSCN_LINE',
                'SPLSCN_FEDR',
                'SPLSCN_ORDERNO',
                'SPLSCN_ITMCD',
                DB::raw('SUM(SPLSCN_QTY) AS SPLSCN_QTY'),
                'PIS2_WONO',
                DB::raw('SUM(PIS2_REQQT) AS PIS2_REQQT'),
                // 'RCV_PRPRC',
                // 'RCV_DONO'
            ])
                ->where('SPLSCN_DOC', $valueVal['PPSN1_PSNNO'])
                ->where('SPLSCN_CAT', $valueVal['PPSN2_ITMCAT'])
                ->where('SPLSCN_LINE', $valueVal['PPSN1_LINENO'])
                ->where('SPLSCN_FEDR', $valueVal['PPSN1_FR'])
                ->where('SPLSCN_CAT', $valueVal['PPSN2_ITMCAT'])
                ->where('SPLSCN_SAVED', 1)
                ->join('SRVMEGA.PSI_MEGAEMS.dbo.PPSN2_TBL', function ($q1) {
                    $q1->on('SPLSCN_DOC', 'PPSN2_PSNNO');
                    $q1->on('SPLSCN_LINE', 'PPSN2_LINENO');
                    $q1->on('SPLSCN_FEDR', 'PPSN2_FR');
                    $q1->on('SPLSCN_CAT', 'PPSN2_ITMCAT');
                    $q1->on('SPLSCN_ITMCD', 'PPSN2_SUBPN');
                    $q1->on('SPLSCN_ORDERNO', 'PPSN2_MCZ');
                })
                ->join('SRVMEGA.PSI_MEGAEMS.dbo.PIS2_TBL', function ($q1) {
                    $q1->on('PPSN2_DOCNO', 'PIS2_DOCNO');
                    $q1->on('SPLSCN_LINE', 'PIS2_LINENO');
                    $q1->on('SPLSCN_FEDR', 'PIS2_FR');
                    $q1->on('PPSN2_MC', 'PIS2_MC');
                    $q1->on('PPSN2_MCZ', 'PIS2_MCZ');
                    // $q1->on('SPLSCN_ITMCD', 'PIS2_MPART');
                })
                ->orderBy('PIS2_WONO', 'asc')
                ->orderBy('SPLSCN_ORDERNO', 'asc')
                ->groupBy([
                    'SPLSCN_DOC',
                    'SPLSCN_CAT',
                    'SPLSCN_LINE',
                    'SPLSCN_FEDR',
                    'SPLSCN_ORDERNO',
                    'SPLSCN_ITMCD',
                    'PIS2_WONO',
                    // 'RCV_PRPRC',
                    // 'RCV_DONO'
                ])
                ->get()
                ->toArray();

            $getWO = ppsn1Table::select($selHeader2)
                ->where('PPSN1_PSNNO', $valueVal['PPSN1_PSNNO'])
                ->where('PPSN1_LINENO', $valueVal['PPSN1_LINENO'])
                ->where('PPSN1_FR', $valueVal['PPSN1_FR'])
                ->groupBy($selHeader2)
                ->get()
                ->toArray();

            // $listingQTYScanByWO[] = array_merge($valueWO, ['SCAN' =>
            //     array_values(array_filter($getScan, function ($q) use ($valueWO) {
            //         return $q['PIS2_WONO'] === $valueWO['PPSN1_WONO'];
            //     }, ARRAY_FILTER_USE_BOTH ))
            // ]);

            foreach ($getScan as $keyFif => $valueFif) {
                // $getScan[$keyFif]['SPLSCN_QTY'] = $valueFif['SPLSCN_QTY'] - $valueFif['PIS2_REQQT'];
                $cekBefore = array_values(array_filter($getScan, function ($fScan) use ($valueFif) {
                    return $fScan['SPLSCN_ORDERNO'] === $valueFif['SPLSCN_ORDERNO']
                        && $fScan['PIS2_WONO'] !== $valueFif['PIS2_WONO']
                        && isset($fScan['SISA_SCAN']);
                }));

                $cekAfter = [];

                // Cek apakah masih ada item sama, order sama, dan wo tidak sama di array selanjutnya
                foreach ($getScan as $keyAfter => $valueAfter) {
                    if (
                        $valueAfter['SPLSCN_ORDERNO'] && $valueFif['SPLSCN_ORDERNO']
                        && $valueAfter['PIS2_WONO'] !== $valueFif['PIS2_WONO']
                        && $valueAfter['SPLSCN_ITMCD'] == $valueFif['SPLSCN_ITMCD']
                        && $keyAfter > $keyFif
                    ) {
                        $cekAfter[] = $valueAfter;
                    }
                }
                // $getScan[$keyFif]['WOQTY_SCAN'] = (count($cekBefore) > 0 ? $cekBefore[count($cekBefore) - 1]['WOQTY_SCAN'] : $valueFif['SPLSCN_QTY']) - $valueFif['PIS2_REQQT'];

                $sisaSebelMinQty = count($cekBefore) > 0 ? $cekBefore[count($cekBefore) - 1]['SISA_SCAN'] : $valueFif['SPLSCN_QTY'];

                $lastQty = $sisaSebelMinQty - $valueFif['PIS2_REQQT'];

                $totalQty = count($cekAfter) > 0
                    ? ($lastQty > 0
                        ? $valueFif['PIS2_REQQT']
                        : ($lastQty < 0
                            ? (count($cekBefore) > 0
                                ? 0
                                : $valueFif['SPLSCN_QTY'])
                            : $lastQty))
                    : $lastQty + $valueFif['PIS2_REQQT'];

                $getlot = SPLSCN::select(['SPLSCN_ITMCD', 'SPLSCN_LOTNO'])
                    ->where('SPLSCN_ITMCD', $valueFif['SPLSCN_ITMCD'])
                    ->where('SPLSCN_DOC', $valueFif['SPLSCN_DOC'])
                    ->where('SPLSCN_CAT', $valueFif['SPLSCN_CAT'])
                    ->where('SPLSCN_LINE', $valueFif['SPLSCN_LINE'])
                    ->where('SPLSCN_FEDR', $valueFif['SPLSCN_FEDR'])
                    ->where('SPLSCN_ORDERNO', $valueFif['SPLSCN_ORDERNO'])
                    ->groupBy(['SPLSCN_ITMCD', 'SPLSCN_LOTNO'])
                    ->get()
                    ->toArray();

                foreach ($getlot as $keyLot => $valueLot) {
                    $getrcv = RCVSCN::select([
                        'RCVSCN_DONO',
                        'RCVSCN_ITMCD',
                        'RCVSCN_LOTNO',
                        'RCV_PRPRC'
                    ])
                        ->where('RCVSCN_ITMCD', $valueLot['SPLSCN_ITMCD'])
                        ->where('RCVSCN_LOTNO', $valueLot['SPLSCN_LOTNO'])
                        ->join('RCV_TBL', function ($j1) {
                            $j1->on('RCV_DONO', 'RCVSCN_DONO');
                            $j1->on('RCV_ITMCD', 'RCVSCN_ITMCD');
                        })
                        ->groupBy([
                            'RCVSCN_DONO',
                            'RCVSCN_ITMCD',
                            'RCVSCN_LOTNO',
                            'RCV_PRPRC'
                        ])
                        ->orderBy('RCVSCN_DONO', 'DESC')
                        ->get()
                        ->toArray();

                    $getScan[$keyFif]['LOT_DET'][$keyLot] = array_merge($valueLot, ['LIST_DO' => $getrcv]);
                }
                // $getrcv = RCVSCN::where('RCVSCN_ITMCD', $valueFif['SPLSCN_ITMCD']);

                $getScan[$keyFif]['PROCD'] = $valueVal['PPSN1_PROCD'];
                $getScan[$keyFif]['SISA_SCAN'] = $lastQty;
                $getScan[$keyFif]['WOTOTQTY_SCAN'] = $totalQty;
                // $getScan[$keyFif]['TEST'] = $cekAfter;
            }

            $parseToFIFO[$keyVal] = array_merge($valueVal, ['LISTWO' => $getWO, 'DATA' => $getScan]);
            // $parseToFIFO[$keyVal] = $getScan;
        }

        $dataFinal = [];
        foreach ($parseToFIFO as $keyFinalFifo => $valueFinalFifo) {
            foreach ($valueFinalFifo['DATA'] as $keyFinalFifoDet => $valueFinalFifoDet) {
                $dataFinal[] = $valueFinalFifoDet;
            }
        }

        return $dataFinal;

        $kit = collect($dataFinal);

        return $kit;

        $rows = isset($req->pagination) ? $req['pagination']['rowsPerPage'] : 5;
        $page = isset($req->pagination) ? $req['pagination']['page'] : 1;

        if ($rows == 0) {
            $cekall = $kit->count();
            return $kit->paginate($cekall);
        } else {
            return $kit->paginate($rows, $req['pagination']['rowsNumber'], $page);
        }
    }

    public function parsingDataDev($req)
    {

        $selHeader = [
            'PPSN1_PSNNO', //SPL_DOC
            'PPSN2_ITMCAT', //SPL_CAT
            'PPSN1_LINENO', //SPL_LINE
            'PPSN1_FR', //SPL_FR
            'PPSN1_PROCD', // SPL_CODE
            'PIS2_WONO', // SPL_JOB
            // 'SPLSCN_DOC',
            // 'SPLSCN_CAT',
            // 'SPLSCN_LINE',
            // 'SPLSCN_FEDR',
            'SPLSCN_ORDERNO',
            'SPLSCN_ITMCD',
            'SPLSCN_QTY',
            'PIS2_REQQT',
            'RCVSCN_DONO',
            'RCVSCN_LOTNO',
            'RCV_PRPRC'
            // DB::raw('SUM(SPLSCN_QTY) AS SPLSCN_QTY'),
            // DB::raw('SUM(PIS2_REQQT) AS PIS2_REQQT'),
        ];

        $selHeader2 = [
            'PPSN1_WONO',
            'PPSN1_SIMQT'
        ];

        $psn1WOSel = ppsn1Table::select(array_merge($selHeader, [DB::raw('SUM(SPLSCN_QTY) AS SPLSCN_QTY'), DB::raw('SUM(PIS2_REQQT) AS PIS2_REQQT')]))
            ->join('SRVMEGA.PSI_MEGAEMS.dbo.PPSN2_TBL', function ($q1) {
                $q1->on('PPSN1_PSNNO', 'PPSN2_PSNNO');
                $q1->on('PPSN1_LINENO', 'PPSN2_LINENO');
                $q1->on('PPSN1_FR', 'PPSN2_FR');
                $q1->on('PPSN1_DOCNO', 'PPSN2_DOCNO');
                $q1->on('PPSN1_BSGRP', 'PPSN2_BSGRP');
            })
            ->join('SPLSCN_TBL', function ($spl) {
                $spl->on('PPSN1_PSNNO', 'SPLSCN_DOC');
                $spl->on('PPSN2_ITMCAT', 'SPLSCN_CAT');
                $spl->on('PPSN1_LINENO', 'SPLSCN_LINE');
                $spl->on('PPSN1_FR', 'SPLSCN_FEDR');
                $spl->on('PPSN2_MCZ', 'SPLSCN_ORDERNO');
            })
            ->join('SRVMEGA.PSI_MEGAEMS.dbo.PIS2_TBL', function ($q1) {
                $q1->on('PPSN2_DOCNO', 'PIS2_DOCNO');
                $q1->on('SPLSCN_LINE', 'PIS2_LINENO');
                $q1->on('SPLSCN_FEDR', 'PIS2_FR');
                $q1->on('PPSN2_MC', 'PIS2_MC');
                $q1->on('PPSN2_MCZ', 'PIS2_MCZ');
                // $q1->on('PPSN1_WONO', 'PIS2_WONO');
            })
            ->leftjoin('RCVSCN_TBL', function ($q1) {
                $q1->on('SPLSCN_ITMCD', 'RCVSCN_ITMCD');
                $q1->on('SPLSCN_LOTNO', 'RCVSCN_LOTNO');
                // $q1->on('SPLSCN_ITMCD', 'PIS2_MPART');
            })
            ->join('RCV_TBL', function ($q1) {
                $q1->on('RCVSCN_DONO', 'RCV_DONO');
                $q1->on('RCVSCN_ITMCD', 'RCV_ITMCD');
                // $q1->on('SPLSCN_ITMCD', 'PIS2_MPART');
            });

        // Jika ada parameter "filter" di variabel $req
        if (isset($req->filter)) {
            $filter = is_array($req->filter) ? $req->filter : json_decode($req->filter);
            foreach ($filter as $keyFil => $valFil) {
                $psn1WOSel->where($valFil[0], $valFil[1], $valFil[2]);
            }

            $listFilterUser = $psn1WOSel
                ->groupBy($selHeader)
                ->orderByRaw('
                    PPSN1_PSNNO asc,
                    PPSN2_ITMCAT asc,
                    PPSN1_LINENO asc,
                    PPSN1_FR asc,
                    PPSN1_PROCD asc,
                    PIS2_WONO asc
                ')
                ->get()
                ->toArray();
        } else { // Jika tidak ada
            $listFilterUser = [];
        }

        $hasilPSN = [];
        $hasilJOB = [];

        $hasilDET = [];

        $countPsn = 0;
        $countJob = 0;
        foreach ($listFilterUser as $key => $value) {
            $hasilDET[$key] = $value;
            // $hasilJOB[$countJob] = array_merge(['JOB_NO' => $value['PIS2_WONO']], ['JOB_DET' => $hasilDET]);

            $hasilPSN[$countPsn] = array_merge([
                'PPSN_PSNNO' => $value['PPSN1_PSNNO'],
                'PPSN_ITMCAT' => $value['PPSN2_ITMCAT'],
                'PPSN_LINENO' => $value['PPSN1_LINENO'],
                'PPSN_FR' => $value['PPSN1_FR']
            ], ['PPSN_DET' => $hasilDET]);

            if ($key !== 0 && (
                ($listFilterUser[$key - 1]['PIS2_WONO'] !== $value['PIS2_WONO']))) {
                $countJob++;
            }

            if ($key !== 0 && (
                ($listFilterUser[$key - 1]['PPSN1_PSNNO'] !== $value['PPSN1_PSNNO']) &&
                ($listFilterUser[$key - 1]['PPSN2_ITMCAT'] !== $value['PPSN2_ITMCAT']) &&
                ($listFilterUser[$key - 1]['PPSN1_LINENO'] !== $value['PPSN1_LINENO']) &&
                ($listFilterUser[$key - 1]['PPSN1_FR'] !== $value['PPSN1_FR']))) {
                $countPsn++;
            }
        }

        // return $hasilPSN;

        // $storeValueforBefore

        // Looping PPSN1 Table join dengan splscn & PIS2 Table
        // foreach ($listFilterUser as $key => $valueByPSN) {
        //     $cekBefore = array_values(array_filter($valueByPSN, function ($fScan) use ($valueFif) {
        //         return $fScan['SPLSCN_ORDERNO'] === $valueFif['SPLSCN_ORDERNO']
        //             && $fScan['PIS2_WONO'] !== $valueFif['PIS2_WONO']
        //             && isset($fScan['SISA_SCAN']);
        //     }));
        // }

        return $listFilterUser;

        $parseToFIFO = [];
        foreach ($listFilterUser as $keyVal => $valueVal) {
            $getScan = SPLSCN::select([
                'SPLSCN_DOC',
                'SPLSCN_CAT',
                'SPLSCN_LINE',
                'SPLSCN_FEDR',
                'SPLSCN_ORDERNO',
                'SPLSCN_ITMCD',
                DB::raw('SUM(SPLSCN_QTY) AS SPLSCN_QTY'),
                'PIS2_WONO',
                DB::raw('SUM(PIS2_REQQT) AS PIS2_REQQT'),
                // 'RCV_PRPRC',
                // 'RCV_DONO'
            ])
                ->where('SPLSCN_DOC', $valueVal['PPSN1_PSNNO'])
                ->where('SPLSCN_CAT', $valueVal['PPSN2_ITMCAT'])
                ->where('SPLSCN_LINE', $valueVal['PPSN1_LINENO'])
                ->where('SPLSCN_FEDR', $valueVal['PPSN1_FR'])
                ->where('SPLSCN_CAT', $valueVal['PPSN2_ITMCAT'])
                ->where('SPLSCN_SAVED', 1)
                ->join('SRVMEGA.PSI_MEGAEMS.dbo.PPSN2_TBL', function ($q1) {
                    $q1->on('SPLSCN_DOC', 'PPSN2_PSNNO');
                    $q1->on('SPLSCN_LINE', 'PPSN2_LINENO');
                    $q1->on('SPLSCN_FEDR', 'PPSN2_FR');
                    $q1->on('SPLSCN_CAT', 'PPSN2_ITMCAT');
                    $q1->on('SPLSCN_ITMCD', 'PPSN2_SUBPN');
                    $q1->on('SPLSCN_ORDERNO', 'PPSN2_MCZ');
                })
                ->join('SRVMEGA.PSI_MEGAEMS.dbo.PIS2_TBL', function ($q1) {
                    $q1->on('PPSN2_DOCNO', 'PIS2_DOCNO');
                    $q1->on('SPLSCN_LINE', 'PIS2_LINENO');
                    $q1->on('SPLSCN_FEDR', 'PIS2_FR');
                    $q1->on('PPSN2_MC', 'PIS2_MC');
                    $q1->on('PPSN2_MCZ', 'PIS2_MCZ');
                    // $q1->on('SPLSCN_ITMCD', 'PIS2_MPART');
                })
                ->orderBy('PIS2_WONO', 'asc')
                ->orderBy('SPLSCN_ORDERNO', 'asc')
                ->groupBy([
                    'SPLSCN_DOC',
                    'SPLSCN_CAT',
                    'SPLSCN_LINE',
                    'SPLSCN_FEDR',
                    'SPLSCN_ORDERNO',
                    'SPLSCN_ITMCD',
                    'PIS2_WONO',
                    // 'RCV_PRPRC',
                    // 'RCV_DONO'
                ])
                ->get()
                ->toArray();

            $getWO = ppsn1Table::select($selHeader2)
                ->where('PPSN1_PSNNO', $valueVal['PPSN1_PSNNO'])
                ->where('PPSN1_LINENO', $valueVal['PPSN1_LINENO'])
                ->where('PPSN1_FR', $valueVal['PPSN1_FR'])
                ->groupBy($selHeader2)
                ->get()
                ->toArray();

            // $listingQTYScanByWO[] = array_merge($valueWO, ['SCAN' =>
            //     array_values(array_filter($getScan, function ($q) use ($valueWO) {
            //         return $q['PIS2_WONO'] === $valueWO['PPSN1_WONO'];
            //     }, ARRAY_FILTER_USE_BOTH ))
            // ]);

            foreach ($getScan as $keyFif => $valueFif) {
                // $getScan[$keyFif]['SPLSCN_QTY'] = $valueFif['SPLSCN_QTY'] - $valueFif['PIS2_REQQT'];
                $cekBefore = array_values(array_filter($getScan, function ($fScan) use ($valueFif) {
                    return $fScan['SPLSCN_ORDERNO'] === $valueFif['SPLSCN_ORDERNO']
                        && $fScan['PIS2_WONO'] !== $valueFif['PIS2_WONO']
                        && isset($fScan['SISA_SCAN']);
                }));

                $cekAfter = [];

                // Cek apakah masih ada item sama, order sama, dan wo tidak sama di array selanjutnya
                foreach ($getScan as $keyAfter => $valueAfter) {
                    if (
                        $valueAfter['SPLSCN_ORDERNO'] && $valueFif['SPLSCN_ORDERNO']
                        && $valueAfter['PIS2_WONO'] !== $valueFif['PIS2_WONO']
                        && $valueAfter['SPLSCN_ITMCD'] == $valueFif['SPLSCN_ITMCD']
                        && $keyAfter > $keyFif
                    ) {
                        $cekAfter[] = $valueAfter;
                    }
                }
                // $getScan[$keyFif]['WOQTY_SCAN'] = (count($cekBefore) > 0 ? $cekBefore[count($cekBefore) - 1]['WOQTY_SCAN'] : $valueFif['SPLSCN_QTY']) - $valueFif['PIS2_REQQT'];

                $sisaSebelMinQty = count($cekBefore) > 0 ? $cekBefore[count($cekBefore) - 1]['SISA_SCAN'] : $valueFif['SPLSCN_QTY'];

                $lastQty = $sisaSebelMinQty - $valueFif['PIS2_REQQT'];

                $totalQty = count($cekAfter) > 0
                    ? ($lastQty > 0
                        ? $valueFif['PIS2_REQQT']
                        : ($lastQty < 0
                            ? (count($cekBefore) > 0
                                ? 0
                                : $valueFif['SPLSCN_QTY'])
                            : $lastQty))
                    : $lastQty + $valueFif['PIS2_REQQT'];

                $getlot = SPLSCN::select(['SPLSCN_ITMCD', 'SPLSCN_LOTNO'])
                    ->where('SPLSCN_ITMCD', $valueFif['SPLSCN_ITMCD'])
                    ->where('SPLSCN_DOC', $valueFif['SPLSCN_DOC'])
                    ->where('SPLSCN_CAT', $valueFif['SPLSCN_CAT'])
                    ->where('SPLSCN_LINE', $valueFif['SPLSCN_LINE'])
                    ->where('SPLSCN_FEDR', $valueFif['SPLSCN_FEDR'])
                    ->where('SPLSCN_ORDERNO', $valueFif['SPLSCN_ORDERNO'])
                    ->groupBy(['SPLSCN_ITMCD', 'SPLSCN_LOTNO'])
                    ->get()
                    ->toArray();

                foreach ($getlot as $keyLot => $valueLot) {
                    $getrcv = RCVSCN::select([
                        'RCVSCN_DONO',
                        'RCVSCN_ITMCD',
                        'RCVSCN_LOTNO',
                        'RCV_PRPRC'
                    ])
                        ->where('RCVSCN_ITMCD', $valueLot['SPLSCN_ITMCD'])
                        ->where('RCVSCN_LOTNO', $valueLot['SPLSCN_LOTNO'])
                        ->join('RCV_TBL', function ($j1) {
                            $j1->on('RCV_DONO', 'RCVSCN_DONO');
                            $j1->on('RCV_ITMCD', 'RCVSCN_ITMCD');
                        })
                        ->groupBy([
                            'RCVSCN_DONO',
                            'RCVSCN_ITMCD',
                            'RCVSCN_LOTNO',
                            'RCV_PRPRC'
                        ])
                        ->orderBy('RCVSCN_DONO', 'DESC')
                        ->get()
                        ->toArray();

                    $getScan[$keyFif]['LOT_DET'][$keyLot] = array_merge($valueLot, ['LIST_DO' => $getrcv]);
                }
                // $getrcv = RCVSCN::where('RCVSCN_ITMCD', $valueFif['SPLSCN_ITMCD']);

                $getScan[$keyFif]['PROCD'] = $valueVal['PPSN1_PROCD'];
                $getScan[$keyFif]['SISA_SCAN'] = $lastQty;
                $getScan[$keyFif]['WOTOTQTY_SCAN'] = $totalQty;
                // $getScan[$keyFif]['TEST'] = $cekAfter;
            }

            $parseToFIFO[$keyVal] = array_merge($valueVal, ['LISTWO' => $getWO, 'DATA' => $getScan]);
            // $parseToFIFO[$keyVal] = $getScan;
        }

        $dataFinal = [];
        foreach ($parseToFIFO as $keyFinalFifo => $valueFinalFifo) {
            foreach ($valueFinalFifo['DATA'] as $keyFinalFifoDet => $valueFinalFifoDet) {
                $dataFinal[] = $valueFinalFifoDet;
            }
        }

        return $dataFinal;

        $kit = collect($dataFinal);

        return $kit;

        $rows = isset($req->pagination) ? $req['pagination']['rowsPerPage'] : 5;
        $page = isset($req->pagination) ? $req['pagination']['page'] : 1;

        if ($rows == 0) {
            $cekall = $kit->count();
            return $kit->paginate($cekall);
        } else {
            return $kit->paginate($rows, $req['pagination']['rowsNumber'], $page);
        }
    }

    public function getEloquentSqlWithBindings($query)
    {
        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            $binding = addslashes($binding);
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }

    public function getAssyCode($filter = '', $pagination = null, $model = null)
    {
        if (empty($filter)) {
            return ItemMaster::take(10);
        } else {
            $item = ItemMaster::select(
                'MITM_ITMCD',
                'MITM_ITMD1',
                'MITM_MODEL',
                DB::raw('coalesce(CAST(SUM(ITH_QTY) AS INT), 0) AS ITH_QTY'),
                DB::raw("CONCAT(MITM_ITMD1, '(', MITM_ITMCD ,')') AS MODEL2")
            )->where('MITM_ITMCD', 'LIKE', base64_decode($filter) . '%')
                ->orWhere('MITM_ITMD1', 'LIKE', base64_decode($filter) . '%')
                ->join('ITH_TBL', function ($j) {
                    $j->on('MITM_ITMCD', '=', 'ITH_ITMCD')
                        ->whereRaw("LEFT([ITH_WH], 4) IN ('ARWH', 'AFWH')");
                })
                ->groupBy(
                    'MITM_ITMCD',
                    'MITM_ITMD1',
                    'MITM_MODEL'
                );

            if (empty($pagination) || $pagination === 'null') {
                if (!empty($model)) {
                    return $item->where('MITM_MODEL', (string)$model)->get();
                }

                return $item->get();
            } else {
                $request = json_decode(base64_decode($pagination));
                // return $request->rowsPerPage;
                $rows = $request->rowsPerPage;
                $page = $request->page;

                $hasilFilter = base64_decode($filter);
                $hasil = $item->get()->toArray();

                if (!empty($model)) {
                    $parseArr = array_values(array_filter($hasil, function ($f) use ($model) {
                        return $f['MITM_MODEL'] == $model;
                    }));

                    $rowsNum = collect($parseArr)->count();

                    return collect($parseArr)->paginate($rows, $rowsNum, $page);
                }

                return $item->paginate($rows, ['*'], 'page', $page);
            }
        }
    }

    public function submiterProcessMaster(Request $req)
    {
        return processMstr::updateOrCreate([
            'RPDSGN_CODE' => $req->code
        ], [
            'RPDSGN_CODE' => $req->code,
            'RPDSGN_DESC' => $req->desc
        ]);
    }

    public function getListProcess()
    {
        return processMstr::get();
    }

    public function getListMappingMaster()
    {
        return processMapMstr::get();
    }

    public function getDetailMapping(Request $r, $id)
    {
        $data = processMapDet::where('RPDSGN_MSTR_ID', $id);

        if (!empty($r->filter)) {
            $data->where('RPDSGN_ITEMCD', 'like', '%' . $r->filter . '%');
        }

        $hasil = [];
        foreach ($data->get() as $key => $value) {
            $hasil[$value['RPDSGN_ITEMCD']][$key] = $value;
        }

        return $hasil;

        return $data->paginate(15, ['*'], 'page', $r->page);
    }

    public function resubmitScrapDet($id_trans, $id = '')
    {
        $cekHist = scrapHist::where('ID_TRANS', base64_decode($id_trans));

        if (!empty($id)) {
            $cekHist->where('id', base64_decode($id));
        }

        $cekHist->restore();
        $data = clone $cekHist;
        // return $cekHist->get()->toArray();

        $checkMap = [];
        foreach ($data->get()->toArray() as $key => $value) {
            $checkMapnya = $this->getListMapProcess(
                base64_encode($value['ITEMNUM']),
                'NULL',
                base64_encode($value['DOC_NO']),
                $value['DSGN_MAP_ID']
            )[0];

            $transact = [];
            foreach ($checkMapnya['process_det'] as $keyDet => $valueDet) {
                $cek = $this->submitToJobs($value['DOC_NO'], $valueDet['RPDSGN_PRO_ID'], $value['ITEMNUM'], $value['QTY'], $value['id'], $value['ID_TRANS'], $value['DATE_OUT']);

                $transact[] = $cek;
                if ($cek['status'] === false) {
                    scrapHist::where('id', $value['id'])->delete();
                }
            }

            $checkMap[] = $transact;
        }

        return $checkMap;

        return $cekHist->get()->toArray();
    }

    public function getListMapProcess($assyCode = '', $processCode = '', $jobs = '', $id = '')
    {
        $processCode = $processCode === 'NULL' ? '' : $processCode;

        $assyCodeParse = base64_decode($assyCode);
        $processCodeParse = base64_decode($processCode);
        $processJob = base64_decode($jobs);

        $parseHasil = processMapMstr::with(['processDet' => function ($q) use ($assyCode, $processCode, $assyCodeParse, $processCodeParse, $id) {
            $q->select(
                DB::raw('MAX(id) as id'),
                'RPDSGN_PRO_ID',
                'RPDSGN_ITEMCD',
                'RPDSGN_MSTR_ID',
                DB::raw('MAX(RPDSGN_PROCD) as RPDSGN_PROCD')
            );

            if (!empty($assyCode)) {
                if (strpos($assyCodeParse, 'ASP') !== false || strpos($assyCodeParse, 'KD') !== false || strpos($assyCodeParse, 'KDASP') !== false) {
                    $newAssyCode = str_replace(['ASP', 'KD', 'KDASP'], '', $assyCodeParse);
                    $q->where('RPDSGN_ITEMCD', '=', $newAssyCode);
                } else {
                    $q->where('RPDSGN_ITEMCD', '=', $assyCodeParse);
                }
            }

            if (!empty($processCode)) {
                logger($processCodeParse);
                $q->where(function ($w) use ($processCodeParse) {
                    $w->where('RPDSGN_PROCD', trim($processCodeParse));
                    $w->orWhere('RPDSGN_PROCD', 'ALL');
                });
            }

            if (!empty($id) && is_numeric($id)) {
                $q->where('id', $id);
            }

            $q->where('RPDSGN_PROCD', '<>', '');

            $q->with('processMstr')->orderBy('RPDSGN_ITEMCD')->orderBy('RPDSGN_PRO_ID');

            $q->groupBy(
                'RPDSGN_MSTR_ID',
                'RPDSGN_ITEMCD',
                'RPDSGN_PRO_ID'
            );

            logger($q->toSql());
        }]);
        // return 'bnla';

        // return $parseHasil;

        $hasil = [];
        foreach ($parseHasil->get()->toArray() as $key => $value) {
            $hasilDet = [];
            foreach ($value['process_det'] as $keyProcess => $valueProcess) {
                $hasilDet[] = array_merge($valueProcess, [
                    'last_process_stat' => $valueProcess['RPDSGN_PRO_ID'] === $value['process_det'][count($value['process_det']) - 1]['RPDSGN_PRO_ID']
                        ? true
                        : false
                ]);
            }

            if (count($hasilDet) === 0) {
                $whereJob = empty($jobs) ? NULL : "AND PDPP_WONO = '" . $processJob . "'";

                $expID = explode('-', $id);
                $getSeq = empty($id) ? NULL : "AND MBO2_SEQNO = " . $expID[count($expID) - 1];
                $processJobs = DB::select("
                    select
                        bo.MBO2_MDLCD AS RPDSGN_ITEMCD,
                        LTRIM(RTRIM(bo.MBO2_PROCD)) AS RPDSGN_PROCD,
                        CASE WHEN bo.MBO2_PROCD = 'SMT-HW' OR bo.MBO2_PROCD = 'SMT-HWAD' OR bo.MBO2_PROCD = 'SMT-SP'
                            THEN LTRIM(RTRIM(bo.MBO2_PROCD))
                            ELSE LTRIM(RTRIM(CONCAT('#', bo.MBO2_SEQNO)))
                        END AS RPDSGN_PRO_ID,
                        CASE WHEN (
                            SELECT TOP 1 dt.DLV_CONSIGN FROM PSI_WMS.dbo.DLV_TBL dt
                            INNER JOIN PSI_WMS.dbo.SERD2_TBL st ON st.SERD2_SER = dt.DLV_SER
                            WHERE st.SERD2_JOB = wo.PDPP_WONO
                            AND dt.DLV_CONSIGN IS NOT NULL
                        ) = 'IEI'
                            THEN 'MFG1'
                            ELSE 'MFG2'
                        END
                        as TEST,
                        bo.MBO2_BOMRV,
                        wo.PDPP_WONO,
                        wo.PDPP_BOMRV
                    from XMBO2 bo
                    INNER JOIN
                        XWO wo ON wo.PDPP_MDLCD = bo.MBO2_MDLCD
                        AND wo.PDPP_BOMRV = bo.MBO2_BOMRV
                    WHERE MBO2_MDLCD = '" . $assyCodeParse . "'
                    $whereJob
                    $getSeq
                    ORDER BY bo.MBO2_SEQNO
                ");

                $processJobs = array_map(function ($value) {
                    return (array)$value;
                }, $processJobs);

                $cekDataMGF = array_filter($processJobs, function ($f) use ($value) {
                    return $f['TEST'] === $value['RPDSGN_CODE'];
                });

                // $hasilDet = $processJobs;

                if (count($cekDataMGF) > 0) {
                    $cekSMTAB = array_filter($processJobs, function ($f) {
                        return $f['RPDSGN_PROCD'] === 'SMT-AB';
                    });

                    if (count($cekSMTAB) > 0) {
                        if (count($cekSMTAB) > 1) {
                            $hasil[] = array_merge($value, ['process_det' => $this->removeProcess1($processJobs)]);
                        } else {
                            array_push($processJobs, [
                                'MBO2_BOMRV' => $cekSMTAB[0]['MBO2_BOMRV'],
                                'PDPP_BOMRV' => $cekSMTAB[0]['PDPP_BOMRV'],
                                'PDPP_WONO' => $cekSMTAB[0]['PDPP_WONO'],
                                'RPDSGN_ITEMCD' => $cekSMTAB[0]['RPDSGN_ITEMCD'],
                                'RPDSGN_PROCD' => $cekSMTAB[0]['RPDSGN_PROCD'],
                                'RPDSGN_PRO_ID' => '#2',
                                'TEST' => $cekSMTAB[0]['TEST'],
                            ]);

                            $hasil[] = array_merge($value, ['process_det' => $this->removeProcess1($processJobs)]);
                        }
                    } else {
                        $hasil[] = array_merge($value, ['process_det' => $this->removeProcess1($processJobs)]);
                    }
                }
            } else {
                $hasil[] = array_merge($value, ['process_det' => $this->removeProcess1($hasilDet)]);
            }
        }

        return $hasil;
    }

    public function removeProcess1($data)
    {
        $cekABProcess = array_filter($data, function ($f) {
            return $f['RPDSGN_PROCD'] === 'SMT-AB';
        });

        if (count($cekABProcess) > 0) {
            $hilangkanPro1 = array_values(array_filter($data, function ($f) {
                return $f['RPDSGN_PRO_ID'] !== '#1';
            }));

            return $hilangkanPro1;
        } else {
            return $data;
        }
    }

    public function storeProcessMap(Request $req)
    {
        if (empty($req->idPro)) {
            $hasil = processMapMstr::create([
                'RPDSGN_DESC' => $req->proDesc
            ]);
        } else {
            $hasil = $req->idPro;
        }

        foreach ($req->listMapping as $key => $value) {
            foreach ($value['listProcess'] as $keyDet => $valueDet) {
                if ($req->idPro !== '') {
                    processMapDet::where('RPDSGN_PRO_ID')
                        ->where('RPDSGN_ITEMCD', $value['model'])
                        ->where('RPDSGN_PROCD', $value['code'])
                        ->where('RPDSGN_MSTR_ID', isset($hasil->id) ? $hasil->id : $hasil)
                        ->delete();
                }

                processMapDet::updateOrCreate([
                    'RPDSGN_PRO_ID' => $valueDet['key'],
                    'RPDSGN_ITEMCD' => $value['model'],
                    'RPDSGN_PROCD' => $value['code'],
                    'RPDSGN_MSTR_ID' => isset($hasil->id) ? $hasil->id : $hasil
                ], [
                    'RPDSGN_PRO_ID' => $valueDet['key'],
                    'RPDSGN_ITEMCD' => $value['model'],
                    'RPDSGN_PROCD' => $valueDet['val'],
                    'RPDSGN_MSTR_ID' => isset($hasil->id) ? $hasil->id : $hasil,
                    'RPDSGN_CODE' => $value['code']
                ]);
            }
        }

        return 'success';
    }

    public function ondeleteProcessMap($id)
    {
        processMapMstr::where('id', base64_decode($id))->delete();
        processMapDet::where('RPDSGN_MSTR_ID', base64_decode($id))->delete();

        return 'success';
    }

    public function downloadTemplateMapping()
    {
        $filename = 'mappingProcess.xlsx';
        Excel::store(new mappingProcessDataExport, $filename, 'public');

        return 'storage/app/public/' . $filename;
    }

    public function importMapping(Request $req)
    {
        $nama_file = uniqid('upprocess_') . rand() . '.' . $req->file->extension();

        // return $nama_file;
        $req->file->storeAs('/public/upload_process', $nama_file);

        try {
            $importer = new mappingProcessDataImport;
            Excel::import($importer, 'storage/app/public/upload_process/' . $nama_file);
            if ($importer->data == 'item_not_found') {
                return 'Item tidak ditemukan';
            }

            return 'Upload Sukses ' . $nama_file;
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();

            foreach ($failures as $failure) {
                $failure->row(); // row that went wrong
                $failure->attribute(); // either heading key (if using heading row concern) or column index
                $failure->errors(); // Actual error messages from Laravel validator
                $failure->values(); // The values of the row that has failed.
            }

            return response()->json($failure, 422);
        }
    }

    public function importMappingNew(Request $req)
    {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load($req->file('file'));
        $worksheet = $spreadsheet->getActiveSheet();

        $highestRow = $worksheet->getHighestRow();
        $highestColumn = $worksheet->getHighestColumn();

        $rows = $worksheet->rangeToArray(
            'A10:' . $highestColumn . $highestRow,
            NULL,
            TRUE,
            FALSE
        );

        $hasil = 0;

        $process = processMstr::get();

        $hasil = processMapMstr::updateOrCreate([
            'RPDSGN_DESC' => $worksheet->getCell('C2')->getValue()
        ], [
            'RPDSGN_DESC' => $worksheet->getCell('C2')->getValue(),
            'RPDSGN_CODE' => $worksheet->getCell('K2')->getValue()
        ]);

        processMapDet::where('RPDSGN_MSTR_ID', $hasil->id)->delete();

        foreach ($rows as $keyRow => $row) {
            processMapDet::where('RPDSGN_MSTR_ID', $hasil->id)
                ->where('RPDSGN_ITEMCD', strval($row[2]))
                ->where('RPDSGN_PROCD', 'ALL')
                ->delete();

            processMapDet::where('RPDSGN_MSTR_ID', $hasil->id)
                ->where('RPDSGN_ITEMCD', strval($row[2]))
                ->where('RPDSGN_PROCD', '')
                ->delete();
            foreach ($process as $key => $value) {
                $delLama = processMapDet::where('RPDSGN_MSTR_ID', $hasil->id)
                    ->where('RPDSGN_ITEMCD', strval($row[2]))
                    ->where('RPDSGN_CODE', strval($row[3]))
                    ->where('RPDSGN_PRO_ID', $value['RPDSGN_CODE']);

                if (isset($row[$key + 3])) {
                    $delLama->where('RPDSGN_PROCD', $row[$key + 4])->delete();
                }

                if ($value['FLAG_ALL']) {
                    processMapDet::create([
                        'RPDSGN_PRO_ID' => $value['RPDSGN_CODE'],
                        'RPDSGN_ITEMCD' => strval($row[2]),
                        'RPDSGN_PROCD' => 'ALL',
                        'RPDSGN_MSTR_ID' => $hasil->id,
                        'RPDSGN_CODE' => strval($row[3])
                    ]);
                } else {
                    processMapDet::create([
                        'RPDSGN_PRO_ID' => $value['RPDSGN_CODE'],
                        'RPDSGN_ITEMCD' => strval($row[2]),
                        'RPDSGN_PROCD' => isset($row[$key + 4]) ? $row[$key + 4] : '',
                        'RPDSGN_MSTR_ID' => $hasil->id,
                        'RPDSGN_CODE' => strval($row[3])
                    ]);
                }
            }
        }
    }

    public function findListJob(Request $req)
    {
        if ($req->has('pagination')) {
            $selHeader = [
                'PIS2_WONO',
                'PIS2_MDLCD'
            ];

            $psn1WOSel = pis2Table::select(
                array_merge(
                    $selHeader,
                    [
                        DB::raw('0 AS IS_RETURN'),
                        DB::raw("(
                            SELECT COALESCE(SUM(CAST(ITH_QTY AS INT)), 0) FROM ITH_TBL it
                            WHERE it.ITH_ITMCD = PIS2_MDLCD
                            AND it.ITH_WH = 'AFWH9SC'
                        ) AS TOTAL_QTY_SCRAP"),
                        DB::raw("(
                            (SELECT CAST(SUM(PDPP_WORQT) AS INT) FROM XWO WHERE PDPP_WONO = PIS2_WONO) -
                            (SELECT CAST(COALESCE(SUM(ITH_QTY), 0) AS INT) FROM ITH_TBL WHERE ITH_DOC = PIS2_WONO AND ITH_WH = 'AFWH3') -
                            (SELECT COALESCE(SUM(QTY), 0) FROM PSI_RPCUST.dbo.RPSCRAP_HIST WHERE DOC_NO = PIS2_WONO AND deleted_at IS NULL)
                        ) AS TOTAL_QTY")
                    ]
                )
            )
                ->where('PIS2_MDLCD', $req->item)
                ->groupBy($selHeader)
                ->orderBy('PIS2_WONO', 'DESC')
                ->havingRaw(
                    "(
                        SELECT COALESCE(SUM(CAST(ITH_QTY AS INT)), 0) FROM ITH_TBL it
                        WHERE it.ITH_ITMCD = PIS2_MDLCD
                        AND it.ITH_WH = 'AFWH9SC'
                    ) > 0 OR
                    (
                        (SELECT CAST(SUM(PDPP_WORQT) AS INT) FROM XWO WHERE PDPP_WONO = PIS2_WONO) -
                        (SELECT CAST(COALESCE(SUM(ITH_QTY), 0) AS INT) FROM ITH_TBL WHERE ITH_DOC = PIS2_WONO AND ITH_WH = 'AFWH3') -
                        (SELECT COALESCE(SUM(QTY), 0) FROM PSI_RPCUST.dbo.RPSCRAP_HIST WHERE DOC_NO = PIS2_WONO AND deleted_at IS NULL)
                    ) > 0"
                );

            // return $psn1WOSel->toSql();

            if (isset($req->filter)) {
                $psn1WOSel->where('PIS2_WONO', 'LIKE', '%' . $req->filter . '%');
            }

            if (isset($req->pagination['sortBy']) && $req->pagination['sortBy'] !== 'desc') {
                $psn1WOSel->orderBy($req->pagination['sortBy'], $req->pagination['descending'] ? 'desc' : 'asc');
            }

            $rows = isset($req->pagination) ? $req->pagination['rowsPerPage'] : 5;
            $page = isset($req->pagination) ? $req->pagination['page'] : 1;

            if ($rows == 0) {
                $cekall = $psn1WOSel->count();
                return $psn1WOSel->paginate($cekall);
            } else {
                return $psn1WOSel->paginate($rows, ['*'], 'page', $page);
            }
        } else {
            $selHeader = [
                'PIS2_WONO',
                // 'PIS2_PROCD',
                'PIS2_MDLCD',
                // 'PIS2_LINENO'
            ];

            $psn1WOSel = pis2Table::select($selHeader)
                ->where('PIS2_MDLCD', $req->model['item'])
                // ->where('PIS2_PROCD', $req->process)
                ->groupBy($selHeader)
                ->orderBy('PIS2_WONO', 'DESC');

            if ($req->has('filter')) {
                return $psn1WOSel->where('PIS2_WONO', 'LIKE', '%' . $req->filter . '%')->get();
            }

            return $psn1WOSel->get();
        }
    }

    // Buat cari
    public function newfindListJob(Request $req)
    {
        if ($req->has('pagination')) {

            $searchJob = collect(DB::select(DB::raw("exec sr_sp_checkJobTotalScrapQty @model = ?, @jobSearch = ?"), [
                $req->item,
                $req->filter === 'undefined' ? '' : $req->filter
            ]));

            $rows = isset($req->pagination) ? $req->pagination['rowsPerPage'] : 5;
            $page = isset($req->pagination) ? $req->pagination['page'] : 1;
            $rowsNum = $req->has('pagination') ? $req->pagination['rowsNumber'] : $searchJob->count();

            return $searchJob->paginate($rows, $rowsNum, $page);
        } else {
            $searchJob = collect(DB::select(DB::raw("exec sr_sp_checkJobTotalScrapQty @model = '" . $req->data['item'] . "', @jobSearch = '" . $req->data['filter'] . "'")));

            return $searchJob;
        }
    }

    public function findListDO(Request $req)
    {
        if ($req->has('pagination')) {
            $selHeader = [
                'RCVSCN_DONO',
                'RCVSCN_ITMCD',
                'RCVSCN_LOTNO'
                // 'SERD2_ITMCD',
                // 'SERD2_LOTNO'
            ];

            $selHeaderSerD = [
                'SERD2_ITMCD',
                'SERD2_LOTNO'
            ];

            $DOSel = RCVSCN::select(
                array_merge(
                    $selHeader,
                    [DB::raw("CONCAT(RCVSCN_DONO, '-', RCVSCN_LOTNO) as KEY_LOT")],
                    [DB::raw('CAST(SUM(RCVSCN_QTY) AS INT) AS QTY_TOT')],
                    [DB::raw("(
                        SELECT SUM(ITH_QTY) FROM PSI_WMS.dbo.ITH_TBL IT
                        WHERE IT.ITH_ITMCD = RCVSCN_TBL.RCVSCN_ITMCD
                        AND IT.ITH_WH = 'ARWH9SC'
                    ) AS QTY")]
                    // [DB::raw('CAST(SUM(SERD2_QTY) AS INT) AS QTY_FG')]
                )
            )
                ->join('ITH_TBL', function ($j) {
                    $j->on('RCVSCN_ITMCD', '=', 'ITH_ITMCD')
                        ->where('ITH_WH', 'LIKE', 'ARWH%');
                })
                ->where('RCVSCN_DONO', '<>', '')
                ->where('RCVSCN_LOTNO', '<>', '')
                ->where('RCVSCN_ITMCD', $req->item)
                ->groupBy(
                    $selHeader
                );

            if ($req->has('filter')) {
                $DOSel->where(function ($q) use ($req) {
                    $q->where('RCVSCN_DONO', 'LIKE', '%' . $req->filter . '%')
                        ->orWhere('RCVSCN_LOTNO', 'LIKE', '%' . $req->filter . '%');
                });
            }

            if (isset($req->pagination['sortBy']) && $req->pagination['sortBy'] !== 'desc') {
                $DOSel->orderBy($req->pagination['sortBy'], $req->pagination['descending'] ? 'desc' : 'asc');
            }

            // $hasilnya = [];
            // foreach ($DOSel->get()->toArray() as $key => $value) {
            //     $getSerD = SERD2TBL::select(
            //         array_merge(
            //             $selHeaderSerD,
            //             [DB::raw('CAST(SUM(SERD2_QTY) AS INT) AS QTY_FG')]
            //         )
            //     )
            //         ->where('SERD2_ITMCD', $value['RCVSCN_ITMCD'])
            //         ->Where('SERD2_LOTNO', $value['RCVSCN_LOTNO'])
            //         ->groupBy($selHeaderSerD)
            //         ->first();

            //     $hasilnya[] = array_merge($value, ['QTY_FG' => empty($getSerD) ? 0 : $getSerD->toArray()['QTY_FG']]);
            // }

            // $hasilCollection = collect($hasilnya);

            // return $DOSel->get();

            $rows = isset($req->pagination) ? $req->pagination['rowsPerPage'] : 5;
            $page = isset($req->pagination) ? $req->pagination['page'] : 1;

            if ($rows == 0) {
                $cekall = $DOSel->count();
                return $DOSel->paginate($cekall);
            } else {
                return $DOSel->paginate($rows, ['*'], 'page', $page);
            }

            return $DOSel;
        } else {
            $selHeader = [
                'RCVSCN_DONO',
                'RCVSCN_ITMCD',
                'RCVSCN_LOTNO'
                // 'SERD2_ITMCD',
                // 'SERD2_LOTNO'
            ];

            $DOSel = RCVSCN::select(
                array_merge(
                    $selHeader,
                    [DB::raw("CONCAT(RCVSCN_DONO, '-', RCVSCN_LOTNO) as KEY_LOT")],
                    [DB::raw('CAST(SUM(RCVSCN_QTY) AS INT) AS QTY_TOT')],
                    [DB::raw("(
                        SELECT SUM(ITH_QTY) FROM PSI_WMS.dbo.ITH_TBL IT
                        WHERE IT.ITH_ITMCD = RCVSCN_TBL.RCVSCN_ITMCD
                        AND IT.ITH_WH = 'ARWH9SC'
                    ) AS QTY")]
                    // [DB::raw('CAST(SUM(SERD2_QTY) AS INT) AS QTY_FG')]
                )
            )
                ->join('ITH_TBL', function ($j) {
                    $j->on('RCVSCN_ITMCD', '=', 'ITH_ITMCD')
                        ->where('ITH_WH', 'LIKE', 'ARWH%');
                })
                ->where('RCVSCN_LOTNO', 'LIKE', '%' . $req->filter . '%')
                ->where('RCVSCN_ITMCD', $req->item)
                ->groupBy(
                    $selHeader
                );

            if ($req->has('do') && !empty($req->do)) {
                $DOSel->where('RCVSCN_DONO', 'LIKE', '%' . $req->do . '%');
            } else {
                $DOSel->where('RCVSCN_DONO', '<>', '');
            }

            return $DOSel->get();
        }
    }

    public function findListRef(Request $req)
    {
        $cekITH = ITHMaster::select(
            'ITH_ITMCD',
            DB::raw("
                CASE WHEN ITH_WH = 'ARQA1'
                    THEN ITH_DOC
                    ELSE CASE WHEN ITH_WH = 'QAFG'
                        THEN (
                            SELECT TOP 1 it2.ITH_DOC FROM PSI_WMS.dbo.ITH_TBL it2
                            WHERE it2.ITH_SER = ITH_SER
                            AND it2.ITH_ITMCD = ITH_ITMCD
                            AND it2.ITH_WH = 'AFWH3'
                        )
                        ELSE ITH_REMARK
                    END
                END AS ITH_DOC
            "),
            DB::raw("
                CASE WHEN ITH_WH = 'ARQA1'
                    THEN ITH_SER
                    ELSE CASE WHEN ITH_WH = 'QAFG'
                        THEN ITH_SER
                        ELSE ITH_DOC
                    END
                END AS ITH_SER
            "),
            'MITM_MODEL',
            DB::raw('SUM(ITH_QTY) AS ITH_QTY')
        );

        if (is_array($req->from)) {
            $cekITH->whereIn('ITH_WH', $req->from);
        } else {
            $cekITH->where('ITH_WH', $req->from);
        }

        if ($req->has('ser_defined') && $req->ser_defined) {
            $hasil = $cekITH->where(DB::raw("
                CASE WHEN ITH_WH = 'ARQA1'
                    THEN ITH_SER
                    ELSE CASE WHEN ITH_WH = 'QAFG'
                        THEN ITH_SER
                        ELSE ITH_DOC
                    END
                END"), $req->ref_no)
                ->join('MITM_TBL', 'MITM_ITMCD', 'ITH_ITMCD')
                ->groupBy(
                    'ITH_ITMCD',
                    'MITM_MODEL',
                    'ITH_DOC',
                    'ITH_SER',
                    'ITH_WH',
                    'ITH_REMARK'
                )
                ->havingRaw('SUM(ITH_QTY) > 0');

            return is_array($req->from) ? $hasil->get() : $hasil->first();
        } else {
            return $cekITH->where('ITH_SER', 'LIKE', $req->ref_no . '%')
                ->join('MITM_TBL', 'MITM_ITMCD', 'ITH_ITMCD')
                ->groupBy(
                    'ITH_ITMCD',
                    'MITM_MODEL',
                    'ITH_DOC',
                    'ITH_SER',
                    'ITH_WH',
                    'ITH_REMARK'
                )
                ->havingRaw('SUM(ITH_QTY) > 0')
                ->get()
                ->toArray();
        }
    }

    // End Buat Cari
    public function storeHist(Request $req)
    {
        set_time_limit(3600);
        $cek = scrapHist::whereBetween('created_at', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59'])->orderBy('created_at', 'desc')->withTrashed()->first();

        $idTrans = date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->ID_TRANS, -3) + 1));
        foreach ($req->form as $key => $value) {
            $hasil = [];
            foreach ($value['choosedProces'] as $keyDet => $valueDet) {
                foreach ($valueDet['PROCESS'] as $keyDetDet => $valueDetDet) {
                    if (isset($valueDetDet['scrapqty'])) {
                        $hasil[] = scrapHist::create([
                            'DSGN_MAP_ID' => $valueDetDet['id'],
                            'USERNAME' => $req->username,
                            'DEPT' => $value['dept'],
                            'QTY' => $valueDetDet['scrapqty'],
                            'ID_TRANS' => $idTrans,
                            'TYPE_TRANS' => $req->type,
                            'JOB_NO' => $value['wo'],
                            'REASON' => isset($valueDetDet['reason']) ? $valueDetDet['reason'] : '',
                            'SCR_PROCD' => $value['process'] ? $value['process'] : (substr($valueDetDet['RPDSGN_PROCD'], 0, 3) === 'SMT' ? $valueDetDet['RPDSGN_PROCD'] : $valueDetDet['RPDSGN_PRO_ID']),
                            'MDL_FLAG' => $value['model']['flag'],
                            'ITEMNUM' => $value['model']['item'],
                        ]);
                    }
                }
            }
        }

        // return $this->pdfExport($hasil);

        return $idTrans;
    }

    public function searchPSNKeyByJob($job)
    {

        $selHeader = [
            'PPSN1_MDLCD', //SPL_DOC
            'PPSN1_DOCNO', //SPL_DOC
            'PPSN1_PSNNO', //SPL_DOC
            'PPSN2_ITMCAT', //SPL_CAT
            'PPSN1_LINENO', //SPL_LINE
            'PPSN1_FR', //SPL_FR
            'PPSN1_PROCD', // SPL_CODE
            'PPSN2_SUBPN', // ITEM_CODE
            'PPSN2_MC', // ITEM_CODE
            'PPSN2_MCZ', // ITEM_CODE
            'PPSN2_QTPER', // ITEM_CODE
        ];

        $psn1WOSel = ppsn1Table::select($selHeader)
            ->join('SRVMEGA.PSI_MEGAEMS.dbo.PPSN2_TBL', function ($q1) {
                $q1->on('PPSN1_PSNNO', 'PPSN2_PSNNO');
                $q1->on('PPSN1_LINENO', 'PPSN2_LINENO');
                $q1->on('PPSN1_FR', 'PPSN2_FR');
                $q1->on('PPSN1_DOCNO', 'PPSN2_DOCNO');
                $q1->on('PPSN1_BSGRP', 'PPSN2_BSGRP');
            })
            ->where('PPSN1_WONO', $job)
            ->groupBy($selHeader)
            ->orderBy('PPSN1_PSNNO')
            ->orderBy('PPSN2_ITMCAT')
            ->orderBy('PPSN1_LINENO')
            ->orderBy('PPSN1_FR')
            ->orderBy('PPSN2_MCZ')
            ->get()
            ->toArray();

        return $psn1WOSel;
    }

    public function cekMsppStructureBOMPN($model, $item)
    {
        $cekSub = msppTable::where('MSPP_SUBPN', $item)
            ->with('parent')
            ->where('MSPP_MDLCD', $model)
            ->where('MSPP_ACTIVE', 'Y')
            ->first();

        return $cekSub;
    }

    public function cekJobItem($val, $item, $job)
    {
        // Cek job berdasarkan item PSN
        $data = pis2Table::where('PIS2_DOCNO', $val['PPSN1_DOCNO'])
            ->where('PIS2_WONO', $job)
            ->where('PIS2_LINENO', $val['PPSN1_LINENO'])
            ->where('PIS2_PROCD', $val['PPSN1_PROCD'])
            ->where('PIS2_ITMCD', $item)
            ->where('PIS2_MC', $val['PPSN2_MC'])
            ->where('PIS2_MCZ', $val['PPSN2_MCZ'])
            ->orderBy('PIS2_DOCNO')
            ->orderBy('PIS2_MCZ')
            ->first();

        // logger(json_encode(array_merge($val, ['cekpis2' => $data])));

        // Jika kosong maka cari struktur BOM nya
        if (empty($data)) {
            $cekStructure = $this->cekMsppStructureBOMPN($val['PPSN1_MDLCD'], $item);

            // logger(json_encode(array_merge($val, ['cekstructurebom' => $cekStructure])));

            if (!empty($cekStructure) && $cekStructure['parent']['MSPP_BOMPN'] !== $item) {
                // Cek job berdasarkan item BOM
                $cekJobLagi = pis2Table::where('PIS2_DOCNO', $val['PPSN1_DOCNO'])
                    ->where('PIS2_WONO', $job)
                    ->where('PIS2_LINENO', $val['PPSN1_LINENO'])
                    ->where('PIS2_PROCD', $val['PPSN1_PROCD'])
                    ->where('PIS2_ITMCD', $cekStructure['MSPP_BOMPN'])
                    ->where('PIS2_MC', $val['PPSN2_MC'])
                    ->where('PIS2_MCZ', $val['PPSN2_MCZ'])
                    ->first();

                if (empty($cekJobLagi) && !empty($cekStructure['parent'])) {
                    $cekjoblain = pis2Table::select('PIS2_WONO')
                        ->where('PIS2_DOCNO', $val['PPSN1_DOCNO'])
                        // ->where('PIS2_WONO', $job)
                        ->where('PIS2_LINENO', $val['PPSN1_LINENO'])
                        ->where('PIS2_PROCD', $val['PPSN1_PROCD'])
                        // ->where('PIS2_ITMCD', $item)
                        ->where('PIS2_MC', $val['PPSN2_MC'])
                        ->where('PIS2_MCZ', $val['PPSN2_MCZ'])
                        ->groupBy('PIS2_WONO')
                        ->get()
                        ->toArray();

                    // return $this->cekJobItem($val, $cekStructure['allParentList']['MSPP_BOMPN'], $job);

                    // return empty($cekjoblain) ? 'Item Not found on Job Simulation' : $cekjoblain;
                    return empty($this->cekJobItem($val, $cekStructure['parent']['MSPP_BOMPN'], $job))
                        ? (empty($cekjoblain)
                            ? 'Item Not found on Job Simulation'
                            : $cekjoblain)
                        : $this->cekJobItem($val, $cekStructure['parent']['MSPP_BOMPN'], $job);
                } else {
                    return $cekJobLagi;
                }
            } else {
                // return $data;
                $cekJobLagi = pis2Table::where('PIS2_DOCNO', $val['PPSN1_DOCNO'])
                    ->where('PIS2_WONO', $job)
                    ->where('PIS2_LINENO', $val['PPSN1_LINENO'])
                    ->where('PIS2_PROCD', $val['PPSN1_PROCD'])
                    // ->where('PIS2_ITMCD', $cekStructure['MSPP_BOMPN'])
                    ->where('PIS2_MC', $val['PPSN2_MC'])
                    ->where('PIS2_MCZ', $val['PPSN2_MCZ'])
                    ->first();

                return !empty($cekJobLagi) ? array_merge($cekJobLagi->toArray(), ['SUBTITUTE_PART' => true]) : [];
            }
        } else {
            return $data;
        }
    }

    public function newStoreHist(Request $req)
    {
        set_time_limit(3600);

        // Cek Last ID today
        $cek = scrapHist::whereBetween('created_at', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59'])->orderBy('created_at', 'desc')->withTrashed()->first();
        $idTrans = date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->ID_TRANS, -3) + 1));

        $hasil = [];
        foreach ($req->data as $key => $value) {
            // Jika itemnya adalah Model
            if ($value['MITM_MODEL'] === '1') {

                // Parsing job yang di pilih
                foreach ($value['SCRAP_DET'] as $keyJobDet => $valueJobDet) {
                    $job = $this->searchPSNKeyByJob($valueJobDet['PIS2_WONO']);

                    // Parsing list psn berdasarkan Job
                    $hasilDet = $this->parsingJobToDetail($job, $valueJobDet['PIS2_WONO']);

                    // logger(json_encode($hasilDet));

                    // Parsing Process Master (PROCESS_DET)
                    foreach ($valueJobDet['PROCESS_DET'] as $keyProMstr => $valueProMstr) {
                        // Parsing Process Detail
                        foreach ($valueProMstr['DET'] as $keyProDet => $valueProDet) {
                            if (intval($valueProDet['QTY']) > 0) {
                                $insertingScrap = scrapHist::create([
                                    'DSGN_MAP_ID' => $valueProDet['id'],
                                    'USERNAME' => $req->username,
                                    'DEPT' => $req->dept,
                                    'QTY' => $valueProDet['QTY'],
                                    'ID_TRANS' => $idTrans,
                                    'TYPE_TRANS' => $req->type,
                                    'DOC_NO' => $valueJobDet['PIS2_WONO'],
                                    'REASON' => isset($valueProDet['REASON']) ? $valueProDet['REASON'] : '',
                                    'SCR_PROCD' => $valueProDet['RPDSGN_PROCD'],
                                    'MDL_FLAG' => 1,
                                    'ITEMNUM' => $value['MITM_ITMCD'],
                                ]);

                                // Insert Detail Transaction
                                foreach ($hasilDet as $keyDetailModel => $valueDetailModel) {
                                    $getqtper = empty($valueDetailModel['JOB_DET']) ? $valueDetailModel['PPSN2_QTPER'] : $valueDetailModel['JOB_DET']['PIS2_QTPER'];

                                    $cekLot = $this->getLotSPLScanFetch(
                                        $valueDetailModel['PPSN1_PSNNO'],
                                        $valueDetailModel['PPSN2_ITMCAT'],
                                        $valueDetailModel['PPSN1_LINENO'],
                                        $valueDetailModel['PPSN1_FR'],
                                        $valueDetailModel['PPSN2_MCZ'],
                                        $valueDetailModel['PPSN2_SUBPN'],
                                        $valueProDet['QTY'] * floatVal($getqtper)
                                    );

                                    foreach ($cekLot as $keyLotDet => $valueLotDet) {
                                        $cekRcvScnForPrice = $this->cekLatestHarga(trim($valueDetailModel['PPSN2_SUBPN']), $valueLotDet['SPLSCN_LOTNO']);

                                        $insertDet = scrapHistDet::create([
                                            'SCR_HIST_ID' => $insertingScrap->id,
                                            'SCR_PSN' => $valueDetailModel['PPSN1_PSNNO'],
                                            'SCR_CAT' => $valueDetailModel['PPSN2_ITMCAT'],
                                            'SCR_LINE' => $valueDetailModel['PPSN1_LINENO'],
                                            'SCR_FR' => $valueDetailModel['PPSN1_FR'],
                                            'SCR_MC' => $valueDetailModel['PPSN2_MC'],
                                            'SCR_MCZ' => $valueDetailModel['PPSN2_MCZ'],
                                            'SCR_ITMCD' => trim($valueDetailModel['PPSN2_SUBPN']),
                                            'SCR_QTPER' => $getqtper,
                                            'SCR_QTY' => $valueLotDet['QTYGET'],
                                            'SCR_LOTNO' => $valueLotDet['SPLSCN_LOTNO'],
                                            'SCR_ITMPRC' => empty($cekRcvScnForPrice) ? 0 : $cekRcvScnForPrice['RCV_PRPRC']
                                        ]);

                                        // Insert scrap to stock
                                        $insertingStock = $this->sendingParamBCStock(
                                            trim($valueDetailModel['PPSN2_SUBPN']),
                                            $valueLotDet['QTYGET'],
                                            $idTrans,
                                            $valueLotDet['SPLSCN_LOTNO'],
                                            $cekRcvScnForPrice['RCV_BCTYPE'],
                                            $insertDet->id
                                        );

                                        $hasil[] = $insertingStock;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Jika itemnya adalah material
            else {
                // Parsing lot yang di pilih
                foreach ($value['SCRAP_DET'] as $keyLotDet => $valueLotDet) {
                    if ($valueLotDet['QTY'] > 0) {
                        $cekRcvScnForPrice = $this->cekLatestHarga($valueLotDet['RCVSCN_ITMCD'], $valueLotDet['SPLSCN_LOTNO'], $valueLotDet['RCVSCN_DONO']);
                        // Insert ke table Hist Master
                        $insertingScrap = scrapHist::create([
                            'DSGN_MAP_ID' => $valueLotDet['KEY_LOT'],
                            'USERNAME' => '',
                            'DEPT' => '',
                            'QTY' => intVal($valueLotDet['QTY']),
                            'ID_TRANS' => $idTrans,
                            'TYPE_TRANS' => $req->type,
                            'DOC_NO' => $valueLotDet['RCVSCN_DONO'],
                            'REASON' => isset($valueLotDet['REASON']) ? $valueLotDet['REASON'] : '',
                            'SCR_PROCD' => $valueLotDet['RCVSCN_LOTNO'],
                            'MDL_FLAG' => 0,
                            'ITEMNUM' => $valueLotDet['RCVSCN_ITMCD'],
                        ]);

                        // Insert ke table Hist Detail
                        $insertDet = scrapHistDet::create([
                            'SCR_HIST_ID' => $insertingScrap->id,
                            'SCR_PSN' => null,
                            'SCR_CAT' => null,
                            'SCR_LINE' => null,
                            'SCR_FR' => null,
                            'SCR_MC' => null,
                            'SCR_MCZ' => null,
                            'SCR_ITMCD' => $valueLotDet['RCVSCN_ITMCD'],
                            'SCR_QTPER' => 1,
                            'SCR_QTY' => intVal($valueLotDet['QTY']),
                            'SCR_LOTNO' => $valueLotDet['RCVSCN_LOTNO'],
                            'SCR_ITMPRC' => empty($cekRcvScnForPrice) ? 0 : $cekRcvScnForPrice['RCV_PRPRC']
                        ]);

                        // Insert scrap to stock
                        $insertingStock = $this->sendingParamBCStock(
                            $valueLotDet['RCVSCN_ITMCD'],
                            intVal($valueLotDet['QTY']),
                            $idTrans,
                            $valueLotDet['SPLSCN_LOTNO'],
                            $cekRcvScnForPrice['RCV_BCTYPE'],
                            $insertDet->id
                        );

                        $hasil[] = $insertingStock;
                    }
                }
            }
        }

        return [
            'ID' => $idTrans,
            'DATA' => $hasil
        ];
    }

    // public function cekLatestHarga($item, $lot, $do = '')
    // {
    //     $cekRcvScnForPriceHead = RCVSCN::select(
    //         'RCVSCN_ITMCD',
    //         'RCVSCN_DONO',
    //         'RCV_PRPRC',
    //         'RCVSCN_LUPDT',
    //         'RCV_BCTYPE'
    //     )
    //         ->where('RCVSCN_ITMCD', $item)
    //         ->where('RCVSCN_LOTNO', $lot)
    //         ->join('RCV_TBL', function ($j) {
    //             $j->on('RCV_ITMCD', 'RCVSCN_ITMCD');
    //             $j->on('RCV_DONO', 'RCVSCN_DONO');
    //         })
    //         ->groupBy(
    //             'RCVSCN_ITMCD',
    //             'RCVSCN_DONO',
    //             'RCV_PRPRC',
    //             'RCVSCN_LUPDT',
    //             'RCV_BCTYPE'
    //         )
    //         ->orderBy('RCVSCN_LUPDT', 'DESC');

    //     if (empty($do)) {
    //         $cekRcvScnForPrice = $cekRcvScnForPriceHead->first();
    //     } else {
    //         $cekRcvScnForPrice = $cekRcvScnForPriceHead->where('RCVSCN_DONO', $do)->first();
    //     }

    //     return $cekRcvScnForPrice;
    // }

    public function parsingJobToDetail($data, $job)
    {
        $hasilDet = [];
        foreach ($data as $keyPsnDet => $valuePsnDet) {
            $cekPis2 = $this->cekJobItem($valuePsnDet, $valuePsnDet['PPSN2_SUBPN'], $job);

            $hasilDet[] = array_merge($valuePsnDet, ['JOB_DET' => $cekPis2]);

            // logger(json_encode(array_merge($valuePsnDet, ['JOB_DET' => $cekPis2])));
        }

        return $hasilDet;
    }

    public function getLotSPLScanFetch($psn, $cat, $line, $fr, $orderNo, $item, $qty, $arr = [], $currentArr = [], $lot = [])
    {
        if (count($arr) === 0) {
            $ceksplscn = SPLSCN::select([
                'SPLSCN_DOC',
                'SPLSCN_CAT',
                'SPLSCN_LINE',
                'SPLSCN_FEDR',
                'SPLSCN_ORDERNO',
                'SPLSCN_ITMCD',
                'SPLSCN_LOTNO',
                'SPLSCN_ID',
                DB::raw('SUM(SPLSCN_QTY) - COALESCE(SUM(RETSCN_QTYAFT), 0) AS SPLSCN_QTY'),
                DB::raw('SUM(RETSCN_QTYAFT) AS RETURN_QTY'),
                DB::raw('SUM(SPLSCN_QTY) AS REAL_SPLSCN_QTY')
            ])
                ->leftjoin('RETSCN_TBL', function ($f) {
                    $f->on('RETSCN_ITMCD', '=', 'SPLSCN_ITMCD');
                    $f->on('RETSCN_SPLDOC', '=', 'SPLSCN_DOC');
                    $f->on('RETSCN_CAT', '=', 'SPLSCN_CAT');
                    $f->on('RETSCN_LINE', '=', 'SPLSCN_LINE');
                    $f->on('RETSCN_FEDR', '=', 'SPLSCN_FEDR');
                    $f->on('RETSCN_ORDERNO', '=', 'SPLSCN_ORDERNO');
                    $f->on('RETSCN_LOT', '=', 'SPLSCN_LOTNO');
                })
                ->where('SPLSCN_DOC', $psn)
                ->where('SPLSCN_CAT', $cat)
                ->where('SPLSCN_LINE', $line)
                ->where('SPLSCN_FEDR', $fr)
                ->where('SPLSCN_ORDERNO', $orderNo)
                ->where('SPLSCN_ITMCD', $item)
                ->groupBy(
                    [
                        'SPLSCN_DOC',
                        'SPLSCN_CAT',
                        'SPLSCN_LINE',
                        'SPLSCN_FEDR',
                        'SPLSCN_ORDERNO',
                        'SPLSCN_ITMCD',
                        'SPLSCN_LOTNO',
                        'SPLSCN_ID'
                    ]
                )
                ->orderBy('SPLSCN_ID', 'DESC')
                ->get()
                ->toArray();

            // return $ceksplscn;

            if (!empty($ceksplscn)) {
                return $this->getLotSPLScanFetch($psn, $cat, $line, $fr, $orderNo, $item, $qty, $ceksplscn, $currentArr, $lot);
            } else {
                return $arr;
            }
        } else {
            $getCurrArr = empty($currentArr) ? current($arr) : $currentArr;
            // logger($qty);
            // logger($getCurrArr);
            if (!empty($getCurrArr['SPLSCN_QTY'])) {
                $cektotal = $getCurrArr['SPLSCN_QTY'] - $qty;

                if ($cektotal <= 0) {
                    array_push($lot, array_merge($getCurrArr, ['QTYGET' => intVal($getCurrArr['SPLSCN_QTY'])]));
                    return $this->getLotSPLScanFetch($psn, $cat, $line, $fr, $orderNo, $item, $cektotal, $arr, next($getCurrArr), $lot);
                } else {
                    array_push($lot, array_merge($getCurrArr, ['QTYGET' => intVal($qty)]));
                    // $lot[] = array_merge($getCurrArr, ['QTYGET' => $qty]);

                    return $lot;
                }
            } else {
                return $lot;
            }
        }
    }

    public function pdfExport($id, $typePrint = null, $datas = false, $excel = false)
    {
        $rustart = getrusage();
        $data = [];
        $getData = scrapHist::where('ID_TRANS', base64_decode($id))
            ->with('users')
            ->with(['histDet' => function ($q) {
                $q->join('PSI_WMS.dbo.MITM_TBL', 'MITM_ITMCD', 'SCR_ITMCD');
                $q->orderBy('SCR_PSN');
                $q->orderBy('SCR_CAT');
                $q->orderBy('SCR_LINE');
                $q->orderBy('SCR_FR');
                $q->orderBy('SCR_MCZ');
                $q->orderBy('SCR_MC');
            }])
            ->orderBy('ITEMNUM')
            ->get()
            ->toArray();

        $checkDone = array_values(array_filter($getData, function ($f) {
            return $f['IS_DONE'] == 0;
        }));

        if (empty($getData)) {
            return 'not_found';
        }

        if (count($checkDone) > 0) {
            return 'on_progress';
        }

        foreach ($getData as $key => $value) {
            // logger(json_encode($this->getMappingForPrint($value['DSGN_MAP_ID'])));
            $getMap = $this->getMappingForPrint($value['DSGN_MAP_ID']);
            $getItem = ItemMaster::where('MITM_ITMCD', 'like', trim($value['ITEMNUM']) . '%')->first();
            $getharga = MutasiStok::where('RCV_ITMCD', trim($value['ITEMNUM']))->orderBy('RCV_LUPDT', 'desc')->first();
            $data[] = array_merge($value, ['design_map' => $getMap, 'item_det' => $getItem, 'harga' => $getharga]);
        }

        // return $data;

        $cekFoundScrapLocation = array_filter($data, function ($f) {
            $f['design_map'] !== null;
        });

        $filterNotEmptyDesignmap = array_values(array_filter($data, function ($f) {
            return !empty($f['design_map']);
        }));

        $cekFoundAt = [];
        foreach ($filterNotEmptyDesignmap as $keyDesign => $valueDesign) {
            $cekFoundAt[$valueDesign['design_map']['dsgMaster']['RPDSGN_CODE']][$keyDesign] = $valueDesign;
        }

        $ru = getrusage();

        // return $data;

        if (!$datas) {
            if (empty($typePrint)) {
                $pdf = SnappyPDF::loadview('EXPORT.scrapSign', [
                    'data' => $data,
                    'processmstr' => processMstr::get()->toArray(),
                    'location' => $cekFoundScrapLocation,
                    'foundLoc' => array_values($cekFoundAt),
                    'time' => $this->rutime($ru, $rustart, "utime")
                ]);

                return $pdf->download('scrap_report.pdf');
                // return view('EXPORT.scrapSign', ['data' => $valData, 'fifo' => $fifonya, 'processmstr' => processMstr::get()->toArray()]);
            } else {
                if (!$excel) {
                    $pdf = SnappyPDF::setOrientation('landscape')->loadview('EXPORT.detailScrap', [
                        'data' => $data,
                        'processmstr' => processMstr::get()->toArray()
                    ]);
                    return $pdf->download('component_scrap_report.pdf');
                } else {
                    return Excel::download(new scrapReportCompFrontExport($data), 'scrapReportComponentExport.xlsx');

                    return 'public/storage/scrapReportComponentExport.xlsx';
                }
            }
        } else {
            return $data;
        }
    }

    public function getMappingForPrint($id)
    {
        if (ctype_digit($id)) {
            $cekData = processMapDet::where('id', $id)->with(['processMstr', 'dsgMaster'])->first();
            return $cekData;
        } else {
            // logger('tidak ada mapping');
            // logger($id);
            $expID = explode('-', $id);
            $getIDSeq = $expID[count($expID) - 1];

            array_pop($expID);
            // $getAssyCode = $expID[count($expID) - 1];
            $processJob = implode('-', $expID);
            $whereJob = empty($jobs) ? NULL : "WHERE PDPP_WONO = '" . $processJob . "'";
            $getSeq = empty($id) ? NULL : "AND MBO2_SEQNO = " . $getIDSeq;
            logger("
                select
                    bo.MBO2_MDLCD AS RPDSGN_ITEMCD,
                    LTRIM(RTRIM(bo.MBO2_PROCD)) AS RPDSGN_PROCD,
                    CASE WHEN bo.MBO2_PROCD = 'SMT-HW' OR bo.MBO2_PROCD = 'SMT-HWAD' OR bo.MBO2_PROCD = 'SMT-SP'
                        THEN LTRIM(RTRIM(bo.MBO2_PROCD))
                        ELSE LTRIM(RTRIM(CONCAT('#', bo.MBO2_SEQNO)))
                    END AS RPDSGN_PRO_ID,
                    CASE WHEN (
                        SELECT TOP 1 dt.DLV_CONSIGN FROM PSI_WMS.dbo.DLV_TBL dt
                        INNER JOIN PSI_WMS.dbo.SERD2_TBL st ON st.SERD2_SER = dt.DLV_SER
                        WHERE st.SERD2_JOB = wo.PDPP_WONO
                        AND dt.DLV_CONSIGN IS NOT NULL
                    ) = 'IEI'
                        THEN 'MFG1'
                        ELSE 'MFG2'
                    END
                    as TEST,
                    bo.MBO2_BOMRV,
                    wo.PDPP_WONO,
                    wo.PDPP_BOMRV
                from XMBO2 bo
                INNER JOIN
                    XWO wo ON wo.PDPP_MDLCD = bo.MBO2_MDLCD
                    AND wo.PDPP_BOMRV = bo.MBO2_BOMRV
                $whereJob
                $getSeq
                ORDER BY bo.MBO2_SEQNO
            ");
            return [
                $processJob,
                $getIDSeq
            ];
            $processJobs = DB::select("
                        select
                            bo.MBO2_MDLCD AS RPDSGN_ITEMCD,
                            LTRIM(RTRIM(bo.MBO2_PROCD)) AS RPDSGN_PROCD,
                            CASE WHEN bo.MBO2_PROCD = 'SMT-HW' OR bo.MBO2_PROCD = 'SMT-HWAD' OR bo.MBO2_PROCD = 'SMT-SP'
                                THEN LTRIM(RTRIM(bo.MBO2_PROCD))
                                ELSE LTRIM(RTRIM(CONCAT('#', bo.MBO2_SEQNO)))
                            END AS RPDSGN_PRO_ID,
                            CASE WHEN (
                                SELECT TOP 1 dt.DLV_CONSIGN FROM PSI_WMS.dbo.DLV_TBL dt
                                INNER JOIN PSI_WMS.dbo.SERD2_TBL st ON st.SERD2_SER = dt.DLV_SER
                                WHERE st.SERD2_JOB = wo.PDPP_WONO
                                AND dt.DLV_CONSIGN IS NOT NULL
                            ) = 'IEI'
                                THEN 'MFG1'
                                ELSE 'MFG2'
                            END
                            as TEST,
                            bo.MBO2_BOMRV,
                            wo.PDPP_WONO,
                            wo.PDPP_BOMRV
                        from XMBO2 bo
                        INNER JOIN
                            XWO wo ON wo.PDPP_MDLCD = bo.MBO2_MDLCD
                            AND wo.PDPP_BOMRV = bo.MBO2_BOMRV
                        $whereJob
                        $getSeq
                        ORDER BY bo.MBO2_SEQNO
                    ");

            $processJobs = array_map(function ($value) {
                return (array)$value;
            }, $processJobs);

            return $processJob;
        }
    }

    public function getScrapByID($id)
    {
        return scrapHist::where('ID_TRANS', $id)->get()->toArray();
    }

    public function reviseScrap($id, $methods = 'doc')
    {
        if ($methods === 'doc') {
            $getScrapMaster = scrapHist::where('ID_TRANS', base64_decode($id))->get()->toArray();

            if (empty($getScrapMaster)) {
                return 'not_found';
            }

            $cancel = $this->cancelBCStock(base64_decode($id));

            $hasil = [];
            if ($cancel['status'] !== 'failed') {
                foreach ($getScrapMaster as $key => $value) {
                    scrapHist::where('id', $value['id'])->update([
                        'IS_DELETE' => 1
                    ]);

                    $cekDet = scrapHistDet::where('SCR_HIST_ID', $value['id'])->get();

                    // Cancel ITH
                    // foreach ($cekDet as $keyDet => $valueDet) {
                    //     // $cekRcvScnForPrice = $this->cekLatestHarga($valueDet['SCR_ITMCD'], $valueDet['SCR_LOTNO']);

                    //     $cekITH = ITHMaster::where('ITH_ITMCD', trim($valueDet['SCR_ITMCD']))
                    //         ->where('ITH_SER', $valueDet['id'])
                    //         ->where('ITH_USRID', 'SCR_RPT')
                    //         ->get();

                    //     foreach ($cekITH as $keyITH => $valueITH) {
                    //         $this->insertToITH(
                    //             trim($valueDet['SCR_ITMCD']),
                    //             $valueITH['ITH_DATE'],
                    //             $valueITH['ITH_FORM'],
                    //             $valueITH['ITH_DOC'],
                    //             $valueITH['ITH_QTY'] * -1,
                    //             $valueITH['ITH_WH'],
                    //             $valueDet['id'],
                    //             'SCR_REV',
                    //             'SCR_RPT'
                    //         );
                    //     }
                    // }

                    $hasil[] = scrapHistDet::where('SCR_HIST_ID', $value['id'])->delete();
                }

                scrapHist::where('ID_TRANS', base64_decode($id))->delete();
            } else {
                foreach ($getScrapMaster as $key => $value) {
                    scrapHist::where('id', $value['id'])->update([
                        'IS_DELETE' => 1
                    ]);

                    scrapHist::where('id', $value['id'])->delete();
                }
            }

            return [
                'DATA' => $hasil,
                'bcstock' => $cancel
            ];
        } else {
            $getScrapMaster = scrapHist::whereIn('id', json_decode(base64_decode($id)))->get()->toArray();
            if (empty($getScrapMaster)) {
                return 'not_found';
            }

            $hasil = [];
            foreach ($getScrapMaster as $key => $value) {
                scrapHist::where('id', $value['id'])->update([
                    'IS_DELETE' => 1
                ]);

                $cancel = $this->cancelBCStock($value['ID_TRANS'], $value['id']);

                if ($cancel['status'] !== 'failed') {
                    $cekDet = scrapHistDet::where('SCR_HIST_ID', $value['id'])->get();

                    foreach ($cekDet as $keyDet => $valueDet) {
                        $cekRcvScnForPrice = $this->cekLatestHarga($valueDet['SCR_ITMCD'], $valueDet['SCR_LOTNO']);
                        // $hasil[] = $this->sendingParamBCStock(
                        //     $valueDet['SCR_ITMCD'],
                        //     $value['DATE_OUT'],
                        //     $valueDet['SCR_LOTNO'],
                        //     round($valueDet['SCR_QTY']) * -1,
                        //     base64_decode($id),
                        //     $cekRcvScnForPrice['RCV_BCTYPE']
                        // );

                        $cekITH = ITHMaster::where('ITH_ITMCD', $valueDet['SCR_ITMCD'])
                            ->where('ITH_SER', $valueDet['id'])
                            ->where('ITH_USRID', 'SCR_RPT')
                            ->get();

                        foreach ($cekITH as $keyITH => $valueITH) {
                            $this->insertToITH(
                                trim($valueDet['SCR_ITMCD']),
                                $valueITH['ITH_DATE'],
                                $valueITH['ITH_FORM'],
                                $valueITH['ITH_DOC'],
                                $valueITH['ITH_QTY'] * -1,
                                $valueITH['ITH_WH'],
                                $valueDet['id'],
                                'SCR_REV',
                                'SCR_RPT'
                            );
                        }
                    }
                    $hasil[] = scrapHistDet::where('SCR_HIST_ID', $value['id'])->delete();
                } else {
                    scrapHist::where('id', $value['id'])->update([
                        'IS_DELETE' => 0
                    ]);
                }
            }

            scrapHist::whereIn('id', json_decode(base64_decode($id)))->delete();

            return [
                'DATA' => $hasil
            ];
        }
    }

    public function reportTransaction(Request $req)
    {
        // return $req;
        $scrap = scrapHist::select(
            DB::raw('
                (SELECT
                    COALESCE(SUM(QTY), 0) AS QTY
                FROM
                    RPSCRAP_HIST AA
                WHERE AA.ID_TRANS = RPSCRAP_HIST.ID_TRANS
                AND AA.MDL_FLAG = 1)
            as QTY_FG'),
            DB::raw('
                (SELECT
                    COALESCE(SUM(QTY), 0) AS QTY
                FROM
                    RPSCRAP_HIST AA
                WHERE AA.ID_TRANS = RPSCRAP_HIST.ID_TRANS
                AND AA.MDL_FLAG = 0)
            as QTY_MTR'),
            'USERNAME',
            DB::raw('MAX(CAST(REASON AS NVARCHAR(MAX))) AS REASON'),
            'ID_TRANS',
            DB::raw('(
                SELECT COUNT(*) AS det_c
                    FROM RPSCRAP_HIST_DET
                    INNER JOIN RPSCRAP_HIST S ON SCR_HIST_ID = S.id
                    WHERE S.ID_TRANS = RPSCRAP_HIST.ID_TRANS
            ) AS det_count'),
            DB::raw('COUNT(DSGN_MAP_ID) AS process_count'),
            'MSTEMP_FNM',
            'MSTEMP_LNM'
        )
            ->leftjoin('PSI_WMS.dbo.MSTEMP_TBL', 'MSTEMP_ID', 'RPSCRAP_HIST.USERNAME')
            ->orderBy('ID_TRANS');

        if ($req->has('filter') && !empty($req->filter)) {
            $scrap->where('ID_TRANS', 'like', '%' . $req->filter . '%');
        }

        // if ($req->has('user') && !empty($req->user) && $req->user !== 'ane') {
        //     $scrap->where('username', $req->user);
        // }

        $sc = $scrap
            ->groupBy(
                'ID_TRANS',
                'MSTEMP_FNM',
                'MSTEMP_LNM',
                'USERNAME'
            )
            ->havingRaw('(
                SELECT COUNT(*) AS det_c
                    FROM RPSCRAP_HIST_DET
                    INNER JOIN RPSCRAP_HIST S ON SCR_HIST_ID = S.id
                    WHERE S.ID_TRANS = RPSCRAP_HIST.ID_TRANS
            ) > 0');

        $hasil = $sc->paginate($req->rowsPerPage === 0 ? $sc->count() : $req->rowsPerPage, ['*'], 'page', $req->page);

        $hasil->getCollection()->transform(function ($value) {
            $setReq = new Request([
                'rowsPerPage' => 10,
                'page' => 1
            ]);

            $dataDet = $this->reportTransactionDetail($setReq, base64_encode($value['ID_TRANS']));

            return array_merge(
                $value->toArray(),
                ['DET' => $dataDet]
            );
        });

        $custom = collect(['loading' => false]);

        return $custom->merge($hasil);
    }

    public function reportTransactionDetail(Request $req, $id)
    {
        $scrapHist = scrapHist::where('ID_TRANS', base64_decode($id));

        if (!empty($req->filter)) {
            $scrapHist = $scrapHist
                ->where('ITEMNUM', 'like', '%' . $req->filter . '%');
        }

        $hasil = $scrapHist->withTrashed()->paginate($req->rowsPerPage === 0 ? $scrapHist->count() : $req->rowsPerPage, ['*'], 'page', $req->page);

        $custom = collect(['filter' => $req->filter]);

        return $custom->merge($hasil);
    }

    public function findith(Request $req)
    {
        $select = [
            'ITH_DOC',
            'ITH_ITMCD',
            'MITM_ITMD1'
        ];
        $cekIth = ITHMaster::select(
            array_merge($select, [
                DB::raw('CAST(PDPP_WORQT AS INTEGER) AS PDPP_WORQT'),
                DB::raw('CAST(SUM(ITH_QTY) AS INTEGER) AS FGQTY'),
                DB::raw('CAST(coalesce(SUM(QTY), 0) AS INTEGER) AS SCRAPQTY'),
                DB::raw("(
                    SELECT CAST(coalesce(SUM(pt.PNDSER_QTY), 0)- coalesce(SUM(st2.SCRSER_QTY), 0) - coalesce(SUM(rt.RLSSER_QTY), 0) AS INTEGER)  FROM (
                        SELECT st.SERD2_SER FROM SERD2_TBL st
                        WHERE st.SERD2_JOB = ITH_DOC
                        GROUP BY st.SERD2_SER
                    ) st
                    INNER JOIN PNDSER_TBL pt ON st.SERD2_SER = pt.PNDSER_SER
                    LEFT JOIN SCRSER_TBL st2 ON st2.SCRSER_REFFDOC = pt.PNDSER_DOC
                        AND st2.SCRSER_SER = pt.PNDSER_SER
                    LEFT JOIN RLSSER_TBL rt ON rt.RLSSER_REFFDOC = pt.PNDSER_DOC
                        AND rt.RLSSER_REFFSER = pt.PNDSER_SER
                ) AS PENDINGQTY"),
                DB::raw('CAST(
                    coalesce(SUM(QTY), 0) +
                    SUM(ITH_QTY) AS INTEGER) +
                    (
                        SELECT coalesce(SUM(pt.PNDSER_QTY), 0) - coalesce(SUM(st2.SCRSER_QTY), 0) - coalesce(SUM(rt.RLSSER_QTY), 0) FROM (
                            SELECT st.SERD2_SER FROM SERD2_TBL st
                            WHERE st.SERD2_JOB = ITH_DOC
                            GROUP BY st.SERD2_SER
                        ) st
                        INNER JOIN PNDSER_TBL pt ON st.SERD2_SER = pt.PNDSER_SER
                        LEFT JOIN SCRSER_TBL st2 ON st2.SCRSER_REFFDOC = pt.PNDSER_DOC
                            AND st2.SCRSER_SER = pt.PNDSER_SER
                        LEFT JOIN RLSSER_TBL rt ON rt.RLSSER_REFFDOC = pt.PNDSER_DOC
                            AND rt.RLSSER_REFFSER = pt.PNDSER_SER
                    )
                AS TOTAL_CONFIRM'),
                DB::raw('CAST(PDPP_WORQT AS INTEGER) - (
                    CAST(coalesce(SUM(QTY), 0) + SUM(ITH_QTY) AS INTEGER) +
                    (
                        SELECT coalesce(SUM(pt.PNDSER_QTY), 0) - coalesce(SUM(st2.SCRSER_QTY), 0) - coalesce(SUM(rt.RLSSER_QTY), 0) FROM (
                            SELECT st.SERD2_SER FROM SERD2_TBL st
                            WHERE st.SERD2_JOB = ITH_DOC
                            GROUP BY st.SERD2_SER
                        ) st
                        INNER JOIN PNDSER_TBL pt ON st.SERD2_SER = pt.PNDSER_SER
                        LEFT JOIN SCRSER_TBL st2 ON st2.SCRSER_REFFDOC = pt.PNDSER_DOC
                            AND st2.SCRSER_SER = pt.PNDSER_SER
                        LEFT JOIN RLSSER_TBL rt ON rt.RLSSER_REFFDOC = pt.PNDSER_DOC
                            AND rt.RLSSER_REFFSER = pt.PNDSER_SER
                    )
                )
                AS SELISIH')
            ])
        )
            ->join('XWO', 'PDPP_WONO', '=', 'ITH_DOC')
            ->join('MITM_TBL', 'MITM_ITMCD', '=', 'ITH_ITMCD')
            ->leftjoin('PSI_RPCUST.dbo.RPSCRAP_HIST', 'DOC_NO', '=', 'ITH_DOC')
            ->whereBetween('ITH_DATE', [$req->fdate, $req->ldate])
            ->whereRaw("ITH_WH = 'AFWH3' AND ITH_FORM = 'INC-WH-FG'")
            ->groupBy(array_merge($select, ['PDPP_WORQT']))
            ->havingRaw('(CAST(coalesce(SUM(QTY), 0) + SUM(ITH_QTY) AS INTEGER) +
                (
                    SELECT coalesce(SUM(pt.PNDSER_QTY), 0) - coalesce(SUM(st2.SCRSER_QTY), 0) - coalesce(SUM(rt.RLSSER_QTY), 0) FROM (
                        SELECT st.SERD2_SER FROM SERD2_TBL st
                        WHERE st.SERD2_JOB = ITH_DOC
                        GROUP BY st.SERD2_SER
                    ) st
                    INNER JOIN PNDSER_TBL pt ON st.SERD2_SER = pt.PNDSER_SER
                    LEFT JOIN SCRSER_TBL st2 ON st2.SCRSER_REFFDOC = pt.PNDSER_DOC
                        AND st2.SCRSER_SER = pt.PNDSER_SER
                    LEFT JOIN RLSSER_TBL rt ON rt.RLSSER_REFFDOC = pt.PNDSER_DOC
                        AND rt.RLSSER_REFFSER = pt.PNDSER_SER
                )
            ) < PDPP_WORQT')
            // ->take(5)
            ->get()
            ->toArray();

        return ['data' => $cekIth, 'next_id' => date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->ID_TRANS, -3) + 1))];
    }

    public function confirmedLoss(Request $req)
    {
        // return $req;
        if ($req->FGQTY < 0) {
            return response()->json([
                "message" => "The given data was invalid.",
                "errors" => "Confirmed Qty less than 0!"
            ]);
        } else {
            // app();
            $cekMapProcess = $this->getListMapProcess(
                base64_encode($req->ITH_ITMCD),
                'NULL',
                base64_encode($req->ITH_DOC)
            );

            $dataParse = array_merge(
                $req->all(),
                ['MITM_ITMD1' => ItemMaster::where('MITM_ITMCD', $req->ITH_ITMCD)->first()['MITM_ITMD1']]
            );
            if (count($cekMapProcess) > 0) {

                $cekStock = $this->newfindListJob(new Request([
                    'data' => [
                        'item' => $req->ITH_ITMCD,
                        'filter' => $req->ITH_DOC
                    ]
                ]));

                if (count($cekStock) > 0) {
                    $cekProcess = processMstr::get();
                    $parseToParam = [];
                    foreach ($cekProcess as $keyParam => $valueParam) {
                        $parseToParam[] = 0;
                    }

                    $forSubmit = array_merge([
                        'assy_no' => $req->ITH_ITMCD,
                        'date' => $req->has('date') && !empty($req->date) ? date('Y-m-d', strtotime($req->date)) : date('Y-m-d'),
                        'flag' => 2,
                        'job_no' => $req->ITH_DOC,
                        'reason' => 'SCRAP_LOSS',
                        'tot_qty' => $cekStock
                    ]);
                    return [
                        'status' => true,
                        'data' => $dataParse,
                        'process' => $cekProcess,
                        'forSubmit' => $forSubmit,
                        'mapping' => $cekMapProcess,
                        'dataResult' => [],
                        'desc' => 'Stock found'
                    ];
                } else {
                    return [
                        'status' => false,
                        'data' => $dataParse,
                        'dataResult' => [],
                        'desc' => 'Stock not found !'
                    ];
                }
            } else {
                return [
                    'status' => false,
                    'data' => $dataParse,
                    'dataResult' => [],
                    'desc' => 'Mapping not found !'
                ];
            }

            return $cekMapProcess;

            $cek = scrapHist::whereBetween('created_at', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59'])->orderBy('created_at', 'desc')->withTrashed()->first();
            $idTrans = date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->ID_TRANS, -3) + 1));

            $job = $this->searchPSNKeyByJob($req->ITH_DOC);
            // return $job;
            $cekJobDetail = $this->parsingJobToDetail($job, $req->ITH_DOC);

            // return $cekJobDetail;

            $totalQty = intVal($req->PDPP_WORQT) - intVal($req->FGQTY);

            $insertingScrap = scrapHist::create([
                'DSGN_MAP_ID' => 'LOSS-' . $idTrans = date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->ID_TRANS, -3) + 1)),
                'USERNAME' => '',
                'DEPT' => $req->dept,
                'QTY' => $totalQty,
                'ID_TRANS' => $req->id,
                'TYPE_TRANS' => $req->type,
                'DOC_NO' => $req->ITH_DOC,
                'REASON' => 'LOSS',
                'SCR_PROCD' => '',
                'MDL_FLAG' => 0,
                'ITEMNUM' => $req->ITH_ITMCD,
            ]);

            $hasil = [];
            foreach ($cekJobDetail as $key => $value) {
                $getqtper = empty($valueDetailModel['JOB_DET']) ? $value['PPSN2_QTPER'] : $value['JOB_DET']['PIS2_QTPER'];
                $cekLot = $this->getLotSPLScanFetch(
                    $value['PPSN1_PSNNO'],
                    $value['PPSN2_ITMCAT'],
                    $value['PPSN1_LINENO'],
                    $value['PPSN1_FR'],
                    $value['PPSN2_MCZ'],
                    $value['PPSN2_SUBPN'],
                    $totalQty * floatVal($getqtper)
                );

                $hasil[] = array_merge($value, ['fifoscrap' => $cekLot, 'totalfifo' => count($cekLot)]);
                // Insert hasil fifo Lot
                foreach ($cekLot as $keyLot => $valueLot) {
                    $cekRcvScnForPrice = $this->cekLatestHarga(trim($value['PPSN2_SUBPN']), $valueLot['SPLSCN_LOTNO']);

                    scrapHistDet::create([
                        'SCR_HIST_ID' => $insertingScrap->id,
                        'SCR_PSN' => $value['PPSN1_PSNNO'],
                        'SCR_CAT' => $value['PPSN2_ITMCAT'],
                        'SCR_LINE' => $value['PPSN1_LINENO'],
                        'SCR_FR' => $value['PPSN1_FR'],
                        'SCR_MC' => $value['PPSN2_MC'],
                        'SCR_MCZ' => $value['PPSN2_MCZ'],
                        'SCR_ITMCD' => trim($value['PPSN2_SUBPN']),
                        'SCR_QTPER' => $getqtper,
                        'SCR_QTY' => $valueLot['QTYGET'],
                        'SCR_LOTNO' => $valueLot['SPLSCN_LOTNO'],
                        'SCR_ITMPRC' => isset($cekRcvScnForPrice) ? $cekRcvScnForPrice['RCV_PRPRC'] : 0
                    ]);
                }
            }

            return $hasil;
        }
    }

    public function rutime($ru, $rus, $index)
    {
        return ($ru["ru_$index.tv_sec"] * 1000 + intval($ru["ru_$index.tv_usec"] / 1000))
            -  ($rus["ru_$index.tv_sec"] * 1000 + intval($rus["ru_$index.tv_usec"] / 1000));
    }

    public function insertToITH($item, $date, $form, $doc, $qty, $wh, $ser, $remark, $user)
    {
        ITHMaster::insert([
            'ITH_ITMCD' => $item,
            'ITH_DATE' => $date,
            'ITH_FORM' => $form,
            'ITH_DOC' => $doc,
            'ITH_QTY' => $qty,
            'ITH_WH' => $wh,
            'ITH_LOC' => '',
            'ITH_SER' => $ser,
            'ITH_REMARK' => $remark,
            'ITH_LINE' => DB::raw('dbo.fun_ithline()'),
            'ITH_LUPDT' => date('Y-m-d H:i:s'),
            'ITH_USRID' => $user,
        ]);
    }

    public function sendingParamBCStock($item_num, $date_out = '', $lot = null, $qty = 0, $doc = null, $bc = null)
    {
        $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item_num,
            'date_out' => empty($date_out) ? date('Y-m-d') : $date_out,
            'lot' => $lot,
            'qty' => $qty,
            'doc' => $doc,
            'bc' => $bc,
            // 'scrap' => true,
            // 'remark' => $id,
            // 'revise' => $rev
        ];

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content['CURL'];
    }

    public function cancelBCStock($id, $loc = '')
    {
        if (empty($loc)) {
            $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/cancelDO/' . base64_encode($id);
        } else {
            $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/cancelDO/' . base64_encode($id) . '/' . base64_encode($loc);
        }

        $content = [];
        $guzz = new \GuzzleHttp\Client();

        $res = $guzz->request('GET', $endpoint);

        $content['CURL'] = json_decode($res->getBody(), true);

        return $content['CURL'];
    }

    public function migrationsScrap()
    {
        // $dataRM = DB::select(DB::raw("
        //     SELECT
        //         it.ITH_ITMCD,
        //         SUM(it.ITH_QTY) AS TOT_QTY,
        //         (
        //             SELECT sum(vsebda.EXBC_TOT_QTY)
        //             FROM PSI_RPCUST.dbo.v_stock_exbc_by_date_aju vsebda
        //             WHERE vsebda.RPSTOCK_ITMNUM = it.ITH_ITMCD
        //         ) AS TOT_EXBC
        //     FROM PSI_WMS.dbo.v_ith_tblc it
        //     WHERE it.ITH_WH = 'ARWH9SC'
        //     AND it.ITH_DATEC <= '2021-10-31'
        //     GROUP BY
        //         it.ITH_ITMCD
        //     HAVING
        //     SUM(it.ITH_QTY) > 0 AND
        //     SUM(it.ITH_QTY) <> (
        //         SELECT COALESCE(SUM(it2.ITH_QTY), 0) FROM PSI_WMS.dbo.v_ith_tblc it2
        //         WHERE it2.ITH_WH = 'SCRRPT'
        //         AND it2.ITH_ITMCD = it.ITH_ITMCD
        //     ) AND (
        //         SELECT sum(vsebda.EXBC_TOT_QTY)
        //         FROM PSI_RPCUST.dbo.v_stock_exbc_by_date_aju vsebda
        //         WHERE vsebda.RPSTOCK_ITMNUM = it.ITH_ITMCD
        //     ) > 0
        // "));

        $dataFG = DB::select(DB::raw("
            SELECT
                st.SERD2_ITMCD as ITH_ITMCD,
                SUM(st.SERD2_QTY) AS TOT_QTY,
                (
                    SELECT
                        sum(vsebda.EXBC_TOT_QTY)
                    FROM
                        PSI_RPCUST.dbo.v_stock_exbc_by_date_aju vsebda
                    WHERE
                        vsebda.RPSTOCK_ITMNUM = st.SERD2_ITMCD
                ) AS TOT_EXBCl
            FROM v_ith_tblc vit
            INNER JOIN SERD2_TBL st ON st.SERD2_SER = vit.ITH_SER
            WHERE vit.ITH_WH = 'AFWH9SC'
            AND vit.ITH_DATEC <= '2021-10-31'
            AND (
                SELECT
                    SUM(vit2.ITH_QTY)
                FROM v_ith_tblc vit2
                WHERE vit2.ITH_WH = vit.ITH_WH
                AND vit2.ITH_ITMCD = vit.ITH_ITMCD
                AND vit2.ITH_SER = vit.ITH_SER
            ) > 0
            GROUP BY
                vit.ITH_WH,
                st.SERD2_ITMCD
        "));

        // $dataRM = array_map(function ($value22) {
        //     return (array)$value22;
        // }, $dataRM);

        $dataFG = array_map(function ($value22) {
            return (array)$value22;
        }, $dataFG);

        // return $dataFG;

        $cek = scrapHist::whereBetween('created_at', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59'])->orderBy('created_at', 'desc')->withTrashed()->first();
        $id = date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->ID_TRANS, -3) + 1));

        $transact = [];
        foreach ($dataFG as $key => $value) {
            $insertingScrap = scrapHist::create([
                'DSGN_MAP_ID' => $id,
                'USERNAME' => 'ane',
                'DEPT' => 'PPIC',
                'QTY' => intVal($value['TOT_QTY']),
                'ID_TRANS' => $id,
                'TYPE_TRANS' => 'NORMAL',
                'DOC_NO' => 'ADJ-SCR-' . date('y/m/d'),
                'REASON' => 'ADJUSTMENT_FG',
                'SCR_PROCD' => '',
                'MDL_FLAG' => 0,
                'ITEMNUM' => $value['ITH_ITMCD'],
            ]);

            $transact[$value['ITH_ITMCD']] = $this->materialScrap($value['ITH_ITMCD'], $insertingScrap->id, intVal($value['TOT_QTY']), 'ADJ', '2021-10-31', $id);
        }

        return $transact;
    }

    public function checkItemStock($item, $qty)
    {
        return $this->checkFIFODO(base64_decode($item), $qty);
    }
}
