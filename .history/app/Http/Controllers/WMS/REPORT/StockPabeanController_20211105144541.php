<?php

namespace App\Http\Controllers\WMS\REPORT;

use App\Http\Controllers\Controller;
use App\Model\RPCUST\DetailStock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\WMS\API\RCVSCN;
use App\Model\WMS\API\InvRM;
use GuzzleHttp\Client;
use App\Jobs\WMSFinishGoodCountingJobs;
use App\Model\WMS\API\StockSMT;
use App\Model\RPCUST\StockExBC;
use App\Model\WMS\REPORT\MutasiStok;
use Illuminate\Support\Facades\Storage;
use App\Jobs\dataMigrationSMTToBC;

use App\Jobs\exBCCountingJobs;
use App\Jobs\testingSocket;

class StockPabeanController extends Controller
{
    public function sendExBCQueue(Request $r)
    {
        $sendQueue = (new exBCCountingJobs(
            $r->item_num,
            $r->tujuan,
            $r->date_out,
            $r->lot,
            $r->bc,
            $r->doc,
            $r->kontrak,
            $r->qty,
            $r->fgdo,
            $r->fgitem,
            $r->fgprice
        ));

        dispatch($sendQueue)->onQueue('exBC');

        return 'Queue Ex-BC successfully inserted';
    }

    public function testingSocket()
    {
        $sendQueue = new testingSocket;

        dispatch($sendQueue)->onQueue('test');

        return 'berhasil';
    }

    public function testingArray()
    {
        $arr = [];

        $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        foreach ($arr as $key => $value) {
            $param = [
                'item_num' => $value[1],
                'qty' => $value[2],
                'do' => $value[0],
                'adj' => false,
                'doc' => 'CLOSINGDO'
            ];

            $res = $guzz->request('POST', $endpoint, ['query' => $param]);

            $content[$key]['PARAM'] = $param;
            $content[$key]['CURL'] = json_decode($res->getBody(), true);
        }

        return $content;
    }

    public function sendingParam($item_num, $qty = 0, $do = null, $doc = null, $adj = false, $lot = null, $tujuan = null, $bc = null)
    {
        $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item_num,
            'qty' => $qty,
            'do' => $do,
            'adj' => $adj,
            'doc' => $doc,
            'lot' => $lot,
            'tujuan' => $tujuan,
            'bc' => $bc
        ];

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content;
    }

    public function sendParam2($param)
    {
        $endpoint = 'http://192.168.0.29:8081/api-report-custom/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content;
    }

    public function sendAdj($item_num, $qty)
    {
        $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item_num,
            'qty' => $qty,
            'adj' => true
        ];

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content;
    }

    public function findRCVScan($item, $lot = null, $adj = false)
    {
        if (empty($lot)) {
            $getRCVSCN = RCVSCN::select(
                'RCVSCN_DONO',
                'RCV_BCDATE',
                'RCV_ITMCD_REFF',
                'RCV_DONO_REFF',
                'RCV_KPPBC',
                'RCV_HSCD',
                'RCV_ZNOURUT',
                'RCV_PRPRC',
                'RCV_BM',
                'RCVSCN_ITMCD',
                'RCV_ZSTSRCV',
                DB::raw('(
                    SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                    FROM [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                    WHERE rb.RPSTOCK_DOC = RCVSCN_DONO
                    and rb.RPSTOCK_ITMNUM = RCVSCN_ITMCD
                    and rb.RPSTOCK_BCDATE = RCV_BCDATE
                ) as STOCK'),
                // DB::raw('COALESCE(SUM(CAST(RCVSCN_QTY as BIGINT)), 0) AS RCVSTOCK')
            )
                ->where('RCVSCN_ITMCD', $item)
                ->join('RCV_TBL', function ($j) {
                    $j->on('RCVSCN_DONO', '=', 'RCV_DONO');
                    $j->on('RCVSCN_ITMCD', '=', 'RCV_ITMCD');
                })
                // ->leftjoin('PSI_RPCUST.dbo.RPSAL_BCSTOCK', function ($j2) {
                //     $j2->on('RCVSCN_DONO', '=', 'RPSTOCK_DOC');
                //     $j2->on('RCVSCN_ITMCD', '=', 'RPSTOCK_ITMNUM');
                // })
                ->orderBy('RCV_BCDATE', $adj ? 'desc' : 'asc')
                ->groupBy(
                    'RCVSCN_DONO',
                    'RCV_BCDATE',
                    'RCV_ITMCD_REFF',
                    'RCV_DONO_REFF',
                    'RCV_KPPBC',
                    'RCV_HSCD',
                    'RCV_ZNOURUT',
                    'RCV_PRPRC',
                    'RCV_BM',
                    'RCVSCN_ITMCD',
                    'RCV_ZSTSRCV'
                );

            if (!$adj) {
                $getRCVSCN->havingRaw('(
                    SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                    FROM [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                    WHERE rb.RPSTOCK_DOC = RCVSCN_DONO
                    and rb.RPSTOCK_ITMNUM = RCVSCN_ITMCD
                    and rb.RPSTOCK_BCDATE = RCV_BCDATE
                ) > 0');
            }
        } else {
            $getRCVSCN = RCVSCN::select(
                'RCVSCN_DONO',
                'RCV_BCDATE',
                'RCV_ITMCD_REFF',
                'RCV_DONO_REFF',
                'RCV_KPPBC',
                'RCV_HSCD',
                'RCV_ZNOURUT',
                'RCV_PRPRC',
                'RCV_BM',
                'RCVSCN_LOTNO',
                'RCVSCN_ITMCD',
                'RCV_ZSTSRCV',
                // DB::raw('COALESCE(SUM(CAST(RPSTOCK_QTY as BIGINT)), 0) AS STOCK'),
                DB::raw('(
                    SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                    FROM [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                    WHERE rb.RPSTOCK_DOC = RCVSCN_DONO
                    and rb.RPSTOCK_ITMNUM = RCVSCN_ITMCD
                    and rb.RPSTOCK_BCDATE = RCV_BCDATE
                ) as STOCK'),
                // DB::raw('COALESCE(SUM(CAST(RCVSCN_QTY as BIGINT)), 0) AS RCVSTOCK')
            )
                ->where('RCVSCN_ITMCD', $item)
                ->where('RCVSCN_LOTNO', $lot)
                ->join('RCV_TBL', function ($j) {
                    $j->on('RCVSCN_DONO', '=', 'RCV_DONO');
                    $j->on('RCVSCN_ITMCD', '=', 'RCV_ITMCD');
                })
                // ->leftjoin('PSI_RPCUST.dbo.RPSAL_BCSTOCK', function ($j2) {
                //     $j2->on('RCVSCN_DONO', '=', 'RPSTOCK_DOC');
                //     $j2->on('RCVSCN_ITMCD', '=', 'RPSTOCK_ITMNUM');
                // })
                ->orderBy('RCV_BCDATE', $adj ? 'desc' : 'asc')
                ->groupBy(
                    'RCVSCN_DONO',
                    'RCV_BCDATE',
                    'RCV_ITMCD_REFF',
                    'RCV_DONO_REFF',
                    'RCV_KPPBC',
                    'RCV_HSCD',
                    'RCV_ZNOURUT',
                    'RCV_PRPRC',
                    'RCV_BM',
                    'RCVSCN_LOTNO',
                    'RCVSCN_ITMCD',
                    'RCV_ZSTSRCV'
                );

            if (!$adj) {
                $getRCVSCN->havingRaw('(
                    SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                    FROM [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                    WHERE rb.RPSTOCK_DOC = RCVSCN_DONO
                    and rb.RPSTOCK_ITMNUM = RCVSCN_ITMCD
                    and rb.RPSTOCK_BCDATE = RCV_BCDATE
                ) > 0');
            }
        }

        return $getRCVSCN;
    }

    public function testingFindRCVScan($item, $lot = null, $qty = 0)
    {
        $cekData = $this->findRCVScan($item, $lot)->count();

        $getRCVSCN = $cekData === 0
            ? clone $this->findRCVScan($item)
            : ($this->findRCVScan($item, $lot)->first()['STOCK'] < $qty
                ? clone $this->findRCVScan($item)
                : clone $this->findRCVScan($item, $lot));

        return $getRCVSCN->get();
    }

    public function arrayParsedExBC(Request $r)
    {
        ini_set('max_execution_time', 3600);
        $hasil = [];

        foreach ($r->item_num as $key => $value) {
            $req = new Request([
                'item_num' => $value,
                'tujuan' => $r->tujuan,
                'date_out' => $r->has('date_out') ? $r->date_out : '',
                'lot' => $r->has('lot') ? $r->lot[$key] : '',
                'bc' => $r->bc,
                'doc' => $r->doc,
                'kontrak' => $r->has('kontrak') || !empty($r->kontrak) ? $r->kontrak : '',
                'qty' => $r->qty[$key],
                'adj' => $r->adj,
                'isNotSaved' => $r->isNotSaved,
                'do' => $r->has('do') ? $r->do : ''
            ]);

            // $hasil[] = $this->showavailablestock($req)->original;
            $hasil[] = $this->calculateEXBC($req);
        }

        return $hasil;
    }

    public function showavailablestock(Request $r)
    {
        $select = [
            'RPSTOCK_ITMNUM',
            'RPSTOCK_BCTYPE',
            'RPSTOCK_BCNUM',
            'RPSTOCK_BCDATE',
            'RPSTOCK_NOAJU',
            'RPSTOCK_DOC',
            'RPSTOCK_LOT',
            'RCV_KPPBC',
            'RCV_HSCD',
            'RCV_ZNOURUT',
            'RCV_PRPRC',
            'RCV_BM',
            'MITM_ITMD1',
            'MITM_STKUOM'
        ];

        if (!$r->has('adj') || empty($r->adj)) {
            if (!$r->has('lot') || empty($r->lot)) {
                $getRCVSCN = $this->findRCVScan($r->item_num);
            } else {
                $cekData = $this->findRCVScan($r->item_num, $r->lot)->count();

                $getRCVSCN = $cekData === 0
                    ? clone $this->findRCVScan($r->item_num)
                    : ($this->findRCVScan($r->item_num, $r->lot)->first()['STOCK'] < $r->qty
                        ? clone $this->findRCVScan($r->item_num)
                        : clone $this->findRCVScan($r->item_num, $r->lot));
            }

            if (($r->has('tujuan') && !empty($r->tujuan)) && ($r->has('bc') && !empty($r->tujuan))) {
                if ($r->bc === '27') {
                    if ($r->tujuan == 6) {
                        $getRCVSCN->where('RCV_ZSTSRCV', '2');
                    } else {
                        $getRCVSCN->where(function ($w) {
                            $w->where('RCV_ZSTSRCV', '<>', '2');
                            $w->orWhereNull('RCV_ZSTSRCV');
                        });
                    }
                }
            }

            if ($r->bc == '41') {
                if ($r->has('kontrak') || !empty($r->kontrak)) {
                    $getRCVSCN->where('RCV_CONA', $r->kontrak);
                } else {
                    $getRCVSCN->where('RCV_CONA', '<>', 'NULL');
                }
            }

            if ($r->has('do') || !empty($r->do)) {
                $getRCVSCN->where('RCVSCN_DONO', $r->do);
            }
        } else {
            $getRCVSCN = $this->findRCVScan($r->item_num, null, true);
            if ($r->has('do') || !empty($r->do)) {
                $getRCVSCN->where('RCVSCN_DONO', $r->do);
            }

            if ($r->has('item_num') || !empty($r->item_num)) {
                $getRCVSCN->where('RCVSCN_ITMCD', $r->item_num);
            }

            if ($r->has('less_than') || !empty($r->less_than)) {
                $getRCVSCN->where('RCVSCN_LUPDT', '<', $r->less_than);
            }
        }

        $dataAwal = $getRCVSCN->get();

        // return $dataAwal;

        $test = DetailStock::select(array_merge($select, [
            DB::raw('SUM(RPSTOCK_QTY) RPSTOCK_QTY'),
            DB::raw('(
                SELECT
                SUM(rt.RCVSCN_QTY)
                FROM PSI_WMS.dbo.RCVSCN_TBL rt
                WHERE rt.RCVSCN_SAVED = 1
                AND rt.RCVSCN_ITMCD = RPSTOCK_ITMNUM
                AND rt.RCVSCN_DONO = RPSTOCK_DOC
            ) as STOCK_RCVSCN')
        ]))
            ->join('PSI_WMS.dbo.RCV_TBL', function ($f) {
                $f->on('RPSTOCK_ITMNUM', 'RCV_ITMCD');
                $f->on('RPSTOCK_DOC', 'RCV_DONO');
                $f->on('RPSTOCK_BCNUM', 'RCV_BCNO');
                $f->on('RPSTOCK_NOAJU', 'RCV_RPNO');
            })
            ->join('PSI_WMS.dbo.XMITM_V', 'MITM_ITMCD', '=', 'RPSTOCK_ITMNUM');

        $hasil = [];
        foreach ($dataAwal as $key => $value) {
            $do = !empty($value['RCV_DONO_REFF']) ? $value['RCV_DONO_REFF'] : $value['RCVSCN_DONO'];
            $item = !empty($value['RCV_ITMCD_REFF']) ? $value['RCV_ITMCD_REFF'] : $value['RCVSCN_ITMCD'];

            $hasil[] = ['do' => $do, 'item' => $item];
            if ($key === 0) {
                $test->where(function ($w) use ($do, $item) {
                    $w->where('RPSTOCK_DOC', $do);
                    $w->where('RPSTOCK_ITMNUM', $item);
                });
            } else {
                $test->orwhere(function ($w) use ($do, $item) {
                    $w->where('RPSTOCK_DOC', $do);
                    $w->where('RPSTOCK_ITMNUM', $item);
                });
            }
        }

        // if ($r->has('date_out') && !empty($r->date_out)) {
        //     $test->whereBetween('created_at', [$r->date_out.'00:00:00', $r->date_out.'23:59:59']);
        // }

        // Cek tanggal terlama dari BC
        if ($r->has('adj') && $r->adj == true) {
            $cek = $test->orderBy('RPSTOCK_BCDATE', 'DESC')
                ->orderBy('RPSTOCK_QTY', 'DESC')
                ->groupBy($select)
                ->get()
                ->toArray();
        } else {
            $cek = $test->havingRaw('SUM(RPSTOCK_QTY) > 0')
                ->orderBy('RPSTOCK_BCDATE', 'ASC')
                ->orderBy('RPSTOCK_QTY', 'DESC')
                ->groupBy($select)
                ->get()
                ->toArray();
        }

        // return $cek;

        // return $test->havingRaw('SUM(RPSTOCK_QTY) > 0')
        // ->orderBy('RPSTOCK_BCDATE', 'ASC')
        // ->orderBy('RPSTOCK_QTY', 'DESC')
        // ->groupBy($select)
        // ->toSql();

        $totQty = 0;
        if ($r->has('adj') || !empty($r->adj)) {
            foreach ($cek as $keyData => $valueData) {
                $totQty += $valueData['RPSTOCK_QTY'];
            }
        }

        if ($r->has('remark')) {
            $remark = $r->remark;
        } else {
            $remark = null;
        }

        if ($r->has('adj') && $r->adj == true) {
            $typeoutincsrc = ['OUT-ADJ', NULL];
            $locoutincsrc = ['WH-RM', NULL];
        } else {
            if ($r->has('scrap') && $r->scrap == true) {
                $typeoutincsrc = ['OUT', 'INC-SCR'];
                $locoutincsrc = ['WH-RM', 'WH-SCR-RM'];
            } else {
                // Old
                // $typeoutincsrc = ['OUT', 'INC-DO'];
                // $locoutincsrc = ['WH-RM', 'WH-DO'];

                // New
                $typeoutincsrc = ['OUT', NULL];
                $locoutincsrc = ['WH-RM', NULL];

                $remark = [$r->doc];
            }
        }

        if ($r->has('revise') && $r->revise === true) {
            $rev = true;
        } else {
            $rev = false;
        }

        // return $cek;

        // Start compiling to recursive
        if (!empty($cek) && count($hasil) > 0) {
            $qty = !$r->has('adj') || empty($r->adj) || $r->adj === false
                ? $r->qty
                : ($r->qty < 0
                    ? $r->qty
                    : $totQty - $r->qty);
            $hasil = $this->comparedata(
                $r->doc,
                $r->bc,
                $cek,
                $qty,
                $r->item_num,
                [],
                $r->lot,
                $typeoutincsrc,
                $remark,
                $locoutincsrc,
                $rev,
                $r->has('adj') || $r->adj == true,
                $r->has('date_out') && !empty($r->date_out) ? $r->date_out : date('Y-m-d')
            );

            // return $hasil;
            if (count($hasil) == 0) {
                return response()->json([
                    'status' => 'failed',
                    'data' => [],
                    'message' => 'no item found with enough stock!!'
                ]);
            }

            // return $hasil;

            //Tester

            $hasil_final = [];
            foreach ($hasil as $key => $value) {
                $hasil_final[] = [
                    'BC_TYPE' => $value['RPSTOCK_BCTYPE'],
                    'BC_NUM' => $value['RPSTOCK_BCNUM'],
                    'BC_AJU' => $value['RPSTOCK_NOAJU'],
                    'BC_DATE' => $value['RPSTOCK_BCDATE'],
                    'BC_QTY' => $value['RPSTOCK_QTY'],
                    'BC_DO' => $value['DO_ASAL'],
                    'BC_ITEM' => trim($value['RPSTOCK_ITMNUM']),
                    'QTY_SISA' => $value['QTY_SISA'],
                    'RCV_KPPBC' => $value['RCV_KPPBC'],
                    'RCV_HSCD' => $value['RCV_HSCD'],
                    'RCV_ZNOURUT' => $value['RCV_ZNOURUT'],
                    'RCV_PRPRC' => $value['RCV_PRPRC'],
                    'RCV_BM' => $value['RCV_BM'],
                    'MITM_ITMD1' => trim($value['MITM_ITMD1']),
                    'MITM_STKUOM' => trim($value['MITM_STKUOM']),
                    'OUT_BCDATE' => $value['RPSTOCK_BCDATEOUT']
                ];
            }

            return response()->json([
                'status' => 'success',
                'data' => $hasil_final
            ]);
        } else {
            return response()->json([
                'status' => 'failed',
                'data' => [],
                'message' => 'no item found on RCVSCN!!'
            ]);
        }

        return $cek;
    }

    public function comparedata($doc, $bcout, $arr, $qty, $item, $currentarr = [], $lot, $inoutarr, $remark, $loc, $rev, $adj, $dateOut)
    {
        $totalarray = $currentarr;
        $cekdatanow = current($arr);

        if (!empty($cekdatanow)) {
            $tot = $adj ? round($qty) + $cekdatanow['STOCK_RCVSCN'] : round($qty) - $cekdatanow['RPSTOCK_QTY'];
            $hasil = $adj
                ? ($qty < 0
                    ? ($qty + (int)$cekdatanow['RPSTOCK_QTY'] < 0
                        ? ((int)$cekdatanow['STOCK_RCVSCN'] > $qty * -1
                            ? $qty
                            : (int)$cekdatanow['STOCK_RCVSCN'] * -1 + (int)$cekdatanow['RPSTOCK_QTY'])
                        : $qty + (int)$cekdatanow['RPSTOCK_QTY'])
                    : ($qty > $cekdatanow['RPSTOCK_QTY'] ? $cekdatanow['RPSTOCK_QTY'] : round($qty)))
                : ($qty > $cekdatanow['RPSTOCK_QTY'] ? $cekdatanow['RPSTOCK_QTY'] : round($qty));

            // return [$hasil, $tot];
            // return [$hasil, $qty + (int)$cekdatanow['RPSTOCK_QTY'] > 0, $qty, $cekdatanow['RPSTOCK_QTY'], $cekdatanow['STOCK_RCVSCN'], $qty + (int)$cekdatanow['RPSTOCK_QTY']];

            if ($qty !== 0) {
                // Deducted stock incoming
                DetailStock::create([
                    'RPSTOCK_BCTYPE' => $cekdatanow['RPSTOCK_BCTYPE'],
                    'RPSTOCK_BCNUM' => $cekdatanow['RPSTOCK_BCNUM'],
                    'RPSTOCK_BCDATE' => $cekdatanow['RPSTOCK_BCDATE'],
                    'RPSTOCK_QTY' => $rev === true ? $hasil : $hasil * -1,
                    'RPSTOCK_DOC' => $cekdatanow['RPSTOCK_DOC'],
                    'RPSTOCK_TYPE' => $inoutarr[0],
                    'RPSTOCK_ITMNUM' => $cekdatanow['RPSTOCK_ITMNUM'],
                    'RPSTOCK_NOAJU' =>  $cekdatanow['RPSTOCK_NOAJU'],
                    'RPSTOCK_LOT' =>  $cekdatanow['RPSTOCK_LOT'],
                    'RPSTOCK_DOINC' => $cekdatanow['RPSTOCK_DOC'],
                    'RPSTOCK_REMARK' => is_array($remark) ? $remark[0] : '',
                    'RPSTOCK_LOC' => $loc[0],
                    'RPSTOCK_BCDATEOUT' => $dateOut
                ]);

                // IF not adjustment add to doc location
                if ($inoutarr[1]) {
                    DetailStock::create([
                        'RPSTOCK_BCTYPE' => $bcout,
                        'RPSTOCK_BCNUM' => $cekdatanow['RPSTOCK_BCNUM'],
                        'RPSTOCK_BCDATE' => $cekdatanow['RPSTOCK_BCDATE'],
                        'RPSTOCK_QTY' => $rev === true ? $hasil * -1 : $hasil,
                        'RPSTOCK_DOC' => $doc,
                        'RPSTOCK_TYPE' => $inoutarr[1],
                        'RPSTOCK_ITMNUM' => $cekdatanow['RPSTOCK_ITMNUM'],
                        'RPSTOCK_NOAJU' =>  $cekdatanow['RPSTOCK_NOAJU'],
                        'RPSTOCK_LOT' =>  $lot,
                        'RPSTOCK_DOINC' => $cekdatanow['RPSTOCK_DOC'],
                        'RPSTOCK_REMARK' => is_array($remark) ? $remark[1] : $remark,
                        'RPSTOCK_LOC' => $loc[0],
                        'RPSTOCK_BCDATEOUT' => $dateOut
                    ]);
                }
            }

            $arrhasil = [
                'RPSTOCK_BCTYPE' => $cekdatanow['RPSTOCK_BCTYPE'],
                'RPSTOCK_BCNUM' => $cekdatanow['RPSTOCK_BCNUM'],
                'RPSTOCK_BCDATE' => $cekdatanow['RPSTOCK_BCDATE'],
                'RPSTOCK_QTY' => $hasil,
                'RPSTOCK_DOC' => $doc,
                'RPSTOCK_TYPE' => $inoutarr[0],
                'RPSTOCK_ITMNUM' => $cekdatanow['RPSTOCK_ITMNUM'],
                'RPSTOCK_NOAJU' =>  $cekdatanow['RPSTOCK_NOAJU'],
                'RPSTOCK_LOT' =>  $lot,
                'DO_ASAL' => $cekdatanow['RPSTOCK_DOC'],
                'QTY_SISA' => $cekdatanow['RPSTOCK_QTY'] - $hasil,
                'RCV_KPPBC' => $cekdatanow['RCV_KPPBC'],
                'RCV_HSCD' => $cekdatanow['RCV_HSCD'],
                'RCV_ZNOURUT' => $cekdatanow['RCV_ZNOURUT'],
                'RCV_PRPRC' => $cekdatanow['RCV_PRPRC'],
                'RCV_BM' => $cekdatanow['RCV_BM'],
                'MITM_ITMD1' => trim($cekdatanow['MITM_ITMD1']),
                'MITM_STKUOM' => trim($cekdatanow['MITM_STKUOM']),
                'RPSTOCK_BCDATEOUT' => $dateOut
            ];

            if ($adj) {
                if ($qty < 0) {
                    next($arr);
                    return $this->comparedata($doc, $bcout, $arr, $tot, $item, count($totalarray) > 0 ? array_merge($totalarray, [$arrhasil]) : [$arrhasil], $lot, $inoutarr, $remark, $loc, $rev, $adj, $dateOut);
                } else {
                    return count($totalarray) > 0 ? array_merge($totalarray, [$arrhasil]) : [$arrhasil];
                }
            } else {
                if ($cekdatanow['RPSTOCK_QTY'] < $qty) {
                    next($arr);
                    return $this->comparedata($doc, $bcout, $arr, $tot, $item, count($totalarray) > 0 ? array_merge($totalarray, [$arrhasil]) : [$arrhasil], $lot, $inoutarr, $remark, $loc, $rev, $adj, $dateOut);
                } else {
                    return count($totalarray) > 0 ? array_merge($totalarray, [$arrhasil]) : [$arrhasil];
                }
            }
        } else {
            return $totalarray;
        }
    }

    public function cancelDoc($doOut)
    {
        // OLD
        // $cekData = DetailStock::where('RPSTOCK_DOC', base64_decode($doOut))
        //     ->where('RPSTOCK_TYPE', 'INC-DO')
        //     ->where('RPSTOCK_DOINC', '<>', '')
        //     ->get()
        //     ->toArray();

        // NEW
        $cekData = DetailStock::where('RPSTOCK_REMARK', base64_decode($doOut))
            ->where('RPSTOCK_TYPE', 'OUT')
            // ->where('RPSTOCK_DOINC', '<>', '')
            ->get()
            ->toArray();

        // return $cekData;

        if (count($cekData) === 0) {
            return response()->json([
                'status' => 'failed',
                'data' => [],
                'message' => 'No DO reference found !!'
            ]);
        } else {
            $hasil = [];
            foreach ($cekData as $key => $value) {
                $hasil[] = DetailStock::where('RPSTOCK_DOINC', $value['RPSTOCK_DOINC'])
                    ->where('RPSTOCK_ITMNUM', $value['RPSTOCK_ITMNUM'])
                    ->where('RPSTOCK_TYPE', 'OUT')
                    ->delete();

                // OLD
                $hasil[] = DetailStock::where('RPSTOCK_DOC', base64_decode($doOut))
                    ->where('RPSTOCK_BCTYPE', $value['RPSTOCK_BCTYPE'])
                    ->where('RPSTOCK_BCNUM', $value['RPSTOCK_BCNUM'])
                    ->where('RPSTOCK_LOT', $value['RPSTOCK_LOT'])
                    ->where('RPSTOCK_DOINC', $value['RPSTOCK_DOINC'])
                    ->where('RPSTOCK_TYPE', 'INC-DO')
                    ->delete();
            }

            return response()->json([
                'status' => 'success',
                'data' => $hasil,
                'message' => 'DO Stock successfully revised'
            ]);
        }
    }

    public function exBCItemAdjustment($item, $qty)
    {
        ini_set('max_execution_time', -1);
        $qtynya = base64_decode($qty);
        $getInv = InvRM::select([
            'cPartCode',
            DB::raw('SUM(cQty) AS QTY')
        ])
            // ->where('cPartCode', base64_decode($item))
            ->groupBy('cPartCode');

        $dataAwal = $getInv->get();

        $hasil = [];
        foreach ($dataAwal as $key => $value) {
            $cekStock = DetailStock::select([
                'RPSTOCK_ITMNUM',
                DB::raw('SUM(RPSTOCK_QTY) as QTY_STOCK')
            ])
                ->where('RPSTOCK_ITMNUM', $value['cPartCode'])
                ->where('RPSTOCK_TYPE', '<>', 'INC-DO')
                ->groupBy('RPSTOCK_ITMNUM')
                ->first();

            $hasil[] = [
                'item_num' => $value['cPartCode'],
                'stock_taking' => $value['QTY'],
                'stock' => $cekStock['QTY_STOCK'],
                'api' => [
                    'status' => $value['QTY'] - $cekStock['QTY_STOCK'] === 0
                        ? 'Already Balanced'
                        : ($value['QTY'] > $cekStock['QTY_STOCK']
                            ? 'Another Stock is on ICS'
                            : 'Stock has been updated'),
                    'result' => !is_null($cekStock['QTY_STOCK'])
                        ? ($value['QTY'] - $cekStock['QTY_STOCK'] !== 0
                            ? $this->sendingParam($value['cPartCode'], $value['QTY'], null, 'STOCK_TAKING_ADJ', true)
                            : 'Already Balanced')
                        : 'No Data on Stock'
                ]
            ];
        }

        return $hasil;
    }

    public function findStokItem($item)
    {
        $cekStock = DetailStock::select([
            'RPSTOCK_ITMNUM',
            'RPSTOCK_BCNUM',
            'RPSTOCK_BCTYPE',
            'RPSTOCK_BCDATE',
            'MITM_ITMD1',
            'MITM_ITMD2',
            'MITM_SPTNO',
            DB::raw('SUM(RPSTOCK_QTY) as QTY_STOCK')
        ])
            ->where('RPSTOCK_ITMNUM', 'like', '%' . $item . '%')
            ->where('RPSTOCK_TYPE', '<>', 'INC-DO')
            ->join('PSI_WMS.dbo.XMITM_V', 'MITM_ITMCD', '=', 'RPSTOCK_ITMNUM')
            ->groupBy(
                'RPSTOCK_BCNUM',
                'RPSTOCK_ITMNUM',
                'RPSTOCK_LOT',
                'RPSTOCK_BCTYPE',
                'RPSTOCK_BCDATE',
                'MITM_ITMD1',
                'MITM_ITMD2',
                'MITM_SPTNO'
            )
            ->havingRaw('SUM(RPSTOCK_QTY) > 0')
            ->orderBy('RPSTOCK_BCDATE', 'asc')
            ->get()
            ->toArray();

        $hasil = [];
        foreach ($cekStock as $key => $value) {
            $hasil[] = [
                'ITEM_NUM' => $value['RPSTOCK_ITMNUM'],
                'ITEM_DESC' => trim($value['MITM_ITMD1']) . ' ' . trim($value['MITM_ITMD2']) . '(' . trim($value['MITM_SPTNO']) . ')',
                'BC_TYPE' => $value['RPSTOCK_BCTYPE'],
                'BC_NO_PEN' => $value['RPSTOCK_BCNUM'],
                'BC_DATE' => $value['RPSTOCK_BCDATE'],
                'QTY_STOCK' => $value['QTY_STOCK']
            ];
        }

        return $hasil;

        return $cekStock;
    }

    public function prepMigration($item = null)
    {
        set_time_limit(3600);
        $dataMigrate1 = '(SELECT COALESCE(SUM(Quantity), 0) as qty from migrasi202103 where ItemCode = SERD2_TBL.SERD2_ITMCD) as STOCK_EXC';
        $dataMigrate2 = '(SELECT COALESCE(SUM(Quantity), 0) as qty from migrasiOMC202103 where ItemCode = SERD2_TBL.SERD2_ITMCD) as STOCK_EXC';
        $dataMigrate3 = '(SELECT COALESCE(SUM(Quantity), 0) as qty from migrasiOMI202103 where ItemCode = SERD2_TBL.SERD2_ITMCD) as STOCK_EXC';
        $dataMigrate4 = '(SELECT COALESCE(SUM(Quantity), 0) as qty from migrasiIEI202103 where ItemCode = SERD2_TBL.SERD2_ITMCD) as STOCK_EXC';
        $getData2 = DB::query()
            ->fromRaw("(select ITH_SER,sum(ITH_QTY) stkqty from ITH_TBL where ITH_WH='AFWH3' OR ITH_WH='ARSHP' group by ITH_SER, ITH_WH having sum(ITH_QTY)>0) vserstock")
            ->select(
                DB::raw("
                SERD2_TBL.SERD2_ITMCD,
                SUM(SERD2_QTY) AS SERD2,
                (SELECT COALESCE(SUM(Quantity), 0) as qty from migrasi202103 where ItemCode = SERD2_TBL.SERD2_ITMCD) as STOCK_EXC,
                $dataMigrate4,
                (SELECT SUM(RPSTOCK_QTY) FROM PSI_RPCUST.dbo.RPSAL_BCSTOCK where RPSTOCK_ITMNUM = SERD2_TBL.SERD2_ITMCD) AS TOT_BCSTOCK,
                SUM(SERD2_QTY) + (SELECT COALESCE(SUM(Quantity), 0) as qty from migrasi202103 where ItemCode = SERD2_TBL.SERD2_ITMCD)AS TOT_ADJ
            ")
            )
            ->leftJoin('SERD2_TBL', 'ITH_SER', 'SERD2_SER')
            ->groupBy('SERD2_TBL.SERD2_ITMCD');

        if (empty($item)) {
            $getData = $getData2->get()->toArray();
        } else {
            $getData = $getData2->where('SERD2_ITMCD', $item)->get()->toArray();
        }

        // return $getData;

        $hasil = [];
        foreach ($getData as $key => $value) {
            if ($value->SERD2 > $value->TOT_BCSTOCK) {
                $hasil[] = [
                    'status' => 'failed',
                    'msg' => 'SERD2 > STOCK BC'
                ];
            } else {
                $hasil[] = [
                    'status' => 'success',
                    'msg' => 'Stock adjusted',
                    'data' => $this->sendingParam($value->SERD2_ITMCD, $value->TOT_ADJ, null, null, true)
                ];
            }
        }

        return $hasil;
    }

    public function calculate_raw_material_resume(Request $req)
    {
        $queueInsert = (new WMSFinishGoodCountingJobs($req->inunique, $req->inunique_qty, $req->inunique_job));

        dispatch($queueInsert)->onQueue('WMSFGCounting');

        return 'Process queue success !';
    }

    public function adjustmentStockWithWMS()
    {
        set_time_limit(14400);
        $cekDataSMT = StockSMT::select(
            'ITH_ITMCD',
            DB::raw('SUM(STKQTY) AS STKQTY')
        )
            ->groupBy('ITH_ITMCD')->get()->toArray();

        foreach ($cekDataSMT as $key => $value) {
            dataMigrationSMTToBC::dispatch($value['ITH_ITMCD'], $value['STKQTY'], '2021-03-28')->onQueue('migrationsStock');
        }

        return 'Berhasil';
    }

    // New calculate ExBC
    public function calculateEXBC(Request $r)
    {
        // ini_set('memory_limit', '2G');

        // logger($r);

        if (!$r->has('adj') || empty($r->adj)) {
            if ($r->has('kontrak') && !empty($r->kontrak)) {
                if (!$r->has('lot') || empty($r->lot)) {
                    if ($r->has('usedEXBC') && $r->has('usedEXBC') == 'true') {
                        $getRCVSCN = $this->findRCVScan2WithoutLot($r->item_num, null, null, $r->kontrak, true);
                    } else {
                        $getRCVSCN = $this->findRCVScan2WithoutLot($r->item_num, null, null, $r->kontrak);
                    }
                } else {
                    if ($r->has('usedEXBC') && $r->has('usedEXBC') == 'true') {
                        $getRCVSCN = $this->findRCVScan2($r->item_num, $r->lot, null, $r->kontrak, true);
                    } else {
                        $cekData = $this->findRCVScan2($r->item_num, $r->lot, null, $r->kontrak)->count();
                        $cekHasilWithLot = $this->findRCVScan2($r->item_num, $r->lot, null, $r->kontrak)->first();

                        if (!empty($cekHasilWithLot['RCV_ITMCD_REFF'])) {
                            $getRCVSCN = $cekData === 0
                                ? clone $this->findRCVScan2($r->item_num, null, null, $r->kontrak)
                                : ($cekHasilWithLot['STOCK_REF'] < $r->qty
                                    ? clone $this->findRCVScan2($r->item_num, null, null, $r->kontrak)->orderByRaw("CASE WHEN RCVSCN_LOTNO = '" . $r->lot . "' THEN 1 ELSE 2 END")
                                    : clone $this->findRCVScan2($r->item_num, $r->lot, null, $r->kontrak));
                        } else {
                            $getRCVSCN = $cekData === 0
                                ? clone $this->findRCVScan2($r->item_num, null, null, $r->kontrak)
                                : ($cekHasilWithLot['RCVSCN_QTY'] < $r->qty
                                    ? clone $this->findRCVScan2($r->item_num, null, null, $r->kontrak)->orderByRaw("CASE WHEN RCVSCN_LOTNO = '" . $r->lot . "' THEN 1 ELSE 2 END")
                                    : clone $this->findRCVScan2($r->item_num, $r->lot, null, $r->kontrak));
                        }
                    }
                }
            } else {
                if (!$r->has('lot') || empty($r->lot)) {
                    $getRCVSCN = $this->findRCVScan2WithoutLot($r->item_num);
                } else {
                    $cekData = $this->findRCVScan2($r->item_num, $r->lot)->count();
                    $cekHasilWithLot = $this->findRCVScan2($r->item_num, $r->lot, null, $r->kontrak)->first();

                    if (!empty($cekHasilWithLot['RCV_ITMCD_REFF'])) {
                        $getRCVSCN = $cekData === 0
                            ? clone $this->findRCVScan2($r->item_num)
                            : ($this->findRCVScan2($r->item_num, $r->lot)->first()['STOCK_REF'] < $r->qty
                                ? clone $this->findRCVScan2($r->item_num)->orderByRaw("CASE WHEN RCVSCN_LOTNO = '" . $r->lot . "' THEN 1 ELSE 2 END")
                                : clone $this->findRCVScan2($r->item_num, $r->lot));
                    } else {
                        $getRCVSCN = $cekData === 0
                            ? clone $this->findRCVScan2($r->item_num)
                            : ($this->findRCVScan2($r->item_num, $r->lot)->first()['RCVSCN_QTY'] < $r->qty
                                ? clone $this->findRCVScan2($r->item_num)->orderByRaw("CASE WHEN RCVSCN_LOTNO = '" . $r->lot . "' THEN 1 ELSE 2 END")
                                : clone $this->findRCVScan2($r->item_num, $r->lot));
                    }
                }
            }

            if (($r->has('tujuan') && !empty($r->tujuan)) && ($r->has('bc') && !empty($r->bc))) {
                if ($r->bc === '27') {
                    if ($r->tujuan == 6) {
                        $getRCVSCN->where('RCV_ZSTSRCV', '2');
                    } else {
                        $getRCVSCN->where(function ($w) {
                            $w->where('RCV_ZSTSRCV', '<>', '2');
                            $w->orWhereNull('RCV_ZSTSRCV');
                        });
                    }
                }
            }

            if ($r->has('do') && !empty($r->do)) {
                $getRCVSCN->where('RCVSCN_DONO', $r->do);
            }
        } else {
            $getRCVSCN = $this->findRCVScan2WithoutLot($r->item_num, null, null, null, true);

            if ($r->has('do') && !empty($r->do)) {
                $getRCVSCN->where('RCVSCN_DONO', $r->do);
            }

            if ($r->has('less_than') && !empty($r->less_than)) {
                $getRCVSCN->where('RCV_BCDATE', '<', $r->less_than);
            }
        }

        // return  $getRCVSCN->toSql();

        $dataListDO = $getRCVSCN->orderBy('RCV_BCDATE', 'ASC')->get()->toArray();

        return $dataListDO;

        if (count($dataListDO) > 0) {
            $isSaved = ($r->has('isNotSaved') && $r->isNotSaved == 'true') ? false : true;
            $insertDO = $this->checkStockDO(
                $dataListDO,
                $r->qty,
                $r->doc,
                $r->date_out,
                [],
                $r->adj,
                false,
                $isSaved
            );

            // return $insertDO;
            $status = true;
            $data = $this->convertReturn($insertDO, $r->date_out);
            $message = 'Ex-BC Found.';
        } else {
            $status = false;
            $data = [];
            $message = 'no item found on RCVSCN!!';
        }

        return [
            'status' => $status,
            'data' => $data,
            'message' => $message
        ];
    }

    public function checkStockDO($data, $qty, $docOut, $dateOut, $currentarr = [], $adj = false, $isExceeded = false, $isSaved = true)
    {
        $cekdatanow = current($data);

        if ($adj) {
            $qtynya = 0;
            foreach ($this->findRCVScan2($cekdatanow['RCVSCN_ITMCD'])->get()->toArray() as $keyQtyTot => $valueQtyTot) {
                $qtynya = $qtynya + (int)$valueQtyTot['RCVSCN_QTY'];
            }

            $qtyInit = $qtynya - $qty;
        } else {
            $qtyInit = $qty;
        }

        if (!empty($cekdatanow['RCV_DONO_REFF'])) {
            $cekParent = $this->findRCVScan2($cekdatanow['RCV_ITMCD_REFF'], null, $cekdatanow['RCV_DONO_REFF'])->first();
            $getDOData = $cekParent ? $cekParent->toArray() : null;
        } else {
            $getDOData = $cekdatanow;
        }

        if ($cekdatanow && !empty($getDOData)) {
            $totalReqQty = $getDOData['RCVSCN_QTY'] < $qtyInit
                ? $qtyInit - $getDOData['RCVSCN_QTY']
                : 0;

            $totalStockQty = $getDOData['RCVSCN_QTY'] < $qtyInit
                ? 0
                : $getDOData['RCVSCN_QTY'] - $qtyInit;

            $totalSavedQty = $getDOData['RCVSCN_QTY'] < $qtyInit
                ? $getDOData['RCVSCN_QTY']
                : $qtyInit;

            $currentarr[] = array_merge(
                $getDOData,
                [
                    'IS_EXCEEDED' => $isExceeded,
                    'REQ_REMAIN' => $totalReqQty,
                    'STOCK_REMAIN' => $totalStockQty,
                    'SAVED_QTY' => $totalSavedQty
                ]
            );

            if ($isSaved && $totalSavedQty > 0) {
                $this->insertTransaction(
                    $getDOData,
                    $totalSavedQty * -1,
                    'OUT',
                    $docOut,
                    '',
                    $dateOut
                );
            }

            if ($getDOData['RCVSCN_QTY'] < $qtyInit) {
                next($data);
                $hasil = $this->checkStockDO($data, $totalReqQty, $docOut, $dateOut, $currentarr, false, false, $isSaved);
            } else {
                $hasil = $currentarr;
            }
        } else {
            if ($adj === false) {
                if ($qtyInit > 0) {
                    // logger('masuk sini');
                    end($data);
                    $hasil = $this->checkStockDO($data, $qtyInit, $docOut, $dateOut, $currentarr, false, true, $isSaved);
                } else {
                    $hasil = $currentarr;
                }
            } else {
                $hasil = $currentarr;
            }
        }

        return $hasil;
    }

    public function insertTransaction($data, $qty, $type, $remark, $loc, $dateOut)
    {
        return DetailStock::create([
            'RPSTOCK_BCTYPE' => $data['RCV_BCTYPE'],
            'RPSTOCK_BCNUM' => $data['RCV_BCNO'],
            'RPSTOCK_BCDATE' => $data['RCV_BCDATE'],
            'RPSTOCK_QTY' => $qty,
            'RPSTOCK_DOC' => $data['RCVSCN_DONO'],
            'RPSTOCK_TYPE' => $type,
            'RPSTOCK_ITMNUM' => $data['RCVSCN_ITMCD'],
            'RPSTOCK_NOAJU' =>  $data['RCV_RPNO'],
            'RPSTOCK_LOT' =>  $data['RCVSCN_LOTNO'],
            'RPSTOCK_DOINC' => $data['RCVSCN_DONO'],
            'RPSTOCK_REMARK' => $remark,
            'RPSTOCK_LOC' => $loc,
            'RPSTOCK_BCDATEOUT' => $dateOut
        ]);
    }

    public function findRCVScan2($item, $lot = null, $do = null, $kontrak = null, $showUsed = false)
    {
        $select = [
            'RCV_BCTYPE',
            'RCV_BCNO',
            'RCV_RPNO',
            'RCV_BCDATE',
            'RCVSCN_DONO',
            'RCVSCN_LOTNO',
            'RCV_ITMCD_REFF',
            'RCV_DONO_REFF',
            'RCVSCN_ITMCD',
            'MITM_ITMD1',
            'MITM_STKUOM',
            'MITM_SPTNO',
            'RCV_PRPRC',
            'RCV_KPPBC',
            'RCV_HSCD',
            'RCV_ZNOURUT',
            'RCV_BM',
            'RCV_ZSTSRCV',
            'RCV_CONA'
        ];

        $getRCVSCN = RCVSCN::select(
            array_merge(
                $select,
                [
                    DB::raw("
                    (
                        SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                        FROM [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE rb.RPSTOCK_DOC = RCVSCN_DONO
                        and rb.RPSTOCK_ITMNUM = RCVSCN_ITMCD
                        and rb.RPSTOCK_BCDATE = RCV_BCDATE
                        and rb.deleted_at IS NULL
                    ) as STOCK"),
                    DB::raw("
                    (
                        SELECT
                            COALESCE(SUM(RPSTOCK_QTY),
                            0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCV_DONO_REFF
                            and rb.RPSTOCK_ITMNUM = RCV_ITMCD_REFF
                            and rb.RPSTOCK_BCDATE = RCV_BCDATE
                            and rb.deleted_at IS NULL
                    ) as STOCK_REF"),
                    DB::raw("COALESCE(SUM(CAST(RCVSCN_QTY as INT)), 0)
                    + (
                        SELECT
                            COALESCE(SUM(RPSTOCK_QTY),
                            0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCVSCN_DONO
                            and rb.RPSTOCK_ITMNUM = RCVSCN_ITMCD
                            and rb.RPSTOCK_BCDATE = RCV_BCDATE
                            and ISNULL(rb.RPSTOCK_LOT, 'LOT0') = ISNULL(RCVSCN_LOTNO, 'LOT0')
                            and rb.deleted_at IS NULL
                            and rb.RPSTOCK_TYPE = 'OUT'
                    )AS RCVSCN_QTY ")
                ]
            )
        )
            ->where('RCVSCN_ITMCD', $item)
            ->join(DB::raw('(
            SELECT
                RCV_ITMCD,
                RCV_BCTYPE,
                RCV_BCNO,
                RCV_DONO,
                RCV_RPNO,
                RCV_BCDATE,
                RCV_ITMCD_REFF,
                RCV_DONO_REFF,
                MAX(RCV_KPPBC) AS RCV_KPPBC,
                MAX(RCV_HSCD) AS RCV_HSCD,
                MAX(RCV_ZNOURUT) AS RCV_ZNOURUT,
                MAX(RCV_PRPRC) AS RCV_PRPRC,
                MAX(RCV_BM) AS RCV_BM,
                MAX(RCV_ZSTSRCV) AS RCV_ZSTSRCV,
                MAX(RCV_CONA) AS RCV_CONA,
                sum(RCV_QTY) AS RCV_QTY
            FROM
                PSI_WMS.dbo.RCV_TBL
            GROUP BY
                RCV_ITMCD,
                RCV_BCTYPE,
                RCV_BCNO,
                RCV_DONO,
                RCV_RPNO,
                RCV_BCDATE,
                RCV_ITMCD_REFF,
                RCV_DONO_REFF
            ) a'), function ($j) {
                $j->on('RCVSCN_DONO', '=', 'RCV_DONO');
                $j->on('RCVSCN_ITMCD', '=', 'RCV_ITMCD');
            })
            ->join('PSI_WMS.dbo.MITM_TBL', 'MITM_ITMCD', '=', 'RCVSCN_ITMCD')
            ->groupBy($select);

        if (!$showUsed) {
            $getRCVSCN->havingRaw("
            (
                CASE
                    WHEN
                        RCV_ITMCD_REFF IS NOT NULL
                    THEN
                    (
                        SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCV_DONO_REFF
                        and rb.RPSTOCK_ITMNUM = RCV_ITMCD_REFF
                        and rb.RPSTOCK_BCDATE = RCV_BCDATE
                        and rb.deleted_at IS NULL
                    )
                    ELSE
                    (
                        SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCVSCN_DONO
                        and rb.RPSTOCK_ITMNUM = RCVSCN_ITMCD
                        and rb.RPSTOCK_BCDATE = RCV_BCDATE
                        and rb.deleted_at IS NULL
                    )
                    END
            ) > 0");
        }

        if (!empty($lot)) {
            $getRCVSCN->where('RCVSCN_LOTNO', $lot);
        }

        if (!empty($do)) {
            $getRCVSCN->where('RCVSCN_DONO', trim($do));
        }

        if (!empty($kontrak)) {
            $getRCVSCN->where('RCV_CONA', trim($kontrak));
        }

        return $getRCVSCN;
    }

    public function findRCVScan2WithoutLot($item, $lot = null, $do = null, $kontrak = null, $showUsed = false)
    {
        $select = [
            'RCV_BCTYPE',
            'RCV_BCNO',
            'RCV_RPNO',
            'RCV_BCDATE',
            'RCV_ITMCD_REFF',
            'RCV_DONO_REFF',
            'MITM_ITMD1',
            'MITM_STKUOM',
            'MITM_SPTNO'
        ];

        $getRCVSCN = MutasiStok::select(
            array_merge(
                [
                    DB::raw('MAX(RCV_KPPBC) AS RCV_KPPBC'),
                    DB::raw('MAX(RCV_HSCD) AS RCV_HSCD'),
                    DB::raw('MAX(RCV_ZNOURUT) AS RCV_ZNOURUT'),
                    DB::raw('MAX(RCV_PRPRC) AS RCV_PRPRC'),
                    DB::raw('MAX(RCV_BM) AS RCV_BM'),
                    DB::raw('MAX(RCV_ZSTSRCV) AS RCV_ZSTSRCV'),
                    DB::raw('MAX(RCV_CONA) AS RCV_CONA')
                ],
                [
                    DB::raw("
                    (
                        SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                        FROM [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE rb.RPSTOCK_DOC = RCV_DONO
                        and rb.RPSTOCK_ITMNUM = RCV_ITMCD
                        and rb.RPSTOCK_BCDATE = RCV_BCDATE
                        and rb.deleted_at IS NULL
                    ) as STOCK"),
                    DB::raw("
                    (
                        SELECT
                            COALESCE(SUM(RPSTOCK_QTY),
                            0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCV_DONO_REFF
                            and rb.RPSTOCK_ITMNUM = RCV_ITMCD_REFF
                            and rb.RPSTOCK_BCDATE = RCV_BCDATE
                            and rb.deleted_at IS NULL
                    ) as STOCK_REF"),
                    DB::raw("COALESCE(SUM(CAST(RCV_QTY as INT)), 0)
                    + (
                        SELECT
                            COALESCE(SUM(RPSTOCK_QTY),
                            0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCV_DONO
                            and rb.RPSTOCK_ITMNUM = RCV_ITMCD
                            and rb.RPSTOCK_BCDATE = RCV_BCDATE
                            --and ISNULL(rb.RPSTOCK_LOT, 'LOT0') = ISNULL(RCVSCN_LOTNO, 'LOT0')
                            and rb.deleted_at IS NULL
                            and rb.RPSTOCK_TYPE = 'OUT'
                    )AS RCVSCN_QTY ")
                ]
            ),
            $select
        ) ->where('RCV_ITMCD', $item)
        ->join('PSI_WMS.dbo.MITM_TBL', 'MITM_ITMCD', '=', 'RCV_ITMCD')
        ->groupBy(array_merge(
            [
                'RCV_DONO',
                'RCV_ITMCD'
            ],
            $select
        ));

        if (!$showUsed) {
            $getRCVSCN->havingRaw("
            (
                CASE
                    WHEN
                        RCV_ITMCD_REFF IS NOT NULL
                    THEN
                    (
                        SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCV_DONO_REFF
                        and rb.RPSTOCK_ITMNUM = RCV_ITMCD_REFF
                        and rb.RPSTOCK_BCDATE = RCV_BCDATE
                        and rb.deleted_at IS NULL
                    )
                    ELSE
                    (
                        SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCV_DONO
                        and rb.RPSTOCK_ITMNUM = RCV_ITMCD
                        and rb.RPSTOCK_BCDATE = RCV_BCDATE
                        and rb.deleted_at IS NULL
                    )
                    END
            ) > 0");
        }

        if (!empty($do)) {
            $getRCVSCN->where('RCV_DONO', trim($do));
        }

        if (!empty($kontrak)) {
            $getRCVSCN->where('RCV_CONA', trim($kontrak));
        }

        return $getRCVSCN;
    }

    public function convertReturn($dataInc, $dateOut)
    {
        // return count($dataInc);
        if (count($dataInc) > 0) {
            $data = [];
            foreach ($dataInc as $key => $value) {
                if (isset($value['RCV_BCTYPE'])) {
                    $data[] = [
                        "BC_TYPE" => $value['RCV_BCTYPE'],
                        "BC_NUM" => $value['RCV_BCNO'],
                        "BC_AJU" => $value['RCV_RPNO'],
                        "BC_DATE" => $value['RCV_BCDATE'],
                        "BC_QTY" => $value['SAVED_QTY'],
                        "BC_DO" => $value['RCVSCN_DONO'],
                        "BC_ITEM" => $value['RCVSCN_ITMCD'],
                        "QTY_SISA" => $value['STOCK_REMAIN'],
                        "RCV_KPPBC" => $value['RCV_KPPBC'],
                        "RCV_HSCD" => $value['RCV_HSCD'],
                        "RCV_ZNOURUT" => $value['RCV_ZNOURUT'],
                        "RCV_ZSTSRCV" => $value['RCV_ZSTSRCV'],
                        "RCV_PRPRC" => $value['RCV_PRPRC'],
                        "RCV_BM" => $value['RCV_BM'],
                        "MITM_ITMD1" => trim($value['MITM_ITMD1']),
                        "MITM_STKUOM" => trim($value['MITM_STKUOM']),
                        "MITM_SPTNO" => trim($value['MITM_SPTNO']),
                        "OUT_BCDATE" => $dateOut,
                        "LOT" => $value['RCVSCN_LOTNO'],
                        "STOCK" => $value['STOCK'],
                        "REQ_REMAIN" => $value['REQ_REMAIN'],
                        "STOCK_REMAIN" => $value['STOCK_REMAIN'],
                        "SAVED_QTY" => $value['SAVED_QTY'],
                        "IS_EXCEEDED" => $value['IS_EXCEEDED']
                    ];
                }
            }

            return $data;
        } else {
            return [];
        }
    }


    public function spCalculationEXBC(Request $r)
    {
        $query = "DECLARE @tempTableEXBC table (
            BC_TYPE varchar(10),
            BC_NUM varchar(20),
            BC_AJU varchar(50),
            BC_DATE DATE,
            BC_QTY INT,
            BC_DO varchar(100),
            BC_ITEM varchar(100),
            RCV_KPPBC varchar(50),
            RCV_HSCD varchar(50),
            RCV_ZNOURUT varchar(50),
            RCV_PRPRC varchar(50),
            RCV_BM varchar(50),
            OUT_BCDATE DATE,
            LOT varchar(50)
        );";

        // if (is_array($r->item_num)) {
        //     foreach ($r->item_num as $key => $value) {
        //         $query .= $this->setupQuery($value);
        //     }
        // }
    }

    public function setupQuery($item, $doc, $dateOut, $qty)
    {
        return "
            insert into @tempTableEXBC
            EXEC PSI_RPCUST.dbo.sp_calculation_exbc
                @item_num = '".$item."',
                @doc_out = '".$doc."',
                @date_out = '".$dateOut."',
                @qty = ".$qty.";
        ";
    }
}
