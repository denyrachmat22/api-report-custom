<?php

namespace App\Http\Controllers\WMS\REPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\RPCUST\SaldoWIP;
use App\Helpers\CustomFunctionHelper;
use Illuminate\Support\Facades\DB;
use App\Exports\MutasiWIPExport;
use Excel;
use App\Model\MASTER\ITHMaster;

class MutasiWIPController extends Controller
{
    public function show(Request $request, $methode = null)
    {
        // $filterData = $this->data($request->form, $methode);

        logger($request->header('userinput'));
        $data = $this->data($request->form, $methode, $request->header('userinput'));

        $hasil = [];
        foreach ($data->get()->toArray() as $key => $value) {
            $hasil[] = [
                'no' => $key + 1,
                'RPWIP_ITMCOD' => $value['RPWIP_ITMCOD'],
                'MITM_ITMD1' => $value['MITM_ITMD1'],
                'MITM_STKUOM' => $value['MITM_STKUOM'],
                'QTY' => number_format(intval($value['QTY'])),
            ];
        }

        // return $data;

        $filterData = collect($hasil);

        $rows = $request->has('pagination') ? $request->pagination['rowsPerPage'] : 20;
        $page = $request->has('pagination') ? $request->pagination['page'] : 1;
        $rowsNum = $request->has('pagination') ? $request->pagination['rowsNumber'] : $filterData->count();

        if ($methode == 'table') {

            // file_put_contents('logs.txt', $filterData->toSql().json_encode($filterData->getBindings()).PHP_EOL , FILE_APPEND | LOCK_EX);
            if ($rows == 0) {
                $cekall = $filterData->count();
                return $filterData->paginate($cekall);
                return $filterData->paginate($cekall, ['*'], 'page', $page);
            } else {
                return $filterData->paginate($rows, $rowsNum, $page);
                return $filterData->paginate($rows, ['*'], 'page', $page);
            }
        } elseif ($methode == 'data') {
            return $filterData->get();
        } else {
            return $this->ExportToExcell($request);
        }
    }

    public function data($filteropt, $methode = null, $username = '')
    {
        // $select = [
        //     // 'id',
        //     // 'RPWIP_DATEIS',
        //     'RPWIP_ITMCOD',
        //     'MITM_ITMD1',
        //     'MITM_STKUOM'
        // ];

        // $getData = SaldoWIP::select(
        //         array_merge(
        //             $select,
        //             [DB::raw(
        //                 "COALESCE(SUM(
        //                     CASE WHEN RPWIP_TYPE = 'INC' THEN
        //                         RPWIP_QTYTOT
        //                     ELSE
        //                         RPWIP_QTYTOT * -1
        //                     END
        //                 ), 0) as QTY"
        //             )]
        //         )
        //     )
        //     ->join('PSI_WMS.dbo.MITM_TBL', 'MITM_ITMCD', '=', 'RPWIP_ITMCOD')
        //     ->groupBy($select);

        // Less than 0
        $getData = ITHMaster::select(
            DB::raw('ITH_ITMCD as RPWIP_ITMCOD'),
            'MITM_ITMD1',
            'MITM_STKUOM',
            !empty($username) && $username === 'smt'
            ? DB::raw('SUM(ITH_QTY) as QTY')
            : DB::raw('CASE WHEN SUM(ITH_QTY) < 0
            THEN 0
            ELSE SUM(ITH_QTY) END as QTY')
        )
        ->join('PSI_WMS.dbo.MITM_TBL', 'MITM_ITMCD', '=', 'ITH_ITMCD')
        ->where('MITM_MODEL', '0')
        ->whereIn('ITH_WH', ['PLANT1', 'PLANT2', 'PLANT_NA'])
        ->groupBy(
            'ITH_ITMCD',
            'MITM_ITMD1',
            'MITM_STKUOM'
        );

        if ($methode == 'all') {
            return $getData->get();
        } else {
            foreach ($filteropt as $keyFilter => $valueFilter) {
                if ($valueFilter['field']['field'] === 'RPWIP_DATEIS' && !empty($valueFilter['val'])) {
                    $getData->where('ITH_DATE', '<=', $valueFilter['val']);
                }

                if ($valueFilter['field']['field'] === 'RPWIP_ITMCOD' && !empty($valueFilter['val'])) {
                    $getData->where('ITH_ITMCD', 'like', '%'.$valueFilter['val'].'%');
                }

                if ($valueFilter['field']['field'] === 'MITM_ITMD1' && !empty($valueFilter['val'])) {
                    $getData->where('MITM_ITMD1', 'like', '%'.$valueFilter['val'].'%');
                }
            }
        }

        return $getData->orderby('ITH_ITMCD');
    }

    public function Searching(Request $req)
    {
        $getquery = SaldoWIP::orderBy('RPWIP_DATEIS')->join('PSI_WMS.dbo.MITM_TBL', 'MITM_ITMCD', '=', 'RPWIP_ITMCOD');
        if (!empty($req->date)) {
            $hasil = $getquery->where('RPWIP_DATEIS','<=',$req->ldate);
        } elseif(!empty($req->kd_brng)){
            $hasil = $getquery->where('RPWIP_ITMCOD',$req->kodeBrg);
        } else {
            $hasil = $getquery->where('MITM_ITMD1','like','%'.$req->namaBrg.'%');
        }

        return $hasil->get();
    }

    public function ExportToExcell(Request $req)
    {
        $cekTanggalAkhir = array_filter($req->param, function ($f) {
            return $f['field']['field'] === 'RPWIP_DATEIS';
        });

        if (count($cekTanggalAkhir) > 0) {
            $datePeriod = ['', date('d M Y', strtotime($cekTanggalAkhir[0]['val']))];
        } else {
            $datePeriod = ['', ''];
        }

        $data = $this->data($req->param)->get();

        if ($req->format == 'excel') {
            Excel::store(new MutasiWIPExport($data, $datePeriod, $req->format), 'MutasiWIPExport.xls', 'public');

            return 'public/storage/MutasiWIPExport.xls';
        } elseif ($req->format == 'pdf') {
            Excel::store(new MutasiWIPExport($data, $datePeriod, $req->format), 'MutasiWIPExport.pdf', 'public', \Maatwebsite\Excel\Excel::DOMPDF);

            return 'public/storage/MutasiWIPExport.pdf';
        }
    }
}
