<?php

namespace App\Http\Controllers\WMS\REPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\WMS\REPORT\MutasiStok;
use Illuminate\Support\Facades\DB;
use App\Exports\MutasiMasukExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Model\RPCUST\MutasiDokPabean;
use App\Helpers\CustomFunctionHelper;
use Illuminate\Support\Facades\Log;

class MutasiStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WMS\REPORT\MutasiStok  $mutasiStok
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $methode = null)
    {
        $filterData = $this->getData(
            $request->fdate,
            $request->ldate,
            $request->jenPabean,
            $request->kodeBrg,
            $request->namaBrg,
            $request->noPabean,
            $request->pagination
        );

        // return $request->pagination;

        $rows = $request->has('pagination') ? $request->pagination['rowsPerPage'] : 20;
        $page = $request->has('pagination') ? $request->pagination['page'] : 1;

        // return [$rows, ['*'], 'page', $page];
        if ($methode == 'table') {
            if ($rows == 0) {
                $cekall = $filterData->count();
                return $filterData->paginate($cekall, ['*'], 'page', $page);
            } else {
                return $filterData->paginate($rows, ['*'], 'page', $page);
            }
        } elseif ($methode == 'data') {
            return $filterData->get();
        } else {
            $data = $filterData->get();
            return $this->ExportToExcell($data);
        }
    }

    public function newShow(Request $request, $methode = null, $state = 'INC', $export = false)
    {
        $data = $this->newGetData($request->form, $methode, $state, $export);

        // return $data->get();
        $hasil = [];
        foreach ($data->get()->toArray() as $key => $value) {
            $cussup = $state === 'INC' ? 'MSUP_SUPNM' : 'MCUS_CUSNM';

            if ($export) {
                $valnya = [
                    'MITM_ITMD1' => $value['MITM_ITMD1'],
                    $cussup => $state === 'INC' ? $value['MSUP_SUPNM'] : $value['MCUS_CUSNM'],
                    'QTY' => (string)number_format($value['RPBEA_QTYSAT'] * intval($value['RPBEA_QTYJUM']), 2, '.', ','),
                    'RPBEA_ITMCOD' => $value['RPBEA_ITMCOD'],
                    'RPBEA_JENBEA' => 'BC ' . $value['RPBEA_JENBEA'],
                    'RPBEA_NUMBEA' => $value['RPBEA_NUMBEA'],
                    'RPBEA_NUMPEN' => $value['RPBEA_NUMPEN'],
                    'RPBEA_NUMSJL' => $value['RPBEA_NUMSJL'],
                    'RPBEA_QTYJUM' => number_format($value['RPBEA_QTYJUM']),
                    'RPBEA_TGLBEA' => !empty($value['RPBEA_TGLBEA']) ? date('d-m-Y', strtotime($value['RPBEA_TGLBEA'])) : '',
                    'RPBEA_TGLPEN' => !empty($value['RPBEA_TGLPEN']) ? date('d-m-Y', strtotime($value['RPBEA_TGLPEN'])) : '',
                    'RPBEA_RCVEDT' => !empty($value['RPBEA_RCVEDT']) ? date('d-m-Y', strtotime($value['RPBEA_RCVEDT'])) : '',
                    'RPBEA_TGLSJL' => !empty($value['RPBEA_TGLSJL']) ? date('d-m-Y', strtotime($value['RPBEA_TGLSJL'])) : '',
                    'RPBEA_UNITMS' => $value['RPBEA_UNITMS'],
                    'RPBEA_VALAS' => $value['RPBEA_VALAS'],
                    'no' => $key + 1,
                    'INV_NO' => $value['INV_NO'],
                    'STATUS_KIRIM' => $value['STATUS_KIRIM'],
                ];
            } else {
                $valnya = [
                    'MITM_ITMD1' => $value['MITM_ITMD1'],
                    $cussup => $state === 'INC' ? $value['MSUP_SUPNM'] : $value['MCUS_CUSNM'],
                    'QTY' => (string)number_format($value['RPBEA_QTYSAT'] * intval($value['RPBEA_QTYJUM']), 2, '.', ','),
                    'RPBEA_ITMCOD' => $value['RPBEA_ITMCOD'],
                    'RPBEA_JENBEA' => 'BC ' . $value['RPBEA_JENBEA'],
                    'RPBEA_NUMBEA' => $value['RPBEA_NUMBEA'],
                    'RPBEA_NUMPEN' => $value['RPBEA_NUMPEN'],
                    'RPBEA_NUMSJL' => $value['RPBEA_NUMSJL'],
                    'RPBEA_QTYJUM' => number_format($value['RPBEA_QTYJUM']),
                    'RPBEA_TGLBEA' => !empty($value['RPBEA_TGLBEA']) ? date('d-m-Y', strtotime($value['RPBEA_TGLBEA'])) : '',
                    'RPBEA_TGLPEN' => !empty($value['RPBEA_TGLPEN']) ? date('d-m-Y', strtotime($value['RPBEA_TGLPEN'])) : '',
                    'RPBEA_RCVEDT' => !empty($value['RPBEA_RCVEDT']) ? date('d-m-Y', strtotime($value['RPBEA_RCVEDT'])) : '',
                    'RPBEA_TGLSJL' => !empty($value['RPBEA_TGLSJL']) ? date('d-m-Y', strtotime($value['RPBEA_TGLSJL'])) : '',
                    'RPBEA_UNITMS' => $value['RPBEA_UNITMS'],
                    'RPBEA_VALAS' => $value['RPBEA_VALAS'],
                    'no' => $key + 1
                ];
            }
            $hasil[] = $valnya;
        }

        $filterData = collect($hasil);

        if ($methode == 'table') {
            $rows = $request->has('pagination') ? $request->pagination['rowsPerPage'] : 20;
            $page = $request->has('pagination') ? $request->pagination['page'] : 1;
            $rowsNum = $request->has('pagination') ? $filterData->count() : $filterData->count();

            // file_put_contents('logs.txt', $filterData->toSql().json_encode($filterData->getBindings()).PHP_EOL , FILE_APPEND | LOCK_EX);
            if ($rows == 0) {
                $cekall = $filterData->count();
                return $filterData->paginate($cekall);
                return $filterData->paginate($cekall, ['*'], 'page', $page);
            } else {
                return $filterData->paginate($rows, $rowsNum, $page);
                return $filterData->paginate($rows, ['*'], 'page', $page);
            }
        } elseif ($methode == 'data') {
            return $filterData;
        } else {
            $data = $filterData->get();
            return $this->ExportToExcell($request, $data, $state);
        }
    }

    public function newGetData($filteropt, $methode = null, $state = 'INC', $export = false)
    {
        if ($export) {
            $select = [
                'RPBEA_JENBEA',
                'RPBEA_NUMBEA',
                DB::raw("ISNULL(CONVERT(VARCHAR(30),RPBEA_TGLBEA,121),'') AS RPBEA_TGLBEA"),
                'RPBEA_NUMSJL',
                DB::raw("ISNULL(CONVERT(VARCHAR(30),RPBEA_TGLSJL,121),'') AS RPBEA_TGLSJL"),
                'RPBEA_NUMPEN',
                DB::raw("ISNULL(CONVERT(VARCHAR(30),RPBEA_TGLPEN,121),'') AS RPBEA_TGLPEN"),
                DB::raw('CASE WHEN MSUP_SUPNM IS NULL THEN MCUS_CUSNM ELSE MSUP_SUPNM END AS MSUP_SUPNM'),
                DB::raw('CASE WHEN RPBEA_CONSIGNEE IS NOT NULL
                    THEN (
                        SELECT MDEL_ZNAMA FROM PSI_WMS.dbo.MDEL_TBL
                        WHERE MDEL_DELCD = RPBEA_CONSIGNEE
                    ) ELSE (
                        CASE WHEN MCUS_CUSNM IS NULL THEN MSUP_SUPNM ELSE MCUS_CUSNM END
                    ) END
                    AS MCUS_CUSNM'),
                'RPBEA_ITMCOD',
                'MITM_ITMD1',
                DB::raw('SUM(CAST(RPBEA_QTYJUM AS FLOAT)) AS RPBEA_QTYJUM'),
                'RPBEA_UNITMS',
                DB::raw("(
                CASE WHEN RPBEA_TYPE = 'INC'
                    THEN (
                        select max(MSUP_SUPCR) MSUP_SUPCR from
                        (select MSUP_SUPCD, MSUP_SUPNM, MSUP_SUPCR from PSI_WMS.dbo.XMSUP
                        union
                        select MCUS_CUSCD MSUP_SUPCD,MCUS_CUSNM MSUP_SUPNM, MCUS_CURCD MSUP_SUPCR from PSI_WMS.dbo.XMCUS) vb
                        WHERE vb.MSUP_SUPCD = RPBEA_CUSSUP
                        group by MSUP_SUPCD
                    )
                    ELSE (
                        SELECT MCUS_CURCD FROM PSI_WMS.dbo.MCUS_TBL
                        WHERE MCUS_CUSCD = RPBEA_CUSSUP
                    )
                    END
                ) as RPBEA_VALAS"),
                DB::raw('SUM(CAST(RPBEA_QTYJUM AS INT)) * RPBEA_QTYSAT as QTY'),
                'RPBEA_RCVEDT',
                'RPBEA_QTYSAT',
                DB::raw("
                    CASE WHEN RPBEA_TYPE = 'INC'
                    THEN (
                        SELECT DISTINCT PNGR_INVNO FROM PSI_WMS.dbo.XPNGR
                        WHERE PNGR_SUPNO = RPBEA_NUMSJL
                    )
                    ELSE (
                        SELECT DISTINCT DLV_INVNO FROM PSI_WMS.dbo.DLV_TBL
                        WHERE DLV_ID = RPBEA_NUMSJL
                    ) END AS INV_NO
                "),
                DB::raw("
                    CASE WHEN RPBEA_TYPE = 'INC'
                    THEN (
                        SELECT TOP 1 URAIAN_TUJUAN_PENGIRIMAN FROM PSI_WMS.dbo.ZTJNKIR_TBL
                            WHERE KODE_DOKUMEN = RPBEA_JENBEA
                            AND KODE_TUJUAN_PENGIRIMAN = RCV_ZSTSRCV
                    ) ELSE (
                        SELECT TOP 1 URAIAN_TUJUAN_PENGIRIMAN FROM PSI_WMS.dbo.ZTJNKIR_TBL
                            WHERE KODE_DOKUMEN = DLV_BCTYPE
                            AND KODE_TUJUAN_PENGIRIMAN = DLV_PURPOSE
                    ) END AS STATUS_KIRIM
                ")
            ];

            $group = [
                'RPBEA_JENBEA',
                'RPBEA_NUMBEA',
                'RPBEA_TGLBEA',
                'RPBEA_NUMSJL',
                'RPBEA_TGLSJL',
                'RPBEA_NUMPEN',
                'RPBEA_TGLPEN',
                'MSUP_SUPNM',
                'MCUS_CUSNM',
                'RPBEA_ITMCOD',
                'MITM_ITMD1',
                'RPBEA_UNITMS',
                'RPBEA_RCVEDT',
                'RPBEA_QTYSAT',
                'RPBEA_CUSSUP',
                'RPBEA_TYPE',
                'RPBEA_CONSIGNEE',,
                'DLV_BCTYPE',
                'DLV_PURPOSE',
                'RCV_ZSTSRCV'
            ];
        } else {
            $select = [
                // 'id',
                'RPBEA_JENBEA',
                'RPBEA_NUMBEA',
                DB::raw("ISNULL(CONVERT(VARCHAR(30),RPBEA_TGLBEA,121),'') AS RPBEA_TGLBEA"),
                'RPBEA_NUMSJL',
                DB::raw("ISNULL(CONVERT(VARCHAR(30),RPBEA_TGLSJL,121),'') AS RPBEA_TGLSJL"),
                'RPBEA_NUMPEN',
                DB::raw("ISNULL(CONVERT(VARCHAR(30),RPBEA_TGLPEN,121),'') AS RPBEA_TGLPEN"),
                DB::raw('CASE WHEN MSUP_SUPNM IS NULL THEN MCUS_CUSNM ELSE MSUP_SUPNM END AS MSUP_SUPNM'),
                DB::raw('CASE WHEN RPBEA_CONSIGNEE IS NOT NULL
                    THEN (
                        SELECT MDEL_ZNAMA FROM PSI_WMS.dbo.MDEL_TBL
                        WHERE MDEL_DELCD = RPBEA_CONSIGNEE
                    ) ELSE (
                        CASE WHEN MCUS_CUSNM IS NULL THEN MSUP_SUPNM ELSE MCUS_CUSNM END
                    ) END
                    AS MCUS_CUSNM'),
                'RPBEA_ITMCOD',
                'MITM_ITMD1',
                DB::raw('SUM(CAST(RPBEA_QTYJUM AS FLOAT)) AS RPBEA_QTYJUM'),
                'RPBEA_UNITMS',
                DB::raw("(
                CASE WHEN RPBEA_TYPE = 'INC'
                    THEN (
                        select max(MSUP_SUPCR) MSUP_SUPCR from
                        (select MSUP_SUPCD, MSUP_SUPNM, MSUP_SUPCR from PSI_WMS.dbo.XMSUP
                        union
                        select MCUS_CUSCD MSUP_SUPCD,MCUS_CUSNM MSUP_SUPNM, MCUS_CURCD MSUP_SUPCR from PSI_WMS.dbo.XMCUS) vb
                        WHERE vb.MSUP_SUPCD = RPBEA_CUSSUP
                        group by MSUP_SUPCD
                    )
                    ELSE (
                        SELECT MCUS_CURCD FROM PSI_WMS.dbo.MCUS_TBL
                        WHERE MCUS_CUSCD = RPBEA_CUSSUP
                    )
                    END
                ) as RPBEA_VALAS"),
                DB::raw('SUM(CAST(RPBEA_QTYJUM AS INT)) * RPBEA_QTYSAT as QTY'),
                'RPBEA_RCVEDT',
                'RPBEA_QTYSAT'
            ];

            $group = [
                'RPBEA_JENBEA',
                'RPBEA_NUMBEA',
                'RPBEA_TGLBEA',
                'RPBEA_NUMSJL',
                'RPBEA_TGLSJL',
                'RPBEA_NUMPEN',
                'RPBEA_TGLPEN',
                'MSUP_SUPNM',
                'MCUS_CUSNM',
                'RPBEA_ITMCOD',
                'MITM_ITMD1',
                'RPBEA_UNITMS',
                'RPBEA_RCVEDT',
                'RPBEA_QTYSAT',
                'RPBEA_CUSSUP',
                'RPBEA_TYPE',
                'RPBEA_CONSIGNEE',
                'DLV_BCTYPE',
                'DLV_PURPOSE'
            ];
        }

        $getData = MutasiDokPabean::select($select)
            ->leftjoin('PSI_WMS.dbo.MSUP_TBL', 'MSUP_SUPCD', '=', 'RPBEA_CUSSUP')
            ->leftjoin('PSI_WMS.dbo.MCUS_TBL', 'MCUS_CUSCD', '=', 'RPBEA_CUSSUP')
            ->leftjoin('PSI_WMS.dbo.RCV_TBL', function($j) {
                $j->on('RCV_DONO', 'RPBEA_NUMSJL');
                $j->on('RCV_ITMCD', 'RPBEA_ITMCOD');
                $j->on('RCV_BCTYPE', 'RPBEA_JENBEA');
            })
            ->leftjoin('PSI_WMS.dbo.DLV_TBL', function($j) {
                $j->on('DLV_ID', 'RPBEA_NUMSJL');
                $j->on('DLV_BCTYPE', 'RPBEA_JENBEA');
            })
            ->join('PSI_WMS.dbo.MITM_TBL', 'MITM_ITMCD', '=', 'RPBEA_ITMCOD')
            ->where('RPBEA_TYPE', $state)
            // Exclude num bea with zero trail
            ->where('RPBEA_NUMBEA', '<>', '000000')
            ->whereNotNull('RPBEA_NUMBEA')
            ->orderBy('RPBEA_TGLBEA')
            ->groupBy($group);

        // logger($getData->toSql());

        if ($methode == 'all') {
            return $getData->get();
        } else {
            $hasil = CustomFunctionHelper::filtering($getData, $filteropt);

            return $hasil;
        }

        return $getData->orderby('id');
    }

    public function getData($fdate, $ldate = null, $jenPabean = null, $kodeBrg = null, $namaBrg = null, $noPabean = null, $pagination = null)
    {
        // return $request->pagination;
        $select = [
            'id',
            'RPBEA_JENBEA',
            'RPBEA_NUMBEA',
            'RPBEA_TGLBEA',
            'RPBEA_NUMSJL',
            'RPBEA_TGLSJL',
            'RPBEA_NUMPEN',
            'RPBEA_TGLPEN',
            'MSUP_SUPNM',
            'RPBEA_ITMCOD',
            'MITM_ITMD1',
            'RPBEA_QTYJUM',
            'RPBEA_UNITMS',
            'RPBEA_VALAS',
            DB::raw('RPBEA_QTYJUM as QTY')
        ];

        $getData = MutasiDokPabean::select($select)
            ->join('PSI_WMS.dbo.MSUP_TBL', 'MSUP_SUPCD', '=', 'RPBEA_CUSSUP')
            ->join('PSI_WMS.dbo.XMITM_TBL', 'MITM_ITMCD', '=', 'RPBEA_ITMCOD');

        $filterData = $getData->where('RPBEA_RCVEDT', '>=', $fdate)
            ->when(!empty($ldate), function ($query) use ($ldate) {
                $query->where('RPBEA_RCVEDT', '<=', $ldate);
            })
            ->when(!empty($jenPabean), function ($query) use ($jenPabean) {
                $query->wherein('RPBEA_JENBEA', $jenPabean);
            })
            ->when(!empty($noPabean), function ($query) use ($noPabean) {
                $query->where('RPBEA_NUMBEA', $noPabean);
            })
            ->when(!empty($kodeBrg), function ($query) use ($kodeBrg) {
                $query->where('RPBEA_ITMCOD', 'like', '%' . $kodeBrg . '%');
            })
            ->when(!empty($namaBrg), function ($query) use ($namaBrg) {
                $query->where('MITM_ITMD1', 'like', '%' . $namaBrg . '%');
            });

        return $filterData;
    }

    public function ExportToExcell(Request $req, $met = 'INC')
    {
        $dataInit = $this->newShow(new Request(['form' => $req->param]), 'data', $met, true);

        $cekDateRange = array_filter($req->param, function ($f) {
            return $f['field']['field'] === 'RPBEA_TGLBEA';
        })[0];

        $datePeriod = [date('d M Y', strtotime($cekDateRange['val'])), date('d M Y', strtotime($cekDateRange['val2']))];

        $data = [];
        foreach ($dataInit as $key => $value) {
            $cussup = $met === 'INC' ? 'MSUP_SUPNM' : 'MCUS_CUSNM';
            $data[] = [
                'MITM_ITMD1' => $value['MITM_ITMD1'],
                $cussup => $value[$cussup],
                'QTY' => $value['QTY'],
                'RPBEA_ITMCOD' => $value['RPBEA_ITMCOD'],
                'RPBEA_JENBEA' => $value['RPBEA_JENBEA'],
                'RPBEA_NUMBEA' => $value['RPBEA_NUMBEA'],
                'RPBEA_NUMPEN' => $value['RPBEA_NUMPEN'],
                'RPBEA_NUMSJL' => $value['RPBEA_NUMSJL'],
                'RPBEA_QTYJUM' => $value['RPBEA_QTYJUM'],
                'RPBEA_TGLBEA' => !empty($value['RPBEA_TGLBEA']) ? date('d-m-Y', strtotime($value['RPBEA_TGLBEA'])) : '',
                'RPBEA_TGLPEN' => !empty($value['RPBEA_TGLPEN']) ? date('d-m-Y', strtotime($value['RPBEA_TGLPEN'])) : '',
                'RPBEA_RCVEDT' => !empty($value['RPBEA_RCVEDT']) ? date('d-m-Y', strtotime($value['RPBEA_RCVEDT'])) : '',
                'RPBEA_TGLSJL' => !empty($value['RPBEA_TGLSJL']) ? date('d-m-Y', strtotime($value['RPBEA_TGLSJL'])) : '',
                'RPBEA_UNITMS' => $value['RPBEA_UNITMS'],
                'RPBEA_VALAS' => $value['RPBEA_VALAS'],
                'no' => $value['no'],
                'INV_NO' => $value['INV_NO'],
                'STATUS_KIRIM' => $value['STATUS_KIRIM']
            ];
        }
        // return $datePeriod;

        if ($req->format == 'excel') {
            Excel::store(new MutasiMasukExport($data, $datePeriod, $met, $req->format), $met == 'INC' ? 'MutasiMasukExport.xls' : 'MutasiKeluarExport_' . date('Y_m_d') . '.xls', 'public');

            return $met == 'INC' ? 'storage/app/public/MutasiMasukExport.xls' : 'storage/app/public/MutasiKeluarExport_' . date('Y_m_d') . '.xls';
        } elseif ($req->format == 'pdf') {
            Excel::store(new MutasiMasukExport($data, $datePeriod, $met, $req->format), $met == 'INC' ? 'MutasiMasukExport.pdf' : 'MutasiKeluarExport_' . date('Y_m_d') . '.pdf', 'public', \Maatwebsite\Excel\Excel::DOMPDF);

            return $met == 'INC' ? 'storage/app/public/MutasiMasukExport.pdf' : 'storage/app/public/MutasiKeluarExport_' . date('Y_m_d') . '.pdf';
        }
    }
}
