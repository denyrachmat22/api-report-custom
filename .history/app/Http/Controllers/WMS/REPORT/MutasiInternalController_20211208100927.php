<?php

namespace App\Http\Controllers\WMS\REPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\RPCUST\MutasiRaw;
use App\Helpers\CustomFunctionHelper;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MutasiBahanBakuExport;
use Illuminate\Support\Facades\DB;
use App\Model\MASTER\ItemMaster;
use App\Model\MASTER\ITHMaster;
use Illuminate\Support\Facades\Log;

class MutasiInternalController extends Controller
{
    public function show(Request $request)
    {
        $rows = $request->has('pagination') ? $request['pagination']['rowsPerPage'] : 20;
        $page = $request->has('pagination') ? $request['pagination']['page'] : 1;

        $data = $this->newData($request);

        return $data;

        $hasil = [];
        foreach ($data as $key => $value) {
            $hasil[] = array_merge($value, ['no' => $key + 1]);
        }

        $filterData = collect($hasil);

        $rowsNum = $request->has('pagination') ? $request->pagination['rowsNumber'] : $filterData->count();
    }

    public function data($filteropt, $mutasi, $rows = null, $page = null, $username = '')
    {
        $queryString = "SET NOCOUNT ON ;EXEC PSI_RPCUST.dbo.sp_mutasi_inv @mutasi = 'bahan_baku', @inc_rcv = 1";

        foreach ($filteropt as $keyFilter => $valueFilter) {
            if ($valueFilter['field']['field'] === 'RPRAW_DATEIS' && !empty($valueFilter['val'])) {
                $queryString .= ", @date_from='".$valueFilter['val']."', @date_to='".$valueFilter['val2']."'";
            }

            if ($valueFilter['field']['field'] === 'RPRAW_ITMCOD' && !empty($valueFilter['val'])) {
                $queryString .= ", @item_num='".$valueFilter['val']."'";
            }
        }

        if (!empty($rows) && !empty($page)) {
            $queryString .= ", @per_page=".$rows.", @page=".$page;
        }

        if (empty($username) || $username !== 'smt') {
            $queryString .= ", @minus_prot = 1";
        } else {
            $queryString .= ", @minus_prot = 0";
        }

        $result = array_map(function ($value) {
            return (array)$value;
        }, DB::select($queryString));

        $data = [];
        foreach ($result as $key => $value) {
            foreach ($value as $keyDet => $valueDet) {
                if (($keyDet === 'RPRAW_ITMCOD' || $keyDet === 'MITM_ITMD1' || $keyDet === 'RPRAW_UNITMS')) {
                    $data[$key][$keyDet] = $value[$keyDet];
                } else {
                    if ($value[$keyDet] == (int)$value[$keyDet]) {
                        $data[$key][$keyDet] = (int)$value[$keyDet];
                    } else {
                        $data[$key][$keyDet] = $value[$keyDet];
                    }
                }
            }
        }

        return collect($data);
    }

    public function newData($req)
    {
        $queryString = "SET NOCOUNT ON ;EXEC PSI_RPCUST.dbo.sp_mutasi_inv @mutasi = '".$req->mutasi."', @inc_rcv = 1";

        if (count($req->filter) > 0) {
            # code...
        }

        $result = array_map(function ($value) {
            return (array)$value;
        }, DB::select($queryString));

        return $result;
    }
}
