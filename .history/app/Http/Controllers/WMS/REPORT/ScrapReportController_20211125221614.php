<?php

namespace App\Http\Controllers\WMS\REPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use App\Model\WMS\API\SPLSCN;
use App\Model\RPCUST\scrapHist;
use App\Model\RPCUST\scrapHistDet;
use App\Model\RPCUST\processMstr;
use App\Model\RPCUST\processMapDet;
use App\Model\MASTER\MEGAEMS\ppsn1Table;
use App\Model\WMS\API\RCVSCN2;
use Illuminate\Support\Facades\DB;
use App\Model\MASTER\SERD2TBL;
use App\Model\WMS\REPORT\MutasiStok;
use App\Model\WMS\API\StockSMT;
use App\Model\MASTER\ITHMaster;

use App\Jobs\scrapReportInsert;
use App\Jobs\scrapDetailInserts;
use App\Jobs\scrapDetailInsertsMaterial;
use App\Model\RPCUST\StockExBC;
use App\Model\WMS\API\RCVSCN;
use LRedis;
use Illuminate\Support\Facades\Queue;
use App\Model\WMS\API\SERDTBL;

use App\Model\WMS\API\RETFG;

use App\Imports\uploadScrapHist;

// Template Upload
use App\Exports\scrapWIPUploadTemplateExport;
use App\Exports\scrapPendingUploadTemplateExport;
use App\Exports\scrapMaterialUploadTemplateExport;
use App\Exports\scrapFGReturnUploadTemplateExport;
use App\Exports\scrapReportUploadTemplate;

class ScrapReportController extends Controller
{
    public function psnListByJob($job, $procd = '')
    {
        $getData = ppsn1Table::select(
            'PPSN1_PSNNO',
            'PPSN2_ITMCAT',
            'PPSN1_LINENO',
            'PPSN1_FR',
            'PPSN2_MC',
            'PPSN2_MCZ',
            DB::raw('
            CASE WHEN PIS2_QTPER IS NULL
                THEN PPSN2_QTPER
                ELSE PIS2_QTPER
            END AS QTPER_VAL
            '),
            'PPSN2_SUBPN',
            'PPSN1_PROCD',
            'PPSN2_QTPER'
        )
            ->join('SRVMEGA.PSI_MEGAEMS.dbo.PPSN2_TBL', function ($f) {
                $f->on('PPSN1_PSNNO', 'PPSN2_PSNNO');
                $f->on('PPSN1_LINENO', 'PPSN2_LINENO');
                $f->on('PPSN1_FR', 'PPSN2_FR');
                $f->on('PPSN1_DOCNO', 'PPSN2_DOCNO');
                $f->on('PPSN1_BSGRP', 'PPSN2_BSGRP');
            })
            ->leftjoin('SRVMEGA.PSI_MEGAEMS.dbo.PIS2_TBL', function ($f) {
                $f->on('PIS2_WONO', 'PPSN1_WONO');
                $f->on('PIS2_PROCD', 'PPSN1_PROCD');
                $f->on('PIS2_ITMCD', 'PPSN2_SUBPN');
                $f->on('PIS2_MC', 'PPSN2_MC');
                $f->on('PIS2_MCZ', 'PPSN2_MCZ');
            })
            ->leftjoin('SPL_TBL', function ($f) {
                $f->on('SPL_DOC', 'PPSN2_PSNNO');
                $f->on('SPL_PROCD', 'PPSN1_PROCD');
                $f->on('SPL_ITMCD', 'PPSN2_SUBPN');
                $f->on('SPL_MC', 'PPSN2_MC');
                $f->on('SPL_ORDERNO', 'PPSN2_MCZ');
            })
            // ->where('PPSN1_WONO', $job)
            ->groupBy(
                'PPSN1_DOCNO',
                'PPSN1_PSNNO',
                'PPSN2_ITMCAT',
                'PPSN1_LINENO',
                'PPSN1_FR',
                'PPSN1_PROCD',
                'PPSN2_SUBPN',
                'PPSN2_MC',
                'PPSN2_MCZ',
                'PPSN2_QTPER',
                'PIS2_QTPER'
            );

        if (empty($procd)) {
            return $getData->get()->toArray();
        } else {
            return $getData->where('PPSN1_PROCD', $procd)->get()->toArray();
        }
    }

    public function psnListByJobSerD($job, $procd = '')
    {
        $getData = SERDTBL::select(
            DB::raw('SERD_PSNNO AS PPSN1_PSNNO'),
            DB::raw('SERD_CAT AS PPSN2_ITMCAT'),
            DB::raw('SERD_LINENO AS PPSN1_LINENO'),
            DB::raw('SERD_FR AS PPSN1_FR'),
            DB::raw('SERD_MC AS PPSN2_MC'),
            DB::raw('SERD_MCZ AS PPSN2_MCZ'),
            DB::raw('SERD_QTPER AS QTPER_VAL'),
            DB::raw('SERD_MPART AS PPSN2_SUBPN'),
            DB::raw('SERD_PROCD AS PPSN1_PROCD'),
            DB::raw('SERD_QTPER AS PPSN2_QTPER')
        )
            ->where('SERD_JOB', $job);

        if (empty($procd)) {
            return $getData->get()->toArray();
        } else {
            return $getData->where('SERD_PROCD', $procd)->get()->toArray();
        }
    }

    public function getDataSPLSCN($psn, $cat, $line, $fr, $orderNo, $item)
    {
        $data = SPLSCN::select([
            'SPLSCN_DOC',
            'SPLSCN_CAT',
            'SPLSCN_LINE',
            'SPLSCN_FEDR',
            'SPLSCN_ORDERNO',
            'SPLSCN_ITMCD',
            'SPLSCN_LOTNO',
            'SPLSCN_LUPDT',
            DB::raw('(SUM(SPLSCN_QTY) - COALESCE(SUM(RETSCN_QTYAFT), 0)) - COALESCE(SUM(SCR_QTY), 0) AS SPLSCN_QTY'),
            DB::raw('SUM(RETSCN_QTYAFT) AS RETURN_QTY'),
            DB::raw('COALESCE(SUM(SCR_QTY), 0) AS SCRAP_QTY'),
            DB::raw('SUM(SPLSCN_QTY) AS REAL_SPLSCN_QTY'),
            DB::raw('(SELECT TOP 1
                RCV_PRPRC
                FROM RCVSCN_TBL
                INNER JOIN RCV_TBL ON RCVSCN_DONO = RCV_DONO
                    AND RCVSCN_ITMCD = RCV_ITMCD
                WHERE RCVSCN_ITMCD = SPLSCN_ITMCD
                AND RCVSCN_LOTNO = SPLSCN_LOTNO
                ORDER BY RCV_RPDATE DESC) as PRICE'),
            DB::raw('(SELECT TOP 1
                RCV_BCTYPE
                FROM RCVSCN_TBL
                INNER JOIN RCV_TBL ON RCVSCN_DONO = RCV_DONO
                    AND RCVSCN_ITMCD = RCV_ITMCD
                WHERE RCVSCN_ITMCD = SPLSCN_ITMCD
                AND RCVSCN_LOTNO = SPLSCN_LOTNO
                ORDER BY RCV_RPDATE DESC) as RCV_BCTYPE')
        ])
            ->leftjoin('RETSCN_TBL', function ($f) {
                $f->on('RETSCN_ITMCD', '=', 'SPLSCN_ITMCD');
                $f->on('RETSCN_SPLDOC', '=', 'SPLSCN_DOC');
                $f->on('RETSCN_CAT', '=', 'SPLSCN_CAT');
                $f->on('RETSCN_LINE', '=', 'SPLSCN_LINE');
                $f->on('RETSCN_FEDR', '=', 'SPLSCN_FEDR');
                $f->on('RETSCN_ORDERNO', '=', 'SPLSCN_ORDERNO');
                $f->on('RETSCN_LOT', '=', 'SPLSCN_LOTNO');
            })
            ->leftjoin('PSI_RPCUST.dbo.RPSCRAP_HIST_DET', function ($f2) {
                $f2->on('SCR_PSN', '=', 'SPLSCN_DOC');
                $f2->on('SCR_CAT', '=', 'SPLSCN_CAT');
                $f2->on('SCR_LINE', '=', 'SPLSCN_LINE');
                $f2->on('SCR_FR', '=', 'SPLSCN_FEDR');
                $f2->on('SCR_MCZ', '=', 'SPLSCN_ORDERNO');
                $f2->on('SCR_ITMCD', '=', 'SPLSCN_ITMCD');
            })
            ->where('SPLSCN_DOC', $psn)
            ->where('SPLSCN_CAT', $cat)
            ->where('SPLSCN_LINE', $line)
            ->where('SPLSCN_FEDR', $fr)
            ->where('SPLSCN_ORDERNO', $orderNo)
            ->where('SPLSCN_ITMCD', $item)
            ->groupBy(
                [
                    'SPLSCN_DOC',
                    'SPLSCN_CAT',
                    'SPLSCN_LINE',
                    'SPLSCN_FEDR',
                    'SPLSCN_ORDERNO',
                    'SPLSCN_ITMCD',
                    'SPLSCN_LOTNO',
                    'SPLSCN_LUPDT'
                ]
            )
            ->orderBy('SPLSCN_LOTNO', 'DESC')
            ->get()
            ->toArray();

        return $data;
    }

    public function getDataSerd2($psn, $cat, $line, $fr, $orderNo, $item)
    {
        $data = SERD2TBL::select(
            DB::raw('SERD2_PSNNO as SPLSCN_DOC'),
            DB::raw('SERD2_LINENO as SPLSCN_LINE'),
            DB::raw('SERD2_PROCD as SPLSCN_LINE'),
            DB::raw('SERD2_CAT as SPLSCN_CAT'),
            DB::raw('SERD2_ITMCD as SPLSCN_ITMCD'),
            DB::raw('SERD2_LOTNO as SPLSCN_LOTNO'),
            // DB::raw('SERD2_QTY as SPLSCN_QTY'),
            DB::raw('SERD2_MCZ as SPLSCN_ORDERNO'),
            DB::raw('SERD2_FR as SPLSCN_FEDR'),
            DB::raw('(SELECT TOP 1
                RCV_PRPRC
                FROM RCVSCN_TBL
                INNER JOIN RCV_TBL ON RCVSCN_DONO = RCV_DONO
                    AND RCVSCN_ITMCD = RCV_ITMCD
                WHERE RCVSCN_ITMCD = SERD2_ITMCD
                AND RCVSCN_LOTNO = SERD2_LOTNO
                ORDER BY RCV_RPDATE DESC) as PRICE'),
            DB::raw('(SELECT TOP 1
                RCV_BCTYPE
                FROM RCVSCN_TBL
                INNER JOIN RCV_TBL ON RCVSCN_DONO = RCV_DONO
                    AND RCVSCN_ITMCD = RCV_ITMCD
                WHERE RCVSCN_ITMCD = SERD2_ITMCD
                AND RCVSCN_LOTNO = SERD2_LOTNO
                ORDER BY RCV_RPDATE DESC) as RCV_BCTYPE'),
            DB::raw('(SUM(SERD2_QTY) - COALESCE(SUM(RETSCN_QTYAFT), 0)) - COALESCE(SUM(SCR_QTY), 0) AS SPLSCN_QTY'),
            DB::raw('SUM(RETSCN_QTYAFT) AS RETURN_QTY'),
            DB::raw('COALESCE(SUM(SCR_QTY), 0) AS SCRAP_QTY'),
            DB::raw('SUM(SERD2_QTY) AS REAL_SPLSCN_QTY'),
        )
            ->leftjoin('RETSCN_TBL', function ($f) {
                $f->on('RETSCN_ITMCD', '=', 'SERD2_ITMCD');
                $f->on('RETSCN_SPLDOC', '=', 'SERD2_PSNNO');
                $f->on('RETSCN_CAT', '=', 'SERD2_CAT');
                $f->on('RETSCN_LINE', '=', 'SERD2_LINENO');
                $f->on('RETSCN_FEDR', '=', 'SERD2_FR');
                $f->on('RETSCN_ORDERNO', '=', 'SERD2_MCZ');
                $f->on('RETSCN_LOT', '=', 'SERD2_LOTNO');
            })
            ->leftjoin('PSI_RPCUST.dbo.RPSCRAP_HIST_DET', function ($f2) {
                $f2->on('SCR_PSN', '=', 'SERD2_PSNNO');
                $f2->on('SCR_CAT', '=', 'SERD2_CAT');
                $f2->on('SCR_LINE', '=', 'SERD2_LINENO');
                $f2->on('SCR_FR', '=', 'SERD2_FR');
                $f2->on('SCR_MCZ', '=', 'SERD2_MCZ');
                $f2->on('SCR_ITMCD', '=', 'SERD2_ITMCD');
            })
            ->where('SERD2_PSNNO', $psn)
            ->where('SERD2_CAT', $cat)
            ->where('SERD2_LINENO', $line)
            ->where('SERD2_FR', $fr)
            ->where('SERD2_MCZ', $orderNo)
            ->where('SERD2_ITMCD', $item)
            ->groupBy(
                'SERD2_PSNNO',
                'SERD2_LINENO',
                'SERD2_PROCD',
                'SERD2_CAT',
                'SERD2_ITMCD',
                'SERD2_LOTNO',
                'SERD2_QTY',
                'SERD2_MCZ',
                'SERD2_FR'
            )
            ->orderBy('SERD2_LOTNO', 'DESC')
            ->get()
            ->toArray();

        return $data;
    }

    public function defineMappingScrap($data)
    {
        if ($data['RPDSGN_PRO_ID'] == 'SMT-HW' && !empty($data['RPDSGN_PROCD'])) {
            return 'SMT-HW';
        } elseif ($data['RPDSGN_PRO_ID'] == 'SMT-HWADD' && !empty($data['RPDSGN_PROCD'])) {
            return 'SMT-HWAD';
        } elseif ($data['RPDSGN_PRO_ID'] == 'SMT-SP' && !empty($data['RPDSGN_PROCD'])) {
            return 'SMT-SP';
        } else {
            if ($data['RPDSGN_PROCD'] == 'SMT-AV') {
                return 'SMT-AX';
            } else {
                return $data['RPDSGN_PROCD'];
            }
        }
    }

    public function getPsnListByJobAndProcd($listProcess, $job, $idProcess, $item, $hasil = []) // Recursive here
    {
        $cekProcessNow = current($listProcess);
        if (!empty($cekProcessNow)) {
            $getDataMapping = processMapDet::where('RPDSGN_ITEMCD', $item)
                ->where('RPDSGN_PRO_ID', $cekProcessNow['RPDSGN_CODE'])
                ->orderBy('id')
                ->first();

            if ($idProcess == $cekProcessNow['RPDSGN_CODE']) {
                $hasil = array_merge($hasil, $this->psnListByJobSerD($job, $this->defineMappingScrap($getDataMapping)));
                // $hasil[$cekProcessNow['RPDSGN_CODE']] = $this->psnListByJob($job, $this->defineMappingScrap($getDataMapping));
                return $hasil;
            } else {
                if (!empty($getDataMapping['RPDSGN_PROCD'])) {
                    $hasil = array_merge($hasil, $this->psnListByJobSerD($job, $this->defineMappingScrap($getDataMapping)));
                    next($listProcess);
                    return $this->getPsnListByJobAndProcd($listProcess, $job, $idProcess, $item, $hasil);
                } else {
                    next($listProcess);
                    return $this->getPsnListByJobAndProcd($listProcess, $job, $idProcess, $item, $hasil);
                }
            }
        } else {
            return $hasil;
        }
    }

    public function getFIFOLOT($process, $qty, $hasil = [])
    {
        $cekProcessNow = current($process);

        if (!empty($cekProcessNow)) {
            $total = (int)$qty - (int)$cekProcessNow['SPLSCN_QTY'];
            if ($total > 0) {
                $hasil = array_merge($hasil, [array_merge($cekProcessNow, ['QTY_USED' => (int)$cekProcessNow['SPLSCN_QTY']])]);
                next($process);
                return $this->getFIFOLOT($process, $total, $hasil);
            } else {
                $hasil = array_merge($hasil, [array_merge($cekProcessNow, ['QTY_USED' => $qty])]);
                return $hasil;
            }
        } else {
            return $hasil;
        }
    }

    public function cekLatestHarga($item, $lot, $do = '')
    {
        $cekRcvScnForPriceHead = RCVSCN2::select(
            'RCVSCN_ITMCD',
            'RCVSCN_DONO',
            'RCV_PRPRC',
            'RCVSCN_LUPDT',
            'RCV_BCTYPE'
        )
            ->where('RCVSCN_ITMCD', $item)
            ->where('RCVSCN_LOTNO', $lot)
            ->join('RCV_TBL', function ($j) {
                $j->on('RCV_ITMCD', 'RCVSCN_ITMCD');
                $j->on('RCV_DONO', 'RCVSCN_DONO');
            })
            ->groupBy(
                'RCVSCN_ITMCD',
                'RCVSCN_DONO',
                'RCV_PRPRC',
                'RCVSCN_LUPDT',
                'RCV_BCTYPE'
            )
            ->orderBy('RCVSCN_LUPDT', 'DESC');

        if (empty($do)) {
            $cekRcvScnForPrice = $cekRcvScnForPriceHead->first();
        } else {
            $cekRcvScnForPrice = $cekRcvScnForPriceHead->where('RCVSCN_DONO', $do)->first();
        }

        return $cekRcvScnForPrice;
    }

    public function testingListJob(Request $req)
    {
        $fetchProcessMaster = processMstr::get()->toArray();

        $getPSNJob = $this->getPsnListByJobAndProcd(
            $fetchProcessMaster,
            $req->job,
            $req->proid,
            $req->item
        );

        return $getPSNJob;

        $hasil = [];
        foreach ($getPSNJob as $key => $value) {

            // Get Data LOT SPLSCN
            $getFIFOLotNo = $this->getDataSPLSCN(
                $value['PPSN1_PSNNO'],
                $value['PPSN2_ITMCAT'],
                $value['PPSN1_LINENO'],
                $value['PPSN1_FR'],
                $value['PPSN2_MCZ'],
                $value['PPSN2_SUBPN']
            );

            $hasil[] = $getFIFOLotNo;

            // $totalQty = $req->qty * floatVal($req->per);

            // $fifoLotData = $this->getFIFOLOT($getFIFOLotNo, $totalQty);

            // foreach ($fifoLotData as $keyLotData => $valueLotData) {
            //     # code...
            // }
        }

        return $hasil;
        return $getPSNJob;
    }

    public function storeHist(Request $req)
    {
        // Cek Last ID today
        $cek = scrapHist::whereBetween('created_at', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59'])->orderBy('created_at', 'desc')->withTrashed()->first();
        $idTrans = date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->ID_TRANS, -3) + 1));

        $hasil = [];

        $fetchProcessMaster = processMstr::get()->toArray();

        foreach ($req->data as $key => $value) {
            // Jika itemnya adalah Model
            if ($value['MITM_MODEL'] === '1') {
                // Parsing job yang di pilih
                foreach ($value['SCRAP_DET'] as $keyJobDet => $valueJobDet) {
                    // Parsing Process Master (PROCESS_DET)
                    foreach ($valueJobDet['PROCESS_DET'] as $keyProMstr => $valueProMstr) {
                        // Parsing Process Detail
                        foreach ($valueProMstr['DET'] as $keyProDet => $valueProDet) {

                            if (intval($valueProDet['QTY']) > 0) {
                                $insertHeader = scrapHist::create([
                                    'DSGN_MAP_ID' => $valueProDet['id'],
                                    'USERNAME' => $req->username,
                                    'DEPT' => $req->dept,
                                    'QTY' => $valueProDet['QTY'],
                                    'ID_TRANS' => $idTrans,
                                    'TYPE_TRANS' => $req->type,
                                    'DOC_NO' => $valueJobDet['PIS2_WONO'],
                                    'REASON' => isset($valueProDet['REASON']) ? $valueProDet['REASON'] : '',
                                    'SCR_PROCD' => $valueProDet['RPDSGN_PROCD'],
                                    'MDL_FLAG' => 1,
                                    'ITEMNUM' => $value['MITM_ITMCD'],
                                ]);

                                // Insert Detail Transaction

                                $findByProcess = $this->getPsnListByJobAndProcd(
                                    $fetchProcessMaster,
                                    $valueJobDet['PIS2_WONO'],
                                    $valueProDet['id'],
                                    $value['MITM_ITMCD']
                                );

                                $hasil[] = $valueProDet;

                                // Fetch All PSN by process and job number
                                foreach ($findByProcess as $key => $value) {

                                    // Get Data LOT SPLSCN
                                    $getFIFOLotNo = $this->getDataSPLSCN(
                                        $value['PPSN1_PSNNO'],
                                        $value['PPSN2_ITMCAT'],
                                        $value['PPSN1_LINENO'],
                                        $value['PPSN1_FR'],
                                        $value['PPSN2_MCZ'],
                                        $value['PPSN2_SUBPN']
                                    );

                                    $totalQty = $valueProDet['QTY'] * floatVal($value['QTPER_VAL']);

                                    $fetchFIFOLotNo = $this->getFIFOLOT($getFIFOLotNo, $totalQty);

                                    foreach ($fetchFIFOLotNo as $keyFifoLot => $valueFifoLot) {
                                        $insertDet = scrapHistDet::create([
                                            'SCR_HIST_ID' => $insertHeader->id,
                                            'SCR_PSN' => $value['PPSN1_PSNNO'],
                                            'SCR_CAT' => $value['PPSN2_ITMCAT'],
                                            'SCR_LINE' => $value['PPSN1_LINENO'],
                                            'SCR_FR' => $value['PPSN1_FR'],
                                            'SCR_MC' => $value['PPSN2_MC'],
                                            'SCR_MCZ' => $value['PPSN2_MCZ'],
                                            'SCR_ITMCD' => trim($value['PPSN2_SUBPN']),
                                            'SCR_QTPER' => (int)$value['PPSN2_QTPER'],
                                            'SCR_QTY' => $totalQty,
                                            'SCR_LOTNO' => $valueFifoLot['SPLSCN_LOTNO'],
                                            'SCR_ITMPRC' => empty($valueFifoLot['PRICE']) ? 0 : $valueFifoLot['PRICE']
                                        ]);

                                        // Insert scrap to stock
                                        $this->sendingParamBCStock(
                                            trim($value['PPSN2_SUBPN']),
                                            $totalQty,
                                            $idTrans,
                                            $valueFifoLot['SPLSCN_LOTNO'],
                                            $valueFifoLot['RCV_BCTYPE'],
                                            $insertDet->id
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                foreach ($value['SCRAP_DET'] as $keyLotDet => $valueLotDet) {
                    if ($valueLotDet['QTY'] > 0) {
                        $cekRcvScnForPrice = $this->cekLatestHarga($valueLotDet['RCVSCN_ITMCD'], $valueLotDet['SPLSCN_LOTNO'], $valueLotDet['RCVSCN_DONO']);

                        // Insert ke table Hist Master
                        $insertingScrap = scrapHist::create([
                            'DSGN_MAP_ID' => $valueLotDet['KEY_LOT'],
                            'USERNAME' => '',
                            'DEPT' => '',
                            'QTY' => intVal($valueLotDet['QTY']),
                            'ID_TRANS' => $idTrans,
                            'TYPE_TRANS' => $req->type,
                            'DOC_NO' => $valueLotDet['RCVSCN_DONO'],
                            'REASON' => isset($valueLotDet['REASON']) ? $valueLotDet['REASON'] : '',
                            'SCR_PROCD' => $valueLotDet['RCVSCN_LOTNO'],
                            'MDL_FLAG' => 0,
                            'ITEMNUM' => $valueLotDet['RCVSCN_ITMCD'],
                        ]);

                        // Insert ke table Hist Detail
                        $insertDet = scrapHistDet::create([
                            'SCR_HIST_ID' => $insertingScrap->id,
                            'SCR_PSN' => null,
                            'SCR_CAT' => null,
                            'SCR_LINE' => null,
                            'SCR_FR' => null,
                            'SCR_MC' => null,
                            'SCR_MCZ' => null,
                            'SCR_ITMCD' => $valueLotDet['RCVSCN_ITMCD'],
                            'SCR_QTPER' => 1,
                            'SCR_QTY' => intVal($valueLotDet['QTY']),
                            'SCR_LOTNO' => $valueLotDet['RCVSCN_LOTNO'],
                            'SCR_ITMPRC' => empty($cekRcvScnForPrice) ? 0 : $cekRcvScnForPrice['RCV_PRPRC']
                        ]);

                        // Insert scrap to stock
                        $insertingStock = $this->sendingParamBCStock(
                            $valueLotDet['RCVSCN_ITMCD'],
                            intVal($valueLotDet['QTY']),
                            $idTrans,
                            $valueLotDet['SPLSCN_LOTNO'],
                            $cekRcvScnForPrice['RCV_BCTYPE'],
                            $insertDet->id
                        );
                    }
                }
            }
        }

        return $hasil;
    }

    public function storeByQueue(Request $req)
    {
        $cek = scrapHist::whereBetween('created_at', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59'])->orderBy('created_at', 'desc')->withTrashed()->first();
        $idTrans = date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->ID_TRANS, -3) + 1));

        $queueInsert = (new scrapReportInsert($req->all(), $idTrans));

        dispatch($queueInsert);

        return array_merge($req->all(), ['id' => $idTrans]);
    }

    public function checkReverseProcess($data)
    {
        $parsed = [];

        foreach ($data as $key => $value) {
            if (!empty($value['PPSN1_PROCD'])) {
                // // logger($value['PPSN1_PROCD']);
                if (trim($value['PPSN1_PROCD']) == 'SMT-AB') {
                    $getPCBPer = array_values(array_filter($data, function ($f) {
                        return trim($f['PPSN1_PROCD']) == 'SMT-AB' && trim($f['PPSN2_ITMCAT']) == 'PCB';
                    }))[0]['PPSN2_QTPER'];

                    if ($value['PPSN2_ITMCAT'] != 'PCB') {
                        $parsed[] = [
                            "PPSN1_PSNNO" => $value['PPSN1_PSNNO'],
                            "PPSN2_ITMCAT" => $value['PPSN2_ITMCAT'],
                            "PPSN1_LINENO" => $value['PPSN1_LINENO'],
                            "PPSN1_FR" => $value['PPSN1_FR'],
                            "PPSN2_MC" => $value['PPSN2_MC'],
                            "PPSN2_MCZ" => $value['PPSN2_MCZ'],
                            "QTPER_VAL" => (0.5 / $getPCBPer) * $value['QTPER_VAL'],
                            "PPSN2_SUBPN" => $value['PPSN2_SUBPN'],
                            "PPSN1_PROCD" => $value['PPSN1_PROCD'],
                            "PPSN2_QTPER" => $value['PPSN2_QTPER']
                        ];
                    } else {
                        $parsed[] = $value;
                    }
                } else {
                    $parsed[] = $value;
                }
            }
        }

        return $parsed;
    }

    public function newStoreQueue(Request $req)
    {
        $cek = scrapHist::whereBetween('created_at', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59'])->orderBy('created_at', 'desc')->withTrashed()->first();
        $idTrans = date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->ID_TRANS, -3) + 1));

        $fetchProcessMaster = processMstr::get()->toArray();
        $countJobs = 0;
        $idList = [];
        foreach ($req->data as $key => $value) {
            // Jika itemnya adalah Model
            if ($value['MITM_MODEL'] === '1') {
                // Parsing job yang di pilih
                foreach ($value['SCRAP_DET'] as $keyJobDet => $valueJobDet) {
                    // Parsing Process Master (PROCESS_DET)
                    if ($valueJobDet['IS_RETURN'] == 1) {
                        if ($valueJobDet['QTY_SCRAP'] > 0) {
                            // Insert ke table Hist Master
                            $insertingScrap = scrapHist::create([
                                'DSGN_MAP_ID' => $valueJobDet['PIS2_WONO'],
                                'USERNAME' =>  $req->username,
                                'DEPT' =>  $req->dept,
                                'QTY' => intVal($valueJobDet['QTY_SCRAP']),
                                'ID_TRANS' => $idTrans,
                                'TYPE_TRANS' => $req->type,
                                'DOC_NO' => $valueJobDet['PIS2_WONO'],
                                'REASON' => isset($valueJobDet['REASON']) ? $valueJobDet['REASON'] : '',
                                'SCR_PROCD' => '',
                                'MDL_FLAG' => 1,
                                'ITEMNUM' => $valueJobDet['PIS2_MDLCD'],
                                'IS_RETURN' => 1
                            ]);

                            $data = $this->getPartFGReturn($valueJobDet['PIS2_MDLCD'], $valueJobDet['PIS2_WONO']);

                            foreach ($data as $keyComp => $valueComp) {
                                $hasilComp = [
                                    'RCVSCN_ITMCD' => $valueComp['SERRC_BOMPN'],
                                    'RCVSCN_LOTNO' => $valueComp['SERRC_LOTNO'],
                                    'RCVSCN_DONO' => $valueComp['RETFG_DOCNO'],
                                    'QTY_SCRAP' => $valueComp['SERRC_SERXQTY'] * intVal($valueJobDet['QTY_SCRAP']),
                                    'RCV_PRPRC' => $this->getLatestHargaFromBC($valueComp['SERRC_BOMPN'], $valueComp['SERRC_LOTNO'])['RCV_PRPRC']
                                ];
                                // // logger([$hasilComp, $idTrans, $req->username, $req->dept, $req->type, '', $value, $insertingScrap->id]);
                                $queueInsertDet = (new scrapDetailInsertsMaterial($hasilComp, $idTrans, $req->username, $req->dept, $req->type, '', $value, $insertingScrap->id));
                                dispatch($queueInsertDet)->onQueue('scrapReport');
                            }
                        }
                    } else {
                        foreach ($valueJobDet['PROCESS_DET'] as $keyProMstr => $valueProMstr) {
                            // Parsing Process Detail
                            foreach ($valueProMstr['DET'] as $keyProDet => $valueProDet) {

                                if (intval($valueProDet['QTY']) > 0) {
                                    $insertHeader = scrapHist::create([
                                        'DSGN_MAP_ID' => $valueProDet['id'],
                                        'USERNAME' => $req->username,
                                        'DEPT' => $req->dept,
                                        'QTY' => $valueProDet['QTY'],
                                        'QTY_SCRAP' => 0,
                                        'ID_TRANS' => $idTrans,
                                        'TYPE_TRANS' => $req->type,
                                        'DOC_NO' => $valueJobDet['PIS2_WONO'],
                                        'REASON' => isset($valueProDet['REASON']) ? $valueProDet['REASON'] : '',
                                        'SCR_PROCD' => $valueProDet['RPDSGN_PROCD'],
                                        'MDL_FLAG' => 1,
                                        'ITEMNUM' => $value['MITM_ITMCD'],
                                    ]);

                                    $idList[] = $insertHeader->id;

                                    // Insert Detail Transaction

                                    $findByProcess = $this->checkReverseProcess($this->getPsnListByJobAndProcd(
                                        $fetchProcessMaster,
                                        $valueJobDet['PIS2_WONO'],
                                        $valueProDet['RPDSGN_PRO_ID'],
                                        $value['MITM_ITMCD']
                                    ));

                                    $hasil[] = $valueProDet;
                                    // $queueInsertDet = (new );
                                    // logger([$findByProcess, $valueProDet['QTY'], $insertHeader->id, $value, $idTrans, $valueJobDet['PIS2_WONO'], 0]);
                                    scrapDetailInserts::dispatch($findByProcess, $valueProDet['QTY'], $insertHeader->id, $value, $idTrans, $valueJobDet['PIS2_WONO'], 0)->onQueue('scrapReport');
                                    $countJobs++;
                                }

                                if (isset($valueProDet['QTY_SI']) && intval($valueProDet['QTY_SI']) > 0) {
                                    $insertHeader = scrapHist::create([
                                        'DSGN_MAP_ID' => $valueProDet['id'],
                                        'USERNAME' => $req->username,
                                        'DEPT' => $req->dept,
                                        'QTY' => $valueProDet['QTY_SI'],
                                        'QTY_SCRAP' => 1,
                                        'ID_TRANS' => $idTrans,
                                        'TYPE_TRANS' => $req->type,
                                        'DOC_NO' => $valueJobDet['PIS2_WONO'],
                                        'REASON' => isset($valueProDet['REASON']) ? $valueProDet['REASON'] : '',
                                        'SCR_PROCD' => $valueProDet['RPDSGN_PROCD'],
                                        'MDL_FLAG' => 1,
                                        'ITEMNUM' => $value['MITM_ITMCD'],
                                    ]);

                                    $idList[] = $insertHeader->id;

                                    // Insert Detail Transaction

                                    $findByProcess = $this->checkReverseProcess($this->getPsnListByJobAndProcd(
                                        $fetchProcessMaster,
                                        $valueJobDet['PIS2_WONO'],
                                        $valueProDet['RPDSGN_PRO_ID'],
                                        $value['MITM_ITMCD']
                                    ));

                                    // Issue ITH
                                    $this->insertToITH(
                                        trim($value['MITM_ITMCD']),
                                        date('Y-m-d'),
                                        'OUT-SCR-FG',
                                        $idTrans,
                                        $valueProDet['QTY_SI'] * -1,
                                        'AFWH9SC',
                                        '',
                                        'SCR_RPT'
                                    );

                                    // logger(json_encode([
                                    //     'valueProDet' => $valueProDet,
                                    //     'findByProcess' => $findByProcess
                                    // ]));

                                    $hasil[] = $valueProDet;
                                    scrapDetailInserts::dispatch($findByProcess, $valueProDet['QTY'], $insertHeader->id, $value, $idTrans, $valueJobDet['PIS2_WONO'], 1)->onQueue('scrapReport');
                                    $countJobs++;
                                }
                            }
                        }
                    }
                }
            } else {
                foreach ($value['SCRAP_DET'] as $keyLotDet => $valueLotDet) {
                    if ($valueLotDet['QTY_SCRAP'] > 0) {
                        $cekRcvScnForPrice = $this->cekLatestHarga($valueLotDet['RCVSCN_ITMCD'], $valueLotDet['RCVSCN_LOTNO'], $valueLotDet['RCVSCN_DONO']);

                        // Insert ke table Hist Master
                        $insertingScrap = scrapHist::create([
                            'DSGN_MAP_ID' => $valueLotDet['KEY_LOT'],
                            'USERNAME' =>  $req->username,
                            'DEPT' =>  $req->dept,
                            'QTY' => intVal($valueLotDet['QTY_SCRAP']),
                            'QTY_SCRAP' => 1,
                            'ID_TRANS' => $idTrans,
                            'TYPE_TRANS' => $req->type,
                            'DOC_NO' => $valueLotDet['RCVSCN_DONO'],
                            'REASON' => isset($valueLotDet['REASON']) ? $valueLotDet['REASON'] : '',
                            'SCR_PROCD' => $valueLotDet['RCVSCN_LOTNO'],
                            'MDL_FLAG' => 0,
                            'ITEMNUM' => $valueLotDet['RCVSCN_ITMCD'],
                        ]);

                        // Issue ITH
                        $this->insertToITH(
                            trim($value['MITM_ITMCD']),
                            date('Y-m-d'),
                            'OUT-SCR-RM',
                            $idTrans,
                            intVal($valueLotDet['QTY_SCRAP']) * -1,
                            'ARWH9SC',
                            '',
                            'SCR_RPT'
                        );

                        $idList[] = $insertingScrap->id;

                        $queueInsertDet = (new scrapDetailInsertsMaterial($valueLotDet, $idTrans, $req->username, $req->dept, $req->type, $cekRcvScnForPrice, $value, $insertingScrap->id));
                        dispatch($queueInsertDet)->onQueue('scrapReport');
                        $countJobs++;
                    }
                }
            }
        }

        return array_merge($req->all(), ['id' => $idTrans, 'latest_id' => (count($idList) > 0 ? $idList[count($idList) - 1] : NULL)]);
    }

    public function cekAngkaKoma($data, $qty)
    {
        $hasil = [];
        foreach ($data as $key => $value) {
            $hasil[] = ['jumlah' => $qty * $value['PPSN2_QTPER'], 'process' => $value['PPSN1_PROCD']];
        }

        $cek = array_filter($hasil, function ($f) {
            return $this->is_decimal($f['jumlah']);
        });

        $proc = [];
        foreach ($cek as $key => $value) {
            $proc[] = $value['process'];
        }

        if (count($cek) > 0) {
            return [
                'status' => false,
                'process' => implode(",", $proc)
            ];
        } else {
            return [
                'status' => true,
                'process' => implode(",", $proc)
            ];
        }
    }

    public function sendingParamBCStock($item_num, $qty = 0, $doc = null, $lot = null, $bc = null, $id = '', $rev = false)
    {
        $endpoint = 'http://192.168.0.29:8081/api-report-custom/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item_num,
            'qty' => $qty,
            'doc' => $doc,
            'lot' => $lot,
            'bc' => $bc,
            'scrap' => true,
            'remark' => $id,
            'revise' => $rev
        ];

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content;
    }

    public function testRedis()
    {
        $redis = LRedis::connection();

        try {
            $redis = LRedis::connection();

            $redis->publish('message', json_encode([
                'app' => "scrap_report",
                'id' => "21/03/24/0001",
                'id_transaction' => 6184,
                'item' => "217013409",
                'item_desc' => "CG77MB",
                'doc' => "21-3A01-217013409",
                'message' => "This scrap report detail model is successfully inserted",
                'status' => "positive"
            ]));

            $allKeys = $redis->keys('*');

            return $allKeys;
        } catch (\Predis\Connection\ConnectionException $e) {
            return response('error connection redis');
        }

        // $redis = LRedis::connection();
        // $name = $redis;
        // return json_encode($name);
    }

    public function findRetFG(Request $req)
    {
        $getRet = RETFG::select(
            DB::raw('RETFG_DOCNO AS PIS2_WONO'),
            DB::raw('RETFG_ITMCD AS PIS2_MDLCD'),
            DB::raw('COALESCE(SUM(RETFG_QTY),0) - (SELECT COALESCE(SUM(QTY), 0) FROM PSI_RPCUST.dbo.RPSCRAP_HIST WHERE DOC_NO = RETFG_DOCNO AND ITEMNUM = RETFG_ITMCD) AS TOTAL_QTY'),
            DB::raw('1 AS IS_RETURN')
        )->where('RETFG_ITMCD', trim($req->item))
            ->groupBy(
                'RETFG_DOCNO',
                'RETFG_ITMCD'
            );

        if (isset($req->filter)) {
            $getRet->where('RETFG_DOCNO', 'LIKE', '%' . $req->filter . '%');
        }

        if (isset($req->pagination['sortBy']) && $req->pagination['sortBy'] !== 'desc') {
            $getRet->orderBy($req->pagination['sortBy'], $req->pagination['descending'] ? 'desc' : 'asc');
        }

        $parsednya = $getRet->get()->toArray();
        // return $parsednya;

        $hasil = [];
        foreach ($parsednya as $key => $value) {
            // $hasil[] = array_merge($value, ['get_component' => $this->getPartFGReturn($value['PIS2_MDLCD'], $value['PIS2_WONO'])]);
            if (count($this->getPartFGReturn($value['PIS2_MDLCD'], $value['PIS2_WONO'])) > 0) {
                $hasil[] = array_merge($value, ['get_component' => $this->getPartFGReturn($value['PIS2_MDLCD'], $value['PIS2_WONO'])]);
            }
        }

        // return $hasil;

        $parsedHasil = collect($hasil);

        if ($req->has('pagination')) {
            $rows = isset($req->pagination) ? $req->pagination['rowsPerPage'] : 5;
            $page = isset($req->pagination) ? $req->pagination['page'] : 1;

            if ($rows == 0) {
                // $cekall = $parsedHasil->count();
                return $parsedHasil->paginate(count($hasil));
            } else {
                return $parsedHasil->paginate($rows);
            }
        } else {
            return $hasil;
        }
    }

    public function getPartFGReturn($item, $doc)
    {
        $getRet = RETFG::select(
            'RETFG_DOCNO',
            'RETFG_ITMCD',
            'SERRC_BOMPN',
            'SERRC_BOMPNQTY',
            'SERRC_SER',
            'SERRC_SERX',
            'SERRC_SERXQTY',
            'SERRC_LOTNO'
        )
            ->join('SER_TBL', function ($f) {
                $f->on('SER_DOC', 'RETFG_DOCNO');
                $f->on('SER_ITMID', 'RETFG_ITMCD');
                $f->on('SER_PRDLINE', 'RETFG_LINE');
            })
            ->join('SERRC_TBL', function ($f) {
                $f->on('SERRC_SER', 'SER_ID');
            })
            ->where('RETFG_ITMCD', trim($item))
            ->where('RETFG_DOCNO', trim($doc))
            ->where('SERRC_BOMPN', '<>', '')
            ->where('SERRC_BOMPNQTY', '<>', '')
            ->where('SERRC_LOTNO', '<>', '')
            ->groupBy(
                'RETFG_DOCNO',
                'RETFG_ITMCD',
                'SERRC_BOMPN',
                'SERRC_BOMPNQTY',
                'SERRC_SER',
                'SERRC_SERX',
                'SERRC_SERXQTY',
                'SERRC_LOTNO'
            )
            ->get()
            ->toArray();

        return $getRet;
    }

    public function getLatestHargaFromBC($item, $lot = '')
    {
        $stocknya = StockExBC::where('RPSTOCK_ITMNUM', $item)
            ->orderBy('RPSTOCK_BCDATE', 'DESC')
            ->where('TOTAL_INC', '>', 0)
            ->first();

        $harga = RCVSCN::where('RCVSCN_ITMCD', $item)
            ->join('RCV_TBL', function ($f) {
                $f->on('RCVSCN_ITMCD', 'RCV_ITMCD');
                $f->on('RCVSCN_DONO', 'RCV_DONO');
            })
            ->where('RCV_BCNO', $stocknya['BCNUM_INC']);

        if (!empty($lot)) {
            $harga->where('RCVSCN_LOTNO', $lot);
        }

        return $harga->first();
    }

    public function insertToITH($item, $date, $form, $doc, $qty, $wh, $remark, $user)
    {
        ITHMaster::insert([
            'ITH_ITMCD' => $item,
            'ITH_DATE' => $date,
            'ITH_FORM' => $form,
            'ITH_DOC' => $doc,
            'ITH_QTY' => $qty,
            'ITH_WH' => $wh,
            'ITH_LOC' => '',
            'ITH_SER' => '',
            'ITH_REMARK' => $remark,
            'ITH_LINE' => '',
            'ITH_LUPDT' => date('Y-m-d H:i:s'),
            'ITH_USRID' => $user,
        ]);
    }

    public function processDataWithoutUpload(Request $req, $typeScrap)
    {
        $fileName = '';
        switch ($typeScrap) {
            case 'wip':
                $fileName = 'WIP_SCRAP_UPLOAD_' . date('Ymd') . '_' . date('his') . '.xlsx';
                Excel::store(new scrapReportUploadTemplate($req->data, $typeScrap), $fileName, 'public');
                // return 'public/storage/' . $fileName;
                return $this->executeUpload($req->user, $req->dept, $fileName, '/storage/');

                break;
            case 'pending':
                $fileName = 'PENDING_SCRAP_UPLOAD_' . date('Ymd') . '_' . date('his') . '.xlsx';
                Excel::store(new scrapReportUploadTemplate($req->data, $typeScrap), $fileName, 'public');
                // return 'public/storage/' . $fileName;
                return $this->executeUpload($req->user, $req->dept, $fileName, '/storage/');

                break;
            case 'material':
                $fileName = 'MATERIAL_SCRAP_UPLOAD_' . date('Ymd') . '_' . date('his') . '.xlsx';
                Excel::store(new scrapReportUploadTemplate($req->data, $typeScrap), $fileName, 'public');
                // return 'public/storage/' . $fileName;
                return $this->executeUpload($req->user, $req->dept, $fileName, '/storage/');

                break;
            default:
                $fileName = 'FGRETURN_SCRAP_UPLOAD_' . date('Ymd') . '_' . date('his') . '.xlsx';
                Excel::store(new scrapReportUploadTemplate($req->data, $typeScrap), $fileName, 'public');
                // return 'public/storage/' . $fileName;
                return $this->executeUpload($req->user, $req->dept, $fileName, '/storage/');

                break;
        }
    }

    public function getTemplateUpload(Request $req, $typeScrap)
    {
        switch ($typeScrap) {
            case 'wip':
                $fileName = 'WIP_SCRAP_UPLOAD_' . date('Ymd') . '_' . date('his') . '.xlsx';
                Excel::store(new scrapReportUploadTemplate($req->data, $typeScrap), $fileName, 'public');
                return 'public/storage/' . $fileName;

                break;
            case 'pending':
                $fileName = 'PENDING_SCRAP_UPLOAD_' . date('Ymd') . '_' . date('his') . '.xlsx';
                Excel::store(new scrapReportUploadTemplate($req->data, $typeScrap), $fileName, 'public');
                return 'public/storage/' . $fileName;

                break;
            case 'material':
                $fileName = 'MATERIAL_SCRAP_UPLOAD_' . date('Ymd') . '_' . date('his') . '.xlsx';
                Excel::store(new scrapReportUploadTemplate($req->data, $typeScrap), $fileName, 'public');
                return 'public/storage/' . $fileName;

                break;
            default:
                $fileName = 'FGRETURN_SCRAP_UPLOAD_' . date('Ymd') . '_' . date('his') . '.xlsx';
                Excel::store(new scrapReportUploadTemplate($req->data, $typeScrap), $fileName, 'public');
                return 'public/storage/' . $fileName;

                break;
        }
    }

    public function uploadTemplate(Request $req)
    {
        $nama_file = $req->file->hashName();

        $req->file->storeAs('/public/upload_scrap', $nama_file);

        return $this->executeUpload($req->users, $req->dept, $nama_file);
    }

    public function executeUpload($users, $dept, $nama_file, $public_path = '/storage/upload_scrap/')
    {
        try {
            $cek = scrapHist::whereBetween('created_at', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59'])->orderBy('created_at', 'desc')->withTrashed()->first();
            $idTrans = date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->ID_TRANS, -3) + 1));

            $importer = new uploadScrapHist($users, $dept, $idTrans);
            Excel::import($importer, public_path($public_path . $nama_file));

            if (!empty($importer->dataRet)) {
                return $importer->dataRet;
            }

            return 'Upload Sukses ' . $nama_file;
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();

            foreach ($failures as $failure) {
                $failure->row(); // row that went wrong
                $failure->attribute(); // either heading key (if using heading row concern) or column index
                $failure->errors(); // Actual error messages from Laravel validator
                $failure->values(); // The values of the row that has failed.
            }

            return response()->json($failure, 422);
        }
    }

    public function testRCV($item, $sj)
    {
        $headerData = MutasiStok::select(
            'RCV_BCTYPE',
            'RCV_BCNO',
            'RCV_BCDATE',
            'RCV_DONO',
            'RCV_INVDATE',
            'RCV_RPNO',
            'RCV_RPDATE',
            'RCV_SUPCD',
            'RCV_ITMCD',
            DB::raw('ROUND(RCV_PRPRC, 4, 1) as RCV_PRPRC'),
            'MITM_STKUOM',
            DB::raw('MSUP_SUPCR'),
            DB::raw('SUM(RCV_QTY) as RCV_QTY'),
            'RCV_RCVDATE',
            'RCV_DONO',
            'MITM_MODEL'
        )
        ->leftjoin('PSI_WMS.dbo.MITM_TBL', 'RCV_ITMCD', 'MITM_ITMCD')
        ->leftjoin('PSI_WMS.dbo.MSUP_TBL', 'RCV_SUPCD', 'MSUP_SUPCD');

        if (!empty($sj)) {
            $headerData->where('RCV_DONO', $sj);
        }

        if (!empty($item)) {
            $headerData->where('RCV_ITMCD', $item);
        }

        $getData = $headerData->groupBy(
            'RCV_BCTYPE',
            'RCV_BCNO',
            'RCV_BCDATE',
            'RCV_DONO',
            'RCV_INVDATE',
            'RCV_RPNO',
            'RCV_RPDATE',
            'RCV_SUPCD',
            'RCV_ITMCD',
            DB::raw('ROUND(RCV_PRPRC, 4, 1)'),
            'MITM_STKUOM',
            'MSUP_SUPCR',
            'RCV_RCVDATE',
            'RCV_DONO',
            'MITM_MODEL'
        )->get();

        $hasil = [];
        foreach ($getData as $key => $value) {
            if ($value['MITM_MODEL'] == '0') {
                $hasil[] = [
                    'item' => $value['RCV_ITMCD'],
                    'model' => $value['MITM_MODEL'],
                    'status' => 'masuk ke material'
                ];
            } elseif ($value['MITM_MODEL'] == '1') {
                $hasil[] = [
                    'item' => $value['RCV_ITMCD'],
                    'model' => $value['MITM_MODEL'],
                    'status' => 'masuk ke model'
                ];
            } elseif ($value['MITM_MODEL'] == '2') {
                $hasil[] = [
                    'item' => $value['RCV_ITMCD'],
                    'model' => $value['MITM_MODEL'],
                    'status' => 'masuk ke model'
                ];
            }
        }

        return $hasil;
    }

    public function getDetailComponentByID($id)
    {
        $data = scrapHistDet::where('SCR_HIST_ID', $id)->get()->toArray();

        return $data;
    }
}
