<?php

namespace App\Http\Controllers\WMS\REPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Exports\MutasiMasukExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Model\RPCUST\MutasiDokPabean;
use App\Helpers\CustomFunctionHelper;
use Illuminate\Support\Facades\Log;

class MutasiStockKeluarController extends Controller
{
    public function show(Request $request, $methode = null)
    {
        $data = $this->data($request->form, $methode);

        $hasil = [];
        foreach ($data->get()->toArray() as $key => $value) {
            $valnya = [
                'MITM_ITMD1' => $value['MITM_ITMD1'],
                'MCUS_CUSNM' => $value['MCUS_CUSNM'],
                'QTY' => (string)number_format($value['QTY'], 2, '.', ','),
                'RPBEA_ITMCOD' => $value['RPBEA_ITMCOD'],
                'RPBEA_JENBEA' => 'BC ' . $value['RPBEA_JENBEA'],
                'RPBEA_NUMBEA' => $value['RPBEA_NUMBEA'],
                'RPBEA_NUMPEN' => $value['RPBEA_NUMPEN'],
                'RPBEA_NUMSJL' => $value['RPBEA_NUMSJL'],
                'RPBEA_QTYJUM' => number_format($value['RPBEA_QTYJUM']),
                'RPBEA_TGLBEA' => date('d-m-Y', strtotime($value['RPBEA_TGLBEA'])),
                'RPBEA_TGLPEN' => date('d-m-Y', strtotime($value['RPBEA_TGLPEN'])),
                'RPBEA_TGLSJL' => date('d-m-Y', strtotime($value['RPBEA_TGLSJL'])),
                'RPBEA_UNITMS' => $value['RPBEA_UNITMS'],
                'RPBEA_VALAS' => $value['RPBEA_VALAS'],
                'no' => $key + 1
            ];

            $hasil[] = $valnya;
        }

        $filterData = collect($hasil);

        $rows = $request->has('pagination') ? $request->pagination['rowsPerPage'] : 20;
        $page = $request->has('pagination') ? $request->pagination['page'] : 1;

        if ($methode == 'table') {
            // file_put_contents('logs.txt', $filterData->toSql().json_encode($filterData->getBindings()).PHP_EOL , FILE_APPEND | LOCK_EX);
            if ($rows == 0) {
                $cekall = $filterData->count();
                return $filterData->paginate($cekall);
                return $filterData->paginate($cekall, ['*'], 'page', $page);
            } else {
                return $filterData->paginate($rows, $request['pagination']['rowsNumber'], $page);
                return $filterData->paginate($rows, ['*'], 'page', $page);
            }
        } elseif ($methode == 'data') {
            return $filterData->get();
        } else {
            return $this->ExportToExcell($request);
        }
    }

    public function data($filteropt, $methode = null)
    {
        $select = [
            'RPBEA_JENBEA',
            'RPBEA_NUMBEA',
            'RPBEA_TGLBEA',
            'RPBEA_NUMSJL',
            'RPBEA_TGLSJL',
            'RPBEA_NUMPEN',
            'RPBEA_TGLPEN',
            'MCUS_CUSNM',
            'RPBEA_ITMCOD',
            'MITM_ITMD1',
            'RPBEA_UNITMS',
            'RPBEA_VALAS',
            DB::raw('SUM(RPBEA_QTYTOT) AS RPBEA_QTYJUM'),
            DB::raw('RPBEA_QTYSAT as QTY')
        ];

        $getData = MutasiDokPabean::select($select)
            ->join('PSI_WMS.dbo.MCUS_TBL', 'MCUS_CUSCD', '=', 'RPBEA_CUSSUP')
            ->leftjoin('PSI_WMS.dbo.MITM_TBL', 'MITM_ITMCD', '=', 'RPBEA_ITMCOD')
            ->where('RPBEA_TYPE', 'OUT')
            ->where('RPBEA_QTYJUM', '>', 0)
            ->groupBy(
                'RPBEA_JENBEA',
                'RPBEA_NUMBEA',
                'RPBEA_TGLBEA',
                'RPBEA_NUMSJL',
                'RPBEA_TGLSJL',
                'RPBEA_NUMPEN',
                'RPBEA_TGLPEN',
                'MCUS_CUSNM',
                'RPBEA_ITMCOD',
                'MITM_ITMD1',
                'RPBEA_UNITMS',
                'RPBEA_VALAS',
                'RPBEA_QTYSAT'
            );

        if ($methode == 'all') {
            return $getData->get();
        } else {
            $hasil = CustomFunctionHelper::filtering($getData, $filteropt)->orderby('RPBEA_TGLBEA');

            return $hasil;
        }

        return $getData->orderby('RPBEA_TGLBEA');
    }

    public function ExportToExcell(Request $req)
    {
        $cekmap = array_map(function ($array) {
            return $array['RPBEA_TGLBEA'];
        }, $req->data);

        $datePeriod = [min($cekmap), max($cekmap)];

        if ($req->format == 'excel') {
            Excel::store(new MutasiMasukExport($req->data, $datePeriod, 'OUT'), 'MutasiKeluarExport.xls', 'public');

            return 'public/storage/MutasiKeluarExport.xls';
        } elseif ($req->format == 'pdf') {
            Excel::store(new MutasiMasukExport($req->data, $datePeriod, 'OUT'), 'MutasiKeluarExport.pdf', 'public', \Maatwebsite\Excel\Excel::DOMPDF);

            return 'public/storage/MutasiKeluarExport.pdf';
        }
    }
}
