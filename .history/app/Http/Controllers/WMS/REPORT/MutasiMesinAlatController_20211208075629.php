<?php

namespace App\Http\Controllers\WMS\REPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\RPCUST\MutasiMesin;
use App\Helpers\CustomFunctionHelper;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MutasiMesinAlatExport;
use Illuminate\Support\Facades\DB;

class MutasiMesinAlatController extends Controller
{
    public function show(Request $request, $methode = null)
    {
        $data = $this->data($request->form, $methode);

        $hasil = [];
        foreach ($data as $key => $value) {
            $hasil[] = array_merge($value, ['no' => $key + 1]);
        }

        $filterData = collect($hasil);

        $rows = $request->has('pagination') ? $request->pagination['rowsPerPage'] : 20;
        $page = $request->has('pagination') ? $request->pagination['page'] : 1;
        $rowsNum = $request->has('pagination') ? $request->pagination['rowsNumber'] : $filterData->count();

        // dd($filterData->all());

        if ($methode == 'table') {

            // file_put_contents('logs.txt', $filterData->toSql().json_encode($filterData->getBindings()).PHP_EOL , FILE_APPEND | LOCK_EX);
            if ($rows == 0) {
                $cekall = $filterData->count();
                return $filterData->paginate($cekall);
                return $filterData->paginate($cekall, ['*'], 'page', $page);
            } else {
                return $filterData->paginate($rows, $rowsNum, $page);
                return $filterData->paginate($rows, ['*'], 'page', $page);
            }
        } elseif ($methode == 'data') {
            return $filterData->all();
        } else {
            // $data = $filterData->get();
            return $this->ExportToExcell($request);
        }
    }

    public function data($filteropt, $methode = null)
    {
        $queryString = "SET NOCOUNT ON ;EXEC PSI_RPCUST.dbo.sp_mutasi_inv @mutasi = 'mesin'";

        foreach ($filteropt as $keyFilter => $valueFilter) {
            if ($valueFilter['field']['field'] === 'RPMCH_DATEIS' && !empty($valueFilter['val'])) {
                $queryString .= ", @date_from='".$valueFilter['val']."', @date_to='".$valueFilter['val2']."'";
            }

            if ($valueFilter['field']['field'] === 'RPMCH_ITMCOD' && !empty($valueFilter['val'])) {
                $queryString .= ", @item_num='".$valueFilter['val']."'";
            }
        }

        $result = array_map(function ($value) {
            return (array)$value;
        }, DB::select($queryString));

        $data = [];
        foreach ($result as $key => $value) {
            foreach ($value as $keyDet => $valueDet) {
                if (($keyDet === 'RPMCH_ITMCOD' || $keyDet === 'MITM_ITMD1' || $keyDet === 'RPMCH_UNITMS')) {
                    $data[$key][$keyDet] = $value[$keyDet];
                } else {
                    if ($value[$keyDet] == (int)$value[$keyDet]) {
                        $data[$key][$keyDet] = (int)$value[$keyDet];
                    } else {
                        $data[$key][$keyDet] = $value[$keyDet];
                    }
                }
            }
        }

        return collect($data);
    }

    public function ExportToExcell(Request $req)
    {
        $cekDate = array_filter($req->param, function ($f) {
            return $f['field']['field'] === 'RPMCH_DATEIS';
        });

        if (count($cekDate) > 0) {
            $datePeriod = [date('d M Y', strtotime($cekDate[0]['val'])), date('d M Y', strtotime($cekDate[0]['val2']))];
        } else {
            $datePeriod = ['', ''];
        }

        $data = $this->data($req->param);

        if ($req->format == 'excel') {
            Excel::store(new MutasiMesinAlatExport($data, $datePeriod, $req->format), 'MutasiMesinAlatExport.xls', 'public');

            return 'public/storage/MutasiMesinAlatExport.xls';
        } elseif ($req->format == 'pdf') {
            Excel::store(new MutasiMesinAlatExport($data, $datePeriod, $req->format), 'MutasiMesinAlatExport.pdf', 'public', \Maatwebsite\Excel\Excel::DOMPDF);

            return 'public/storage/MutasiMesinAlatExport.pdf';
        }
    }

    public function ConverttoLedger($arrcollection)
    {
        $parsing_item = [];
        $count_item = 0;
        foreach ($arrcollection as $key => $value) {
            if ($key !== 0 && $value['RPMCH_ITMCOD'] !== $arrcollection[$key - 1]['RPMCH_ITMCOD']) {
                $count_item++;
                $parsing_item[$count_item]['RPMCH_DATEIS'] = $value['RPMCH_DATEIS'];
            } else {
                $parsing_item[$count_item]['RPMCH_DATEIS'] = $arrcollection[$key]['RPMCH_DATEIS'];
            }

            $parsing_item[$count_item]['RPMCH_ITMCOD'] = $value['RPMCH_ITMCOD'];
            $parsing_item[$count_item]['MITM_ITMD1'] = $value['MITM_ITMD1'];
            $parsing_item[$count_item]['RPMCH_UNITMS'] = $value['RPMCH_UNITMS'];
        }

        $parsing_saldoawal = [];
        foreach ($parsing_item as $key_saldoawal => $value_saldoawal) {
            $saldoawal = MutasiMesin::select(DB::raw('SUM(RPMCH_QTYINC) as saldo'))
                ->where('RPMCH_ITMCOD', $value_saldoawal['RPMCH_ITMCOD'])
                ->where('RPMCH_DATEIS', '<=', $value_saldoawal['RPMCH_DATEIS'])
                ->first();
            $saldocurrent = MutasiMesin::select(
                    DB::raw('SUM(RPMCH_QTYINC) as saldoin'),
                    DB::raw('SUM(RPMCH_QTYOUT) as saldoout'),
                    DB::raw('SUM(RPMCH_QTYOPN) as saldoopn'),
                    DB::raw('SUM(RPMCH_QTYOPN) as saldoadj')
                )
                ->where('RPMCH_ITMCOD', $value_saldoawal['RPMCH_ITMCOD'])
                ->where('RPMCH_DATEIS', '>=', $value_saldoawal['RPMCH_DATEIS'])
                ->first();

            $parsing_saldoawal[$key_saldoawal] = array_merge(
                $value_saldoawal,
                ['SALDO_AWAL' => $saldoawal['saldo']],
                ['SALDO_MASUK' => $saldocurrent['saldoin']],
                ['SALDO_KELUAR' => $saldocurrent['saldoout']],
                ['SALDO_ADJ' => $saldocurrent['saldoadj']],
                ['SALDO_TOT' => ($saldocurrent['saldoin'] - $saldocurrent['saldoout']) + $saldocurrent['saldoadj']],
                ['SALDO_OPN' => $saldocurrent['saldoopn']],
                ['SALDO_SLS' => (($saldocurrent['saldoin'] - $saldocurrent['saldoout']) + $saldocurrent['saldoadj']) - $saldocurrent['saldoopn']]
            );
        }

        return $parsing_saldoawal;
    }
}
