<?php

namespace App\Http\Controllers\WMS\REPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\RPCUST\MutasiScrap;
use App\Helpers\CustomFunctionHelper;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MutasiScrapExport;
use Illuminate\Support\Facades\DB;

class MutasiScrapController extends Controller
{
    public function show(Request $request, $methode = null)
    {
        $data = $this->data($request->form, $methode);

        $hasil = [];
        foreach ($data->toArray() as $key => $value) {
            $hasil[] = array_merge($value, ['no' => $key + 1]);
        }

        $filterData = collect($hasil);

        $rows = $request->has('pagination') ? $request->pagination['rowsPerPage'] : 20;
        $page = $request->has('pagination') ? $request->pagination['page'] : 1;
        $rowsNum = $request->has('pagination') ? $request->pagination['rowsNumber'] : $filterData->count();

        // dd($filterData->all());

        if ($methode == 'table') {

            // file_put_contents('logs.txt', $filterData->toSql().json_encode($filterData->getBindings()).PHP_EOL , FILE_APPEND | LOCK_EX);
            if ($rows == 0) {
                $cekall = $filterData->count();
                return $filterData->paginate($cekall);
                return $filterData->paginate($cekall, ['*'], 'page', $page);
            } else {
                return $filterData->paginate($rows, $rowsNum, $page);
                return $filterData->paginate($rows, ['*'], 'page', $page);
            }
        } elseif ($methode == 'data') {
            return $filterData->all();
        } else {
            // $data = $filterData->get();
            return $this->ExportToExcell($request);
        }
    }

    public function data($filteropt, $methode = null)
    {
        $queryString = "SET NOCOUNT ON ;EXEC PSI_RPCUST.dbo.sp_mutasi_inv_test @mutasi = 'scrap'";

        foreach ($filteropt as $keyFilter => $valueFilter) {
            if ($valueFilter['field']['field'] === 'RPREJECT_DATEIS' && !empty($valueFilter['val'])) {
                $queryString .= ", @date_from='".$valueFilter['val']."', @date_to='".$valueFilter['val2']."'";
            }

            if ($valueFilter['field']['field'] === 'RPREJECT_ITMCOD' && !empty($valueFilter['val'])) {
                $queryString .= ", @item_num='".$valueFilter['val']."'";
            }
        }

        $result = array_map(function ($value) {
            return (array)$value;
        }, DB::select($queryString));

        $data = [];
        foreach ($result as $key => $value) {
            foreach ($value as $keyDet => $valueDet) {
                if (($keyDet === 'RPREJECT_ITMCOD' || $keyDet === 'MITM_ITMD1' || $keyDet === 'RPREJECT_UNITMS')) {
                    $data[$key][$keyDet] = $value[$keyDet];
                } elseif ($keyDet === 'SALDO_OPN_DATE') {
                    $data[$key][$keyDet] = date('d M Y', strtotime($value[$keyDet]));
                } else {
                    if ($value[$keyDet] == (int)$value[$keyDet]) {
                        $data[$key][$keyDet] = (int)$value[$keyDet];
                    } else {
                        $data[$key][$keyDet] = $value[$keyDet];
                    }
                }
            }
        }

        return collect($data);
    }

    public function data2($filteropt, $methode = null)
    {
        $select = [
            // 'id',
            'RPREJECT_DATEIS',
            'RPREJECT_ITMCOD',
            'MITM_ITMD1',
            'RPREJECT_KET',
            // 'RPREJECT_QTYADJ',
            // 'RPREJECT_QTYINC',
            // 'RPREJECT_QTYOPN',
            // 'RPREJECT_QTYOUT',
            // 'RPREJECT_QTYTOT',
            'RPREJECT_UNITMS',
        ];

        $getData = MutasiScrap::select(array_merge(
            $select,
            [
                DB::raw('SUM(RPREJECT_QTYADJ) AS RPREJECT_QTYADJ'),
                DB::raw('SUM(RPREJECT_QTYINC) AS RPREJECT_QTYINC'),
                DB::raw('SUM(RPREJECT_QTYOPN) AS RPREJECT_QTYOPN'),
                DB::raw('SUM(RPREJECT_QTYOUT) AS RPREJECT_QTYOUT'),
                DB::raw('SUM(RPREJECT_QTYTOT) AS RPREJECT_QTYTOT')
            ]
        ))->join('PSI_WMS.dbo.MITM_TBL', 'MITM_ITMCD', '=', 'RPREJECT_ITMCOD')
            ->groupBy($select);

        if ($methode == 'all') {
            return $getData->orderby('id');
        } else {
            $hasil = CustomFunctionHelper::filtering($getData, $filteropt)->orderby('RPREJECT_ITMCOD', 'asc');

            $hasilData = $hasil->select(
                'MITM_ITMD1',
                // 'RPREJECT_DATEIS',
                'RPREJECT_ITMCOD',
                'RPREJECT_UNITMS',
                DB::raw('SUM(RPREJECT_QTYADJ) AS SALDO_ADJ'),
                DB::raw('(
                    SELECT COALESCE(SUM(RPREJECT_QTYINC), 0) - COALESCE(SUM(RPREJECT_QTYOUT), 0)
                    FROM RPSAL_REJECT rr
                    WHERE rr.RPREJECT_ITMCOD = RPSAL_REJECT.RPREJECT_ITMCOD
                    AND rr.RPREJECT_DATEIS < DATEADD(DAY,1,EOMONTH(RPSAL_REJECT.RPREJECT_DATEIS,-1))
                ) AS SALDO_AWAL'),
                DB::raw('SUM(RPREJECT_QTYOUT) AS SALDO_KELUAR'),
                DB::raw('SUM(RPREJECT_QTYINC) AS SALDO_MASUK'),
                DB::raw('(
                    (
                        SELECT COALESCE(SUM(RPREJECT_QTYINC), 0) - COALESCE(SUM(RPREJECT_QTYOUT), 0)
                        FROM RPSAL_REJECT rr
                        WHERE rr.RPREJECT_ITMCOD = RPSAL_REJECT.RPREJECT_ITMCOD
                        AND rr.RPREJECT_DATEIS < DATEADD(DAY,1,EOMONTH(RPSAL_REJECT.RPREJECT_DATEIS,-1))
                    ) + SUM(RPREJECT_QTYINC)
                ) - SUM(RPREJECT_QTYOUT) AS SALDO_OPN'),
                DB::raw('0 AS SALDO_SLS'),
                DB::raw('
                    (
                        (
                            SELECT COALESCE(SUM(RPREJECT_QTYINC), 0) - COALESCE(SUM(RPREJECT_QTYOUT), 0)
                            FROM RPSAL_REJECT rr
                            WHERE rr.RPREJECT_ITMCOD = RPSAL_REJECT.RPREJECT_ITMCOD
                            AND rr.RPREJECT_DATEIS < DATEADD(DAY,1,EOMONTH(RPSAL_REJECT.RPREJECT_DATEIS,-1))
                        ) + SUM(RPREJECT_QTYINC)
                    ) - SUM(RPREJECT_QTYOUT) AS SALDO_TOT'),
            )
                ->groupBy('RPREJECT_ITMCOD')
                ->get();

            $hasil = [];
            foreach ($hasilData as $key => $value) {
                $hasil[trim($value['RPREJECT_ITMCOD'])][$key] = [
                    'MITM_ITMD1' => $value['MITM_ITMD1'],
                    'RPREJECT_ITMCOD' => $value['RPREJECT_ITMCOD'],
                    'RPREJECT_UNITMS' => $value['RPREJECT_UNITMS'],
                    'SALDO_ADJ' => $value['SALDO_ADJ'],
                    'SALDO_AWAL' => $value['SALDO_AWAL'],
                    'SALDO_KELUAR' => $value['SALDO_KELUAR'],
                    'SALDO_MASUK' => $value['SALDO_MASUK'],
                    'SALDO_OPN' => $value['SALDO_OPN'],
                    'SALDO_SLS' => $value['SALDO_SLS'],
                    'SALDO_TOT' => $value['SALDO_TOT'],
                ];
            }

            // return collect(array_values($hasil));

            $hasilParse = [];
            foreach ($hasil as $key => $value) {
                $hasilParse[$key]['SALDO_ADJ'] = 0;
                $hasilParse[$key]['SALDO_AWAL'] = 0;
                $hasilParse[$key]['SALDO_KELUAR'] = 0;
                $hasilParse[$key]['SALDO_MASUK'] = 0;
                $hasilParse[$key]['SALDO_OPN'] = 0;
                $hasilParse[$key]['SALDO_SLS'] = 0;
                $hasilParse[$key]['SALDO_TOT'] = 0;
                foreach ($value as $keyDet => $valueDet) {
                    $hasilParse[$key]['MITM_ITMD1'] = $valueDet['MITM_ITMD1'];
                    $hasilParse[$key]['RPREJECT_ITMCOD'] = $valueDet['RPREJECT_ITMCOD'];
                    $hasilParse[$key]['RPREJECT_UNITMS'] = $valueDet['RPREJECT_UNITMS'];
                    $hasilParse[$key]['SALDO_ADJ'] += $valueDet['SALDO_ADJ'];
                    $hasilParse[$key]['SALDO_AWAL'] = (int)$valueDet['SALDO_AWAL'];
                    $hasilParse[$key]['SALDO_KELUAR'] += $valueDet['SALDO_KELUAR'];
                    $hasilParse[$key]['SALDO_MASUK'] += $valueDet['SALDO_MASUK'];
                    $hasilParse[$key]['SALDO_OPN'] += $valueDet['SALDO_OPN'];
                    $hasilParse[$key]['SALDO_SLS'] += $valueDet['SALDO_SLS'];
                    $hasilParse[$key]['SALDO_TOT'] += $valueDet['SALDO_TOT'];
                }

                $hasilParse[$key]['SALDO_OPN'] = ($hasilParse[$key]['SALDO_AWAL'] + $hasilParse[$key]['SALDO_MASUK']) - $hasilParse[$key]['SALDO_KELUAR'];
                $hasilParse[$key]['SALDO_TOT'] = ($hasilParse[$key]['SALDO_AWAL'] + $hasilParse[$key]['SALDO_MASUK']) - $hasilParse[$key]['SALDO_KELUAR'];
            }

            return collect(array_values($hasilParse));
        }

        return $getData->orderby('id');
    }

    public function ExportToExcell(Request $req)
    {
        $cekDate = array_filter($req->param, function ($f) {
            return $f['field']['field'] === 'RPREJECT_DATEIS';
        });

        if (count($cekDate) > 0) {
            $datePeriod = [date('d M Y', strtotime($cekDate[0]['val'])), date('d M Y', strtotime($cekDate[0]['val2']))];
        } else {
            $datePeriod = ['', ''];
        }

        $data = $this->data($req->param);

        if ($req->format == 'excel') {
            Excel::store(new MutasiScrapExport($data, $datePeriod, $req->format), 'MutasiScrapExport.xls', 'public');

            return 'public/storage/MutasiScrapExport.xls';
        } elseif ($req->format == 'pdf') {
            Excel::store(new MutasiScrapExport($data, $datePeriod, $req->format), 'MutasiScrapExport.pdf', 'public', \Maatwebsite\Excel\Excel::DOMPDF);

            return 'public/storage/MutasiScrapExport.pdf';
        }
    }

    public function ConverttoLedger($arrcollection)
    {
        $parsing_item = [];
        $count_item = 0;
        $onlydate = [];

        if (count($arrcollection) > 0) {

            foreach ($arrcollection as $key => $value) {
                if ($key !== 0 && $value['RPREJECT_ITMCOD'] !== $arrcollection[$key - 1]['RPREJECT_ITMCOD']) {
                    $count_item++;
                    $parsing_item[$count_item]['RPREJECT_DATEIS'] = $value['RPREJECT_DATEIS'];
                } else {
                    $parsing_item[$count_item]['RPREJECT_DATEIS'] = $arrcollection[$key]['RPREJECT_DATEIS'];
                }

                $parsing_item[$count_item]['RPREJECT_ITMCOD'] = $value['RPREJECT_ITMCOD'];
                $parsing_item[$count_item]['MITM_ITMD1'] = $value['MITM_ITMD1'];
                $parsing_item[$count_item]['RPREJECT_UNITMS'] = $value['RPREJECT_UNITMS'];

                $onlydate[] =  $value['RPREJECT_DATEIS'];
            }

            $ldate = max(array_map('strtotime', $onlydate));

            logger($onlydate);

            $parsing_saldoawal = [];
            foreach ($parsing_item as $key_saldoawal => $value_saldoawal) {
                $saldoawal = MutasiScrap::select(DB::raw('COALESCE(SUM(RPREJECT_QTYINC),0) as saldo'))
                    ->where('RPREJECT_ITMCOD', $value_saldoawal['RPREJECT_ITMCOD'])
                    ->where('RPREJECT_DATEIS', '<=', $value_saldoawal['RPREJECT_DATEIS'])
                    ->first();
                $saldocurrent = MutasiScrap::select(
                    DB::raw('COALESCE(SUM(RPREJECT_QTYINC),0) as saldoin'),
                    DB::raw('COALESCE(SUM(RPREJECT_QTYOUT),0) as saldoout'),
                    DB::raw('COALESCE(SUM(RPREJECT_QTYOPN),0) as saldoopn'),
                    DB::raw('COALESCE(SUM(RPREJECT_QTYOPN),0) as saldoadj')
                )
                    ->where('RPREJECT_ITMCOD', $value_saldoawal['RPREJECT_ITMCOD'])
                    // ->where('RPREJECT_DATEIS', '>=', $value_saldoawal['RPREJECT_DATEIS'])
                    ->whereBetween('RPREJECT_DATEIS', [$value_saldoawal['RPREJECT_DATEIS'], date('Y-m-d', $ldate)])
                    ->first();

                $parsing_saldoawal[$key_saldoawal] = array_merge(
                    $value_saldoawal,
                    ['SALDO_AWAL' => $saldoawal['saldo']],
                    ['SALDO_MASUK' => $saldocurrent['saldoin']],
                    ['SALDO_KELUAR' => $saldocurrent['saldoout']],
                    ['SALDO_ADJ' => $saldocurrent['saldoadj']],
                    ['SALDO_TOT' => ($saldocurrent['saldoin'] - $saldocurrent['saldoout']) + $saldocurrent['saldoadj']],
                    ['SALDO_OPN' => $saldocurrent['saldoopn']],
                    ['SALDO_SLS' => (($saldocurrent['saldoin'] - $saldocurrent['saldoout']) + $saldocurrent['saldoadj']) - $saldocurrent['saldoopn']]
                );
            }

            logger($parsing_saldoawal);

            return $parsing_saldoawal;
        } else {
            return $arrcollection;
        }
    }
}
