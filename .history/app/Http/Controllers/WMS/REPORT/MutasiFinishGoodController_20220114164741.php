<?php

namespace App\Http\Controllers\WMS\REPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\RPCUST\MutasiFinishGood;
use App\Helpers\CustomFunctionHelper;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MutasiFinishGoodExport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MutasiFinishGoodController extends Controller
{
    public function show(Request $request, $methode = null)
    {
        $data = $this->data($request->form, $methode, $request->header('userinput'));

        $hasil = [];
        foreach ($data as $key => $value) {
            $hasil[] = array_merge($value, ['no' => $key + 1]);
        }

        $filterData = collect($hasil);

        $rows = $request->has('pagination') ? $request['pagination']['rowsPerPage'] : 20;
        $page = $request->has('pagination') ? $request['pagination']['page'] : 1;
        $rowsNum = $request->has('pagination') ? $request->pagination['rowsNumber'] : $filterData->count();

        if ($methode == 'table') {
            // file_put_contents('logs.txt', $filterData->toSql().json_encode($filterData->getBindings()).PHP_EOL , FILE_APPEND | LOCK_EX);
            if ($rows == 0) {
                $cekall = $filterData->count();
                return $filterData->paginate($cekall);
                return $filterData->paginate($cekall, ['*'], 'page', $page);
            } else {
                // Log::info($filterData->paginate($rows));
                return $filterData->paginate($rows, $rowsNum, $page);
                return $filterData->paginate($rows, ['*'], 'page', $page);
            }
        } elseif ($methode == 'data') {
            return $filterData->all();
        } else {
            // $data = $filterData->get();
            return $this->ExportToExcell($request);
        }
    }

    public function data($filteropt, $methode = null, $username = '')
    {
        $queryString = "SET NOCOUNT ON ;EXEC PSI_RPCUST.dbo.sp_mutasi_inv_test @mutasi = 'finish_good'";

        foreach ($filteropt as $keyFilter => $valueFilter) {
            if ($valueFilter['field']['field'] === 'RPGOOD_DATEIS' && !empty($valueFilter['val'])) {
                $queryString .= ", @date_from='".$valueFilter['val']."', @date_to='".$valueFilter['val2']."'";
            }

            if ($valueFilter['field']['field'] === 'RPGOOD_ITMCOD' && !empty($valueFilter['val'])) {
                $queryString .= ", @item_num='".$valueFilter['val']."'";
            }
        }

        // if (empty($username) || $username !== 'smt') {
        //     $queryString .= ", @minus_prot = 1";
        // } else {
        //     $queryString .= ", @minus_prot = 0";
        // }

        $result = array_map(function ($value) {
            return (array)$value;
        }, DB::select($queryString));

        $data = [];
        foreach ($result as $key => $value) {
            foreach ($value as $keyDet => $valueDet) {
                if (($keyDet === 'RPGOOD_ITMCOD' || $keyDet === 'MITM_ITMD1' || $keyDet === 'RPGOOD_UNITMS')) {
                    $data[$key][$keyDet] = $value[$keyDet];
                } elseif ($keyDet === 'SALDO_OPN_DATE') {
                    $data[$key][$keyDet] = date('d M Y', strtotime($value[$keyDet]));
                } else {
                    if ($value[$keyDet] == (int)$value[$keyDet]) {
                        $data[$key][$keyDet] = (int)$value[$keyDet];
                    } else {
                        $data[$key][$keyDet] = $value[$keyDet];
                    }
                }
            }
        }

        return collect($data);
    }

    public function ExportToExcell(Request $req)
    {
        $cekDate = array_filter($req->param, function ($f) {
            return $f['field']['field'] === 'RPGOOD_DATEIS';
        });

        if (count($cekDate) > 0) {
            $datePeriod = [date('d M Y', strtotime($cekDate[0]['val'])), date('d M Y', strtotime($cekDate[0]['val2']))];
        } else {
            $datePeriod = ['', ''];
        }

        $data = $this->data($req->param);

        if ($req->format == 'excel') {
            Excel::store(new MutasiFinishGoodExport($data, $datePeriod, $req->format), 'MutasiFinishGoodExport.xls', 'public');

            return 'public/storage/MutasiFinishGoodExport.xls';
        } elseif ($req->format == 'pdf') {
            Excel::store(new MutasiFinishGoodExport($data, $datePeriod, $req->format), 'MutasiFinishGoodExport.pdf', 'public', \Maatwebsite\Excel\Excel::DOMPDF);

            return 'public/storage/MutasiFinishGoodExport.pdf';
        }
    }

    public function ConverttoLedger($arrcollection)
    {
        $parsing_item = [];
        $count_item = 0;
        $onlydate = [];

        foreach ($arrcollection as $key => $value) {
            if ($key !== 0 && $value['RPGOOD_ITMCOD'] !== $arrcollection[$key - 1]['RPGOOD_ITMCOD']) {
                $count_item++;
                $parsing_item[$count_item]['RPGOOD_DATEIS'] = $value['RPGOOD_DATEIS'];
            } else {
                $parsing_item[$count_item]['RPGOOD_DATEIS'] = $arrcollection[$key]['RPGOOD_DATEIS'];
            }

            $parsing_item[$count_item]['RPGOOD_ITMCOD'] = $value['RPGOOD_ITMCOD'];
            $parsing_item[$count_item]['MITM_ITMD1'] = $value['MITM_ITMD1'];
            $parsing_item[$count_item]['RPGOOD_UNITMS'] = $value['RPGOOD_UNITMS'];

            $onlydate[] =  $value['RPGOOD_DATEIS'];
        }

        $fdate = min(array_map('strtotime', $onlydate));
        $ldate = max(array_map('strtotime', $onlydate));

        $parsing_saldototal = [];
        foreach ($parsing_item as $key_saldoawal => $value_saldoawal) {
            $saldoawal = MutasiFinishGood::select(DB::raw('COALESCE(SUM(RPGOOD_QTYINC),0) as saldo'))
                ->where('RPGOOD_ITMCOD', $value_saldoawal['RPGOOD_ITMCOD'])
                ->where('RPGOOD_DATEIS', '<=', $value_saldoawal['RPGOOD_DATEIS'])
                ->first();
            $saldocurrent = MutasiFinishGood::select(
                DB::raw('SUM(RPGOOD_QTYINC) as saldoin'),
                DB::raw('SUM(RPGOOD_QTYOUT) as saldoout'),
                DB::raw('SUM(RPGOOD_QTYOPN) as saldoopn'),
                DB::raw('SUM(RPGOOD_QTYOPN) as saldoadj')
            )
                ->where('RPGOOD_ITMCOD', $value_saldoawal['RPGOOD_ITMCOD'])
                ->whereBetween('RPGOOD_DATEIS', [$value_saldoawal['RPGOOD_DATEIS'], date('Y-m-d', $ldate)])
                ->first();

            $parsing_saldototal[$key_saldoawal] = array_merge(
                $value_saldoawal,
                ['SALDO_AWAL' => $saldoawal['saldo']],
                ['SALDO_MASUK' => $saldocurrent['saldoin']],
                ['SALDO_KELUAR' => $saldocurrent['saldoout']],
                ['SALDO_ADJ' => $saldocurrent['saldoadj']],
                ['SALDO_TOT' => ($saldocurrent['saldoin'] - $saldocurrent['saldoout']) + $saldocurrent['saldoadj']],
                ['SALDO_OPN' => (($saldoawal['saldo'] + $saldocurrent['saldoin']) - $saldocurrent['saldoout']) + $saldocurrent['saldoadj']],
                // ['SALDO_OPN' => $saldocurrent['saldoopn']],
                ['SALDO_SLS' => (($saldocurrent['saldoin'] - $saldocurrent['saldoout']) + $saldocurrent['saldoadj']) - $saldocurrent['saldoopn']]
            );
        }

        return $parsing_saldototal;
    }
}
