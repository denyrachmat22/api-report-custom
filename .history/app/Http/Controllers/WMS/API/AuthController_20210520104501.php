<?php

namespace App\Http\Controllers\WMS\API;

date_default_timezone_set("Asia/Jakarta");

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\WMS\AuthorizationRequest;
use App\Model\WMS\API\UserMaster;
use Illuminate\Support\Facades\DB;
use App\Model\WMS\API\RoleMaster;
use App\Http\Requests\WMS\RegisterRequest;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(AuthorizationRequest $req, $app = null)
    {
        $select = [
            'MSTEMP_ID',
            'MSTEMP_FNM',
            'MSTEMP_LNM',
            'MSTEMP_NIK',
            'MSTEMP_GRP',
            'MSTEMP_ACTSTS',
            'MSTEMP_STS'
        ];

        if ($app == 'mobilewms') {
            $collection = UserMaster::select($select)->where(DB::raw('LEFT(MSTEMP_GRP,3)'), 'MBL');
        } else {
            $collection = UserMaster::select($select)->where(DB::raw('LEFT(MSTEMP_GRP,6)'), 'RPCUST');
        }

        $cekuser = $collection->with(['menu' => function ($q) {
            $q->where('MENU_PRNT', '0');
            $q->with(['child' => function ($qchild) {
                $qchild->wherehas('role');
                $qchild->with(['role' => function ($qDet) {
                    $qDet->with('user');
                    $qDet->has('user');
                }]);
            }]);
            $q->orderBy('A
            AA
            AAA
            AAB
            AAC
            AAD
            AB
            AE
            AF
            AH
            AI
            AJ
            AK
            AKA
            AKB
            AZ
            AZA
            AZB
            AZC
            B
            BA
            BAA
            BAB
            BAC
            BAE
            BAEA
            BAEB
            BAEC
            BAF
            BAG
            BB
            BBA
            BBB
            BBC
            BBD
            BBE
            BC
            BCA
            BCB
            BCC
            BCD
            BD
            BDA
            BDB
            BE
            BEA
            BEB
            BEC
            BED
            BEE
            BEF
            BF
            BFA
            BFB
            BFC
            BFD
            BG
            BGA
            BGB
            BH
            BHA
            BHB
            BI
            BIA
            BIB
            BIC
            BID
            BIE
            BIG
            BIH
            BII
            BIJ
            BIK
            BIL
            BIM
            BM
            BN
            BNA
            BNB
            BNC
            BND
            BNE
            BNF
            BO
            BOA
            BOB
            BOC
            BOD
            BP
            BQ
            BR
            BS
            BSA
            BSB
            BSC
            BT
            BU
            BUA
            BUB
            BUC
            BV
            C
            CA
            CB
            CBA
            CBB
            CC
            CCA
            CCB
            CCC
            CCD
            CCDA
            CCDB
            CCE
            CCEA
            CCEB
            CCF
            CCFA
            CCFB
            CCFC
            CCG
            CCH
            CD
            CDA
            CDAA
            CDAB
            CDB
            CE
            CF
            CG
            CI
            CIA
            CIB
            CJ
            MBL_A_INC
            MBL_A_INC_A
            MBL_A_INC_B
            MBL_B_OUT
            MBL_B_OUT_A
            MBL_B_OUT_B
            MBL_B_OUT_C
            MBL_C_DLV
            MBL_C_DLV_A
            MBL_D_TRANS
            MBL_D_TRANS_A
            MBL_E_SET
            MBL_E_SET_A
            MBL_E_SET_B
            MBL_E_SET_C
            RPCUST_A_RPT
            RPCUST_A_RPT_A
            RPCUST_A_RPT_B
            RPCUST_A_RPT_C
            RPCUST_A_RPT_D
            RPCUST_A_RPT_E
            RPCUST_A_RPT_F
            RPCUST_C
            RPCUST_B_SCR
            RPCUST_B_SCR_A
            RPCUST_B_SCR_B
            RPCUST_B_TRX
            RPCUST_B_TRX_A
            RPCUST_B_TRX_B
            RPCUST_sad
            RPCUST_Testerl
            RPCUST_Z_SET
            RPCUST_Z_SET_A
            RPCUST_Z_SET_B
            RPCUST_Z_SET_C
            X
            XA
            XB
            XC');
        }])->where('MSTEMP_ID', $req->username)->where('MSTEMP_PW', hash('sha256', $req->password))->first();

        // return $cekuser;
        $getdata = [];

        if (empty($cekuser)) {
            return response()->json([
                'message' => "The given data was invalid.",
                'errors' => [
                    'username' => ['Password / username salah!']
                ]
            ], 422);
        }

        foreach ($cekuser->menu as $key => $value) {
            $getdata['parent'][$key]['MENU_ID'] = $value['MENU_ID'];
            $getdata['parent'][$key]['MENU_NAME'] = $value['MENU_NAME'];
            $getdata['parent'][$key]['MENU_DSCRPTN'] = $value['MENU_DSCRPTN'];
            $getdata['parent'][$key]['MENU_URL'] = $value['MENU_URL'];
            $getdata['parent'][$key]['MENU_ICON'] = $value['MENU_ICON'];

            if (count($value['child']) > 0) {
                foreach ($value['child'] as $keyDet => $valueDet) {
                    if (!empty($valueDet['role'])) {
                        $getdata['parent'][$key]['child'][$keyDet]['MENU_ID'] = $valueDet['MENU_ID'];
                        $getdata['parent'][$key]['child'][$keyDet]['MENU_NAME'] = $valueDet['MENU_NAME'];
                        $getdata['parent'][$key]['child'][$keyDet]['MENU_DSCRPTN'] = $valueDet['MENU_DSCRPTN'];
                        $getdata['parent'][$key]['child'][$keyDet]['MENU_URL'] = $valueDet['MENU_URL'];
                        $getdata['parent'][$key]['child'][$keyDet]['MENU_ICON'] = $valueDet['MENU_ICON'];
                    }
                }
            }
        }

        if ($cekuser->MSTEMP_ACTSTS == 0) {
            return response()->json([
                'message' => "The given data was invalid.",
                'errors' => [
                    'username' => ['User ini belum di aktivasi!']
                ]
            ], 422);
        } elseif ($cekuser->MSTEMP_STS == 0) {
            return response()->json([
                'message' => "The given data was invalid.",
                'errors' => [
                    'username' => ['User ini tidak aktif!']
                ]
            ], 422);
        } else {
            return response()->json([
                'data' => $getdata,
                'auth' => $collection->where('MSTEMP_ID', $req->username)->first(),
                'message' => "Login Success!",
                'key' => base64_encode(date('d-m-y h:i:s'))
            ]);
        }
    }

    public function getuser($met = 'all')
    {
        if ($met == 'all') {
            return UserMaster::get();
        } elseif ($met == 'custom') {
            return UserMaster::where(DB::raw('LEFT(MSTEMP_GRP, 6)'), 'RPCUST')->orWhere('MSTEMP_GRP', NULL)->get();
        } else {
            return UserMaster::where(DB::raw('LEFT(MSTEMP_GRP, 3)'), 'MBL')->get();
        }
    }

    public function getrole()
    {
        return RoleMaster::select('EMPACCESS_GRPID', 'EMPACCESS_MENUID')->with('menu')->groupBy('EMPACCESS_GRPID')->get();
    }

    public function register(RegisterRequest $req)
    {
        UserMaster::insert([
            'MSTEMP_ID' => $req->username,
            'MSTEMP_FNM' => $req->fname,
            'MSTEMP_LNM' => $req->lname,
            'MSTEMP_NIK' => $req->nik,
            'MSTEMP_PW' => hash('sha256', $req['password']),
        ]);
    }

    public function updateuser(Request $req, $id)
    {
        if ($req->has('fname')) {
            $array = ['MSTEMP_FNM' => $req->fname];
        } elseif ($req->has('lname')) {
            $array = ['MSTEMP_LNM' => $req->lname];
        } elseif ($req->has('nik')) {
            $array = ['MSTEMP_NIK' => $req->nik];
        } elseif ($req->has('role')) {
            $array = ['MSTEMP_GRP' => $req->role];
        } elseif ($req->has('status')) {
            $array = ['MSTEMP_ACTSTS' => $req->status, 'MSTEMP_STS' => $req->status];
        }

        return UserMaster::where('MSTEMP_ID', $id)->update($array);
    }

    public function delete($id)
    {
        return UserMaster::where('MSTEMP_ID', $id)->delete();
    }
}
