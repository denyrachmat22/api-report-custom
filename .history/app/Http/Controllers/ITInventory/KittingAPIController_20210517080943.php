<?php

namespace App\Http\Controllers\ITInventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\WMS\API\SPLSCN;

use App\Jobs\ITInventory\BahanBakuStock;

class KittingAPIController extends Controller
{
    public function updateOnKitting($req)
    {
        $insertJob = (new BahanBakuStock($req));

        dispatch($insertJob)->onQueue('BahanBakuStock');

        return 'Request Mutasi Pabean Masuk Queued';
    }

    public function KittingAPI($item, $psn = '', $cat = '', $line = '', $feeder = '')
    {
        $getSPLSCN = SPLSCN::where('SPLSCN_ITMCD', $item);

        if (!empty($psn)) {
            $getSPLSCN->where('SPLSCN_DOC', $psn);
        }

        if (!empty($cat)) {
            $getSPLSCN->where('SPLSCN_CAT', $cat);
        }

        if (!empty($line)) {
            $getSPLSCN->where('SPLSCN_LINE', $line);
        }

        if (!empty($feeder)) {
            $getSPLSCN->where('SPLSCN_FEDR', $feeder);
        }

        $getSPLSCN
        ->where('SPLSCN_SAVED', 1)
        ->get();

        $sendQueue = [];
        foreach ($getSPLSCN as $key => $value) {
            $parseValue = [
                'RPRAW_ITMCOD' => $value[''],
                'RPRAW_UNITMS' => $value[''],
                'RPRAW_DATEIS' => $value[''],
                'RPRAW_QTYINC' => $value[''],
                'RPRAW_QTYOUT' => $value[''],
                'RPRAW_QTYADJ' => $value[''],
                'RPRAW_QTYTOT' => $value[''],
                'RPRAW_QTYOPN' => $value[''],
                'RPRAW_KET' => $value[''],
                'RPRAW_REF' => $value['']
            ];

            $sendQueue[] = $this->updateOnKitting($value);
        }

        return $sendQueue;
    }

    public function resendKittingAll()
    {
        $getSPLSCN = SPLSCN::get();

        $sendQueue = [];
        foreach ($getSPLSCN as $key => $value) {
            $sendQueue[] = $this->updateOnKitting($value);
        }

        return 'All kitting queued';
    }
}
