<?php

namespace App\Http\Controllers\ITInventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\WMS\API\SERD2TBL;
use App\Model\MASTER\ITHMaster;

use App\Jobs\ITInventory\WIPStock;
use App\Jobs\ITInventory\FinishGoodStock;

class IncomingFGAPIController extends Controller
{
    public function updateOnFGInc($reqFG, $reqWIP)
    {
        $insertJobBahanBaku = (new FinishGoodStock($reqFG));

        dispatch($insertJobBahanBaku)->onQueue('FGStock');

        $insertJobWIP = (new WIPStock($reqWIP));

        dispatch($insertJobWIP)->onQueue('WIPStock');

        return 'Request Outgoing Raw & Incoming WIP Queued';
    }

    public function incFGAPI($sernum)
    {
        $getFG = ITHMaster::where('ITH_SER', $sernum)
            ->where('ITH_WH', 'AFWH3')
            ->where('ITH_FORM', 'INC-WH-FG')
            ->where('ITH_EXPORTED', 1)
            ->get();

        $getSerialDet = SERD2TBL::where('SERD2_SER', $sernum)->get();

        foreach ($getFG as $key => $value) {
            $parseValueFG = [
                'RPGOOD_UNITMS' => $value['MITM_STKUOM'],
                'RPGOOD_QTYTOT' => $value['ITH_QTY'],
                'RPGOOD_QTYOUT' => $value['RETSCN_LUPDT'],
                'RPGOOD_QTYOPN' => (int)$value['RETSCN_QTY'],
                'RPGOOD_QTYINC' => 0,
                'RPGOOD_QTYADJ' => 0,
                'RPGOOD_KET' => (int)$value['RETSCN_QTY'],
                'RPGOOD_ITMCOD' => 0,
                'RPGOOD_DATEIS' => 'INC',
                'RPGOOD_REF' => trim($value['RETSCN_DOC']).'||'.trim($value['RETSCN_CAT']).'||'.trim($value['RETSCN_LINE']).'||'.trim($value['RETSCN_FEDR'].'||'.$value['RETSCN_ORDERNO'].'||'.$value['RETSCN_LOT'])
            ];
        }
        
        return ['fg' => $getFG, 'det' => $getSerialDet];
    }
}
