<?php

namespace App\Http\Controllers\ITInventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\WMS\API\SERD2TBL;
use App\Model\MASTER\ITHMaster;

use App\Jobs\ITInventory\WIPStock;
use App\Jobs\ITInventory\FinishGoodStock;

class IncomingFGAPIController extends Controller
{
    public function updateOnFGInc($reqFG, $reqWIP)
    {
        $insertJobBahanBaku = (new FinishGoodStock($reqFG));

        dispatch($insertJobBahanBaku)->onQueue('FGStock');

        $insertJobWIP = (new WIPStock($reqWIP));

        dispatch($insertJobWIP)->onQueue('WIPStock');

        return 'Request Outgoing Raw & Incoming WIP Queued';
    }

    public function incFGAPI($sernum)
    {
        $getFG = ITHMaster::where('ITH_SER', $sernum)
            ->where('ITH_WH', 'AFWH3')
            ->where('ITH_FORM', 'INC-WH-FG')
            ->get();

        $getSerialDet = SERD2TBL::where('SERD2_SER', $sernum)->get();

        foreach ($getFG as $key => $value) {
            $parseValueFG = [
                'RPRAW_ITMCOD' => $value['RETSCN_ITMCD'],
                'RPRAW_UNITMS' => $value['MITM_STKUOM'],
                'RPRAW_DATEIS' => $value['RETSCN_LUPDT'],
                'RPRAW_QTYINC' => (int)$value['RETSCN_QTY'],
                'RPRAW_QTYOUT' => 0,
                'RPRAW_QTYADJ' => 0,
                'RPRAW_QTYTOT' => (int)$value['RETSCN_QTY'],
                'RPRAW_QTYOPN' => 0,
                'RPRAW_KET' => 'INC',
                'RPRAW_REF' => trim($value['RETSCN_DOC']).'||'.trim($value['RETSCN_CAT']).'||'.trim($value['RETSCN_LINE']).'||'.trim($value['RETSCN_FEDR'].'||'.$value['RETSCN_ORDERNO'].'||'.$value['RETSCN_LOT'])
            ];
        }
        
        return ['fg' => $getFG, 'det' => $getSerialDet];
    }
}
