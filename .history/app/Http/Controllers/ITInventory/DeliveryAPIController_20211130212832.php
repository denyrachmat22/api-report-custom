<?php

namespace App\Http\Controllers\ITInventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Model\MASTER\DLVMaster;

use App\Jobs\ITInventory\OutgoingPabean as OutgoingPabeanJobs;
use App\Model\RPCUST\MutasiDokPabean;

class DeliveryAPIController extends Controller
{
    // Incoming Pabean, Incoming Raw Material, Incoming FG From Customer, Incoming Mesin & peralatan
    public function updateIncomingPabean($jobs)
    {
        $insertJob = (new OutgoingPabeanJobs($jobs));

        dispatch($insertJob)->onQueue('OutgoingStock');

        return 'Request Mutasi Pabean Out Queued';
    }

    public function updateOutgoingByDate(Request $req)
    {
        $data = DLVMaster::select('DLV_ID')
            ->whereBetween('DLV_DATE', [$req->fdate, $req->ldate])
            ->where('DLV_ZNOMOR_AJU', '<>', '')
            ->groupBy('DLV_ID')
            ->get()
            ->toArray();

        $hasil = [];
        foreach ($data as $key => $value) {
            $hasil[$value['DLV_ID']] = $this->updateIncomingPabean(base64_encode(trim($value['DLV_ID'])));
        }

        return $hasil;
    }

    public function onDelivery($dlv, $item = '')
    {
        $headerData = DLVMaster::select(
            'DLV_BCTYPE', //RPBEA_JENBEA
            'DLV_NOAJU', //RPBEA_NUMBEA
            'SISCN_LINENO',
            'DLV_BCDATE',
            'DLV_ID',
            'DLV_DATE',
            DB::raw("DLV_ZNOMOR_AJU AS DLV_RPNO"),
            'DLV_RPDATE',
            'DLV_CUSTCD',
            DB::raw("
                CASE WHEN DLV_ITMCD IS NULL OR DLV_ITMCD = ''
                    THEN ITH_TBL.ITH_ITMCD
                    ELSE DLV_ITMCD
                END AS DLV_ITMCD
            "),
            DB::raw("COALESCE((
                SELECT TOP 1 DLVPRC_PRC FROM DLVPRC_TBL
                WHERE DLVPRC_SILINE = SISCN_LINENO
                AND DLVPRC_SER = DLV_SER
            ), 0) AS DLV_PRPRC"),
            'DLV_QTY',
            'DLV_SER',
            DB::raw("(
                SELECT MITM_STKUOM FROM MITM_TBL
                WHERE MITM_ITMCD = (
                    CASE WHEN DLV_TBL.DLV_ITMCD IS NULL OR DLV_TBL.DLV_ITMCD = ''
                        THEN ITH_TBL.ITH_ITMCD
                        ELSE DLV_TBL.DLV_ITMCD
                    END
                )
            ) AS MITM_STKUOM"),
            DB::raw("(
                SELECT MCUS_CURCD FROM MCUS_TBL
                    WHERE MCUS_CUSCD = DLV_CUSTCD
            ) AS MITM_SUPCR"),
            DB::raw("COALESCE((
                SELECT MAX(DLVPRC_PRC) FROM DLVPRC_TBL
                WHERE DLVPRC_SILINE = SISCN_LINENO
                AND DLVPRC_SER = DLV_SER
            ), 0) * DLV_QTY as DLV_TOT_HARGA"),
            'DLV_INVDT',
            'DLV_ID',
            DB::raw("(
                SELECT MITM_MODEL FROM MITM_TBL
                WHERE MITM_ITMCD = (
                    CASE WHEN DLV_TBL.DLV_ITMCD IS NULL OR DLV_TBL.DLV_ITMCD = ''
                        THEN ITH_TBL.ITH_ITMCD
                        ELSE DLV_TBL.DLV_ITMCD
                    END
                )
            ) AS MITM_MODEL")
        )
        ->leftjoin(DB::raw("(
            SELECT * FROM ITH_TBL
            WHERE ITH_FORM = 'INC-WH-FG'
            AND ITH_WH = 'AFWH3'
        ) AS ITH_TBL "), 'ITH_SER', 'DLV_SER')
        // ->leftjoin('SERD2_TBL', 'SERD2_SER', 'DLV_SER')
        ->join('MITM_TBL', 'ITH_ITMCD', 'MITM_ITMCD')
        ->join('SISCN_TBL', 'DLV_SER', 'SISCN_SER');

        if (!empty($dlv)) {
            $headerData->where('DLV_ID', $dlv);
        }

        if (!empty($item)) {
            $headerData->where('DLV_ITMCD', $item);
        }

        return $headerData->get();

        MutasiDokPabean::where('RPBEA_NUMSJL', base64_decode($dlv))->delete();

        $hasil = [];
        foreach ($headerData->get()->toArray() as $key => $value) {
            $hasil[] =  MutasiDokPabean::updateOrCreate([
                'RPBEA_DOC' => $value['DLV_SER'].'-'.$value['SISCN_LINENO'],
                'RPBEA_ITMCOD' => $value['DLV_ITMCD'],
                'RPBEA_QTYSAT' => floatval($value['DLV_PRPRC']),
                'RPBEA_NUMPEN' => $value['DLV_RPNO']
            ],[
                'RPBEA_JENBEA' => $value['DLV_BCTYPE'],
                'RPBEA_NUMBEA' => $value['DLV_NOAJU'],
                'RPBEA_TGLBEA' => $value['DLV_RPDATE'],
                'RPBEA_NUMSJL' => $value['DLV_ID'],
                'RPBEA_TGLSJL' => $value['DLV_DATE'],
                'RPBEA_NUMPEN' => $value['DLV_RPNO'],
                'RPBEA_TGLPEN' => $value['DLV_BCDATE'],
                'RPBEA_CUSSUP' => $value['DLV_CUSTCD'],
                'RPBEA_ITMCOD' => $value['DLV_ITMCD'],
                'RPBEA_QTYSAT' => floatval($value['DLV_PRPRC']),
                'RPBEA_QTYJUM' => (int)$value['DLV_TOT_HARGA'],
                'RPBEA_UNITMS' => $value['MITM_STKUOM'],
                'RPBEA_VALAS' => $value['MITM_SUPCR'],
                'RPBEA_QTYTOT' => (int)$value['DLV_QTY'],
                'RPBEA_TYPE' => 'OUT',
                'RPBEA_RCVEDT' => $value['DLV_INVDT'],
                'RPBEA_DOC' => $value['DLV_SER'].'-'.$value['SISCN_LINENO']
            ]);
        }

        return $hasil;
    }

    public function pushAllData()
    {
        $headerData = DLVMaster::select(
            'DLV_BCTYPE', //RPBEA_JENBEA
            'DLV_NOAJU', //RPBEA_NUMBEA
            'DLV_BCDATE',
            'DLV_ID',
            'DLV_DATE',
            DB::raw("CONCAT( SUBSTRING(DLV_FROMOFFICE, 1, 4), DLV_BCTYPE , RIGHT(CONCAT('000000',(SELECT ID_MODUL FROM aktivasi_aplikasi)), 6) , FORMAT(DLV_BCDATE, 'yyyyMMdd') , DLV_NOAJU) AS DLV_RPNO"),
            'DLV_RPDATE',
            'DLV_CUSTCD',
            DB::raw("
                CASE WHEN DLV_ITMCD IS NULL OR DLV_ITMCD = ''
                    THEN ITH_TBL.ITH_ITMCD
                    ELSE DLV_ITMCD
                END AS DLV_ITMCD
            "),
            DB::raw("0 AS DLV_PRPRC"),
            'DLV_QTY',
            'DLV_SER',
            DB::raw("(
                SELECT MITM_STKUOM FROM MITM_TBL
                WHERE MITM_ITMCD = (
                    CASE WHEN DLV_TBL.DLV_ITMCD IS NULL OR DLV_TBL.DLV_ITMCD = ''
                        THEN ITH_TBL.ITH_ITMCD
                        ELSE DLV_TBL.DLV_ITMCD
                    END
                )
            ) AS MITM_STKUOM"),
            DB::raw("(
                SELECT MITM_SUPCR FROM MITM_TBL
                WHERE MITM_ITMCD = (
                    CASE WHEN DLV_TBL.DLV_ITMCD IS NULL OR DLV_TBL.DLV_ITMCD = ''
                        THEN ITH_TBL.ITH_ITMCD
                        ELSE DLV_TBL.DLV_ITMCD
                    END
                )
            ) AS MITM_SUPCR"),
            DB::raw("0 as DLV_TOT_HARGA"),
            'DLV_INVDT',
            'DLV_ID',
            DB::raw("(
                SELECT MITM_MODEL FROM MITM_TBL
                WHERE MITM_ITMCD = (
                    CASE WHEN DLV_TBL.DLV_ITMCD IS NULL OR DLV_TBL.DLV_ITMCD = ''
                        THEN ITH_TBL.ITH_ITMCD
                        ELSE DLV_TBL.DLV_ITMCD
                    END
                )
            ) AS MITM_MODEL")
        )
            ->leftjoin(DB::raw("(
            SELECT * FROM  ITH_TBL
            WHERE ITH_FORM = 'INC-WH-FG'
            AND ITH_WH = 'AFWH3'
        ) AS ITH_TBL "), 'ITH_SER', 'DLV_SER')
            // ->leftjoin('SERD2_TBL', 'SERD2_SER', 'DLV_SER')
            ->join('MITM_TBL', 'ITH_ITMCD', 'MITM_ITMCD');

        $hasil = [];
        foreach ($headerData->get() as $key => $value) {
            $hasil[] = $this->updateIncomingPabean($value['DLV_ID']);
        }

        return $hasil;
    }

    public function getSISO($ref)
    {
        $getSISO = DB::select(DB::raw("
            SELECT
                it.ITH_ITMCD,
                st.SISCN_SER,
                st.SISCN_LINENO,
                st2.SISO_CPONO,
                st2.SISO_SOLINE,
                CASE
                    WHEN (
                    SELECT
                        SSO2_SLPRC
                    FROM
                        XSSO2 x2
                    WHERE
                        x2.SSO2_SOLNO = st2.SISO_SOLINE
                        AND x2.SSO2_CPONO = st2.SISO_CPONO
                        AND x2.SSO2_MDLCD = it.ITH_ITMCD ) IS NULL THEN (
                    SELECT
                        SSO2_SLPRC
                    FROM
                        XSSO2 x2
                    WHERE
                        x2.SSO2_MDLCD = it.ITH_ITMCD )
                    ELSE (
                    SELECT
                        SSO2_SLPRC
                    FROM
                        XSSO2 x2
                    WHERE
                        x2.SSO2_SOLNO = st2.SISO_SOLINE
                        AND x2.SSO2_CPONO = st2.SISO_CPONO
                        AND x2.SSO2_MDLCD = it.ITH_ITMCD )
                END AS PRICE,
                CASE
                    WHEN (
                    SELECT
                        SSO2_ISUDT
                    FROM
                        XSSO2 x2
                    WHERE
                        x2.SSO2_SOLNO = st2.SISO_SOLINE
                        AND x2.SSO2_CPONO = st2.SISO_CPONO
                        AND x2.SSO2_MDLCD = it.ITH_ITMCD ) IS NULL THEN (
                    SELECT
                        SSO2_ISUDT
                    FROM
                        XSSO2 x2
                    WHERE
                        x2.SSO2_MDLCD = it.ITH_ITMCD )
                    ELSE (
                    SELECT
                        SSO2_ISUDT
                    FROM
                        XSSO2 x2
                    WHERE
                        x2.SSO2_SOLNO = st2.SISO_SOLINE
                        AND x2.SSO2_CPONO = st2.SISO_CPONO
                        AND x2.SSO2_MDLCD = it.ITH_ITMCD )
                END AS PRC_DATE
            FROM
                SISCN_TBL st
            INNER JOIN (
                SELECT
                    *
                FROM
                    ITH_TBL it
                WHERE
                    it.ITH_FORM = 'INC-WH-FG'
                    AND it.ITH_WH = 'AFWH3' ) it ON
                it.ITH_SER = st.SISCN_SER
            LEFT JOIN (
                SELECT
                    SISO_HLINE,
                    SSO2_SLPRC,
                    SISO_CPONO,
                    SUM(SISO_QTY) PLOTQTY,
                    MAX(SISO_SOLINE) SISO_SOLINE
                FROM
                    SISO_TBL
                LEFT JOIN XSSO2 ON
                    SISO_CPONO = SSO2_CPONO
                    AND SSO2_SOLNO = SISO_SOLINE
                WHERE
                    SISO_QTY>0
                GROUP BY
                    SISO_HLINE,
                    SSO2_SLPRC,
                    SSO2_MDLCD,
                    SISO_CPONO,
                    SISO_SOLINE ) st2 ON
                st2.SISO_HLINE = st.SISCN_LINENO
            WHERE
                st.SISCN_SER = '" . $ref . "'
        "));

        return array_map(function ($value) {
            return (array)$value;
        }, $getSISO);
    }

    public function getPrice($ref)
    {
        $endpoint = 'http://192.168.0.29:8081/wms/SI/getsobyreffno';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'inser' => [$ref]
        ];

        $res = $guzz->request('POST', $endpoint, ['form_params' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content['CURL'];
    }
}
