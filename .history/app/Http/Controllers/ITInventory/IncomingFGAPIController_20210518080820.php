<?php

namespace App\Http\Controllers\ITInventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\WMS\API\SERD2TBL;

use App\Jobs\ITInventory\FinishGoodStock;

class IncomingFGAPIController extends Controller
{
    public function updateOnFGInc($reqFG)
    {
        $insertJobBahanBaku = (new FinishGoodStock($reqFG));

        dispatch($insertJobBahanBaku)->onQueue('FGStock');

        return 'Request Outgoing Raw & Incoming WIP Queued';
    }

    public function incFGAPI($sernum)
    {
        # code...
    }
}
