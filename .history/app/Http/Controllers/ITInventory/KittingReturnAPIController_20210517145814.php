<?php

namespace App\Http\Controllers\ITInventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\WMS\API\RETSCN;

use App\Jobs\ITInventory\BahanBakuStock;
use App\Jobs\ITInventory\WIPStock;

class KittingReturnAPIController extends Controller
{
    public function updateOnReturn($reqBB, $reqWIP)
    {
        $insertJobBahanBaku = (new BahanBakuStock($reqBB));

        dispatch($insertJobBahanBaku)->onQueue('BahanBakuStock');

        $insertJobWIP = (new WIPStock($reqWIP));

        dispatch($insertJobWIP)->onQueue('WIPStock');

        return 'Request Outgoing Raw & Incoming WIP Queued';
    }

    public function returnAPI($item, $psn = '', $cat = '', $line = '', $feeder = '', $mc = '', $lot = '')
    {
        $getRETSCN = RETSCN::where('RETSCN_ITMCD', $item)
        ->join('PSI_WMS.dbo.MITM_TBL', 'RETSCN_ITMCD', 'MITM_ITMCD');

        if (!empty($psn)) {
            $getRETSCN->where('RETSCN_DOC', $psn);
        }

        if (!empty($cat)) {
            $getRETSCN->where('RETSCN_CAT', $cat);
        }

        if (!empty($line)) {
            $getRETSCN->where('RETSCN_LINE', $line);
        }

        if (!empty($feeder)) {
            $getRETSCN->where('RETSCN_FEDR', $feeder);
        }

        $hasil = $getRETSCN
        ->where('RETSCN_SAVED', '1')
        ->get();

        $sendQueue = [];
        foreach ($hasil as $key => $value) {
        }
    }
}
