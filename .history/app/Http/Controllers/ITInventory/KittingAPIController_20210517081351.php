<?php

namespace App\Http\Controllers\ITInventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\WMS\API\SPLSCN;

use App\Jobs\ITInventory\BahanBakuStock;

class KittingAPIController extends Controller
{
    public function updateOnKitting($req)
    {
        $insertJob = (new BahanBakuStock($req));

        dispatch($insertJob)->onQueue('BahanBakuStock');

        return 'Request Mutasi Pabean Masuk Queued';
    }

    public function KittingAPI($item, $psn = '', $cat = '', $line = '', $feeder = '')
    {
        $getSPLSCN = SPLSCN::where('SPLSCN_ITMCD', $item)
        ->join('PSI_WMS.dbo.MITM_TBL', 'SPLSCN_ITMCD', 'MITM_ITMCD');

        if (!empty($psn)) {
            $getSPLSCN->where('SPLSCN_DOC', $psn);
        }

        if (!empty($cat)) {
            $getSPLSCN->where('SPLSCN_CAT', $cat);
        }

        if (!empty($line)) {
            $getSPLSCN->where('SPLSCN_LINE', $line);
        }

        if (!empty($feeder)) {
            $getSPLSCN->where('SPLSCN_FEDR', $feeder);
        }

        $getSPLSCN
        ->where('SPLSCN_SAVED', 1)
        ->get();

        $sendQueue = [];
        foreach ($getSPLSCN as $key => $value) {
            $parseValue = [
                'RPRAW_ITMCOD' => $value['SPLSCN_ITMCD'],
                'RPRAW_UNITMS' => $value['MITM_STKUOM'],
                'RPRAW_DATEIS' => $value['SPLSCN_LUPDT'],
                'RPRAW_QTYINC' => $value['SPLSCN_QTY'],
                'RPRAW_QTYOUT' => 0,
                'RPRAW_QTYADJ' => 0,
                'RPRAW_QTYTOT' => $value['SPLSCN_QTY'],
                'RPRAW_QTYOPN' => 0,
                'RPRAW_KET' => 'INC',
                'RPRAW_REF' => trim($psn).'||',trim($cat).'||',trim($line).'||',trim($feeder)
            ];

            $sendQueue[] = $this->updateOnKitting($parseValue);
        }

        return $sendQueue;
    }

    public function resendKittingAll()
    {
        $getSPLSCN = SPLSCN::join('PSI_WMS.dbo.MITM_TBL', 'SPLSCN_ITMCD', 'MITM_ITMCD')
        ->get();

        $sendQueue = [];
        foreach ($getSPLSCN as $key => $value) {
            $sendQueue[] = $this->updateOnKitting($value);
        }

        return 'All kitting queued';
    }
}
