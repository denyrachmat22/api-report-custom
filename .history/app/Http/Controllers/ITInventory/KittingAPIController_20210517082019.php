<?php

namespace App\Http\Controllers\ITInventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\WMS\API\SPLSCN;

use App\Jobs\ITInventory\BahanBakuStock;
USE App\Jobs\ITInventory\WIPStock;

class KittingAPIController extends Controller
{
    public function updateOnKitting($reqBB, $reqWIP)
    {
        $insertJobBahanBaku = (new BahanBakuStock($reqBB));

        dispatch($insertJobBahanBaku)->onQueue('BahanBakuStock');

        $insertJobWIP = (new WIPStock($reqWIP));

        dispatch($insertJobWIP)->onQueue('WIPStock');

        return 'Request Mutasi Pabean Masuk Queued';
    }

    public function KittingAPI($item, $psn = '', $cat = '', $line = '', $feeder = '')
    {
        // OUTGOING BAHAN BAKU
        $getSPLSCN = SPLSCN::where('SPLSCN_ITMCD', $item)
        ->join('PSI_WMS.dbo.MITM_TBL', 'SPLSCN_ITMCD', 'MITM_ITMCD');

        if (!empty($psn)) {
            $getSPLSCN->where('SPLSCN_DOC', $psn);
        }

        if (!empty($cat)) {
            $getSPLSCN->where('SPLSCN_CAT', $cat);
        }

        if (!empty($line)) {
            $getSPLSCN->where('SPLSCN_LINE', $line);
        }

        if (!empty($feeder)) {
            $getSPLSCN->where('SPLSCN_FEDR', $feeder);
        }

        $getSPLSCN
        ->where('SPLSCN_SAVED', 1)
        ->get();

        $sendQueue = [];
        foreach ($getSPLSCN as $key => $value) {
            $parseValueKitting = [
                'RPRAW_ITMCOD' => $value['SPLSCN_ITMCD'],
                'RPRAW_UNITMS' => $value['MITM_STKUOM'],
                'RPRAW_DATEIS' => $value['SPLSCN_LUPDT'],
                'RPRAW_QTYINC' => 0,
                'RPRAW_QTYOUT' => $value['SPLSCN_QTY'],
                'RPRAW_QTYADJ' => 0,
                'RPRAW_QTYTOT' => $value['SPLSCN_QTY'],
                'RPRAW_QTYOPN' => 0,
                'RPRAW_KET' => 'OUT',
                'RPRAW_REF' => trim($psn).'||',trim($cat).'||',trim($line).'||',trim($feeder)
            ];

            $parseValueWIP = [
                'RPWIP_DATEIS' => $value['SPLSCN_LUPDT'],
                'RPWIP_ITMCOD' => $value['SPLSCN_ITMCD'],
                'RPWIP_UNITMS' => $value['MITM_STKUOM'],
                'RPWIP_QTYTOT' => $value['SPLSCN_QTY'],
                'RPWIP_TYPE' => 'INC',
                'RPWIP_PSN' => $value['SPLSCN_DOC'],
                'RPWIP_CAT' => $value['SPLSCN_CAT'],
                'RPWIP_LINE' => $value['SPLSCN_LINE'],
                'RPWIP_FR' => $value['SPLSCN_FEDR'],
                'RPWIP_JOB' => '',
                'RPWIP_MCH' => $value['SPLSCN_ORDERNO'],
                'RPWIP_LOT' => $value['SPLSCN_LOTNO'],
                'RPWIP_SER' => '',
            ];

            $sendQueue[] = $this->updateOnKitting($parseValueKitting);
        }

        return $sendQueue;
    }

    public function resendKittingAll()
    {
        // OUTGOING BAHAN BAKU
        $getSPLSCN = SPLSCN::join('PSI_WMS.dbo.MITM_TBL', 'SPLSCN_ITMCD', 'MITM_ITMCD')
        ->get();

        $sendQueue = [];
        foreach ($getSPLSCN as $key => $value) {
            $parseValue = [
                'RPRAW_ITMCOD' => $value['SPLSCN_ITMCD'],
                'RPRAW_UNITMS' => $value['MITM_STKUOM'],
                'RPRAW_DATEIS' => $value['SPLSCN_LUPDT'],
                'RPRAW_QTYINC' => 0,
                'RPRAW_QTYOUT' => $value['SPLSCN_QTY'],
                'RPRAW_QTYADJ' => 0,
                'RPRAW_QTYTOT' => $value['SPLSCN_QTY'],
                'RPRAW_QTYOPN' => 0,
                'RPRAW_KET' => 'OUT',
                'RPRAW_REF' => trim($value['SPLSCN_DOC']).'||',trim($value['SPLSCN_CAT']).'||',trim($value['SPLSCN_LINE']).'||',trim($value['SPLSCN_FEDR'])
            ];

            $sendQueue[] = $this->updateOnKitting($parseValue);
        }

        return 'All kitting queued';
    }
}
