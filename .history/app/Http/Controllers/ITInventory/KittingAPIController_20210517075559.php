<?php

namespace App\Http\Controllers\ITInventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Jobs\ITInventory\BahanBakuStock;

class KittingAPIController extends Controller
{
    public function updateOnKitting($req)
    {
        $insertJob = (new BahanBakuStock($req));

        dispatch($insertJob)->onQueue('BahanBakuStock');

        return 'Request Mutasi Pabean Masuk Queued';
    }

    public function KittingAPI($psn, $cat, $line, $feeder)
    {
        # code...
    }
}
