<?php

namespace App\Http\Controllers\ITInventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\WMS\API\SERD2TBL;
use App\Model\MASTER\ITHMaster;

use App\Jobs\ITInventory\WIPStock;
use App\Jobs\ITInventory\FinishGoodStock;

class IncomingFGAPIController extends Controller
{
    public function updateOnFGInc($reqFG, $reqWIP)
    {
        $insertJobBahanBaku = (new FinishGoodStock($reqFG));

        dispatch($insertJobBahanBaku)->onQueue('FGStock');

        $insertJobWIP = (new WIPStock($reqWIP));

        dispatch($insertJobWIP)->onQueue('WIPStock');

        return 'Request Outgoing Raw & Incoming WIP Queued';
    }

    public function incFGAPI($sernum)
    {
        $getFG = ITHMaster::join('PSI_WMS.dbo.MITM_TBL', 'RETSCN_ITMCD', 'ITH_ITMCD')
            ->where('ITH_SER', $sernum)
            ->where('ITH_WH', 'AFWH3')
            ->where('ITH_FORM', 'INC-WH-FG')
            ->where('ITH_EXPORTED', 1)
            ->get();

        $getSerialDet = SERD2TBL::where('SERD2_SER', $sernum)->get();

        foreach ($getFG as $key => $value) {
            $parseValueFG = [
                'RPGOOD_UNITMS' => $value['MITM_STKUOM'],
                'RPGOOD_QTYTOT' => (int)$value['ITH_QTY'],
                'RPGOOD_QTYOUT' => 0,
                'RPGOOD_QTYOPN' => 0,
                'RPGOOD_QTYINC' => (int)$value['ITH_QTY'],
                'RPGOOD_QTYADJ' => 0,
                'RPGOOD_KET' => 'INC',
                'RPGOOD_ITMCOD' => $value['ITH_ITMCD'],
                'RPGOOD_DATEIS' => $value['ITH_DATE'],
                'RPGOOD_REF' => $sernum
            ];

            foreach ($getSerialDet as $keyDet => $valueDet) {
                $parseValueWIP = [
                    'RPWIP_DATEIS' => $value['SERD2_LUPDT'],
                    'RPWIP_ITMCOD' => $value['SERD2_ITMCD'],
                    'RPWIP_UNITMS' => $value['MITM_STKUOM'],
                    'RPWIP_QTYTOT' => (int)$value['RETSCN_QTY'],
                    'RPWIP_TYPE' => 'OUT',
                    'RPWIP_PSN' => $value['RETSCN_DOC'],
                    'RPWIP_CAT' => $value['RETSCN_CAT'],
                    'RPWIP_LINE' => $value['RETSCN_LINE'],
                    'RPWIP_FR' => $value['RETSCN_FEDR'],
                    'RPWIP_JOB' => 'RETURN',
                    'RPWIP_MCH' => $value['RETSCN_ORDERNO'],
                    'RPWIP_LOT' => $value['RETSCN_LOT'],
                    'RPWIP_SER' => 'RETURN',
                ];
            }

            $sendQueue[] = $this->updateOnKitting($parseValueFG, $parseValueWIP);
        }
        
        return ['fg' => $getFG, 'det' => $getSerialDet];
    }
}
