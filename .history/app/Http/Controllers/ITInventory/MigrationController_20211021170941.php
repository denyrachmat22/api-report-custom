<?php

namespace App\Http\Controllers\ITInventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\RPCUST\DetailStock;
use Illuminate\Support\Facades\DB;
use App\Jobs\adjEXBCQueue;

class MigrationController extends Controller
{
    public function getDataFromMigration()
    {
        ini_set('max_execution_time', 14400);
        $arr = [];

        $endpoint = 'http://192.168.0.29:8081/wms/smtstock?date=2021-09-26';

        $content = [];
        $guzz = new \GuzzleHttp\Client();

        $res = $guzz->request('GET', $endpoint);
        $content = json_decode($res->getBody(), true);

        return $content['data'];
    }

    public function migrateFromAPI($insert = false, $status = 'less')
    {
        $data = $this->getDataFromMigration();

        $hasil = [];
        foreach ($data as $key => $value) {
            $hasil[trim($value['ITH_ITMCD'])][] = (int)$value['BEFQTY'];
        }

        return $hasil;
        $getAllStockBC = $hasilCompare = [];
        foreach ($hasil as $key => $value) {
            $dataEXBC = $this->checkStokEXBC((string)$key);
            $total = array_sum($value) - (int)$dataEXBC['QTY'];
            if (!$insert || $insert == 'false') {
                if ($status === 'less') {
                    if ($total < 0) {
                        $hasilCompare[] = [
                            'ITEM' => $key,
                            'QTY' => array_sum($value),
                            'EXBC_QTY' => (int)$dataEXBC['QTY'],
                            'SELISIH' => $total
                        ];
                    }
                } elseif ($status === 'more') {
                    if ($total > 0) {
                        $hasilCompare[] = [
                            'ITEM' => $key,
                            'QTY' => array_sum($value),
                            'EXBC_QTY' => (int)$dataEXBC['QTY'],
                            'SELISIH' => $total
                        ];
                    }
                } else {
                    if ($total === 0) {
                        $hasilCompare[] = [
                            'ITEM' => $key,
                            'QTY' => array_sum($value),
                            'EXBC_QTY' => (int)$dataEXBC['QTY'],
                            'SELISIH' => $total
                        ];
                    }
                }
            } elseif ($insert == 'true') {
                if ($status === 'less') {
                    if ($total < 0) {
                        $queueInsert = (new adjEXBCQueue($key, $value, true));

                        dispatch($queueInsert)->onQueue('migrationsJobs');

                        $hasilCompare[] = $queueInsert;
                    }
                } elseif ($status === 'more') {
                    if ($total > 0) {
                        $queueInsert = (new adjEXBCQueue($key, $value, true));

                        dispatch($queueInsert)->onQueue('migrationsJobs');

                        $hasilCompare[] = $queueInsert;
                    }
                } else {
                    if ($total === 0) {
                        $hasilCompare[] = [
                            'ITEM' => $key,
                            'QTY' => array_sum($value),
                            'EXBC_QTY' => (int)$dataEXBC['QTY'],
                            'SELISIH' => $total
                        ];
                    }
                }
            }
        }

        return $hasilCompare;
    }

    public function checkDiscrepancyStock($status = null, $insert = false, $item = null)
    {
        $dataWMS = $this->getDataFromMigration();
        $dataEXBC = empty($item) ? $this->checkStokEXBCAll() : $this->checkStokEXBC($item);

        $hasilStock = [];
        foreach ($dataWMS as $key => $value) {
            $hasilStock[trim($value['ITH_ITMCD'])][] = (int)$value['BEFQTY'];
        }

        // return $hasilStock;

        $hasilCompare = [];
        foreach ($hasilStock as $keyData => $valueData) {
            $checkStock = array_values(array_filter($dataEXBC, function ($f) use ($keyData) {
                return $f['RPSTOCK_ITMNUM'] == $keyData;
            }));

            if (count($checkStock) > 0) {
                $totQty = array_sum($valueData);
                $total = ($totQty - round($totQty * 0.3)) - (int)$checkStock[0]['QTY'];

                if (empty($status)) {
                    if ($total <> 0) {
                        if ($total < 0) {
                            $queueInsert = (new adjEXBCQueue($keyData, $valueData, true));

                            dispatch($queueInsert)->onQueue('migrationsJobs');

                            $hasilCompare[] = [
                                'ITEM' => $keyData,
                                'QTY' => ($totQty - round($totQty * 0.3)),
                                'EXBC_QTY' => (int)$checkStock[0]['QTY'],
                                'SELISIH' => $total,
                                'STATUS_QUEUE' => $queueInsert
                            ];
                        } else {
                            $hasilCompare[] = [
                                'ITEM' => $keyData,
                                'QTY' => ($totQty - round($totQty * 0.3)),
                                'EXBC_QTY' => (int)$checkStock[0]['QTY'],
                                'SELISIH' => $total
                            ];
                        }
                    }
                } else {
                    if ($status === 'less') {
                        if ($total < 0) {
                            $hasilCompare[] = [
                                'ITEM' => $keyData,
                                'QTY' => ($totQty - round($totQty * 0.3)),
                                'EXBC_QTY' => (int)$checkStock[0]['QTY'],
                                'SELISIH' => $total
                            ];

                            if ($insert) {
                                $queueInsert = (new adjEXBCQueue($keyData, $valueData, true));

                                dispatch($queueInsert)->onQueue('migrationsJobs');
                            }
                        }
                    } elseif ($status === 'more') {
                        if ($total > 0) {
                            $hasilCompare[] = [
                                'ITEM' => $keyData,
                                'QTY' => ($totQty - round($totQty * 0.3)),
                                'EXBC_QTY' => (int)$checkStock[0]['QTY'],
                                'SELISIH' => $total
                            ];
                        }
                    } else {
                        if ($total === 0) {
                            $hasilCompare[] = [
                                'ITEM' => $keyData,
                                'QTY' => ($totQty - round($totQty * 0.3)),
                                'EXBC_QTY' => (int)$checkStock[0]['QTY'],
                                'SELISIH' => $total
                            ];
                        }
                    }
                }
            }
        }

        return $hasilCompare;
    }

    public function checkStokEXBC($item)
    {
        $hasil = DetailStock::select('RPSTOCK_ITMNUM', DB::raw('SUM(RPSTOCK_QTY) AS QTY'))->where('RPSTOCK_BCDATE', '<=', '2021-09-05')->where('RPSTOCK_ITMNUM', $item)->groupBy('RPSTOCK_ITMNUM')->get()->toArray();

        return $hasil;
    }

    public function checkStokEXBCAll()
    {
        $hasil = DetailStock::select('RPSTOCK_ITMNUM', DB::raw('SUM(RPSTOCK_QTY) AS QTY'))->where('RPSTOCK_BCDATE', '<=', '2021-09-05')->groupBy('RPSTOCK_ITMNUM')->get()->toArray();

        return $hasil;
    }

    public function checkStokEXBCAllTrue()
    {
        $hasil = DetailStock::select('RPSTOCK_ITMNUM', DB::raw('SUM(RPSTOCK_QTY) AS QTY'))->groupBy('RPSTOCK_ITMNUM')->get()->toArray();

        return $hasil;
    }

    public function checkMigrationDouble($remarks, $item = null)
    {
        $whereItem = empty($item) ? null : "AND rb.RPSTOCK_ITMNUM = '" . $item . "'";
        $data = DB::connection('PSI_RPCUST')->select("
                SELECT
                rb.RPSTOCK_ITMNUM,
                rb.RPSTOCK_BCDATE,
                rb.RPSTOCK_NOAJU,
                SUM(rb.RPSTOCK_QTY) as QTY,
                (
                    SELECT SUM(RPSTOCK_QTY) FROM RPSAL_BCSTOCK rb2
                    WHERE rb2.RPSTOCK_ITMNUM = rb.RPSTOCK_ITMNUM
                    AND rb2.RPSTOCK_BCDATE = rb.RPSTOCK_BCDATE
                    AND rb2.RPSTOCK_REMARK LIKE '" . $remarks . "%'
                    AND rb2.RPSTOCK_BCDATE <= '2021-09-26'
                    AND rb2.deleted_at IS NULL
                ) * -1 as ADJ_QTY
            FROM RPSAL_BCSTOCK rb
            WHERE rb.RPSTOCK_TYPE = 'INC'
            AND rb.RPSTOCK_BCDATE <= '2021-09-26'
            AND rb.deleted_at IS NULL
            " . $whereItem . "
            GROUP BY
                rb.RPSTOCK_ITMNUM,
                rb.RPSTOCK_BCDATE,
                rb.RPSTOCK_NOAJU
            HAVING (
                SELECT SUM(RPSTOCK_QTY) * -1 FROM RPSAL_BCSTOCK rb2
                WHERE rb2.RPSTOCK_ITMNUM = rb.RPSTOCK_ITMNUM
                AND rb2.RPSTOCK_BCDATE = rb.RPSTOCK_BCDATE
                AND rb2.RPSTOCK_REMARK LIKE '" . $remarks . "%'
                AND rb2.RPSTOCK_BCDATE <= '2021-09-26'
                AND rb2.deleted_at IS NULL
            ) > SUM(rb.RPSTOCK_QTY)
        ");

        return $data;

        $hasil = [];
        foreach ($data as $key => $value) {
            $cek = DetailStock::create(
                [
                    'RPSTOCK_BCDATE' => $value->RPSTOCK_BCDATE,
                    'RPSTOCK_QTY' => $value->ADJ_QTY - $value->QTY,
                    'RPSTOCK_TYPE' => 'INC',
                    'RPSTOCK_NOAJU' => $value->RPSTOCK_NOAJU,
                    'RPSTOCK_ITMNUM' => $value->RPSTOCK_ITMNUM,
                    'RPSTOCK_REMARK' => 'FIX_DOUBLE_ADJ'
                ]
            );

            // $queueInsert = $this->sendMigrations(trim($value->RPSTOCK_ITMNUM), );

            // dispatch($queueInsert)->onQueue('migrationsJobs');

            // $hasil[] = $queueInsert;
        }

        return $data;
    }

    public function migrationOutAdj()
    {
        $data = DB::select("
            SELECT SERD2_ITMCD ITH_ITMCD
                ,SUM(SERD2_QTY) BEFQTY,
                (
                    SELECT COALESCE(SUM(qty), 0)
                    FROM v_stock_exbc_by_date_aju vsebda
                    WHERE vsebda.RPSTOCK_ITMNUM = ITH_ITMCD
                ) AS EXBC_STOCK
            FROM (
                SELECT ITH_SER
                FROM v_ith_tblc
                WHERE ITH_DATEC BETWEEN '2021-09-27'
                        AND '2021-10-21'
                    AND ITH_WH = 'ARSHP'
                    AND ITH_QTY < 0
                GROUP BY ITH_SER
                ) VOUTFG
            LEFT JOIN SERD2_TBL ON ITH_SER = SERD2_SER
            GROUP BY SERD2_ITMCD
            ORDER BY 1
        ");

        $data = array_map(function ($value) {
            return (array)$value;
        }, $data);

        $dataExBC = $this->checkStokEXBCAllTrue();

        return $data;

        $hasil = [];
        foreach ($data as $key => $value) {
            if (!empty($value['ITH_ITMCD'])) {
                $getDataExBC = array_values(array_filter($dataExBC, function ($f) use ($value){
                    return trim($f['RPSTOCK_ITMNUM']) == $value['ITH_ITMCD'];
                }));

                if (count($getDataExBC) > 0) {
                    $hasil[] = array_merge($value, [
                        'QTY_EXBC' => $getDataExBC[0]['QTY']
                    ]);

                    $queueInsert = (new adjEXBCQueue($value['ITH_ITMCD'], (int)$value['BEFQTY'], false, 'ADJ_OUT_FROM_2021-09-06_TO_2021-09-15'));

                    dispatch($queueInsert)->onQueue('migrationsJobsDevs');
                } else {
                    $hasil[] = array_merge($value, [
                        'QTY_EXBC' => 0
                    ]);
                }
            }
        }

        return $hasil;
    }

    public function sendMigrations($item, $qty)
    {
        $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item,
            'qty' => $qty,
            'doc' => 'ADJ_2021_07_31',
            'date_out' => date('Y-m-d')
        ];

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content;
    }

    public function removeExBCNotExistsRCVTBL()
    {
        $data = DB::select('SELECT * FROM PSI_RPCUST.dbo.v_stock_exbc_not_exists_rcv_tbl');

        $datanya = [];
        foreach (json_decode(json_encode($data), true) as $key => $value) {
            $deleted = DetailStock::where('RPSTOCK_ITMNUM', $value['RPSTOCK_ITMNUM'])
                ->where('RPSTOCK_NOAJU', $value['RPSTOCK_NOAJU'])
                ->where('RPSTOCK_BCDATE', $value['RPSTOCK_BCDATE'])
                ->delete();

            $datanya[] = [
                'RPSTOCK_ITMNUM' => $value['RPSTOCK_ITMNUM'],
                'RPSTOCK_NOAJU' => $value['RPSTOCK_NOAJU'],
                'RPSTOCK_BCDATE' => $value['RPSTOCK_BCDATE'],
                'status' => $deleted,
            ];
        }

        return $datanya;
    }

    public function checkingStockAju()
    {
        $data = DB::connection('PSI_RPCUST')->select("
            SELECT *
            FROM v_stock_exbc_by_date_aju vsebda
            WHERE vsebda.RPSTOCK_BCDATE <= '2021-09-26'
            --AND vsebda.RPSTOCK_ITMNUM = '211470600'
            AND vsebda.qty < 0
            ORDER BY vsebda.RPSTOCK_BCDATE
        ");

        // return $data;

        foreach ($data as $key => $value) {
            DetailStock::where('RPSTOCK_ITMNUM', $value->RPSTOCK_ITMNUM)
                ->where('RPSTOCK_NOAJU', $value->RPSTOCK_NOAJU)
                ->where('RPSTOCK_BCDATE', $value->RPSTOCK_BCDATE)
                ->where('RPSTOCK_REMARK', 'LIKE', 'MIGRATION%')
                ->delete();
        }

        return $data;
    }
}
