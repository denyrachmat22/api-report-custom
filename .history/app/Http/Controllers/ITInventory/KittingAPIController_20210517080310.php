<?php

namespace App\Http\Controllers\ITInventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\WMS\API\SPLSCN;

use App\Jobs\ITInventory\BahanBakuStock;

class KittingAPIController extends Controller
{
    public function updateOnKitting($req)
    {
        $insertJob = (new BahanBakuStock($req));

        dispatch($insertJob)->onQueue('BahanBakuStock');

        return 'Request Mutasi Pabean Masuk Queued';
    }

    public function KittingAPI($item, $psn = '', $cat = '', $line = '', $feeder = '')
    {
        $getSPLSCN = SPLSCN::where('SPLSCN_ITMCD', $item);

        if (!empty($psn)) {
            $getSPLSCN->where('SPLSCN_DOC', $psn);
        }

        if (!empty($cat)) {
            $getSPLSCN->where('SPLSCN_CAT', $cat);
        }

        if (!empty($line)) {
            $getSPLSCN->where('SPLSCN_LINE', $line);
        }

        if (!empty($feeder)) {
            $getSPLSCN->where('SPLSCN_FEDR', $feeder);
        }
    }
}
