<?php

namespace App\Http\Controllers\ITInventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\WMS\API\RETSCN;

use App\Jobs\ITInventory\BahanBakuStock;
use App\Jobs\ITInventory\WIPStock;

class KittingReturnAPIController extends Controller
{
    public function updateOnReturn($reqBB, $reqWIP)
    {
        $insertJobBahanBaku = (new BahanBakuStock($reqBB));

        dispatch($insertJobBahanBaku)->onQueue('BahanBakuStock');

        $insertJobWIP = (new WIPStock($reqWIP));

        dispatch($insertJobWIP)->onQueue('WIPStock');

        return 'Request Outgoing Raw & Incoming WIP Queued';
    }
}
