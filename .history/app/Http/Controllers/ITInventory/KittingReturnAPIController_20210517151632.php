<?php

namespace App\Http\Controllers\ITInventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Model\WMS\API\RETSCN;

use App\Jobs\ITInventory\BahanBakuStock;
use App\Jobs\ITInventory\WIPStock;

class KittingReturnAPIController extends Controller
{
    public function updateOnReturn($reqBB, $reqWIP)
    {
        $insertJobBahanBaku = (new BahanBakuStock($reqBB));

        dispatch($insertJobBahanBaku)->onQueue('BahanBakuStock');

        $insertJobWIP = (new WIPStock($reqWIP));

        dispatch($insertJobWIP)->onQueue('WIPStock');

        return 'Request Outgoing Raw & Incoming WIP Queued';
    }

    public function returnAPI($item, $psn = '', $cat = '', $line = '', $feeder = '', $mc = '', $lot = '')
    {
        $getRETSCN = RETSCN::select(
            'RETSCN_SPLDOC',
            'RETSCN_CAT',
            'RETSCN_LINE',
            'RETSCN_FEDR',
            'RETSCN_ORDERNO',
            'RETSCN_ITMCD',
            'RETSCN_LOT',
            'RETSCN_SAVED',
            DB::raw('SUM(RETSCN_QTYAFT) AS RETSCN_QTY')
        )
        ->where('RETSCN_ITMCD', $item)
        ->join('PSI_WMS.dbo.MITM_TBL', 'RETSCN_ITMCD', 'MITM_ITMCD');

        if (!empty($psn)) {
            $getRETSCN->where('RETSCN_DOC', $psn);
        }

        if (!empty($cat)) {
            $getRETSCN->where('RETSCN_CAT', $cat);
        }

        if (!empty($line)) {
            $getRETSCN->where('RETSCN_LINE', $line);
        }

        if (!empty($feeder)) {
            $getRETSCN->where('RETSCN_FEDR', $feeder);
        }

        if (!empty($mc)) {
            $getRETSCN->where('RETSCN_ORDERNO', $mc);
        }

        if (!empty($lot)) {
            $getRETSCN->where('RETSCN_LOT', $lot);
        }

        $hasil = $getRETSCN
        ->where('RETSCN_SAVED', '1')
        ->groupBy(
            'RETSCN_SPLDOC',
            'RETSCN_CAT',
            'RETSCN_LINE',
            'RETSCN_FEDR',
            'RETSCN_ORDERNO',
            'RETSCN_ITMCD',
            'RETSCN_LOT',
            'RETSCN_SAVED'
        )
        ->get();

        $sendQueue = [];
        foreach ($hasil as $key => $value) {
            $parseValueReturn = [
                'RPRAW_ITMCOD' => $value['RETSCN_ITMCD'],
                'RPRAW_UNITMS' => $value['MITM_STKUOM'],
                'RPRAW_DATEIS' => $value['RETSCN_LUPDT'],
                'RPRAW_QTYINC' => (int)$value['RETSCN_QTY'],
                'RPRAW_QTYOUT' => 0,
                'RPRAW_QTYADJ' => 0,
                'RPRAW_QTYTOT' => (int)$value['RETSCN_QTY'],
                'RPRAW_QTYOPN' => 0,
                'RPRAW_KET' => 'INC',
                'RPRAW_REF' => trim($psn).'||'.trim($cat).'||'.trim($line).'||'.trim($feeder)
            ];

            $parseValueWIP = [
                'RPWIP_DATEIS' => $value['RETSCN_LUPDT'],
                'RPWIP_ITMCOD' => $value['RETSCN_ITMCD'],
                'RPWIP_UNITMS' => $value['MITM_STKUOM'],
                'RPWIP_QTYTOT' => (int)$value['RETSCN_QTY'],
                'RPWIP_TYPE' => 'OUT',
                'RPWIP_PSN' => $value['RETSCN_DOC'],
                'RPWIP_CAT' => $value['RETSCN_CAT'],
                'RPWIP_LINE' => $value['RETSCN_LINE'],
                'RPWIP_FR' => $value['RETSCN_FEDR'],
                'RPWIP_JOB' => 'RETURN',
                'RPWIP_MCH' => $value['RETSCN_ORDERNO'],
                'RPWIP_LOT' => $value['RETSCN_LOT'],
                'RPWIP_SER' => 'RETURN',
            ];

            $sendQueue[] = $this->updateOnKitting($parseValueReturn, $parseValueWIP);
        }

        return $sendQueue;
    }

    public function resendReturnAll()
    {

        $getRETSCN = RETSCN::select(
            'RETSCN_SPLDOC',
            'RETSCN_CAT',
            'RETSCN_LINE',
            'RETSCN_FEDR',
            'RETSCN_ORDERNO',
            'RETSCN_ITMCD',
            'RETSCN_LOT',
            'RETSCN_SAVED',
            DB::raw('SUM(RETSCN_QTYAFT) AS RETSCN_QTY')
        )
        ->join('PSI_WMS.dbo.MITM_TBL', 'RETSCN_ITMCD', 'MITM_ITMCD');

        $hasil = $getRETSCN
        ->where('RETSCN_SAVED', '1')
        ->groupBy(
            'RETSCN_SPLDOC',
            'RETSCN_CAT',
            'RETSCN_LINE',
            'RETSCN_FEDR',
            'RETSCN_ORDERNO',
            'RETSCN_ITMCD',
            'RETSCN_LOT',
            'RETSCN_SAVED'
        )
        ->get();

        $sendQueue = [];
        foreach ($hasil as $key => $value) {
            $parseValueReturn = [
                'RPRAW_ITMCOD' => $value['RETSCN_ITMCD'],
                'RPRAW_UNITMS' => $value['MITM_STKUOM'],
                'RPRAW_DATEIS' => $value['RETSCN_LUPDT'],
                'RPRAW_QTYINC' => (int)$value['RETSCN_QTY'],
                'RPRAW_QTYOUT' => 0,
                'RPRAW_QTYADJ' => 0,
                'RPRAW_QTYTOT' => (int)$value['RETSCN_QTY'],
                'RPRAW_QTYOPN' => 0,
                'RPRAW_KET' => 'INC',
                'RPRAW_REF' => trim($psn).'||'.trim($cat).'||'.trim($line).'||'.trim($feeder)
            ];

            $parseValueWIP = [
                'RPWIP_DATEIS' => $value['RETSCN_LUPDT'],
                'RPWIP_ITMCOD' => $value['RETSCN_ITMCD'],
                'RPWIP_UNITMS' => $value['MITM_STKUOM'],
                'RPWIP_QTYTOT' => (int)$value['RETSCN_QTY'],
                'RPWIP_TYPE' => 'OUT',
                'RPWIP_PSN' => $value['RETSCN_DOC'],
                'RPWIP_CAT' => $value['RETSCN_CAT'],
                'RPWIP_LINE' => $value['RETSCN_LINE'],
                'RPWIP_FR' => $value['RETSCN_FEDR'],
                'RPWIP_JOB' => 'RETURN',
                'RPWIP_MCH' => $value['RETSCN_ORDERNO'],
                'RPWIP_LOT' => $value['RETSCN_LOT'],
                'RPWIP_SER' => 'RETURN',
            ];

            $sendQueue[] = $this->updateOnKitting($parseValueReturn, $parseValueWIP);
        }

        return $sendQueue;
    }
}
