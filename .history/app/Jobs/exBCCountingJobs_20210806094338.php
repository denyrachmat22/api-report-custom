<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Model\WMS\API\RCVSCN;
use App\Model\RPCUST\DetailStock;
use LRedis;

class exBCCountingJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $item_num, $tujuan, $date_out, $lot, $bc, $doc, $kontrak, $qty;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($item_num, $tujuan, $date_out, $lot, $bc, $doc, $kontrak, $qty)
    {
        $this->item_num = $item_num;
        $this->tujuan = $tujuan;
        $this->date_out = $date_out;
        $this->lot = $lot;
        $this->bc = $bc;
        $this->doc = $doc;
        $this->kontrak = $kontrak;
        $this->qty = $qty;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->item_num as $key => $value) {
            $req = new Request([
                'item_num' => $value,
                'tujuan' => $this->tujuan,
                'date_out' => $this->date_out,
                'lot' => $this->lot[$key],
                'bc' => $this->bc,
                'doc' => $this->doc,
                'kontrak' => $this->kontrak,
                'qty' => $this->qty[$key]
            ]);

            $hasil[] = $this->showavailablestock($req)->original;
        }

        return $hasil;

        try{
            $redis = LRedis::connection();

            $redis->publish('exBCMessage', json_encode([
                'id' => 'exBCMessage',
                'do' => 'SMT-0001',
                'fg' => '0001',
                'price' => 10.21,
                'data' => $hasil,
                'data' => $hasil
            ]));
        }catch(\Predis\Connection\ConnectionException $e){
            return response('error connection redis');
        }
    }

    public function showavailablestock(Request $r)
    {
        $select = [
            'RPSTOCK_ITMNUM',
            'RPSTOCK_BCTYPE',
            'RPSTOCK_BCNUM',
            'RPSTOCK_BCDATE',
            'RPSTOCK_NOAJU',
            'RPSTOCK_DOC',
            'RPSTOCK_LOT',
            'RCV_KPPBC',
            'RCV_HSCD',
            'RCV_ZNOURUT',
            'RCV_PRPRC',
            'RCV_BM',
            'MITM_ITMD1',
            'MITM_STKUOM'
        ];

        if (!$r->has('adj') || empty($r->adj)) {
            if (!$r->has('lot') || empty($r->lot)) {
                $getRCVSCN = $this->findRCVScan($r->item_num);
            } else {
                $cekData = $this->findRCVScan($r->item_num, $r->lot)->count();

                $getRCVSCN = $cekData === 0
                    ? clone $this->findRCVScan($r->item_num)
                    : ($this->findRCVScan($r->item_num, $r->lot)->first()['STOCK'] < $r->qty
                        ? clone $this->findRCVScan($r->item_num)
                        : clone $this->findRCVScan($r->item_num, $r->lot));
            }

            if (($r->has('tujuan') && !empty($r->tujuan)) && ($r->has('bc') && !empty($r->tujuan))) {
                if ($r->bc === '27') {
                    if ($r->tujuan == 6) {
                        $getRCVSCN->where('RCV_ZSTSRCV', '2');
                    } else {
                        $getRCVSCN->where(function ($w) {
                            $w->where('RCV_ZSTSRCV', '<>', '2');
                            $w->orWhereNull('RCV_ZSTSRCV');
                        });
                    }
                }
            }

            if ($r->bc == '41') {
                if ($r->has('kontrak') || !empty($r->kontrak)) {
                    $getRCVSCN->where('RCV_CONA', $r->kontrak);
                } else {
                    $getRCVSCN->where('RCV_CONA', '<>', 'NULL');
                }
            }

            if ($r->has('do') || !empty($r->do)) {
                $getRCVSCN->where('RCVSCN_DONO', $r->do);
            }
        } else {
            $getRCVSCN = $this->findRCVScan($r->item_num, null, true);
            if ($r->has('do') || !empty($r->do)) {
                $getRCVSCN->where('RCVSCN_DONO', $r->do);
            }

            if ($r->has('item_num') || !empty($r->item_num)) {
                $getRCVSCN->where('RCVSCN_ITMCD', $r->item_num);
            }

            if ($r->has('less_than') || !empty($r->less_than)) {
                $getRCVSCN->where('RCVSCN_LUPDT', '<', $r->less_than);
            }
        }

        $dataAwal = $getRCVSCN->get();

        // return $dataAwal;

        $test = DetailStock::select(array_merge($select, [
            DB::raw('SUM(RPSTOCK_QTY) RPSTOCK_QTY'),
            DB::raw('(
                SELECT
                SUM(rt.RCVSCN_QTY)
                FROM PSI_WMS.dbo.RCVSCN_TBL rt
                WHERE rt.RCVSCN_SAVED = 1
                AND rt.RCVSCN_ITMCD = RPSTOCK_ITMNUM
                AND rt.RCVSCN_DONO = RPSTOCK_DOC
            ) as STOCK_RCVSCN')
        ]))
            ->join('PSI_WMS.dbo.RCV_TBL', function ($f) {
                $f->on('RPSTOCK_ITMNUM', 'RCV_ITMCD');
                $f->on('RPSTOCK_DOC', 'RCV_DONO');
                $f->on('RPSTOCK_BCNUM', 'RCV_BCNO');
                $f->on('RPSTOCK_NOAJU', 'RCV_RPNO');
            })
            ->join('PSI_WMS.dbo.XMITM_V', 'MITM_ITMCD', '=', 'RPSTOCK_ITMNUM');

        $hasil = [];
        foreach ($dataAwal as $key => $value) {
            $do = !empty($value['RCV_DONO_REFF']) ? $value['RCV_DONO_REFF'] : $value['RCVSCN_DONO'];
            $item = !empty($value['RCV_ITMCD_REFF']) ? $value['RCV_ITMCD_REFF'] : $value['RCVSCN_ITMCD'];

            $hasil[] = ['do' => $do, 'item' => $item];
            if ($key === 0) {
                $test->where(function ($w) use ($do, $item) {
                    $w->where('RPSTOCK_DOC', $do);
                    $w->where('RPSTOCK_ITMNUM', $item);
                });
            } else {
                $test->orwhere(function ($w) use ($do, $item) {
                    $w->where('RPSTOCK_DOC', $do);
                    $w->where('RPSTOCK_ITMNUM', $item);
                });
            }
        }

        // if ($r->has('date_out') && !empty($r->date_out)) {
        //     $test->whereBetween('created_at', [$r->date_out.'00:00:00', $r->date_out.'23:59:59']);
        // }

        // Cek tanggal terlama dari BC
        if ($r->has('adj') && $r->adj == true) {
            $cek = $test->orderBy('RPSTOCK_BCDATE', 'DESC')
                ->orderBy('RPSTOCK_QTY', 'DESC')
                ->groupBy($select)
                ->get()
                ->toArray();
        } else {
            $cek = $test->havingRaw('SUM(RPSTOCK_QTY) > 0')
                ->orderBy('RPSTOCK_BCDATE', 'ASC')
                ->orderBy('RPSTOCK_QTY', 'DESC')
                ->groupBy($select)
                ->get()
                ->toArray();
        }

        // return $cek;
        $totQty = 0;
        if ($r->has('adj') || !empty($r->adj)) {
            foreach ($cek as $keyData => $valueData) {
                $totQty += $valueData['RPSTOCK_QTY'];
            }
        }

        if ($r->has('adj') && $r->adj == true) {
            $typeoutincsrc = ['OUT-ADJ', NULL];
            $locoutincsrc = ['WH-RM', NULL];
        } else {
            if ($r->has('scrap') && $r->scrap == true) {
                $typeoutincsrc = ['OUT', 'INC-SCR'];
                $locoutincsrc = ['WH-RM', 'WH-SCR-RM'];
            } else {
                $typeoutincsrc = ['OUT', 'INC-DO'];
                $locoutincsrc = ['WH-RM', 'WH-DO'];
            }
        }

        if ($r->has('remark')) {
            $remark = $r->remark;
        } else {
            $remark = null;
        }

        if ($r->has('revise') && $r->revise === true) {
            $rev = true;
        } else {
            $rev = false;
        }

        // return $cek;

        // Start compiling to recursive
        if (!empty($cek) && count($hasil) > 0) {
            $qty = !$r->has('adj') || empty($r->adj) || $r->adj === false
                ? $r->qty
                : ($r->qty < 0
                    ? $r->qty
                    : $totQty - $r->qty);
            $hasil = $this->comparedata(
                $r->doc,
                $r->bc,
                $cek,
                $qty,
                $r->item_num,
                [],
                $r->lot,
                $typeoutincsrc,
                $remark,
                $locoutincsrc,
                $rev,
                $r->has('adj') || $r->adj == true,
                $r->has('date_out') && !empty($r->date_out) ? $r->date_out : date('Y-m-d')
            );

            // return $hasil;
            if (count($hasil) == 0) {
                return response()->json([
                    'status' => 'failed',
                    'data' => [],
                    'message' => 'no item found with enough stock!!'
                ]);
            }

            // return $hasil;

            //Tester

            $hasil_final = [];
            foreach ($hasil as $key => $value) {
                $hasil_final[] = [
                    'BC_TYPE' => $value['RPSTOCK_BCTYPE'],
                    'BC_NUM' => $value['RPSTOCK_BCNUM'],
                    'BC_AJU' => $value['RPSTOCK_NOAJU'],
                    'BC_DATE' => $value['RPSTOCK_BCDATE'],
                    'BC_QTY' => $value['RPSTOCK_QTY'],
                    'BC_DO' => $value['DO_ASAL'],
                    'BC_ITEM' => trim($value['RPSTOCK_ITMNUM']),
                    'QTY_SISA' => $value['QTY_SISA'],
                    'RCV_KPPBC' => $value['RCV_KPPBC'],
                    'RCV_HSCD' => $value['RCV_HSCD'],
                    'RCV_ZNOURUT' => $value['RCV_ZNOURUT'],
                    'RCV_PRPRC' => $value['RCV_PRPRC'],
                    'RCV_BM' => $value['RCV_BM'],
                    'MITM_ITMD1' => trim($value['MITM_ITMD1']),
                    'MITM_STKUOM' => trim($value['MITM_STKUOM']),
                    'OUT_BCDATE' => $value['RPSTOCK_BCDATEOUT']
                ];
            }

            return response()->json([
                'status' => 'success',
                'data' => $hasil_final
            ]);
        } else {
            return response()->json([
                'status' => 'failed',
                'data' => [],
                'message' => 'no item found on RCVSCN!!'
            ]);
        }

        return $cek;
    }

    public function comparedata($doc, $bcout, $arr, $qty, $item, $currentarr = [], $lot, $inoutarr, $remark, $loc, $rev, $adj, $dateOut)
    {
        $totalarray = $currentarr;
        //return $totalarray;
        $cekdatanow = current($arr);
        if (!empty($cekdatanow)) {
            $tot = $adj ? round($qty) + $cekdatanow['STOCK_RCVSCN'] : round($qty) - $cekdatanow['RPSTOCK_QTY'];
            $hasil = $adj
                ? ($qty < 0
                    ? ($qty + (int)$cekdatanow['RPSTOCK_QTY'] < 0
                        ? ((int)$cekdatanow['STOCK_RCVSCN'] > $qty * -1
                            ? $qty
                            : (int)$cekdatanow['STOCK_RCVSCN'] * -1 + (int)$cekdatanow['RPSTOCK_QTY'])
                        : $qty + (int)$cekdatanow['RPSTOCK_QTY'])
                    : ($qty > $cekdatanow['RPSTOCK_QTY'] ? $cekdatanow['RPSTOCK_QTY'] : round($qty)))
                : ($qty > $cekdatanow['RPSTOCK_QTY'] ? $cekdatanow['RPSTOCK_QTY'] : round($qty));

            // return [$hasil, $tot];
            // return [$hasil, $qty + (int)$cekdatanow['RPSTOCK_QTY'] > 0, $qty, $cekdatanow['RPSTOCK_QTY'], $cekdatanow['STOCK_RCVSCN'], $qty + (int)$cekdatanow['RPSTOCK_QTY']];
            if ($qty !== 0) {
                // Deducted stock incoming
                DetailStock::create([
                    'RPSTOCK_BCTYPE' => $cekdatanow['RPSTOCK_BCTYPE'],
                    'RPSTOCK_BCNUM' => $cekdatanow['RPSTOCK_BCNUM'],
                    'RPSTOCK_BCDATE' => $cekdatanow['RPSTOCK_BCDATE'],
                    'RPSTOCK_QTY' => $rev === true ? $hasil : $hasil * -1,
                    'RPSTOCK_DOC' => $cekdatanow['RPSTOCK_DOC'],
                    'RPSTOCK_TYPE' => $inoutarr[0],
                    'RPSTOCK_ITMNUM' => $cekdatanow['RPSTOCK_ITMNUM'],
                    'RPSTOCK_NOAJU' =>  $cekdatanow['RPSTOCK_NOAJU'],
                    'RPSTOCK_LOT' =>  $cekdatanow['RPSTOCK_LOT'],
                    'RPSTOCK_DOINC' => $cekdatanow['RPSTOCK_DOC'],
                    'RPSTOCK_REMARK' => is_array($remark) ? $remark[0] : '',
                    'RPSTOCK_LOC' => $loc[0],
                    'RPSTOCK_BCDATEOUT' => $dateOut
                ]);

                // IF not adjustment add to doc location
                if ($inoutarr[1]) {
                    DetailStock::create([
                        'RPSTOCK_BCTYPE' => $bcout,
                        'RPSTOCK_BCNUM' => $cekdatanow['RPSTOCK_BCNUM'],
                        'RPSTOCK_BCDATE' => $cekdatanow['RPSTOCK_BCDATE'],
                        'RPSTOCK_QTY' => $rev === true ? $hasil * -1 : $hasil,
                        'RPSTOCK_DOC' => $doc,
                        'RPSTOCK_TYPE' => $inoutarr[1],
                        'RPSTOCK_ITMNUM' => $cekdatanow['RPSTOCK_ITMNUM'],
                        'RPSTOCK_NOAJU' =>  $cekdatanow['RPSTOCK_NOAJU'],
                        'RPSTOCK_LOT' =>  $lot,
                        'RPSTOCK_DOINC' => $cekdatanow['RPSTOCK_DOC'],
                        'RPSTOCK_REMARK' => is_array($remark) ? $remark[1] : $remark,
                        'RPSTOCK_LOC' => $loc[0],
                        'RPSTOCK_BCDATEOUT' => $dateOut
                    ]);
                }
            }

            $arrhasil = [
                'RPSTOCK_BCTYPE' => $cekdatanow['RPSTOCK_BCTYPE'],
                'RPSTOCK_BCNUM' => $cekdatanow['RPSTOCK_BCNUM'],
                'RPSTOCK_BCDATE' => $cekdatanow['RPSTOCK_BCDATE'],
                'RPSTOCK_QTY' => $hasil,
                'RPSTOCK_DOC' => $doc,
                'RPSTOCK_TYPE' => $inoutarr[0],
                'RPSTOCK_ITMNUM' => $cekdatanow['RPSTOCK_ITMNUM'],
                'RPSTOCK_NOAJU' =>  $cekdatanow['RPSTOCK_NOAJU'],
                'RPSTOCK_LOT' =>  $lot,
                'DO_ASAL' => $cekdatanow['RPSTOCK_DOC'],
                'QTY_SISA' => $cekdatanow['RPSTOCK_QTY'] - $hasil,
                'RCV_KPPBC' => $cekdatanow['RCV_KPPBC'],
                'RCV_HSCD' => $cekdatanow['RCV_HSCD'],
                'RCV_ZNOURUT' => $cekdatanow['RCV_ZNOURUT'],
                'RCV_PRPRC' => $cekdatanow['RCV_PRPRC'],
                'RCV_BM' => $cekdatanow['RCV_BM'],
                'MITM_ITMD1' => trim($cekdatanow['MITM_ITMD1']),
                'MITM_STKUOM' => trim($cekdatanow['MITM_STKUOM']),
                'RPSTOCK_BCDATEOUT' => $dateOut
            ];

            if ($adj) {
                if ($qty < 0) {
                    next($arr);
                    return $this->comparedata($doc, $bcout, $arr, $tot, $item, count($totalarray) > 0 ? array_merge($totalarray, [$arrhasil]) : [$arrhasil], $lot, $inoutarr, $remark, $loc, $rev, $adj, $dateOut);
                } else {
                    return count($totalarray) > 0 ? array_merge($totalarray, [$arrhasil]) : [$arrhasil];
                }
            } else {
                if ($cekdatanow['RPSTOCK_QTY'] < $qty) {
                    next($arr);
                    return $this->comparedata($doc, $bcout, $arr, $tot, $item, count($totalarray) > 0 ? array_merge($totalarray, [$arrhasil]) : [$arrhasil], $lot, $inoutarr, $remark, $loc, $rev, $adj, $dateOut);
                } else {
                    return count($totalarray) > 0 ? array_merge($totalarray, [$arrhasil]) : [$arrhasil];
                }
            }
        } else {
            return $totalarray;
        }
    }

    public function findRCVScan($item, $lot = null, $adj = false)
    {
        if (empty($lot)) {
            $getRCVSCN = RCVSCN::select(
                'RCVSCN_DONO',
                'RCV_BCDATE',
                'RCV_ITMCD_REFF',
                'RCV_DONO_REFF',
                'RCVSCN_ITMCD',
                'RCV_ZSTSRCV',
                DB::raw('(
                    SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                    FROM [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                    WHERE rb.RPSTOCK_DOC = RCVSCN_DONO
                    and rb.RPSTOCK_ITMNUM = RCVSCN_ITMCD
                ) as STOCK'),
                DB::raw('COALESCE(SUM(CAST(RCVSCN_QTY as BIGINT)), 0) AS RCVSTOCK')
            )
                ->where('RCVSCN_ITMCD', $item)
                ->join('RCV_TBL', function ($j) {
                    $j->on('RCVSCN_DONO', '=', 'RCV_DONO');
                    $j->on('RCVSCN_ITMCD', '=', 'RCV_ITMCD');
                })
                // ->leftjoin('PSI_RPCUST.dbo.RPSAL_BCSTOCK', function ($j2) {
                //     $j2->on('RCVSCN_DONO', '=', 'RPSTOCK_DOC');
                //     $j2->on('RCVSCN_ITMCD', '=', 'RPSTOCK_ITMNUM');
                // })
                ->orderBy('RCV_BCDATE', 'asc')
                ->groupBy(
                    'RCVSCN_DONO',
                    'RCV_BCDATE',
                    'RCV_ITMCD_REFF',
                    'RCV_DONO_REFF',
                    'RCVSCN_ITMCD',
                    'RCV_ZSTSRCV'
                );

            if (!$adj) {
                $getRCVSCN->havingRaw('SUM(CAST(RPSTOCK_QTY as BIGINT)) > 0');
            }
        } else {
            $getRCVSCN = RCVSCN::select(
                'RCVSCN_DONO',
                'RCV_BCDATE',
                'RCV_ITMCD_REFF',
                'RCV_DONO_REFF',
                'RCVSCN_LOTNO',
                'RCVSCN_ITMCD',
                'RCV_ZSTSRCV',
                // DB::raw('COALESCE(SUM(CAST(RPSTOCK_QTY as BIGINT)), 0) AS STOCK'),
                DB::raw('(
                    SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                    FROM [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                    WHERE rb.RPSTOCK_DOC = RCVSCN_DONO
                    and rb.RPSTOCK_ITMNUM = RCVSCN_ITMCD
                ) as STOCK'),
                DB::raw('COALESCE(SUM(CAST(RCVSCN_QTY as BIGINT)), 0) AS RCVSTOCK')
            )
                ->where('RCVSCN_ITMCD', $item)
                ->where('RCVSCN_LOTNO', $lot)
                ->join('RCV_TBL', function ($j) {
                    $j->on('RCVSCN_DONO', '=', 'RCV_DONO');
                    $j->on('RCVSCN_ITMCD', '=', 'RCV_ITMCD');
                })
                // ->leftjoin('PSI_RPCUST.dbo.RPSAL_BCSTOCK', function ($j2) {
                //     $j2->on('RCVSCN_DONO', '=', 'RPSTOCK_DOC');
                //     $j2->on('RCVSCN_ITMCD', '=', 'RPSTOCK_ITMNUM');
                // })
                ->orderBy('RCV_BCDATE', 'asc')
                ->groupBy(
                    'RCVSCN_DONO',
                    'RCV_BCDATE',
                    'RCV_ITMCD_REFF',
                    'RCV_DONO_REFF',
                    'RCVSCN_LOTNO',
                    'RCVSCN_ITMCD',
                    'RCV_ZSTSRCV'
                );
        }

        return $getRCVSCN;
    }
}
