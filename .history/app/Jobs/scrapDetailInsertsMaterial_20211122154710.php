<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
// use Illuminate\Queue\SerializesModels;
use LRedis;
use Illuminate\Support\Facades\DB;

use App\Model\RPCUST\scrapHistDet;
use App\Model\RPCUST\scrapHist;
use App\Model\WMS\API\RCVSCN2;

use App\Model\MASTER\ITHMaster;
use App\Jobs\ITInventory\ScrapStock;

class scrapDetailInsertsMaterial implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $valueLotDet, $id, $username, $dept, $type, $cekRcvScnForPrice, $item, $idTrans;
    public function __construct($valueLotDet, $id, $username, $dept, $type, $cekRcvScnForPrice, $item, $idTrans)
    {
        $this->valueLotDet = $valueLotDet;
        $this->id = $id;
        $this->username = $username;
        $this->dept = $dept;
        $this->type = $type;
        $this->cekRcvScnForPrice = $cekRcvScnForPrice;
        $this->item = $item;
        $this->idTrans = $idTrans;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->valueLotDet['RCVSCN_ITMCD'] && $this->valueLotDet['RCVSCN_LOTNO'] && $this->valueLotDet['RCVSCN_DONO']) {
            $cekRcvScnForPrice = $this->cekLatestHarga($this->valueLotDet['RCVSCN_ITMCD'], $this->valueLotDet['RCVSCN_LOTNO'], $this->valueLotDet['RCVSCN_DONO']);
            // Insert ke table Hist Detail
            $insertDet = scrapHistDet::create([
                'SCR_HIST_ID' => $this->idTrans,
                'SCR_PSN' => null,
                'SCR_CAT' => null,
                'SCR_LINE' => null,
                'SCR_FR' => null,
                'SCR_MC' => null,
                'SCR_MCZ' => null,
                'SCR_ITMCD' => $this->valueLotDet['RCVSCN_ITMCD'],
                'SCR_QTPER' => 1,
                'SCR_QTY' => round(intVal($this->valueLotDet['QTY_SCRAP'])),
                'SCR_LOTNO' => $this->valueLotDet['RCVSCN_LOTNO'],
                'SCR_ITMPRC' => isset($this->valueLotDet['RCV_PRPRC'])
                ? $this->valueLotDet['RCV_PRPRC']
                : (empty($cekRcvScnForPrice) ? 0 : $cekRcvScnForPrice['RCV_PRPRC'])
            ]);

            // Receive
            $this->insertToITH(
                trim($this->valueLotDet['RCVSCN_ITMCD']),
                date('Y-m-d'),
                'INC-SCR-RM',
                $this->idTrans,
                round(intVal($this->valueLotDet['QTY_SCRAP'])),
                'SCRRPT',
                $insertDet->id,
                'SCR_RPT'
            );

            // Insert scrap to Ex-BC
            $insertingStock = $this->sendingParamBCStock(
                $this->valueLotDet['RCVSCN_ITMCD'],
                intVal($this->valueLotDet['QTY_SCRAP']),
                $this->id,
                $this->valueLotDet['RCVSCN_LOTNO'],
                $cekRcvScnForPrice['RCV_BCTYPE'],
                $insertDet->id
            );

            // $insertJobScrap = (new ScrapStock([
            //     'RPREJECT_UNITMS' => $this->valueLotDet['RCVSCN_ITMCD'],
            //     'RPREJECT_QTYTOT' => round(intVal($this->valueLotDet['QTY_SCRAP'])),
            //     'RPREJECT_QTYOUT' => 0,
            //     'RPREJECT_QTYOPN' => 0,
            //     'RPREJECT_QTYINC' => round(intVal($this->valueLotDet['QTY_SCRAP'])),
            //     'RPREJECT_QTYADJ' => 0,
            //     'RPREJECT_KET' => '',
            //     'RPREJECT_ITMCOD' => $this->valueLotDet['RCVSCN_ITMCD'],
            //     'RPREJECT_DATEIS' => date('Y-m-d'),
            //     'RPREJECT_REF' => $this->id
            // ]));

            // dispatch($insertJobScrap)->onQueue('ScrapStock');
        }

        try{
            $redis = LRedis::connection();

            $redis->publish('message', json_encode([
                'app' => 'scrap_report',
                'id' => $this->id,
                'id_transaction' => $this->idTrans,
                'item' => $this->item['MITM_ITMCD'],
                'item_desc' => $this->item['MITM_ITMD1'],
                'doc' => $this->valueLotDet['RCVSCN_LOTNO'],
                'status' => 'positive',
                'message' => 'This scrap report detail material is successfully inserted'
            ]));
        }catch(\Predis\Connection\ConnectionException $e){
            return response('error connection redis');
        }
    }

    public function failed(\Throwable $exception)
    {
        try {
            $cekJob = scrapHist::where('id', $this->id)->first();
            $redis = LRedis::connection();

            $redis->publish('message', json_encode([
                'app' => 'scrap_report',
                'id' => $this->idTrans,
                'id_transaction' => $this->id,
                'item' => $this->item['MITM_ITMCD'],
                'item_desc' => $this->item['MITM_ITMD1'],
                'doc' => $cekJob->DOC_NO,
                'status' => 'negative',
                'message' => 'There is an error on server, please contact administrator !'
            ]));

            scrapHist::where('id', $this->id)->delete();
        } catch (\Predis\Connection\ConnectionException $e) {
            return response('error connection redis');
        }
    }

    // ETC Function
    public function cekLatestHarga($item, $lot, $do = '')
    {
        $cekRcvScnForPriceHead = RCVSCN2::select(
            'RCVSCN_ITMCD',
            'RCVSCN_DONO',
            'RCV_PRPRC',
            'RCVSCN_LUPDT',
            'RCV_BCTYPE'
        )
            ->where('RCVSCN_ITMCD', $item)
            ->where('RCVSCN_LOTNO', $lot)
            ->join('RCV_TBL', function ($j) {
                $j->on('RCV_ITMCD', 'RCVSCN_ITMCD');
                $j->on('RCV_DONO', 'RCVSCN_DONO');
            })
            ->groupBy(
                'RCVSCN_ITMCD',
                'RCVSCN_DONO',
                'RCV_PRPRC',
                'RCVSCN_LUPDT',
                'RCV_BCTYPE'
            )
            ->orderBy('RCVSCN_LUPDT', 'DESC');

        if (empty($do)) {
            $cekRcvScnForPrice = $cekRcvScnForPriceHead->first();
        } else {
            $cekRcvScnForPrice = $cekRcvScnForPriceHead->where('RCVSCN_DONO', $do)->first();
        }

        return $cekRcvScnForPrice;
    }

    public function sendingParamBCStock($item_num, $qty = 0, $doc = null, $lot = null, $bc = null, $id = '', $rev = false)
    {
        $endpoint = 'http://192.168.0.29:8081/api-report-custom/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item_num,
            'qty' => $qty,
            'doc' => $doc,
            'lot' => $lot,
            'bc' => $bc,
            'scrap' => true,
            'remark' => $id,
            'revise' => $rev
        ];

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content;
    }

    public function insertToITH($item, $date, $form, $doc, $qty, $wh, $remark, $user)
    {
        ITHMaster::insert([
            'ITH_ITMCD' => $item,
            'ITH_DATE' => $date,
            'ITH_FORM' => $form,
            'ITH_DOC' => $doc,
            'ITH_QTY' => $qty,
            'ITH_WH' => $wh,
            'ITH_LOC' => '',
            'ITH_SER' => '',
            'ITH_REMARK' => $remark,
            'ITH_LINE' => '',
            'ITH_LUPDT' => date('Y-m-d H:i:s'),
            'ITH_USRID' => $user,
        ]);
    }
}
