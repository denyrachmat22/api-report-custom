<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class scrapReportInsert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $req;

    public function __construct($req)
    {
        $this->req = $req;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Cek Last ID today
        $cek = scrapHist::whereBetween('created_at', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59'])->orderBy('created_at', 'desc')->withTrashed()->first();
        $idTrans = date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->ID_TRANS, -3) + 1));

        $hasil = [];

        $fetchProcessMaster = processMstr::get()->toArray();
        foreach ($req->data as $key => $value) {
            // Jika itemnya adalah Model
            if ($value['MITM_MODEL'] === '1') {
                // Parsing job yang di pilih
                foreach ($value['SCRAP_DET'] as $keyJobDet => $valueJobDet) {
                    // Parsing Process Master (PROCESS_DET)
                    foreach ($valueJobDet['PROCESS_DET'] as $keyProMstr => $valueProMstr) {
                        // Parsing Process Detail
                        foreach ($valueProMstr['DET'] as $keyProDet => $valueProDet) {

                            if (intval($valueProDet['QTY']) > 0) {
                                $insertHeader = scrapHist::create([
                                    'DSGN_MAP_ID' => $valueProDet['id'],
                                    'USERNAME' => $req->username,
                                    'DEPT' => $req->dept,
                                    'QTY' => $valueProDet['QTY'],
                                    'ID_TRANS' => $idTrans,
                                    'TYPE_TRANS' => $req->type,
                                    'DOC_NO' => $valueJobDet['PIS2_WONO'],
                                    'REASON' => isset($valueProDet['REASON']) ? $valueProDet['REASON'] : '',
                                    'SCR_PROCD' => $valueProDet['RPDSGN_PROCD'],
                                    'MDL_FLAG' => 1,
                                    'ITEMNUM' => $value['MITM_ITMCD'],
                                ]);

                                // Insert Detail Transaction

                                $findByProcess = $this->getPsnListByJobAndProcd(
                                    $fetchProcessMaster,
                                    $valueJobDet['PIS2_WONO'],
                                    $valueProDet['id'],
                                    $value['MITM_ITMCD']
                                );

                                // Fetch All PSN by process and job number
                                foreach ($findByProcess as $key => $value) {

                                    // Get Data LOT SPLSCN
                                    $getFIFOLotNo = $this->getDataSPLSCN(
                                        $value['PPSN1_PSNNO'],
                                        $value['PPSN2_ITMCAT'],
                                        $value['PPSN1_LINENO'],
                                        $value['PPSN1_FR'],
                                        $value['PPSN2_MCZ'],
                                        $value['PPSN2_SUBPN']
                                    );

                                    $totalQty = $valueProDet['QTY'] * floatVal($valueProDet['QTPER_VAL']);

                                    $fetchFIFOLotNo = $this->getFIFOLOT($getFIFOLotNo, $totalQty);

                                    foreach ($fetchFIFOLotNo as $keyFifoLot => $valueFifoLot) {
                                        # code...
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
            }
        }
    }

    // Helpernya


}
