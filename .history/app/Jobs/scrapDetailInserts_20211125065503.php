<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use LRedis;
use Illuminate\Support\Facades\DB;

use App\Model\RPCUST\scrapHist;
use App\Model\RPCUST\scrapHistDet;
use App\Model\WMS\API\SPLSCN;

use App\Model\MASTER\ITHMaster;
use App\Model\MASTER\ItemMaster;

use App\Jobs\ITInventory\ScrapStock;
use App\Model\WMS\API\SERDTBL;

class scrapDetailInserts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $process, $qty, $id, $item, $idTrans, $job, $scrap_instruction, $date_out, $insertStock;
    public function __construct($process, $qty, $id, $item, $idTrans, $job, $scrap_instruction, $date_out = '', $insertStock = false)
    {
        $this->process = $process;
        $this->qty = $qty;
        $this->id = $id;
        $this->item = $item;
        $this->idTrans = $idTrans;
        $this->job = $job;
        $this->scrap_instruction = $scrap_instruction;
        $this->date_out = empty($date_out) || $date_out === '1970-01-01' ? date('Y-m-d') : $date_out;
        $this->insertStock = $insertStock;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        set_time_limit(3600);
        // Fetch All PSN by process and job number

        // logger(json_encode($this->process));

        $getAngkaKoma = array_filter($this->process, function ($f) {
            return $f['PPSN2_QTPER'] < 1;
        });

        // logger(json_encode($getAngkaKoma));

        $cekKoma = $this->cekAngkaKoma($getAngkaKoma, $this->qty);

        // logger($cekKoma);
        if ($cekKoma['status'] === true) {
            $listStoredItem = [];
            foreach ($this->process as $key => $value) {
                $getFIFOLotNo = $this->getListLotFromSERD(
                    $value['PPSN1_PSNNO'],
                    $value['PPSN1_LINENO'],
                    $value['PPSN2_SUBPN'],
                    $value['SERD_JOB']
                );

                $totalQty = $this->qty * $value['PPSN2_QTPER'];

                $fetchFIFOLotNo = $this->getFIFOLOTSERD($getFIFOLotNo, $totalQty);

                foreach ($fetchFIFOLotNo as $keyFifoLot => $valueFifoLot) {
                    $insertDet = scrapHistDet::insertGetId([
                        'SCR_HIST_ID' => $this->id,
                        'SCR_PSN' => $value['PPSN1_PSNNO'],
                        'SCR_CAT' => $valueFifoLot['SERD_CAT'],
                        'SCR_LINE' => $value['PPSN1_LINENO'],
                        'SCR_FR' => $valueFifoLot['SERD_FR'],
                        'SCR_MC' => $valueFifoLot['SERD_MC'],
                        'SCR_MCZ' => $valueFifoLot['SERD_MCZ'],
                        'SCR_ITMCD' => trim($value['PPSN2_SUBPN']),
                        'SCR_QTPER' => $value['PPSN2_QTPER'],
                        'SCR_QTY' => $totalQty,
                        'SCR_LOTNO' => $valueFifoLot['SERD_LOTNO'],
                        'SCR_ITMPRC' => empty($valueFifoLot['PRICE']) ? 0 : $valueFifoLot['PRICE'],
                        'SCR_PROCD' => $value['PPSN1_PROCD'],
                        'SCR_PROID' => $value['RPDSGN_CODE']
                    ]);

                    if ($this->insertStock) {
                        logger($insertDet);
                        $listStoredItem[trim($value['PPSN2_SUBPN'])][$key] = array_merge($valueFifoLot, [
                            'QTY' => $this->qty,
                            'PPSN2_QTPER' => $value['PPSN2_QTPER'],
                            'ID_DET' => $insertDet,
                            'ID_MSTR' => $this->id
                        ]);
                        // Insert scrap to stock
                        $hasilBCStock = $this->sendingParamBCStock(
                            trim($value['PPSN2_SUBPN']),
                            $this->date_out,
                            $valueFifoLot['SERD_LOTNO'],
                            round($totalQty),
                            $this->idTrans,
                            null,
                            $this->id
                        );
                    }
                }
            }

            // logger(json_encode($listStoredItem));

            if (count($listStoredItem) > 0) {
                $sumItem = [];
                foreach ($listStoredItem as $keyStock => $valueStock) {

                    $tot_qty = 0;
                    foreach ($valueStock as $keyTot => $valueTot) {
                        $tot_qty += $valueTot['QTY'] * $valueTot['PPSN2_QTPER'];
                    }

                    $sumItem[] = array_merge(array_values($valueStock)[0], ['QTY' => round($tot_qty)]);

                    if ($this->insertStock) {
                        if ($hasilBCStock['status'] == true) {
                            // Insert Scrap to ITH_TBL if qty is from WIP
                            $cekStockITH = ITHMaster::select(
                                'ITH_DOC',
                                'ITH_DATE',
                                'ITH_WH'
                            )
                                // ->where('ITH_DOC', $value['PPSN1_PSNNO'] . '|' . $value['PPSN2_ITMCAT'] . '|' . $value['PPSN1_LINENO'] . '|' . $value['PPSN1_FR'])
                                ->where('ITH_ITMCD', trim($valueTot['SERD_ITMCD']))
                                ->whereIn('ITH_FORM', ['INC-PRD-RM', 'OUT-RET'])
                                ->first();

                            if ($this->scrap_instruction == 0 && !empty($cekStockITH)) {
                                // Issue
                                $this->insertToITH(
                                    trim($valueTot['SERD_ITMCD']),
                                    $cekStockITH['ITH_DATE'],
                                    'OUT-PRD-RM',
                                    $cekStockITH['ITH_DOC'],
                                    round($tot_qty) * -1,
                                    $cekStockITH['ITH_WH'],
                                    $valueTot['ID_DET'],
                                    $valueTot['ID_MSTR'],
                                    'SCR_RPT'
                                );
                            }

                            // Receive
                            $this->insertToITH(
                                trim($valueTot['SERD_ITMCD']),
                                $this->date_out,
                                'INC-SCR-RM',
                                $this->idTrans,
                                round($tot_qty),
                                'SCRRPT',
                                $valueTot['ID_DET'],
                                $valueTot['ID_MSTR'],
                                'SCR_RPT'
                            );
                        }
                    }
                }
            }

            try {
                $cekJob = scrapHist::where('id', $this->id)->first();
                $redis = LRedis::connection();

                $redis->publish('message', json_encode([
                    'app' => 'scrap_report',
                    'id' => $this->idTrans,
                    'id_transaction' => $this->id,
                    'item' => $this->item['MITM_ITMCD'],
                    'item_desc' => $this->item['MITM_ITMD1'],
                    'doc' => $cekJob->DOC_NO,
                    'status' => 'positive',
                    'message' => 'This ' . $cekJob->DOC_NO . ' detail is successfully inserted',
                    'user' => $cekJob->USERNAME,
                    'stat_print' => false
                ]));

                scrapHist::where('id', $this->id)->update(['IS_DONE' => 1]);

                $cekOk = scrapHist::where('ID_TRANS', $this->idTrans)->get()->toArray();

                $cekData = array_values(array_filter($cekOk, function ($f) {
                    return $f['IS_DONE'] == 1;
                }));

                if (count($cekData) === scrapHist::where('ID_TRANS', $this->idTrans)->count()) {
                    $redis->publish('message', json_encode([
                        'app' => 'scrap_report',
                        'id' => $this->idTrans,
                        'id_transaction' => $this->id,
                        'item' => $this->item['MITM_ITMCD'],
                        'item_desc' => $this->item['MITM_ITMD1'],
                        'doc' => $cekJob->DOC_NO,
                        'status' => 'positive',
                        'message' => 'This ' . $this->idTrans . ' already finished, you can print now.',
                        'user' => $cekJob->USERNAME,
                        'stat_print' => true
                    ]));
                }
            } catch (\Predis\Connection\ConnectionException $e) {
                return response('error connection redis');
            }
        } else {
            $cekJob = scrapHist::where('id', $this->id)->first();
            $redis = LRedis::connection();
            $redis->publish('message', json_encode([
                'app' => 'scrap_report',
                'id' => $this->idTrans,
                'id_transaction' => $this->id,
                'item' => $this->item['MITM_ITMCD'],
                'item_desc' => $this->item['MITM_ITMD1'],
                'doc' => $cekJob->DOC_NO,
                'status' => 'negative',
                'message' => 'Your input qty will be decimal please put right amount of scrap !!',
                'user' => $cekJob->USERNAME,
                'stat_print' => false,
                'hasil_dec' => $cekKoma['data']
            ]));
            scrapHist::where('id', $this->id)->update(['failed_reason' => 'Your input qty will be decimal please put right amount of scrap !!']);
            ITHMaster::where('ITH_SER', (string)$this->id)->where('ITH_USRID', 'SCR_RPT')->delete();

            scrapHist::where('id', $this->id)->delete();
            scrapHistDet::where('SCR_HIST_ID', $this->id)->delete();
        }
    }

    public function failed(\Throwable $exception)
    {
        try {
            $cekJob = scrapHist::where('id', $this->id)->first();
            $redis = LRedis::connection();

            $redis->publish('message', json_encode([
                'app' => 'scrap_report',
                'id' => $this->idTrans,
                'id_transaction' => $this->id,
                'item' => $this->item['MITM_ITMCD'],
                'item_desc' => $this->item['MITM_ITMD1'],
                'doc' => $cekJob->DOC_NO,
                'status' => 'negative',
                'message' => 'There is an error on server, please contact administrator !',
                'user' => $cekJob->USERNAME,
                'stat_print' => false
            ]));
            scrapHist::where('id', $this->id)->update(['failed_reason' => $exception]);
            ITHMaster::where('ITH_SER', (string)$this->id)->where('ITH_USRID', 'SCR_RPT')->delete();

            scrapHist::where('id', $this->id)->delete();
            scrapHistDet::where('SCR_HIST_ID', $this->id)->delete();
        } catch (\Predis\Connection\ConnectionException $e) {
            return response('error connection redis');
        }
    }

    // ETC Function
    public function convertifABProcess($data)
    {
        $cekAB = array_filter($data, function ($f) {
            return $f['PPSN1_PROCD'] === 'SMT-AB';
        });

        if (count($cekAB) > 0) {
            $hasil = [];
            foreach ($cekAB as $key => $value) {
                if ($value['PPSN2_ITMCAT'] !== 'PCB') {
                    $hasil = array_merge($value, ['PPSN2_QTPER' => $value['PPSN2_QTPER'] * 0.5]);
                }
            }
        }
    }

    public function cekAngkaKoma($data, $qty)
    {
        $hasil = [];
        foreach ($data as $key => $value) {
            $hasil[trim($value['PPSN2_SUBPN'])][$key] = ['jumlah' => $qty * $value['PPSN2_QTPER'], 'process' => $value['PPSN1_PROCD']];
        }

        $cekData = [];
        foreach ($hasil as $keyJum => $valueJum) {
            $sumnya1 = 0;
            foreach (array_values($valueJum) as $keyJum2 => $valueJum2) {
                $sumnya1 += $valueJum2['jumlah'];
            }
            $cekData[] = [
                'ITEM' => $keyJum,
                'TOTAL' => $sumnya1
            ];
        }

        $cek = array_filter(array_values($hasil), function ($f) {
            $sumnya = 0;
            foreach (array_values($f) as $key => $value) {
                $sumnya += $value['jumlah'];
            }

            return $this->is_decimal($sumnya);
        });

        $proc = [];
        foreach ($cek as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $proc[] = $value2['process'];
            }
        }

        if (count($cek) > 0) {
            return [
                'status' => false,
                'process' => implode(",", $proc),
                'data' => $cekData
            ];
        } else {
            return [
                'status' => true,
                'process' => implode(",", $proc)
            ];
        }
    }

    public function is_decimal($val)
    {
        return is_numeric($val) && floor($val) != $val;
    }

    public function getDataSPLSCN($psn, $line, $fr, $item)
    {
        $data = SPLSCN::select([
            'SPLSCN_DOC',
            'SPLSCN_CAT',
            'SPLSCN_LINE',
            'SPLSCN_FEDR',
            'SPLSCN_ORDERNO',
            'SPLSCN_ITMCD',
            'SPLSCN_LOTNO',
            'SPLSCN_LUPDT',
            DB::raw('(SUM(SPLSCN_QTY) - COALESCE(SUM(RETSCN_QTYAFT), 0)) - COALESCE(SUM(SCR_QTY), 0) AS SPLSCN_QTY'),
            DB::raw('SUM(RETSCN_QTYAFT) AS RETURN_QTY'),
            DB::raw('COALESCE(SUM(SCR_QTY), 0) AS SCRAP_QTY'),
            DB::raw('SUM(SPLSCN_QTY) AS REAL_SPLSCN_QTY'),
            DB::raw('(SELECT TOP 1
                RCV_PRPRC
                FROM RCVSCN_TBL
                INNER JOIN RCV_TBL ON RCVSCN_DONO = RCV_DONO
                    AND RCVSCN_ITMCD = RCV_ITMCD
                WHERE RCVSCN_ITMCD = SPLSCN_ITMCD
                AND RCVSCN_LOTNO = SPLSCN_LOTNO
                ORDER BY RCV_RPDATE DESC) as PRICE'),
            DB::raw('(SELECT TOP 1
                RCV_BCTYPE
                FROM RCVSCN_TBL
                INNER JOIN RCV_TBL ON RCVSCN_DONO = RCV_DONO
                    AND RCVSCN_ITMCD = RCV_ITMCD
                WHERE RCVSCN_ITMCD = SPLSCN_ITMCD
                AND RCVSCN_LOTNO = SPLSCN_LOTNO
                ORDER BY RCV_RPDATE DESC) as RCV_BCTYPE')
        ])
            ->leftjoin('RETSCN_TBL', function ($f) {
                $f->on('RETSCN_ITMCD', '=', 'SPLSCN_ITMCD');
                $f->on('RETSCN_SPLDOC', '=', 'SPLSCN_DOC');
                $f->on('RETSCN_CAT', '=', 'SPLSCN_CAT');
                $f->on('RETSCN_LINE', '=', 'SPLSCN_LINE');
                $f->on('RETSCN_FEDR', '=', 'SPLSCN_FEDR');
                $f->on('RETSCN_ORDERNO', '=', 'SPLSCN_ORDERNO');
                $f->on('RETSCN_LOT', '=', 'SPLSCN_LOTNO');
            })
            ->leftjoin('PSI_RPCUST.dbo.RPSCRAP_HIST_DET', function ($f2) {
                $f2->on('SCR_PSN', '=', 'SPLSCN_DOC');
                $f2->on('SCR_CAT', '=', 'SPLSCN_CAT');
                $f2->on('SCR_LINE', '=', 'SPLSCN_LINE');
                $f2->on('SCR_FR', '=', 'SPLSCN_FEDR');
                $f2->on('SCR_MCZ', '=', 'SPLSCN_ORDERNO');
                $f2->on('SCR_ITMCD', '=', 'SPLSCN_ITMCD');
            })
            ->where('SPLSCN_DOC', $psn)
            // ->where('SPLSCN_CAT', $cat)
            ->where('SPLSCN_LINE', $line)
            ->where('SPLSCN_FEDR', $fr)
            // ->where('SPLSCN_ORDERNO', $orderNo)
            ->where('SPLSCN_ITMCD', $item)
            ->groupBy(
                [
                    'SPLSCN_DOC',
                    'SPLSCN_CAT',
                    'SPLSCN_LINE',
                    'SPLSCN_FEDR',
                    'SPLSCN_ORDERNO',
                    'SPLSCN_ITMCD',
                    'SPLSCN_LOTNO',
                    'SPLSCN_LUPDT'
                ]
            )
            ->orderBy('SPLSCN_LOTNO', 'DESC')
            ->get()
            ->toArray();

        return $data;
    }

    public function getListLotFromSERD($psn, $line, $item, $job)
    {
        $getData = SERDTBL::select(
            '*',
            DB::raw(
                '(SELECT TOP 1
                    RCV_PRPRC
                    FROM RCVSCN_TBL
                    INNER JOIN RCV_TBL ON RCVSCN_DONO = RCV_DONO
                        AND RCVSCN_ITMCD = RCV_ITMCD
                    WHERE RCVSCN_ITMCD = SERD_ITMCD
                    AND RCVSCN_LOTNO = SERD_LOTNO
                    ORDER BY RCV_RPDATE DESC) as PRICE'
            ),
        )
            ->where('SERD_PSNNO', $psn)
            ->where('SERD_LINENO', $line)
            ->where('SERD_ITMCD', $item)
            ->where('SERD_JOB', $job)
            ->get()
            ->toArray();

        return $getData;
    }

    public function getFIFOLOT($process, $qty, $hasil = [])
    {
        $cekProcessNow = current($process);

        if (!empty($cekProcessNow)) {
            $total = (int)$qty - (int)$cekProcessNow['SPLSCN_QTY'];
            if ($total > 0) {
                $hasil = array_merge($hasil, [array_merge($cekProcessNow, ['QTY_USED' => (int)$cekProcessNow['SPLSCN_QTY']])]);
                next($process);
                return $this->getFIFOLOT($process, $total, $hasil);
            } else {
                $hasil = array_merge($hasil, [array_merge($cekProcessNow, ['QTY_USED' => $qty])]);
                return $hasil;
            }
        } else {
            return $hasil;
        }
    }

    public function getFIFOLOTSERD($process, $qty, $hasil = [])
    {
        $cekProcessNow = current($process);

        if (!empty($cekProcessNow)) {
            $totalInserted = 0;
            // logger(json_encode($hasil));
            foreach ($hasil as $key => $value) {
                if ($value['SERD_ITMCD'] == $cekProcessNow['SERD_ITMCD']) {
                    $totalInserted += $value['QTY_USED'];
                }
            }

            $total = ((int)$qty - $totalInserted) - (int)$cekProcessNow['SERD_QTY'];
            if (((int)$qty - $totalInserted) > 0 && $total > 0) {
                $hasil = array_merge($hasil, [array_merge($cekProcessNow, ['QTY_USED' => (int)$cekProcessNow['SERD_QTY']])]);
                next($process);

                // logger('---------- on qty still on stock ------------');
                // logger(json_encode($hasil));
                return $this->getFIFOLOTSERD($process, $total, $hasil);
            } else {
                $hasil = array_merge($hasil, [array_merge($cekProcessNow, ['QTY_USED' => $qty])]);

                // logger('---------- on qty stock alredy full ------------');
                // logger(json_encode($hasil));
                return $hasil;
            }
        } else {
            return $hasil;
        }
    }

    public function sendingParamBCStock($item_num, $date_out = '', $lot = null, $qty = 0, $doc = null, $bc = null, $loc = '')
    {
        $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item_num,
            'date_out' => empty($date_out) ? date('Y-m-d') : $date_out,
            'lot' => $lot,
            'qty' => $qty,
            'doc' => $doc,
            'bc' => $bc,
            'loc' => $loc
            // 'scrap' => true,
            // 'remark' => $id,
            // 'revise' => $rev
        ];

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content['CURL'];
    }

    public function sendMigrationsSP($item, $qty, $lot, $id)
    {
        $result =  DB::select(
            "SET NOCOUNT ON; EXEC PSI_RPCUST.dbo.sp_calculation_exbc
                @item_num= '" . $item . "',
                @doc_out= '" . $id . "',
                @lot = '" . $lot . "',
                @date_out= '" . date('Y-m-d') . "',
                @qty= " . (int)$qty . ",
                @is_saved= 1"
        );

        return $result;
    }

    public function sendMigrationsSPQuery($item, $qty, $lot, $id)
    {
        $result =  "SET NOCOUNT ON; EXEC PSI_RPCUST.dbo.sp_calculation_exbc
                @item_num= '" . $item . "',
                @doc_out= '" . $id . "',
                @lot = '" . $lot . "',
                @date_out= '" . date('Y-m-d') . "',
                @qty= " . (int)$qty . ",
                @is_saved= 1";

        return $result;
    }

    public function insertToITH($item, $date, $form, $doc, $qty, $wh, $ser, $remark, $user)
    {
        ITHMaster::insert([
            'ITH_ITMCD' => $item,
            'ITH_DATE' => $date,
            'ITH_FORM' => $form,
            'ITH_DOC' => $doc,
            'ITH_QTY' => $qty,
            'ITH_WH' => $wh,
            'ITH_LOC' => '',
            'ITH_SER' => $ser,
            'ITH_REMARK' => $remark,
            'ITH_LINE' => DB::raw('dbo.fun_ithline()'),
            'ITH_LUPDT' => date('Y-m-d H:i:s'),
            'ITH_USRID' => $user,
        ]);
    }
}
