<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Model\RPCUST\StockExBC;
use Illuminate\Support\Facades\DB;
class dataMigrationSMTToBC implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $item, $qty, $date;
    public function __construct($item, $qty, $date)
    {
        $this->item = $item;
        $this->qty = $qty;
        $this->date = $date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $getStockEXBC = StockExBC::select(
            'RPSTOCK_ITMNUM',
            DB::raw('SUM(TOTAL_INC) AS TOTAL_INC')
        )
        ->where('RPSTOCK_ITMNUM', $this->item)
        ->where('RPSTOCK_BCDATE', '<', '2021-03-28')
        ->groupBy(
            'RPSTOCK_ITMNUM'
        )
        ->first();

        if ((int)$getStockEXBC['TOTAL_INC'] < (int) $this->qty) {
            $kirim = $this->sendParam2([
                'item_num' => $this->item,
                'adj' => true,
                'qty' =>   $this->qty * -1,
                'less_than' => '2021-03-28'
            ]);
            $hasilStok = 'Stok BC kurang dari stok SMT';
        } elseif ((int)$getStockEXBC['TOTAL_INC'] > (int) $this->qty) {
            $kirim = $this->sendParam2([
                'item_num' =>  $this->qty,
                'adj' => true,
                'qty' =>  $this->item,
                'less_than' => '2021-03-28'
            ]);
            // $kirim = "";
            $hasilStok = 'Stok SMT kurang dari stok BC';
        }
    }

    public function sendParam2($param)
    {
        $endpoint = 'http://192.168.0.29:8081/api-report-custom/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content;
    }
}
