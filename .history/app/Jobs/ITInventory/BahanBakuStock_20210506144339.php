<?php

namespace App\Jobs\ITInventory;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Model\RPCUST\MutasiRaw;

class BahanBakuStock implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        MutasiRaw::updateOrCreate([
            'RPRAW_REF' => $this->data['RPRAW_REF'],
            'RPRAW_ITMCOD' => $this->data['RPRAW_ITMCOD']
        ],[
            'RPRAW_ITMCOD' => $this->data['RPRAW_ITMCOD'],
            'RPRAW_UNITMS' => $this->data['RPRAW_UNITMS'],
            'RPRAW_DATEIS' => $this->data['RPRAW_DATEIS'],
            'RPRAW_QTYINC' => $this->data['RPRAW_QTYINC'],
            'RPRAW_QTYOUT' => $this->data['RPRAW_QTYOUT'],
            'RPRAW_QTYADJ' => $this->data['RPRAW_QTYADJ'],
            'RPRAW_QTYTOT' => $this->data['RPRAW_QTYTOT'],
            'RPRAW_QTYOPN' => $this->data['RPRAW_QTYOPN'],
            'RPRAW_KET' => $this->data['RPRAW_KET'],
            'RPRAW_REF' => $this->data['RPRAW_REF']
        ]);
    }
}
