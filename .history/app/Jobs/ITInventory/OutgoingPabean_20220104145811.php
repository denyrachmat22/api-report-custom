<?php

namespace App\Jobs\ITInventory;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Model\MASTER\DLVMaster;
use App\Model\RPCUST\MutasiDokPabean;

use Illuminate\Support\Facades\DB;

class OutgoingPabean implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $sj;
    protected $isMtr;
    public function __construct($sj, $isMtr)
    {
        $this->sj = base64_decode($sj);
        $this->isMtr = $isMtr;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $headerData = DLVMaster::select(
            'DLV_BCTYPE', //RPBEA_JENBEA
            'DLV_NOAJU', //RPBEA_NUMBEA
            'DLV_NOPEN',
            'DLV_CONSIGN',
            !$this->isMtr
            ? 'SISCN_LINENO'
            : DB::raw("'' as SISCN_LINENO"),
            'DLV_BCDATE',
            'DLV_ID',
            'DLV_DATE',
            DB::raw("DLV_ZNOMOR_AJU AS DLV_RPNO"),
            'DLV_RPDATE',
            'DLV_CUSTCD',
            DB::raw("
                CASE WHEN DLV_ITMCD IS NULL OR DLV_ITMCD = ''
                    THEN ITH_ITMCD
                    ELSE DLV_ITMCD
                END AS DLV_ITMCD
            "),
            !$this->isMtr
            ? DB::raw("COALESCE((
                SELECT TOP 1 DLVPRC_PRC FROM DLVPRC_TBL
                WHERE DLVPRC_SILINE = SISCN_LINENO
                AND DLVPRC_SER = DLV_SER
            ), 0) AS DLV_PRPRC")
            : DB::raw('COALESCE(DLVRMDOC_PRPRC, 0) as DLV_PRPRC'),
            'DLV_QTY',
            'DLV_SER',
            DB::raw("(
                SELECT MITM_STKUOM FROM MITM_TBL
                WHERE MITM_ITMCD = (
                    CASE WHEN DLV_TBL.DLV_ITMCD IS NULL OR DLV_TBL.DLV_ITMCD = ''
                        THEN ITH_ITMCD
                        ELSE DLV_TBL.DLV_ITMCD
                    END
                )
            ) AS MITM_STKUOM"),
            DB::raw("(
                SELECT MCUS_CURCD FROM MCUS_TBL
                    WHERE MCUS_CUSCD = DLV_CUSTCD
            ) AS MITM_SUPCR"),
            !$this->isMtr
            ? DB::raw("COALESCE((
                SELECT MAX(DLVPRC_PRC) FROM DLVPRC_TBL
                WHERE DLVPRC_SILINE = SISCN_LINENO
                AND DLVPRC_SER = DLV_SER
            ), 0) * DLV_QTY as DLV_TOT_HARGA")
            : DB::raw("DLVRMDOC_PRPRC * DLV_QTY as DLV_TOT_HARGA"),
            'DLV_INVDT',
            'DLV_ID',
            DB::raw("(
                SELECT MITM_MODEL FROM MITM_TBL
                WHERE MITM_ITMCD = (
                    CASE WHEN DLV_TBL.DLV_ITMCD IS NULL OR DLV_TBL.DLV_ITMCD = ''
                        THEN ITH_ITMCD
                        ELSE DLV_TBL.DLV_ITMCD
                    END
                )
            ) AS MITM_MODEL"),
            'DLV_PURPOSE'
            // !$isMtr ? DB::raw('0 AS DLVRMDOC_ITMQT') : DB::raw('COALESCE(DLVRMDOC_ITMQT, 0) AS DLVRMDOC_ITMQT'),
            // !$isMtr ? DB::raw("0 AS DLVRMDOC_PRPRC") : 'DLVRMDOC_PRPRC'
        );

        if (!$this->isMtr) {
            $headerData
            ->leftjoin(DB::raw("(
                SELECT * FROM ITH_TBL
                WHERE ITH_FORM IN ('OUT-SHP-FG', 'OUT-SHP-RM', 'RPR-INC', 'INC-SHP-FG')
                AND ITH_WH IN ('ARSHP', 'ARWH0PD', 'ARSHPRTN', 'ARSHPRTN2')
            ) AS ITH_TBL"), 'ITH_SER', 'DLV_SER')
            ->leftjoin('SISCN_TBL', 'DLV_SER', 'SISCN_SER');
        } else {
            $headerData
            ->leftjoin(DB::raw("(
                SELECT * FROM ITH_TBL
                WHERE ITH_FORM IN ('OUT-SHP-FG', 'OUT-SHP-RM', 'RPR-INC', 'INC-SHP-FG')
                AND ITH_WH IN ('ARSHP', 'ARWH0PD', 'ARSHPRTN', 'ARSHPRTN2')
            ) AS ITH_TBL"), function ($f) {
                $f->on('ITH_ITMCD', 'DLV_ITMCD')
                ->on('ITH_DOC', 'DLV_ID');
            })
            ->leftjoin('DLVRMDOC_TBL', function($j) {
                $j->on('DLVRMDOC_TXID', 'DLV_ID')
                ->on('DLVRMDOC_ITMID', 'DLV_ITMCD');
            });
        }

        if (!empty($this->sj)) {
            $headerData->where('DLV_ID', $this->sj);
        }

        $headerData->join('MITM_TBL', DB::raw("
            CASE WHEN DLV_ITMCD IS NULL OR DLV_ITMCD = ''
                THEN ITH_ITMCD
                ELSE DLV_ITMCD
            END
        "), 'MITM_ITMCD');

        logger($headerData->toSql());
        logger($this->sj);
        logger(json_encode($headerData->get()->toArray()));

        MutasiDokPabean::where('RPBEA_NUMSJL', $this->sj)->where('RPBEA_TYPE', 'OUT')->delete();

        foreach ($headerData->get() as $key => $value) {
            if (
                ($value['DLV_PURPOSE'] == 6
                && !empty($value['DLV_RPNO'])
                && !empty($value['DLV_NOPEN'])) ||
                (floatval($value['DLV_PRPRC']) > 0
                && !empty($value['DLV_RPNO'])
                && !empty($value['DLV_NOPEN']))
            ) {
                // Insert Mutasi Pemasukan Pabean
                MutasiDokPabean::create([
                    'RPBEA_JENBEA' => $value['DLV_BCTYPE'],
                    'RPBEA_NUMBEA' => $value['DLV_NOPEN'],
                    'RPBEA_TGLBEA' => $value['DLV_RPDATE'],
                    'RPBEA_NUMSJL' => $value['DLV_ID'],
                    'RPBEA_TGLSJL' => $value['DLV_DATE'],
                    'RPBEA_NUMPEN' => $value['DLV_RPNO'],
                    'RPBEA_TGLPEN' => $value['DLV_BCDATE'],
                    'RPBEA_CUSSUP' => $value['DLV_CUSTCD'],
                    'RPBEA_ITMCOD' => $value['DLV_ITMCD'],
                    'RPBEA_QTYSAT' => floatval($value['DLV_PRPRC']),
                    'RPBEA_QTYJUM' => (int)$value['DLV_QTY'],
                    'RPBEA_UNITMS' => $value['MITM_STKUOM'],
                    'RPBEA_VALAS' => $value['MITM_SUPCR'],
                    'RPBEA_QTYTOT' => (int)$value['DLV_QTY'],
                    'RPBEA_TYPE' => 'OUT',
                    'RPBEA_RCVEDT' => $value['DLV_INVDT'],
                    'RPBEA_DOC' => !$this->isMtr ? $value['DLV_SER'] . '-' . $value['SISCN_LINENO'] : '',
                    'RPBEA_CONSIGNEE' => $value['DLV_CONSIGN']
                ]);

                // Kirim Bahan Baku jika incoming nya adalah material
                if ($value['MITM_MODEL'] == 0) {
                    $arrBahanBaku = [
                        'RPRAW_ITMCOD' => $value['DLV_ITMCD'],
                        'RPRAW_UNITMS' => $value['MITM_STKUOM'],
                        'RPRAW_DATEIS' => $value['DLV_BCDATE'],
                        'RPRAW_QTYINC' => 0,
                        'RPRAW_QTYOUT' => (int)$value['DLV_QTY'],
                        'RPRAW_QTYADJ' => 0,
                        'RPRAW_QTYTOT' => (int)$value['DLV_QTY'],
                        'RPRAW_QTYOPN' => 0,
                        'RPRAW_KET' => 'OUT-DLV',
                        'RPRAW_REF' => $value['DLV_ID'],
                    ];

                    // $insertBahanBaku = (new BahanBakuStock($arrBahanBaku));
                    // dispatch($insertBahanBaku)->onQueue('BahanBakuStock');
                } elseif ($value['MITM_MODEL'] == 1) { // Kirim Finish Good jika incoming nya adalah Finish Good
                    $arrFinishGood = [
                        'RPGOOD_ITMCOD' => $value['DLV_ITMCD'],
                        'RPGOOD_UNITMS' => $value['MITM_STKUOM'],
                        'RPGOOD_DATEIS' => $value['DLV_BCDATE'],
                        'RPGOOD_QTYINC' => 0,
                        'RPGOOD_QTYOUT' => (int)$value['DLV_QTY'],
                        'RPGOOD_QTYADJ' => 0,
                        'RPGOOD_QTYTOT' => (int)$value['DLV_QTY'],
                        'RPGOOD_QTYOPN' => 0,
                        'RPGOOD_KET' => 'OUT-DLV',
                        'RPGOOD_REF' => $value['DLV_SER'] . '-' . $value['SISCN_LINENO'],
                    ];

                    // $insertBahanBaku = (new FinishGoodStock($arrFinishGood));
                    // dispatch($insertBahanBaku)->onQueue('FinishGoodStock');
                } elseif ($value['MITM_MODEL'] == 6) { // Kirim Mesin & Peralatan jika incoming nya adalah Mesin & Peralatan
                    $arrMesin = [
                        'RPMCH_ITMCOD' => $value['DLV_ITMCD'],
                        'RPMCH_UNITMS' => $value['MITM_STKUOM'],
                        'RPMCH_DATEIS' => $value['DLV_BCDATE'],
                        'RPMCH_QTYINC' => 0,
                        'RPMCH_QTYOUT' => (int)$value['DLV_QTY'],
                        'RPMCH_QTYADJ' => 0,
                        'RPMCH_QTYTOT' => (int)$value['DLV_QTY'],
                        'RPMCH_QTYOPN' => 0,
                        'RPMCH_KET' => 'OUT-DLV',
                        'RPMCH_REF' => $value['DLV_ID'],
                    ];

                    // $insertMesin = (new MesinPeralatanStock($arrMesin));
                    // dispatch($insertMesin)->onQueue('MesinStock');
                }
            }
        }
    }

    public function getSISO($ref)
    {
        $getSISO = DB::select(DB::raw("
            SELECT
                V1.*,
                V2.*,
                SI_ITMCD,
                SI_BSGRP,
                SI_CUSCD
            FROM
                (
                select
                    SISCN_LINENO,
                    sum(SISCN_SERQTY) SCNQTY
                from
                    SISCN_TBL
                where
                    SISCN_SER in (" . $ref . ")
                group by
                    SISCN_LINENO) V1
            LEFT JOIN (
                SELECT
                    SISO_HLINE,
                    SSO2_SLPRC,
                    SUM(SISO_QTY) PLOTQTY,
                    MAX(SISO_SOLINE) SISO_SOLINE
                FROM
                    SISO_TBL
                LEFT JOIN XSSO2 ON
                    SISO_CPONO = SSO2_CPONO
                    AND SSO2_SOLNO = SISO_SOLINE
                WHERE
                    SISO_QTY>0
                GROUP BY
                    SISO_HLINE,
                    SSO2_SLPRC,
                    SSO2_MDLCD) V2 ON
                SISCN_LINENO = SISO_HLINE
            LEFT JOIN SI_TBL ON
                SISCN_LINENO = SI_LINENO
        "));

        return $getSISO;
    }
}
