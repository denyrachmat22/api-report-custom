<?php

namespace App\Jobs\ITInventory;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Model\RPCUST\SaldoWIP;

class WIPStock implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data['RPWIP_JOB'])) {
            SaldoWIP::updateOrCreate([
                'RPWIP_TYPE' => $this->data['RPWIP_TYPE'],
                'RPWIP_PSN' => $this->data['RPWIP_PSN'],
                'RPWIP_CAT' => $this->data['RPWIP_CAT'],
                'RPWIP_LINE' => $this->data['RPWIP_LINE'],
                'RPWIP_FR' => $this->data['RPWIP_FR'],
                'RPWIP_JOB' => $this->data['RPWIP_JOB'],
                'RPWIP_MCH' => $this->data['RPWIP_MCH'],
                'RPWIP_LOT' => $this->data['RPWIP_LOT'],
                'RPWIP_SER' => $this->data['RPWIP_SER']
            ],[
                'RPWIP_DATEIS' => $this->data['RPWIP_DATEIS'],
                'RPWIP_ITMCOD' => $this->data['RPWIP_ITMCOD'],
                'RPWIP_UNITMS' => $this->data['RPWIP_UNITMS'],
                'RPWIP_QTYTOT' => $this->data['RPWIP_QTYTOT'],
                'RPWIP_TYPE' => $this->data['RPWIP_TYPE'],
                'RPWIP_PSN' => $this->data['RPWIP_PSN'],
                'RPWIP_CAT' => $this->data['RPWIP_CAT'],
                'RPWIP_LINE' => $this->data['RPWIP_LINE'],
                'RPWIP_FR' => $this->data['RPWIP_FR'],
                'RPWIP_JOB' => $this->data['RPWIP_JOB'],
                'RPWIP_MCH' => $this->data['RPWIP_MCH'],
                'RPWIP_LOT' => $this->data['RPWIP_LOT'],
                'RPWIP_SER' => $this->data['RPWIP_SER']
            ]);
        } else {
            SaldoWIP::updateOrCreate([
                'RPWIP_TYPE' => $this->data['RPWIP_TYPE'],
                'RPWIP_PSN' => $this->data['RPWIP_PSN'],
                'RPWIP_CAT' => $this->data['RPWIP_CAT'],
                'RPWIP_LINE' => $this->data['RPWIP_LINE'],
                'RPWIP_FR' => $this->data['RPWIP_FR'],
                'RPWIP_MCH' => $this->data['RPWIP_MCH'],
                'RPWIP_LOT' => $this->data['RPWIP_LOT'],
                'RPWIP_SER' => $this->data['RPWIP_SER']
            ],[
                'RPWIP_DATEIS' => $this->data['RPWIP_DATEIS'],
                'RPWIP_ITMCOD' => $this->data['RPWIP_ITMCOD'],
                'RPWIP_UNITMS' => $this->data['RPWIP_UNITMS'],
                'RPWIP_QTYTOT' => $this->data['RPWIP_QTYTOT'],
                'RPWIP_TYPE' => $this->data['RPWIP_TYPE'],
                'RPWIP_PSN' => $this->data['RPWIP_PSN'],
                'RPWIP_CAT' => $this->data['RPWIP_CAT'],
                'RPWIP_LINE' => $this->data['RPWIP_LINE'],
                'RPWIP_FR' => $this->data['RPWIP_FR'],
                'RPWIP_MCH' => $this->data['RPWIP_MCH'],
                'RPWIP_LOT' => $this->data['RPWIP_LOT'],
                'RPWIP_SER' => $this->data['RPWIP_SER']
            ]);
        }
    }
}
