<?php

namespace App\Jobs\ITInventory;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Model\RPCUST\MutasiScrap;

class ScrapStock implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        MutasiScrap::updateOrCreate([
            'RPREJECT_REF' => $this->data['RPREJECT_REF'],
            'RPREJECT_ITMCOD' => $this->data['RPREJECT_ITMCOD']
        ],[
            'RPREJECT_UNITMS' => $this->data['RPREJECT_UNITMS'],
            'RPREJECT_QTYTOT' => $this->data['RPREJECT_QTYTOT'],
            'RPREJECT_QTYOUT' => $this->data['RPREJECT_QTYOUT'],
            'RPREJECT_QTYOPN' => $this->data['RPREJECT_QTYOPN'],
            'RPREJECT_QTYINC' => $this->data['RPREJECT_QTYINC'],
            'RPREJECT_QTYADJ' => $this->data['RPREJECT_QTYADJ'],
            'RPREJECT_KET' => $this->data['RPREJECT_KET'],
            'RPREJECT_ITMCOD' => $this->data['RPREJECT_ITMCOD'],
            'RPREJECT_DATEIS' => $this->data['RPREJECT_DATEIS'],
            'RPREJECT_REF' => $this->data['RPREJECT_REF']
        ]);
    }
}
