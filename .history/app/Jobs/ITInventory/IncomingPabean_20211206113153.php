<?php

namespace App\Jobs\ITInventory;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

use App\Model\WMS\REPORT\MutasiStok;
use App\Model\RPCUST\MutasiRaw;
use App\Model\RPCUST\MutasiDokPabean;
use App\Model\RPCUST\DetailStock;
use App\Model\WMS\API\RCVSCN;

use App\Jobs\ITInventory\BahanBakuStock;
use App\Jobs\ITInventory\FinishGoodStock;
use App\Jobs\ITInventory\MesinPeralatanStock;

class IncomingPabean implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $sj, $item;
    public function __construct($sj, $item)
    {
        $this->sj = $this->validBase64($sj);
        $this->item = $this->validBase64($item);
    }

    public function validBase64($string)
    {
        if (base64_encode(base64_decode($string, true)) === $string) {
            return base64_decode($string);
        } else {
            return $string;
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $getData = $this->getData();


        DetailStock::where('RPSTOCK_TYPE', 'INC')
            ->where('RPSTOCK_DOC', $this->sj)
            ->delete();

        MutasiDokPabean::where('RPBEA_DOC', $this->sj)->delete();

        foreach ($getData as $key => $value) {
            if (empty($value['RCV_DONO_REFF']) && empty($value['RCV_ITMCD_REFF'])) {

                // Insert Mutasi Pemasukan Pabean
                MutasiDokPabean::create([
                    'RPBEA_JENBEA' => $value['RCV_BCTYPE'],
                    'RPBEA_NUMBEA' => $value['RCV_BCNO'],
                    'RPBEA_TGLBEA' => $value['RCV_BCDATE'],
                    'RPBEA_NUMSJL' => $value['RCV_DONO'],
                    'RPBEA_TGLSJL' => $value['RCV_INVDATE'],
                    'RPBEA_NUMPEN' => $value['RCV_RPNO'],
                    'RPBEA_TGLPEN' => $value['RCV_RPDATE'],
                    'RPBEA_CUSSUP' => $value['RCV_SUPCD'],
                    'RPBEA_ITMCOD' => $value['RCV_ITMCD'],
                    'RPBEA_QTYSAT' => $value['RCV_PRPRC'],
                    'RPBEA_QTYJUM' => (int)$value['RCV_QTY'],
                    'RPBEA_UNITMS' => $value['MITM_STKUOM'],
                    'RPBEA_VALAS' => $value['MSUP_SUPCR'],
                    'RPBEA_QTYTOT' => (int)$value['RCV_QTY'],
                    'RPBEA_TYPE' => 'INC',
                    'RPBEA_RCVEDT' => $value['RCV_RCVDATE'],
                    'RPBEA_DOC' => $value['RCV_DONO']
                ]);

                // logger(json_encode($value));

                // Kirim Bahan Baku jika incoming nya adalah material
                if ($value['MITM_MODEL'] == '0') {
                    $getDataItem = array_values(array_filter($getData, function ($f) use ($value) {
                        return $f['RCV_DONO'] == $value['RCV_DONO']
                            && $f['RCV_ITMCD'] == $value['RCV_ITMCD']
                            && $f['RCV_BCDATE'] == $value['RCV_BCDATE']
                            && $f['RCV_RPNO'] == $value['RCV_RPNO'];
                    }));

                    $totalQTY = 0;
                    foreach ($getDataItem as $keyGetTotQTY => $valueGetTotQTY) {
                        $totalQTY += intval($valueGetTotQTY['RCV_QTY']);
                    }

                    $cek = DetailStock::create(
                        [
                            'RPSTOCK_BCTYPE' => $value['RCV_BCTYPE'],
                            'RPSTOCK_BCNUM' => $value['RCV_BCNO'],
                            'RPSTOCK_BCDATE' => $value['RCV_BCDATE'],
                            'RPSTOCK_QTY' => $totalQTY > 0 ? $totalQTY : intval($value['RCV_QTY']),
                            'RPSTOCK_DOC' => $value['RCV_DONO'],
                            'RPSTOCK_TYPE' => 'INC',
                            'RPSTOCK_ITMNUM' => $value['RCV_ITMCD'],
                            'RPSTOCK_NOAJU' => $value['RCV_RPNO'],
                        ]
                    );

                    // Update out jika ada out
                    $cekOut = DetailStock::where('RPSTOCK_DOC', $value['RCV_DONO'])
                        ->where('RPSTOCK_ITMNUM', $value['RCV_ITMCD'])
                        ->where('RPSTOCK_BCDATE', $value['RCV_BCDATE'])
                        ->where('RPSTOCK_NOAJU', $value['RCV_RPNO'])
                        ->where('RPSTOCK_TYPE', 'OUT')
                        ->first();

                    if (!empty($cekOut)) {
                        DetailStock::updateOrCreate(
                            [
                                'RPSTOCK_DOC' => $value['RCV_DONO'],
                                'RPSTOCK_ITMNUM' => $value['RCV_ITMCD'],
                                'RPSTOCK_BCDATE' => $value['RCV_BCDATE'],
                                'RPSTOCK_NOAJU' => $value['RCV_RPNO'],
                                'RPSTOCK_TYPE' => 'OUT'
                            ],
                            [
                                'RPSTOCK_BCTYPE' => $value['RCV_BCTYPE'],
                                'RPSTOCK_BCNUM' => $value['RCV_BCNO'],
                                'RPSTOCK_BCDATE' => $value['RCV_BCDATE'],
                                'RPSTOCK_DOC' => $value['RCV_DONO'],
                                'RPSTOCK_TYPE' => 'OUT',
                                'RPSTOCK_ITMNUM' => $value['RCV_ITMCD'],
                                'RPSTOCK_NOAJU' => $value['RCV_RPNO'],
                            ]
                        );
                    }

                    // $arrBahanBaku = [
                    //     'RPRAW_ITMCOD' => $value['RCV_ITMCD'],
                    //     'RPRAW_UNITMS' => $value['MITM_STKUOM'],
                    //     'RPRAW_DATEIS' => $value['RCV_BCDATE'],
                    //     'RPRAW_QTYINC' => (int)$value['RCV_QTY'],
                    //     'RPRAW_QTYOUT' => 0,
                    //     'RPRAW_QTYADJ' => 0,
                    //     'RPRAW_QTYTOT' => (int)$value['RCV_QTY'],
                    //     'RPRAW_QTYOPN' => 0,
                    //     'RPRAW_KET' => 'INC-DO',
                    //     'RPRAW_REF' => $value['RCV_DONO'],
                    // ];

                    // $insertBahanBaku = (new BahanBakuStock($arrBahanBaku));
                    // dispatch($insertBahanBaku)->onQueue('BahanBakuStock');
                } elseif ($value['MITM_MODEL'] == '1') { // Kirim Finish Good jika incoming nya adalah Finish Good
                    $arrFinishGood = [
                        'RPGOOD_ITMCOD' => $value['RCV_ITMCD'],
                        'RPGOOD_UNITMS' => $value['MITM_STKUOM'],
                        'RPGOOD_DATEIS' => $value['RCV_BCDATE'],
                        'RPGOOD_QTYINC' => (int)$value['RCV_QTY'],
                        'RPGOOD_QTYOUT' => 0,
                        'RPGOOD_QTYADJ' => 0,
                        'RPGOOD_QTYTOT' => (int)$value['RCV_QTY'],
                        'RPGOOD_QTYOPN' => 0,
                        'RPGOOD_KET' => 'INC-DO',
                        'RPGOOD_REF' => $value['RCV_DONO'],
                    ];

                    $cek = DetailStock::create(
                        [
                            'RPSTOCK_BCTYPE' => $value['RCV_BCTYPE'],
                            'RPSTOCK_BCNUM' => $value['RCV_BCNO'],
                            'RPSTOCK_BCDATE' => $value['RCV_BCDATE'],
                            'RPSTOCK_QTY' => intval($value['RCV_QTY']),
                            'RPSTOCK_DOC' => $value['RCV_DONO'],
                            'RPSTOCK_TYPE' => 'INC',
                            'RPSTOCK_ITMNUM' => $value['RCV_ITMCD'],
                            'RPSTOCK_NOAJU' => $value['RCV_RPNO'],
                        ]
                    );

                    // $insertModel = (new FinishGoodStock($arrFinishGood));
                    // dispatch($insertModel)->onQueue('FinishGoodStock');
                } elseif ($value['MITM_MODEL'] == '6') { // Kirim Mesin & Peralatan jika incoming nya adalah Mesin & Peralatan
                    $arrMesin = [
                        'RPMCH_ITMCOD' => $value['RCV_ITMCD'],
                        'RPMCH_UNITMS' => $value['MITM_STKUOM'],
                        'RPMCH_DATEIS' => $value['RCV_BCDATE'],
                        'RPMCH_QTYINC' => (int)$value['RCV_QTY'],
                        'RPMCH_QTYOUT' => 0,
                        'RPMCH_QTYADJ' => 0,
                        'RPMCH_QTYTOT' => (int)$value['RCV_QTY'],
                        'RPMCH_QTYOPN' => 0,
                        'RPMCH_KET' => 'INC-DO',
                        'RPMCH_REF' => $value['RCV_DONO'],
                    ];

                    $cek = DetailStock::create(
                        [
                            'RPSTOCK_BCTYPE' => $value['RCV_BCTYPE'],
                            'RPSTOCK_BCNUM' => $value['RCV_BCNO'],
                            'RPSTOCK_BCDATE' => $value['RCV_BCDATE'],
                            'RPSTOCK_QTY' => intval($value['RCV_QTY']),
                            'RPSTOCK_DOC' => $value['RCV_DONO'],
                            'RPSTOCK_TYPE' => 'INC',
                            'RPSTOCK_ITMNUM' => $value['RCV_ITMCD'],
                            'RPSTOCK_NOAJU' => $value['RCV_RPNO'],
                        ]
                    );

                    // $insertMesin = (new MesinPeralatanStock($arrMesin));
                    // dispatch($insertMesin)->onQueue('MesinStock');
                }
            } else {
                $getDataItem = array_values(array_filter($getData, function ($f) use ($value) {
                    return $f['RCV_DONO'] == $value['RCV_DONO']
                        && $f['RCV_ITMCD'] == $value['RCV_ITMCD']
                        && $f['RCV_BCDATE'] == $value['RCV_BCDATE']
                        && $f['RCV_RPNO'] == $value['RCV_RPNO'];
                }));

                $totalQTY = 0;
                foreach ($getDataItem as $keyGetTotQTY => $valueGetTotQTY) {
                    $totalQTY += intval($valueGetTotQTY['RCV_QTY']);
                }

                $cek = DetailStock::updateOrCreate(
                    [
                        'RPSTOCK_DOC' => $value['RCV_DONO'],
                        'RPSTOCK_ITMNUM' => $value['RCV_ITMCD'],
                        'RPSTOCK_BCDATE' => $value['RCV_BCDATE'],
                        'RPSTOCK_NOAJU' => $value['RCV_RPNO'],
                        'RPSTOCK_TYPE' => 'INC'
                    ],
                    [
                        'RPSTOCK_BCTYPE' => $value['RCV_BCTYPE'],
                        'RPSTOCK_BCNUM' => $value['RCV_BCNO'],
                        'RPSTOCK_BCDATE' => $value['RCV_BCDATE'],
                        'RPSTOCK_QTY' => $totalQTY > 0 ? $totalQTY : intval($value['RCV_QTY']),
                        'RPSTOCK_DOC' => $value['RCV_DONO'],
                        'RPSTOCK_TYPE' => 'INC',
                        'RPSTOCK_ITMNUM' => $value['RCV_ITMCD'],
                        'RPSTOCK_NOAJU' => $value['RCV_RPNO'],
                    ]
                );
            }
        }
    }

    public function getData()
    {
        $headerData = MutasiStok::select(
            'RCV_BCTYPE',
            'RCV_BCNO',
            'RCV_BCDATE',
            'RCV_DONO',
            'RCV_INVDATE',
            'RCV_RPNO',
            'RCV_RPDATE',
            'RCV_SUPCD',
            'RCV_ITMCD',
            'RCV_PRPRC',
            'MITM_STKUOM',
            DB::raw('MSUP_SUPCR'),
            DB::raw('SUM(RCV_QTY) as RCV_QTY'),
            'RCV_RCVDATE',
            'RCV_DONO',
            'MITM_MODEL',
            'RCV_DONO_REFF',
            'RCV_ITMCD_REFF'
        )
            ->leftjoin('PSI_WMS.dbo.MITM_TBL', 'RCV_ITMCD', 'MITM_ITMCD')
            ->leftjoin('PSI_WMS.dbo.MSUP_TBL', 'RCV_SUPCD', 'MSUP_SUPCD');

        if (!empty($this->sj)) {
            $headerData->where('RCV_DONO', $this->sj);
        }

        if (!empty($this->item)) {
            $headerData->where('RCV_ITMCD', $this->item);
        }

        $getData = $headerData->groupBy(
            'RCV_BCTYPE',
            'RCV_BCNO',
            'RCV_BCDATE',
            'RCV_DONO',
            'RCV_INVDATE',
            'RCV_RPNO',
            'RCV_RPDATE',
            'RCV_SUPCD',
            'RCV_ITMCD',
            'RCV_PRPRC',
            'MITM_STKUOM',
            'MSUP_SUPCR',
            'RCV_RCVDATE',
            'RCV_DONO',
            'MITM_MODEL',
            'RCV_DONO_REFF',
            'RCV_ITMCD_REFF'
        )->get()->toArray();

        return $getData;
    }

    public function getDataNew()
    {
        $select = [
            'RCV_BCTYPE',
            'RCV_BCNO',
            'RCV_RPNO',
            'RCV_BCDATE',
            'RCV_INVDATE',
            'RCV_DONO',
            'RCV_ITMCD_REFF',
            'RCV_DONO_REFF',
            'RCV_ITMCD',
            'RCVSCN_LOTNO',
            'MITM_ITMD1',
            'MITM_STKUOM',
            'RCV_PRPRC',
            'RCV_KPPBC',
            'RCV_HSCD',
            'RCV_ZNOURUT',
            'RCV_BM',
            'RCV_ZSTSRCV',
            'RCV_CONA'
        ];

        $getRCVSCN = RCVSCN::select(
            array_merge(
                $select,
                [
                    DB::raw("
                    (
                        SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                        FROM [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE rb.RPSTOCK_DOC = RCV_DONO
                        and rb.RPSTOCK_ITMNUM = RCV_ITMCD
                        and rb.RPSTOCK_BCDATE = RCV_BCDATE
                        and rb.deleted_at IS NULL
                    ) as STOCK"),
                    DB::raw("
                    (
                        SELECT
                            COALESCE(SUM(RPSTOCK_QTY),
                            0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCV_DONO_REFF
                            and rb.RPSTOCK_ITMNUM = RCV_ITMCD_REFF
                            and rb.RPSTOCK_BCDATE = RCV_BCDATE
                            and rb.deleted_at IS NULL
                    ) as STOCK_REF"),
                    DB::raw("COALESCE(SUM(CAST(RCVSCN_QTY as INT)), 0)
                    + (
                        SELECT
                            COALESCE(SUM(RPSTOCK_QTY),
                            0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCV_DONO
                            and rb.RPSTOCK_ITMNUM = RCV_ITMCD
                            and rb.RPSTOCK_BCDATE = RCV_BCDATE
                            and ISNULL(rb.RPSTOCK_LOT, 'LOT0') = ISNULL(RCVSCN_LOTNO, 'LOT0')
                            and rb.deleted_at IS NULL
                            and rb.RPSTOCK_TYPE = 'OUT'
                    )AS RCV_QTY ")
                ]
            )
        )
            ->join(DB::raw('(
            SELECT
                RCV_ITMCD,
                RCV_BCTYPE,
                RCV_BCNO,
                RCV_DONO,
                RCV_RPNO,
                RCV_BCDATE,
                RCV_ITMCD_REFF,
                RCV_DONO_REFF,
                MAX(RCV_KPPBC) AS RCV_KPPBC,
                MAX(RCV_HSCD) AS RCV_HSCD,
                MAX(RCV_ZNOURUT) AS RCV_ZNOURUT,
                MAX(RCV_PRPRC) AS RCV_PRPRC,
                MAX(RCV_BM) AS RCV_BM,
                MAX(RCV_ZSTSRCV) AS RCV_ZSTSRCV,
                MAX(RCV_CONA) AS RCV_CONA,
                sum(RCV_QTY) AS RCV_QTY
            FROM
                PSI_WMS.dbo.RCV_TBL
            GROUP BY
                RCV_ITMCD,
                RCV_BCTYPE,
                RCV_BCNO,
                RCV_DONO,
                RCV_RPNO,
                RCV_BCDATE,
                RCV_ITMCD_REFF,
                RCV_DONO_REFF
            ) a'), function ($j) {
                $j->on('RCVSCN_DONO', '=', 'RCV_DONO');
                $j->on('RCVSCN_ITMCD', '=', 'RCV_ITMCD');
            })
            ->join('PSI_WMS.dbo.XMITM_V', 'MITM_ITMCD', '=', 'RCV_ITMCD')
            ->groupBy($select);

        $getRCVSCN->havingRaw("
            (
                CASE
                    WHEN
                        RCV_ITMCD_REFF IS NOT NULL
                    THEN
                    (
                        SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCV_DONO_REFF
                        and rb.RPSTOCK_ITMNUM = RCV_ITMCD_REFF
                        and rb.RPSTOCK_BCDATE = RCV_BCDATE
                        and rb.deleted_at IS NULL
                    )
                    ELSE
                    (
                        SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCV_DONO
                        and rb.RPSTOCK_ITMNUM = RCV_ITMCD
                        and rb.RPSTOCK_BCDATE = RCV_BCDATE
                        and rb.deleted_at IS NULL
                    )
                    END
            ) > 0");

        if (!empty($this->sj)) {
            $getRCVSCN->where('RCV_DONO', $this->sj);
        }

        if (!empty($this->item)) {
            $getRCVSCN->where('RCV_ITMCD', $this->item);
        }

        return $getRCVSCN;
    }
}
