<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Model\RPCUST\DetailStock;
use Illuminate\Support\Facades\DB;

class adjEXBCQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $dataRet;
    private $item, $data, $adj, $customRemarks, $date, $lessThan;
    public function __construct($item, $data, $adj, $customRemarks = '', $date = '', $lessThan = '')
    {
        $this->item = $item;
        $this->data = $data;
        $this->adj = $adj;
        $this->customRemarks = $customRemarks;
        $this->date = $date;
        $this->lessThan = $lessThan;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dataEXBC = $this->checkStokEXBC((string)$this->item);

        if ($this->adj) {
            $qty = array_sum($this->data);
            $total = $qty - (int)$dataEXBC['QTY'];
        } else {
            $qty = $this->data;
            $total = $qty - (int)$dataEXBC['QTY'];
        }

        $hasilCompare = [];
        if ($total < 0) {
            $hasilCompare = [
                'ITEM' => $this->item,
                'QTY' => $qty,
                'EXBC_QTY' => (int)$dataEXBC['QTY'],
                'SELISIH' => $total,
                'CURL_STATUS' => $this->sendMigrationsSP((string)$this->item, $total * -1)
            ];
        } elseif ($total > 0) {
            $hasilCompare = [
                'ITEM' => $this->item,
                'QTY' => $qty,
                'EXBC_QTY' => (int)$dataEXBC['QTY'],
                'SELISIH' => $total,
                'CURL_STATUS' => 'qty EX-BC Less than qty stock take !'
            ];
            logger($this->item);
            logger($hasilCompare);
        }

        logger(json_encode($this->migrationSPQuery((string)$this->item, $total * -1)))
        // logger(json_encode($this->data));
        // logger(json_encode([$total, $this->item, $dataEXBC]));
    }

    public function checkStokEXBC($item)
    {
        $hasil = DetailStock::select(DB::raw('SUM(RPSTOCK_QTY) AS QTY'))
            ->where('RPSTOCK_ITMNUM', $item)
            ->first()
            ->toArray();

        return empty($this->lessThan)
            ? $hasil->where('RPSTOCK_BCDATE', '<=', '2021-09-26')->first()->toArray()
            : $hasil->first()->toArray();
    }

    public function sendMigrations($item, $qty)
    {
        $endpoint = 'http://localhost/api-report-custom/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item,
            'qty' => $qty,
            'doc' => empty($this->customRemarks) ? 'ADJ_' . date('Y_m_d') : $this->customRemarks,
            // 'adj' => false,
            'date_out' => '2021-09-26'
        ];

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content;
    }

    public function sendMigrationsSP($item, $qty)
    {
        $result =  DB::update(
            "SET NOCOUNT ON; EXEC PSI_RPCUST.dbo.sp_calculation_exbc
                @item_num= '" . $item . "',
                @doc_out= '". (empty($this->customRemarks) ? 'MIGRATION_' . date('y_m_d') : $this->customRemarks) ."',
                @date_out= '". (empty($this->date) ? date('Y-m-d') : $this->date) ."',
                @less_than = '". $this->lessThan ."',
                @qty= " . (int)$qty .",
                @is_saved= 1"
        );

        return $result;
    }

    public function migrationSPQuery($item, $qty)
    {
        $result =  "SET NOCOUNT ON; EXEC PSI_RPCUST.dbo.sp_calculation_exbc
            @item_num= '" . $item . "',
            @doc_out= '". (empty($this->customRemarks) ? 'MIGRATION_' . date('y_m_d') : $this->customRemarks) ."',
            @date_out= '". (empty($this->date) ? date('Y-m-d') : $this->date) ."',
            @less_than = '". $this->lessThan ."',
            @qty= " . (int)$qty .",
            @is_saved= 1";

        return $result;
    }
}
