<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class exBCCountingJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $item_num, $tujuan, $date_out, $lot, $bc, $doc, $kontrak, $qty;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($item_num, $tujuan, $date_out, $lot, $bc, $doc, $kontrak, $qty)
    {
        $this->item_num = $item_num;
        $this->tujuan = $tujuan;
        $this->date_out = $date_out;
        $this->lot = $lot;
        $this->bc = $bc;
        $this->doc = $doc;
        $this->kontrak = $kontrak;
        $this->qty = $qty;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }
}
