<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Http\Request;
use App\Model\WMS\API\SPLSCN;
use App\Model\RPCUST\scrapHist;
use App\Model\RPCUST\scrapHistDet;
use App\Model\RPCUST\processMstr;
use App\Model\RPCUST\processMapDet;
use App\Model\MASTER\MEGAEMS\ppsn1Table;
use Illuminate\Support\Facades\DB;

class scrapReportInsert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $req;

    public function __construct($req)
    {
        $this->req = $req;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Cek Last ID today
        $cek = scrapHist::whereBetween('created_at', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59'])->orderBy('created_at', 'desc')->withTrashed()->first();
        $idTrans = date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->ID_TRANS, -3) + 1));

        $hasil = [];

        $fetchProcessMaster = processMstr::get()->toArray();
        foreach ($req->data as $key => $value) {
            // Jika itemnya adalah Model
            if ($value['MITM_MODEL'] === '1') {
                // Parsing job yang di pilih
                foreach ($value['SCRAP_DET'] as $keyJobDet => $valueJobDet) {
                    // Parsing Process Master (PROCESS_DET)
                    foreach ($valueJobDet['PROCESS_DET'] as $keyProMstr => $valueProMstr) {
                        // Parsing Process Detail
                        foreach ($valueProMstr['DET'] as $keyProDet => $valueProDet) {

                            if (intval($valueProDet['QTY']) > 0) {
                                $insertHeader = scrapHist::create([
                                    'DSGN_MAP_ID' => $valueProDet['id'],
                                    'USERNAME' => $req->username,
                                    'DEPT' => $req->dept,
                                    'QTY' => $valueProDet['QTY'],
                                    'ID_TRANS' => $idTrans,
                                    'TYPE_TRANS' => $req->type,
                                    'DOC_NO' => $valueJobDet['PIS2_WONO'],
                                    'REASON' => isset($valueProDet['REASON']) ? $valueProDet['REASON'] : '',
                                    'SCR_PROCD' => $valueProDet['RPDSGN_PROCD'],
                                    'MDL_FLAG' => 1,
                                    'ITEMNUM' => $value['MITM_ITMCD'],
                                ]);

                                // Insert Detail Transaction

                                $findByProcess = $this->getPsnListByJobAndProcd(
                                    $fetchProcessMaster,
                                    $valueJobDet['PIS2_WONO'],
                                    $valueProDet['id'],
                                    $value['MITM_ITMCD']
                                );

                                // Fetch All PSN by process and job number
                                foreach ($findByProcess as $key => $value) {

                                    // Get Data LOT SPLSCN
                                    $getFIFOLotNo = $this->getDataSPLSCN(
                                        $value['PPSN1_PSNNO'],
                                        $value['PPSN2_ITMCAT'],
                                        $value['PPSN1_LINENO'],
                                        $value['PPSN1_FR'],
                                        $value['PPSN2_MCZ'],
                                        $value['PPSN2_SUBPN']
                                    );

                                    $totalQty = $valueProDet['QTY'] * floatVal($valueProDet['QTPER_VAL']);

                                    $fetchFIFOLotNo = $this->getFIFOLOT($getFIFOLotNo, $totalQty);

                                    foreach ($fetchFIFOLotNo as $keyFifoLot => $valueFifoLot) {
                                        # code...
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
            }
        }
    }

    // Helpernya

    public function psnListByJob($job, $procd = '')
    {
        $getData = ppsn1Table::select(
            'PPSN1_PSNNO',
            'PPSN2_ITMCAT',
            'PPSN1_LINENO',
            'PPSN1_FR',
            'PPSN2_MC',
            'PPSN2_MCZ',
            DB::raw('
            CASE WHEN PIS2_QTPER IS NULL
                THEN PPSN2_QTPER
                ELSE PIS2_QTPER
            END AS QTPER_VAL
            '),
            'PPSN2_SUBPN',
            'PPSN1_PROCD',
            'PPSN2_QTPER'
        )
            ->join('SRVMEGA.PSI_MEGAEMS.dbo.PPSN2_TBL', function ($f) {
                $f->on('PPSN1_PSNNO', 'PPSN2_PSNNO');
                $f->on('PPSN1_LINENO', 'PPSN2_LINENO');
                $f->on('PPSN1_FR', 'PPSN2_FR');
                $f->on('PPSN1_DOCNO', 'PPSN2_DOCNO');
                $f->on('PPSN1_BSGRP', 'PPSN2_BSGRP');
            })
            ->leftJoin('SRVMEGA.PSI_MEGAEMS.dbo.PIS2_TBL', function ($f) {
                $f->on('PIS2_WONO', 'PPSN1_WONO');
                $f->on('PIS2_PROCD', 'PPSN1_PROCD');
                $f->on('PIS2_ITMCD', 'PPSN2_SUBPN');
                $f->on('PIS2_MC', 'PPSN2_MC');
                $f->on('PIS2_MCZ', 'PPSN2_MCZ');
            })
            ->where('PPSN1_WONO', $job)
            ->groupBy(
                'PPSN1_DOCNO',
                'PPSN1_PSNNO',
                'PPSN2_ITMCAT',
                'PPSN1_LINENO',
                'PPSN1_FR',
                'PPSN1_PROCD',
                'PPSN2_SUBPN',
                'PPSN2_MC',
                'PPSN2_MCZ',
                'PPSN2_QTPER',
                'PIS2_QTPER'
            );

        if (empty($procd)) {
            return $getData->get()->toArray();
        } else {
            return $getData->where('PPSN1_PROCD', $procd)->get()->toArray();
        }
    }

    public function getDataSPLSCN($psn, $cat, $line, $fr, $orderNo, $item)
    {
        $data = SPLSCN::select([
            'SPLSCN_DOC',
            'SPLSCN_CAT',
            'SPLSCN_LINE',
            'SPLSCN_FEDR',
            'SPLSCN_ORDERNO',
            'SPLSCN_ITMCD',
            'SPLSCN_LOTNO',
            'SPLSCN_LUPDT',
            DB::raw('(SUM(SPLSCN_QTY) - COALESCE(SUM(RETSCN_QTYAFT), 0)) - COALESCE(SUM(SCR_QTY), 0) AS SPLSCN_QTY'),
            DB::raw('SUM(RETSCN_QTYAFT) AS RETURN_QTY'),
            DB::raw('COALESCE(SUM(SCR_QTY), 0) AS SCRAP_QTY'),
            DB::raw('SUM(SPLSCN_QTY) AS REAL_SPLSCN_QTY'),
            DB::raw('(SELECT TOP 1
                RCV_PRPRC
                FROM RCVSCN_TBL
                INNER JOIN RCV_TBL ON RCVSCN_DONO = RCV_DONO
                    AND RCVSCN_ITMCD = RCV_ITMCD
                WHERE RCVSCN_ITMCD = SPLSCN_ITMCD
                AND RCVSCN_LOTNO = SPLSCN_LOTNO
                ORDER BY RCV_RPDATE DESC) as PRICE')
        ])
            ->leftjoin('RETSCN_TBL', function ($f) {
                $f->on('RETSCN_ITMCD', '=', 'SPLSCN_ITMCD');
                $f->on('RETSCN_SPLDOC', '=', 'SPLSCN_DOC');
                $f->on('RETSCN_CAT', '=', 'SPLSCN_CAT');
                $f->on('RETSCN_LINE', '=', 'SPLSCN_LINE');
                $f->on('RETSCN_FEDR', '=', 'SPLSCN_FEDR');
                $f->on('RETSCN_ORDERNO', '=', 'SPLSCN_ORDERNO');
                $f->on('RETSCN_LOT', '=', 'SPLSCN_LOTNO');
            })
            ->leftjoin('PSI_RPCUST.dbo.RPSCRAP_HIST_DET', function ($f2) {
                $f2->on('SCR_PSN', '=', 'SPLSCN_DOC');
                $f2->on('SCR_CAT', '=', 'SPLSCN_CAT');
                $f2->on('SCR_LINE', '=', 'SPLSCN_LINE');
                $f2->on('SCR_FR', '=', 'SPLSCN_FEDR');
                $f2->on('SCR_MCZ', '=', 'SPLSCN_ORDERNO');
                $f2->on('SCR_ITMCD', '=', 'SPLSCN_ITMCD');
            })
            ->where('SPLSCN_DOC', $psn)
            ->where('SPLSCN_CAT', $cat)
            ->where('SPLSCN_LINE', $line)
            ->where('SPLSCN_FEDR', $fr)
            ->where('SPLSCN_ORDERNO', $orderNo)
            ->where('SPLSCN_ITMCD', $item)
            ->groupBy(
                [
                    'SPLSCN_DOC',
                    'SPLSCN_CAT',
                    'SPLSCN_LINE',
                    'SPLSCN_FEDR',
                    'SPLSCN_ORDERNO',
                    'SPLSCN_ITMCD',
                    'SPLSCN_LOTNO',
                    'SPLSCN_LUPDT'
                ]
            )
            ->orderBy('SPLSCN_LOTNO', 'DESC')
            ->get()
            ->toArray();

        return $data;
    }

    public function defineMappingScrap($data)
    {
        if ($data['RPDSGN_PRO_ID'] == 'SMT-HW' && !empty($data['RPDSGN_PROCD'])) {
            return 'SMT-HW';
        } elseif ($data['RPDSGN_PRO_ID'] == 'SMT-HWADD' && !empty($data['RPDSGN_PROCD'])) {
            return 'SMT-HWAD';
        } elseif ($data['RPDSGN_PRO_ID'] == 'SMT-SP' && !empty($data['RPDSGN_PROCD'])) {
            return 'SMT-SP';
        } else {
            return $data['RPDSGN_PROCD'];
        }
    }

    public function getPsnListByJobAndProcd($listProcess, $job, $idProcess, $item, $hasil = []) // Recursive here
    {
        $cekProcessNow = current($listProcess);
        if (!empty($cekProcessNow)) {
            $getDataMapping = processMapDet::where('RPDSGN_ITEMCD', $item)
                ->where('RPDSGN_PRO_ID', $cekProcessNow['RPDSGN_CODE'])
                ->orderBy('RPDSGN_PRO_ID')
                ->first();

            if ($idProcess == $cekProcessNow['RPDSGN_CODE']) {
                $hasil = array_merge($hasil, $this->psnListByJob($job, $this->defineMappingScrap($getDataMapping)));
                // $hasil[$cekProcessNow['RPDSGN_CODE']] = $this->psnListByJob($job, $this->defineMappingScrap($getDataMapping));
                return $hasil;
            } else {
                if (!empty($getDataMapping['RPDSGN_PROCD'])) {
                    $hasil = array_merge($hasil, $this->psnListByJob($job, $this->defineMappingScrap($getDataMapping)));
                    next($listProcess);
                    return $this->getPsnListByJobAndProcd($listProcess, $job, $idProcess, $item, $hasil);
                } else {
                    next($listProcess);
                    return $this->getPsnListByJobAndProcd($listProcess, $job, $idProcess, $item, $hasil);
                }
            }
        } else {
            return $hasil;
        }
    }

    public function getFIFOLOT($process, $qty, $hasil = [])
    {
        $cekProcessNow = current($process);

        if (!empty($cekProcessNow)) {
            $total = (int)$qty - (int)$cekProcessNow['SPLSCN_QTY'];
            if ($total > 0) {
                $hasil = array_merge($hasil, [array_merge($cekProcessNow, ['QTY_USED' => (int)$cekProcessNow['SPLSCN_QTY']])]);
                next($process);
                return $this->getFIFOLOT($process, $total, $hasil);
            } else {
                $hasil = array_merge($hasil, [array_merge($cekProcessNow, ['QTY_USED' => $qty])]);
                return $hasil;
            }
        } else {
            return $hasil;
        }
    }
}
