<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use LRedis;
use Illuminate\Support\Facades\DB;

use App\Model\RPCUST\scrapHist;
use App\Model\RPCUST\scrapHistDet;
use App\Model\WMS\API\SPLSCN;
use App\Model\WMS\API\RCVSCN2;

use App\Model\MASTER\ITHMaster;
use App\Model\WMS\REPORT\MutasiStok;

class scrapDetailMaterial implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $item, $id, $qty, $source, $date, $transID, $lot, $do;
    public function __construct($item, $id, $qty, $source, $date, $transID, $lot = '', $do = '')
    {
        $this->item = $item;
        $this->id = $id;
        $this->qty = $qty;
        $this->source = $source;
        $this->date = $date;
        $this->transID = $transID;
        $this->lot = $lot;
        $this->do = $do;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        set_time_limit(3600);

        logger('masuk sini');

        try {
            $datanya = $this->materialScrap(
                $this->item,
                $this->id,
                $this->qty,
                $this->source,
                $this->date,
                $this->transID,
                $this->lot,
                $this->do
            );

            if ($datanya['status'] === true) {
                $cekJob = scrapHist::where('id', $this->id)->first();
                $redis = LRedis::connection();

                $redis->publish('message', json_encode([
                    'app' => 'scrap_report',
                    'id' => $this->transID,
                    'id_transaction' => $this->id,
                    'item' => $this->item,
                    'item_desc' => $this->item,
                    'doc' => $cekJob->DOC_NO,
                    'status' => 'positive',
                    'message' => 'This ' . $cekJob->DOC_NO . ' detail is successfully inserted',
                    'user' => $cekJob->USERNAME,
                    'stat_print' => false
                ]));

                scrapHist::where('id', $this->id)->update(['IS_DONE' => 1]);

                $cekOk = scrapHist::where('ID_TRANS', $this->transID)->get()->toArray();

                $cekData = array_values(array_filter($cekOk, function ($f) {
                    return $f['IS_DONE'] == 1;
                }));

                if (count($cekData) === scrapHist::where('ID_TRANS', $this->transID)->count()) {
                    $redis->publish('message', json_encode([
                        'app' => 'scrap_report',
                        'id' => $this->transID,
                        'id_transaction' => $this->id,
                        'item' => $this->item,
                        'item_desc' => $this->item,
                        'doc' => $cekJob->DOC_NO,
                        'status' => 'positive',
                        'message' => 'This ' . $this->transID . ' already finished, you can print now.',
                        'user' => $cekJob->USERNAME,
                        'stat_print' => true
                    ]));
                }
            } else {
                logger(json_encode($datanya));
            }
        } catch (\Predis\Connection\ConnectionException $e) {
            return response('error connection redis');
        }
    }

    public function materialScrap($item, $id, $qty, $source, $date, $transID, $lot = '', $do = '')
    {
        $return = [];
        if (!empty($lot) && !empty($do)) {
            $cekRcvScnForPrice = $this->cekLatestHarga($item, $lot, $do);

            $insertDet = scrapHistDet::insertGetId([
                'SCR_HIST_ID' => $id,
                'SCR_PSN' => null,
                'SCR_CAT' => null,
                'SCR_LINE' => null,
                'SCR_FR' => null,
                'SCR_MC' => null,
                'SCR_MCZ' => null,
                'SCR_ITMCD' => $item,
                'SCR_QTPER' => 1,
                'SCR_QTY' => intVal($qty),
                'SCR_LOTNO' => $lot,
                'SCR_ITMPRC' => empty($cekRcvScnForPrice) ? 0 : $cekRcvScnForPrice['RCV_PRPRC']
            ]);

            $hasilBCStock = $this->sendingParamBCStock(
                trim($item),
                $date,
                $lot,
                round($qty),
                $transID,
                null,
                $id
            );

            if ($hasilBCStock['status'] == true) {
                $ith = [];
                if ($source === 'WH') {
                    $cekStockITH = ITHMaster::select(
                        'ITH_DOC',
                        'ITH_DATE',
                        'ITH_WH'
                    )
                        // ->where('ITH_DOC', $value['PPSN1_PSNNO'] . '|' . $value['PPSN2_ITMCAT'] . '|' . $value['PPSN1_LINENO'] . '|' . $value['PPSN1_FR'])
                        ->where('ITH_ITMCD', trim($item))
                        ->whereIn('ITH_WH', ['ARWH1', 'ARWH2'])
                        ->whereIn('ITH_FORM', ['INC-RET', 'INC-DO'])
                        ->first();

                    // Issue
                    // $ith[0] = $this->insertToITH(
                    //     trim($item),
                    //     $cekStockITH['ITH_DATE'],
                    //     'OUT-WH-RM',
                    //     $cekStockITH['ITH_DOC'],
                    //     round($qty) * -1,
                    //     $cekStockITH['ITH_WH'],
                    //     $insertDet,
                    //     $id,
                    //     'SCR_RPT'
                    // );
                } else {
                    $cekStockITH = ITHMaster::select(
                        'ITH_DOC',
                        'ITH_DATE',
                        'ITH_WH'
                    )
                        // ->where('ITH_DOC', $value['PPSN1_PSNNO'] . '|' . $value['PPSN2_ITMCAT'] . '|' . $value['PPSN1_LINENO'] . '|' . $value['PPSN1_FR'])
                        ->where('ITH_ITMCD', trim($item))
                        ->whereIn('ITH_WH', ['ARWH9SC'])
                        ->whereIn('ITH_FORM', ['ADJ-I-INC', 'ADJ-INC', 'INC-RET'])
                        ->first();

                    // Issue
                    // $ith[0] = $this->insertToITH(
                    //     trim($item),
                    //     $cekStockITH['ITH_DATE'],
                    //     'OUT-WH-RM',
                    //     $cekStockITH['ITH_DOC'],
                    //     round($qty) * -1,
                    //     $cekStockITH['ITH_WH'],
                    //     $insertDet,
                    //     $id,
                    //     'SCR_RPT'
                    // );
                }

                // Receive
                // $ith[1] = $this->insertToITH(
                //     trim($item),
                //     $date,
                //     'INC-SCR-RM',
                //     $transID,
                //     round($qty),
                //     'SCRRPT',
                //     $insertDet,
                //     $id,
                //     'SCR_RPT'
                // );

                $return[] = [
                    'status' => true,
                    'message' => 'Validation is pass, data material inserted',
                    'data' => $insertDet,
                    'ith' => $ith
                ];
            } else {
                $return[] = [
                    'status' => false,
                    'message' => 'No Stock found in exbc',
                    'data' => []
                ];
            }
        } else {
            logger('check item');
            logger([$item, $qty]);
            $data = $this->checkFIFODO($item, $qty);
            logger('masuk ke scrap material');
            logger($data);
            foreach ($data as $key => $value) {
                $getLot = $this->checkFIFOLot($item, $value['RPSTOCK_DOC'], $value['EXEC_QTY']);

                if (!empty($getLot)) {
                    foreach ($getLot as $keyLot => $valueLot) {
                        $cekRcvScnForPrice = $this->cekLatestHarga($item, $valueLot['RCVSCN_LOTNO'], $value['RPSTOCK_DOC']);

                        $insertDet = scrapHistDet::insertGetId([
                            'SCR_HIST_ID' => $id,
                            'SCR_PSN' => null,
                            'SCR_CAT' => null,
                            'SCR_LINE' => null,
                            'SCR_FR' => null,
                            'SCR_MC' => null,
                            'SCR_MCZ' => null,
                            'SCR_ITMCD' => $item,
                            'SCR_QTPER' => 1,
                            'SCR_QTY' => intVal($qty),
                            'SCR_LOTNO' => $valueLot['RCVSCN_LOTNO'],
                            'SCR_ITMPRC' => empty($cekRcvScnForPrice) || !isset($cekRcvScnForPrice['RCV_PRPRC']) ? 0 : $cekRcvScnForPrice['RCV_PRPRC']
                        ]);

                        $hasilBCStock = $this->sendingParamBCStock(
                            trim($item),
                            $date,
                            $lot,
                            round($qty),
                            $transID,
                            null,
                            $id
                        );

                        if ($hasilBCStock['status'] == true) {
                            $ith = [];
                            if ($source === 'WH') {
                                $cekStockITH = ITHMaster::select(
                                    'ITH_DOC',
                                    'ITH_DATE',
                                    'ITH_WH'
                                )
                                    // ->where('ITH_DOC', $value['PPSN1_PSNNO'] . '|' . $value['PPSN2_ITMCAT'] . '|' . $value['PPSN1_LINENO'] . '|' . $value['PPSN1_FR'])
                                    ->where('ITH_ITMCD', trim($item))
                                    ->whereIn('ITH_WH', ['ARWH1', 'ARWH2'])
                                    ->whereIn('ITH_FORM', ['INC-RET', 'INC-DO'])
                                    ->first();

                                // Issue
                                // $ith[0] = $this->insertToITH(
                                //     trim($item),
                                //     $cekStockITH['ITH_DATE'],
                                //     'OUT-WH-RM',
                                //     $cekStockITH['ITH_DOC'],
                                //     round($qty) * -1,
                                //     $cekStockITH['ITH_WH'],
                                //     $insertDet,
                                //     $id,
                                //     'SCR_RPT'
                                // );
                            } else {
                                $cekStockITH = ITHMaster::select(
                                    'ITH_DOC',
                                    'ITH_DATE',
                                    'ITH_WH'
                                )
                                    // ->where('ITH_DOC', $value['PPSN1_PSNNO'] . '|' . $value['PPSN2_ITMCAT'] . '|' . $value['PPSN1_LINENO'] . '|' . $value['PPSN1_FR'])
                                    ->where('ITH_ITMCD', trim($item))
                                    ->whereIn('ITH_WH', ['ARWH9SC'])
                                    ->whereIn('ITH_FORM', ['ADJ-I-INC', 'ADJ-INC', 'INC-RET'])
                                    ->first();

                                // Issue
                                // $ith[0] = $this->insertToITH(
                                //     trim($item),
                                //     $cekStockITH['ITH_DATE'],
                                //     'OUT-WH-RM',
                                //     $cekStockITH['ITH_DOC'],
                                //     round($qty) * -1,
                                //     $cekStockITH['ITH_WH'],
                                //     $insertDet,
                                //     $id,
                                //     'SCR_RPT'
                                // );
                            }

                            // Receive
                            // $ith[1] = $this->insertToITH(
                            //     trim($item),
                            //     $date,
                            //     'INC-SCR-RM',
                            //     $transID,
                            //     round($qty),
                            //     'SCRRPT',
                            //     $insertDet,
                            //     $id,
                            //     'SCR_RPT'
                            // );

                            $return[] = [
                                'status' => true,
                                'message' => 'Validation is pass, data material inserted',
                                'data' => $insertDet,
                                'ith' => $ith
                            ];
                        } else {
                            $return[] = [
                                'status' => false,
                                'message' => 'No Stock found in exbc',
                                'data' => []
                            ];
                        }
                    }
                } else {
                    $cekRcvScnForPrice = MutasiStok::where('RCV_DONO', $value['RPSTOCK_DOC'])->first();

                    $insertDet = scrapHistDet::insertGetId([
                        'SCR_HIST_ID' => $id,
                        'SCR_PSN' => null,
                        'SCR_CAT' => null,
                        'SCR_LINE' => null,
                        'SCR_FR' => null,
                        'SCR_MC' => null,
                        'SCR_MCZ' => null,
                        'SCR_ITMCD' => $item,
                        'SCR_QTPER' => 1,
                        'SCR_QTY' => intVal($qty),
                        'SCR_LOTNO' => '',
                        'SCR_ITMPRC' => empty($cekRcvScnForPrice) ? 0 : $cekRcvScnForPrice['RCV_PRPRC']
                    ]);

                    $hasilBCStock = $this->sendingParamBCStock(
                        trim($item),
                        $date,
                        $lot,
                        round($qty),
                        $transID,
                        null,
                        $id
                    );

                    if ($hasilBCStock['status'] == true) {
                        $ith = [];
                        if ($source === 'WH') {
                            $cekStockITH = ITHMaster::select(
                                'ITH_DOC',
                                'ITH_DATE',
                                'ITH_WH'
                            )
                                // ->where('ITH_DOC', $value['PPSN1_PSNNO'] . '|' . $value['PPSN2_ITMCAT'] . '|' . $value['PPSN1_LINENO'] . '|' . $value['PPSN1_FR'])
                                ->where('ITH_ITMCD', trim($item))
                                ->whereIn('ITH_WH', ['ARWH1', 'ARWH2'])
                                ->whereIn('ITH_FORM', ['INC-RET', 'INC-DO'])
                                ->first();

                            // Issue
                            // $ith[0] = $this->insertToITH(
                            //     trim($item),
                            //     $cekStockITH['ITH_DATE'],
                            //     'OUT-WH-RM',
                            //     $cekStockITH['ITH_DOC'],
                            //     round($qty) * -1,
                            //     $cekStockITH['ITH_WH'],
                            //     $insertDet,
                            //     $id,
                            //     'SCR_RPT'
                            // );
                        } else {
                            $cekStockITH = ITHMaster::select(
                                'ITH_DOC',
                                'ITH_DATE',
                                'ITH_WH'
                            )
                                // ->where('ITH_DOC', $value['PPSN1_PSNNO'] . '|' . $value['PPSN2_ITMCAT'] . '|' . $value['PPSN1_LINENO'] . '|' . $value['PPSN1_FR'])
                                ->where('ITH_ITMCD', trim($item))
                                ->whereIn('ITH_WH', ['ARWH9SC'])
                                ->whereIn('ITH_FORM', ['ADJ-I-INC', 'ADJ-INC', 'INC-RET'])
                                ->first();

                            // Issue
                            $ith[0] = $this->insertToITH(
                                trim($item),
                                $cekStockITH['ITH_DATE'],
                                'OUT-WH-RM',
                                $cekStockITH['ITH_DOC'],
                                round($qty) * -1,
                                $cekStockITH['ITH_WH'],
                                $insertDet,
                                $id,
                                'SCR_RPT'
                            );
                        }

                        // Receive
                        $ith[1] = $this->insertToITH(
                            trim($item),
                            $date,
                            'INC-SCR-RM',
                            $transID,
                            round($qty),
                            'SCRRPT',
                            $insertDet,
                            $id,
                            'SCR_RPT'
                        );

                        scrapHist::where('id', $id)->update([
                            'IS_DONE' => 1
                        ]);

                        $return[] = [
                            'status' => true,
                            'message' => 'Validation is pass, data material inserted',
                            'data' => $insertDet,
                            'ith' => $ith
                        ];
                    } else {
                        $return[] = [
                            'status' => false,
                            'message' => 'No Stock found in exbc',
                            'data' => []
                        ];
                    }
                }
            }
        }

        if (count($return) > 0) {
            return [
                'status' => false,
                'message' => 'No data !!',
                'data' => $return
            ];
        } else {
            return [
                'status' => true,
                'message' => 'Validation is pass, data material inserted',
                'data' => $return
            ];
        }
    }

    public function failed(\Throwable $exception)
    {
        try {
            $cekJob = scrapHist::where('id', $this->id)->first();
            $redis = LRedis::connection();

            $redis->publish('message', json_encode([
                'app' => 'scrap_report',
                'id' => $this->transID,
                'id_transaction' => $this->id,
                'item' => $this->item,
                'item_desc' => $this->item,
                'doc' => $cekJob->DOC_NO,
                'status' => 'negative',
                'message' => 'There is an error on server, please contact administrator !',
                'user' => $cekJob->USERNAME,
                'stat_print' => false
            ]));
            scrapHist::where('id', $this->id)->update(['failed_reason' => $exception]);
            ITHMaster::where('ITH_SER', (string)$this->id)->where('ITH_USRID', 'SCR_RPT')->delete();

            scrapHist::where('id', $this->id)->delete();
            scrapHistDet::where('SCR_HIST_ID', $this->id)->delete();
        } catch (\Predis\Connection\ConnectionException $e) {
            return response('error connection redis');
        }
    }

    public function cekLatestHarga($item, $lot = '', $do = '')
    {
        $cekRcvScnForPriceHead = RCVSCN2::select(
            'RCVSCN_ITMCD',
            'RCVSCN_DONO',
            'RCV_PRPRC',
            'RCVSCN_LUPDT',
            'RCV_BCTYPE',
            'RCVSCN_LOTNO',
            DB::raw('SUM(RCVSCN_QTY) AS QTY')
        )
            ->where('RCVSCN_ITMCD', $item)
            ->join('RCV_TBL', function ($j) {
                $j->on('RCV_ITMCD', 'RCVSCN_ITMCD');
                $j->on('RCV_DONO', 'RCVSCN_DONO');
            })
            ->groupBy(
                'RCVSCN_ITMCD',
                'RCVSCN_DONO',
                'RCV_PRPRC',
                'RCVSCN_LUPDT',
                'RCV_BCTYPE',
                'RCVSCN_LOTNO'
            )
            ->orderBy('RCVSCN_LUPDT', 'DESC');

        if (!empty($lot)) {
            if (empty($do)) {
                $cekRcvScnForPrice = $cekRcvScnForPriceHead->where('RCVSCN_LOTNO', $lot)->first();
            } else {
                $cekRcvScnForPrice = $cekRcvScnForPriceHead->where('RCVSCN_LOTNO', $lot)->where('RCVSCN_DONO', $do)->first();
            }
        } else {
            if (!empty($do)) {
                $cekRcvScnForPrice = $cekRcvScnForPriceHead->where('RCVSCN_DONO', $do)->get();
            }
        }

        return $cekRcvScnForPrice;
    }

    public function checkFIFODO($item, $qty, $do = '')
    {
        $cekExBC = DB::table('PSI_RPCUST.dbo.v_stock_exbc_by_date_aju')
            ->where('RPSTOCK_ITMNUM', $item)
            ->where('EXBC_TOT_QTY', '>', 0)
            ->orderBy('RPSTOCK_BCDATE', 'DESC');

        if (!empty($do)) {
            $cekExBC->where('RPSTOCK_DOC', $do);
        }

        $cekExBC = array_map(function ($value) {
            return (array)$value;
        }, $cekExBC->get()->toArray());

        $data = [];
        if (count($cekExBC) === 0) {
            return $data;
        } else {
            $totqtytemp = $qty;
            foreach ($cekExBC as $key => $value) {
                $totalQty = $totqtytemp - $value['EXBC_TOT_QTY'];
                if ($totalQty > 0) {
                    $data[] = array_merge($value, [
                        'SISA_QTY' => $totalQty,
                        'EXEC_QTY' => $totqtytemp
                    ]);

                    $totqtytemp = $totalQty;
                } elseif ($totalQty === 0) {
                    $data[] = array_merge($value, [
                        'SISA_QTY' => $totalQty,
                        'EXEC_QTY' => $totqtytemp
                    ]);
                    break;
                } else {
                    $data[] = array_merge($value, [
                        'SISA_QTY' => $totalQty,
                        'EXEC_QTY' => $totqtytemp
                    ]);;
                    break;
                }
            }

            return $data;
        }
    }

    public function checkFIFOLot($item, $do, $qty)
    {
        $dataLOT = $this->cekLatestHarga($item, '', $do);
        if (!empty($dataLOT)) {
            $dataLOT = $dataLOT->toArray();

            $data = [];
            if (count($dataLOT) === 0) {
                return $data;
            } else {
                $totqtytemp = $qty;

                // logger(json_encode($dataLOT));
                foreach ($dataLOT as $key => $value) {
                    $totalQty = $totqtytemp - $value['QTY'];
                    if ($totalQty > 0) {
                        $data[] = array_merge($value, [
                            'SISA_QTY' => $totalQty,
                            'EXEC_QTY' => $totqtytemp
                        ]);

                        $totqtytemp = $totalQty;
                    } elseif ($totalQty === 0) {
                        $data[] = array_merge($value, [
                            'SISA_QTY' => $totalQty,
                            'EXEC_QTY' => $totqtytemp
                        ]);
                        break;
                    } else {
                        $data[] = array_merge($value, [
                            'SISA_QTY' => $totalQty,
                            'EXEC_QTY' => $totqtytemp
                        ]);;
                        break;
                    }
                }

                return $data;
            }
        }
    }

    public function insertToITH($item, $date, $form, $doc, $qty, $wh, $ser, $remark, $user)
    {
        ITHMaster::insert([
            'ITH_ITMCD' => $item,
            'ITH_DATE' => $date,
            'ITH_FORM' => $form,
            'ITH_DOC' => $doc,
            'ITH_QTY' => $qty,
            'ITH_WH' => $wh,
            'ITH_LOC' => '',
            'ITH_SER' => $ser,
            'ITH_REMARK' => $remark,
            'ITH_LINE' => DB::raw('dbo.fun_ithline()'),
            'ITH_LUPDT' => $date . ' ' . date('H:i:s'),
            'ITH_USRID' => $user,
        ]);
    }

    public function sendingParamBCStock($item_num, $date_out = '', $lot = null, $qty = 0, $doc = null, $bc = null, $loc = '')
    {
        $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item_num,
            'date_out' => empty($date_out) ? date('Y-m-d') : $date_out,
            'lot' => $lot,
            'qty' => $qty,
            'doc' => $doc,
            'bc' => $bc,
            'loc' => $loc
            // 'scrap' => true,
            // 'remark' => $id,
            // 'revise' => $rev
        ];

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content['CURL'];
    }
}
