<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Model\WMS\REPORT\MutasiStok;

class mutasiMasukRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $cekReceiveBarang = MutasiStok::leftjoin('MSUP_TBL', 'MSUP_SUPCD', '=', 'RCV_SUPCD')
            ->join('MITM_TBL', 'MITM_ITMCD', '=', 'RCV_ITMCD')
            ->where('RCV_DONO', '<>', '');

        $arrayinput = [
            'JENBEA' => 'RCV_BCTYPE',
            'TGLBEA' => 'RCV_BCDATE',
            'NUMBEA' => 'RCV_BCNO',
            'NUMSJL' => 'RCV_DONO',
            'TGLSJL' => 'RCV_INVDATE',
            'NUMPEN' => 'RCV_RPNO',
            'TGLPEN' => 'RCV_RPDATE',
            'CUSSUP' => 'RCV_SUPCD',
            'ITMCOD' => 'RCV_ITMCD',
            'QTYJUM' => 'RCV_QTY',
            'UNITMS' => 'MITM_STKUOM',
            'VALAS' => 'MSUP_SUPCR',
            'QTYTOT' => 'RCV_AMT',
            'QTYSAT' => 'RCV_PRPRC',
            'TYPE' => 'INC',
            'RCVEDT' => 'RCV_RCVDATE',
            'DOC' => 'RCV_DONO'
        ];
    }
}
