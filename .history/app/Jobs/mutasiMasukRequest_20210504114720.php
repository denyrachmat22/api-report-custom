<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Model\WMS\REPORT\MutasiStok;
use App\Model\RPCUST\MutasiDokPabean;
use App\Model\RPCUST\DetailStock;
use App\Model\RPCUST\MutasiRaw;

class mutasiMasukRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $arrayinput = [
            'JENBEA' => 'RCV_BCTYPE',
            'TGLBEA' => 'RCV_BCDATE',
            'NUMBEA' => 'RCV_BCNO',
            'NUMSJL' => 'RCV_DONO',
            'TGLSJL' => 'RCV_INVDATE',
            'NUMPEN' => 'RCV_RPNO',
            'TGLPEN' => 'RCV_RPDATE',
            'CUSSUP' => 'RCV_SUPCD',
            'ITMCOD' => 'RCV_ITMCD',
            'QTYJUM' => 'RCV_QTY',
            'UNITMS' => 'MITM_STKUOM',
            'VALAS' => 'MSUP_SUPCR',
            'QTYTOT' => 'RCV_AMT',
            'QTYSAT' => 'RCV_PRPRC',
            'TYPE' => 'INC',
            'RCVEDT' => 'RCV_RCVDATE',
            'DOC' => 'RCV_DONO',
            'MDLFLAG' => 'MITM_MODEL'
        ];

        $valueBarang = $this->data;

        $arraynya =
            [
                'RPBEA_JENBEA' => $valueBarang[$arrayinput['JENBEA']],
                'RPBEA_TGLBEA' => $valueBarang[$arrayinput['TGLBEA']],
                'RPBEA_NUMBEA' => $valueBarang[$arrayinput['NUMBEA']],
                'RPBEA_NUMSJL' => $valueBarang[$arrayinput['NUMSJL']],
                'RPBEA_TGLSJL' => $valueBarang[$arrayinput['TGLSJL']],
                'RPBEA_NUMPEN' => $valueBarang[$arrayinput['NUMPEN']],
                'RPBEA_TGLPEN' => $valueBarang[$arrayinput['TGLPEN']],
                'RPBEA_CUSSUP' => $valueBarang[$arrayinput['CUSSUP']],
                'RPBEA_ITMCOD' => $valueBarang[$arrayinput['ITMCOD']],
                'RPBEA_QTYJUM' => intval($valueBarang[$arrayinput['QTYJUM']]),
                'RPBEA_UNITMS' => $valueBarang[$arrayinput['UNITMS']],
                'RPBEA_VALAS' => $valueBarang[$arrayinput['VALAS']],
                'RPBEA_QTYTOT' => intval($valueBarang[$arrayinput['QTYTOT']]),
                'RPBEA_QTYSAT' => intval($valueBarang[$arrayinput['QTYSAT']]),
                'RPBEA_TYPE' => $arrayinput['TYPE'],
                'RPBEA_RCVEDT' => $valueBarang[$arrayinput['RCVEDT']],
                'RPBEA_DOC' => $valueBarang[$arrayinput['DOC']],
                'RPBEA_MDLFLAG' => $valueBarang[$arrayinput['MDLFLAG']],
            ];

        MutasiDokPabean::updateOrCreate(
            [
                'RPBEA_DOC' => $valueBarang[$arrayinput['DOC']],
                'RPBEA_ITMCOD' => $valueBarang[$arrayinput['ITMCOD']],
            ],
            $arraynya
        );

        DetailStock::updateOrCreate(
            [
                'RPSTOCK_DOC' => $valueBarang[$arrayinput['DOC']],
                'RPSTOCK_ITMNUM' => $valueBarang[$arrayinput['ITMCOD']],
                'RPSTOCK_BCDATE' => $valueBarang[$arrayinput['TGLBEA']],
                'RPSTOCK_NOAJU' => $valueBarang[$arrayinput['NUMPEN']],
                'RPSTOCK_TYPE' => 'INC'
            ],
            [
                'RPSTOCK_BCTYPE' => $valueBarang[$arrayinput['JENBEA']],
                'RPSTOCK_BCNUM' => $valueBarang[$arrayinput['NUMBEA']],
                'RPSTOCK_BCDATE' => $valueBarang[$arrayinput['TGLBEA']],
                'RPSTOCK_QTY' => intval($valueBarang[$arrayinput['QTYJUM']]),
                'RPSTOCK_DOC' => $valueBarang[$arrayinput['DOC']],
                'RPSTOCK_TYPE' => 'INC',
                'RPSTOCK_ITMNUM' => $valueBarang[$arrayinput['ITMCOD']],
                'RPSTOCK_NOAJU' => $valueBarang[$arrayinput['NUMPEN']],
            ]
        );

        $cekdata = MutasiRaw::where('RPRAW_ITMCOD', $valueBarang[$arrayinput['ITMCOD']])
                ->where('RPRAW_REF', $valueBarang[$arrayinput['DOC']])
                ->join('PSI_WMS.dbo.MITM_TBL', 'MITM_ITMCD', 'RPRAW_ITMCOD')
                ->first();
            if ($valueBarang['MITM_MODEL'] === '0') {
                if (empty($cekdata)) {
                    MutasiRaw::insert([
                        'RPRAW_ITMCOD' => $valueBarang[$arrayinput['ITMCOD']],
                        'RPRAW_UNITMS' => $valueBarang[$arrayinput['UNITMS']],
                        'RPRAW_DATEIS' => $valueBarang[$arrayinput['TGLBEA']],
                        'RPRAW_QTYINC' => $intval($valueBarang[$arrayinput['QTYJUM']]),
                        'RPRAW_QTYOUT' => 0,
                        'RPRAW_QTYADJ' => 0,
                        'RPRAW_QTYTOT' => intval($valueBarang[$arrayinput['QTYJUM']]),
                        'RPRAW_QTYOPN' => 0,
                        'RPRAW_KET' => '',
                        'RPRAW_REF' => $valueBarang[$arrayinput['DOC']],
                    ]);
                } else {
                    $cekdata->update([
                        'RPRAW_DATEIS' => $valueBarang[$arrayinput['TGLBEA']],
                        'RPRAW_QTYINC' => $type == 'INC' ? intval($valueBarang[$arrayinput['QTYJUM']]) : intval($cekdata['RPBEA_QTYJUM']),
                        'RPRAW_QTYOUT' => $type == 'OUT' ? intval($valueBarang[$arrayinput['QTYJUM']]) : intval($cekdata['RPBEA_QTYJUM']),
                        'RPRAW_QTYADJ' => 0,
                        'RPRAW_QTYTOT' => intval($valueBarang[$arrayinput['QTYJUM']]),
                        'RPRAW_QTYOPN' => 0,
                        'RPRAW_KET' => '',
                    ]);
                }
            }
    }
}
