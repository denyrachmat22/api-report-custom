<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use LRedis;

// Models
use App\Model\WMS\API\SPLSCN;
use App\Model\RPCUST\scrapHist;
use App\Model\RPCUST\scrapHistDet;
use App\Model\RPCUST\processMstr;
use App\Model\RPCUST\processMapDet;
use App\Model\MASTER\MEGAEMS\ppsn1Table;
use Illuminate\Support\Facades\DB;
use App\Model\WMS\API\RCVSCN;
use App\Model\MASTER\SERD2TBL;

class scrapReportInsert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $req;
    protected $idTrans;

    public function __construct($req, $idTrans)
    {
        $this->req = $req;
        $this->idTrans = $idTrans;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        set_time_limit(3600);
        $hasil = [];

        $fetchProcessMaster = processMstr::get()->toArray();
        foreach ($this->req['data'] as $key => $value) {
            // Jika itemnya adalah Model
            if ($value['MITM_MODEL'] === '1') {
                // Parsing job yang di pilih
                foreach ($value['SCRAP_DET'] as $keyJobDet => $valueJobDet) {
                    // Parsing Process Master (PROCESS_DET)
                    foreach ($valueJobDet['PROCESS_DET'] as $keyProMstr => $valueProMstr) {
                        // Parsing Process Detail
                        foreach ($valueProMstr['DET'] as $keyProDet => $valueProDet) {

                            if (intval($valueProDet['QTY']) > 0) {
                                $insertHeader = scrapHist::create([
                                    'DSGN_MAP_ID' => $valueProDet['id'],
                                    'USERNAME' => $this->req['username'],
                                    'DEPT' => $this->req['dept'],
                                    'QTY' => $valueProDet['QTY'],
                                    'ID_TRANS' => $this->idTrans,
                                    'TYPE_TRANS' => $this->req['type'],
                                    'DOC_NO' => $valueJobDet['PIS2_WONO'],
                                    'REASON' => isset($valueProDet['REASON']) ? $valueProDet['REASON'] : '',
                                    'SCR_PROCD' => $valueProDet['RPDSGN_PROCD'],
                                    'MDL_FLAG' => 1,
                                    'ITEMNUM' => $valueJobDet['PIS2_MDLCD'],
                                ]);

                                // Insert Detail Transaction

                                $findByProcess = $this->getPsnListByJobAndProcd(
                                    $fetchProcessMaster,
                                    $valueJobDet['PIS2_WONO'],
                                    $valueProDet['id'],
                                    $valueJobDet['PIS2_MDLCD']
                                );

                                $hasil[] = $valueProDet;

                                // Fetch All PSN by process and job number
                                foreach ($findByProcess as $key => $value) {

                                    // Get Data LOT SPLSCN
                                    $getFIFOLotNo = $this->getDataSPLSCN(
                                        $value['PPSN1_PSNNO'],
                                        $value['PPSN2_ITMCAT'],
                                        $value['PPSN1_LINENO'],
                                        $value['PPSN1_FR'],
                                        $value['PPSN2_MCZ'],
                                        $value['PPSN2_SUBPN']
                                    );

                                    $totalQty = $valueProDet['QTY'] * floatVal($value['QTPER_VAL']);

                                    $fetchFIFOLotNo = $this->getFIFOLOT($getFIFOLotNo, $totalQty);

                                    foreach ($fetchFIFOLotNo as $keyFifoLot => $valueFifoLot) {
                                        $insertDet = scrapHistDet::create([
                                            'SCR_HIST_ID' => $insertHeader->id,
                                            'SCR_PSN' => $value['PPSN1_PSNNO'],
                                            'SCR_CAT' => $value['PPSN2_ITMCAT'],
                                            'SCR_LINE' => $value['PPSN1_LINENO'],
                                            'SCR_FR' => $value['PPSN1_FR'],
                                            'SCR_MC' => $value['PPSN2_MC'],
                                            'SCR_MCZ' => $value['PPSN2_MCZ'],
                                            'SCR_ITMCD' => trim($value['PPSN2_SUBPN']),
                                            'SCR_QTPER' => $value['PPSN2_QTPER'],
                                            'SCR_QTY' => round($totalQty),
                                            'SCR_LOTNO' => $valueFifoLot['SPLSCN_LOTNO'],
                                            'SCR_ITMPRC' => empty($valueFifoLot['PRICE']) ? 0 : $valueFifoLot['PRICE']
                                        ]);

                                        // Insert scrap to stock
                                        $this->sendingParamBCStock(
                                            trim($value['PPSN2_SUBPN']),
                                            $totalQty,
                                            $this->idTrans,
                                            $valueFifoLot['SPLSCN_LOTNO'],
                                            $valueFifoLot['RCV_BCTYPE'],
                                            $insertDet->id
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                foreach ($value['SCRAP_DET'] as $keyLotDet => $valueLotDet) {
                    if ($valueLotDet['QTY'] > 0) {
                        $cekRcvScnForPrice = $this->cekLatestHarga($valueLotDet['RCVSCN_ITMCD'], $valueLotDet['SPLSCN_LOTNO'], $valueLotDet['RCVSCN_DONO']);

                        // Insert ke table Hist Master
                        $insertingScrap = scrapHist::create([
                            'DSGN_MAP_ID' => $valueLotDet['KEY_LOT'],
                            'USERNAME' => '',
                            'DEPT' => '',
                            'QTY' => intVal($valueLotDet['QTY']),
                            'ID_TRANS' => $this->idTrans,
                            'TYPE_TRANS' => $this->req->type,
                            'DOC_NO' => $valueLotDet['RCVSCN_DONO'],
                            'REASON' => isset($valueLotDet['REASON']) ? $valueLotDet['REASON'] : '',
                            'SCR_PROCD' => $valueLotDet['RCVSCN_LOTNO'],
                            'MDL_FLAG' => 0,
                            'ITEMNUM' => $valueLotDet['RCVSCN_ITMCD'],
                        ]);

                        // Insert ke table Hist Detail
                        $insertDet = scrapHistDet::create([
                            'SCR_HIST_ID' => $insertingScrap->id,
                            'SCR_PSN' => null,
                            'SCR_CAT' => null,
                            'SCR_LINE' => null,
                            'SCR_FR' => null,
                            'SCR_MC' => null,
                            'SCR_MCZ' => null,
                            'SCR_ITMCD' => $valueLotDet['RCVSCN_ITMCD'],
                            'SCR_QTPER' => 1,
                            'SCR_QTY' => round(intVal($valueLotDet['QTY'])),
                            'SCR_LOTNO' => $valueLotDet['RCVSCN_LOTNO'],
                            'SCR_ITMPRC' => empty($cekRcvScnForPrice) ? 0 : $cekRcvScnForPrice['RCV_PRPRC']
                        ]);

                        // Insert scrap to stock
                        $insertingStock = $this->sendingParamBCStock(
                            $valueLotDet['RCVSCN_ITMCD'],
                            intVal($valueLotDet['QTY']),
                            $this->idTrans,
                            $valueLotDet['SPLSCN_LOTNO'],
                            $cekRcvScnForPrice['RCV_BCTYPE'],
                            $insertDet->id
                        );
                    }
                }
            }
        }

        try{
            $redis = LRedis::connection();

            $redis->publish('message', json_encode([
                'app' => 'scrap_report',
                'id' => $this->idTrans,
                'status' => 'positive',
                'message' => 'This scrap report is successfully inserted'
            ]));
        }catch(\Predis\Connection\ConnectionException $e){
            return response('error connection redis');
        }
    }

    public function failed(\Throwable $exception)
    {
        try{
            $redis = LRedis::connection();

            $redis->publish('message', json_encode([
                'app' => 'scrap_report',
                'id' => $this->idTrans,
                'status' => 'negative',
                'message' => implode(' ', $exception['errorInfo'])
            ]));
        }catch(\Predis\Connection\ConnectionException $e){
            return response('error connection redis');
        }
    }

    // Helpernya

    public function psnListByJob($job, $procd = '')
    {
        $getData = ppsn1Table::select(
            'PPSN1_PSNNO',
            'PPSN2_ITMCAT',
            'PPSN1_LINENO',
            'PPSN1_FR',
            'PPSN2_MC',
            'PPSN2_MCZ',
            DB::raw('
            CASE WHEN PIS2_QTPER IS NULL
                THEN PPSN2_QTPER
                ELSE PIS2_QTPER
            END AS QTPER_VAL
            '),
            'PPSN2_SUBPN',
            'PPSN1_PROCD',
            'PPSN2_QTPER'
        )
            ->join('SRVMEGA.PSI_MEGAEMS.dbo.PPSN2_TBL', function ($f) {
                $f->on('PPSN1_PSNNO', 'PPSN2_PSNNO');
                $f->on('PPSN1_LINENO', 'PPSN2_LINENO');
                $f->on('PPSN1_FR', 'PPSN2_FR');
                $f->on('PPSN1_DOCNO', 'PPSN2_DOCNO');
                $f->on('PPSN1_BSGRP', 'PPSN2_BSGRP');
            })
            ->leftJoin('SRVMEGA.PSI_MEGAEMS.dbo.PIS2_TBL', function ($f) {
                $f->on('PIS2_WONO', 'PPSN1_WONO');
                $f->on('PIS2_PROCD', 'PPSN1_PROCD');
                $f->on('PIS2_ITMCD', 'PPSN2_SUBPN');
                $f->on('PIS2_MC', 'PPSN2_MC');
                $f->on('PIS2_MCZ', 'PPSN2_MCZ');
            })
            ->where('PPSN1_WONO', $job)
            ->groupBy(
                'PPSN1_DOCNO',
                'PPSN1_PSNNO',
                'PPSN2_ITMCAT',
                'PPSN1_LINENO',
                'PPSN1_FR',
                'PPSN1_PROCD',
                'PPSN2_SUBPN',
                'PPSN2_MC',
                'PPSN2_MCZ',
                'PPSN2_QTPER',
                'PIS2_QTPER'
            );

        if (empty($procd)) {
            return $getData->get()->toArray();
        } else {
            return $getData->where('PPSN1_PROCD', $procd)->get()->toArray();
        }
    }

    public function getDataSPLSCN($psn, $cat, $line, $fr, $orderNo, $item)
    {
        $data = SPLSCN::select([
            'SPLSCN_DOC',
            'SPLSCN_CAT',
            'SPLSCN_LINE',
            'SPLSCN_FEDR',
            'SPLSCN_ORDERNO',
            'SPLSCN_ITMCD',
            'SPLSCN_LOTNO',
            'SPLSCN_LUPDT',
            DB::raw('(SUM(SPLSCN_QTY) - COALESCE(SUM(RETSCN_QTYAFT), 0)) - COALESCE(SUM(SCR_QTY), 0) AS SPLSCN_QTY'),
            DB::raw('SUM(RETSCN_QTYAFT) AS RETURN_QTY'),
            DB::raw('COALESCE(SUM(SCR_QTY), 0) AS SCRAP_QTY'),
            DB::raw('SUM(SPLSCN_QTY) AS REAL_SPLSCN_QTY'),
            DB::raw('(SELECT TOP 1
                RCV_PRPRC
                FROM RCVSCN_TBL
                INNER JOIN RCV_TBL ON RCVSCN_DONO = RCV_DONO
                    AND RCVSCN_ITMCD = RCV_ITMCD
                WHERE RCVSCN_ITMCD = SPLSCN_ITMCD
                AND RCVSCN_LOTNO = SPLSCN_LOTNO
                ORDER BY RCV_RPDATE DESC) as PRICE'),
            DB::raw('(SELECT TOP 1
                    RCV_BCTYPE
                    FROM RCVSCN_TBL
                    INNER JOIN RCV_TBL ON RCVSCN_DONO = RCV_DONO
                        AND RCVSCN_ITMCD = RCV_ITMCD
                    WHERE RCVSCN_ITMCD = SPLSCN_ITMCD
                    AND RCVSCN_LOTNO = SPLSCN_LOTNO
                    ORDER BY RCV_RPDATE DESC) as RCV_BCTYPE')
        ])
            ->leftjoin('RETSCN_TBL', function ($f) {
                $f->on('RETSCN_ITMCD', '=', 'SPLSCN_ITMCD');
                $f->on('RETSCN_SPLDOC', '=', 'SPLSCN_DOC');
                $f->on('RETSCN_CAT', '=', 'SPLSCN_CAT');
                $f->on('RETSCN_LINE', '=', 'SPLSCN_LINE');
                $f->on('RETSCN_FEDR', '=', 'SPLSCN_FEDR');
                $f->on('RETSCN_ORDERNO', '=', 'SPLSCN_ORDERNO');
                $f->on('RETSCN_LOT', '=', 'SPLSCN_LOTNO');
            })
            ->leftjoin('PSI_RPCUST.dbo.RPSCRAP_HIST_DET', function ($f2) {
                $f2->on('SCR_PSN', '=', 'SPLSCN_DOC');
                $f2->on('SCR_CAT', '=', 'SPLSCN_CAT');
                $f2->on('SCR_LINE', '=', 'SPLSCN_LINE');
                $f2->on('SCR_FR', '=', 'SPLSCN_FEDR');
                $f2->on('SCR_MCZ', '=', 'SPLSCN_ORDERNO');
                $f2->on('SCR_ITMCD', '=', 'SPLSCN_ITMCD');
            })
            ->where('SPLSCN_DOC', $psn)
            ->where('SPLSCN_CAT', $cat)
            ->where('SPLSCN_LINE', $line)
            ->where('SPLSCN_FEDR', $fr)
            ->where('SPLSCN_ORDERNO', $orderNo)
            ->where('SPLSCN_ITMCD', $item)
            ->groupBy(
                [
                    'SPLSCN_DOC',
                    'SPLSCN_CAT',
                    'SPLSCN_LINE',
                    'SPLSCN_FEDR',
                    'SPLSCN_ORDERNO',
                    'SPLSCN_ITMCD',
                    'SPLSCN_LOTNO',
                    'SPLSCN_LUPDT'
                ]
            )
            ->orderBy('SPLSCN_LOTNO', 'DESC')
            ->get()
            ->toArray();

        return $data;
    }

    public function cekLatestHarga($item, $lot, $do = '')
    {
        $cekRcvScnForPriceHead = RCVSCN::select(
            'RCVSCN_ITMCD',
            'RCVSCN_DONO',
            'RCV_PRPRC',
            'RCVSCN_LUPDT',
            'RCV_BCTYPE'
        )
            ->where('RCVSCN_ITMCD', $item)
            ->where('RCVSCN_LOTNO', $lot)
            ->join('RCV_TBL', function ($j) {
                $j->on('RCV_ITMCD', 'RCVSCN_ITMCD');
                $j->on('RCV_DONO', 'RCVSCN_DONO');
            })
            ->groupBy(
                'RCVSCN_ITMCD',
                'RCVSCN_DONO',
                'RCV_PRPRC',
                'RCVSCN_LUPDT',
                'RCV_BCTYPE'
            )
            ->orderBy('RCVSCN_LUPDT', 'DESC');

        if (empty($do)) {
            $cekRcvScnForPrice = $cekRcvScnForPriceHead->first();
        } else {
            $cekRcvScnForPrice = $cekRcvScnForPriceHead->where('RCVSCN_DONO', $do)->first();
        }

        return $cekRcvScnForPrice;
    }

    public function defineMappingScrap($data)
    {
        if ($data['RPDSGN_PRO_ID'] == 'SMT-HW' && !empty($data['RPDSGN_PROCD'])) {
            return 'SMT-HW';
        } elseif ($data['RPDSGN_PRO_ID'] == 'SMT-HWADD' && !empty($data['RPDSGN_PROCD'])) {
            return 'SMT-HWAD';
        } elseif ($data['RPDSGN_PRO_ID'] == 'SMT-SP' && !empty($data['RPDSGN_PROCD'])) {
            return 'SMT-SP';
        } else {
            return $data['RPDSGN_PROCD'];
        }
    }

    public function getPsnListByJobAndProcd($listProcess, $job, $idProcess, $item, $hasil = []) // Recursive here
    {
        $cekProcessNow = current($listProcess);
        if (!empty($cekProcessNow)) {
            $getDataMapping = processMapDet::where('RPDSGN_ITEMCD', $item)
                ->where('RPDSGN_PRO_ID', $cekProcessNow['RPDSGN_CODE'])
                ->orderBy('id')
                ->first();

            if ($idProcess == $cekProcessNow['RPDSGN_CODE']) {
                $hasil = array_merge($hasil, $this->psnListByJob($job, $this->defineMappingScrap($getDataMapping)));
                // $hasil[$cekProcessNow['RPDSGN_CODE']] = $this->psnListByJob($job, $this->defineMappingScrap($getDataMapping));
                return $hasil;
            } else {
                if (!empty($getDataMapping['RPDSGN_PROCD'])) {
                    $hasil = array_merge($hasil, $this->psnListByJob($job, $this->defineMappingScrap($getDataMapping)));
                    next($listProcess);
                    return $this->getPsnListByJobAndProcd($listProcess, $job, $idProcess, $item, $hasil);
                } else {
                    next($listProcess);
                    return $this->getPsnListByJobAndProcd($listProcess, $job, $idProcess, $item, $hasil);
                }
            }
        } else {
            return $hasil;
        }
    }

    public function getFIFOLOT($process, $qty, $hasil = [])
    {
        $cekProcessNow = current($process);

        if (!empty($cekProcessNow)) {
            $total = (int)$qty - (int)$cekProcessNow['SPLSCN_QTY'];
            if ($total > 0) {
                $hasil = array_merge($hasil, [array_merge($cekProcessNow, ['QTY_USED' => (int)$cekProcessNow['SPLSCN_QTY']])]);
                next($process);
                return $this->getFIFOLOT($process, $total, $hasil);
            } else {
                $hasil = array_merge($hasil, [array_merge($cekProcessNow, ['QTY_USED' => $qty])]);
                return $hasil;
            }
        } else {
            return $hasil;
        }
    }

    public function sendingParamBCStock($item_num, $qty = 0, $doc = null, $lot = null, $bc = null, $id = '', $rev = false)
    {
        $endpoint = 'http://192.168.0.29:8081/api-report-custom/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item_num,
            'qty' => $qty,
            'doc' => $doc,
            'lot' => $lot,
            'bc' => $bc,
            'scrap' => true,
            'remark' => $id,
            'revise' => $rev
        ];

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content;
    }
}
