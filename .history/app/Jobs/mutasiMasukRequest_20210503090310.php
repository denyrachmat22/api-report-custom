<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Model\WMS\REPORT\MutasiStok;
use App\Model\RPCUST\MutasiDokPabean;
use App\Model\RPCUST\DetailStock;

class mutasiMasukRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $arrayinput = [
            'JENBEA' => 'RCV_BCTYPE',
            'TGLBEA' => 'RCV_BCDATE',
            'NUMBEA' => 'RCV_BCNO',
            'NUMSJL' => 'RCV_DONO',
            'TGLSJL' => 'RCV_INVDATE',
            'NUMPEN' => 'RCV_RPNO',
            'TGLPEN' => 'RCV_RPDATE',
            'CUSSUP' => 'RCV_SUPCD',
            'ITMCOD' => 'RCV_ITMCD',
            'QTYJUM' => 'RCV_QTY',
            'UNITMS' => 'MITM_STKUOM',
            'VALAS' => 'MSUP_SUPCR',
            'QTYTOT' => 'RCV_AMT',
            'QTYSAT' => 'RCV_PRPRC',
            'TYPE' => 'INC',
            'RCVEDT' => 'RCV_RCVDATE',
            'DOC' => 'RCV_DONO',
            'MDLFLAG' => 'MITM_MODEL'
        ];

        foreach ($this->data as $keyBarang => $valueBarang) {
            $arraynya =
                [
                    'RPBEA_JENBEA' => $valueBarang[$arrayinput['JENBEA']],
                    'RPBEA_TGLBEA' => $valueBarang[$arrayinput['TGLBEA']],
                    'RPBEA_NUMBEA' => $valueBarang[$arrayinput['NUMBEA']],
                    'RPBEA_NUMSJL' => $valueBarang[$arrayinput['NUMSJL']],
                    'RPBEA_TGLSJL' => $valueBarang[$arrayinput['TGLSJL']],
                    'RPBEA_NUMPEN' => $valueBarang[$arrayinput['NUMPEN']],
                    'RPBEA_TGLPEN' => $valueBarang[$arrayinput['TGLPEN']],
                    'RPBEA_CUSSUP' => $valueBarang[$arrayinput['CUSSUP']],
                    'RPBEA_ITMCOD' => $valueBarang[$arrayinput['ITMCOD']],
                    'RPBEA_QTYJUM' => intval($valueBarang[$arrayinput['QTYJUM']]),
                    'RPBEA_UNITMS' => $valueBarang[$arrayinput['UNITMS']],
                    'RPBEA_VALAS' => $valueBarang[$arrayinput['VALAS']],
                    'RPBEA_QTYTOT' => intval($valueBarang[$arrayinput['QTYTOT']]),
                    'RPBEA_QTYSAT' => intval($valueBarang[$arrayinput['QTYSAT']]),
                    'RPBEA_TYPE' => $arrayinput['TYPE'],
                    'RPBEA_RCVEDT' => $valueBarang[$arrayinput['RCVEDT']],
                    'RPBEA_DOC' => $valueBarang[$arrayinput['DOC']],
                    'RPBEA_MDLFLAG' => $valueBarang[$arrayinput['MDLFLAG']],
                ];

            MutasiDokPabean::updateOrCreate(
                [
                    'RPBEA_DOC' => $valueBarang[$arrayinput['DOC']],
                    'RPBEA_ITMCOD' => $valueBarang[$arrayinput['ITMCOD']],
                ],
                $arraynya
            );

            DetailStock::updateOrCreate(
                [
                    'RPSTOCK_DOC' => $valueBarang[$arrayinput['DOC']],
                    'RPSTOCK_ITMNUM' => $valueBarang[$arrayinput['ITMCOD']],
                    'RPSTOCK_BCDATE' => $valueBarang[$arrayinput['TGLBEA']],
                    'RPSTOCK_NOAJU' => $valueBarang[$arrayinput['NUMPEN']],
                ],
                [
                    'RPSTOCK_BCTYPE' => $valueBarang[$arrayinput['JENBEA']],
                    'RPSTOCK_BCNUM' => $valueBarang[$arrayinput['NUMBEA']],
                    'RPSTOCK_BCDATE' => $valueBarang[$arrayinput['TGLBEA']],
                    'RPSTOCK_QTY' => intval($valueBarang[$arrayinput['QTYJUM']]),
                    'RPSTOCK_DOC' => $valueBarang[$arrayinput['DOC']],
                    'RPSTOCK_TYPE' => 'INC',
                    'RPSTOCK_ITMNUM' => $valueBarang[$arrayinput['ITMCOD']],
                    'RPSTOCK_NOAJU' => $valueBarang[$arrayinput['NUMPEN']],
                ]
            );
        }
    }
}
