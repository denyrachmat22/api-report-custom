<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Model\RPCUST\MutasiDokPabean;
use App\Model\WMS\REPORT\MutasiStok;
use App\Model\WMS\API\RCVSCN;
use App\Model\WMS\REPORT\MutasiKeluarStok;
use App\Model\RPCUST\MutasiRaw;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Model\MASTER\ITHMaster;
use App\Model\RPCUST\MutasiFinishGood;
use App\Model\MASTER\SPLMaster;
use App\Model\RPCUST\DetailStock;

class MutasiStokDocPabeanJobs implements ShouldQueue
{
    public $timeout = 0;
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $sj;
    protected $stat;

    public function __construct($sj, $stat)
    {
        $this->sj = $sj;
        $this->stat = $stat;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->stat == 'INC' || $this->stat == 'INC_ITM') {
            $cekReceiveBarang = MutasiStok::leftjoin('MSUP_TBL', 'MSUP_SUPCD', '=', 'RCV_SUPCD')
                ->join('MITM_TBL', 'MITM_ITMCD', '=', 'RCV_ITMCD')
                ->where('RCV_DONO', '<>', '');

            $arrayinput = [
                'JENBEA' => 'RCV_BCTYPE',
                'TGLBEA' => 'RCV_BCDATE',
                'NUMBEA' => 'RCV_BCNO',
                'NUMSJL' => 'RCV_DONO',
                'TGLSJL' => 'RCV_INVDATE',
                'NUMPEN' => 'RCV_RPNO',
                'TGLPEN' => 'RCV_RPDATE',
                'CUSSUP' => 'RCV_SUPCD',
                'ITMCOD' => 'RCV_ITMCD',
                'QTYJUM' => 'RCV_QTY',
                'UNITMS' => 'MITM_STKUOM',
                'VALAS' => 'MSUP_SUPCR',
                'QTYTOT' => 'RCV_AMT',
                'QTYSAT' => 'RCV_PRPRC',
                'TYPE' => 'INC',
                'RCVEDT' => 'RCV_RCVDATE',
                'DOC' => 'RCV_DONO'
            ];

            $col = $this->stat == 'INC' ? 'RCV_DONO' : 'RCV_ITMCD';

            if (!isset($this->sj)) {
                $this->HandleInserting($cekReceiveBarang, $arrayinput);
            } else {
                $this->HandleInserting($cekReceiveBarang, $arrayinput, $col, $this->sj);
            }
        } else {
            $this->parseFinishGood();
        }

        return 'success';
    }

    public function parseFinishGood()
    {
        $select2 = [
            'ITH_ITMCD',
            'ITH_DATE',
            'ITH_DOC',
            // 'ITH_SER',
            'MITM_STKUOM'
        ];

        $cekfingood = ITHMaster::select(array_merge($select2, [DB::raw('SUM(ITH_QTY) * -1 AS ITH_QTY')]))
            ->join('MITM_TBL', 'MITM_ITMCD', 'ITH_ITMCD')
            ->with('dlv.serial.spl')
            ->with('modelprice')
            ->with(['dlv' => function ($qdlv) {
                $qdlv->select(
                    'DLV_ID',
                    'DLV_DATE',
                    'DLV_CUSTCD',
                    'DLV_DOCREFF',
                    'DLV_SER',
                    'DLV_QTY',
                    'DLV_NOPEN',
                    'DLV_RPDATE',
                    'DLV_BCTYPE',
                    'DLV_NOAJU',
                    'DLV_BCDATE',
                    'MCUS_CUSNM',
                    'MCUS_ABBRV',
                    'MCUS_IVCUR'
                );
                $qdlv->join('MCUS_TBL', 'MCUS_CUSCD', 'DLV_CUSTCD');
                $qdlv->with(['serial' => function ($qser) {
                    $qser->select(
                        'SER_ID',
                        'SER_DOC',
                        'SER_ITMID',
                        'SER_QTY',
                        'SER_LUPDT',
                        'MITM_ITMD1',
                        'MITM_STKUOM',
                        'MSLSPRICE_TBL.*'
                    );
                    $qser->join('MSLSPRICE_TBL', 'MSLSPRICE_ITMCD', 'SER_ITMID');
                    $qser->join('MITM_TBL', 'MITM_ITMCD', 'SER_ITMID');
                    $qser->with(['spl' => function ($qspl) {
                        $qspl->select(
                            'SPL_DOC',
                            'SPL_CAT',
                            'SPL_LINE',
                            'SPL_FEDR',
                            'SPL_JOBNO',
                            'SPL_FG',
                            'SPL_ORDERNO',
                            'SPL_ITMCD',
                            'MITM_STKUOM',
                            'SPL_QTYREQ',
                            'SPL_QTYUSE',
                            DB::raw('SPL_QTYUSE * SPL_QTYREQ AS QTY_REQ_TOT'),
                            // 'SPLSCN_ID',
                            'SPLSCN_ORDERNO',
                            'SPLSCN_LOTNO',
                            'RETSCN_QTYAFT',
                            DB::raw('SUM(SPLSCN_QTY) AS SPLSCN_QTY'),
                            DB::raw('CAST(SPLSCN_LUPDT AS DATE) as SPLSCN_LUPDT'),
                            DB::raw('(
                                SELECT TOP 1 CAST(SPLSCN_LUPDT AS DATE) as SPLSCN_LUPDT
                                    FROM SPLSCN_TBL B
                                    WHERE B.SPLSCN_DOC = SPL_DOC
                                    AND B.SPLSCN_CAT = SPL_CAT
                                    AND B.SPLSCN_LINE = SPL_LINE
                                    AND B.SPLSCN_FEDR = SPL_FEDR
                                    AND B.SPLSCN_ORDERNO = SPL_ORDERNO
                                    AND B.SPLSCN_ITMCD = SPL_ITMCD
                                    AND B.SPLSCN_LOTNO = SPLSCN_LOTNO
                                    ORDER BY CAST(B.SPLSCN_LUPDT AS DATE) ASC
                            ) AS APAINISIH')
                        );
                        // $qspl->with('splscn');
                        $qspl->join('SPLSCN_TBL', function ($join) {
                            $join->on('SPLSCN_DOC', '=', 'SPL_DOC');
                            $join->on('SPLSCN_CAT', '=', 'SPL_CAT');
                            $join->on('SPLSCN_LINE', '=', 'SPL_LINE');
                            $join->on('SPLSCN_FEDR', '=', 'SPL_FEDR');
                            $join->on('SPLSCN_ITMCD', '=', 'SPL_ITMCD');
                            $join->on('SPLSCN_ORDERNO', '=', 'SPL_ORDERNO');
                        });
                        $qspl->leftjoin('RETSCN_TBL', function ($join) {
                            $join->on('RETSCN_SPLDOC', '=', 'SPL_DOC');
                            $join->on('RETSCN_CAT', '=', 'SPL_CAT');
                            $join->on('RETSCN_LINE', '=', 'SPL_LINE');
                            $join->on('RETSCN_FEDR', '=', 'SPL_FEDR');
                            $join->on('RETSCN_ITMCD', '=', 'SPL_ITMCD');
                            $join->on('RETSCN_ORDERNO', '=', 'SPL_ORDERNO');
                        });
                        $qspl->join('MITM_TBL', 'MITM_ITMCD', 'SPL_ITMCD');
                        $qspl->where('SPLSCN_SAVED', 1);
                        $qspl->groupBy([
                            'SPL_DOC',
                            'SPL_CAT',
                            'SPL_LINE',
                            'SPL_FEDR',
                            'SPL_JOBNO',
                            'SPL_FG',
                            'SPL_ORDERNO',
                            'SPL_ITMCD',
                            'MITM_STKUOM',
                            'SPL_QTYREQ',
                            'SPL_QTYUSE',
                            // 'SPLSCN_ID',
                            'SPLSCN_ORDERNO',
                            'SPLSCN_LOTNO',
                            'RETSCN_QTYAFT',
                            'SPLSCN_DOC',
                            'SPLSCN_CAT',
                            'SPLSCN_LINE',
                            'SPLSCN_FEDR',
                            'SPLSCN_ORDERNO',
                            'SPLSCN_ITMCD',
                            DB::raw('CAST(SPLSCN_LUPDT AS DATE)')
                        ]);
                    }]);
                    $qser->with(['siscn' => function ($qsiscn) {
                        $qsiscn->where('SISCN_PLLT', '<>', NULL);
                        $qsiscn->with('si');
                    }]);
                    $qser->with(['serd2' => function ($qserd) {
                        $qserd->join('MITM_TBL', 'MITM_ITMCD', 'SERD2_ITMCD');
                    }]);
                }]);
            }])
            ->groupBy($select2)
            ->where('ITH_FORM', 'OUT-SHP-FG');

        $hasilfingood = (!empty($this->sj)) ?
            $cekfingood->where('ITH_DOC', base64_decode($this->sj))
            ->get()
            ->toArray()
            :   $cekfingood->get()->toArray();

        $this->insertToMutasiFingood($hasilfingood);
    }

    public function HandleInserting($modeljoined, $arraygetdata, $col = null, $filter = null)
    {
        $hasilfilter = empty($filter) ? $modeljoined->get() : $modeljoined->where($col, base64_decode($filter))->get();

        // logger($hasilfilter);
        logger(base64_decode($filter));
        foreach ($hasilfilter as $keyBarang => $valueBarang) {
            $arraynya =
                [
                    'RPBEA_JENBEA' => $valueBarang[$arraygetdata['JENBEA']],
                    'RPBEA_TGLBEA' => $valueBarang[$arraygetdata['TGLBEA']],
                    'RPBEA_NUMBEA' => $valueBarang[$arraygetdata['NUMBEA']],
                    'RPBEA_NUMSJL' => $valueBarang[$arraygetdata['NUMSJL']],
                    'RPBEA_TGLSJL' => $valueBarang[$arraygetdata['TGLSJL']],
                    'RPBEA_NUMPEN' => $valueBarang[$arraygetdata['NUMPEN']],
                    'RPBEA_TGLPEN' => $valueBarang[$arraygetdata['TGLPEN']],
                    'RPBEA_CUSSUP' => $valueBarang[$arraygetdata['CUSSUP']],
                    'RPBEA_ITMCOD' => $valueBarang[$arraygetdata['ITMCOD']],
                    'RPBEA_QTYJUM' => intval($valueBarang[$arraygetdata['QTYJUM']]),
                    'RPBEA_UNITMS' => $valueBarang[$arraygetdata['UNITMS']],
                    'RPBEA_VALAS' => $valueBarang[$arraygetdata['VALAS']],
                    'RPBEA_QTYTOT' => intval($valueBarang[$arraygetdata['QTYTOT']]),
                    'RPBEA_QTYSAT' => intval($valueBarang[$arraygetdata['QTYSAT']]),
                    'RPBEA_TYPE' => $arraygetdata['TYPE'],
                    'RPBEA_RCVEDT' => $valueBarang[$arraygetdata['RCVEDT']],
                    'RPBEA_DOC' => $valueBarang[$arraygetdata['DOC']],
                ];

            MutasiDokPabean::updateOrCreate(
                [
                    'RPBEA_DOC' => $valueBarang[$arraygetdata['DOC']],
                    'RPBEA_ITMCOD' => $valueBarang[$arraygetdata['ITMCOD']],
                ],
                $arraynya
            );

            DetailStock::updateOrCreate(
                [
                    'RPSTOCK_DOC' => $valueBarang[$arraygetdata['DOC']],
                    'RPSTOCK_ITMNUM' => $valueBarang[$arraygetdata['ITMCOD']],
                    'RPSTOCK_BCDATE' => $valueBarang[$arraygetdata['TGLBEA']],
                    'RPSTOCK_NOAJU' => $valueBarang[$arraygetdata['NUMPEN']],
                ],
                [
                    'RPSTOCK_BCTYPE' => $valueBarang[$arraygetdata['JENBEA']],
                    'RPSTOCK_BCNUM' => $valueBarang[$arraygetdata['NUMBEA']],
                    'RPSTOCK_BCDATE' => $valueBarang[$arraygetdata['TGLBEA']],
                    'RPSTOCK_QTY' => intval($valueBarang[$arraygetdata['QTYJUM']]),
                    'RPSTOCK_DOC' => $valueBarang[$arraygetdata['DOC']],
                    'RPSTOCK_TYPE' => 'INC',
                    'RPSTOCK_ITMNUM' => $valueBarang[$arraygetdata['ITMCOD']],
                    'RPSTOCK_NOAJU' => $valueBarang[$arraygetdata['NUMPEN']],
                ]
            );
        }

        if ($arraygetdata['TYPE'] == 'INC') {
            $this->insertToMutasiBahanBaku($hasilfilter, $arraygetdata, $arraygetdata['TYPE']);
        } else {
            $this->insertToMutasiBahanBaku($hasilfilter, $arraygetdata, $arraygetdata['TYPE']);
        }
    }

    public function insertToMutasiBahanBaku($data, $filter, $type)
    {
        foreach ($data as $key => $value) {
            $cekdata = MutasiRaw::where('RPRAW_ITMCOD', $value[$filter['ITMCOD']])
                ->where('RPRAW_REF', $value[$filter['DOC']])
                ->join('PSI_WMS.dbo.MITM_TBL', 'MITM_ITMCD', 'RPRAW_ITMCOD')
                ->first();
            if ($value['MITM_MODEL'] === '0') {
                if (empty($cekdata)) {
                    MutasiRaw::insert([
                        'RPRAW_ITMCOD' => $value[$filter['ITMCOD']],
                        'RPRAW_UNITMS' => $value[$filter['UNITMS']],
                        'RPRAW_DATEIS' => $value[$filter['TGLBEA']],
                        'RPRAW_QTYINC' => $type == 'INC' ? intval($value[$filter['QTYJUM']]) : 0,
                        'RPRAW_QTYOUT' => $type == 'OUT' ? intval($value[$filter['QTYJUM']]) : 0,
                        'RPRAW_QTYADJ' => 0,
                        'RPRAW_QTYTOT' => intval($value[$filter['QTYJUM']]),
                        'RPRAW_QTYOPN' => 0,
                        'RPRAW_KET' => '',
                        'RPRAW_REF' => $value[$filter['DOC']],
                    ]);
                } else {
                    $cekdata->update([
                        'RPRAW_DATEIS' => $value[$filter['TGLBEA']],
                        'RPRAW_QTYINC' => $type == 'INC' ? intval($value[$filter['QTYJUM']]) : intval($cekdata['RPBEA_QTYJUM']),
                        'RPRAW_QTYOUT' => $type == 'OUT' ? intval($value[$filter['QTYJUM']]) : intval($cekdata['RPBEA_QTYJUM']),
                        'RPRAW_QTYADJ' => 0,
                        'RPRAW_QTYTOT' => intval($value[$filter['QTYJUM']]),
                        'RPRAW_QTYOPN' => 0,
                        'RPRAW_KET' => '',
                    ]);
                }
            }
        }
    }

    public function insertToMutasiFingood($data)
    {
        foreach ($data as $key => $value_head) {
            // Save output qty
            $cekdatafingood = MutasiFinishGood::where('RPGOOD_ITMCOD', $value_head['ITH_ITMCD'])
                ->where('RPGOOD_REF', $value_head['ITH_DOC'])
                ->first();
            if (empty($cekdatafingood)) {
                MutasiFinishGood::create([
                    'RPGOOD_ITMCOD' => $value_head['ITH_ITMCD'],
                    'RPGOOD_UNITMS' => $value_head['MITM_STKUOM'],
                    'RPGOOD_DATEIS' => $value_head['ITH_DATE'],
                    'RPGOOD_QTYINC' => 0,
                    'RPGOOD_QTYOUT' => (int) $value_head['ITH_QTY'],
                    'RPGOOD_QTYADJ' => 0,
                    'RPGOOD_QTYTOT' => (int) $value_head['ITH_QTY'] * -1,
                    'RPGOOD_QTYOPN' => 0,
                    'RPGOOD_KET' => '',
                    'RPGOOD_REF' => $value_head['ITH_DOC']
                ]);
            } else {
                $cekdatafingood->update([
                    'RPGOOD_UNITMS' => $value_head['MITM_STKUOM'],
                    'RPGOOD_DATEIS' => $value_head['ITH_DATE'],
                    'RPGOOD_QTYINC' => 0,
                    'RPGOOD_QTYOUT' => (int) $value_head['ITH_QTY'],
                    'RPGOOD_QTYADJ' => 0,
                    'RPGOOD_QTYTOT' => (int) $value_head['ITH_QTY'] * -1,
                    'RPGOOD_QTYOPN' => 0,
                    'RPGOOD_KET' => '',
                ]);
            }

            $totqtyforpabean = 0;
            foreach ($value_head['dlv'] as $key_dlv => $value_dlv) {
                if ($key_dlv !== 0) {
                    if (
                        $value_dlv['serial']['SER_ITMID'] === $value_head['dlv'][$key_dlv - 1]['serial']['SER_ITMID']
                    ) {
                        $totqtyforpabean = $totqtyforpabean + intval($value_dlv['DLV_QTY']);
                    } else {
                        $totqtyforpabean = 0;
                        $totqtyforpabean = intval($value_dlv['DLV_QTY']);
                    }
                } else {
                    $totqtyforpabean = intval($value_dlv['DLV_QTY']);
                }

                MutasiDokPabean::updateOrCreate(
                    [
                        'RPBEA_DOC' => $value_dlv['DLV_ID'],
                        'RPBEA_ITMCOD' => $value_dlv['serial']['SER_ITMID'],
                    ],
                    [
                        'RPBEA_JENBEA' => $value_dlv['DLV_BCTYPE'],
                        'RPBEA_TGLBEA' => $value_dlv['DLV_BCDATE'],
                        'RPBEA_NUMBEA' => $value_dlv['DLV_NOAJU'],
                        'RPBEA_NUMSJL' => $value_dlv['DLV_ID'],
                        'RPBEA_TGLSJL' => $value_dlv['DLV_DATE'],
                        'RPBEA_NUMPEN' => $value_dlv['DLV_NOPEN'],
                        'RPBEA_TGLPEN' => $value_dlv['DLV_RPDATE'],
                        'RPBEA_CUSSUP' => $value_dlv['DLV_CUSTCD'],
                        'RPBEA_ITMCOD' => $value_dlv['serial']['SER_ITMID'],
                        'RPBEA_QTYJUM' => $totqtyforpabean,
                        'RPBEA_UNITMS' => $value_head['MITM_STKUOM'],
                        'RPBEA_VALAS' => $value_dlv['MCUS_IVCUR'],
                        'RPBEA_QTYTOT' => $totqtyforpabean * intval($value_dlv['serial']['siscn']['si']['SI_DOCREFFPRC']),
                        'RPBEA_QTYSAT' => 1,
                        'RPBEA_TYPE' => 'OUT',
                        'RPBEA_RCVEDT' => $value_dlv['DLV_DATE'],
                        'RPBEA_DOC' => $value_dlv['DLV_ID'],
                    ]
                );
            }

            //Save input qty
            foreach ($value_head['dlv'] as $key_deep => $value_deep) {
                if (!empty($value_deep['serial'])) {
                    $cekdatafingood = MutasiFinishGood::where('RPGOOD_ITMCOD', $value_deep['serial']['SER_ITMID'])
                        ->where('RPGOOD_REF', $value_deep['serial']['SER_ID'])
                        ->first();

                    if (empty($cekdatafingood)) {
                        MutasiFinishGood::create([
                            'RPGOOD_ITMCOD' => $value_deep['serial']['SER_ITMID'],
                            'RPGOOD_UNITMS' => $value_deep['serial']['MITM_STKUOM'],
                            'RPGOOD_DATEIS' => $value_deep['serial']['SER_LUPDT'],
                            'RPGOOD_QTYINC' => (int) $value_deep['serial']['SER_QTY'],
                            'RPGOOD_QTYOUT' => 0,
                            'RPGOOD_QTYADJ' => 0,
                            'RPGOOD_QTYTOT' => (int) $value_deep['serial']['SER_QTY'],
                            'RPGOOD_QTYOPN' => 0,
                            'RPGOOD_KET' => '',
                            'RPGOOD_REF' => $value_deep['serial']['SER_ID']
                        ]);
                    } else {
                        $cekdatafingood->update([
                            'RPGOOD_UNITMS' => $value_deep['serial']['MITM_STKUOM'],
                            'RPGOOD_DATEIS' => $value_deep['serial']['SER_LUPDT'],
                            'RPGOOD_QTYINC' => (int) $value_deep['serial']['SER_QTY'],
                            'RPGOOD_QTYOUT' => 0,
                            'RPGOOD_QTYADJ' => 0,
                            'RPGOOD_QTYTOT' => (int) $value_deep['serial']['SER_QTY'],
                            'RPGOOD_QTYOPN' => 0,
                            'RPGOOD_KET' => '',
                        ]);
                    }
                }
            }
        }

        $this->materialout($data);
    }

    public function UsedMaterial($data)
    {
        foreach ($data as $key => $value_head) { // parse list by ith dan surat jalan keluar
            foreach ($value_head['dlv'] as $val_det_head) { // parse by serial list
                $value = $val_det_head;
                $hasiltotalscan = [];
                if (isset($val_det_head['serial']['spl'])) {
                    foreach ($val_det_head['serial']['spl'] as $key => $val_det) { // parse by spl list by job order serial

                        // kombinasi PSL / PSN
                        $explodedcombination = trim($val_det['SPL_DOC']) . '|' . trim($val_det['SPL_LINE']) . '|' . trim($val_det['SPL_CAT']) . '|' . trim($val_det['SPL_FEDR']);

                        // cari keseluruhan item di table RPSAL_RAW berdasarkan kombinasi PSL / PSN
                        $cekbedawoqty = MutasiRaw::where('RPRAW_ITMCOD', trim($val_det['SPL_ITMCD']))
                            ->where('RPRAW_REF', 'like', $explodedcombination . '%')
                            ->get()
                            ->toArray();

                        // Get list job order di SPL
                        $cekwodispl = SPLMaster::select('SPL_JOBNO')
                            ->where('SPL_DOC', $val_det['SPL_DOC'])
                            ->where('SPL_LINE', $val_det['SPL_LINE'])
                            ->where('SPL_CAT', $val_det['SPL_CAT'])
                            ->where('SPL_FEDR', $val_det['SPL_FEDR'])
                            ->groupBy('SPL_JOBNO')
                            ->orderBy('SPL_JOBNO')
                            ->get();

                        $hasilspl = []; // INIT SPL
                        $countspl = 0; // INIT JUMLAH JOB ORDER
                        foreach ($cekwodispl as $keyspl => $valspl) { // Parsing list job order
                            $countspl++;
                            $hasilspl[$valspl['SPL_JOBNO']] = $countspl;
                        }

                        // Jumlah Finish good per serial di kali dengan QTY Use di SPL_TBL untik mengetahui pemakaian bahan baku
                        $qtyserial = $value['serial']['SER_QTY'] * $val_det['SPL_QTYUSE'];
                        $qtyserial = $val_det['SPLSCN_LUPDT'] === $val_det['APAINISIH'] && !empty($val_det['RETSCN_QTYAFT']) ? $qtyserial - $val_det['RETSCN_QTYAFT'] : $qtyserial;

                        if ($val_det['SPLSCN_LUPDT'] === $val_det['APAINISIH'] && !empty($val_det['RETSCN_QTYAFT']))
                            $hasiltotalscan[$value['DLV_SER']][$val_det['SPL_ORDERNO']][$val_det['SPL_ITMCD']] = [
                                $val_det['SPL_ORDERNO'],
                                $val_det['SPLSCN_LOTNO'],
                                $val_det['SPLSCN_QTY'],
                                $qtyserial,
                                $val_det['RETSCN_QTYAFT'],
                            ];

                        // Jumlah Finish good yang di kirim di kali dengan QTY Use di SPL_TBL untik mengetahui pemakaian bahan baku
                        $qtytot = intval($value_head['ITH_QTY']) * $val_det['SPL_QTYUSE'];

                        if (count($cekbedawoqty) === 0) { // jika di table RPSAL_RAW tidak ditemukan data PSN / PSL Tersebut

                            // Kombinasikan PSN / SPL + Job No + Order / MCZ / No Mesin + Lot Material
                            $combinesplorder = trim($val_det['SPL_DOC']) . '|' . trim($val_det['SPL_LINE']) . '|' . trim($val_det['SPL_CAT']) . '|' . trim($val_det['SPL_FEDR']) . '|' . trim($val_det['SPL_JOBNO']) . '|' . trim($val_det['SPLSCN_ORDERNO'] . '|' . trim($val_det['SPLSCN_LOTNO']));

                            $hasilout = intval($qtyserial) >= intval($val_det['SPL_QTYREQ']) // Jika qty scan lebih besar daripada require maka
                                ? intval($val_det['SPL_QTYREQ']) // insert qty req
                                : intval($qtyserial); // insert qty scan

                            // Insert ke table RPSAL_RAW
                            MutasiRaw::create([
                                'RPRAW_ITMCOD' => trim($val_det['SPL_ITMCD']),
                                'RPRAW_UNITMS' => trim($val_det['MITM_STKUOM']),
                                'RPRAW_DATEIS' => trim($val_det['SPLSCN_LUPDT']),
                                'RPRAW_QTYINC' => 0,
                                'RPRAW_QTYOUT' => $hasilout,
                                'RPRAW_QTYADJ' => 0,
                                'RPRAW_QTYTOT' => $hasilout * -1,
                                'RPRAW_QTYOPN' => 0,
                                'RPRAW_KET' => '',
                                'RPRAW_REF' => $combinesplorder,
                            ]);
                        } else { // jika di table RPSAL_RAW ditemukan data PSN / PSL Tersebut

                            // Kombinasikan PSN / SPL + Job No + Order / MCZ / No Mesin + Lot Material
                            $combinesplorder = trim($val_det['SPL_DOC']) . '|' . trim($val_det['SPL_LINE']) . '|' . trim($val_det['SPL_CAT']) . '|' . trim($val_det['SPL_FEDR']) . '|' . trim($val_det['SPL_JOBNO']) . '|' . trim($val_det['SPLSCN_ORDERNO'] . '|' . trim($val_det['SPLSCN_LOTNO']));

                            $assignedqty2 = 0; // INIT assign qty yang mempunyai kesamaan dengan variable $combinesplorder di table RPSAL_RAW
                            $assignedqtyall = 0; // INIT assign qty keseluruhan SPL / PSN di table RPSAL_RAW
                            $cekoriginalref = []; // INIT variable $combinesplorder found di table RPSAL_RAW
                            foreach ($cekbedawoqty as $key_prevqty => $value_prevqty1) {
                                $assignedqtyall += intval($value_prevqty1['RPRAW_QTYOUT']); // Total keseluruhan item di SPL / PSN
                            }

                            foreach ($cekbedawoqty as $key_prevqty => $value_prevqty) {
                                if ($value_prevqty['RPRAW_REF'] === $combinesplorder) { // Jika variable $combinesplorder ditemukan di table RPSAL_RAW
                                    $assignedqty2 += intval($value_prevqty['RPRAW_QTYOUT']);
                                    $cekoriginalref[] = $value_prevqty['RPRAW_REF'];
                                }
                            }

                            // Hasil total yang akan di inser ke RPSAL_RAW
                            $hasilouts = $qtyserial >= intval($val_det['SPL_QTYREQ']) // Jika qty scan lebih besar daripada require maka
                                ? $hasilspl[$val_det['SPL_JOBNO']] == count($cekwodispl) ? $qtyserial : intval($val_det['SPL_QTYREQ']) // insert qty req
                                : $qtyserial;
                            $hasilout = $hasilouts < 0 ? $hasilouts * -1 : $hasilouts;

                            if (count($cekoriginalref) > 0) { // Jika kombinasi variable $combinesplorder ditemukan
                                $tot = $assignedqty2 + $qtyserial;
                                logger($cekoriginalref);
                                logger([$val_det['SPL_ITMCD'], $val_det['SPLSCN_LOTNO'], $tot, $assignedqtyall, $hasilout, ]);
                                if (($assignedqtyall + $hasilout) <= $qtytot) {
                                    MutasiRaw::whereIn('RPRAW_REF', $cekoriginalref)->update([
                                        'RPRAW_DATEIS' => trim($val_det['SPLSCN_LUPDT']),
                                        'RPRAW_QTYOUT' => $tot,
                                        'RPRAW_QTYTOT' => $tot * -1,
                                    ]);
                                }
                            } else {
                                if ($hasilout > 0) {
                                    MutasiRaw::create([
                                        'RPRAW_ITMCOD' => trim($val_det['SPL_ITMCD']),
                                        'RPRAW_UNITMS' => trim($val_det['MITM_STKUOM']),
                                        'RPRAW_DATEIS' => trim($val_det['SPLSCN_LUPDT']),
                                        'RPRAW_QTYINC' => 0,
                                        'RPRAW_QTYOUT' => $hasilout,
                                        'RPRAW_QTYADJ' => 0,
                                        'RPRAW_QTYTOT' => $hasilout * -1,
                                        'RPRAW_QTYOPN' => 0,
                                        'RPRAW_KET' => '',
                                        'RPRAW_REF' => $combinesplorder,
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function materialout($data)
    {
        $fingood_material = [];
        foreach ($data as $key => $value) {
            $fingood_material[$value['ITH_ITMCD']]['DOC'] = $value['ITH_DOC'];
            $fingood_material[$value['ITH_ITMCD']]['SJ_QTY'] = $value['ITH_QTY'];
            $fingood_material[$value['ITH_ITMCD']]['SJ_DATE'] = $value['ITH_DATE'];

            $countarrdlv = 0;
            foreach ($value['dlv'] as $key_dlv => $value_dlv) {
                if ($value['ITH_ITMCD'] == $value_dlv['serial']['SER_ITMID']) {
                    $fingood_material[$value['ITH_ITMCD']]['JOB'] = $value_dlv['serial']['SER_DOC'];
                    $fingood_material[$value['ITH_ITMCD']]['LIST_SERIAL'][] = [
                        'SERIAL' => $value_dlv['DLV_SER'],
                        'QTY_SERIAL' => $value_dlv['DLV_QTY']
                    ];

                    $countspl = 0;
                    foreach ($value_dlv['serial']['serd2'] as $key_spl => $value_spl) {
                        if (
                            $key_spl !== 0 && ($value_spl['SPL_DOC'] !== $value_dlv['serial']['serd2'][$key_spl - 1]['SERD2_PSNNO'] ||
                                $value_spl['SPL_CAT'] !== $value_dlv['serial']['serd2'][$key_spl - 1]['SERD2_CAT'] ||
                                $value_spl['SPL_LINE'] !== $value_dlv['serial']['serd2'][$key_spl - 1]['SERD2_LINENO'] ||
                                $value_spl['SPL_FEDR'] !== $value_dlv['serial']['serd2'][$key_spl - 1]['SERD2_FR'])
                        ) {
                            $countspl++;
                        }

                        $fingood_material[$value['ITH_ITMCD']]['LIST_SERIAL'][$countarrdlv]['DATA_SPL'][$countspl] = [
                            'PSN_NO' => $value_spl['SERD2_PSNNO'],
                            'PSN_CAT' => $value_spl['SERD2_CAT'],
                            'PSN_LINE' => $value_spl['SERD2_LINENO'],
                            'PSN_FEEDER' => $value_spl['SERD2_FR']
                        ];

                        $countspldet = 0;
                        foreach ($value_dlv['serial']['serd2'] as $key_spl_det => $value_spl_det) {
                            if (
                                $value_spl['SERD2_PSNNO'] == $value_spl_det['SERD2_PSNNO'] &&
                                $value_spl['SERD2_CAT'] == $value_spl_det['SERD2_CAT'] &&
                                $value_spl['SERD2_LINENO'] == $value_spl_det['SERD2_LINENO'] &&
                                $value_spl['SERD2_FR'] == $value_spl_det['SERD2_FR']
                            ) {
                                if ($key_spl_det !== 0 && (
                                    // $value_spl_det['SPL_ORDERNO'] !== $value_dlv['serial']['spl'][$key_spl_det - 1]['SPL_ORDERNO'] ||
                                    $value_spl_det['SPL_ITMCD'] !== $value_dlv['serial']['spl'][$key_spl_det - 1]['SPL_ITMCD'])) {
                                    $countspldet++;
                                }

                                $fingood_material[$value['ITH_ITMCD']]['LIST_SERIAL'][$countarrdlv]['DATA_SPL'][$countspl]['PSN_DET'][] = [
                                    'PSN_CAT' => $value_spl_det['SERD2_CAT'],
                                    // 'MCH_NO' => $value_spl_det['SPL_ORDERNO'],
                                    'PART_NUM' => $value_spl_det['SERD2_ITMCD'],
                                    'UM' => $value_spl_det['MITM_STKUOM'],
                                    // 'QTY_USE' => $value_spl_det['SPL_QTYUSE'],
                                    // 'QTY_REQ_PER_WO' => $value_spl_det['SPL_QTYREQ'],
                                    'SCAN_QTY' => $value_spl_det['SERD2_QTY'],
                                    'SCAN_LOT' => $value_spl_det['SERD2_LOTNO'],
                                    // 'RETURN_QTY' => $value_spl_det['RETSCN_QTYAFT'],
                                ];
                            }
                        }
                    }

                    $countarrdlv++;
                }
            }
        }

        $hasiloutmaterial = [];
        foreach ($fingood_material as $key_mat => $value_mat) {
            foreach ($value_mat['LIST_SERIAL'] as $key_ser => $value_ser) {
                if (isset($value_ser['DATA_SPL'])) {
                    foreach ($value_ser['DATA_SPL'] as $key_spl => $value_spl) {
                        foreach ($value_spl['PSN_DET'] as $key_scn => $value_scn) {
                            $combinedocs = $value_ser['SERIAL'] . '|' . $value_spl['PSN_NO'] . "|" . $value_spl['PSN_CAT'] . "|" . $value_spl['PSN_LINE'] . "|" . $value_spl['PSN_FEEDER'] . "|" . $value_scn['SCAN_LOT'];
                            // $totalout = $key_scn !== 0 && $value_scn['PART_NUM'] == $value_spl['PSN_DET'][$key_scn -1]['PART_NUM'] ? 0 : ($value_ser['QTY_SERIAL'] * $value_scn['QTY_USE']);
                            $totalout = $value_scn['SCAN_QTY'];
                            MutasiRaw::updateOrCreate([
                                'RPRAW_REF' => $combinedocs,
                                'RPRAW_ITMCOD' => trim($value_scn['PART_NUM'])
                            ], [
                                'RPRAW_ITMCOD' => trim($value_scn['PART_NUM']),
                                'RPRAW_UNITMS' => trim($value_scn['UM']),
                                'RPRAW_DATEIS' => trim($value_mat['SJ_DATE']),
                                'RPRAW_QTYINC' => 0,
                                'RPRAW_QTYOUT' => $totalout,
                                'RPRAW_QTYADJ' => 0,
                                'RPRAW_QTYTOT' => $totalout * -1,
                                'RPRAW_QTYOPN' => 0,
                                'RPRAW_KET' => '',
                                'RPRAW_REF' => $combinedocs,
                            ]);

                            $hasiloutmaterial[] = [
                                'DOC' => $value_spl['PSN_NO'] . "|" . $value_spl['PSN_CAT'] . "|" . $value_spl['PSN_LINE'] . "|" . $value_spl['PSN_FEEDER'] . "|" . $value_scn['SCAN_LOT'],
                                'ITEM_MAT' => $value_scn['PART_NUM'],
                                'UM' => $value_scn['UM'],
                                'QTY_OUT' => $key_scn !== 0 && $value_scn['PART_NUM'] === $value_spl['PSN_DET'][$key_scn - 1]['PART_NUM'] ? 0 :   $value_ser['QTY_SERIAL'],
                                // 'QTY_USE' => $value_scn['QTY_USE'],
                                'LOT' => $value_scn['SCAN_LOT'],
                            ];
                        }
                    }
                }
            }
        }
    }
}
