<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WMSFinishGoodCountingJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $inunique, $inunique_qty, $inunique_job;

    public function __construct($inunique, $inunique_qty, $inunique_job)
    {
        $this->inunique = $inunique;
        $this->inunique_qty = $inunique_qty;
        $this->inunique_job = $inunique_job;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $endpoint = 'http://192.168.0.29:8081/wms/DELV/calculate_raw_material_resume';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'inunique' => $this->inunique,
            'inunique_qty' => $this->inunique_qty,
            'inunique_job' => $this->inunique_job
        ];

        // $param = [
        //     'inunique' => ['abcdef','defghijklm'],
        //     'inunique_qty' => [10,10],
        //     'inunique_job' => ['JobB','jobA']
        // ];

        $res = $guzz->request('POST', $endpoint, ['form_params' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);
    }
}
