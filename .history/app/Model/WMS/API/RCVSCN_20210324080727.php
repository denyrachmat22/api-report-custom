<?php

namespace App\Model\WMS\API;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;

class RCVSCN extends Model
{
    use Compoships;

    protected $table = 'RCVSCN_TBL';
    const UPDATED_AT = 'RCVSCN_LUPDT';
    protected $primaryKey = 'RCVSCN_ID'; // or null
    public $incrementing = false;
    protected $fillable = [
        'RCVSCN_ID',
        'RCVSCN_DONO',
        'RCVSCN_LOCID',
        'RCVSCN_ITMCD',
        'RCVSCN_LOTNO',
        'RCVSCN_QTY',
        'RCVSCN_SAVED',
        'RCVSCN_LUPDT',
        'RCVSCN_USRID',
        'RCVSCN_STATUS',
    ];

    public function rcv()
    {
        return $this->hasOne('App\Model\WMS\REPORT\MutasiStok','RCV_ITMCD','RCVSCN_ITMCD');
    }

    public function rcvwithdo()
    {
        return $this->hasOne('App\Model\WMS\REPORT\MutasiStok',['RCV_ITMCD', 'RCV_DONO'],['RCVSCN_ITMCD', 'RCVSCN_DONO']);
    }
}
