<?php

namespace App\Model\WMS\API;

use Illuminate\Database\Eloquent\Model;

class StockSMT extends Model
{
    protected $table = 'v_stock_smt';
}
