<?php

namespace App\Model\WMS\API;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CompositeKey;

class SERRC extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'SERRC_TBL';
}
