<?php

namespace App\Model\WMS\API;

use Illuminate\Database\Eloquent\Model;

// use Awobaz\Compoships\Database\Eloquent\Model;

class RETSCN extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'RETFG_TBL';
}
