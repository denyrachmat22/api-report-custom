<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;

class StockExBC extends Model
{
    protected $table = 'RPCUST.dbo.v_stock_exbc';
}
