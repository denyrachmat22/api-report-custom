<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;
// use App\Helpers\CompositeKey;
use Illuminate\Database\Eloquent\SoftDeletes;
class DetailStock extends Model
{
    // use CompositeKey;
    use SoftDeletes;
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPSAL_BCSTOCK';
    protected $dates = ['deleted_at'];

    // protected $primaryKey = [
    //     'RPSTOCK_DOC',
    //     'RPSTOCK_ITMNUM',
    //     'RPSTOCK_BCDATE',
    //     'RPSTOCK_NOAJU',
    //     'RPSTOCK_TYPE'
    // ];

    protected $fillable = [
        'RPSTOCK_BCTYPE',
        'RPSTOCK_BCNUM',
        'RPSTOCK_BCDATE',
        'RPSTOCK_QTY',
        'RPSTOCK_DOC',
        'RPSTOCK_TYPE',
        'RPSTOCK_ITMNUM',
        'RPSTOCK_NOAJU',
        'RPSTOCK_LOT',
        'RPSTOCK_DOINC',
        'RPSTOCK_REMARK',
        'RPSTOCK_LOC'
    ];

    public $timestamps = true;

    public function serdetTbl()
    {
        return $this->hasMany('App\Model\WMS\API\SERD2TBL','SERD2_ITMCD','RPSTOCK_ITMNUM');
    }

    public function rcvscan()
    {
        return $this->hasOne('App\Model\WMS\API\RCVSCN','RCVSCN_ITMCD','RPSTOCK_ITMNUM');
    }
}
