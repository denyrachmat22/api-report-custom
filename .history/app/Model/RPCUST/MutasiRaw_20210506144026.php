<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CompositeKey;

class MutasiRaw extends Model
{
    use CompositeKey;

    public $timestamps = true;
    protected $guarded = [];
    protected $primaryKey = [
        'RPRAW_REF',
        'RPRAW_ITMCOD'
    ];

    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPSAL_RAW';
}
