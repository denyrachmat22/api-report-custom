<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CompositeKey;

class MutasiRaw extends Model
{
    use CompositeKey;

    public $timestamps = true;
    protected $guarded = [];
    protected $primaryKey = [
        'RPRAW_REF',
        'RPRAW_ITMCOD'
    ];

    protected $fillable = [
        'RPRAW_ITMCOD',
        'RPRAW_UNITMS',
        'RPRAW_DATEIS',
        'RPRAW_QTYINC',
        'RPRAW_QTYOUT',
        'RPRAW_QTYADJ',
        'RPRAW_QTYTOT',
        'RPRAW_QTYOPN',
        'RPRAW_KET',
        'RPRAW_REF'
    ];

    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPSAL_RAW';
}
