<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CompositeKey;

class MutasiFinishGood extends Model
{
    use CompositeKey;

    public $timestamps = true;
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPSAL_FINGOOD';

    protected $primaryKey = [
        'RPBEA_DOC',
        'RPBEA_ITMCOD'
    ];

    protected $fillable = [
        'RPGOOD_UNITMS',
        'RPGOOD_QTYTOT',
        'RPGOOD_QTYOUT',
        'RPGOOD_QTYOPN',
        'RPGOOD_QTYINC',
        'RPGOOD_QTYADJ',
        'RPGOOD_KET',
        'RPGOOD_ITMCOD',
        'RPGOOD_DATEIS',
        'RPGOOD_REF'
    ];
}
