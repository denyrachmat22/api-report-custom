<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\CompositeKey;

class scrapHistDet extends Model
{
    use Compoships, SoftDeletes, CompositeKey;

    public $timestamps = true;
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPSCRAP_HIST_DET';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id',
        'SCR_HIST_ID',
        'SCR_PSN',
        'SCR_CAT',
        'SCR_LINE',
        'SCR_FR',
        'SCR_MC',
        'SCR_MCZ',
        'SCR_ITMCD',
        'SCR_QTPER',
        'SCR_QTY',
        'SCR_LOTNO',
        'SCR_ITMPRC'
    ];

    protected $primaryKey = [
        'SCR_HIST_ID',
        'SCR_PSN',
        'SCR_CAT',
        'SCR_LINE',
        'SCR_FR',
        'SCR_MC',
        'SCR_MCZ',
        'SCR_ITMCD',
        'SCR_QTPER'
    ];

    public function getRcvScn()
    {
        return $this->hasOne('App\Model\WMS\API\RCVSCN', ['RCVSCN_ITMCD', 'RCVSCN_LOTNO'] , ['SCR_ITMCD', 'SCR_LOTNO']);
    }
}
