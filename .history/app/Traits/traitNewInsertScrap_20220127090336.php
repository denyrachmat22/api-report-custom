<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

// Models
use App\Model\RPCUST\processMstr;
use App\Model\RPCUST\processMapDet;
use App\Model\RPCUST\scrapHist;
use App\Model\WMS\API\SERDTBL;
use App\Model\MASTER\ItemMaster;
use App\Model\WMS\API\RCVSCN;

// Jobs
use App\Jobs\scrapDetailInserts;

trait traitNewInsertScrap
{
    public function __construct()
    {
        $cek = scrapHist::whereBetween('created_at', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59'])->orderBy('created_at', 'desc')->withTrashed()->first();
        
        $this->process = processMstr::get()->toArray();
        $this->idTransaction = date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->ID_TRANS, -3) + 1));
        $this->user = '';
        $this->dept = '';
        $this->type = false;
    }

    public function wipTransaction($data)
    {
        $dataAddedProcess = [];
        foreach ($data as $keyData => $valueData) {
            $checkMappingItem = $valueData['mapping_det'];
            
            $dataAddedProcess[$valueData['assy_no']][$valueData['job_no']] = $this->storeDataFinishGood($checkMappingItem, $valueData);
        }

        $returnNotif = [];
        foreach ($dataAddedProcess as $keyItem => $valueItem) {
            foreach ($valueItem as $keyJob => $valueJob) {
                foreach ($valueJob as $keyProc => $valueProc) {
                    $returnNotif[] = [
                        'status' => $valueProc['status'],
                        'message' => "Item: ".($keyItem).", ".$valueProc['message'],
                        'data' => $valueProc['data'],
                    ];
                }
            }
        }

        return $returnNotif;
    }

    public function pendingTransation($data)
    {
        $dataAddedProcess = [];
        foreach ($data as $keyData => $valueData) {
            $checkMappingItem = $valueData['mapping_det'];

            if (!$valueData['is_material']) {
                $dataAddedProcess[$valueData['assy_no']][$valueData['job_no']] = $this->storeDataFinishGood($checkMappingItem, $valueData);
            }
        }

        $returnNotif = [];
        foreach ($dataAddedProcess as $keyItem => $valueItem) {
            foreach ($valueItem as $keyJob => $valueJob) {
                foreach ($valueJob as $keyProc => $valueProc) {
                    $returnNotif[] = [
                        'status' => $valueProc['status'],
                        'message' => "Item: ".($keyItem).", ".$valueProc['message'],
                        'data' => $valueProc['data'],
                    ];
                }
            }
        }

        return $returnNotif;
    }

    public function getMappingItem($item, $process = null)
    {
        $data = processMapDet::where('RPDSGN_ITEMCD', $item);
        
        if (!empty($process)) {
            $data->where('RPDSGN_PRO_ID', $process);
        }

        return $data->get()->toArray();
    }

    public function storeDataMaterial($data, $hasil = [])
    {
        $cekRCVSCN = RCVSCN::where('RCVSCN_ITMCD', $data['assy_no']);
    }

    public function storeDataFinishGood($dataMapping, $data, $hasil = [])
    {
        $nowData = current($dataMapping);

        if (!empty($nowData)) {
            $processNm = strpos($nowData['RPDSGN_PROCD'], 'SMT') !== false
                ? $nowData['RPDSGN_PROCD']
                : $nowData['RPDSGN_PRO_ID'];
            
            $insert = [];
            if (isset($nowData['process_mstr']) && $data[$nowData['process_mstr']['RPDSGN_CODE']] > 0) {
                $insert = $this->insertMasterScrap(
                    $nowData['id'], 
                    $data[$nowData['process_mstr']['RPDSGN_CODE']], 
                    $this->type === 'pending' ? true : false, 
                    $data['job_no'], 
                    $data['reason'], 
                    $processNm, 
                    1, 
                    $data['assy_no'], 
                    $data['date'], 
                    $data['ref_no']
                ); 

                $IDHeader = $insert->id;

                $listProcess = [];

                $dataMapping = $this->defineMappingScrapArray($data['mapping_det']);
                foreach ($dataMapping as $keyProcess => $valueProcess) {
                    // $processNm2 = strpos($valueProcess['RPDSGN_PROCD'], 'SMT') !== false
                    //     ? $valueProcess['RPDSGN_PROCD']
                    //     : $valueProcess['RPDSGN_PRO_ID'];
                    
                    $listProcess[] = $valueProcess['RPDSGN_PROCD'];
                    if ($valueProcess['RPDSGN_PROCD'] === $nowData['RPDSGN_PROCD']) {
                        break;
                    }
                }

                $cekSERD = $this->checkSERDTable(
                    $this->getSERDTable($data['job_no']), 
                    $data['job_no'],
                    $nowData['process_mstr']['RPDSGN_CODE'], 
                    $data[$nowData['process_mstr']['RPDSGN_CODE']],
                    $listProcess
                );
            } else {
                $cekSERD = [];
            }

            if (count($cekSERD) > 0 && $data[$nowData['process_mstr']['RPDSGN_CODE']] > 0) {
                $findByProcess = $this->checkReverseProcess($cekSERD);

                $getAngkaKoma = array_filter($findByProcess, function ($f) {
                    return $this->is_decimal($f['PPSN2_QTPER']);
                });
    
                $cekQtyKoma = $this->cekAngkaKoma($getAngkaKoma, (int)$data['tot_qty']);

                if (!$cekQtyKoma['status']) {
                    $hasil[$processNm] = [
                        'status' => false,
                        'message' => 'QTY will be decimal, please check it !!',
                        'data' => $cekQtyKoma
                    ];
                    
                    if(!empty($IDHeader)) {
                        scrapHist::where('id', $IDHeader)->update(['failed_reason' => 'QTY will be decimal, please check it !!']);

                        scrapHist::where('id', $IDHeader)->delete();
                    }
                } else {              
                    $cekItem = ItemMaster::where('MITM_ITMCD', $data['assy_no'])->first()->toArray();
                    
                    scrapDetailInserts::dispatch(
                        $findByProcess,
                        $data[$nowData['process_mstr']['RPDSGN_CODE']],
                        $IDHeader,
                        $cekItem,
                        $this->idTransaction,
                        $data['job_no'],
                        0,
                        empty($data['date']) ? date('Y-m-d') : date('Y-m-d', strtotime($data['date'])),
                        true
                    )->onQueue('scrapReport');

                    $hasil[$processNm] = [
                        'status' => true,
                        'message' => 'Job found in calculation finish good !!',
                        'data' => $findByProcess,
                        'inserted_data' => $insert
                    ];
                }
            }

            next($dataMapping);
            return $this->storeDataFinishGood($dataMapping, $data, $hasil);
        } else {
            return $hasil;
        }
    }

    public function checkSERDTable($data, $job, $code, $qty, $procd = [])
    {
        $getData = $data;
        if (!empty($procd) || count($procd) > 0) {
            $getData = array_values(array_filter($data, function($f) use($procd) {
                return in_array($f['PPSN1_PROCD'], $procd);
            }));
        }

        $dataFinal = [];
        foreach ($getData as $key => $value) {
            $dataFinal[] = array_merge($value, ['RPDSGN_CODE' => $code]);
        }

        $dataGrouping = [];
        $blackList = [];
        $groupingCount = 0;
        $blCount = 0;
        foreach ($dataFinal as $keyData => $valueData) {
            // Check Main Sub Part
            $getModel = explode('-', $job);
            $cekItemSub = $this->checkItemSub(trim($valueData['PPSN2_SUBPN']), $getModel[count($getModel) - 1], $job);
            if (count($cekItemSub) > 0) {
                $cekBlacklist = array_filter($blackList, function ($f) use ($valueData) {
                    return $f == $valueData['PPSN2_SUBPN'];
                });

                if (count($cekBlacklist) === 0) {
                    $dataGrouping[$valueData['PPSN2_SUBPN']][$groupingCount] = array_merge(
                        $valueData,
                        ['STATUS' => 'PUNYA SUB']
                    );
                    $groupingCount = $groupingCount + 1;

                    foreach ($cekItemSub as $keyUsedSub => $valueUsedSub) {
                        $hasilnya = array_filter($dataFinal, function ($f) use ($valueUsedSub) {
                            return trim($f['PPSN2_SUBPN']) == $valueUsedSub->CMI_ITMSUB;
                        }, ARRAY_FILTER_USE_BOTH);

                        if (count($hasilnya) > 0) {
                            foreach ($hasilnya as $keySum => $valueSum) {
                                $blackList[$blCount] = $valueSum['PPSN2_SUBPN'];
                                if (
                                    $valueData['PPSN2_SUBPN'] !== $valueSum['PPSN2_SUBPN'] &&
                                    $valueData['SERD_LOTNO'] !== $valueSum['SERD_LOTNO']
                                ) {
                                    $dataGrouping[$valueData['PPSN2_SUBPN']][$groupingCount] = array_merge(
                                        $valueSum,
                                        ['STATUS' => 'PUNYA SUB & ADA DI SERD ITEM']
                                    );

                                    $blCount++;
                                }
                            }
                        }
                    }
                }
            } else {
                $cekBlacklist = array_filter($blackList, function ($f) use ($valueData) {
                    return $f == $valueData['PPSN2_SUBPN'];
                });

                if (count($cekBlacklist) === 0) {
                    $dataGrouping[$valueData['PPSN2_SUBPN']][$groupingCount] = array_merge(
                        $valueData,
                        ['STATUS' => 'TIDAK PUNYA SUB']
                    );
                }
            }

            $groupingCount++;
        }
        
        // logger($dataGrouping);

        // return $dataGrouping;

        $hasil = [];
        $countHasil = 0;
        foreach ($dataGrouping as $keyGroup => $valueGroup) {
            $fifoData = $this->FIFOSERDLOT(array_values($valueGroup), $qty);
            if ($fifoData && count($fifoData) > 0) {
                foreach ($fifoData as $keyFiTa => $valueFiTa) {
                    $hasil[$countHasil] = $valueFiTa;

                    $countHasil++;
                }
            }
        }

        return $hasil;
    }

    public function FIFOSERDLOT($data, $qty, $hasil = [])
    {
        $now = current($data);
        // logger($now);
        if (!empty($now)) {
            if ((int)$now['SERD_QTY'] > 0) {
                $total = ((int)round($now['SERD_QTY'])) - $qty;

                $totalPer = 0;
                $totalQty = 0;
                foreach ($data as $keyPer => $valuePer) {
                    if ($valuePer['PPSN2_SUBPN'] == $now['PPSN2_SUBPN']) {
                        $totalPer += round((float)$valuePer['PPSN2_QTPER'], 2);
                        $totalQty += round($valuePer['SERD_QTY'], 2);
                    }
                }

                // if ($valuePer['PPSN2_SUBPN'] == '215367500') {
                //     logger(json_encode([
                //         'item' => '215367500',
                //         'totalPer' => $totalPer,
                //         'totalQty' => $totalQty,
                //         'count' => count($hasil),
                //         'is_dec' => $this->is_decimal($totalPer),
                //         'rounding' => round($totalPer),
                //         'data' => $data,
                //     ]));
                // }

                if ($total >= 0) {
                    $now['PPSN2_QTPER'] = count($hasil) === 0
                        ? (
                            $this->is_decimal($totalPer)
                            ? round($totalPer)
                            : $totalPer
                        ) : round($now['PPSN2_QTPER'], 2);
                    $hasil[] = $now;
                } else {
                    $totalnya = $qty - $now['SERD_QTY'];
                    $now['PPSN2_QTPER'] = round($now['PPSN2_QTPER']);
                    $hasil[] = $now;
                    next($data);
                    return $this->FIFOSERDLOT($data, $totalnya, $hasil);
                }

                return $hasil;
            } else {
                next($data);
                return $this->FIFOSERDLOT($data, $qty, $hasil);
            }
        } else {
            return $hasil;
        }
    }

    public function is_decimal($val)
    {
        return is_numeric($val) && floor($val) != $val;
    }

    public function getSERDTable($job)
    {
        $getData = SERDTBL::select(
            DB::raw('MAX(SERD_PSNNO) AS PPSN1_PSNNO'),
            DB::raw('SERD_LINENO AS PPSN1_LINENO'),
            DB::raw('SERD_ITMCD AS PPSN2_SUBPN'),
            DB::raw('SERD_PROCD AS PPSN1_PROCD'),
            DB::raw("CAST(SUM(st.SERD_QTY) / (
                SELECT MAX(st2.PPSN1_SIMQT) FROM XPPSN1 st2
                WHERE st2.PPSN1_WONO = st.SERD_JOB
                -- AND st2.PPSN1_PSNNO = st.SERD_PSNNO
                AND st2.PPSN1_LINENO = st.SERD_LINENO
            ) AS DECIMAL(10,2)) AS PPSN2_QTPER"),
            DB::raw("SUM(st.SERD_QTY) / (
                SELECT MAX(st2.PPSN1_SIMQT) FROM XPPSN1 st2
                WHERE st2.PPSN1_WONO = st.SERD_JOB
                -- AND st2.PPSN1_PSNNO = st.SERD_PSNNO
                AND st2.PPSN1_LINENO = st.SERD_LINENO
            ) AS REAL_PER"),
            'SERD_JOB',
            'SERD_LOTNO',
            DB::raw("SUM(st.SERD_QTY) - COALESCE(SUM(scrdet.QTYSCR), 0) AS SERD_QTY"),
            'MITM_ITMD1',
            'MITM_ITMD2'
        )->from('SERD_TBL as st')
            ->where('SERD_JOB', $job)
            ->join('MITM_TBL', 'SERD_ITMCD', 'MITM_ITMCD')
            ->leftjoin(DB::raw('(
                    SELECT
                        DOC_NO,
                        SCR_ITMCD,
                        SCR_LOTNO,
                        SUM(rhd.SCR_QTY) as QTYSCR
                    FROM PSI_RPCUST.dbo.RPSCRAP_HIST rh
                    INNER JOIN PSI_RPCUST.dbo.RPSCRAP_HIST_DET rhd ON rh.id = rhd.SCR_HIST_ID
                    WHERE rh.deleted_at IS NULL
                    AND rhd.deleted_at IS NULL
                    GROUP BY
                        DOC_NO,
                        SCR_ITMCD,
                        SCR_LOTNO
                ) scrdet'), function ($j) {
                    $j->on('scrdet.DOC_NO', 'SERD_JOB');
                    $j->on('scrdet.SCR_ITMCD', 'SERD_ITMCD');
                    $j->on('scrdet.SCR_LOTNO', 'SERD_LOTNO');
                })
            ->groupBy(
                'SERD_ITMCD',
                'SERD_LINENO',
                'SERD_PROCD',
                'SERD_LOTNO',
                'MITM_ITMD1',
                'MITM_ITMD2',
                'SERD_JOB'
            )
            ->orderBy('SERD_ITMCD')
            ->get()
            ->toArray();

        return $getData;
    }

    public function checkItemSub($item_mtr, $item_model, $job)
    {
        $searchJob = DB::select(DB::raw("SET NOCOUNT ON;EXEC PSI_RPCUST.dbo.sp_check_mapping_item @item_material = ?, @item_model = ?, @job = ?"), [
            $item_mtr,
            $item_model,
            $job
        ]);

        return $searchJob;
    }

    public function checkReverseProcess($data)
    {
        $parsed = [];

        $searchPCBPer = array_values(array_filter($data, function ($f) {
            // logger(json_encode($f));
            $itemD1 = trim($f['MITM_ITMD1']);
            return
                trim($f['PPSN1_PROCD']) === 'SMT-AB'
                && (strpos($itemD1, 'PRINT CIRCUIT BOARD') !== false ||
                    strpos($itemD1, 'PCB') !== false ||
                    strpos($itemD1, 'P.C.B') !== false ||
                    strpos($itemD1, 'P,C,B') !== false);
        }));

        // logger($searchPCBPer);

        foreach ($data as $key => $value) {
            if (!empty($value['PPSN1_PROCD'])) {
                if (trim($value['PPSN1_PROCD']) === 'SMT-AB') {
                    // Jika komponen bukan PCB maka di bagi 2
                    if (
                        (strpos($value['MITM_ITMD1'], 'PRINT CIRCUIT BOARD') !== false ||
                            strpos($value['MITM_ITMD1'], 'PCB') === false ||
                            strpos($value['MITM_ITMD1'], 'P.C.B') === false ||
                            strpos($value['MITM_ITMD1'], 'P,C,B') === false)
                    ) {
                        $getPCBPer = $searchPCBPer[0]['PPSN2_QTPER'];

                        $parsed[] = [
                            "MITM_ITMD1" => $value['MITM_ITMD1'],
                            "MITM_ITMD2" => $value['MITM_ITMD2'],
                            "PPSN1_LINENO" => $value['PPSN1_LINENO'],
                            "PPSN1_PSNNO" => $value['PPSN1_PSNNO'],
                            "PPSN2_QTPER" => (0.5 / $getPCBPer) * $value['PPSN2_QTPER'],
                            "PPSN2_SUBPN" => $value['PPSN2_SUBPN'],
                            "PPSN1_PROCD" => $value['PPSN1_PROCD'],
                            "SERD_JOB" => $value['SERD_JOB'],
                            "RPDSGN_CODE" => $value['RPDSGN_CODE'],
                            "REAL_PER" => $value['REAL_PER']
                        ];
                    } else {
                        $parsed[] = $value;
                    }
                } else {
                    $parsed[] = $value;
                }
            }
        }

        return $parsed;
    }

    public function cekAngkaKoma($data, $qty)
    {
        $hasil = [];
        foreach ($data as $key => $value) {
            $hasil[trim($value['PPSN2_SUBPN'])][$key] = [
                'qtper' => $value['PPSN2_QTPER'],
                'qty' => $qty,
                'jumlah' => $qty * $value['PPSN2_QTPER'],
                'process' => $value['PPSN1_PROCD'],
                'real_per' => $value['REAL_PER']
            ];
        }

        $cekData = [];
        foreach ($hasil as $keyJum => $valueJum) {
            $sumnya1 = 0;
            foreach (array_values($valueJum) as $keyJum2 => $valueJum2) {
                $sumnya1 += $valueJum2['jumlah'];
            }
            $cekData[] = [
                'ITEM' => $keyJum,
                'TOTAL' => round($sumnya1, 2)
            ];
        }

        $cek = array_filter(array_values($hasil), function ($f) {
            $sumnya = 0;
            foreach (array_values($f) as $key => $value) {
                $sumnya += $value['jumlah'];
            }

            return $this->is_decimal(round($sumnya, 2));
        });

        $proc = [];
        foreach ($cek as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $proc[] = $value2['process'];
            }
        }

        if (count($cek) > 0) {
            return [
                'status' => false,
                'process' => implode(",", $proc),
                'data' => $cekData
            ];
        } else {
            return [
                'status' => true,
                'process' => implode(",", $proc),
                'data' => $cekData
            ];
        }
    }

    public function insertMasterScrap($idMapping, $qty, $isPending, $doc, $remark, $process, $model, $item, $date, $ref = '')
    {
        return scrapHist::create([
            'DSGN_MAP_ID' => $idMapping,
            'USERNAME' => $this->user,
            'DEPT' =>  $this->dept,
            'QTY' => $qty,
            'QTY_SCRAP' => $isPending,
            'ID_TRANS' => $this->idTransaction,
            'TYPE_TRANS' => 'NORMAL',
            'DOC_NO' => $doc,
            'REASON' => $remark,
            'SCR_PROCD' => $process,
            'MDL_FLAG' => $model,
            'ITEMNUM' => $item,
            'DATE_OUT' => empty($date) || $date === '1970-01-01' ? date('Y-m-d') : date('Y-m-d', strtotime($date)),
            'REF_NO' => $ref
        ]);
    }

    public function defineMappingScrapArray($data)
    {
        $datanya = [];
        foreach ($data as $key => $f) {
            if ($f['RPDSGN_PRO_ID'] == 'SMT-HW' && !empty($f['RPDSGN_PROCD'])) {
                $datanya[] = ['RPDSGN_PROCD' => 'SMT-HW'];
            } elseif ($f['RPDSGN_PRO_ID'] == 'SMT-HWADD' && !empty($f['RPDSGN_PROCD'])) {
                $datanya[] = ['RPDSGN_PROCD' => 'SMT-HWAD'];
            } elseif ($f['RPDSGN_PRO_ID'] == 'SMT-SP' && !empty($f['RPDSGN_PROCD'])) {
                $datanya[] = ['RPDSGN_PROCD' => 'SMT-SP'];
            } else {
                if ($f['RPDSGN_PROCD'] == 'SMT-AV') {
                    $datanya[] = ['RPDSGN_PROCD' => 'SMT-AX'];
                } elseif ($f['RPDSGN_PROCD'] == 'SMT-RD') {
                    $datanya[] = ['RPDSGN_PROCD' => 'SMT-RG'];
                } else {
                    $datanya[] = ['RPDSGN_PROCD' => $f['RPDSGN_PROCD']];
                }
            }
        }

        return $datanya;
    }
}