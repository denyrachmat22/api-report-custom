<?php

namespace App\Traits;

use App\Model\RPCUST\processMstr;
use Illuminate\Support\Facades\DB;
use App\Model\RPCUST\processMapDet;
use App\Model\MASTER\ItemMaster;
use App\Jobs\scrapDetailInserts;
use App\Model\RPCUST\scrapHist;
use App\Model\WMS\API\SERDTBL;
use App\Model\RPCUST\scrapHistDet;
use App\Model\WMS\API\RCVSCN2;
use App\Model\WMS\REPORT\MutasiStok;
use App\Model\MASTER\ITHMaster;

use App\Jobs\scrapDetailMaterial;

trait traitInsertScrapDetailFromJobs
{
    public function getListMapID($mapID, $item, $job)
    {
        $getProcMaster = processMstr::get()->toArray();

        return $this->getPsnListByJobAndProcd($getProcMaster, $job, $mapID, $item);
    }

    public function submitToJobs($jobs, $processID, $item, $qty, $IDHeader, $idTrans, $scrapDate = '')
    {
        // logger('masuk ke traits lohh');
        $itemnya = trim($item);
        $jobnya = trim($jobs);

        $process = processMstr::get()->toArray();

        $dataPSNList = $this->getPsnListByJobAndProcd(
            $process,
            $jobnya,
            $processID,
            $itemnya,
            $qty
        );
        // return $dataPSNList;

        if ($dataPSNList['status'] == true) {

            $findByProcess = $this->checkReverseProcess($dataPSNList['data']);

            // logger(json_encode($findByProcess));
            // return $dataPSNList;

            // $checkMainSub = $this->checkItemSub($findByProcess);

            // logger(json_encode($checkMainSub));

            $getAngkaKoma = array_filter($findByProcess, function ($f) {
                return $f['PPSN2_QTPER'] < 1;
            });

            logger($getAngkaKoma);

            $cekQtyKoma = $this->cekAngkaKoma($getAngkaKoma, $qty, $itemnya, $jobnya);

            if ($cekQtyKoma['status'] === true) {

                if (count($findByProcess) > 0) {
                    $cekItem = ItemMaster::where('MITM_ITMCD', $itemnya)->first()->toArray();

                    $insertnya[] = $dataPSNList;

                    scrapDetailInserts::dispatch(
                        $findByProcess,
                        $qty,
                        $IDHeader,
                        $cekItem,
                        $idTrans,
                        $jobnya,
                        0,
                        empty($scrapDate) ? date('Y-m-d') : date('Y-m-d', strtotime($scrapDate)),
                        true
                    )->onQueue('scrapReport');
                    return [
                        'status' => true,
                        // 'message' => 'Row: ' . $data[0] . ' with item: ' . $data[1] . ' ' . $dataPSNList['desc'] . '. will be decimal qty, please check it.',
                        'message' => $dataPSNList['desc'] . '. success',
                        // 'data' => $data,
                        // 'test' => $this->type,
                        // 'id' => $this->idTrans,
                        // 'detail' => []
                    ];
                } else {
                    scrapHist::where('id', $IDHeader)->update(['failed_reason' => 'Data calculation not found in Finish Good !!']);

                    scrapHist::where('id', $IDHeader)->delete();

                    return [
                        'status' => false,
                        // 'message' => 'Row: ' . $data[0] . ' with item: ' . $data[1] . ' ' . $dataPSNList['desc'] . '. will be decimal qty, please check it.',
                        'message' => $dataPSNList['desc'] . '. data calculation not found in Finish Good.',
                        // 'data' => $data,
                        // 'test' => $this->type,
                        // 'id' => $this->idTrans,
                        // 'detail' => []
                    ];
                }
            } else {
                logger($cekQtyKoma);
                scrapHist::where('id', $IDHeader)->update(['failed_reason' => $dataPSNList['desc'] . '. QTY Scrap will be decimal.']);

                scrapHist::where('id', $IDHeader)->delete();

                return [
                    'status' => false,
                    // 'message' => 'Row: ' . $data[0] . ' with item: ' . $data[1] . ' ' . $dataPSNList['desc'] . '. will be decimal qty, please check it.',
                    'message' => $dataPSNList['desc'] . '. QTY Scrap will be decimal.',
                    'data' => $cekQtyKoma,
                    // 'test' => $this->type,
                    // 'id' => $this->idTrans,
                    // 'detail' => []
                ];
            }
        } else {
            scrapHist::where('id', $IDHeader)->update(['failed_reason' => $dataPSNList['desc']]);

            scrapHist::where('id', $IDHeader)->delete();

            return [
                'status' => false,
                // 'message' => 'Row: ' . $data[0] . ' with item: ' . $data[1] . ' ' . $dataPSNList['desc'] . '. Please check your mapping',
                'message' => $dataPSNList['desc'] . '. Please check your mapping',
                // 'data' => $data,
                // 'test' => $this->type,
                // 'id' => $this->idTrans,
                'detail' => []
            ];
        }
    }

    public function checkReverseProcess($data)
    {
        $parsed = [];

        // logger(json_encode($data));

        $searchPCBPer = array_values(array_filter($data, function ($f) {
            $itemD1 = trim($f['MITM_ITMD1']);
            return
                trim($f['PPSN1_PROCD']) === 'SMT-AB'
                && (strpos($itemD1, 'PRINT CIRCUIT BOARD') !== false ||
                    strpos($itemD1, 'PCB') !== false ||
                    strpos($itemD1, 'P.C.B') !== false ||
                    strpos($itemD1, 'P,C,B') !== false);
        }));

        // logger($searchPCBPer);

        foreach ($data as $key => $value) {
            if (!empty($value['PPSN1_PROCD'])) {
                if (trim($value['PPSN1_PROCD']) === 'SMT-AB') {
                    // Jika komponen bukan PCB maka di bagi 2
                    if (
                        (strpos($value['MITM_ITMD1'], 'PRINT CIRCUIT BOARD') !== false ||
                            strpos($value['MITM_ITMD1'], 'PCB') === false ||
                            strpos($value['MITM_ITMD1'], 'P.C.B') === false ||
                            strpos($value['MITM_ITMD1'], 'P,C,B') === false)
                    ) {
                        $getPCBPer = $searchPCBPer[0]['PPSN2_QTPER'];

                        $parsed[] = [
                            "MITM_ITMD1" => $value['MITM_ITMD1'],
                            "MITM_ITMD2" => $value['MITM_ITMD2'],
                            "PPSN1_LINENO" => $value['PPSN1_LINENO'],
                            "PPSN1_PSNNO" => $value['PPSN1_PSNNO'],
                            "PPSN2_QTPER" => (0.5 / $getPCBPer) * $value['PPSN2_QTPER'],
                            "PPSN2_SUBPN" => $value['PPSN2_SUBPN'],
                            "PPSN1_PROCD" => $value['PPSN1_PROCD'],
                            "SERD_JOB" => $value['SERD_JOB'],
                            "RPDSGN_CODE" => $value['RPDSGN_CODE'],
                            "REAL_PER" => $value['REAL_PER']
                        ];
                    } else {
                        $parsed[] = $value;
                    }
                } else {
                    $parsed[] = $value;
                }
            }
        }

        return $parsed;
    }

    public function getPsnListByJobAndProcd($listProcess, $job, $idProcess, $item, $qty, $hasil = []) // Recursive here
    {
        $cekProcessNow = current($listProcess);
        if (!empty($cekProcessNow)) {
            // logger('Tidak kosong');
            $getDataMapping = processMapDet::select(
                'RPDSGN_PRO_ID',
                DB::raw("CASE WHEN LEFT(RPDSGN_PROCD, 3) = 'SMT'
                        THEN RPDSGN_PROCD
                        ELSE RPDSGN_PRO_ID
                        END AS RPDSGN_PROCD
                    ")
            )
                ->where('RPDSGN_ITEMCD', $item)
                ->where('RPDSGN_PRO_ID', $cekProcessNow['RPDSGN_CODE'])
                ->orderBy('RPDSGN_PRO_ID')
                ->whereNotNull('RPDSGN_MSTR_ID')
                ->whereRaw("RPDSGN_PROCD <> ''")
                ->groupBy(
                    'RPDSGN_PRO_ID',
                    DB::raw("CASE WHEN LEFT(RPDSGN_PROCD, 3) = 'SMT'
                        THEN RPDSGN_PROCD
                        ELSE RPDSGN_PRO_ID
                        END
                    ")
                )
                ->get()
                ->toArray();

            // logger('hitung');
            // logger(count($getDataMapping));
            if (count($getDataMapping) === 0) {
                $whereJob = empty($job) ? NULL : "AND PDPP_WONO = '" . $job . "'";

                $getSeq = strpos($cekProcessNow['RPDSGN_CODE'], '#') !== false
                    ? "AND MBO2_SEQNO = " . str_replace("#", "", $cekProcessNow['RPDSGN_CODE']) . " AND MBO2_PROCD NOT IN ('SMT-HW', 'SMT-HWADD', 'SMT-SP')"
                    : "AND MBO2_PROCD = '" . $cekProcessNow['RPDSGN_CODE'] . "'";
                $processJobs = DB::select("
                            select
                                CONCAT(LTRIM(RTRIM(wo.PDPP_WONO)), '-', bo.MBO2_SEQNO) AS id,
                                bo.MBO2_MDLCD AS RPDSGN_ITEMCD,
                                LTRIM(RTRIM(bo.MBO2_PROCD)) AS RPDSGN_PROCD,
                                CASE WHEN bo.MBO2_PROCD = 'SMT-HW' OR bo.MBO2_PROCD = 'SMT-HWAD' OR bo.MBO2_PROCD = 'SMT-SP'
                                    THEN LTRIM(RTRIM(bo.MBO2_PROCD))
                                    ELSE LTRIM(RTRIM(CONCAT('#', bo.MBO2_SEQNO)))
                                END AS RPDSGN_PRO_ID,
                                CASE WHEN (
                                    SELECT TOP 1 dt.DLV_CONSIGN FROM PSI_WMS.dbo.DLV_TBL dt
                                    INNER JOIN PSI_WMS.dbo.SERD2_TBL st ON st.SERD2_SER = dt.DLV_SER
                                    WHERE st.SERD2_JOB = wo.PDPP_WONO
                                    AND dt.DLV_CONSIGN IS NOT NULL
                                ) = 'IEI'
                                    THEN 'MFG1'
                                    ELSE 'MFG2'
                                END
                                as TEST,
                                bo.MBO2_BOMRV,
                                wo.PDPP_WONO,
                                wo.PDPP_BOMRV
                            from XMBO2 bo
                            INNER JOIN
                                XWO wo ON wo.PDPP_MDLCD = bo.MBO2_MDLCD
                                AND wo.PDPP_BOMRV = bo.MBO2_BOMRV
                            WHERE MBO2_MDLCD = '" . $item . "'
                            $getSeq
                            $whereJob
                            ORDER BY bo.MBO2_SEQNO
                        ");

                $result = array_map(function ($value) {
                    return (array)$value;
                }, $processJobs);

                $cekSMTAB = array_filter($result, function ($f) {
                    return $f['RPDSGN_PROCD'] === 'SMT-AB';
                });

                if (count($cekSMTAB) > 0) {
                    if (count($cekSMTAB) > 1) {
                        $getDataMapping = $result;
                    } else {
                        array_push($processJobs, [
                            'MBO2_BOMRV' => $cekSMTAB[0]['MBO2_BOMRV'],
                            'PDPP_BOMRV' => $cekSMTAB[0]['PDPP_BOMRV'],
                            'PDPP_WONO' => $cekSMTAB[0]['PDPP_WONO'],
                            'RPDSGN_ITEMCD' => $cekSMTAB[0]['RPDSGN_ITEMCD'],
                            'RPDSGN_PROCD' => $cekSMTAB[0]['RPDSGN_PROCD'],
                            'RPDSGN_PRO_ID' => '#2',
                            'TEST' => $cekSMTAB[0]['TEST'],
                        ]);

                        $getDataMapping = $result;
                    }
                } else {
                    $getDataMapping = $result;
                }
            }

            $dataProcd = [];
            foreach ($this->defineMappingScrapArray($getDataMapping) as $keyFail => $valueFail) {
                $dataProcd[] = $valueFail['RPDSGN_PROCD'];
            }

            // logger(json_encode($cekProcessNow));

            if ($idProcess == $cekProcessNow['RPDSGN_CODE']) {
                $hasil = [
                    'status' => true,
                    'desc' => implode(", ", $dataProcd) . ' Found',
                    'list_process' => $dataProcd,
                    'data' => array_merge(
                        $hasil,
                        $this->psnListByJobSerD($job, $cekProcessNow['RPDSGN_CODE'], $qty, $this->defineMappingScrapArray($getDataMapping))
                    )
                ];

                return $hasil;
            } else {
                if (count($getDataMapping) > 0) {
                    if (count($this->psnListByJobSerD($job, $cekProcessNow['RPDSGN_CODE'], $qty, $this->defineMappingScrapArray($getDataMapping))) > 0) {
                        $hasil = array_merge(
                            $hasil,
                            $this->psnListByJobSerD($job, $cekProcessNow['RPDSGN_CODE'], $qty, $this->defineMappingScrapArray($getDataMapping))
                        );

                        // logger('masuk sini');
                        // logger($hasil);

                        next($listProcess);
                        return $this->getPsnListByJobAndProcd($listProcess, $job, $idProcess, $item, $qty, $hasil);
                    } else {
                        $hasil = array_merge(
                            $hasil,
                            ['RPDSGN_CODE' => $cekProcessNow['RPDSGN_CODE']],
                            [
                                'status' => false,
                                'list_process' => $dataProcd,
                                'desc' => implode(", ", $dataProcd) . ' Not found in calculation finish good !!'
                            ]
                        );

                        return $hasil;
                    }
                } else {
                    next($listProcess);
                    return $this->getPsnListByJobAndProcd($listProcess, $job, $idProcess, $item, $qty, $hasil);
                }
            }
        } else {
            // logger('Kosong');
            return $hasil;
        }
    }

    public function defineMappingScrapArray($data)
    {
        $datanya = [];
        foreach ($data as $key => $f) {
            if ($f['RPDSGN_PRO_ID'] == 'SMT-HW' && !empty($f['RPDSGN_PROCD'])) {
                $datanya[] = ['RPDSGN_PROCD' => 'SMT-HW'];
            } elseif ($f['RPDSGN_PRO_ID'] == 'SMT-HWADD' && !empty($f['RPDSGN_PROCD'])) {
                $datanya[] = ['RPDSGN_PROCD' => 'SMT-HWAD'];
            } elseif ($f['RPDSGN_PRO_ID'] == 'SMT-SP' && !empty($f['RPDSGN_PROCD'])) {
                $datanya[] = ['RPDSGN_PROCD' => 'SMT-SP'];
            } else {
                if ($f['RPDSGN_PROCD'] == 'SMT-AV') {
                    $datanya[] = ['RPDSGN_PROCD' => 'SMT-AX'];
                } elseif ($f['RPDSGN_PROCD'] == 'SMT-RD') {
                    $datanya[] = ['RPDSGN_PROCD' => 'SMT-RG'];
                } else {
                    $datanya[] = ['RPDSGN_PROCD' => $f['RPDSGN_PROCD']];
                }
            }
        }

        return $datanya;
    }

    public function psnListByJobSerD($job, $code, $qty, $procd = [])
    {
        // logger($procd);
        $getData = SERDTBL::select(
            DB::raw('MAX(SERD_PSNNO) AS PPSN1_PSNNO'),
            DB::raw('SERD_LINENO AS PPSN1_LINENO'),
            DB::raw('SERD_ITMCD AS PPSN2_SUBPN'),
            DB::raw('SERD_PROCD AS PPSN1_PROCD'),
            DB::raw("SUM(st.SERD_QTY) / (
                SELECT MAX(st2.PPSN1_SIMQT) FROM XPPSN1 st2
                WHERE st2.PPSN1_WONO = st.SERD_JOB
                -- AND st2.PPSN1_PSNNO = st.SERD_PSNNO
                AND st2.PPSN1_LINENO = st.SERD_LINENO
            ) AS PPSN2_QTPER"),
            DB::raw("SUM(st.SERD_QTY) / (
                SELECT MAX(st2.PPSN1_SIMQT) FROM XPPSN1 st2
                WHERE st2.PPSN1_WONO = st.SERD_JOB
                -- AND st2.PPSN1_PSNNO = st.SERD_PSNNO
                AND st2.PPSN1_LINENO = st.SERD_LINENO
            ) AS REAL_PER"),
            'SERD_JOB',
            'SERD_LOTNO',
            DB::raw("SUM(st.SERD_QTY) - (
                SELECT
                    SUM(rhd.SCR_QTY)
                FROM PSI_RPCUST.dbo.RPSCRAP_HIST rh
                INNER JOIN PSI_RPCUST.dbo.RPSCRAP_HIST_DET rhd ON rh.id = rhd.SCR_HIST_ID
                WHERE rh.DOC_NO = SERD_JOB
                AND rhd.SCR_ITMCD = SERD_ITMCD
                AND rhd.SCR_LOTNO = SERD_LOTNO
                AND rh.deleted_at IS NULL
                AND rhd.deleted_at IS NULL
            ) AS SERD_QTY"),
            'MITM_ITMD1',
            'MITM_ITMD2'
        )->from('SERD_TBL as st')
            ->where('SERD_JOB', $job)
            ->join('MITM_TBL', 'SERD_ITMCD', 'MITM_ITMCD')
            ->groupBy(
                'SERD_ITMCD',
                'SERD_LINENO',
                'SERD_PROCD',
                'SERD_LOTNO',
                'SERD_QTYREQ',
                'MITM_ITMD1',
                'MITM_ITMD2',
                'SERD_JOB'
            )
            ->orderBy('SERD_ITMCD');

        if (!empty($procd) || count($procd) > 0) {
            $getProCD = [];
            foreach ($procd as $key => $value) {
                $getProCD[] = $value['RPDSGN_PROCD'];
            }
            // logger($getData->where('SERD_PROCD', $procd)->toSql());
            $getData->whereIn('SERD_PROCD', $getProCD)->get();
        }

        $dataFinal = [];
        foreach ($getData->get()->toArray() as $key => $value) {
            $dataFinal[] = array_merge($value, ['RPDSGN_CODE' => $code]);
        }

        // logger($dataFinal);
        // NEW

        $dataGrouping = [];
        $blackList = [];
        $groupingCount = 0;
        $blCount = 0;
        foreach ($dataFinal as $keyData => $valueData) {
            // Check Main Sub Part
            $getModel = explode('-', $job);
            $cek = $this->checkItemSub(trim($valueData['PPSN2_SUBPN']), $getModel[count($getModel) - 1], $job);
            if (count($cek) > 0) {
                foreach ($cek as $keyUsedSub => $valueUsedSub) {
                    $hasilnya = array_filter($dataFinal, function ($f) use ($valueUsedSub) {
                        return trim($f['PPSN2_SUBPN']) == $valueUsedSub->CMI_ITMSUB;
                    }, ARRAY_FILTER_USE_BOTH);

                    $cekBlacklist = array_filter($blackList, function ($f) use ($valueData) {
                        return $f == $valueData['PPSN2_SUBPN'];
                    });

                    if (count($cekBlacklist) === 0) {
                        if (count($hasilnya) > 0) {
                            foreach ($hasilnya as $keySum => $valueSum) {
                                $blackList[$blCount] = $valueSum['PPSN2_SUBPN'];
                                $dataGrouping[$valueData['PPSN2_SUBPN']][$groupingCount] = array_values(array_merge([$valueData], [$valueSum]));

                                $blCount++;
                            }
                        }
                    }
                }
            } else {
                $cekBlacklist = array_filter($blackList, function ($f) use ($valueData) {
                    return $f == $valueData['PPSN2_SUBPN'];
                });
                if (count($cekBlacklist) === 0) {
                    $dataGrouping[$valueData['PPSN2_SUBPN']][$groupingCount] = $valueData;
                }
            }

            $groupingCount++;
        }

        // logger($blackList);

        logger(json_encode($dataGrouping));

        $hasil = [];
        foreach ($dataGrouping as $keyGroup => $valueGroup) {
            $hasil[$keyGroup] = $this->FIFOSERDLOT($valueGroup, $qty);
        }

        logger(json_encode($hasil));

        // OLD

        $changedSubKey = [];
        $dataAfterCheckMainSub = [];
        foreach ($dataFinal as $keyMainSub => $valueMainSub) {
            $cekChanged = array_filter($changedSubKey, function ($f, $k) use ($keyMainSub) {
                return $k === $keyMainSub;
            }, ARRAY_FILTER_USE_BOTH);

            if (count($cekChanged) === 0) {
                if ($this->is_decimal((float)$valueMainSub['PPSN2_QTPER'])) {
                    // logger('checkInsider');
                    $getModel = explode('-', $job);
                    $cek = $this->checkItemSub(trim($valueMainSub['PPSN2_SUBPN']), $getModel[count($getModel) - 1], $job);
                    // logger(json_encode([trim($valueMainSub['PPSN2_SUBPN']), $getModel[count($getModel) - 1], $job]));
                    $totPer = 0;
                    foreach ($cek as $keyUsedSub => $valueUsedSub) {
                        $hasilnya = array_filter($dataFinal, function ($f) use ($valueUsedSub) {
                            return trim($f['PPSN2_SUBPN']) == $valueUsedSub->CMI_ITMSUB;
                        }, ARRAY_FILTER_USE_BOTH);

                        if (count($hasilnya) > 0) {
                            // logger($hasilnya);
                            $cekDataTotQTY = array_filter($dataFinal, function ($f) use ($valueUsedSub) {
                                return $f['PPSN2_SUBPN'] == $valueUsedSub->CMI_ITMNUM && isset($f['TOT_QTY_GET']);
                            });

                            $hasilTotQTY = 0;
                            foreach ($cekDataTotQTY as $keyHasilTot => $valueHasilTot) {
                                $hasilTotQTY += $valueHasilTot['TOT_QTY_GET'];
                            }

                            $sumTot = $hasilTotQTY > 0 ? $hasilTotQTY + $qty : $qty;
                            $checkStock = $valueMainSub['SERD_QTY'] - $sumTot;

                            foreach ($hasilnya as $keySum => $valueSum) {
                                $changedSub[] = $keySum;
                                $dataFinal[$keySum] = array_merge(
                                    $valueMainSub,
                                    [
                                        'PPSN2_QTPER' => $checkStock > 0 ? 0 : $qty,
                                        'REAL_PER' => $hasilnya[$keySum]['PPSN2_QTPER'],
                                        'TOT_QTY_GET' => $checkStock > 0 ? 0 : $qty
                                    ]
                                );
                                $totPer += round($hasilnya[$keySum]['PPSN2_QTPER'], 2);
                            }

                            $dataAfterCheckMainSub[$keyMainSub] = array_merge(
                                $valueMainSub,
                                [
                                    'PPSN2_QTPER' => $checkStock > 0 ? $qty : 0,
                                    // $this->is_decimal(round($valueMainSub['PPSN2_QTPER'], 2) + $totPer) && round($qty - $totPer, 2) > 0 ? round($qty - $totPer, 2) : round($valueMainSub['PPSN2_QTPER'], 2),
                                    'REAL_PER' => $valueMainSub['PPSN2_QTPER'],
                                    'TOT_QTY_GET' => $sumTot
                                ]
                            );
                        } else {
                            $dataAfterCheckMainSub[$keyMainSub] = $valueMainSub;
                        }
                    }
                } else {
                    $dataAfterCheckMainSub[$keyMainSub] = $valueMainSub;
                }
            }
        }

        return $dataAfterCheckMainSub;
    }

    public function FIFOSERDLOT($data, $qty, $hasil = [])
    {
        $now = current($data);
        $total = $now['SERD_QTY'] - $qty;
        if ($total >= 0) {
            $now['PPSN2_QTPER'] = $qty;
            $hasil[] = $now;
        } else {
            $now['PPSN2_QTPER'] = $qty - $now['SERD_QTY'];
            next($data);
            $hasil[] = $this->FIFOSERDLOT($data, $qty - $now['SERD_QTY'], $hasil);
        }

        return $hasil;
    }

    public function cekAngkaKoma($data, $qty, $model, $job)
    {
        $hasil = [];
        foreach ($data as $key => $value) {
            $hasil[trim($value['PPSN2_SUBPN'])][$key] = [
                'qtper' => $value['PPSN2_QTPER'],
                'qty' => $qty,
                'jumlah' => $qty * $value['PPSN2_QTPER'],
                'process' => $value['PPSN1_PROCD'],
                'real_per' => $value['REAL_PER']
            ];
        }

        $cekData = [];
        foreach ($hasil as $keyJum => $valueJum) {
            $sumnya1 = 0;
            foreach (array_values($valueJum) as $keyJum2 => $valueJum2) {
                $sumnya1 += $valueJum2['jumlah'];
            }
            $cekData[] = [
                'ITEM' => $keyJum,
                'TOTAL' => round($sumnya1, 2)
            ];
        }

        $cek = array_filter(array_values($hasil), function ($f) {
            $sumnya = 0;
            foreach (array_values($f) as $key => $value) {
                $sumnya += $value['jumlah'];
            }

            return $this->is_decimal(round($sumnya, 2));
        });

        $proc = [];
        foreach ($cek as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $proc[] = $value2['process'];
            }
        }

        logger(json_encode($cek));

        if (count($cek) > 0) {
            // $seluruhTotal = 0;
            // foreach ($data as $keyHasil2 => $valueHasil2) {
            //     $seluruhTotal += $qty * $valueHasil2['PPSN2_QTPER'];
            // }

            // logger('cek keseluruhan total');
            // logger($seluruhTotal);
            return [
                'status' => false,
                'process' => implode(",", $proc),
                'data' => $cekData
            ];
        } else {
            return [
                'status' => true,
                'process' => implode(",", $proc)
            ];
        }
    }

    public function is_decimal($val)
    {
        return is_numeric($val) && floor($val) != $val;
    }

    public function checkItemSub($item_mtr, $item_model, $job)
    {
        $searchJob = DB::select(DB::raw("SET NOCOUNT ON;EXEC PSI_RPCUST.dbo.sp_check_mapping_item @item_material = ?, @item_model = ?, @job = ?"), [
            $item_mtr,
            $item_model,
            $job
        ]);

        return $searchJob;
    }

    public function materialScrap($item, $id, $qty, $source, $date, $transID, $lot = '', $do = '')
    {
        $data = scrapDetailMaterial::dispatch($item, $id, $qty, $source, $date, $transID, $lot, $do)->onQueue('scrapReportMaterial');

        return [
            'item' => $item,
            'status' => "queued successfully !!",
            'data' => $data
        ];
    }

    public function cekLatestHarga($item, $lot = '', $do = '')
    {
        $cekRcvScnForPriceHead = RCVSCN2::select(
            'RCVSCN_ITMCD',
            'RCVSCN_DONO',
            'RCV_PRPRC',
            'RCVSCN_LUPDT',
            'RCV_BCTYPE',
            'RCVSCN_LOTNO',
            DB::raw('SUM(RCVSCN_QTY) AS QTY')
        )
            ->where('RCVSCN_ITMCD', $item)
            ->join('RCV_TBL', function ($j) {
                $j->on('RCV_ITMCD', 'RCVSCN_ITMCD');
                $j->on('RCV_DONO', 'RCVSCN_DONO');
            })
            ->groupBy(
                'RCVSCN_ITMCD',
                'RCVSCN_DONO',
                'RCV_PRPRC',
                'RCVSCN_LUPDT',
                'RCV_BCTYPE',
                'RCVSCN_LOTNO'
            )
            ->orderBy('RCVSCN_LUPDT', 'DESC');

        if (!empty($lot)) {
            if (empty($do)) {
                $cekRcvScnForPrice = $cekRcvScnForPriceHead->where('RCVSCN_LOTNO', $lot)->first();
            } else {
                $cekRcvScnForPrice = $cekRcvScnForPriceHead->where('RCVSCN_LOTNO', $lot)->where('RCVSCN_DONO', $do)->first();
            }
        } else {
            if (!empty($do)) {
                $cekRcvScnForPrice = $cekRcvScnForPriceHead->where('RCVSCN_DONO', $do)->get();
            }
        }

        return $cekRcvScnForPrice;
    }

    public function checkFIFODO($item, $qty, $do = '')
    {
        $cekExBC = DB::table('PSI_RPCUST.dbo.v_stock_exbc_by_date_aju')
            ->where('RPSTOCK_ITMNUM', $item)
            ->where('EXBC_TOT_QTY', '>', 0)
            ->orderBy('RPSTOCK_BCDATE', 'DESC');

        if (!empty($do)) {
            $cekExBC->where('RPSTOCK_DOC', $do);
        }

        $cekExBC = array_map(function ($value) {
            return (array)$value;
        }, $cekExBC->get()->toArray());

        $data = [];
        if (count($cekExBC) === 0) {
            return $data;
        } else {
            $totqtytemp = $qty;
            foreach ($cekExBC as $key => $value) {
                $totalQty = $totqtytemp - $value['EXBC_TOT_QTY'];
                if ($totalQty > 0) {
                    $data[] = array_merge($value, [
                        'SISA_QTY' => $totalQty,
                        'EXEC_QTY' => $totqtytemp
                    ]);

                    $totqtytemp = $totalQty;
                } elseif ($totalQty === 0) {
                    $data[] = array_merge($value, [
                        'SISA_QTY' => $totalQty,
                        'EXEC_QTY' => $totqtytemp
                    ]);
                    break;
                } else {
                    $data[] = array_merge($value, [
                        'SISA_QTY' => $totalQty,
                        'EXEC_QTY' => $totqtytemp
                    ]);;
                    break;
                }
            }

            return $data;
        }
    }

    public function checkFIFOLot($item, $do, $qty)
    {
        $dataLOT = $this->cekLatestHarga($item, '', $do);
        if (!empty($dataLOT)) {
            $dataLOT = $dataLOT->toArray();

            $data = [];
            if (count($dataLOT) === 0) {
                return $data;
            } else {
                $totqtytemp = $qty;

                // logger(json_encode($dataLOT));
                foreach ($dataLOT as $key => $value) {
                    $totalQty = $totqtytemp - $value['QTY'];
                    if ($totalQty > 0) {
                        $data[] = array_merge($value, [
                            'SISA_QTY' => $totalQty,
                            'EXEC_QTY' => $totqtytemp
                        ]);

                        $totqtytemp = $totalQty;
                    } elseif ($totalQty === 0) {
                        $data[] = array_merge($value, [
                            'SISA_QTY' => $totalQty,
                            'EXEC_QTY' => $totqtytemp
                        ]);
                        break;
                    } else {
                        $data[] = array_merge($value, [
                            'SISA_QTY' => $totalQty,
                            'EXEC_QTY' => $totqtytemp
                        ]);;
                        break;
                    }
                }

                return $data;
            }
        }
    }

    public function insertToITH($item, $date, $form, $doc, $qty, $wh, $ser, $remark, $user)
    {
        ITHMaster::insert([
            'ITH_ITMCD' => $item,
            'ITH_DATE' => $date,
            'ITH_FORM' => $form,
            'ITH_DOC' => $doc,
            'ITH_QTY' => $qty,
            'ITH_WH' => $wh,
            'ITH_LOC' => '',
            'ITH_SER' => $ser,
            'ITH_REMARK' => $remark,
            'ITH_LINE' => DB::raw('dbo.fun_ithline()'),
            'ITH_LUPDT' => $date . ' ' . date('H:i:s'),
            'ITH_USRID' => $user,
        ]);
    }

    public function sendingParamBCStock($item_num, $date_out = '', $lot = null, $qty = 0, $doc = null, $bc = null, $loc = '')
    {
        $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item_num,
            'date_out' => empty($date_out) ? date('Y-m-d') : $date_out,
            'lot' => $lot,
            'qty' => $qty,
            'doc' => $doc,
            'bc' => $bc,
            'loc' => $loc
            // 'scrap' => true,
            // 'remark' => $id,
            // 'revise' => $rev
        ];

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content['CURL'];
    }
}
