<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use App\Model\RPCUST\MutasiMesin;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\Model\MASTER\ItemMaster;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithMappedCells;
use App\Model\RPCUST\processMapMstr;
use App\Model\RPCUST\processMapDet;
use App\Model\RPCUST\processMstr;

class mappingProcessDataImport implements WithMappedCells, ToModel, WithStartRow
{
    public function mapping(): array
    {
        return [
            'process_name'  => 'C2'
        ];
    }

    public function startRow(): int
    {
        return 10;
    }

    /**
     * @param Model $collection
     */
    public function model(array $row)
    {
        logger($row);
        $hasil = processMapMstr::create([
            'RPDSGN_DESC' => $row['process_name']
        ]);

        $process = processMstr::get();

        foreach ($process as $key => $value) {
            processMapDet::updateOrCreate([
                'RPDSGN_PRO_ID' => $value['RPDSGN_CODE'],
                'RPDSGN_ITEMCD' => $row[2],
                'RPDSGN_PROCD' => $row[$key + 5],
                'RPDSGN_MSTR_ID' => isset($hasil->id) ? $hasil->id : $hasil
            ], [
                'RPDSGN_PRO_ID' => $value['RPDSGN_CODE'],
                'RPDSGN_ITEMCD' => $row[2],
                'RPDSGN_PROCD' => $row[$key + 5],
                'RPDSGN_MSTR_ID' => isset($hasil->id) ? $hasil->id : $hasil,
                'RPDSGN_CODE' => $row[3]
            ]);
        }
    }
}
