<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\Model\RPCUST\processMapDet;
use Illuminate\Support\Facades\DB;
use App\Model\MASTER\MEGAEMS\pis2Table;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

use App\Model\RPCUST\scrapHist;
use App\Model\RPCUST\processMstr;
use App\Model\WMS\API\SERDTBL;

use App\Jobs\scrapDetailInserts;
use App\Jobs\scrapDetailInsertsMaterial;

use App\Model\MASTER\ItemMaster;
use App\Model\RPCUST\scrapHistDet;
use App\Model\WMS\API\RCVSCN2;
use App\Model\WMS\API\RETFG;
use App\Model\RPCUST\StockExBC;
use App\Model\WMS\API\RCVSCN;

use App\Traits\traitInsertScrapDetailFromJobs;

class uploadScrapHistMaster implements ToCollection, SkipsEmptyRows
{
    use traitInsertScrapDetailFromJobs;

    public $dataRet, $users, $dept, $type, $idTrans;
    function __construct($users, $dept, $idTrans)
    {
        $this->users = $users;
        $this->dept = $dept;
        $this->process = processMstr::get()->toArray();
        $this->idTrans = $idTrans;
    }

    public function collection(Collection $row)
    {
        foreach ($row as $key => $value) {
            if ($value[0] === 'MFG Repair Center') {
                if (!empty($value[count($value) - 2])) {
                    $this->type = $value[count($value) - 2];
                } else {
                    if (!empty($value[count($value) - 8])) {
                        $this->type = $value[count($value) - 8];
                    } else {
                        $this->type = $value[count($value) - 9];
                    }
                }
            }

            if ($key > 4 && count($value) > 3) {
                // Jika Item tidak kosong
                if (!empty($value[1]) && !empty($value[2])) {
                    $this->dataRet[] = $this->checkingType($this->type, $value);
                } else {
                    $this->dataRet[] = [
                        'status' => false,
                        'message' => 'This row item in Row: ' . $value[0] . ' and key: '. $key .' is empty !',
                        'data' => []
                    ];
                }
            }
        }
    }

    public function checkingType($type, $data)
    {
        // logger('----start----');
        // logger($data);
        // logger('----end----');
        switch ($type) {
            case 'WIP':
                $cekMapping = $this->checkMapping((string)$data[1], (string)$data[2]);

                // Jika mapping item di temukan
                if (count($cekMapping) > 0) {
                    return $this->checkDataExcel($data, $cekMapping);
                } else {
                    return [
                        'status' => false,
                        'message' => 'This item ' . $data[1] . ' in Row: ' . $data[0] . '. mapping is not found !',
                        'data' => []
                    ];
                }
                break;
            case 'PENDING':
                $cekMapping = $this->checkMapping((string)$data[1], (string)$data[2]);

                // Jika mapping item di temukan
                if (count($cekMapping) > 0) {
                    return $this->checkDataExcel($data, $cekMapping, true);
                } else {
                    return $data[count($data) - 3];
                    if (!empty($data[count($data) - 3])) {
                        return $this->checkDataMaterial($data);
                    }
                    return [
                        'status' => false,
                        'message' => 'This item ' . $data[1] . ' in Row: ' . $data[0] . '. mapping is not found !',
                        'data' => []
                    ];
                }
                break;
            case 'MATERIAL':
                return $this->checkDataMaterial($data);
                break;
            case 'FGRETURN':
                return $this->checkDataFGReturn($data);
                break;
            default:
                return [
                    'status' => false,
                    'message' => 'No template type found !',
                    'data' => []
                ];
                break;
        }
    }

    // This is for WIP & Pending Scrap
    public function checkDataExcel($data, $mapping, $isPending = false)
    {
        try {
            $cektotalqty = 0;
            $startProcess = 6;
            // logger($data);
            foreach ($data as $keyQty => $valueQty) {
                if ($keyQty > 6 && $keyQty < count($data) - 2) {
                    $cektotalqty += $valueQty;
                }
            }

            // logger($cektotalqty);

            $parsedJob = count(explode('-', $data[2])) > 2 ? trim($data[2]) : trim($data[2]) . '-' . trim((string)$data[1]);
            $cekJob = $this->checkJob((string)$data[1], $parsedJob);
            $processID = [];
            $qty = [];

            $keysnya = 0;
            foreach ($this->process as $key => $value) {
                // Jika kolom process tidak kosong
                if (!empty($data[$startProcess + $key])) {
                    $processID[$keysnya] = $value['RPDSGN_CODE'];
                    $qty[$keysnya] = $data[$startProcess + $key];

                    $keysnya++;
                }
            }

            // logger(json_encode($processID));

            if (count($processID) > 0) {
                if (count($cekJob) > 0) {
                    $cekChoosedMapping = array_values(array_filter($mapping, function ($f) use ($processID) {
                        return in_array($f['RPDSGN_PRO_ID'], $processID);
                    }));

                    $insertnya = [];
                    foreach ($cekChoosedMapping as $key => $value) {
                        if (!empty($qty[$key])) {
                            $paramInsertHeader = [
                                'DSGN_MAP_ID' => $value['id'],
                                'USERNAME' => $this->users,
                                'DEPT' =>  $this->dept,
                                'QTY' => $qty[$key],
                                'QTY_SCRAP' => $isPending,
                                'ID_TRANS' => $this->idTrans,
                                'TYPE_TRANS' => 'NORMAL',
                                'DOC_NO' => $parsedJob,
                                'REASON' => $data[4],
                                'SCR_PROCD' => $value['RPDSGN_PROCD'],
                                'MDL_FLAG' => 1,
                                'ITEMNUM' => trim($data[1]),
                                'DATE_OUT' => empty($data[5]) || $data[5] === '1970-01-01' ? date('Y-m-d') : date('Y-m-d', strtotime($data[5])),
                            ];

                            // logger(json_encode($paramInsertHeader));

                            $insertHeader = $this->insertHist($paramInsertHeader);

                            // NEW

                            $jobsSubmit = $this->submitToJobs(
                                $parsedJob,
                                $value['RPDSGN_PRO_ID'],
                                trim((string)$data[1]),
                                $qty[$key],
                                $insertHeader->id,
                                $this->idTrans,
                                $data[5]
                            );

                            $insertnya[] = $jobsSubmit;

                            if ($jobsSubmit['status'] !== true) {
                                return [
                                    'status' => false,
                                    'message' => 'Row: ' . $data[0] . ' with item: ' . $data[1] . ' ' . $jobsSubmit['message'],
                                    'data' => $data,
                                    'test' => $this->type,
                                    'id' => $this->idTrans,
                                    'detail' => []
                                ];
                            }

                            // OLD

                            // $dataPSNList = $this->getPsnListByJobAndProcd(
                            //     $this->process,
                            //     $parsedJob,
                            //     $value['RPDSGN_PRO_ID'],
                            //     trim((string)$data[1])
                            // );

                            // // logger(json_encode($dataPSNList));

                            // if ($dataPSNList['status'] == true) {
                            //     $getAngkaKoma = array_filter($dataPSNList['data'], function ($f) {
                            //         return $f['PPSN2_QTPER'] < 1;
                            //     });

                            //     $cekQtyKoma = $this->cekAngkaKoma($getAngkaKoma, $qty[$key]);

                            //     if ($cekQtyKoma['status'] === true) {
                            //         $findByProcess = $this->checkReverseProcess($dataPSNList['data']);

                            //         if (count($findByProcess) > 0) {
                            //             $cekItem = ItemMaster::where('MITM_ITMCD', trim((string)$data[1]))->first()->toArray();

                            //             $insertnya[] = $dataPSNList;

                            //             scrapDetailInserts::dispatch(
                            //                 $findByProcess,
                            //                 $qty[$key],
                            //                 $insertHeader->id,
                            //                 $cekItem,
                            //                 $this->idTrans,
                            //                 $parsedJob,
                            //                 0,
                            //                 date('Y-m-d', strtotime($data[5])),
                            //                 true
                            //             )->onQueue('scrapReport');
                            //         } else {
                            //             scrapHist::where('id', $insertHeader->id)->update(['failed_reason' => 'Data calculation not found in Finish Good !!']);

                            //             scrapHist::where('id', $insertHeader->id)->delete();
                            //         }
                            //     } else {
                            //         return [
                            //             'status' => false,
                            //             'message' => 'Row: ' . $data[0] . ' with item: ' . $data[1] . ' ' . $dataPSNList['desc'] . '. will be decimal qty, please check it.',
                            //             'data' => $data,
                            //             'test' => $this->type,
                            //             'id' => $this->idTrans,
                            //             'detail' => []
                            //         ];
                            //     }
                            // } else {
                            //     scrapHist::where('id', $insertHeader->id)->update(['failed_reason' => $dataPSNList['desc']]);

                            //     scrapHist::where('id', $insertHeader->id)->delete();

                            //     return [
                            //         'status' => false,
                            //         'message' => 'Row: ' . $data[0] . ' with item: ' . $data[1] . ' ' . $dataPSNList['desc'] . '. Please check your mapping',
                            //         'data' => $data,
                            //         'test' => $this->type,
                            //         'id' => $this->idTrans,
                            //         'detail' => []
                            //     ];
                            // }
                        }
                    }

                    if (count($insertnya) > 0) {
                        return [
                            'status' => true,
                            'message' => 'Row: ' . $data[0] . ' with item: ' . $data[1] . ' validation is pass, now Queue inserted',
                            'data' => $data,
                            'test' => $this->type,
                            'id' => $this->idTrans,
                            'detail' => $insertnya
                        ];
                    } else {
                        return [
                            'status' => false,
                            'message' => 'Row: ' . $data[0] . ' with item: ' . $data[1] . ' data not inserted !',
                            'data' => $data,
                            'test' => $this->type,
                            'id' => $this->idTrans,
                            'detail' => $insertnya
                        ];
                    }
                } else {
                    return [
                        'status' => false,
                        'message' => 'Job ' . $parsedJob . ', Item ' . $data[1] . ' in Row: ' . $data[0] . ' Stock Not Found !',
                        'data' => $data,
                        'test' => $this->type,
                        'id' => $this->idTrans,
                        'detail' => []
                    ];
                }
            }
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'message' => 'Row: ' . $data[0] . ' with item: ' . $data[1] . ' ' . $th . '. Error server',
                'data' => $data,
                'test' => $this->type,
                'id' => $this->idTrans,
                'detail' => []
            ];
        }
    }

    public function checkDataMaterial($data, $source = 'WH')
    {
        $dataParsed = [];
        foreach ($data as $key => $value) {
            if ($key < 6) {
                $dataParsed[] = $value;
            }
        }

        // Insert ke table Hist Master
        $insertingScrap = $this->insertHist([
            'DSGN_MAP_ID' => $dataParsed[2] . '-' . $dataParsed[3],
            'USERNAME' =>  $this->users,
            'DEPT' =>  $this->dept,
            'QTY' => intVal($dataParsed[count($dataParsed) - 3]),
            'QTY_SCRAP' => 0,
            'ID_TRANS' => $this->idTrans,
            'TYPE_TRANS' => 'NORMAL',
            'DOC_NO' => $dataParsed[2],
            'REASON' => $dataParsed[4],
            'SCR_PROCD' => $dataParsed[3],
            'MDL_FLAG' => 0,
            'ITEMNUM' => $dataParsed[1],
            'DATE_OUT' => empty($dataParsed[5]) || $dataParsed[5] === '1970-01-01' ? date('Y-m-d') : $dataParsed[5]
        ]);

        // return $this->materialScrap($dataParsed[1], $insertingScrap->id, intVal($dataParsed[6]), $source, $dataParsed[2]);

        // $cekRcvScnForPrice = $this->cekLatestHarga((string)$dataParsed[1], (string)$dataParsed[3], (string)$dataParsed[2]);

        // $insertDet = scrapHistDet::create([
        //     'SCR_HIST_ID' => $insertingScrap->id,
        //     'SCR_PSN' => null,
        //     'SCR_CAT' => null,
        //     'SCR_LINE' => null,
        //     'SCR_FR' => null,
        //     'SCR_MC' => null,
        //     'SCR_MCZ' => null,
        //     'SCR_ITMCD' => $dataParsed[1],
        //     'SCR_QTPER' => 1,
        //     'SCR_QTY' => intVal($dataParsed[5]),
        //     'SCR_LOTNO' => $dataParsed[3],
        //     'SCR_ITMPRC' => empty($cekRcvScnForPrice) ? 0 : $cekRcvScnForPrice['RCV_PRPRC']
        // ]);

        // return [
        //     'status' => true,
        //     'message' => 'Row: ' . $data[0] . ' validation is pass, data material inserted',
        //     'data' => $insertDet,
        //     'test' => $this->type
        // ];
    }

    public function checkDataFGReturn($data)
    {
        $dataParsed = [];
        foreach ($data as $key => $value) {
            if ($key < 6) {
                $dataParsed[] = $value;
            }
        }

        // Insert ke table Hist Master
        $insertingScrap = $this->insertHist([
            'DSGN_MAP_ID' => $dataParsed[2],
            'USERNAME' =>  $this->users,
            'DEPT' =>  $this->dept,
            'QTY' => intVal($dataParsed[5]),
            'QTY_SCRAP' => 0,
            'ID_TRANS' => $this->idTrans,
            'TYPE_TRANS' => 'NORMAL',
            'DOC_NO' => $dataParsed[2],
            'REASON' => $dataParsed[3],
            'SCR_PROCD' => '',
            'MDL_FLAG' => 1,
            'ITEMNUM' => $dataParsed[1],
            'IS_RETURN' => 1,
            'DATE_OUT' => empty($dataParsed[4]) || $dataParsed[4] === '1970-01-01' ? date('Y-m-d') : $dataParsed[4],
        ]);

        $dataRet = $this->getPartFGReturn($dataParsed[1], $dataParsed[2]);

        foreach ($dataRet as $keyComp => $valueComp) {
            $hasilComp = [
                'RCVSCN_ITMCD' => $valueComp['SERRC_BOMPN'],
                'RCVSCN_LOTNO' => $valueComp['SERRC_LOTNO'],
                'RCVSCN_DONO' => $valueComp['RETFG_DOCNO'],
                'QTY_SCRAP' => $valueComp['SERRC_SERXQTY'] * intVal($dataParsed[4]),
                'RCV_PRPRC' => $this->getLatestHargaFromBC($valueComp['SERRC_BOMPN'], $valueComp['SERRC_LOTNO'])['RCV_PRPRC']
            ];

            $cekItem = ItemMaster::where('MITM_ITMCD', trim((string)$data[1]))->first()->toArray();
            $queueInsertDet = (new scrapDetailInsertsMaterial(
                $hasilComp,
                $this->idTrans,
                $this->users,
                $this->dept,
                'NORMAL',
                '',
                $cekItem,
                $insertingScrap->id
            ));
            dispatch($queueInsertDet)->onQueue('scrapReport');
        }

        return [
            'status' => true,
            'message' => 'Row: ' . $data[0] . ' validation is pass, data return queued',
            'data' => $dataRet,
            'test' => $this->type
        ];
    }

    public function getPartFGReturn($item, $doc)
    {
        $getRet = RETFG::select(
            'RETFG_DOCNO',
            'RETFG_ITMCD',
            'SERRC_BOMPN',
            'SERRC_BOMPNQTY',
            'SERRC_SER',
            'SERRC_SERX',
            'SERRC_SERXQTY',
            'SERRC_LOTNO'
        )
            ->join('SER_TBL', function ($f) {
                $f->on('SER_DOC', 'RETFG_DOCNO');
                $f->on('SER_ITMID', 'RETFG_ITMCD');
                $f->on('SER_PRDLINE', 'RETFG_LINE');
            })
            ->join('SERRC_TBL', function ($f) {
                $f->on('SERRC_SER', 'SER_ID');
            })
            ->where('RETFG_ITMCD', trim($item))
            ->where('RETFG_DOCNO', trim($doc))
            ->where('SERRC_BOMPN', '<>', '')
            ->where('SERRC_BOMPNQTY', '<>', '')
            ->where('SERRC_LOTNO', '<>', '')
            ->groupBy(
                'RETFG_DOCNO',
                'RETFG_ITMCD',
                'SERRC_BOMPN',
                'SERRC_BOMPNQTY',
                'SERRC_SER',
                'SERRC_SERX',
                'SERRC_SERXQTY',
                'SERRC_LOTNO'
            )
            ->get()
            ->toArray();

        return $getRet;
    }
    // ETC

    public function insertHist($param)
    {
        $insertHeader = scrapHist::create($param);

        return $insertHeader;
    }

    public function checkMapping($item, $job = '')
    {
        $cekMapping = processMapDet::where('RPDSGN_ITEMCD', $item)->whereNotNull('RPDSGN_MSTR_ID')->where('RPDSGN_PROCD', '<>', '')->get()->toArray();

        if (count($cekMapping) === 0) {
            $whereJob = empty($job) ? NULL : "AND PDPP_WONO = '" . $job . "'";
            $processJobs = DB::select("
                    select
                        CONCAT(LTRIM(RTRIM(wo.PDPP_WONO)), '-', bo.MBO2_SEQNO) AS id,
                        bo.MBO2_MDLCD AS RPDSGN_ITEMCD,
                        LTRIM(RTRIM(bo.MBO2_PROCD)) AS RPDSGN_PROCD,
                        CASE WHEN bo.MBO2_PROCD = 'SMT-HW' OR bo.MBO2_PROCD = 'SMT-HWAD' OR bo.MBO2_PROCD = 'SMT-SP'
                            THEN LTRIM(RTRIM(bo.MBO2_PROCD))
                            ELSE LTRIM(RTRIM(CONCAT('#', bo.MBO2_SEQNO)))
                        END AS RPDSGN_PRO_ID,
                        CASE WHEN (
                            SELECT TOP 1 dt.DLV_CONSIGN FROM PSI_WMS.dbo.DLV_TBL dt
                            INNER JOIN PSI_WMS.dbo.SERD2_TBL st ON st.SERD2_SER = dt.DLV_SER
                            WHERE st.SERD2_JOB = wo.PDPP_WONO
                            AND dt.DLV_CONSIGN IS NOT NULL
                        ) = 'IEI'
                            THEN 'MFG1'
                            ELSE 'MFG2'
                        END
                        as TEST,
                        bo.MBO2_BOMRV,
                        wo.PDPP_WONO,
                        wo.PDPP_BOMRV
                    from XMBO2 bo
                    INNER JOIN
                        XWO wo ON wo.PDPP_MDLCD = bo.MBO2_MDLCD
                        AND wo.PDPP_BOMRV = bo.MBO2_BOMRV
                    WHERE MBO2_MDLCD = '" . $item . "'
                    $whereJob
                    ORDER BY bo.MBO2_SEQNO
                ");

            $result = array_map(function ($value) {
                return (array)$value;
            }, $processJobs);

            return $result;
        }

        return $cekMapping;
    }

    public function checkJob($item, $job)
    {
        $searchJob = DB::select(DB::raw("exec sr_sp_checkJobTotalScrapQty @model = ?, @jobSearch = ?"), [
            $item,
            $job
        ]);

        return $searchJob;
    }

    // public function checkReverseProcess($data)
    // {
    //     $parsed = [];

    //     // logger(json_encode($data));

    //     $searchPCBPer = array_values(array_filter($data, function ($f) {
    //         $itemD1 = trim($f['MITM_ITMD1']);
    //         return
    //             trim($f['PPSN1_PROCD']) === 'SMT-AB'
    //             && (
    //                 strpos($itemD1, 'PRINT CIRCUIT BOARD') !== false ||
    //                 strpos($itemD1, 'PCB') !== false ||
    //                 strpos($itemD1, 'P.C.B') !== false ||
    //                 strpos($itemD1, 'P,C,B') !== false);
    //     }));

    //     // logger($searchPCBPer);

    //     foreach ($data as $key => $value) {
    //         if (!empty($value['PPSN1_PROCD'])) {
    //             if (trim($value['PPSN1_PROCD']) === 'SMT-AB') {
    //                 // Jika komponen bukan PCB maka di bagi 2
    //                 if (
    //                     (
    //                         strpos($value['MITM_ITMD1'], 'PRINT CIRCUIT BOARD') !== false ||
    //                         strpos($value['MITM_ITMD1'], 'PCB') === false ||
    //                         strpos($value['MITM_ITMD1'], 'P.C.B') === false ||
    //                         strpos($value['MITM_ITMD1'], 'P,C,B') === false)
    //                 ) {
    //                     $getPCBPer = $searchPCBPer[0]['PPSN2_QTPER'];

    //                     $parsed[] = [
    //                         "MITM_ITMD1" => $value['MITM_ITMD1'],
    //                         "MITM_ITMD2" => $value['MITM_ITMD2'],
    //                         "PPSN1_LINENO" => $value['PPSN1_LINENO'],
    //                         "PPSN1_PSNNO" => $value['PPSN1_PSNNO'],
    //                         "PPSN2_QTPER" => (0.5 / $getPCBPer) * $value['PPSN2_QTPER'],
    //                         "PPSN2_SUBPN" => $value['PPSN2_SUBPN'],
    //                         "PPSN1_PROCD" => $value['PPSN1_PROCD'],
    //                         "SERD_JOB" => $value['SERD_JOB'],
    //                         "RPDSGN_CODE" => $value['RPDSGN_CODE']
    //                     ];
    //                 } else {
    //                     $parsed[] = $value;
    //                 }
    //             } else {
    //                 $parsed[] = $value;
    //             }
    //         }
    //     }

    //     return $parsed;
    // }

    // public function getPsnListByJobAndProcd($listProcess, $job, $idProcess, $item, $hasil = []) // Recursive here
    // {
    //     $cekProcessNow = current($listProcess);
    //     // logger($cekProcessNow);
    //     if (!empty($cekProcessNow)) {
    //         $getDataMapping = processMapDet::select(
    //             'RPDSGN_PRO_ID',
    //             DB::raw("CASE WHEN LEFT(RPDSGN_PROCD, 3) = 'SMT'
    //                     THEN RPDSGN_PROCD
    //                     ELSE RPDSGN_PRO_ID
    //                     END AS RPDSGN_PROCD
    //                 ")
    //         )
    //             ->where('RPDSGN_ITEMCD', $item)
    //             ->where('RPDSGN_PRO_ID', $cekProcessNow['RPDSGN_CODE'])
    //             ->orderBy('RPDSGN_PRO_ID')
    //             ->whereNotNull('RPDSGN_MSTR_ID')
    //             ->whereRaw("RPDSGN_PROCD <> ''")
    //             ->groupBy(
    //                 'RPDSGN_PRO_ID',
    //                 DB::raw("CASE WHEN LEFT(RPDSGN_PROCD, 3) = 'SMT'
    //                     THEN RPDSGN_PROCD
    //                     ELSE RPDSGN_PRO_ID
    //                     END
    //                 ")
    //             )
    //             ->get()
    //             ->toArray();

    //         if (count($getDataMapping) === 0) {
    //             $whereJob = empty($job) ? NULL : "AND PDPP_WONO = '" . $job . "'";

    //             $getSeq = strpos($cekProcessNow['RPDSGN_CODE'], '#') ? "AND MBO2_SEQNO = ". str_replace("#", "", $cekProcessNow['RPDSGN_CODE']) : "AND MBO2_PROCD = '".$cekProcessNow['RPDSGN_CODE']."'";
    //             $processJobs = DB::select("
    //                         select
    //                             CONCAT(LTRIM(RTRIM(wo.PDPP_WONO)), '-', bo.MBO2_SEQNO) AS id,
    //                             bo.MBO2_MDLCD AS RPDSGN_ITEMCD,
    //                             LTRIM(RTRIM(bo.MBO2_PROCD)) AS RPDSGN_PROCD,
    //                             CASE WHEN bo.MBO2_PROCD = 'SMT-HW' OR bo.MBO2_PROCD = 'SMT-HWAD' OR bo.MBO2_PROCD = 'SMT-SP'
    //                                 THEN LTRIM(RTRIM(bo.MBO2_PROCD))
    //                                 ELSE LTRIM(RTRIM(CONCAT('#', bo.MBO2_SEQNO)))
    //                             END AS RPDSGN_PRO_ID,
    //                             CASE WHEN (
    //                                 SELECT TOP 1 dt.DLV_CONSIGN FROM PSI_WMS.dbo.DLV_TBL dt
    //                                 INNER JOIN PSI_WMS.dbo.SERD2_TBL st ON st.SERD2_SER = dt.DLV_SER
    //                                 WHERE st.SERD2_JOB = wo.PDPP_WONO
    //                                 AND dt.DLV_CONSIGN IS NOT NULL
    //                             ) = 'IEI'
    //                                 THEN 'MFG1'
    //                                 ELSE 'MFG2'
    //                             END
    //                             as TEST,
    //                             bo.MBO2_BOMRV,
    //                             wo.PDPP_WONO,
    //                             wo.PDPP_BOMRV
    //                         from XMBO2 bo
    //                         INNER JOIN
    //                             XWO wo ON wo.PDPP_MDLCD = bo.MBO2_MDLCD
    //                             AND wo.PDPP_BOMRV = bo.MBO2_BOMRV
    //                         WHERE MBO2_MDLCD = '" . $item . "'
    //                         $getSeq
    //                         $whereJob
    //                         ORDER BY bo.MBO2_SEQNO
    //                     ");

    //             $result = array_map(function ($value) {
    //                 return (array)$value;
    //             }, $processJobs);

    //             $getDataMapping = $result;
    //         }

    //         $dataProcd = [];
    //         foreach ($this->defineMappingScrapArray($getDataMapping) as $keyFail => $valueFail) {
    //             $dataProcd[] = $valueFail['RPDSGN_PROCD'];
    //         }

    //         // logger($getDataMapping);
    //         if ($idProcess == $cekProcessNow['RPDSGN_CODE']) {
    //             // logger($this->defineMappingScrapArray($getDataMapping));
    //             $hasil = [
    //                 'status' => true,
    //                 'desc' => implode(", ", $dataProcd) . ' Found',
    //                 'list_process' => $dataProcd,
    //                 'data' => array_merge(
    //                     $hasil,
    //                     $this->psnListByJobSerD($job, $cekProcessNow['RPDSGN_CODE'], $this->defineMappingScrapArray($getDataMapping))
    //                 )
    //             ];

    //             // logger('ini sama dengan');

    //             // logger($this->psnListByJobSerD($job, $this->defineMappingScrapArray($getDataMapping)));
    //             // logger($this->defineMappingScrapArray($getDataMapping));
    //             // logger(json_encode($hasil));
    //             return $hasil;
    //         } else {
    //             if (count($getDataMapping) > 0) {
    //                 if (count($this->psnListByJobSerD($job, $cekProcessNow['RPDSGN_CODE'], $this->defineMappingScrapArray($getDataMapping))) > 0) {
    //                     $hasil = array_merge(
    //                         $hasil,
    //                         $this->psnListByJobSerD($job, $cekProcessNow['RPDSGN_CODE'], $this->defineMappingScrapArray($getDataMapping))
    //                     );

    //                     // logger('ini tidak sama dengan');

    //                     // logger($this->psnListByJobSerD($job, $this->defineMappingScrapArray($getDataMapping)));
    //                     // logger($this->defineMappingScrapArray($getDataMapping));
    //                     // logger(json_encode($hasil));
    //                     next($listProcess);
    //                     return $this->getPsnListByJobAndProcd($listProcess, $job, $idProcess, $item, $hasil);
    //                 } else {
    //                     $hasil = array_merge(
    //                         $hasil,
    //                         ['RPDSGN_CODE' => $cekProcessNow['RPDSGN_CODE']],
    //                         [
    //                             'status' => false,
    //                             'list_process' => $dataProcd,
    //                             'desc' => implode(", ", $dataProcd) . ' Not found in calculation finish good !!'
    //                         ]
    //                     );

    //                     return $hasil;
    //                 }
    //             } else {
    //                 next($listProcess);
    //                 return $this->getPsnListByJobAndProcd($listProcess, $job, $idProcess, $item, $hasil);
    //             }
    //         }
    //     } else {
    //         return $hasil;
    //     }
    // }

    // public function psnListByJobSerD($job, $code, $procd = [])
    // {
    //     // logger($procd);
    //     $getData = SERDTBL::select(
    //         DB::raw('MAX(SERD_PSNNO) AS PPSN1_PSNNO'),
    //         DB::raw('SERD_LINENO AS PPSN1_LINENO'),
    //         DB::raw('SERD_ITMCD AS PPSN2_SUBPN'),
    //         DB::raw('SERD_PROCD AS PPSN1_PROCD'),
    //         DB::raw("SUM(st.SERD_QTY) / (
    //             SELECT MAX(st2.PPSN1_SIMQT) FROM XPPSN1 st2
    //             WHERE st2.PPSN1_WONO = st.SERD_JOB
    //             -- AND st2.PPSN1_PSNNO = st.SERD_PSNNO
    //             AND st2.PPSN1_LINENO = st.SERD_LINENO
    //         ) AS PPSN2_QTPER"),
    //         'SERD_JOB',
    //         'MITM_ITMD1',
    //         'MITM_ITMD2'
    //     )->from('SERD_TBL as st')
    //         ->where('SERD_JOB', $job)
    //         ->join('MITM_TBL', 'SERD_ITMCD', 'MITM_ITMCD')
    //         ->groupBy(
    //             'SERD_ITMCD',
    //             // 'SERD_PSNNO',
    //             'SERD_LINENO',
    //             'SERD_PROCD',
    //             // 'SERD_QTPER',
    //             'SERD_QTYREQ',
    //             'MITM_ITMD1',
    //             'MITM_ITMD2',
    //             'SERD_JOB'
    //         )
    //         ->orderBy('SERD_ITMCD');

    //     if (!empty($procd) || count($procd) > 0) {
    //         $getProCD = [];
    //         foreach ($procd as $key => $value) {
    //             $getProCD[] = $value['RPDSGN_PROCD'];
    //         }
    //         // logger($getData->where('SERD_PROCD', $procd)->toSql());
    //         $getData->whereIn('SERD_PROCD', $getProCD)->get();
    //     }

    //     // logger($getData->get()->toArray());

    //     $dataFinal = [];
    //     foreach ($getData->get()->toArray() as $key => $value) {
    //         $dataFinal[] = array_merge($value, ['RPDSGN_CODE' => $code]);
    //     }

    //     return $dataFinal;
    // }

    // public function cekAngkaKoma($data, $qty)
    // {
    //     $hasil = [];
    //     foreach ($data as $key => $value) {
    //         $hasil[trim($value['PPSN2_SUBPN'])][$key] = [
    //             'qtper' => $value['PPSN2_QTPER'],
    //             'qty' => $qty,
    //             'jumlah' => $qty * $value['PPSN2_QTPER'],
    //             'process' => $value['PPSN1_PROCD']
    //         ];
    //     }

    //     // logger(json_encode($hasil));

    //     $cekData = [];
    //     foreach ($hasil as $keyJum => $valueJum) {
    //         $sumnya1 = 0;
    //         foreach (array_values($valueJum) as $keyJum2 => $valueJum2) {
    //             $sumnya1 += $valueJum2['jumlah'];
    //         }
    //         $cekData[] = [
    //             'ITEM' => $keyJum,
    //             'TOTAL' => round($sumnya1, 2)
    //         ];
    //     }

    //     $cek = array_filter(array_values($hasil), function ($f) {
    //         $sumnya = 0;
    //         foreach (array_values($f) as $key => $value) {
    //             $sumnya += $value['jumlah'];
    //         }

    //         return $this->is_decimal(round($sumnya, 2));
    //     });

    //     $proc = [];
    //     foreach ($cek as $key => $value) {
    //         foreach ($value as $key2 => $value2) {
    //             $proc[] = $value2['process'];
    //         }
    //     }

    //     if (count($cek) > 0) {
    //         return [
    //             'status' => false,
    //             'process' => implode(",", $proc),
    //             'data' => $cekData
    //         ];
    //     } else {
    //         return [
    //             'status' => true,
    //             'process' => implode(",", $proc)
    //         ];
    //     }
    // }

    // public function is_decimal($val)
    // {
    //     return is_numeric($val) && floor($val) != $val;
    // }

    // public function defineMappingScrap($data)
    // {
    //     if ($data['RPDSGN_PRO_ID'] == 'SMT-HW' && !empty($data['RPDSGN_PROCD'])) {
    //         return 'SMT-HW';
    //     } elseif ($data['RPDSGN_PRO_ID'] == 'SMT-HWADD' && !empty($data['RPDSGN_PROCD'])) {
    //         return 'SMT-HWAD';
    //     } elseif ($data['RPDSGN_PRO_ID'] == 'SMT-SP' && !empty($data['RPDSGN_PROCD'])) {
    //         return 'SMT-SP';
    //     } else {
    //         if ($data['RPDSGN_PROCD'] == 'SMT-AV') {
    //             return 'SMT-AX';
    //         } else {
    //             return $data['RPDSGN_PROCD'];
    //         }
    //     }
    // }

    // public function defineMappingScrapArray($data)
    // {
    //     $datanya = [];
    //     foreach ($data as $key => $f) {
    //         if ($f['RPDSGN_PRO_ID'] == 'SMT-HW' && !empty($f['RPDSGN_PROCD'])) {
    //             $datanya[] = ['RPDSGN_PROCD' => 'SMT-HW'];
    //         } elseif ($f['RPDSGN_PRO_ID'] == 'SMT-HWADD' && !empty($f['RPDSGN_PROCD'])) {
    //             $datanya[] = ['RPDSGN_PROCD' => 'SMT-HWAD'];
    //         } elseif ($f['RPDSGN_PRO_ID'] == 'SMT-SP' && !empty($f['RPDSGN_PROCD'])) {
    //             $datanya[] = ['RPDSGN_PROCD' => 'SMT-SP'];
    //         } else {
    //             if ($f['RPDSGN_PROCD'] == 'SMT-AV') {
    //                 $datanya[] = ['RPDSGN_PROCD' => 'SMT-AX'];
    //             } else {
    //                 $datanya[] = ['RPDSGN_PROCD' => $f['RPDSGN_PROCD']];
    //             }
    //         }
    //     }
    //     // $datanya = array_values(array_filter($data, function ($f) {
    //     //     if ($f['RPDSGN_PRO_ID'] == 'SMT-HW' && !empty($f['RPDSGN_PROCD'])) {
    //     //         return ['RPDSGN_PROCD' => 'SMT-HW'];
    //     //     } elseif ($f['RPDSGN_PRO_ID'] == 'SMT-HWADD' && !empty($f['RPDSGN_PROCD'])) {
    //     //         return ['RPDSGN_PROCD' => 'SMT-HWAD'];
    //     //     } elseif ($f['RPDSGN_PRO_ID'] == 'SMT-SP' && !empty($f['RPDSGN_PROCD'])) {
    //     //         return ['RPDSGN_PROCD' => 'SMT-SP'];
    //     //     } else {
    //     //         if ($f['RPDSGN_PROCD'] == 'SMT-AV') {
    //     //             return ['RPDSGN_PROCD' => 'SMT-AX'];
    //     //         } else {
    //     //             return ['RPDSGN_PROCD' => $f['RPDSGN_PROCD']];
    //     //         }
    //     //     }
    //     // }));

    //     // logger($datanya);

    //     return $datanya;
    // }

    // public function cekLatestHarga($item, $lot, $do = '')
    // {
    //     $cekRcvScnForPriceHead = RCVSCN2::select(
    //         'RCVSCN_ITMCD',
    //         'RCVSCN_DONO',
    //         'RCV_PRPRC',
    //         'RCVSCN_LUPDT',
    //         'RCV_BCTYPE'
    //     )
    //         ->where('RCVSCN_ITMCD', $item)
    //         ->where('RCVSCN_LOTNO', $lot)
    //         ->join('RCV_TBL', function ($j) {
    //             $j->on('RCV_ITMCD', 'RCVSCN_ITMCD');
    //             $j->on('RCV_DONO', 'RCVSCN_DONO');
    //         })
    //         ->groupBy(
    //             'RCVSCN_ITMCD',
    //             'RCVSCN_DONO',
    //             'RCV_PRPRC',
    //             'RCVSCN_LUPDT',
    //             'RCV_BCTYPE'
    //         )
    //         ->orderBy('RCVSCN_LUPDT', 'DESC');

    //     if (empty($do)) {
    //         $cekRcvScnForPrice = $cekRcvScnForPriceHead->first();
    //     } else {
    //         $cekRcvScnForPrice = $cekRcvScnForPriceHead->where('RCVSCN_DONO', $do)->first();
    //     }

    //     return $cekRcvScnForPrice;
    // }

    public function getLatestHargaFromBC($item, $lot = '')
    {
        $stocknya = StockExBC::where('RPSTOCK_ITMNUM', $item)
            ->orderBy('RPSTOCK_BCDATE', 'DESC')
            ->where('TOTAL_INC', '>', 0)
            ->first();

        // return $stocknya;
        $getRctScn = $this->checkRCVSCN($item, $stocknya, $lot);
        return isset($getRctScn) ? $getRctScn : $this->checkRCVSCN($item, $stocknya);
    }

    public function checkRCVSCN($item, $stocknya, $lot = '')
    {
        $harga = RCVSCN::where('RCVSCN_ITMCD', $item)
            ->join('RCV_TBL', function ($f) {
                $f->on('RCVSCN_ITMCD', 'RCV_ITMCD');
                $f->on('RCVSCN_DONO', 'RCV_DONO');
            })
            ->where('RCV_BCNO', $stocknya['BCNUM_INC']);

        if (!empty($lot)) {
            $harga->where('RCVSCN_LOTNO', $lot);
        }

        return $harga->first();
    }
}
