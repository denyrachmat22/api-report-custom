<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;

class scrapReportComponentExport implements FromCollection, WithEvents, WithTitle
{
    use RegistersEventListeners;

    private $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function title(): string
    {
        return 'Scrap Component Detail';
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $hasil = [];
        foreach ($this->data as $key => $value) {
            $hasil[$key] = [
                [
                    'Part Number',
                    (string)$value['ITEMNUM']
                ],
                [
                    'Part Name',
                    $value['item_det']['MITM_ITMD1']
                ],
                [
                    $value['MDL_FLAG'] == 0 ? 'DO Number' : 'Job Number',
                    $value['DOC_NO']
                ],
                [
                    'Scrap Qty',
                    $value['QTY']
                ],
                [
                    'No',
                    'Part Code',
                    'Part Name',
                    // 'PSN',
                    // 'Category',
                    'Line',
                    'Process',
                    'Process ID',
                    'Lot No',
                    'Qty',
                    'Per Use',
                    'Price',
                    'Total Price'
                ]
            ];

            $totalCount = 0;
            $keynya = 5;
            foreach ($value['hist_det'] as $keyDet => $valueDet) {
                $hasil[$key][$keynya] = [
                    $keyDet+1,
                    $valueDet['SCR_ITMCD'],
                    $valueDet['MITM_ITMD1'],
                    // $valueDet['SCR_PSN'],
                    // $valueDet['SCR_CAT'],
                    $valueDet['SCR_LINE'],
                    $valueDet['SCR_PROCD'],
                    $valueDet['SCR_PROID'],
                    $valueDet['SCR_LOTNO'],
                    number_format($valueDet['SCR_QTY']),
                    number_format(floatval($valueDet['SCR_QTPER'])),
                    '$ '. number_format(floatval($valueDet['SCR_ITMPRC']), 4),
                    '$ '. number_format($valueDet['SCR_QTY'] * floatval($valueDet['SCR_ITMPRC']), 4),
                ];

                $totalCount += $valueDet['SCR_QTY'] * floatval($valueDet['SCR_ITMPRC']);

                $keynya++;
            }

            $hasil[$key][$keynya] = [
                'Total',
                '',
                '',
                '',
                // '',
                // '',
                '',
                // '',
                // '',
                '',
                '',
                '',
                '$ '.$totalCount,
            ];

            $hasil[$key][$keynya + 1] = [''];
        }

        return collect($hasil);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // $event->sheet->getActiveSheet()->setPrintGridlines(false);
                $event->sheet->getDelegate()->getPageSetup()
                    ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE)
                    ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A3);

                $start = 1;
                foreach ($this->data as $key => $value) {
                    $startTitle = $start;
                    $endTitle = $start + 4;
                    $event->sheet->getDelegate()->mergeCells('B'.$startTitle.':I'.$startTitle);

                    $startTitle2 = $startTitle + 1;
                    $event->sheet->getDelegate()->mergeCells('B'.$startTitle2.':I'.$startTitle2);

                    $startTitle3 = $startTitle2 + 1;
                    $event->sheet->getDelegate()->mergeCells('B'.$startTitle3.':I'.$startTitle3);

                    $startTitle4 = $startTitle3 + 1;
                    $event->sheet->getDelegate()->mergeCells('B'.$startTitle4.':I'.$startTitle4);
                    $event->sheet->getStyle('A'.$startTitle.':I'.$endTitle)->applyFromArray([
                        'font' => [
                            'size' => '12',
                            'bold' => true
                        ]
                    ]);

                    $start = $start + 4;

                    foreach ($value['hist_det'] as $keyDet => $valueDet) {
                        $start++;
                    }

                    $endOfCell = $start + 1;
                    $event->sheet->styleCells(
                        'A'.$startTitle.':I'.$endOfCell,
                        [
                            'borders' => [
                                'allBorders' => [
                                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                ],
                            ]
                        ]
                    );

                    $event->sheet->getDelegate()->mergeCells('A'.$endOfCell.':H'.$endOfCell);

                    $startTitle5 = $startTitle4 + 1;

                    $event->sheet->getStyle('A'.$startTitle5.':A'.$endOfCell)->getAlignment()->setHorizontal('center');
                    $event->sheet->getStyle('A'.$startTitle5.':A'.$endOfCell)->getAlignment()->setVertical('center');

                    $start = $start + 3;
                }
            }
        ];
    }
}
