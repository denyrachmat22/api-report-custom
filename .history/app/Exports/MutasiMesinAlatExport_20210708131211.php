<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MutasiMesinAlatExport implements FromCollection, WithHeadings, WithEvents, WithStartRow
{
    use Exportable;
    use RegistersEventListeners;

    private $data;
    private $period;

    public function __construct($data,$period)
    {
        $this->data = $data;
        $this->period = $period;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $hasil = [];
        foreach ($this->data as $key => $value) {
            $hasil[] = (object) array_merge(
                ['NO' => $key+1],
                (array)$value
            );
        }
        return collect($hasil);
    }

    public function headings(): array
    {
        return [
            ['LAPORAN PERTANGGUNGJAWABAN MUTASI BARANG MESIN DAN PERALATAN'],
            ['PT SMT INDONESIA'],
            ['PERIODE '.$this->period[0].' S.D '.$this->period[1]],
            [],
            [
                'NO',
                'KODE BARANG',
                'NAMA BARANG',
                'SATUAN',
                'SALDO AWAL',
                'PEMASUKAN',
                'PENGELUARAN',
                'PENYESUAIAN',
                'SALDO AKHIR',
                'SALDO OPNAME',
                'SELISIH',
                'KETERANGAN'
            ],
            [
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                $this->period[1],
                $this->period[1],
                '',
                '',
            ]
         ];
    }

    public function startRow() : int
    {
        return 7;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // $event->sheet->getActiveSheet()->setPrintGridlines(false);
                $event->sheet->getDelegate()->getPageSetup()
                    ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE)
                    ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A3);

                $event->sheet->getStyle('A1:A3')->applyFromArray([
                    'font' => [
                        'size' => '15',
                        'bold' => true
                    ]
                ]);

                $highestRow = $event->sheet->getHighestRow();
                $highestColumn = $event->sheet->getHighestColumn();

                $event->sheet->styleCells(
                    'A5:'.$highestColumn.$highestRow,
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],
                        ]
                    ]
                );

                $event->sheet->getStyle('A5:O6')->getAlignment()->setHorizontal('center');

                $event->sheet->getDelegate()->mergeCells('A1:L1');
                $event->sheet->getDelegate()->mergeCells('A2:L2');
                $event->sheet->getDelegate()->mergeCells('A3:L3');
                $event->sheet->getDelegate()->mergeCells('A4:L4');
                $event->sheet->getDelegate()->mergeCells('A5:A6');
                $event->sheet->getDelegate()->mergeCells('B5:B6');
                $event->sheet->getDelegate()->mergeCells('C5:C6');
                $event->sheet->getDelegate()->mergeCells('D5:D6');
                $event->sheet->getDelegate()->mergeCells('E5:E6');
                $event->sheet->getDelegate()->mergeCells('F5:F6');
                $event->sheet->getDelegate()->mergeCells('G5:G6');
                $event->sheet->getDelegate()->mergeCells('H5:H6');
                $event->sheet->getDelegate()->mergeCells('K5:K6');
                $event->sheet->getDelegate()->mergeCells('L5:L6');
            },
        ];
    }
}
