<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;

class scrapFGReturnUploadTemplateExport implements FromCollection, WithHeadings, WithStartRow, WithEvents, WithTitle
{
    private $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $hasil = [];
        foreach ($this->data as $key => $value) {
            $hasil[] = [
                $key + 1,
                (string) $value['assy_no'],
                $value['do_no'],
                $value['reason']
            ];
        }

        return collect($hasil);
    }

    public function title(): string
    {
        return 'Material Scrap Upload Template';
    }

    public function headings(): array
    {
        return [
            ['MFG Repair Center'],
            ['MATERIAL DOC'],
            ['PERIODE :'],
            [],
            [
                'No',
                'PCB Identity',
                '',
                '',
                'Process'
            ],
            array_merge(
                [
                    '',
                    'Assy No',
                    'RA No',
                    'Cause of MRB',
                    'Qty'
                ]
            )
        ];
    }

    public function startRow(): int
    {
        return 5;
    }

    public function toAlpha($num)
    {
        $tot = $num + 65;

        return chr(substr('000' . $tot, -3));
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // Start Protect Template
                $event->sheet->getProtection()->setPassword('PSI2021');
                $event->sheet->getProtection()->setSheet(true);
                // End Protect Template

                // Styling Template
                $highestRow = $event->sheet->getHighestRow();
                $highestColumn = $event->sheet->getHighestColumn();

                $event->sheet->getDelegate()->getPageSetup()
                    ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE)
                    ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A3);

                $event->sheet->getStyle('A1:' . $highestColumn . '3')->applyFromArray([
                    'font' => [
                        'size' => '15',
                        'bold' => true
                    ]
                ]);

                $event->sheet->getDelegate()->mergeCells('A1:C1');
                $event->sheet->getDelegate()->mergeCells('A2:C2');
                $event->sheet->getDelegate()->mergeCells('A3:B3');

                $event->sheet->getDelegate()->setCellValue($highestColumn . '1', 'FGRETURN');
                $event->sheet->getStyle($highestColumn . '1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $event->sheet->getDelegate()->mergeCells('A5:A6');
                $event->sheet->getDelegate()->mergeCells('B5:D5');
                $event->sheet->getDelegate()->mergeCells('E5:' . $highestColumn . '5');

                $event->sheet->getStyle('A5:' . $highestColumn . '6')->applyFromArray([
                    'font' => [
                        'size' => '12',
                        'bold' => true
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);

                $event->sheet->styleCells(
                    'A5:' . $highestColumn . $highestRow,
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],
                        ]
                    ]
                );

                foreach (range('A', $highestColumn) as $columnID) {
                    $event->sheet->getColumnDimension($columnID)->setAutoSize(true);
                }

                // Color Cells
                $event->sheet->getStyle('B5:D6')
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('ffbf91');

                $event->sheet->getStyle('E5:' . $highestColumn . '6')
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('91b4ff');

                // Can Edit Cell

                // Period Cell
                $event->sheet->getStyle('C3')->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
                $event->sheet->getStyle('A7:L1000')->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);

                $startRow = 7;
                $startProcess = 5;

                $startStock = 2;
                foreach ($this->data as $key => $value) {
                    $this->setValidation($event->sheet, $highestColumn. ($startRow + $key), \PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_WHOLE, 'Please input number only !');

                    if (isset($value['qty']) && $value['qty'] > 0) {
                        $event->sheet->getDelegate()->setCellValue('G'. ($startRow + $key), $value['qty']);
                    }

                    $event->sheet->getDelegate()->setCellValue('G'. ($startRow + $key), '=IF(SUM('.$this->toAlpha($startProcess). ($startRow + $key).':'.$highestColumn . ($startRow + $key).') <= Worksheet!B'.($startStock + $key).', "OK", "NOK")');

                    $validation = $event->sheet->getCell($highestColumn. ($startRow + $key))->getDataValidation();
                    $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_CUSTOM);
                    $validation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP);
                    $validation->setAllowBlank(true);
                    $validation->setShowInputMessage(true);
                    $validation->setShowErrorMessage(true);
                    $validation->setErrorTitle('Requested Stock Over');
                    $validation->setError('Your Requested qty is more than stock, please change your qty.');
                    $validation->setFormula1('=IF(G'. ($startRow + $key).' = "OK", TRUE, FALSE)');
                }
            }
        ];
    }

    public function setValidation($fun, $cells, $type, $msg)
    {
        $validation = $fun->getCell($cells)->getDataValidation();
        $validation->setType($type);
        $validation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP);
        $validation->setAllowBlank(true);
        $validation->setShowInputMessage(true);
        $validation->setShowErrorMessage(true);
        $validation->setErrorTitle('Input error');
        $validation->setError($msg);
        $validation->setPromptTitle('Allowed input');

        return $validation;
    }
}
