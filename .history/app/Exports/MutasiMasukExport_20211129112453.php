<?php

namespace App\Exports;

use App\Model\WMS\REPORT\MutasiStok;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class MutasiMasukExport implements FromCollection,WithStartRow,WithHeadings,WithEvents,WithColumnFormatting
{
    use Exportable;
    use RegistersEventListeners;

    private $data;
    private $period;
    private $type;
    private $typeExp;

    public function __construct($data,$period,$type = 'INC', $typeExp = 'excel')
    {
        $this->data = $data;
        $this->period = $period;
        $this->type = $type;
        $this->typeExp = $typeExp;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $hasil = [];
        foreach ($this->data as $key => $value) {
            $newdata2 = [
                'RPBEA_JENBEA' => $value['RPBEA_JENBEA'],
                'RPBEA_NUMBEA' => $value['RPBEA_NUMBEA'],
                'RPBEA_TGLBEA' => $value['RPBEA_TGLBEA'],
                'RPBEA_NUMSJL' => $value['RPBEA_NUMSJL'],
                'RPBEA_TGLSJL' => $value['RPBEA_RCVEDT'],
                'RPBEA_NUMPEN' => $value['RPBEA_NUMPEN'],
                'RPBEA_TGLPEN' => $value['RPBEA_TGLPEN'],
                'MSUP_SUPNM' => $this->type == 'INC'
                    ? ($value['MSUP_SUPNM'])
                    : ($value['MCUS_CUSNM']),
                'RPBEA_ITMCOD' => $value['RPBEA_ITMCOD'],
                'MITM_ITMD1' => $value['MITM_ITMD1'],
                'RPBEA_QTYJUM' => $value['RPBEA_QTYJUM'],
                'RPBEA_UNITMS' => $value['RPBEA_UNITMS'],
                'RPBEA_VALAS' => $value['RPBEA_VALAS'],
                'QTY' => $value['QTY'],
                'INV_NO' => $value['INV_NO'],
                'STATUS_KIRIM' => $value['STATUS_KIRIM'],
                // 'RPBEA_QTYTOT' => $value['RPBEA_QTYTOT'],
                // 'RPBEA_TYPE' => $value['RPBEA_TYPE'],
                // 'RPBEA_RCVEDT' => $value['RPBEA_RCVEDT'],
            ];

            $hasil[] = (object) array_merge(
                ['NO' => $key+1],
                (array)$newdata2
            );
        }
        return collect($hasil);
    }

    public function columnFormats(): array
    {
        return [
            'L' => NumberFormat::FORMAT_TEXT,
            'O' => NumberFormat::FORMAT_TEXT
        ];
    }

    public function headings(): array
    {
        return [
            [$this->type === 'INC' ? 'LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN' : 'LAPORAN PENGELUARAN BARANG PER DOKUMEN PABEAN'],
            ['PT SMT INDONESIA'],
            ['PERIODE '.$this->period[0].' S.D '.$this->period[1]],
            [],
            [
                'NO',
                'DOKUMEN PABEAN',
                '',
                '',
                'BUKTI PENERIMAAN BARANG',
                '',
                'AJU',
                '',
                $this->type == 'INC' ? 'PENGIRIM BARANG' : 'PENERIMA BARANG',
                'KODE BARANG',
                'NAMA BARANG',
                'JUMLAH',
                'SATUAN',
                'VALAS',
                'NILAI',
                'INVOICE',
                'STATUS'
            ],
            [
                '',
                'JENIS',
                'NOMOR',
                'TANGGAL',
                'NOMOR',
                'TANGGAL',
                'NOMOR',
                'TANGGAL',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                ''
            ]
         ];
    }

    public function startRow() : int
    {
        return 7;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // $event->sheet->getActiveSheet()->setPrintGridlines(false);
                $event->sheet->getDelegate()->getPageSetup()
                    ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE)
                    ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A3);

                $event->sheet->getStyle('A1:A3')->applyFromArray([
                    'font' => [
                        'size' => '15',
                        'bold' => true
                    ]
                ]);

                $event->sheet->getStyle('A5:Q6')
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('ffffff');

                $highestRow = $event->sheet->getHighestRow();
                $highestColumn = $event->sheet->getHighestColumn();

                $event->sheet->getStyle('L7:Q'.$highestRow)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);

                if ($this->typeExp === 'excel') {
                    foreach(range('A', $highestColumn) as $columnID) {
                        $event->sheet->getColumnDimension($columnID)->setAutoSize(true) ;
                    }
                }

                $event->sheet->styleCells(
                    'A5:'.$highestColumn.$highestRow,
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],
                        ]
                    ]
                );

                $event->sheet->styleCells(
                    'O7:O'.$highestRow,
                    [
                        'numberFormat' => [
                            'formatCode' => \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2
                        ]
                    ]
                );

                $event->sheet->getStyle('A5:H'.$highestRow)->getAlignment()->setHorizontal('center');

                $event->sheet->getDelegate()->mergeCells('A1:Q1');
                $event->sheet->getDelegate()->mergeCells('A2:Q2');
                $event->sheet->getDelegate()->mergeCells('A3:Q3');
                $event->sheet->getDelegate()->mergeCells('A4:Q4');
                $event->sheet->getDelegate()->mergeCells('A5:A6');
                $event->sheet->getDelegate()->mergeCells('I5:I6');
                $event->sheet->getDelegate()->mergeCells('J5:J6');
                $event->sheet->getDelegate()->mergeCells('K5:K6');
                $event->sheet->getDelegate()->mergeCells('L5:L6');
                $event->sheet->getDelegate()->mergeCells('M5:M6');
                $event->sheet->getDelegate()->mergeCells('N5:N6');
                $event->sheet->getDelegate()->mergeCells('O5:O6');
                $event->sheet->getDelegate()->mergeCells('P5:P6');
                $event->sheet->getDelegate()->mergeCells('Q5:Q6');
                $event->sheet->getDelegate()->mergeCells('B5:D5');
                $event->sheet->getDelegate()->mergeCells('E5:F5');
                $event->sheet->getDelegate()->mergeCells('G5:H5');

                $event->sheet->getStyle('I5:I6')->getAlignment()->setHorizontal('center');
                $event->sheet->getStyle('I5:I6')->getAlignment()->setVertical('center');
                $event->sheet->getStyle('J5:J6')->getAlignment()->setHorizontal('center');
                $event->sheet->getStyle('J5:J6')->getAlignment()->setVertical('center');
                $event->sheet->getStyle('K5:K6')->getAlignment()->setHorizontal('center');
                $event->sheet->getStyle('K5:K6')->getAlignment()->setVertical('center');
                $event->sheet->getStyle('L5:L6')->getAlignment()->setHorizontal('center');
                $event->sheet->getStyle('L5:L6')->getAlignment()->setVertical('center');
                $event->sheet->getStyle('M5:M6')->getAlignment()->setHorizontal('center');
                $event->sheet->getStyle('M5:M6')->getAlignment()->setVertical('center');
                $event->sheet->getStyle('N5:N6')->getAlignment()->setHorizontal('center');
                $event->sheet->getStyle('N5:N6')->getAlignment()->setVertical('center');
                $event->sheet->getStyle('O5:O6')->getAlignment()->setHorizontal('center');
                $event->sheet->getStyle('O5:O6')->getAlignment()->setVertical('center');
                $event->sheet->getStyle('P5:P6')->getAlignment()->setHorizontal('center');
                $event->sheet->getStyle('P5:P6')->getAlignment()->setVertical('center');
                $event->sheet->getStyle('Q5:Q6')->getAlignment()->setHorizontal('center');
                $event->sheet->getStyle('Q5:Q6')->getAlignment()->setVertical('center');

                $event->sheet->getStyle('I7:I'.$highestRow)->getAlignment()->setHorizontal('left');
                $event->sheet->getStyle('I7:I'.$highestRow)->getAlignment()->setVertical('center');
                $event->sheet->getStyle('J7:J'.$highestRow)->getAlignment()->setHorizontal('left');
                $event->sheet->getStyle('J7:J'.$highestRow)->getAlignment()->setVertical('center');
                $event->sheet->getStyle('K7:K'.$highestRow)->getAlignment()->setHorizontal('left');
                $event->sheet->getStyle('K7:K'.$highestRow)->getAlignment()->setVertical('center');
                $event->sheet->getStyle('L7:L'.$highestRow)->getAlignment()->setHorizontal('right');
                $event->sheet->getStyle('L7:L'.$highestRow)->getAlignment()->setVertical('center');
                $event->sheet->getStyle('O5:O'.$highestRow)->getAlignment()->setHorizontal('right');
                $event->sheet->getStyle('O5:O'.$highestRow)->getAlignment()->setVertical('center');
                $event->sheet->getStyle('P5:P'.$highestRow)->getAlignment()->setHorizontal('right');
                $event->sheet->getStyle('P5:P'.$highestRow)->getAlignment()->setVertical('center');
                $event->sheet->getStyle('Q5:Q'.$highestRow)->getAlignment()->setHorizontal('right');
                $event->sheet->getStyle('Q5:Q'.$highestRow)->getAlignment()->setVertical('center');
            },
        ];
    }
}
