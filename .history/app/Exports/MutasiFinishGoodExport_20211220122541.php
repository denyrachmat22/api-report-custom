<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MutasiFinishGoodExport implements FromCollection, WithHeadings, WithEvents, WithStartRow
{
    use Exportable;
    use RegistersEventListeners;

    private $data;
    private $period;
    private $format;

    public function __construct($data,$period,$format)
    {
        $this->data = $data;
        $this->period = $period;
        $this->format = $format;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $hasil = [];
        foreach ($this->data as $key => $value) {
            $hasil[] = (object) array_merge(
                ['NO' => $key+1],
                ['KODE BARANG' => $value['RPGOOD_ITMCOD']],
                ['NAMA BARANG' => $value['MITM_ITMD1']],
                ['SATUAN' => $value['RPGOOD_UNITMS']],
                ['SALDO AWAL' => number_format($value['SALDO_AWAL'])],
                ['PEMASUKAN' => number_format($value['SALDO_MASUK'])],
                ['PENGELUARAN' => number_format($value['SALDO_KELUAR'])],
                ['PENYESUAIAN' => number_format($value['SALDO_ADJ'])],
                ['SALDO AKHIR' => number_format($value['SALDO_TOT'])],
                ['SALDO OPNAME' => number_format($value['SALDO_OPN'])],
                ['SELISIH' => number_format($value['SALDO_SLS'])],
                ['KETERANGAN' => ''],
            );
        }

        return collect($hasil);
    }

    public function headings(): array
    {
        return [
            ['LAPORAN PERTANGGUNGJAWABAN MUTASI BARANG JADI'],
            ['PT SMT INDONESIA'],
            ['PERIODE '.$this->period[0].' S.D '.$this->period[1]],
            [],
            [
                'NO',
                'KODE BARANG',
                'NAMA BARANG',
                'SATUAN',
                'SALDO AWAL',
                'PEMASUKAN',
                'PENGELUARAN',
                'PENYESUAIAN',
                'SALDO BUKU',
                'STOCK OPNAME',
                'SELISIH',
                'KETERANGAN'
            ],
            [
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                $this->data[0]['SALDO_OPN_DATE'],
                '',
                '',
            ]
         ];
    }

    public function startRow() : int
    {
        return 7;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // $event->sheet->getActiveSheet()->setPrintGridlines(false);
                $event->sheet->getDelegate()->getPageSetup()
                    ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE)
                    ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A3);

                $event->sheet->getStyle('A1:A3')->applyFromArray([
                    'font' => [
                        'size' => '15',
                        'bold' => true
                    ]
                ]);

                $event->sheet->getStyle('A5:L5')
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('ffffff');

                $highestRow = $event->sheet->getHighestRow();
                $highestColumn = $event->sheet->getHighestColumn();

                if ($this->format === 'excel') {
                    foreach(range('A', $highestColumn) as $columnID) {
                        $event->sheet->getColumnDimension($columnID)->setAutoSize(true) ;
                    }
                }

                $event->sheet->styleCells(
                    'A5:'.$highestColumn.$highestRow,
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],
                        ]
                    ]
                );

                $event->sheet->getStyle('A5:L5')->getAlignment()->setHorizontal('center');

                $event->sheet->getDelegate()->mergeCells('A1:L1');
                $event->sheet->getDelegate()->mergeCells('A2:L2');
                $event->sheet->getDelegate()->mergeCells('A3:L3');
                $event->sheet->getDelegate()->mergeCells('A4:L4');

                $event->sheet->getDelegate()->mergeCells('A5:A6');
                $event->sheet->getDelegate()->mergeCells('B5:B6');
                $event->sheet->getDelegate()->mergeCells('C5:C6');
                $event->sheet->getDelegate()->mergeCells('D5:D6');
                $event->sheet->getDelegate()->mergeCells('E5:E6');
                $event->sheet->getDelegate()->mergeCells('F5:F6');
                $event->sheet->getDelegate()->mergeCells('G5:G6');
                $event->sheet->getDelegate()->mergeCells('H5:H6');
                $event->sheet->getDelegate()->mergeCells('I5:I6');
                $event->sheet->getDelegate()->mergeCells('K5:K6');
                $event->sheet->getDelegate()->mergeCells('L5:L6');

                $event->sheet->getStyle('A5:A'.$highestRow)->getAlignment()->setHorizontal('left');
                $event->sheet->getStyle('B5:B'.$highestRow)->getAlignment()->setHorizontal('left');
                $event->sheet->getStyle('C5:C'.$highestRow)->getAlignment()->setHorizontal('left');
                $event->sheet->getStyle('D5:D'.$highestRow)->getAlignment()->setHorizontal('left');
                $event->sheet->getStyle('E5:E'.$highestRow)->getAlignment()->setHorizontal('right');
                $event->sheet->getStyle('E5:E'.$highestRow)->getAlignment()->setHorizontal('right');
                $event->sheet->getStyle('F5:F'.$highestRow)->getAlignment()->setHorizontal('right');
                $event->sheet->getStyle('G5:G'.$highestRow)->getAlignment()->setHorizontal('right');
                $event->sheet->getStyle('H5:H'.$highestRow)->getAlignment()->setHorizontal('right');
                $event->sheet->getStyle('I5:I'.$highestRow)->getAlignment()->setHorizontal('right');
                $event->sheet->getStyle('J5:J'.$highestRow)->getAlignment()->setHorizontal('right');
                $event->sheet->getStyle('K5:K'.$highestRow)->getAlignment()->setHorizontal('right');
            },
        ];
    }
}
