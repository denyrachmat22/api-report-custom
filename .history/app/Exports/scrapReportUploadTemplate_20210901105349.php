<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class scrapReportUploadTemplate implements FromCollection, WithMultipleSheets
{
    private $data;
    public function __construct($data, $type)
    {
        $this->data = $data;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
    }

    public function sheets(): array
    {
        $sheets = [
            new scrapReportUploadTemplateStockRows($this->data)
        ];

        return $sheets;
    }
}
