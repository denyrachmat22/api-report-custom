<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class scrapReportUploadTemplate implements FromCollection, WithMultipleSheets
{
    private $data;
    public function __construct($data, $type)
    {
        $this->data = $data;
        $this->type = $type;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
    }

    public function sheets(): array
    {
        switch ($this->type) {
            case 'wip':
                return new scrapWIPUploadTemplateExport($this->data);

                break;
            case 'pending':
                return new scrapPendingUploadTemplateExport($this->data);

                break;
            case 'material':
                return new scrapMaterialUploadTemplateExport($this->data);

                break;
            default:
                return new scrapFGReturnUploadTemplateExport($this->data);

                break;
        }

        $sheets = [
            new scrapReportUploadTemplateStockRows($this->data)
        ];

        return $sheets;
    }
}
