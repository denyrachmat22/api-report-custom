<html>
<style type="text/css">
    .tg {
        border-collapse: collapse;
        border-color: #ccc;
        border-spacing: 0;
    }

    .tg2 {
        border-collapse: collapse;
        border-color: #ccc;
        border-spacing: 0;
    }

    .tg td {
        background-color: #fff;
        border-color: #ccc;
        border-style: none;
        border-width: 0px;
        color: #333;
        font-family: Arial, sans-serif;
        font-size: 14px;
        overflow: hidden;
        padding: 10px 5px;
        word-break: normal;
    }

    .tg th {
        background-color: #f0f0f0;
        border-color: #ccc;
        border-style: none;
        border-width: 0px;
        color: #333;
        font-family: Arial, sans-serif;
        font-size: 14px;
        font-weight: normal;
        overflow: hidden;
        padding: 10px 5px;
        word-break: normal;
    }

    .tg .tg-abx8 {
        background-color: #c0c0c0;
        border-color: #c0c0c0;
        font-weight: bold;
        text-align: left;
        vertical-align: top
    }

    .tg .tg-1wig {
        font-weight: bold;
        text-align: left;
        vertical-align: top
    }

    .tg .tg-k4px {
        background-color: #f0f0f0;
        font-weight: bold;
        text-align: left;
        vertical-align: top
    }

    .tg .tg-baqh {
        text-align: center;
        vertical-align: top
    }

    .tg .tg-8rcp {
        background-color: #FFF;
        font-weight: bold;
        text-align: left;
        vertical-align: middle
    }

    .tg .tg-kftd {
        background-color: #efefef;
        text-align: right;
        vertical-align: top
    }

    .tg .tg-mft3 {
        background-color: #f0f0f0;
        border-color: #cccccc;
        text-align: left;
        vertical-align: top
    }

    .tg .tg-mv9k {
        border-color: #f0f0f0;
        text-align: left;
        vertical-align: top
    }

    .tg .tg-0pky {
        border-color: inherit;
        text-align: left;
        vertical-align: top
    }

    .tg .tg-0lax {
        text-align: left;
        vertical-align: top
    }

    .tg .tg-z3qe {
        background-color: #f0f0f0;
        border-color: inherit;
        font-size: 20px;
        text-align: center;
        vertical-align: top
    }

    .tg .tg-odmy {
        background-color: #f0f0f0;
        text-align: left;
        vertical-align: top
    }

    .tg .tg-y6fn {
        background-color: #c0c0c0;
        margin: 0;
        text-align: left;
        vertical-align: top
    }

    .tg .tg-y6fn-title {
        background-color: #c0c0c0;
        margin: 0;
        text-align: center;
        vertical-align: top
    }

    .tg .tg-y6fn-amount {
        background-color: #c0c0c0;
        margin: 0;
        text-align: right;
        vertical-align: top
    }

    .tg .tg-73oq {
        border-color: #000000;
        text-align: center;
        vertical-align: top
    }

    .tg .tg-0lax {
        text-align: left;
        vertical-align: top
    }

    .tg .tg-mqa1 {
        border-color: #000000;
        font-weight: bold;
        text-align: center;
        vertical-align: top
    }

    .tg .tg-0lax {
        text-align: left;
        vertical-align: top
    }

    .tg .tg-amwm {
        font-weight: bold;
        text-align: center;
        vertical-align: top
    }

    .tg .tg-baqh {
        text-align: center;
        vertical-align: top
    }


    .tg2 {
        border-collapse: collapse;
        border-spacing: 0;
    }

    .tg2 td {
        border-color: black;
        border-style: solid;
        border-width: 1px;
        font-family: Arial, sans-serif;
        font-size: 14px;
        overflow: hidden;
        padding: 10px 5px;
        word-break: normal;
    }

    .tg2 th {
        border-color: black;
        border-style: solid;
        border-width: 1px;
        font-family: Arial, sans-serif;
        font-size: 14px;
        font-weight: normal;
        overflow: hidden;
        padding: 10px 5px;
        word-break: normal;
    }

    .tg2 .tg2-0lax {
        text-align: left;
        vertical-align: top
    }
</style>
<table class="tg">
    <thead>
        <tr>
            <th class="tg-mv9k" colspan="2">PT SMT Indonesia</th>
            <th class="tg-0pky"></th>
            <th class="tg-0lax"></th>
            <th class="tg-0lax"></th>
            <th class="tg-0lax"></th>
            <th class="tg-0lax"></th>
            <th class="tg-0lax"></th>
            <th class="tg-0pky" colspan="5" style="text-align: right;">Form : FSOP-13-01</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="tg-mft3"></td>
            <td class="tg-mft3"></td>
            <td class="tg-mft3"></td>
            <td class="tg-mft3"></td>
            <td class="tg-mft3"></td>
            <td class="tg-mft3"></td>
            <td class="tg-mft3"></td>
            <td class="tg-mft3"></td>
            <td class="tg-mft3" colspan="5" style="text-align: right;">Rev. 02</td>
        </tr>
        <tr>
            <td class="tg-z3qe" colspan="13">
                @if($data[0]['REASON'] === 'LOSS')
                <span style="font-weight:bold">SCRAP REPORT (LOSS PRODUCTION)</span>
                @else
                <span style="font-weight:bold">SCRAP REPORT</span>
                @endif
            </td>
        </tr>
        <tr>
            <td class="tg-odmy" colspan="2"><span style="font-weight:bold">DOC. NUMBER</span></td>
            <td class="tg-k4px">: {{$data[0]['ID_TRANS']}}</td>
            <td class="tg-odmy"></td>
            <td class="tg-odmy"></td>
            <td class="tg-odmy"></td>
            <td class="tg-odmy"></td>
            <td class="tg-odmy"><b>DATE</b></td>
            <td class="tg-odmy" style="min-width: 200px">: {{(date('Y-m-d H:i:s', strtotime($data[0]['created_at'])))}}</td>
            <td class="tg-odmy"></td>
            <td class="tg-odmy"></td>
            <td class="tg-odmy"></td>
            <td class="tg-odmy"></td>
        </tr>
        <tr>
            <td class="tg-1wig" colspan="2">MODEL</td>
            <td class="tg-1wig">: ATTACHED</td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-8rcp">ISSUED BY</td>
            <td class="tg-0lax">: {{$data[0]['users']['MSTEMP_FNM']}} {{$data[0]['users']['MSTEMP_LNM']}}</td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
        <tr>
            <td class="tg-1wig" colspan="2">PART CODE</td>
            <td class="tg-1wig">: {{count($data) === 1 ? $data[0]['ITEMNUM'] : 'ATTACHED'}}</td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-8rcp">DEPARTMENT</td>
            <td class="tg-0lax">: {{$data[0]['DEPT']}}</td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
        <tr>
            <td class="tg-1wig" colspan="2">PART NAME</td>
            <td class="tg-1wig" style="min-width: 250px;">: {{count($data) === 1 ? $data[0]['item_det']['MITM_ITMD1'] : 'ATTACHED'}}</td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-8rcp"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
        <tr>
            <td class="tg-1wig" colspan="2" style="min-width: 200px;">JOB NO / DO NO</td>
            <td class="tg-1wig">: {{count($data) === 1 ? $data[0]['DOC_NO'] : 'ATTACHED'}}</td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
        <tr>
            <td class="tg-1wig" colspan="2">FOUND AT</td>
            <td class="tg-1wig">: {{count($foundLoc) !== 1 ? 'ATTACHED' : $foundLoc[0][0]['design_map']['dsgMaster']['RPDSGN_CODE']}}
            </td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
        <tr>
            <td class="tg-abx8" colspan="3">PROCESS SCRAP :</td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
        </tr>
        <tr>
            <td class="tg-y6fn" colspan="5"><b>{{date('l')}}, {{date('d M Y')}}</b></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-title"><b>QTY</b></td>
            <td class="tg-y6fn-title" colspan="5"><b>AMOUNT</b></td>
        </tr>
        <?php
        $totqty = $totharga = 0;
        ?>

        <!-- Start hitung subtotal Model -->
        @foreach($processmstr as $key => $val)
        <?php
        $totpartHeader = 0;

        $subtotqtynya = $subtotharga = 0;

        $test = [];
        $hasilcekDet = $totalAmountPerProcess = 0;
        $cekArr = [];
        foreach ($data as $keyDataParsing => $valueDataParsing) {
            if ($valueDataParsing['MDL_FLAG'] == "1") {
                $cekArr[] = $valueDataParsing;

                /* looping detail */
                // logger($valueDataParsing['ITEMNUM']);
                // New Code compare design code
                if ($valueDataParsing['design_map'] && isset($valueDataParsing['design_map']['RPDSGN_PRO_ID']) && $valueDataParsing['design_map']['RPDSGN_PRO_ID'] === $val['RPDSGN_CODE']) {
                    $subtotqtynya += isset($valueDataParsing['QTY']) ? (int)$valueDataParsing['QTY'] : 0;
                }

                // Old Code compare design code
                // foreach ($valueDataParsing['DET'] as $keyParseDet => $valueParseDet) {
                //     if ($valueParseDet['design_map']['RPDSGN_PRO_ID'] === $val['RPDSGN_CODE']) {
                //         $subtotqtynya += $valueParseDet['QTY'];
                //     }
                // }

                /* Sum All Part Price */

                // New Code Sum Price per Part
                $subtothargadet = 0;
                foreach ($valueDataParsing['hist_det'] as $keyFifo => $valueFifo) {
                    $subtothargadet += $val['RPDSGN_CODE'] === $valueDataParsing['design_map']['RPDSGN_PRO_ID'] ? floatval($valueFifo['SCR_ITMPRC']) * $valueDataParsing['QTY'] : 0;
                    $test[$keyDataParsing][] = [$valueFifo['SCR_ITMPRC'], $valueFifo['SCR_ITMPRC'] * $valueDataParsing['QTY']];
                }

                // Old Code Sum Price per Part
                // $subtothargadet = 0;
                // foreach ($valueDataParsing['FIFO_ARR'] as $keyFifo => $valueFifo) {
                //     $subtothargadet += floatval($valueFifo['HARGA_ASLI']) * $subtotqtynya;
                //     $test[$keyDataParsing][] = [$valueFifo['HARGA_ASLI'], $valueFifo['HARGA_ASLI'] * $subtotqtynya];
                // }

                $subtotharga += $subtothargadet;
            }
        }
        $totqty += $subtotqtynya;
        $totharga += $subtotharga;
        ?>
        <tr>
            <td class="tg-y6fn" colspan="5">{{$val['RPDSGN_DESC']}}</td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-amount">{{$subtotqtynya === 0 ? '-' : $subtotqtynya}}</td>
            <td class="tg-y6fn-amount" colspan="5">$ {{ $subtotharga === 0 ? '0' : number_format($subtotharga, 4) }}</td>
            <!-- <td class="tg-y6fn" colspan="4">$ {{ json_encode($test) }}</td> -->
        </tr>
        @endforeach
        <!-- End hitung subtotal Model -->

        <!-- Start hitung subtotal Material -->
        <?php
        $cekPart = array_filter($data, function ($f) {
            return $f['MDL_FLAG'] == 0;
        });

        $cekFinGoodReturn = array_filter($data, function ($f) {
            return $f['MDL_FLAG'] == 1 && $f['IS_RETURN'] == 1;
        });

        $subTotPart = $subTotHargaPart = $subTotFinGoodReturn = $subTotHargaFinGoodReturn = 0;

        foreach ($cekPart as $keyPart => $valuePart) {
            $subTotPart += $valuePart['QTY'];
            // foreach ($valuePart['hist_det'] as $keyPartDet => $valuePartDet) {
            //     $subTotPart += $valuePartDet['QTY'];
            // }

            if ($valuePart['REASON'] === 'LOSS') {
                $subtothargadet = 0;
                foreach ($valuePart['hist_det'] as $keyFifo => $valueFifo) {
                    $subtothargadet += $valueFifo['SCR_QTY'] * $valueFifo['SCR_ITMPRC'];
                }

                $subTotHargaPart += $subtothargadet;
            } else {
                $subTotHargaPart += $valuePart['QTY'] * (isset($valuePart['hist_det'][0]['SCR_ITMPRC'])
                    ? $valuePart['hist_det'][0]['SCR_ITMPRC']
                    : 0);
            }
        }

        foreach ($cekFinGoodReturn as $keyFGRet => $valueFGRet) {
            $subTotFinGoodReturn += $valueFGRet['QTY'];
        }
        ?>
        <tr>
            <td class="tg-y6fn" colspan="5">Part Level</td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-amount">{{$subTotPart === 0 ? '-' : $subTotPart}}</td>
            <td class="tg-y6fn-amount" colspan="5">$ {{$subTotHargaPart === 0 ? 0 : number_format($subTotHargaPart, 4)}}</td>
            <!-- <td class="tg-y6fn" colspan="4">$ {{json_encode($cekPart)}}</td> -->
        </tr>
        <tr>
            <td class="tg-y6fn" colspan="5">FG Return</td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-amount">{{$subTotFinGoodReturn === 0 ? '-' : $subTotFinGoodReturn}}</td>
            <td class="tg-y6fn-amount" colspan="5">$ 0</td>
            <!-- <td class="tg-y6fn" colspan="4">$ {{json_encode($cekPart)}}</td> -->
        </tr>
        <tr>
            <td class="tg-y6fn" colspan="5">Other</td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn"></td>
            <td class="tg-y6fn-amount" colspan="5">$ </td>
            <!-- <td class="tg-y6fn" colspan="4">$ {{json_encode($cekPart)}}</td> -->
        </tr>
        <!-- End hitung subtotal Material -->

        <tr>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-1wig">TOTAL</td>
            <td class="tg-y6fn-amount">{{$totqty + $subTotPart}}</td>
            <td class="tg-y6fn-amount" colspan="5">$ {{number_format(($totharga + $subTotHargaPart), 4)}}</td>
            <td class="tg-0lax"></td>
        </tr>

        <tr>
            <td class="tg-baqh" style="border: 1px solid;" colspan="13"><span style="font-weight:bold">CHECKED AND APPROVED BY</span></td>
        </tr>
        <tr>
            <td style="border: 1px solid;" class="tg-mqa1" colspan="5">MANAGER QA</td>
            <td style="border: 1px solid;" class="tg-mqa1" colspan="3">MANAGER MFG</td>
            <td style="border: 1px solid;" class="tg-mqa1" colspan="5">MANAGER ENG</td>
        </tr>
        <tr>
            <td style="border: 1px solid;border-bottom: none;" class="tg-73oq" colspan="5"></td>
            <td style="border: 1px solid;border-bottom: none;" class="tg-73oq" colspan="3"></td>
            <td style="border: 1px solid;border-bottom: none;" class="tg-73oq" colspan="5"></td>
        </tr>
        <tr>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-73oq" colspan="5"></td>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-0lax" colspan="3"></td>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-0lax" colspan="5"></td>
        </tr>
        <tr>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-73oq" colspan="5"></td>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-0lax" colspan="3"></td>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-0lax" colspan="5"></td>
        </tr>
        <tr>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-73oq" colspan="5"></td>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-0lax" colspan="3"></td>
            <td style="border: 1px solid;border-bottom: none;border-top: none;" class="tg-0lax" colspan="5"></td>
        </tr>
        <tr>
            <td style="border: 1px solid;border-top: none;" class="tg-73oq" colspan="5"></td>
            <td style="border: 1px solid;border-top: none;" class="tg-0lax" colspan="3"></td>
            <td style="border: 1px solid;border-top: none;" class="tg-0lax" colspan="5"></td>
        </tr>
        <tr>
            <td class="tg-amwm" style="border: 1px solid;" colspan="5">PPIC</td>
            <td class="tg-amwm" style="border: 1px solid;" colspan="3">GENERAL MANAGER</td>
            <td class="tg-amwm" style="border: 1px solid;" colspan="5">PRESIDENT DIRECTOR</td>
        </tr>
        <tr>
            <td style="border: 1px solid;" class="tg-baqh" colspan="2">DATA INPUT</td>
            <td style="border: 1px solid;" class="tg-baqh" colspan="3">MANAGER</td>
            <td style="border: 1px solid;height: 120px;" class="tg-0lax" colspan="3" rowspan="5"></td>
            <td style="border: 1px solid;" class="tg-0lax" colspan="5" rowspan="5"></td>
        </tr>
        <tr>
            <td style="border: 1px solid;" class="tg-0lax" colspan="2" rowspan="4"></td>
            <td style="border: 1px solid;" class="tg-0lax" colspan="3" rowspan="4"></td>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
        <tr>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
        <tr>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-amwm" style="border: 1px solid;" colspan="5">TRANSFER SCRAP</td>
        </tr>
        <tr>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-baqh" style="border: 1px solid;" colspan="5">Received By Exim</td>
        </tr>
        <tr>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax" style="border: 1px solid;" colspan="5" rowspan="4"></td>
        </tr>
        <tr>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
        <tr>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
        <tr>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
    </tbody>
</table>
<div style="page-break-before:always">
    <table class="tg" style="width:100%">
        <thead>
            <tr>
                <th class="tg2-0lax" rowspan="2"><b>No</b></th>
                <th class="tg2-0lax" rowspan="2"><b>Model</b></th>
                <th class="tg2-0lax" rowspan="2"><b>Part Code</b></th>
                <th class="tg2-0lax" rowspan="2"><b>JOB / DO</b></th>
                <th class="tg2-0lax" rowspan="2"><b>PROCESS / LOT NO</b></th>
                <th class="tg2-0lax" colspan="{{count($processmstr) + 2}}"><b>Proses</b></th>
            </tr>
            <tr class="tg-0lax">
                @foreach($processmstr as $key => $val)
                <th class="tg-0lax">{{$val['RPDSGN_CODE']}}</th>
                @endforeach
                <th class="tg-0lax">Part Level</th>
                <th class="tg-0lax">FG Return</th>
            </tr>
        </thead>
        <tbody>
            <?php $totalPro = [];
            $totalPart = $totalFGRet = 0; ?>
            @foreach($data as $keyData => $valData)
            <tr>
                <td class="tg-0lax">{{$keyData + 1}}</td>
                <td class="tg-0lax">{{$valData['item_det']['MITM_ITMD1']}}</td>
                <td class="tg-0lax">{{$valData['ITEMNUM']}}</td>
                <td class="tg-0lax">{{$valData['DOC_NO']}}</td>
                <td class="tg-0lax">{{$valData['SCR_PROCD']}}</td>
                @if($valData['MDL_FLAG'] == true && $valData['IS_RETURN'] == 0)
                    @foreach($processmstr as $keypro => $val)
                    <?php
                    // $cekArr = array_values(array_filter($valData['hist_det'], function ($fil) use ($val) {
                    //     return $val['RPDSGN_CODE'] == $fil['design_map']['RPDSGN_PRO_ID'];
                    // }));

                    $totalPerProcess = $val['RPDSGN_CODE'] != $valData['design_map']['RPDSGN_PRO_ID'] ? 0 : $valData['QTY'];
                    $totalPro[$keypro] = isset($totalPro[$keypro]) ? $totalPro[$keypro] + $totalPerProcess : $totalPerProcess;
                    ?>
                    <td class="tg-0lax">{{$totalPerProcess === 0 ? '-' : $totalPerProcess}}</td>
                    @endforeach
                    <td class="tg-0lax">-</td>
                @else
                    @if($valData['MDL_FLAG'] == 0)
                        @foreach($processmstr as $key => $val)
                        <td class="tg-0lax">-</td>
                        @endforeach
                        <?php
                            $totalPart += $valData['QTY'];
                        ?>
                        <td class="tg-0lax">{{$valData['QTY'] === 0 ? '-' : $valData['QTY']}}</td>
                        <td class="tg-0lax">-</td>
                    @else
                        @foreach($processmstr as $key => $val)
                        <td class="tg-0lax">-</td>
                        @endforeach
                        <td class="tg-0lax">-</td>
                        <?php
                            $totalFGRet += $valData['QTY'];
                        ?>
                        <td class="tg-0lax">{{$valData['QTY'] === 0 ? '-' : $valData['QTY']}}</td>
                        <td class="tg-0lax">-</td>
                    @endif
                @endif
            </tr>
            @endforeach
            <tr>
                <td class="tg-0lax" colspan="5">Total</td>
                @foreach($processmstr as $keypro2 => $valpro2)
                <td class="tg-0lax">{{isset($totalPro[$keypro2]) ? ($totalPro[$keypro2] === 0 ? '-' : $totalPro[$keypro2]) : '-'}}</td>
                @endforeach
                <td class="tg-0lax">{{$totalPart === 0 ? '-' : $totalPart}}</td>
                <td class="tg-0lax">{{$totalFGRet === 0 ? '-' : $totalFGRet}}</td>
            </tr>
        </tbody>
    </table>
</div>

</html>
