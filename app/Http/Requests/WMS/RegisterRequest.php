<?php

namespace App\Http\Requests\WMS;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|unique:MSTEMP_TBL,MSTEMP_ID',
            'fname' => 'required',
            'nik' => 'required',
            'password' => 'required|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'username.required' => 'Username is required!!',
            'username.unique' => 'Username already used!!',
            'fname.required' => 'First Name is required!!',
            'nik.required' => 'NIK is required!!',
            'password.required' => 'Password is required!!',
            'password.confirmed' => 'Your password is mismatch!!'
        ];
    }
}
