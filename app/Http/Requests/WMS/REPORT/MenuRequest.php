<?php

namespace App\Http\Requests\WMS\REPORT;

use Illuminate\Foundation\Http\FormRequest;

class MenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'menu_id' => 'required',
            'menu_nm' => 'required',
            'menu_desc' => 'required',
            'menu_parent' => 'required',
            'menu_icon' => 'required',
        ];
    }
}
