<?php

namespace App\Http\Requests\DesignProcess;

use Illuminate\Foundation\Http\FormRequest;

class DesignProcessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            return [
                'PROCMSTR_PROCD' => 'required',
                'PROCMSTR_MFGCD' => 'required'
            ];
        } else {
            return [
                'PROCMSTR_PROCD' => 'required',
                'PROCMSTR_MFGCD' => 'required'
            ];
        }
    }
}
