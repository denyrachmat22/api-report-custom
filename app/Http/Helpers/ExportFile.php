<?php
namespace App\Http\Helpers;

use Excel;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
/**
 * Traits terkait export ke excell, pdf, dll
 */
trait ExportFile
{
    use Exportable;
    /**
     * Fungsi untuk export excell
     *
     * @param  \App\WMS\REPORT\MutasiStok  $mutasiStok
     * @return \Illuminate\Http\Response
     */
    public function ExportExcell($data, $filename)
    {
        return ($data::query())->download('invoices.xlsx');
        // Excel::download($filename, function ($excel) {
        //     // Set the title
        //     $excel->setTitle('Our new awesome title');

        //     // Chain the setters
        //     $excel->setCreator('Maatwebsite')
        //         ->setCompany('Maatwebsite');

        //     // Call them separately
        //     $excel->setDescription('A demonstration to change the file properties');
        // });
    }
}
