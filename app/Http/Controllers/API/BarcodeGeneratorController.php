<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DNS1D;
use DNS2D;
use Illuminate\Support\Facades\DB;
use App\Model\MASTER\MEGAEMS\pis3Table;
use App\Model\MASTER\MEGAEMS\ppsn1Table;
use App\Model\MASTER\MEGAEMS\ppsn2Table;
use App\Helpers\CustomDBConnection;
use App\Model\WMS\API\SPLMaster;
use App\Helpers\CustomFunctionHelper;
use Illuminate\Support\Facades\Log;

class BarcodeGeneratorController extends Controller
{
    public function getbarcodePDF417($val)
    {
        echo '<img src="data:image/png;base64,' . DNS2D::getBarcodePNG($val, "PDF417") . '" alt="barcode"   />';
    }

    public function getbarcodeQRCODE($val)
    {
        echo '<img src="data:image/png;base64,' . DNS2D::getBarcodePNG($val, "QRCODE") . '" alt="barcode"   />';
    }

    public function test()
    {
        $select = [
            'PPSN1_PSNNO', //SPL_DOC
            'PPSN2_ITMCAT', //SPL_CAT
            'PPSN1_LINENO', //SPL_LINE
            'PPSN1_FR', //SPL_FEDR
            // 'PPSN1_WONO', //SPL_JOBNO
            'PIS3_WONO', //SPL_JOBNO
            'PPSN1_MDLCD', //SPL_FG
            // 'PPSN2_MCZ', //SPL_ORDERNO
            'PIS3_MCZ', //SPL_ORDERNO
            'ITMLOC_LOC', //SPL_RACKNO
            // 'PPSN2_SUBPN', //SPL_ITMCOD
            'PIS3_ITMCD', //SPL_ITMCOD
            'PPSN2_QTPER', //SPL_QTYUSE
            // 'PIS2_QTPER', //SPL_QTYUSE
            // 'PPSN2_REQQT', //SPL_QTYREQ
            'PIS3_REQQT', //SPL_QTYREQ
            'PPSN2_MSFLG', //SPL_MS
            'PPSN1_SIMQT', //SPL_LOTSZ
            //'PPSN1_SIMQT', //SPL_LOTSZ
            'PDPP_CUSCD', //SPL_CUSTCD
        ];

        $data = ppsn1Table::select(array_merge(
            $select,
            [
                DB::raw('(
                SELECT COUNT(*) FROM SRVMEGA.PSI_MEGAEMS.dbo.PPSN2_TBL A 
                WHERE A.PPSN2_PSNNO = PPSN1_TBL.PPSN1_PSNNO
                and A.PPSN2_LINENO = PPSN1_TBL.PPSN1_LINENO
                and A.PPSN2_FR = PPSN1_TBL.PPSN1_FR
                and A.PPSN2_MCZ = PIS3_TBL.PIS3_MCZ
            ) AS BLABAR '),
            ]
        ))
            ->join('SRVMEGA.PSI_MEGAEMS.dbo.PPSN2_TBL', function ($q1) {
                $q1->on('PPSN1_PSNNO', 'PPSN2_PSNNO');
                $q1->on('PPSN1_LINENO', 'PPSN2_LINENO');
                $q1->on('PPSN1_FR', 'PPSN2_FR');
                $q1->on('PPSN1_DOCNO', 'PPSN2_DOCNO');
                $q1->on('PPSN1_BSGRP', 'PPSN2_BSGRP');
            })
            ->join('SRVMEGA.PSI_MEGAEMS.dbo.PDPP_TBL', function ($q3) {
                $q3->on('PDPP_WONO', 'PPSN1_WONO');
            })
            ->join('SRVMEGA.PSI_MEGAEMS.dbo.PIS3_TBL', function ($q5) {
                $q5->on('PIS3_DOCNO', 'PPSN2_DOCNO');
                $q5->on('PIS3_MCZ', 'PPSN2_MCZ');
                $q5->on('PIS3_PROCD', 'PPSN2_PROCD');
                $q5->on('PIS3_LINENO', 'PPSN1_LINENO');
                //$q5->on('PIS3_WONO', 'PPSN1_WONO');
                $q5->on('PIS3_BSGRP', 'PPSN1_BSGRP');
                $q5->on('PIS3_ITMCD', 'PPSN2_SUBPN');
                $q5->on('PIS3_MC', 'PPSN2_MC');
            })
            ->leftjoin('SRVMEGA.PSI_MEGAEMS.dbo.PIS2_TBL', function ($q6) {
                $q6->on('PIS2_DOCNO', 'PIS3_DOCNO');
                $q6->on('PIS2_PROCD', 'PIS3_PROCD');
                $q6->on('PIS2_LINENO', 'PIS3_LINENO');
                $q6->on('PIS2_WONO', 'PPSN1_WONO');
                $q6->on('PIS2_BSGRP', 'PIS3_BSGRP');
                $q6->on('PIS2_MCZ', 'PIS3_MCZ');
                $q6->on('PIS2_MC', 'PPSN2_MC');
                $q6->on('PIS2_MDLCD', 'PPSN1_MDLCD');
                $q6->on('PIS2_ITMCD', 'PIS3_ITMCD');
            })
            // ->join('SRVMEGA.PSI_MEGAEMS.dbo.MITM_EXCEL_EPSON', function ($q4) {
            //     $q4->on('ITMCD', 'PPSN2_SUBPN');
            // })
            ->leftjoin('ITMLOC_TBL', 'ITMLOC_ITM', 'PPSN2_SUBPN')
            ->where('PPSN1_PSNNO', '=', 'SP-IEI-2020-04-0161')
            //->where('PPSN2_ITMCAT', 'CHIP')
            //->where('PPSN1_LINENO', 'SMT-A1')
            //->where('PPSN1_FR', 'F')
            // ->where('PIS3_ITMCD', '209139900')
            // bATAS SUCI
            // ->where('PPSN1_PSNNO', '=', 'SP-MAT-2019-12-0053')
            // ->where('PPSN2_ITMCAT','HW')
            // ->where('PPSN1_LINENO','SMT-A3')
            // ->where('PPSN1_FR','R')
            // ->where('PIS3_SHORT', '>', 0)
            // ->where('PPSN1_WONO','19-ZA03-218412501')
            //->where('PIS3_MCZ', 'TBL1-05L')
            // ->where('ITMLOC_LOC','LCD4156')
            // ->where('PIS3_ITMCD','218728100')
            // ->where('PPSN2_MSFLG','M')
            // ->where('RAKNO','Z021C')
            //->where('PPSN1_ISUDT', '2019-12-03')
            // ->orderby('PPSN2_MCZ')
            ->orderBy('PIS3_MCZ')
            ->groupby($select)
            ->get();

        // return $data;

        $cek = CustomFunctionHelper::CheckSchedullerCount('TarikDatakeTableSPL', $data->count());

        foreach ($data as $key => $value) {
            $arraynya = [
                'SPL_DOC' => $value['PPSN1_PSNNO'],
                'SPL_CAT' => $value['PPSN2_ITMCAT'],
                'SPL_LINE' => $value['PPSN1_LINENO'],
                'SPL_FEDR' => $value['PPSN1_FR'],
                // 'SPL_JOBNO' => $value['PPSN1_WONO'],
                'SPL_JOBNO' => $value['PIS3_WONO'],
                'SPL_FG' => $value['PPSN1_MDLCD'],
                // 'SPL_ORDERNO' => $value['PPSN2_MCZ'],
                'SPL_ORDERNO' => $value['PIS3_MCZ'],
                'SPL_RACKNO' => $value['ITMLOC_LOC'],
                // 'SPL_ITMCD' => $value['PPSN2_SUBPN'],
                'SPL_ITMCD' => $value['PIS3_ITMCD'],
                // 'SPL_QTYUSE' => $value['PPSN2_QTPER'],
                'SPL_QTYUSE' => $value['PPSN2_QTPER'],
                'SPL_QTYREQ' => $value['BLABAR'] > 1 ? $value['PIS3_REQQT'] * $value['PPSN2_QTPER'] : $value['PIS3_REQQT'],
                'SPL_MS' => $value['PPSN2_MSFLG'],
                'SPL_LOTSZ' => $value['PPSN1_SIMQT'],
                'SPL_CUSCD' => $value['PDPP_CUSCD'],
                'SPL_LUPDT' => date('Y-m-d h:i:s'),
                'SPL_USRID' => 'SCD',
            ];

            $cekrecord = SPLMaster::where('SPL_DOC', $value['PPSN1_PSNNO'])
                ->where('SPL_CAT', $value['PPSN2_ITMCAT'])
                ->where('SPL_LINE', $value['PPSN1_LINENO'])
                ->where('SPL_FEDR', $value['PPSN1_FR'])
                ->where('SPL_JOBNO', $value['PIS3_WONO'])
                ->where('SPL_ORDERNO', $value['PIS3_MCZ'])
                ->where('SPL_ITMCD', $value['PIS3_ITMCD'])
                ->where('SPL_QTYUSE', $value['PPSN2_QTPER'])
                ->first();

            if (!empty($cekrecord)) {
                $cekrecord->delete();
            }

            SPLMaster::updateOrCreate([
                'SPL_DOC' => $value['PPSN1_PSNNO'],
                'SPL_CAT' => $value['PPSN2_ITMCAT'],
                'SPL_LINE' => $value['PPSN1_LINENO'],
                'SPL_FEDR' => $value['PPSN1_FR'],
                // 'SPL_JOBNO' => $value['PPSN1_WONO'],
                'SPL_JOBNO' => $value['PIS3_WONO'],
                // 'SPL_ORDERNO' => $value['PPSN2_MCZ'],
                'SPL_ORDERNO' => $value['PIS3_MCZ'],
                // 'SPL_ITMCD' => $value['PPSN2_SUBPN'],
                'SPL_ITMCD' => $value['PIS3_ITMCD'],
                'SPL_QTYUSE' => $value['PPSN2_QTPER'],
            ], $arraynya);
        }

        return $data;
    }

    public function test2()
    {
        $select = [
            'PPSN2_DOCNO',
            'PPSN1_PSNNO', //SPL_DOC
            'PPSN2_ITMCAT', //SPL_CAT
            'PPSN1_LINENO', //SPL_LINE
            'PPSN1_FR', //SPL_FEDR
            'PPSN1_WONO', //SPL_JOBNO
            'PPSN1_MDLCD', //SPL_FG
            'PIS3_REQQT', //SPL_QTYREQ
            'PIS3_MCZ', //SPL_ORDERNO
            'PPSN1_SIMQT', //SPL_QTYREQ
            'PPSN2_MCZ', //SPL_ORDERNO
            'ITMLOC_LOC', //SPL_RACKNO
            'PPSN2_SUBPN', //SPL_ITMCOD
            'PPSN2_QTPER', //SPL_QTYUSE
            'PPSN2_MSFLG', //SPL_MS
            'PPSN1_SIMQT', //SPL_LOTSZ
            'PDPP_CUSCD', //SPL_CUSTCD
            'PPSN2_PSNQT',
            'PPSN2_REQQT',
            'PPSNA_NREQQT',
            'MSIM_SUBPN'
        ];

        $data = ppsn1Table::select(array_merge(
            $select,
            [
                DB::raw('(SELECT COUNT(*) 
                        FROM [SRVMEGA].[PSI_MEGAEMS].[dbo].[PIS3_TBL] 
                        WHERE PIS3_DOCNO = PPSN2_DOCNO
                        AND PIS3_MCZ = PPSN2_MCZ
                        AND PIS3_ITMCD = PPSN2_SUBPN
                    ) AS NAONSIH')
            ]
        ))
            ->join('SRVMEGA.PSI_MEGAEMS.dbo.PPSN2_TBL', function ($q1) {
                $q1->on('PPSN1_PSNNO', 'PPSN2_PSNNO');
                $q1->on('PPSN1_LINENO', 'PPSN2_LINENO');
                $q1->on('PPSN1_FR', 'PPSN2_FR');
                $q1->on('PPSN1_DOCNO', 'PPSN2_DOCNO');
                $q1->on('PPSN1_BSGRP', 'PPSN2_BSGRP');
            })
            ->join('SRVMEGA.PSI_MEGAEMS.dbo.PDPP_TBL', function ($q3) {
                $q3->on('PDPP_WONO', 'PPSN1_WONO');
            })
            ->leftjoin('SRVMEGA.PSI_MEGAEMS.dbo.PIS3_TBL', function ($q5) {
                $q5->on('PIS3_DOCNO', 'PPSN2_DOCNO');
                $q5->on('PIS3_MCZ', 'PPSN2_MCZ');
                $q5->on('PIS3_PROCD', 'PPSN2_PROCD');
                $q5->on('PIS3_LINENO', 'PPSN1_LINENO');
                $q5->on('PIS3_WONO', 'PPSN1_WONO');
                $q5->on('PIS3_BSGRP', 'PPSN1_BSGRP');
                $q5->on('PIS3_ITMCD', 'PPSN2_SUBPN');
                // $q5->on('PIS3_MC', 'PPSN2_MC');
            })
            ->leftjoin('ITMLOC_TBL', 'ITMLOC_ITM', 'PPSN2_SUBPN')
            ->leftjoin('SRVMEGA.PSI_MEGAEMS.dbo.MSIM_TBL', function ($q6) {
                $q6->on('MSIM_MDLCD', 'PPSN1_MDLCD');
                $q6->on('MSIM_SUBPN', 'PPSN2_SUBPN');
            })
            ->leftjoin('SRVMEGA.PSI_MEGAEMS.dbo.PPSNA_TBL', function ($q7) {
                $q7->on('PPSNA_PSNNO', 'PPSN1_PSNNO');
                $q7->on('PPSNA_LINENO', 'PPSN1_LINENO');
                $q7->on('PPSNA_FR', 'PPSN1_FR');
                $q7->on('PPSNA_LINENO', 'PPSN1_LINENO');
                $q7->on('PPSNA_MCZ', 'PPSN2_MCZ');
                $q7->on('PPSNA_SUBPN', 'PPSNA_SUBPN');
            })
            ->where('PPSN1_PSNNO', '=', 'SP-IEI-2020-04-0161')
            // ->where('PIS3_ITMCD', '209139900')
            // bATAS SUCI
            // ->where('PPSN1_PSNNO', '=', 'SP-MAT-2019-12-0053')
            // ->where('PPSN2_ITMCAT','HW')
            // ->where('PPSN1_LINENO','SMT-A3')
            // ->where('PPSN1_FR','R')
            // ->where('PIS3_SHORT', '>', 0)
            // ->where('PPSN1_WONO','19-ZA03-218412501')
            // ->where('PPSN2_MCZ', 'TBL1-03R')
            // ->where('ITMLOC_LOC','LCD4156')
            // ->where('PIS3_ITMCD','218728100')
            // ->where('PPSN2_MSFLG','M')
            // ->where('RAKNO','Z021C')
            //->where('PPSN1_ISUDT', '2019-12-03')
            // ->orderby('PPSN2_MCZ')
            ->orderBy('PPSN2_MCZ')
            ->groupby($select)
            ->get();

        // return $data;

        foreach ($data as $key => $value) {
            $totalreqperwo = empty($value['PIS3_REQQT']) ? $value['PPSN2_QTPER'] : $value['PIS3_REQQT'] / $value['PPSN1_SIMQT'];

            $arraynya = [
                'SPL_DOC' => $value['PPSN1_PSNNO'],
                'SPL_CAT' => $value['PPSN2_ITMCAT'],
                'SPL_LINE' => $value['PPSN1_LINENO'],
                'SPL_FEDR' => $value['PPSN1_FR'],
                // 'SPL_JOBNO' => $value['PPSN1_WONO'],
                'SPL_JOBNO' => $value['PPSN1_WONO'],
                'SPL_FG' => $value['PPSN1_MDLCD'],
                // 'SPL_ORDERNO' => $value['PPSN2_MCZ'],
                'SPL_ORDERNO' => $value['PPSN2_MCZ'],
                'SPL_RACKNO' => $value['ITMLOC_LOC'],
                // 'SPL_ITMCD' => $value['PPSN2_SUBPN'],
                'SPL_ITMCD' => $value['PPSN2_SUBPN'],
                // 'SPL_QTYUSE' => $value['PPSN2_QTPER'],
                'SPL_QTYUSE' => $totalreqperwo,
                // 'SPL_QTYREQ' => empty($value['PIS3_REQQT'])
                //     ? ($value['PPSN2_PSNQT'] !== $value['PPSN2_REQQT']
                //         ? ($value['NAONSIH'] == 0
                //             ? $value['PPSN1_SIMQT'] * $value['PPSN2_QTPER']
                //             : (empty($value['PPSNA_NREQQT'])
                //                 ? 0
                //                 : $value['PPSN1_SIMQT'] * $value['PPSN2_QTPER']))
                //         : ($value['NAONSIH'] == 0
                //             ? $value['PPSN1_SIMQT'] * $value['PPSN2_QTPER']
                //             : 0))
                //     : ($value['PPSN2_PSNQT'] == $value['PPSN2_REQQT']
                //         ? $value['PIS3_REQQT']
                //         : $value['PPSN1_SIMQT'] * $value['PPSN2_QTPER']),
                'SPL_QTYREQ' => 
                    (empty($value['PPSNA_NREQQT']))
                    ? ((empty($value['PIS3_REQQT']))
                      ? (($value['NAONSIH'] > 0) 
                        ? 0
                        : $value['PPSN1_SIMQT'] * $value['PPSN2_QTPER']
                        )
                      : $value['PIS3_REQQT']
                      )
                    : ((empty($value['PIS3_REQQT'])) 
                      ? (($value['NAONSIH'] > 0) 
                        ? 0
                        : $value['PPSN1_SIMQT'] * $value['PPSN2_QTPER']
                        )
                      : (is_float($value['PIS3_REQQT'] / $value['PPSN1_SIMQT'])
                        ? $value['PPSN1_SIMQT'] * $value['PPSN2_QTPER']
                        : $value['PIS3_REQQT']
                        )
                      ),
                'SPL_MS' => $value['PPSN2_MSFLG'],
                'SPL_LOTSZ' => $value['PPSN1_SIMQT'],
                'SPL_CUSCD' => $value['PDPP_CUSCD'],
                'SPL_LUPDT' => date('Y-m-d h:i:s'),
                'SPL_USRID' => 'SCD',
            ];

            $cekrecord = SPLMaster::where('SPL_DOC', $value['PPSN1_PSNNO'])
                ->where('SPL_CAT', $value['PPSN2_ITMCAT'])
                ->where('SPL_LINE', $value['PPSN1_LINENO'])
                ->where('SPL_FEDR', $value['PPSN1_FR'])
                ->where('SPL_JOBNO', $value['PPSN1_WONO'])
                ->where('SPL_ORDERNO', $value['PPSN2_MCZ'])
                ->where('SPL_ITMCD', $value['PPSN2_SUBPN'])
                // ->where('SPL_QTYUSE', $value['PPSN2_QTPER'])
                ->first();

            if (!empty($cekrecord)) {
                $cekrecord->delete();
            }

            SPLMaster::updateOrCreate([
                'SPL_DOC' => $value['PPSN1_PSNNO'],
                'SPL_CAT' => $value['PPSN2_ITMCAT'],
                'SPL_LINE' => $value['PPSN1_LINENO'],
                'SPL_FEDR' => $value['PPSN1_FR'],
                // 'SPL_JOBNO' => $value['PPSN1_WONO'],
                'SPL_JOBNO' => $value['PPSN1_WONO'],
                // 'SPL_ORDERNO' => $value['PPSN2_MCZ'],
                'SPL_ORDERNO' => $value['PPSN2_MCZ'],
                // 'SPL_ITMCD' => $value['PPSN2_SUBPN'],
                'SPL_ITMCD' => $value['PPSN2_SUBPN'],
                // 'SPL_QTYUSE' => $value['PPSN2_QTPER'],
            ], $arraynya);
        }

        return $data;
    }

    public function test3()
    {
        return 'tes';
    }
}
