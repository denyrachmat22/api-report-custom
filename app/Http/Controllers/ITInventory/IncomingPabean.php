<?php

namespace App\Http\Controllers\ITInventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\WMS\REPORT\MutasiStok;
use Illuminate\Support\Facades\DB;
use App\Model\RPCUST\MutasiDokPabean;
use App\Jobs\ITInventory\IncomingPabean as IncomingPabeanJobs;
use App\Model\RPCUST\DetailStock;

class IncomingPabean extends Controller
{
    // Incoming Pabean, Incoming Raw Material, Incoming FG From Customer, Incoming Mesin & peralatan
    public function updateIncomingPabean($jobs, $item = '')
    {
        $insertJob = (new IncomingPabeanJobs($jobs, $item));

        dispatch($insertJob)->onQueue('IncomingStock');

        return 'Request Mutasi Pabean Masuk Queued';
    }

    public function deleteDO($do, $aju = '')
    {
        $deleteEXBC = DetailStock::where('RPSTOCK_DOC', base64_decode($do))->where('RPSTOCK_TYPE', 'INC');

        if (!empty($aju)) {
            $deleteEXBC->where('RPSTOCK_BCNUM', base64_decode($aju));
        }

        $actionDelete = $deleteEXBC->delete();
        if ($actionDelete) {
            $delete = MutasiDokPabean::where('RPBEA_NUMSJL', base64_decode($do));

            if (!empty($aju)) {
                $delete->where('RPBEA_NUMBEA', base64_decode($aju));
            }

            $actionDeleteITInv = $delete->delete();
            return [
                'status' => true,
                'EXBC' => 'EX-BC Berhasil di hapus',
                'it_inventory' => $actionDeleteITInv ? 'IT Inventory Berhasil di hapus' : 'IT Inventory Gagal di hapus'
            ];
        } else {
            return [
                'status' => true,
                'EXBC' => 'EX-BC Gagal di hapus',
                'it_inventory' => 'IT Inventory Gagal di hapus'
            ];
        }
    }

    public function pushByDoArray(Request $req)
    {
        $dataDO = $req->data;

        // return $dataDO;

        $hasil = [];
        foreach ($dataDO as $key => $value) {
            $hasil[] = $this->updateIncomingPabean(base64_encode($value));
        }

        return $hasil;
    }

    public function pushAllData()
    {
        ini_set('max_execution_time', 21600);
        $headerData = MutasiStok::leftjoin('PSI_WMS.dbo.MITM_TBL', 'RCV_ITMCD', 'MITM_ITMCD')->get();

        $hasil = [];
        foreach ($headerData as $key => $value) {
            $hasil[] = $this->updateIncomingPabean($value['RCV_DONO'], $value['RCV_ITMCD']);
        }

        return $hasil;
    }

    public function getDataByDate(Request $req)
    {
        ini_set('max_execution_time', 21600);
        $headerData = MutasiStok::select('RCV_DONO', 'RCV_ITMCD')
            ->whereBetween('RCV_BCDATE', [$req->first_date, $req->last_date])
            ->leftjoin('PSI_WMS.dbo.MITM_TBL', 'RCV_ITMCD', 'MITM_ITMCD')
            ->get();

        // return $headerData;

        $hasil = [];
        foreach ($headerData as $key => $value) {
            $hasil[] = array_merge([$value], [
                'queue' => $this->updateIncomingPabean(base64_encode($value['RCV_DONO']), base64_encode($value['RCV_ITMCD']))
            ]);
        }

        return $hasil;
    }

    public function cekDataDO(Request $req)
    {
        $dataDO = $req->data;

        // return $dataDO;

        $headerData = MutasiStok::select(
            'RCV_BCTYPE',
            'RCV_BCNO',
            'RCV_BCDATE',
            'RCV_DONO',
            'RCV_INVDATE',
            'RCV_RPNO',
            'RCV_RPDATE',
            'RCV_SUPCD',
            'RCV_ITMCD',
            DB::raw('ROUND(RCV_PRPRC, 3, 1) as RCV_PRPRC'),
            'MITM_STKUOM',
            DB::raw('MSUP_SUPCR'),
            DB::raw('SUM(RCV_QTY) as RCV_QTY'),
            'RCV_RCVDATE',
            'RCV_DONO'
        )
            ->join('PSI_WMS.dbo.MITM_TBL', 'RCV_ITMCD', 'MITM_ITMCD')
            ->leftjoin('PSI_WMS.dbo.XMSUP', 'RCV_SUPCD', 'MSUP_SUPCD');

        $headerData->whereIn('RCV_DONO', $dataDO);

        $getData = $headerData->groupBy(
            'RCV_BCTYPE',
            'RCV_BCNO',
            'RCV_BCDATE',
            'RCV_DONO',
            'RCV_INVDATE',
            'RCV_RPNO',
            'RCV_RPDATE',
            'RCV_SUPCD',
            'RCV_ITMCD',
            DB::raw('ROUND(RCV_PRPRC, 3, 1)'),
            'MITM_STKUOM',
            'MSUP_SUPCR',
            'RCV_RCVDATE',
            'RCV_DONO'
        )->get();

        return $getData;
    }
}
