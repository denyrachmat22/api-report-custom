<?php

namespace App\Http\Controllers\Scheduller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Exports\fourMDetailList;
use App\Jobs\fourMMailJobs;
use Illuminate\Support\Facades\Storage;

class FourMController extends Controller
{
    public function index()
    {

        $filename = 'bom_mega_vs_cims_'.date('Ymd').'.xlsx';
        // return Storage::disk('public')->get($filename);
        $searchData = DB::select(DB::raw("SELECT * FROM v_bom_mega_vs_cims WHERE REV_CIMS <> REV_MEGA AND KITTING_STATUS = 'YES'"));

        $stored = (new fourMDetailList($searchData))->store($filename, 'public');
        if ($stored) {
            $insertJob = (new fourMMailJobs('PT SMT Indonesia', $searchData, $filename));
            dispatch($insertJob)->onQueue('sendEmail');
            return 'Email sent !!';
        }
    }
}
