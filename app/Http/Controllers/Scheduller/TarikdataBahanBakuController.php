<?php

namespace App\Http\Controllers\Scheduller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\RPCUST\MutasiRaw;
use App\Model\WMS\REPORT\MutasiStok;
use Illuminate\Support\Facades\DB;
use App\Model\MASTER\ITHMaster;

class TarikdataBahanBakuController extends Controller
{
    public function GetData()
    {
        $select = [
            'ITH_ITMCD',
            // 'ITH_DATE',
            // DB::raw('SUM(ITH_QTY) as QTY'),
            DB::raw('COUNT(ITH_ITMCD) as ITEM')
        ];

        $cekbyDate = ITHMaster::select($select)->groupBy([
            'ITH_ITMCD',
            'ITH_DATE',
        ])->get();

        // return MutasiStok::select('RCV_ITMCD',DB::raw('COUNT(RCV_ITMCD) as ITEM'))->groupBy('RCV_ITMCD')->get();

        return $cekbyDate;
    }
}
