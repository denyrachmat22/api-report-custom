<?php

namespace App\Http\Controllers\Scheduller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\WMS\API\ViewSPL;
use App\Jobs\TarikDatakeTableSPL;
use App\Model\MASTER\MEGAEMS\ppsn1Table;
use App\Model\WMS\API\SPLMaster;
use Illuminate\Support\Facades\DB;

class TarikDataSPL extends Controller
{
    public function GetData($psn = null, $cat = null, $line = null, $feeder = null)
    {
        $dates = date('Y-m-d');

        $insertJob = (new TarikDatakeTableSPL($dates, $psn, $cat, $line, $feeder));

        dispatch($insertJob);

        return 'Request SPL Queued';
    }

    public function GetDataBydate($date = null)
    {
        if (empty($date)) {
            $dates = date('Y-m-d');
        } else {
            $dates = $date;
        }

        $insertJob = (new TarikDatakeTableSPL($dates));

        dispatch($insertJob);

        return 'Request SPL Queued';
    }

    public function GetDataSPLbySPL($spl, $cat = null, $line = null, $feeder = null, $job = null, $mcz = null)
    {
        $select = [
            'PPSN2_DOCNO',
            'PPSN2_PROCD',
            'PPSN1_PSNNO', //SPL_DOC
            'PPSN2_ITMCAT', //SPL_CAT
            'PPSN1_LINENO', //SPL_LINE
            'PPSN1_FR', //SPL_FEDR
            // 'PPSN1_WONO', //SPL_JOBNO
            'PPSN1_MDLCD', //SPL_FG
            // 'PIS3_REQQT', //SPL_QTYREQ
            // 'PIS3_MCZ', //SPL_ORDERNO
            'PPSN1_SIMQT', //SPL_QTYREQ
            'PPSN2_MCZ', //SPL_ORDERNO
            'PPSN2_MC', //SPL_MC
            'ITMLOC_LOC', //SPL_RACKNO
            'PPSN2_SUBPN', //SPL_ITMCOD
            'PPSN2_QTPER', //SPL_QTYUSE
            'PPSN2_MSFLG', //SPL_MS
            'PPSN1_SIMQT', //SPL_LOTSZ
            'PDPP_CUSCD', //SPL_CUSTCD
            'PPSN2_PSNQT',
            'PPSN2_REQQT',
            'PPSNA_NREQQT',
            'MSIM_SUBPN',
            // 'PIS2_QTPER'
        ];

        $dataCheck = ppsn1Table::select(array_merge(
            $select,
            [
                DB::raw('(SELECT COUNT(*) 
                    FROM [SRVMEGA].[PSI_MEGAEMS].[dbo].[PIS3_TBL] 
                    WHERE PIS3_DOCNO = PPSN2_DOCNO
                    AND PIS3_MCZ = PPSN2_MCZ
                    AND PIS3_ITMCD = PPSN2_SUBPN
                ) AS NAONSIH')
            ]
        ))
            ->join('SRVMEGA.PSI_MEGAEMS.dbo.PPSN2_TBL', function ($q1) {
                $q1->on('PPSN1_PSNNO', 'PPSN2_PSNNO');
                $q1->on('PPSN1_LINENO', 'PPSN2_LINENO');
                $q1->on('PPSN1_FR', 'PPSN2_FR');
                $q1->on('PPSN1_DOCNO', 'PPSN2_DOCNO');
                $q1->on('PPSN1_BSGRP', 'PPSN2_BSGRP');
            })
            ->join('SRVMEGA.PSI_MEGAEMS.dbo.PDPP_TBL', function ($q3) {
                $q3->on('PDPP_WONO', 'PPSN1_WONO');
            })
            ->leftjoin('SRVMEGA.PSI_MEGAEMS.dbo.PIS3_TBL', function ($q5) {
                $q5->on('PIS3_DOCNO', 'PPSN2_DOCNO');
                $q5->on('PIS3_MCZ', 'PPSN2_MCZ');
                $q5->on('PIS3_PROCD', 'PPSN2_PROCD');
                $q5->on('PIS3_LINENO', 'PPSN1_LINENO');
                $q5->on('PIS3_WONO', 'PPSN1_WONO');
                $q5->on('PIS3_BSGRP', 'PPSN1_BSGRP');
                $q5->on('PIS3_ITMCD', 'PPSN2_SUBPN');
                // $q5->on('PIS3_MC', 'PPSN2_MC');
            })
            ->leftjoin('SRVMEGA.PSI_MEGAEMS.dbo.PIS2_TBL', function ($q5) {
                $q5->on('PIS2_DOCNO', 'PPSN2_DOCNO');
                $q5->on('PIS2_MCZ', 'PPSN2_MCZ');
                $q5->on('PIS2_PROCD', 'PPSN2_PROCD');
                $q5->on('PIS2_LINENO', 'PPSN1_LINENO');
                $q5->on('PIS2_WONO', 'PPSN1_WONO');
                $q5->on('PIS2_BSGRP', 'PPSN1_BSGRP');
                $q5->on('PIS2_ITMCD', 'PPSN2_SUBPN');
                // $q5->on('PIS3_MC', 'PPSN2_MC');
            })
            ->leftjoin('ITMLOC_TBL', 'ITMLOC_ITM', 'PPSN2_SUBPN')
            ->leftjoin('SRVMEGA.PSI_MEGAEMS.dbo.MSIM_TBL', function ($q6) {
                $q6->on('MSIM_MDLCD', 'PPSN1_MDLCD');
                $q6->on('MSIM_SUBPN', 'PPSN2_SUBPN');
            })
            ->leftjoin('SRVMEGA.PSI_MEGAEMS.dbo.PPSNA_TBL', function ($q7) {
                $q7->on('PPSNA_PSNNO', 'PPSN1_PSNNO');
                $q7->on('PPSNA_LINENO', 'PPSN1_LINENO');
                $q7->on('PPSNA_FR', 'PPSN1_FR');
                $q7->on('PPSNA_LINENO', 'PPSN1_LINENO');
                $q7->on('PPSNA_MCZ', 'PPSN2_MCZ');
                $q7->on('PPSNA_SUBPN', 'PPSNA_SUBPN');
            });

        $data = $dataCheck->where('PPSN1_PSNNO', $spl)
            ->when(!empty($cat), function ($c) use ($cat){
                $c->where('PPSN2_ITMCAT', $cat);
            })
            ->when(!empty($line), function ($l) use ($line){
                $l->where('PPSN1_LINENO', $line);
            })
            ->when(!empty($feeder), function ($f) use ($feeder){
                $f->where('PPSN1_FR', $feeder);
            })
            ->when(!empty($job), function ($j) use ($job){
                $j->where('PPSN1_WONO', $job);
            })
            ->when(!empty($mcz), function ($m) use ($mcz){
                $m->where('PPSN2_MCZ', $mcz);
            })
            ->orderBy('PPSN2_MCZ')
            ->groupby($select)
            ->get()
            ->toArray();
			
        foreach ($data as $key => $value) {
            // $totalreqperwo = 
            // (empty($value['PIS2_QTPER']))
            //     ? (!isset($value['PIS3_REQQT']))
            //         ? $value['PPSN2_QTPER']
            //         : is_numeric($value['PIS3_REQQT'] / $value['PPSN1_SIMQT']) && $value['PIS3_REQQT'] / $value['PPSN1_SIMQT'] !== 0
            //             ? $value['PIS3_REQQT'] / $value['PPSN1_SIMQT'] == 0
            //                 ? $value['PPSN2_QTPER']
            //                 : $value['PIS3_REQQT'] / $value['PPSN1_SIMQT']
            //             : $value['PPSN2_QTPER']
            //     : $value['PIS2_QTPER'];

            // $totalqtyreq = (empty($value['PIS2_QTPER']))
            //     ? (empty($value['PPSNA_NREQQT']))
            //     ? ((empty($value['PIS3_REQQT']))
            //         ? (($value['NAONSIH'] > 0)
            //             ? 0
            //             : $value['PPSN1_SIMQT'] * $value['PPSN2_QTPER'])
            //         : $value['PIS3_REQQT'])
            //     : ((empty($value['PIS3_REQQT']))
            //         ? (($value['NAONSIH'] > 0)
            //             ? 0
            //             : $value['PPSN1_SIMQT'] * $value['PPSN2_QTPER'])
            //         : (is_float($value['PIS3_REQQT'] / $value['PPSN1_SIMQT'])
            //             ? $value['PPSN1_SIMQT'] * $value['PPSN2_QTPER']
            //             : $value['PIS3_REQQT']))
            //     : $value['PPSN1_SIMQT'] * $value['PIS2_QTPER'];

            $arraynya = [
                'SPL_DOC' => $value['PPSN1_PSNNO'],
                'SPL_DOCNO' => $value['PPSN2_DOCNO'],
                'SPL_CAT' => $value['PPSN2_ITMCAT'],
                'SPL_LINE' => $value['PPSN1_LINENO'],
                'SPL_FEDR' => $value['PPSN1_FR'],
                // 'SPL_JOBNO' => $value['PPSN1_WONO'],
                // 'SPL_JOBNO' => $value['PPSN1_WONO'],
                'SPL_FG' => $value['PPSN1_MDLCD'],
                // 'SPL_ORDERNO' => $value['PPSN2_MCZ'],
                'SPL_ORDERNO' => $value['PPSN2_MCZ'],
                'SPL_MC' => $value['PPSN2_MC'],
                'SPL_RACKNO' => $value['ITMLOC_LOC'],
                // 'SPL_ITMCD' => $value['PPSN2_SUBPN'],
                'SPL_ITMCD' => $value['PPSN2_SUBPN'],
                // 'SPL_QTYUSE' => $value['PPSN2_QTPER'],
                // 'SPL_QTYUSE' => $totalreqperwo,
                // 'SPL_QTYREQ' => $totalqtyreq,
                'SPL_QTYUSE' => $value['PPSN2_QTPER'],
                'SPL_QTYREQ' => $value['PPSN2_REQQT'],
                'SPL_MS' => $value['PPSN2_MSFLG'],
                // 'SPL_LOTSZ' => $value['PPSN1_SIMQT'],
                'SPL_CUSCD' => $value['PDPP_CUSCD'],
                'SPL_PROCD' => $value['PPSN2_PROCD'],
                'SPL_LUPDT' => date('Y-m-d h:i:s'),
                'SPL_USRID' => 'SCD',
            ];

            SPLMaster::where('SPL_DOC',$value['PPSN1_PSNNO'])
                ->where('SPL_CAT', $value['PPSN2_ITMCAT'])
                ->where('SPL_LINE', $value['PPSN1_LINENO'])
                ->where('SPL_FEDR', $value['PPSN1_FR'])
                // ->where('SPL_JOBNO', $value['PPSN1_WONO'])
                ->where('SPL_ORDERNO', $value['PPSN2_MCZ'])
                ->where('SPL_MC', $value['PPSN2_MC'])
                ->orWhere('SPL_MC', NULL)
                ->delete();

            // $cekrecord = SPLMaster::where('SPL_DOC', $value['PPSN1_PSNNO'])
            //     ->where('SPL_CAT', $value['PPSN2_ITMCAT'])
            //     ->where('SPL_LINE', $value['PPSN1_LINENO'])
            //     ->where('SPL_FEDR', $value['PPSN1_FR'])
            //     ->where('SPL_JOBNO', $value['PPSN1_WONO'])
            //     ->where('SPL_ORDERNO', $value['PPSN2_MCZ'])
            //     ->where('SPL_ITMCD', $value['PPSN2_SUBPN'])
            //     // ->where('SPL_QTYUSE', $value['PPSN2_QTPER'])
            //     ->first();

            // if (!empty($cekrecord)) {
            //     $cekrecord->delete();
            // }

            $hasil[] = SPLMaster::updateOrCreate([
                'SPL_DOC' => $value['PPSN1_PSNNO'],
                'SPL_CAT' => $value['PPSN2_ITMCAT'],
                'SPL_LINE' => $value['PPSN1_LINENO'],
                'SPL_FEDR' => $value['PPSN1_FR'],
                // 'SPL_JOBNO' => $value['PPSN1_WONO'],
                // 'SPL_JOBNO' => $value['PPSN1_WONO'],
                // 'SPL_ORDERNO' => $value['PPSN2_MCZ'],
                'SPL_ORDERNO' => $value['PPSN2_MCZ'],
                // 'SPL_ITMCD' => $value['PPSN2_SUBPN'],
                'SPL_ITMCD' => $value['PPSN2_SUBPN'],
                'SPL_MC' => $value['PPSN2_MC'],
                // 'SPL_QTYUSE' => $totalreqperwo,
            ], $arraynya);
        }

        return ['status' => count($hasil) > 0 ? 'Insert Success.' : 'Data not found.', 'data' => $hasil];
    }
}
