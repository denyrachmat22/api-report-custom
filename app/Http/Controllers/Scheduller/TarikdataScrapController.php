<?php

namespace App\Http\Controllers\Scheduller;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Model\MASTER\ITHMaster;
use App\Model\MASTER\SPLMaster;
use App\Model\RPCUST\MutasiScrap;

class TarikdataScrapController extends Controller
{

    public function index($psn, $cat, $line, $feeder, $order, $lot)
    {
        $select = [
            "SPL_DOC",
            "SPL_CAT",
            "SPL_LINE",
            "SPL_FEDR",
            // "SPL_JOBNO",
            "SPL_FG",
            "SPL_ORDERNO",
            "SPL_RACKNO",
            "SPL_ITMCD",
            "SPL_QTYUSE",
            "SPL_QTYREQ",
            'MITM_STKUOM',
            'SPLSCN_ORDERNO',
            // 'SPLSCN_LOTNO',
            'RETSCN_QTYBEF',
            'RETSCN_QTYAFT',
            'RETSCN_LOT'
        ];

        $cek = SPLMaster::select(array_merge(
            $select,
            [
                DB::raw('CAST(RETSCN_LUPDT AS DATE) RETSCN_LUPDT'),
                DB::raw('SUM(SPLSCN_QTY) as SPLSCN_QTY')
            ]
        ))
            ->where("SPL_DOC", $psn)
            ->where("SPL_CAT", $cat)
            ->where("SPL_LINE", $line)
            ->where("SPL_FEDR", $feeder)
            ->where("SPL_ORDERNO", $order)
            ->where("RETSCN_LOT", $lot)
            ->join('SPLSCN_TBL', function ($join) {
                $join->on('SPLSCN_DOC', '=', 'SPL_DOC');
                $join->on('SPLSCN_CAT', '=', 'SPL_CAT');
                $join->on('SPLSCN_LINE', '=', 'SPL_LINE');
                $join->on('SPLSCN_FEDR', '=', 'SPL_FEDR');
                $join->on('SPLSCN_ITMCD', '=', 'SPL_ITMCD');
                $join->on('SPLSCN_ORDERNO', '=', 'SPL_ORDERNO');
            })
            ->leftjoin('RETSCN_TBL', function ($join) {
                $join->on('RETSCN_SPLDOC', '=', 'SPL_DOC');
                $join->on('RETSCN_CAT', '=', 'SPL_CAT');
                $join->on('RETSCN_LINE', '=', 'SPL_LINE');
                $join->on('RETSCN_FEDR', '=', 'SPL_FEDR');
                $join->on('RETSCN_ITMCD', '=', 'SPL_ITMCD');
                $join->on('RETSCN_ORDERNO', '=', 'SPL_ORDERNO');
            })
            ->join('MITM_TBL', 'MITM_ITMCD', 'SPL_ITMCD')
            ->groupBy(array_merge(
                $select,
                [DB::raw('CAST(RETSCN_LUPDT AS DATE)')]
            ))
            ->get()
            ->toArray();

        // return $cek;
        $totalissue = 0;
        $totalissuescan = 0;
        $hasilcek = array_keys($cek);
        $last_key = end($hasilcek);
        foreach ($cek as $key => $value) {
            $totalissue += $value['SPL_QTYREQ'];
            $totalissuescan += $value['SPLSCN_QTY'];

            $combinekey = "$psn|$cat|$line|$feeder|$order|$lot";
            // logger($combinekey);

            $tot = ($key == $last_key) ? ($value['SPLSCN_QTY'] - $totalissue) - $value['RETSCN_QTYAFT'] : $totalissue;

            MutasiScrap::updateOrCreate(
                [
                    'RPREJECT_REF' => $combinekey,
                    'RPREJECT_ITMCOD' => trim($value['SPL_ITMCD'])
                ],
                [
                    'RPREJECT_UNITMS' => trim($value['MITM_STKUOM']),
                    'RPREJECT_QTYTOT' => 0,
                    'RPREJECT_QTYOUT' => 0,
                    'RPREJECT_QTYOPN' => 0,
                    'RPREJECT_QTYINC' => $tot < 0 ? $tot * -1 : $tot,
                    'RPREJECT_QTYADJ' => 0,
                    'RPREJECT_KET' => '',
                    'RPREJECT_ITMCOD' => trim($value['SPL_ITMCD']),
                    'RPREJECT_DATEIS' => $value['RETSCN_LUPDT'],
                    'RPREJECT_REF' => $combinekey,
                ]
            );
        }

        return $totalissue.' - '.$value['SPLSCN_QTY'].' - '.$value['RETSCN_QTYAFT'];
    }

    public function sendscheduller($item)
    {
        $select = [
            'ITH_ITMCD',
            'ITH_DATE',
            'ITH_FORM',
            'ITH_DOC',
            'ITH_WH',
            'MITM_STKUOM'
        ];

        $cekhasil = ITHMaster::select(array_merge($select, [DB::raw('SUM(ITH_QTY) AS ITH_QTY')]))
            ->join('MITM_TBL', 'MITM_ITMCD', 'ITH_ITMCD')
            ->where('ITH_ITMCD', $item)
            ->where('ITH_WH', 'ARWH9SC')
            ->orWhere('ITH_WH', 'AFWH9SC')
            ->groupBy($select)
            ->get()
            ->toArray();

        return $cekhasil;

        foreach ($cekhasil as $key => $value) {
            MutasiScrap::updateOrCreate(
                [
                    'RPREJECT_REF' => $value['ITH_DOC'],
                    'RPREJECT_ITMCOD' => trim($value['ITH_ITMCD'])
                ],
                [
                    'RPREJECT_UNITMS' => trim($value['MITM_STKUOM']),
                    'RPREJECT_QTYTOT' => 0,
                    'RPREJECT_QTYOUT' => (substr($value['ITH_FORM'],0,3) == 'OUT' ? (int)$value['ITH_QTY'] * -1 : 0),
                    'RPREJECT_QTYOPN' => 0,
                    'RPREJECT_QTYINC' => (substr($value['ITH_FORM'],0,3) == 'INC' ? (int)$value['ITH_QTY'] : 0),
                    'RPREJECT_QTYADJ' => 0,
                    'RPREJECT_KET' => '',
                    'RPREJECT_ITMCOD' => trim($value['ITH_ITMCD']),
                    'RPREJECT_DATEIS' => $value['ITH_DATE'],
                    'RPREJECT_REF' => $value['ITH_DOC'],
                ]
            );
        }

        return $cekhasil;
    }
}
