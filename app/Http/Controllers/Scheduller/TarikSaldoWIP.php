<?php

namespace App\Http\Controllers\Scheduller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MASTER\ITHMaster;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Model\RPCUST\SaldoWIP;
use App\Model\RPCUST\WIPDone;
use App\Model\MASTER\SERD2TBL;
use App\Model\WMS\API\SPLSCN;

class TarikSaldoWIP extends Controller
{
    public function masterQuery()
    {
        $select = [
            'ITH_ITMCD',
            'ITH_DATE',
            // 'ITH_DOC',
            // 'ITH_SER',
            // 'ITH_LINE'
        ];

        $masterITH = ITHMaster::select(array_merge($select, [DB::raw('SUM(ITH_QTY) AS ITH_QTY')]))
            // ->whereBetween('ITH_DATE',['2020-01-01', '2020-02-12'])
            // ->with('spl.splscn.returnspl')
            // ->with('serDet')
            // ->has('serDet')
            ->groupBy($select);

        return $masterITH;
    }

    public function index()
    {
        $date = date('Y-m-d');
        // return $this->masterQuery()->get()->toArray();
        $cekwip = $this->masterQuery()
            // ->where(DB::raw('LEFT(ITH_WH,5)'), 'ARPRD')
            ->with('ret.retdet')
            ->whereBetween('ITH_DATE', [date('Y-m-d', strtotime('-10 days')), $date])
            ->where(function ($q) {
                $q->where('ITH_FORM', 'INC-PRD-RM');
                // $q->orwhere('ITH_FORM', 'INC-RET');
            })
            ->get()
            ->toArray();

        $cekfingood = $this->masterQuery()
            // ->where('ITH_DATE', date('Y-m-d'))
            // ->where(DB::raw('LEFT(ITH_WH,5)'), 'ARPRD')
            ->where('ITH_FORM', 'INC-PRD-FG')
            ->get()->toArray();

        // return $cekfingood;
        $hasil = [];
        foreach ($cekwip as $key => $value) {
            $res = [];
            $totfg = 0;
            $totreq = 0;

            // Cek item di finish good query
            $qtuse = '';
            foreach ($cekfingood as $key_det => $value_det) {
                $get = [];
                foreach ($value_det['spl'] as $key_deeper => $value_deeper) {
                    $parsesplmode = explode('|', $value['ITH_DOC']);

                    if (
                        $value_deeper['SPL_DOC'] == $parsesplmode[0] &&
                        $value_deeper['SPL_CAT'] == $parsesplmode[1] &&
                        $value_deeper['SPL_LINE'] == $parsesplmode[2] &&
                        $value_deeper['SPL_FEDR'] == $parsesplmode[3] &&
                        $value_deeper['SPL_ITMCD'] == $value['ITH_ITMCD']
                    ) {
                        $get[trim($value_deeper['SPL_ITMCD'])] = true;
                        $totfg += $value_det['ITH_QTY'];
                        $totreq += $value_deeper['SPL_QTYREQ'];
                        $qtuse = $value_deeper['SPL_QTYUSE'];
                    }
                }

                $res[] = $get;
            }

            $cekdata = array_filter(
                $res,
                function ($c) {
                    if (count($c) > 0)
                        return $c;
                }
            );

            $detret = 0;
            foreach ($value['ret'] as $key_splscan_deeper => $value_splscan_deeper) {
                if ($value['ITH_ITMCD'] == $value_splscan_deeper['ITH_ITMCD']) {
                    $detret += $value_splscan_deeper['ITH_QTY'];
                }
            }

            $hasil[] = [
                'item_code' => $value['ITH_ITMCD'],
                'date' => $value['ITH_DATE'],
                'qty' => $value['ITH_QTY'],
                'status_fingood' => count($cekdata),
                'total_fg' => $totfg * (int) $qtuse,
                'total_return' => $detret,
                'total_req' => $totreq,
                'qty_use' => $qtuse
            ];
        }

        // return $hasil;

        foreach ($hasil as $key_hasil => $value_hasil) {
            $query = SaldoWIP::where('RPWIP_ITMCOD', $value_hasil['item_code'])->first();

            if ($value_hasil['status_fingood'] > 0) {
                if (empty($query)) {
                    SaldoWIP::create([
                        'RPWIP_DATEIS' => $value_hasil['date'],
                        'RPWIP_ITMCOD' => $value_hasil['item_code'],
                        'RPWIP_UNITMS' => NULL,
                        'RPWIP_QTYTOT' => ((int) $value_hasil['qty'] - (int) $value_hasil['total_fg']) - (int) $value_hasil['total_return'],
                    ]);
                } else {
                    $query->update([
                        'RPWIP_DATEIS' => $value_hasil['date'],
                        'RPWIP_ITMCOD' => $value_hasil['item_code'],
                        'RPWIP_UNITMS' => NULL,
                        'RPWIP_QTYTOT' => ((int) $value_hasil['qty'] - (int) $value_hasil['total_fg']) - (int) $value_hasil['total_return'],
                    ]);
                }
            } else {
                if (empty($query)) {
                    SaldoWIP::create([
                        'RPWIP_DATEIS' => $value_hasil['date'],
                        'RPWIP_ITMCOD' => $value_hasil['item_code'],
                        'RPWIP_UNITMS' => NULL,
                        'RPWIP_QTYTOT' => (int) $value_hasil['qty'],
                    ]);
                } else {
                    $query->update([
                        'RPWIP_DATEIS' => $value_hasil['date'],
                        'RPWIP_ITMCOD' => $value_hasil['item_code'],
                        'RPWIP_UNITMS' => NULL,
                        'RPWIP_QTYTOT' => (int) $value_hasil['qty'],
                    ]);
                }
            }
        }

        return $hasil;
    }

    public function wipStockManage(Request $req)
    {
        if ($req->methods === 'INC') {
            $cekSPLSCN = SPLSCN::select(
                'SPLSCN_DOC',
                'SPLSCN_CAT',
                'SPLSCN_LINE',
                'SPLSCN_FEDR',
                'SPLSCN_ITMCD',
                'SPLSCN_LOTNO',
                DB::raw('CAST(SPLSCN_LUPDT AS DATE) AS DATE'),
                DB::raw('SUM(SPLSCN_QTY) AS SPLSCN_QTY')
            )
                ->where('SPLSCN_DOC', $req->PSNNO)
                ->where('SPLSCN_CAT', $req->CAT)
                ->where('SPLSCN_LINE', $req->LINE)
                ->where('SPLSCN_FEDR', $req->FR)
                ->where('SPLSCN_ITMCD', $req->ITMCD)
                ->where('SPLSCN_SAVED', 1)
                ->groupBy(
                    'SPLSCN_DOC',
                    'SPLSCN_CAT',
                    'SPLSCN_LINE',
                    'SPLSCN_FEDR',
                    'SPLSCN_ITMCD',
                    'SPLSCN_LOTNO',
                    DB::raw('CAST(SPLSCN_LUPDT AS DATE)')
                )
                ->get()
                ->toArray();

            $listLot = array_values(array_filter($cekSPLSCN, function ($key) {
                return in_array($key, ['SPLSCN_LOTNO']);
            }, ARRAY_FILTER_USE_KEY));

            SaldoWIP::where('RPWIP_PSN', $req->PSNNO)
                ->where('RPWIP_CAT', $req->CAT)
                ->where('RPWIP_LINE', $req->LINE)
                ->where('RPWIP_FR', $req->FR)
                ->where('RPWIP_ITMCOD', $req->ITMCD)
                ->whereIn('RPWIP_LOT', $listLot)
                ->where('RPWIP_TYPE', 'INC')
                ->delete();

            foreach ($cekSPLSCN as $keyLot => $valueLot) {
                $hasil[] = SaldoWIP::create([
                    'RPWIP_DATEIS' => $valueLot['DATE'],
                    'RPWIP_ITMCOD' => $req->ITMCD,
                    'RPWIP_UNITMS' => NULL,
                    'RPWIP_QTYTOT' => (int) $valueLot['SPLSCN_QTY'],
                    'RPWIP_TYPE' => 'INC',
                    'RPWIP_PSN' => $req->PSNNO,
                    'RPWIP_CAT' => $req->CAT,
                    'RPWIP_LINE' => $req->LINE,
                    'RPWIP_FR' => $req->FR,
                    'RPWIP_LOT' => $valueLot['SPLSCN_LOTNO'],
                ]);
            }

            return 'success';
        }
    }

    public function getData($psn = '', $cat = '', $line = '', $feeder = '')
    {
        set_time_limit(10000);
        $select = [
            'ITH_ITMCD',
            'ITH_DATE',
            'ITH_DOC',
            // 'ITH_SER',
            // 'ITH_LINE'
        ];

        $getOutKittingRMdraft = ITHMaster::select(array_merge($select, [DB::raw('SUM(ITH_QTY) AS ITH_QTY')]))
            ->where('ITH_FORM', 'INC-PRD-RM')
            ->where('ITH_LINE', '<>', NULL);

        if (!empty($psn)) {
            $getOutKittingRMdraft->where('ITH_DOC', 'like', base64_decode($psn).'%');
        } elseif (!empty($cat)) {
            $getOutKittingRMdraft->where('ITH_DOC', 'like', '%'.base64_decode($cat).'%');
        } elseif (!empty($line)) {
            $getOutKittingRMdraft->where('ITH_DOC', 'like', '%'.base64_decode($line).'%');
        } elseif (!empty($feeder)) {
            $getOutKittingRMdraft->where('ITH_DOC', 'like', '%'.base64_decode($feeder));
        }

        $getOutKittingRM = $getOutKittingRMdraft->groupBy($select)
            // ->take(10)
            ->get()
            ->toArray();

        // return $getOutKittingRM;

        $hasil = [];
        foreach ($getOutKittingRM as $key => $value) {
            $parsingPSNDoc = explode('|', $value['ITH_DOC']);

            $cekSPLSCN = SPLSCN::select(
                'SPLSCN_DOC',
                'SPLSCN_CAT',
                'SPLSCN_LINE',
                'SPLSCN_FEDR',
                'SPLSCN_ITMCD',
                'SPLSCN_LOTNO',
                DB::raw('SUM(SPLSCN_QTY) AS SPLSCN_QTY')
            )
                ->where('SPLSCN_DOC', $parsingPSNDoc[0])
                ->where('SPLSCN_CAT', $parsingPSNDoc[1])
                ->where('SPLSCN_LINE', $parsingPSNDoc[2])
                ->where('SPLSCN_FEDR', $parsingPSNDoc[3])
                ->where('SPLSCN_ITMCD', $value['ITH_ITMCD'])
                ->groupBy(
                    'SPLSCN_DOC',
                    'SPLSCN_CAT',
                    'SPLSCN_LINE',
                    'SPLSCN_FEDR',
                    'SPLSCN_ITMCD',
                    'SPLSCN_LOTNO'
                )
                ->get()
                ->toArray();

            $listLot = array_values(array_filter($cekSPLSCN, function ($key) {
                return in_array($key, ['SPLSCN_LOTNO']);
            }, ARRAY_FILTER_USE_KEY));

            SaldoWIP::where('RPWIP_PSN', $parsingPSNDoc[0])
                ->where('RPWIP_CAT', $parsingPSNDoc[1])
                ->where('RPWIP_LINE', $parsingPSNDoc[2])
                ->where('RPWIP_FR', $parsingPSNDoc[3])
                ->where('RPWIP_DATEIS', $value['ITH_DATE'])
                ->where('RPWIP_ITMCOD', $value['ITH_ITMCD'])
                ->whereIn('RPWIP_LOT', $listLot)
                ->where('RPWIP_TYPE', 'INC')
                ->delete();

            foreach ($cekSPLSCN as $keyLot => $valueLot) {
                $hasil[] = SaldoWIP::create([
                    'RPWIP_DATEIS' => $value['ITH_DATE'],
                    'RPWIP_ITMCOD' => $value['ITH_ITMCD'],
                    'RPWIP_UNITMS' => NULL,
                    'RPWIP_QTYTOT' => (int) $valueLot['SPLSCN_QTY'],
                    'RPWIP_TYPE' => 'INC',
                    'RPWIP_PSN' => $parsingPSNDoc[0],
                    'RPWIP_CAT' => $parsingPSNDoc[1],
                    'RPWIP_LINE' => $parsingPSNDoc[2],
                    'RPWIP_FR' => $parsingPSNDoc[3],
                    'RPWIP_LOT' => $valueLot['SPLSCN_LOTNO'],
                ]);
            }
        }

        return 'success';
    }

    public function completeWIP()
    {
        $cekserD2Tbl = SERD2TBL::where('SERD2_MSCANTM', '>', date('Y-12-4 00:00:00'))->get()->toArray();
        // return $cekserD2Tbl;
        $cekData = [];
        foreach ($cekserD2Tbl as $key => $value) {
            $select = [
                'RPWIP_DATEIS',
                'RPWIP_ITMCOD',
                'RPWIP_UNITMS',
                'RPWIP_PSN',
                'RPWIP_CAT',
                'RPWIP_LINE',
                'RPWIP_FR'
            ];

            $cekWIP = SaldoWIP::select(array_merge($select, [DB::raw('SUM(RPWIP_QTYTOT) AS RPWIP_QTYTOT')]))
                ->where('RPWIP_PSN', $value['SERD2_PSNNO'])
                ->where('RPWIP_CAT', $value['SERD2_CAT'])
                ->where('RPWIP_LINE', $value['SERD2_LINENO'])
                ->where('RPWIP_FR', $value['SERD2_FR'])
                ->where('RPWIP_ITMCOD', $value['SERD2_ITMCD'])
                ->orderBy('RPWIP_DATEIS', 'DESC')
                ->groupBy($select)
                ->get()
                ->toArray();

            $cekData[] = $this->fifoStockOut($cekWIP, $value, $value['SERD2_QTY']);
        }

        return $cekData;
    }

    public function fifoStockOut($arr, $arrSerd, $qty)
    {
        $cekCurrentArray = current($arr);
        if (!empty($cekCurrentArray)) {
            $compareStock = $cekCurrentArray['RPWIP_QTYTOT'] - $qty;

            SaldoWIP::updateOrCreate([
                'RPWIP_TYPE' => 'OUT',
                'RPWIP_PSN' => $cekCurrentArray['RPWIP_PSN'],
                'RPWIP_CAT' => $cekCurrentArray['RPWIP_CAT'],
                'RPWIP_LINE' => $cekCurrentArray['RPWIP_LINE'],
                'RPWIP_FR' => $cekCurrentArray['RPWIP_FR'],
                'RPWIP_JOB' => $arrSerd['SERD2_JOB'],
                'RPWIP_LOT' => $arrSerd['SERD2_LOTNO'],
                'RPWIP_SER' => $arrSerd['SERD2_SER']
            ], [
                'RPWIP_DATEIS' => $cekCurrentArray['RPWIP_DATEIS'],
                'RPWIP_ITMCOD' => $cekCurrentArray['RPWIP_ITMCOD'],
                'RPWIP_UNITMS' => $cekCurrentArray['RPWIP_UNITMS'],
                'RPWIP_QTYTOT' => $compareStock <= 0 ? (int) $cekCurrentArray['RPWIP_QTYTOT'] * -1 : $qty * -1,
                'RPWIP_TYPE' => 'OUT',
                'RPWIP_PSN' => $cekCurrentArray['RPWIP_PSN'],
                'RPWIP_CAT' => $cekCurrentArray['RPWIP_CAT'],
                'RPWIP_LINE' => $cekCurrentArray['RPWIP_LINE'],
                'RPWIP_FR' => $cekCurrentArray['RPWIP_FR'],
                'RPWIP_JOB' => $arrSerd['SERD2_JOB'],
                'RPWIP_LOT' => $arrSerd['SERD2_LOTNO'],
                'RPWIP_SER' => $arrSerd['SERD2_SER']
            ]);

            // Jika stok kurang atau nol daripada yang keluar
            if ($compareStock < 0) {
                $totalQty = $qty - $cekCurrentArray['RPWIP_QTYTOT'];
                next($arr);
                return $this->fifoStockOut($arr, $arrSerd, $totalQty);
            } else {
                return $arr;
            }
        } else {
            return $arr;
        }
    }
}
