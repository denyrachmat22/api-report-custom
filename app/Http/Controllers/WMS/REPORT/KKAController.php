<?php

namespace App\Http\Controllers\WMS\REPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use App\Exports\KKAExport;
use App\Exports\KKExportFGFresh;
use App\Exports\KKAExportScrap;
use App\Model\MASTER\ITHMaster;
use App\Model\MASTER\ItemMaster;

class KKAController extends Controller
{
    public function getKKA(Request $r)
    {
        set_time_limit(-1);
        // return $r;
        $nameFile = 'KKA_export_' . $r->mutasi . '_' . date('Y_m_d__His') . '.xlsx';
        if ($r->mutasi === 'bahan_baku') {
            Excel::store(new KKAExport($r->mutasi, $r->fdate, $r->ldate, $r->period, $r->has('item') ? $r->item : ''), $nameFile, 'public');
        } elseif ($r->mutasi === 'finish_good_fresh') {
            Excel::store(new KKExportFGFresh($r->mutasi, $r->fdate, $r->ldate, $r->period, $r->has('item') ? $r->item : ''), $nameFile, 'public');
        } elseif ($r->mutasi === 'finish_good_return') {
            Excel::store(new KKExportFGFresh($r->mutasi, $r->fdate, $r->ldate, $r->period, $r->has('item') ? $r->item : ''), $nameFile, 'public');
        } elseif ($r->mutasi === 'scrap') {
            Excel::store(new KKAExportScrap($r->mutasi, $r->fdate, $r->ldate, $r->period, $r->has('item') ? $r->item : ''), $nameFile, 'public');
        }

        return 'storage/app/public/' . $nameFile;
    }

    public function checkITInventory(Request $req)
    {
        $hasil = DB::table("PSI_RPCUST.dbo.f_it_inventory_checker('" . $req->mutasi . "', '" . $req->date_from . "', '" . $req->date_to . "', '" . $req->item_num . "')")->orderBy('row_num')->get()->toArray();

        $rows = $req->has('pagination') ? $req['pagination']['rowsPerPage'] : 20;
        $page = $req->has('pagination') ? $req['pagination']['page'] : 1;

        // return $hasil;
        $totalCount = count($hasil);

        return clone collect($hasil)->paginate($rows, $totalCount, $page);

        return $hasil;
    }

    public function checkEvent(Request $req)
    {
        $data = DB::table("PSI_RPCUST.dbo.f_int_inventory_det_checker('" . $req->mutasi . "', '" . $req->date_from . "', '" . $req->date_to . "', '" . $req->item_num . "', ".(!empty($req->wh) ? "'".$req->wh."'" : "NULL").", ".(!empty($req->cols) ? "'".$req->cols."'" : "NULL").")")->get();

        return $data;
    }

    public function findItem($item, $model)
    {
        $cek = ItemMaster::where('MITM_ITMCD', 'like', $item.'%')
            ->orwhere('MITM_ITMD1', 'like', $item . '%')
            ->where('MITM_MODEL', $model)
            ->get();

        $hasil = [];
        foreach ($cek as $key => $value) {
            $hasil[] = [
                'label' => trim($value->MITM_ITMCD) . ' - ' . trim($value->MITM_ITMD1),
                'value' => trim($value->MITM_ITMCD)
            ];
        }

        return $hasil;
    }

    public function checkLedger(Request $req)
    {
        $data = DB::table("PSI_RPCUST.dbo.f_it_inventory_checker_ledger('" . $req->mutasi . "', '" . $req->date_from . "', '" . $req->date_to . "', '" . $req->item_num . "', '".$req->wh."')")->get();

        return $data;
    }
}
