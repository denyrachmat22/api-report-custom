<?php

namespace App\Http\Controllers\WMS\REPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\RPCUST\MutasiRaw;
use App\Helpers\CustomFunctionHelper;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MutasiBahanBakuExport;
use Illuminate\Support\Facades\DB;
use App\Model\MASTER\ItemMaster;
use App\Model\MASTER\ITHMaster;
use Illuminate\Support\Facades\Log;

class MutasiBahanBakuController extends Controller
{
    public function index()
    {
        $select = [
            'MITM_ITMCD',
            'MITM_ITMD1',
            'MITM_ITMD2',
            'MITM_MODEL',
            'MITM_STKUOM',
            'MITM_SPTNO',
            'ITH_DATE',
            'ITH_WH',
            'ITH_FORM'
        ];

        $select2 = [
            'ITH_ITMCD',
            'ITH_DATE',
            'ITH_DOC'
        ];


        // Cek data finish good
        $cekfingood = ITHMaster::select(array_merge($select2, [DB::raw('SUM(ITH_QTY) AS ITH_QTY')]))
            ->with('dlv.serial.spl')
            ->with(['dlv' => function ($qdlv) {
                $qdlv->select(
                    'DLV_ID',
                    'DLV_CUSTCD',
                    'DLV_SER'
                );
                $qdlv->with(['serial' => function ($qser) {
                    $qser->select(
                        'SER_ID',
                        'SER_DOC',
                        'SER_ITMID',
                        'SER_QTY'
                    );
                    $qser->with('spl');
                }]);
            }])
            ->groupBy($select2)
            ->where('ITH_FORM', 'OUT-SHP-FG')
            ->get()->toArray();

        return $cekfingood;
        //Cek Item material
        $q = ItemMaster::select(
            array_merge(
                $select,
                [DB::raw("SUM(ITH_QTY) as SUMNYA")],
            )
        )
            ->where('MITM_MODEL', 0)
            // ->where(DB::raw('SUBSTRING(ITH_FORM,5,3)'),'<>','PEN')
            // ->where(DB::raw('SUBSTRING(ITH_FORM,5,3)'),'<>','SCR')
            ->join('ITH_TBL', 'ITH_ITMCD', '=', 'MITM_ITMCD')
            ->groupBy($select)
            ->get()->toArray();

        $hasil1 = [];
        foreach ($q as $key => $value) {
            $res = [];
            // Cek item di finish good query
            foreach ($cekfingood as $key_det => $value_det) {
                $get = [];
                foreach ($value_det['dlv'] as $key_deeper => $value_deeper) {
                    if ($value_deeper['dlv']['serial']['spl']['SPL_ITMCD'] == $value['MITM_ITMCD']) {
                        $get[trim($value_deeper['dlv']['serial']['spl']['SPL_ITMCD'])] = true;
                    }
                }

                $res[] = $get;
            }

            $cekdata = array_filter(
                $res,
                function ($c) {
                    if (count($c) > 0)
                        return $c;
                }
            );

            $hasil1[trim($value['ITH_DATE'])][substr($value['ITH_FORM'], 0, 3)][trim($value['MITM_ITMCD'])][] = array_merge($value, ['FINISH_GOOD' => count($cekdata)]);
            // $hasil1[trim($value['ITH_DATE'])]['FINISH_GOOD'] = count($cekdata);
        }
        return $hasil1;
        $this->StoreToRawMutation($hasil1);
    }

    public function StoreToRawMutation($data)
    {
        foreach ($data as $key => $value) { //FOREACH TANGGAL
            if (isset($value['INC'])) { //Jika key INC ada
                foreach ($value['INC'] as $key_inc => $value_inc) { //Foreach by item
                    $totmasuk = 0;
                    foreach ($value_inc as $key_deeper => $value_deeper) {
                        if ($value_deeper['ITH_FORM'] == "INC-DO" || $value_deeper['ITH_FORM'] == "INC-RET") {
                            $totmasuk += $value_deeper['SUMNYA'];
                        }
                        // $totmasuk += $value_deeper['SUMNYA'];
                    }

                    $cekdata = MutasiRaw::where('RPRAW_DATEIS', $key)
                        ->where('RPRAW_ITMCOD', $key_inc)
                        ->first();

                    if (empty($cekdata)) {
                        MutasiRaw::create([
                            'RPRAW_ITMCOD' => $key_inc,
                            'RPRAW_DATEIS' => $key,
                            'RPRAW_KET' => '',
                            'RPRAW_QTYADJ' => 0,
                            'RPRAW_QTYINC' => $totmasuk,
                            'RPRAW_QTYOPN' => 0,
                            'RPRAW_QTYOUT' => 0,
                            'RPRAW_QTYTOT' => $totmasuk,
                            'RPRAW_UNITMS' => $value['INC'][$key_inc][0]['MITM_STKUOM'],
                        ]);
                    } else {
                        $cekdata->update([
                            'RPRAW_QTYINC' => $totmasuk,
                            'RPRAW_QTYTOT' => $totmasuk,
                        ]);
                    }
                }
            }

            if (isset($value['OUT'])) { //Jika key OUT ada
                foreach ($value['OUT'] as $key_out => $value_out) {
                    // Jika sudah ada data finish good
                    // if ($value_inc['FINISH_GOOD'] !== 0) {
                    //     $totkeluar += $value_out['SUMNYA'];
                    // }
                    $totkeluar = 0;
                    foreach ($value_out as $key_deeper => $value_deeper) {
                        if ($value_deeper['ITH_FORM'] == "OUT-WH-RM") {
                            $totkeluar += $value_deeper['SUMNYA'] * -1;
                        }
                    }

                    // Log::info('dari '.$key_out.$totkeluar);

                    $cekdata = MutasiRaw::where('RPRAW_DATEIS', $key)
                        ->where('RPRAW_ITMCOD', $key_out)
                        ->first();

                    if (empty($cekdata)) {
                        MutasiRaw::create([
                            'RPRAW_ITMCOD' => $key_out,
                            'RPRAW_DATEIS' => $key,
                            'RPRAW_KET' => '',
                            'RPRAW_QTYADJ' => 0,
                            'RPRAW_QTYINC' => 0,
                            'RPRAW_QTYOPN' => 0,
                            'RPRAW_QTYOUT' => $totkeluar,
                            'RPRAW_QTYTOT' => $totkeluar * -1,
                            'RPRAW_UNITMS' => $value['INC'][$key_out][0]['MITM_STKUOM'],
                        ]);
                    } else {
                        $cekdata->update([
                            'RPRAW_QTYOUT' => $totkeluar,
                            'RPRAW_QTYTOT' => $cekdata['RPRAW_QTYTOT'] - $totkeluar,
                        ]);
                    }
                }
            }
        }

        return 'berhasil';
    }

    public function show(Request $request, $methode = null)
    {

        $rows = $request->has('pagination') ? $request['pagination']['rowsPerPage'] : 20;
        $page = $request->has('pagination') ? $request['pagination']['page'] : 1;

        $data = $this->data($request->form, $rows, $page, $request->header('userinput'));

        $hasil = [];
        foreach ($data as $key => $value) {
            $hasil[] = array_merge($value, ['no' => $key + 1]);
        }

        $filterData = collect($hasil);

        $rowsNum = $request->has('pagination') ? $request->pagination['rowsNumber'] : $filterData->count();

        // return $filterData;

        if ($methode == 'table') {

            // file_put_contents('logs.txt', $filterData->toSql().json_encode($filterData->getBindings()).PHP_EOL , FILE_APPEND | LOCK_EX);
            if ($rows == 0) {
                $cekall = $filterData->count();
                return $filterData->paginate($cekall);
                return $filterData->paginate($cekall, ['*'], 'page', $page);
            } else {
                // Log::info($filterData->paginate($rows));
                return $filterData->paginate($rows, $rowsNum, $page);
                return $filterData->paginate($rows, ['*'], 'page', $page);
            }
        } elseif ($methode == 'data') {
            return $filterData->all();
        } else {
            // $data = $filterData->get();
            return $this->ExportToExcell($request);
        }
    }

    public function data($filteropt, $rows = null, $page = null, $username = '')
    {
        $queryString = "SET NOCOUNT ON ;EXEC PSI_RPCUST.dbo.sp_mutasi_inv_dev_2 @mutasi = 'bahan_baku', @inc_rcv = 1";

        foreach ($filteropt as $keyFilter => $valueFilter) {
            if ($valueFilter['field']['field'] === 'RPRAW_DATEIS' && !empty($valueFilter['val'])) {
                $queryString .= ", @date_from='".$valueFilter['val']."', @date_to='".date('Y-m-d', strtotime($valueFilter['val2']. '+1 days'))."'";
            }

            if ($valueFilter['field']['field'] === 'RPRAW_ITMCOD' && !empty($valueFilter['val'])) {
                $queryString .= ", @item_num='".$valueFilter['val']."'";
            }
        }

        if (!empty($rows) && !empty($page)) {
            $queryString .= ", @per_page=".$rows.", @page=".$page;
        }

        if ($username === 'smt' || $username === 'testcust') {
            $queryString .= ", @minus_prot = 0";
        } else {
            $queryString .= ", @minus_prot = 1";
        }

        $result = array_map(function ($value) {
            return (array)$value;
        }, DB::select($queryString));

        $data = [];
        foreach ($result as $key => $value) {
            foreach ($value as $keyDet => $valueDet) {
                if (($keyDet === 'RPRAW_ITMCOD' || $keyDet === 'MITM_ITMD1' || $keyDet === 'RPRAW_UNITMS')) {
                    $data[$key][$keyDet] = $value[$keyDet];
                } elseif ($keyDet === 'SALDO_OPN_DATE') {
                    $data[$key][$keyDet] = date('d M Y', strtotime($value[$keyDet]));
                }
                else {
                    if ($value[$keyDet] == (float)$value[$keyDet]) {
                        $data[$key][$keyDet] = (float)$value[$keyDet];
                    } else {
                        $data[$key][$keyDet] = $value[$keyDet];
                    }
                }
            }
        }

        return collect($data);
    }

    public function ExportToExcell(Request $req)
    {
        $cekDate = array_filter($req->param, function ($f) {
            return $f['field']['field'] === 'RPRAW_DATEIS';
        });

        if (count($cekDate) > 0) {
            $datePeriod = [date('d M Y', strtotime($cekDate[0]['val'])), date('d M Y', strtotime($cekDate[0]['val2']. ' +1 day'))];
        } else {
            $datePeriod = ['', ''];
        }

        // $data = $this->data($req->param);
        $data = $this->data($req->param, null, null, $req->header('userinput'));

        if ($req->format == 'excel') {
            Excel::store(new MutasiBahanBakuExport($data, $datePeriod, $req->format), 'MutasiBahanBakuExport.xls', 'public');

            return 'public/storage/MutasiBahanBakuExport.xls';
        } elseif ($req->format == 'pdf') {
            Excel::store(new MutasiBahanBakuExport($data, $datePeriod, $req->format), 'MutasiBahanBakuExport.pdf', 'public', \Maatwebsite\Excel\Excel::DOMPDF);

            return 'public/storage/MutasiBahanBakuExport.pdf';
        }
    }

    public function ConverttoLedger($arrcollection)
    {
        $parsing_item = [];
        $count_item = 0;
        $onlydate = [];

        if (count($arrcollection) > 0) {
            foreach ($arrcollection as $key => $value) {
                if ($key !== 0 && $value['RPRAW_ITMCOD'] !== $arrcollection[$key - 1]['RPRAW_ITMCOD']) {
                    $count_item++;
                    $parsing_item[$count_item]['RPRAW_DATEIS'] = $value['RPRAW_DATEIS'];
                } else {
                    $parsing_item[$count_item]['RPRAW_DATEIS'] = $arrcollection[$key]['RPRAW_DATEIS'];
                }

                $parsing_item[$count_item]['RPRAW_ITMCOD'] = $value['RPRAW_ITMCOD'];
                $parsing_item[$count_item]['MITM_ITMD1'] = $value['MITM_ITMD1'];
                $parsing_item[$count_item]['RPRAW_UNITMS'] = $value['RPRAW_UNITMS'];

                $onlydate[] =  $value['RPRAW_DATEIS'];
            }

            $fdate = min(array_map('strtotime', $onlydate));
            $ldate = max(array_map('strtotime', $onlydate));

            $parsing_saldototal = [];
            foreach ($parsing_item as $key_saldoawal => $value_saldoawal) {
                $saldoawal = MutasiRaw::select(DB::raw('COALESCE(SUM(RPRAW_QTYINC), 0) as saldo'))
                    ->where('RPRAW_ITMCOD', $value_saldoawal['RPRAW_ITMCOD'])
                    ->where('RPRAW_DATEIS', '<', $value_saldoawal['RPRAW_DATEIS'])
                    ->first();
                $saldocurrent = MutasiRaw::select(
                    DB::raw('SUM(RPRAW_QTYINC) as saldoin'),
                    DB::raw('SUM(RPRAW_QTYOUT) as saldoout'),
                    DB::raw('SUM(RPRAW_QTYOPN) as saldoopn'),
                    DB::raw('SUM(RPRAW_QTYOPN) as saldoadj')
                )
                    ->where('RPRAW_ITMCOD', $value_saldoawal['RPRAW_ITMCOD'])
                    ->whereBetween('RPRAW_DATEIS', [$value_saldoawal['RPRAW_DATEIS'], date('Y-m-d', $ldate)])
                    ->first();

                $parsing_saldototal[$key_saldoawal] = array_merge(
                    $value_saldoawal,
                    ['SALDO_AWAL' => $saldoawal['saldo']],
                    ['SALDO_MASUK' => $saldocurrent['saldoin']],
                    ['SALDO_KELUAR' => $saldocurrent['saldoout']],
                    ['SALDO_ADJ' => $saldocurrent['saldoadj']],
                    ['SALDO_TOT' => (($saldoawal['saldo'] + $saldocurrent['saldoin']) - $saldocurrent['saldoout']) + $saldocurrent['saldoadj']],
                    ['SALDO_OPN' => (($saldoawal['saldo'] + $saldocurrent['saldoin']) - $saldocurrent['saldoout']) + $saldocurrent['saldoadj']],
                    // ['SALDO_OPN' => $saldocurrent['saldoopn']], // Real saldo opname
                    ['SALDO_SLS' => ($saldoawal['saldo'] + ($saldocurrent['saldoin'] - $saldocurrent['saldoout']) + $saldocurrent['saldoadj']) - ((($saldoawal['saldo'] + $saldocurrent['saldoin']) - $saldocurrent['saldoout']) + $saldocurrent['saldoadj'])]
                );
            }

            return $parsing_saldototal;
        } else {
            return $arrcollection;
        }
    }
}
