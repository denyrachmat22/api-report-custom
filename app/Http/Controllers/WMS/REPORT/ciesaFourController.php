<?php

namespace App\Http\Controllers\WMS\REPORT;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Model\MASTER\DLVMaster;
use App\Model\CIESA\ciesaToken;
use Illuminate\Http\Request;
use Symfony\Component\DomCrawler\Crawler;

class ciesaFourController extends Controller
{

    public function apiPointData(
        $url,
        $method,
        $paramBody = [],
        $source = 'nle',
        $useToken = false
    ) {
        set_time_limit(1500);
        if ($source === 'openapi') {
            $endpoint = 'https://apis-gw.beacukai.go.id/openapi/' . $url;
        } else {
            $endpoint = $source === 'nle'
                ? /* 'https://nlehub.kemenkeu.go.id/' */ 'https://apis-gw.beacukai.go.id/' . $url
                : ($source === 'sce'
                    ? 'https://apis-gw.beacukai.go.id/v2/sce-ws/' . $url
                    : ($source === 'browse-service'
                        ? 'https://apis-gw.beacukai.go.id/v2/browse-service/v1/' . $url
                        : ($source === 'parser'
                            ? 'https://apis-gw.beacukai.go.id/v2/parser/v1/' . $url
                            : $url
                        )
                    )
                );
        }

        $content = [];
        $guzz = new \GuzzleHttp\Client(['timeout' => 600, 'connect_timeout' => 600]);

        try {
            if ($useToken) {
                $getToken = $this->cekToken();

                if (!empty($getToken)) {
                    $res = $guzz->request($method, $endpoint, [
                        'verify' => false,
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            'Authorization' => 'Bearer ' . $getToken
                        ],
                        'body' => json_encode($paramBody),
                    ]);
                } else {
                    $res = $guzz->request($method, $endpoint, [
                        'verify' => false,
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                        ],
                        'body' => json_encode($paramBody)
                    ]);
                }
            } else {
                $res = $guzz->request($method, $endpoint, [
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json'
                    ],
                    'body' => json_encode($paramBody)
                ]);
            }
            // return 'masuk sini';
            $content['PARAM'] = $paramBody;
            $content['CURL'] = json_decode($res->getBody(), true);
            logger($content['CURL']);
            return $content['CURL'];
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            // return $endpoint;
            $response = $e->getResponse();

            $responseBodyAsString = $response->getBody()->getContents();
            return json_decode($responseBodyAsString, true);
        }
    }

    public function cekToken()
    {
        $getToken = ciesaToken::orderBy('created_at', 'desc');
        // Jika DB kosong
        if (empty(with(clone $getToken)->first())) {
            logger('data token kosong!!');
            $login = $this->login();
            if ($login['status']) {
                $cekToken = with(clone $getToken)->first();

                return $cekToken->access_token;
            }
        } else {
            $dataToken = with(clone $getToken)->first();
            $from_time = strtotime($dataToken->created_at);
            $to_time = strtotime(date('Y-m-d H:i:s'));

            $minutes = round(abs($to_time - $from_time) / 60, 2);

            // logger('cek 1');
            // logger('menit token : ' . $minutes);
            // logger($minutes >= 5);
            // Jika token sudah lebih dari 5 menit
            if ($minutes >= 5) {
                $cekLogin = $this->login();

                // logger('Cek Login : '. $cekLogin);

                $cekTokenNya = with(clone $getToken)->first();

                // logger('token succes : ' . $cekTokenNya->access_token);

                return $cekTokenNya->access_token;
            } else {
                logger('masih dibawah 5 menit');
                return $dataToken->access_token;
            }
        }
    }
    public function login()
    {

        try {
            $sendData = $this->apiPointData(
                // 'auth-amws/v1/user/login',
                'nle-oauth/v1/user/login',
                'POST',
                [
                    'username' => 'smtind',
                    'password' => 'Smt!custom5C-2'
                ]
            );

            // logger($sendData);

            ciesaToken::create([
                'access_token' => $sendData['item']['access_token'],
                'refresh_token' => $sendData['item']['refresh_token'],
            ]);

            return $sendData;
        } catch (\Throwable $th) {
            return [
                'status' => 'failed',
                'message' => 'Login Error : ' . $th->getMessage()
            ];
        }
    }

    public function refreshToken()
    {
        try {
            $sendData = $this->apiPointData(
                'nle-oauth/v1/auth-amws/v1/user/update-token',
                'POST',
                [],
                'nle',
                true
            );

            ciesaToken::create([
                'access_token' => $sendData['item']['access_token'],
                'refresh_token' => $sendData['item']['refresh_token'],
            ]);

            return $sendData;
        } catch (\Throwable $th) {
            return [
                'status' => 'failed',
                'message' => $th->getMessage()
            ];
            //throw $th;
        }
    }

    function getKursFromDB($data)
    {
        $kursData = DB::connection('sqlsrv2')
            ->table('MEXRATE_TBL')
            ->where('MEXRATE_CURR', $data['kurs'])
            ->where('MEXRATE_DT', $data['date'])
            ->first('MEXRATE_VAL');
        if (!empty($kursData->MEXRATE_VAL ?? '')) {
            return [true, $kursData->MEXRATE_VAL];
        } else {
            return [false, 'Kurs tidak ditemukan di database'];
        }
    }

    public function sendingBC27($dlv, $txid, $cif, $hargaPenyerahan, $netto, $entitas, $dokumen, $pengangkut, $kemasan, $barangByPrice, $bahanBakuList)
    {
        $barangs = [];
        $sumHPBarang = 0;
        foreach ($barangByPrice as $keyBarang => $barang) {

            $dataTgl = [];
            foreach ($bahanBakuList as $keyMatForKurs => $valueMatForKurs) {
                $dataTgl[$valueMatForKurs['TANGGAL_DAFTAR_DOK_ASAL']] = [
                    'tgl_asal' => $valueMatForKurs['TANGGAL_DAFTAR_DOK_ASAL']
                ];
            }

            $dataKurs = [];
            foreach ($dataTgl as $keyKusrData => $valueKursData) {
                if (!empty($valueKursData['tgl_asal'])) {
                    $_kursValue = $this->getKursFromDB(['date' => $valueKursData['tgl_asal'], 'kurs' => 'USD']);
                    if (!$_kursValue[0]) {
                        return ['message' => 'kurs di DB tidak ditemukan untuk tanggal ' . $valueKursData['tgl_asal'], 'date' => $valueKursData['tgl_asal']];
                    }
                    $dataKurs[] = [
                        'tgl' => $valueKursData['tgl_asal'],
                        'kurs' => (float)$_kursValue[1],
                    ];
                }
            }

            $materialList = [];
            foreach ($bahanBakuList as $keyMaterial => $material) {
                if ($material['RASSYCODE'] === $barang['KODE_BARANG'] && $material['RPRICEGROUP'] === $barang['CIF']) {

                    $bahanBakuTarif = [];
                    foreach ($material['tarif'] as $keyTarif => $tarif) {
                        $bahanBakuTarif[] = [
                            "seriBahanBaku" => (int) $tarif['SERI_BAHAN_BAKU'],
                            "kodeJenisPungutan" => $tarif['JENIS_TARIF'],
                            "kodeAsalBahanBaku" => $material['KODE_ASAL_BAHAN_BAKU'],
                            "kodeFasilitasTarif" => (string) $tarif['KODE_FASILITAS'],
                            "nilaiBayar" => (int) $tarif['NILAI_BAYAR'],
                            // "nilaiFasilitas" => (int)$tarif['NILAI_FASILITAS'],
                            "nilaiFasilitas" => 0,
                            "nilaiSudahDilunasi" => 0,
                            "tarif" => empty($tarif['TARIF']) ? floatval(number_format($tarif['NILAI_FASILITAS'], 2)) : floatval(number_format($tarif['TARIF'], 2)),
                            "tarifFasilitas" => floatVal(number_format($tarif['TARIF_FASILITAS'], 2)),
                            "kodeSatuanBarang" => $material['JENIS_SATUAN'],
                            "jumlahSatuan" => floatVal($material['JUMLAH_SATUAN']),
                            "kodeJenisTarif" => (string) $tarif['KODE_TARIF'],
                            "jumlahKemasan" => 0
                        ];
                    }

                    $getNDPBM = array_values(array_filter($dataKurs, function ($f) use ($material) {
                        return $f['tgl'] == $material['TANGGAL_DAFTAR_DOK_ASAL'];
                    }));

                    $materialList[] = [
                        "cif" => round((float) ($material['CIF']), 2) == 0 ? 0.01 : round((float) ($material['CIF']), 2),
                        "cifRupiah" => round((float) ($material['CIF']) * $getNDPBM[0]['kurs'], 2),
                        "hargaPenyerahan" => (float)$material['HARGA_PENYERAHAN'],
                        "hargaPerolehan" => round((float) ($material['CIF']) * $getNDPBM[0]['kurs'], 2),
                        "jumlahSatuan" => floatVal($material['JUMLAH_SATUAN']),
                        "kodeSatuanBarang" => $material['JENIS_SATUAN'],
                        "kodeAsalBahanBaku" => $material['KODE_ASAL_BAHAN_BAKU'],
                        "kodeBarang" => $material['KODE_BARANG'],
                        "kodeDokAsal" => $material['KODE_JENIS_DOK_ASAL'],
                        "kodeKantor" => $material['KODE_KANTOR'],
                        "merkBarang" => "",
                        "ndpbm" => $getNDPBM[0]['kurs'],
                        "netto" => 0,
                        "nomorAjuDokAsal" => $material['NOMOR_AJU_DOK_ASAL'],
                        "nomorDaftarDokAsal" => $material['NOMOR_DAFTAR_DOK_ASAL'],
                        "posTarif" => empty($material['POS_TARIF']) ? "" : $material['POS_TARIF'],
                        "seriBahanBaku" => (int) $material['SERI_BAHAN_BAKU'],
                        "seriBarang" => (int) $material['SERI_BARANG'],
                        "seriBarangDokAsal" => (int) $material['SERI_BARANG_DOK_ASAL'],
                        "seriIjin" => 0,
                        "spesifikasiLainBarang" => empty($material['SPESIFIKASI_LAIN']) ? "" : $material['SPESIFIKASI_LAIN'],
                        "tanggalDaftarDokAsal" => $material['TANGGAL_DAFTAR_DOK_ASAL'],
                        "tipeBarang" => $material['TIPE'],
                        "ukuranBarang" => "",
                        "uraianBarang" => $material['URAIAN'],
                        "nilaiJasa" => 0,
                        'bahanBakuTarif' => $bahanBakuTarif
                    ];
                }
            }

            $_kursToday = $this->getKursFromDB(['date' => date('Y-m-d'), 'kurs' => 'USD']);
            if (!$_kursToday[0]) {
                return ['message' => 'kurs di DB tidak ditemukan untuk tanggal ' . $valueKursData['tgl_asal'], 'date' => $valueKursData['tgl_asal']];
            }
            $kursToday = (float)$_kursToday[1];


            // logger($materialList);
            $sumHPBarang += round(($barang['CIF'] * $kursToday), 2);
            $barangs[] = [
                "cif" => (float) $barang['CIF'],
                // BC 25, BC 27
                "cifRupiah" => round($barang['CIF'] * $kursToday, 2),
                // // BC 25, BC 27
                "hargaEkspor" => 0,
                // BC 25, BC 27
                "hargaPenyerahan" => round($barang['HARGA_PENYERAHAN'], 2),
                // BC 25, BC 27
                "isiPerKemasan" => 0,
                // BC 25, BC 27
                "jumlahSatuan" => floatVal($barang['JUMLAH_SATUAN']),
                // BC 25, BC 27
                "kodeBarang" => $barang['KODE_BARANG'],
                // BC 25, BC 27
                "kodeDokumen" => '27',
                // BC 27
                "kodeSatuanBarang" => $barang['KODE_SATUAN'],
                // BC 25, BC 27
                "merk" => "",
                // BC 25, BC 27
                "netto" => round($barang['NETTO'], 4),
                // BC 25, BC 27
                "nilaiBarang" => 0,
                // BC 25, BC 27
                "posTarif" => empty($barang['POS_TARIF']) ? "" : $barang['POS_TARIF'],
                // BC 25, BC 27
                "seriBarang" => (string) $barang['SERI_BARANG'],
                // BC 25, BC 27
                "spesifikasiLain" => "",
                // BC 25, BC 27
                "tipe" => $barang['TIPE'] ?? "",
                // BC 25, BC 27
                "ukuran" => "",
                // BC 25, BC 27
                "uraian" => $barang['URAIAN'],
                // BC 25, BC 27
                "hargaPerolehan" => 0.00,
                // BC 25, BC 27
                "kodeAsalBahanBaku" => "0",
                "ndpbm" => $kursToday,
                // BC 25, BC 27
                "uangMuka" => 0,
                "nilaiJasa" => 0,
                "bahanBaku" => $materialList,
                "jumlahKemasan" => (int) $barang['JUMLAH_KEMASAN'], // Test
            ];
        }

        $header = [
            'asalData' => 'S',
            'asuransi' => 0.00,
            'bruto' => $this->getBrutoHeader($txid),
            'cif' => round($cif, 2),
            'disclaimer' => "1",
            'kodeJenisTpb' => $dlv->DLV_ZJENIS_TPB_ASAL,
            'freight' => 0.00,
            'hargaPenyerahan' => floatVal(round($sumHPBarang, 2)),
            'jabatanTtd' => "J. SUPERVISOR",
            'jumlahKontainer' => 0,
            'kodeDokumen' => "27",
            'kodeKantor' => "050900",
            'kodeKantorTujuan' => $dlv->DLV_DESTOFFICE,
            'kodeTps' => "",
            'kodeTujuanPengiriman' => $dlv->DLV_PURPOSE == 6 ? '3' : (string) $dlv->DLV_PURPOSE,
            'kodeTujuanTpb' => $dlv->DLV_ZJENIS_TPB_TUJUAN,
            'kodeValuta' => $dlv->MCUS_CURCD,
            'kotaTtd' => "CIKARANG",
            'namaTtd' => $dlv->DLVH_PEMBERITAHU,
            'ndpbm' => (int) $dlv->MEXRATE_VAL,
            'netto' => floatVal(number_format($netto, 4)),
            'nik' => "8120111090314",
            'nilaiBarang' => 0.00,
            'nomorAju' => $dlv->DLV_ZNOMOR_AJU,
            'seri' => 0,
            'tanggalAju' => $dlv->DLV_BCDATE,
            'tanggalTtd' => $dlv->DLV_BCDATE,
            'biayaTambahan' => 0.00,
            'biayaPengurang' => 0.00,
            'vd' => 0.0000,
            'uangMuka' => 0.0000,
            'nilaiJasa' => 0.0000,
            'entitas' => $entitas,
            'dokumen' => $dokumen,
            'pengangkut' => $pengangkut,
            'kemasan' => $kemasan,
            "kontainer" => [],
            "pungutan" => [
                [
                    "idPungutan" => "",
                    "kodeFasilitasTarif" => "3",
                    "kodeJenisPungutan" => "PPN",
                    "nilaiPungutan" => 0 // kosongin info bu gusti
                ]
            ],
            "barang" => $barangs
        ];

        logger($header);

        $posting27 = $this->apiPointData(
            'openapi/document',
            'POST',
            $header,
            'nle',
            true
        );

        return $posting27;
    }

    public function sendingBC25($dlv, $txid, $cif, $hargaPenyerahan, $netto, $entitas, $dokumen, $pengangkut, $kemasan, $barangByPrice, $bahanBakuList)
    {
        $barangs = [];
        $tarifPpnbmPajak = NULL;
        $kursToday = $this->getAmount($this->getKurs(date('Y-m-d'), 'USD'));

        $sumCIFBarang = 0;
        $kodeValutaList = [];
        foreach ($barangByPrice as $keyBarang => $barang) {
            $dataTgl = [];
            foreach ($bahanBakuList as $keyMatForKurs => $valueMatForKurs) {
                $dataTgl[$valueMatForKurs['TANGGAL_DAFTAR_DOK_ASAL']] = [
                    'tgl_asal' => $valueMatForKurs['TANGGAL_DAFTAR_DOK_ASAL']
                ];
            }

            $materialList = [];
            $sumBB = 0;

            foreach ($bahanBakuList as $keyMaterial => $material) {
                $isFound = false;
                foreach ($kodeValutaList as &$_r) {
                    if ($material['CURRENCY'] === $_r['CURRENCY']) {
                        $isFound = true;
                        $_r['COUNTER'] += 1;
                        break;
                    }
                }
                unset($_r);
                if (!$isFound) {
                    $kodeValutaList[] = [
                        'CURRENCY' => $material['CURRENCY'],
                        'COUNTER' => 1,
                    ];
                }

                if ($material['RASSYCODE'] === $barang['KODE_BARANG'] && $material['RPRICEGROUP'] === $barang['CIF']) {

                    $bahanBakuTarif = [];
                    foreach ($material['tarif'] as $keyTarif => $tarif) {
                        $bahanBakuTarif[] = [
                            "seriBahanBaku" => (int) $tarif['SERI_BAHAN_BAKU'],
                            "kodeJenisPungutan" => $tarif['JENIS_TARIF'],
                            "kodeAsalBahanBaku" => $material['KODE_ASAL_BAHAN_BAKU'],
                            "kodeFasilitasTarif" => '1',
                            "nilaiBayar" => (float) ($tarif['NILAI_BAYAR']),
                            "nilaiFasilitas" => 0,
                            "nilaiSudahDilunasi" => 0,
                            "tarif" => (float) $tarif['TARIF'],
                            "tarifFasilitas" => (float) ($tarif['TARIF_FASILITAS']),
                            "jumlahSatuan" => floatVal($material['JUMLAH_SATUAN']),
                            "kodeJenisTarif" => (string) $tarif['KODE_TARIF'],
                            "jumlahKemasan" => 0
                        ];
                    }

                    if ($material['KODE_JENIS_DOK_ASAL'] == '27') {
                        $sumBB += round(floatVal($material['CIF']), 2);
                    }
                    $materialList[] = [
                        "bahanBakuDokumen" => [
                            [
                                "seriDokumen" => 1
                            ]
                        ],
                        "cif" => $material['KODE_JENIS_DOK_ASAL'] == '40' ? 0 : round((float) ($material['CIF']), 2), // KODE_JENIS_DOK_ASAL kalo bc 40 maka
                        "cifRupiah" => $material['KODE_JENIS_DOK_ASAL'] == '40' ? 0 : round((float) $material['CIF'], 2) * $material['NDPBM'], // KODE_JENIS_DOK_ASAL kalo bc 40 maka kosong
                        "hargaPenyerahan" => $material['KODE_JENIS_DOK_ASAL'] == '27' ? 0 : round(floatVal($material['HARGA_PENYERAHAN']), 2),
                        "hargaPerolehan" => $material['KODE_JENIS_DOK_ASAL'] == '27' ? 0 : round(floatVal($material['CIF']), 2), //dollar
                        "jumlahSatuan" => (int) $material['JUMLAH_SATUAN'],
                        "kodeSatuanBarang" => $material['JENIS_SATUAN'],
                        "kodeAsalBahanBaku" => $material['KODE_ASAL_BAHAN_BAKU'],
                        "kodeBarang" => $material['KODE_BARANG'],
                        "kodeDokAsal" => $material['KODE_JENIS_DOK_ASAL'],
                        "kodeKantor" => $material['KODE_KANTOR'],
                        "merkBarang" => "",
                        "ndpbm" => (int) $material['NDPBM'],
                        // "netto" => 0,
                        "nomorAjuDokAsal" => $material['NOMOR_AJU_DOK_ASAL'],
                        "nomorDaftarDokAsal" => $material['NOMOR_DAFTAR_DOK_ASAL'],
                        "posTarif" => empty($material['POS_TARIF']) ? "" : $material['POS_TARIF'],
                        "seriBahanBaku" => (int) $material['SERI_BAHAN_BAKU'],
                        "seriBarang" => (int) $material['SERI_BARANG'],
                        "seriBarangDokAsal" => (int) $material['SERI_BARANG_DOK_ASAL'],
                        "seriIjin" => 0,
                        "spesifikasiLainBarang" => empty($material['SPESIFIKASI_LAIN']) ? "" : $material['SPESIFIKASI_LAIN'],
                        "tanggalDaftarDokAsal" => $material['TANGGAL_DAFTAR_DOK_ASAL'],
                        "tipeBarang" => $material['TIPE'],
                        "ukuranBarang" => "",
                        "uraianBarang" => $material['URAIAN'],
                        // "nilaiJasa" => 0,
                        'bahanBakuTarif' => $bahanBakuTarif
                    ];
                }
            }

            $barangTarif = [];
            foreach ($barang['tarif'] as $keyTarifFG => $tarifFG) {
                if (!$tarifPpnbmPajak && $tarifFG['JENIS_TARIF'] === 'BM') {
                    $tarifPpnbmPajak = $tarifFG['TARIF'];
                }
                $barangTarif[] = [
                    'seriBarang' => (int) $barang['SERI_BARANG'],
                    'kodeJenisTarif' => '1',
                    // kode tarif 1
                    'jumlahSatuan' => 0,
                    // ?
                    'kodeFasilitasTarif' => "1",
                    // ? 1 - 9 tanya bu gusti
                    'kodeSatuanBarang' => "PCE",
                    // ?
                    'kodeJenisPungutan' => $tarifFG['JENIS_TARIF'],
                    'nilaiBayar' => 0,
                    // 0 def
                    'nilaiFasilitas' => 0,
                    // 0 def
                    'nilaiSudahDilunasi' => 0,
                    // 0 deg
                    'tarif' => (float) ($tarifFG['TARIF']), // sekarang kosong. bm 5, ppn 11, pph 2.5
                    'tarifFasilitas' => 100 // 100 def
                ];
            }

            $sumCIFBarang += $sumBB;
            // FG Barang
            $barangs[] = [
                "bruto" => (float) $barang['BRUTO'],
                // BC 25 => ?
                // "cif" => (float)$barang['CIF'], // tot sum cif bb
                "cif" => (float) round($sumBB, 2), // tot sum cif bb
                // BC 25, BC 27
                "diskon" => 0,
                // BC 25 => ?, BC 27
                "fob" => 0,
                // BC 25 => ?, BC 27
                "freight" => 0,
                // BC 25 => ?, BC 27
                "hargaEkspor" => 0,
                // BC 25, BC 27
                "hargaPenyerahan" => round($barang['HARGA_PENYERAHAN'], 2),
                // "hargaPenyerahan" => round(((float)round($sumBB, 2) * $kursToday), 2),
                // "hargaPenyerahan" => trim($dlv->MCUS_CURCD) == 'RPH' ? round(($barang['CIF']), 2) : round(($barang['CIF'] * $kursToday), 2), //cif * kurs
                // BC 25, BC 27
                "isiPerKemasan" => 0,
                // BC 25, BC 27
                "jumlahKemasan" => (int) $barang['JUMLAH_KEMASAN'],
                // BC 25 => ?
                "jumlahSatuan" => floatVal($barang['JUMLAH_SATUAN']),
                // BC 25, BC 27
                "kodeBarang" => $barang['KODE_BARANG'],
                // BC 25, BC 27
                "kodeGunaBarang" => $barang['KODE_GUNA'],
                // BC 25 => ?
                "kodeKategoriBarang" => '1',
                // BC 25 => ?
                "kodeJenisKemasan" => $barang['KODE_KEMASAN'],
                // BC 25 => ?
                "kodeKondisiBarang" => '1',
                // BC 25 => ? || [1] Tidak Rusak, [2] Rusak
                "kodePerhitungan" => '0', //kosong di portal
                // BC 25 => ? || [0] Harga Pemasukan, [1] Harga Pengeluaran
                "kodeSatuanBarang" => $barang['KODE_SATUAN'],
                // BC 25, BC 27
                "merk" => "",
                // BC 25, BC 27
                "netto" => round($barang['NETTO'], 4),
                // BC 25, BC 27
                "nilaiBarang" => 0,
                // BC 25, BC 27
                "posTarif" => empty($barang['POS_TARIF']) ? "" : $barang['POS_TARIF'],
                // BC 25, BC 27
                "seriBarang" => (int) $barang['SERI_BARANG'],
                // BC 25, BC 27
                "spesifikasiLain" => "",
                // BC 25, BC 27
                "tipe" => "",
                // BC 25, BC 27
                "ukuran" => "",
                // BC 25, BC 27
                "uraian" => $barang['URAIAN'],
                // BC 25, BC 27
                "ndpbm" => $kursToday,
                // BC 25, BC 27
                "cifRupiah" => round(((float) round($sumBB, 2) * $kursToday), 2),
                // // BC 25, BC 27
                "hargaPerolehan" => 0,
                // BC 25, BC 27
                "kodeDokAsal" => "",
                // BC 25
                // "flag4tahun" => 0,
                // BC 25 || ["Y", null]
                "barangTarif" => $barangTarif,
                "barangDokumen" => [],
                "bahanBaku" => $materialList
            ];
        }

        $currentGreatestValue = 0;
        $kodeValuta = NULL;
        foreach ($kodeValutaList as $r) {
            if ($r['COUNTER'] >= $currentGreatestValue) {
                $currentGreatestValue = $r['COUNTER'];
                $kodeValuta = $r['CURRENCY'];
            }
        }

        $header = [
            'asalData' => 'S',
            'bruto' => $this->getBrutoHeader($txid),
            // 'cif' => round($cif, 2), //tot sumary cif barang
            'cif' => round($sumCIFBarang, 2), //tot sumary cif barang
            'disclaimer' => "1",
            'kodeJenisTpb' => $dlv->DLV_ZJENIS_TPB_ASAL,
            'hargaPenyerahan' => round(floatVal($hargaPenyerahan), 2), // cif*kurs hari ini
            'idPengguna' => "",
            // ? belum tau
            'jabatanTtd' => "J. SUPERVISOR",
            'jumlahKontainer' => 0,
            'kodeCaraBayar' => "1",
            // ? belum tau
            'kodeDokumen' => "25",
            'kodeKantor' => "050900",
            'kodeLokasiBayar' => '1',
            'kodeTujuanPengiriman' => '1',
            'kodeValuta' => $kodeValuta,
            'kotaTtd' => "CIKARANG",
            'namaTtd' => $dlv->DLVH_PEMBERITAHU,
            'ndpbm' => (int) $dlv->MEXRATE_VAL,
            'netto' => floatVal(number_format($netto, 4)),
            'nomorAju' => $dlv->DLV_ZNOMOR_AJU,
            'seri' => 0,
            'tanggalAju' => $dlv->DLV_BCDATE,
            'tanggalTtd' => $dlv->DLV_BCDATE,
            'volume' => 0.00,
            'ppnPajak' => 0,
            'ppnbmPajak' => 0,
            'tarifPpnPajak' => 11,
            'tarifPpnbmPajak' => (float) $tarifPpnbmPajak,
            'entitas' => $entitas,
            'dokumen' => $dokumen,
            'pengangkut' => $pengangkut,
            'kemasan' => $kemasan,
            "kontainer" => [],
            "pungutan" => [
                [
                    "idPungutan" => "",
                    "kodeFasilitasTarif" => "3",
                    "kodeJenisPungutan" => "PPN",
                    "nilaiPungutan" => 0 // kosongin info bu gusti
                ]
            ],
            "barang" => $barangs
        ];

        logger($header);

        $posting25 = $this->apiPointData(
            'openapi/document',
            'POST',
            $header,
            'nle',
            true
        );

        return $posting25;
    }

    public function sendingBC41($dlv, $txid, $cif, $hargaPenyerahan, $netto, $entitas, $dokumen, $pengangkut, $kemasan, $barangByPrice, $bahanBakuList)
    {
        $barangs = [];
        foreach ($barangByPrice as $keyBarang => $barang) {

            $materialList = [];
            foreach ($bahanBakuList as $keyMaterial => $material) {
                if ($material['RASSYCODE'] === $barang['KODE_BARANG'] && $material['RPRICEGROUP'] === $barang['CIF']) {
                    $materialList[] = [
                        "cif" => (int) $material['CIF'],
                        "cifRupiah" => 0,
                        "hargaPenyerahan" => (float) $material['HARGA_PENYERAHAN'],
                        "hargaPerolehan" => 0,
                        "jumlahSatuan" => (float) $material['JUMLAH_SATUAN'],
                        "kodeSatuanBarang" => $material['JENIS_SATUAN'],
                        "kodeAsalBahanBaku" => $material['KODE_ASAL_BAHAN_BAKU'],
                        "kodeBarang" => $material['KODE_BARANG'],
                        "kodeDokAsal" => $material['KODE_JENIS_DOK_ASAL'],
                        "kodeDokumen" => "",
                        "kodeKantor" => $material['KODE_KANTOR'],
                        "merkBarang" => "",
                        "ndpbm" => 0,
                        "netto" => 0,
                        "nomorAjuDokAsal" => $material['NOMOR_AJU_DOK_ASAL'],
                        "nomorDaftarDokAsal" => $material['NOMOR_DAFTAR_DOK_ASAL'],
                        "posTarif" => empty($material['POS_TARIF']) ? "" : $material['POS_TARIF'],
                        "seriBahanBaku" => (int) $material['SERI_BAHAN_BAKU'],
                        "seriBarang" => (int) $material['SERI_BARANG'],
                        "seriBarangDokAsal" => (int) $material['SERI_BARANG_DOK_ASAL'],
                        "seriIjin" => 0,
                        "spesifikasiLainBarang" => empty($material['SPESIFIKASI_LAIN']) ? "" : $material['SPESIFIKASI_LAIN'],
                        "tanggalDaftarDokAsal" => $material['TANGGAL_DAFTAR_DOK_ASAL'],
                        "tipeBarang" => $material['TIPE'],
                        "ukuranBarang" => "",
                        "uraianBarang" => $material['URAIAN'],
                        "nilaiJasa" => 0,
                        // 'bahanBakuTarif' => $bahanBakuTarif
                    ];
                }
            }

            $barangs[] = [
                "cif" => (float) $barang['CIF'],
                // req
                "cifRupiah" => 0,
                // // BC 25, BC 27
                // ?
                "hargaEkspor" => 0,
                // req
                "hargaPenyerahan" => (float) $barang['HARGA_PENYERAHAN'],
                // BC 25, BC 27
                // "hargaSatuan" => 0,
                // ?
                "isiPerKemasan" => 0,
                // BC 25, BC 27
                "jumlahKemasan" => 0,
                // ?
                // "jumlahRealisasi" => 0,
                // ?
                "jumlahSatuan" => (float) $barang['JUMLAH_SATUAN'],
                // BC 25, BC 27
                "kodeBarang" => $barang['KODE_BARANG'],
                // BC 25, BC 27
                "kodeDokumen" => '41',
                // BC 27
                "kodeJenisKemasan" => '',
                // ?
                "kodeSatuanBarang" => $barang['KODE_SATUAN'],
                // BC 25, BC 27
                "merk" => "",
                // BC 25, BC 27
                "netto" => (int) $barang['NETTO'],
                // BC 25, BC 27
                "nilaiBarang" => 0,
                // BC 25, BC 27
                "posTarif" => empty($barang['POS_TARIF']) ? "" : $barang['POS_TARIF'],
                // BC 25, BC 27
                "seriBarang" => (int) $barang['SERI_BARANG'],
                // BC 25, BC 27
                "spesifikasiLain" => "",
                // BC 25, BC 27
                "tipe" => "",
                // BC 25, BC 27
                "ukuran" => "",
                // BC 25, BC 27
                "uraian" => $barang['URAIAN'],
                "volume" => 0.0000,
                // ?
                // BC 25, BC 27
                "hargaPerolehan" => 0,
                // BC 25, BC 27
                "kodeAsalBahanBaku" => "0",
                "ndpbm" => 0,
                // BC 25, BC 27
                "uangMuka" => 0,
                "nilaiJasa" => 0,
                "bahanBaku" => $materialList
            ];
        }

        $header = [
            'asalData' => 'S',
            'asuransi' => 0,
            // ?
            'bruto' => $this->getBrutoHeader($txid),
            'cif' => round($cif, 2),
            'kodeJenisTpb' => $dlv->DLV_ZJENIS_TPB_ASAL,
            'freight' => 0,
            // ?
            'hargaPenyerahan' => (float) $hargaPenyerahan,
            'idPengguna' => "",
            // ? belum tau
            'jabatanTtd' => "J. SUPERVISOR",
            'jumlahKontainer' => 0,
            'kodeDokumen' => "41",
            'kodeKantor' => "050900",
            'kodeTujuanPengiriman' => $dlv->DLV_PURPOSE,
            // 'kodeValuta' => $dlv->MCUS_CURCD,
            'kotaTtd' => "CIKARANG",
            'namaTtd' => $dlv->DLVH_PEMBERITAHU,
            // 'ndpbm' => $dlv->MEXRATE_VAL,
            'netto' => (float) $netto,
            'nik' => "8120111090314",
            'nomorAju' => $dlv->DLV_ZNOMOR_AJU,
            'seri' => 0,
            'tanggalAju' => $dlv->DLV_BCDATE,
            'tanggalTtd' => $dlv->DLV_BCDATE,
            'userPortal' => "smtind",
            'volume' => 0.00,
            'biayaTambahan' => 0.00,
            // ?
            'biayaPengurang' => 0.00,
            // ?
            'vd' => 0.00,
            // ?
            'uangMuka' => 0.0000,
            // ?
            'NilaiJasa' => 0.00,
            // ?
            'ppnPajak' => 0.00,
            //?
            'ppnbmPajak' => 0.00,
            //?
            'tarifPpnPajak' => 0.00,
            //?
            'tarifPpnbmPajak' => 0.00,
            //?
            'kodeLokasiBayar' => "4",
            'kodePembayar' => "3",
            'nilaiBarang' => 0,
            'entitas' => $entitas,
            'dokumen' => $dokumen,
            'pengangkut' => $pengangkut,
            'kemasan' => $kemasan,
            "kontainer" => [],
            "pungutan" => [
                [
                    "idPungutan" => "",
                    "kodeFasilitasTarif" => "3",
                    "kodeJenisPungutan" => "PPN",
                    "nilaiPungutan" => 0 // kosongin info bu gusti
                ]
            ],
            "barang" => $barangs
        ];

        logger($header);

        $posting40 = $this->apiPointData(
            'openapi/document',
            'POST',
            $header,
            'nle',
            true
        );

        return $posting40;
    }

    public function testNIB(Request $request)
    {
        $getDetilPerusahanPenerima = $this->apiPointData(
            'profil/perusahaan/data-perusahaan-by-npwp/?npwp=' . $request->npwp,
            'GET',
            [],
            'sce',
            true
        );
        return $getDetilPerusahanPenerima;
    }

    public function getDataFromDLVWMS(Request $req, $bc)
    {
        $txid = $req->txid;
        $cif = $req->cif;
        $hargaPenyerahan = $req->hargaPenyerahan;
        $netto = $req->netto;
        $jumlahKemasan = $req->jumlahKemasan;
        $barangByPrice = $req->ListBarangByPrice;
        $bahanBakuList = $req->ListBahanBakuList;

        $dlv = $this->checkDLV(base64_encode($txid));

        if (empty($dlv)) {
            return [
                'status' => 'failed',
                'message' => 'DLV tx id Data not found',
                'data' => base64_encode($txid)
            ];
        }

        //MSTTRANS_TBL ON DLV_TRANS => MSTTRANS_ID
        $npwp = str_replace(['-', '.'], '', $dlv->MDEL_ZTAX);

        $entitas = [
            // Pemasok
            [
                "alamatEntitas" => "JL. CISOKAN 5 PLOT 5C-2 EJIP INDUSTRIAL PARK, SUKARESMI, CIKARANG SELATAN, BEKASI, JAWA BARAT",
                "kodeEntitas" => "3",
                "kodeJenisApi" => "2",
                "kodeStatus" => "5",
                "kodeJenisIdentitas" => "6",
                "namaEntitas" => "PT SMT INDONESIA",
                "nibEntitas" => "8120111090314",
                // Get api from 'Validasi/entitas_dan_pemilik/8e6cac21-1e7d-401c-9dc2-bb093bc72a72'
                "nomorIdentitas" => "0010850188055000",
                "nomorIjinEntitas" => empty($req->nomorIjinEntitas) ? "KEP-508/WBC.09/2022" : $req->nomorIjinEntitas,
                "seriEntitas" => 1,
                "tanggalIjinEntitas" => "2022-01-01"
            ],
            // Pemilik
            [
                "alamatEntitas" => "JL. CISOKAN 5 PLOT 5C-2 EJIP INDUSTRIAL PARK, SUKARESMI, CIKARANG SELATAN, BEKASI, JAWA BARAT",
                "kodeEntitas" => "7",
                "kodeJenisApi" => $bc === '41' ? "02" : "2",
                "kodeStatus" => "5",
                "kodeJenisIdentitas" => "6",
                "namaEntitas" => "PT SMT INDONESIA",
                "nibEntitas" => "8120111090314",
                // Get api from 'Validasi/entitas_dan_pemilik/8e6cac21-1e7d-401c-9dc2-bb093bc72a72'
                "nomorIdentitas" => "0010850188055000",
                "nomorIjinEntitas" => empty($req->nomorIjinEntitas) ? "KEP-508/WBC.09/2022" : $req->nomorIjinEntitas,
                "seriEntitas" => 2,
                "tanggalIjinEntitas" => "2022-01-01"
            ],
            // Penerima
            [
                "alamatEntitas" => $dlv->MDEL_ADDRCUSTOMS,
                "kodeEntitas" => "8",
                "kodeJenisApi" => $bc === '41' ? "02" : "2",
                "kodeJenisIdentitas" => "6",
                "kodeStatus" => "5",
                "namaEntitas" => $dlv->MDEL_ZNAMA,
                "nibEntitas" => $dlv->MDEL_NIB,
                "nomorIdentitas" => $npwp,
                "nomorIjinEntitas" => $dlv->MDEL_ZSKEP,
                "seriEntitas" => 3,
                "tanggalIjinEntitas" => $dlv->MDEL_ZSKEP_DATE,
                "niperEntitas" => ''
                // Get from api portal 'GudangPlb/perusahanSkepFasilitas?idPerusahaanPajak={nomorIdentitas}'
                // Date | Tanggal ijin entitas
            ],
        ];

        $dokumen = [
            [
                // Invoice
                "idDokumen" => $dlv->DLV_INVNO,
                "kodeDokumen" => "380",
                "nomorDokumen" => $dlv->DLV_INVNO,
                "seriDokumen" => 2,
                "tanggalDokumen" => $dlv->DLV_INVDT // DLV_TBL => DLV_INVDT
            ],
            [
                // Packing List
                "idDokumen" => $dlv->DLV_INVNO,
                "kodeDokumen" => "217",
                "nomorDokumen" => $dlv->DLV_INVNO,
                "seriDokumen" => 3,
                "tanggalDokumen" => $dlv->DLV_INVDT // DLV_TBL => DLV_INVDT
            ],
            [
                // Surat Jalan
                "idDokumen" => $dlv->DLV_ID,
                "kodeDokumen" => "640",
                "nomorDokumen" => $dlv->DLV_ID,
                "seriDokumen" => 1,
                "tanggalDokumen" => $dlv->DLV_BCDATE,
                // DLV_TBL => DLV_BCDATE
            ],
        ];


        $DeliveryNonUnique = DB::connection('sqlsrv2')
            ->table('DLV_TBL')
            ->select('DLV_SER', 'DLV_ITMCD')
            ->where('DLV_SER', '')
            ->where('DLV_ID', $dlv->DLV_ID)
            ->count();

        if ($DeliveryNonUnique && $bc == '27') {
            $_SeriDokumen = 4;
            $_Dokumen = [];

            foreach ($bahanBakuList as $r) {
                $_isFound = false;
                foreach ($_Dokumen as $d) {
                    if (
                        $d['NomorPendaftaran'] === $r['NOMOR_DAFTAR_DOK_ASAL']
                        && $d['TangalPendaftaran'] === $r['TANGGAL_DAFTAR_DOK_ASAL']
                        && $d['KodeJenisDokumen'] === $r['KODE_JENIS_DOK_ASAL']
                    ) {
                        $_isFound = true;
                        break;
                    }
                }

                if (!$_isFound) {
                    $_Dokumen[] = [
                        'NomorPendaftaran' => $r['NOMOR_DAFTAR_DOK_ASAL'],
                        'TangalPendaftaran' => $r['TANGGAL_DAFTAR_DOK_ASAL'],
                        'KodeJenisDokumen' => $r['KODE_JENIS_DOK_ASAL'],
                    ];
                }
            }

            foreach ($_Dokumen as $r) {
                $dokumen[] = [
                    'idDokumen' => $r['NomorPendaftaran'],
                    'kodeDokumen' => $r['KodeJenisDokumen'],
                    'nomorDokumen' => $r['NomorPendaftaran'],
                    'seriDokumen' => $_SeriDokumen++,
                    'tanggalDokumen' => $r['TangalPendaftaran'],
                ];
            }
        }

        switch ($bc) {
            case '27':
                $seriPengangkut = '3';
                break;
            case '25':
                $seriPengangkut = 3;
                break;
            case '41':
                $seriPengangkut = '1';
                break;
            default:
                $seriPengangkut = '3';
        }

        if ($bc == '41') {
            $pengangkut = [
                [
                    "idPengangkut" => 1,
                    "namaPengangkut" => $dlv->MSTTRANS_TYPE,
                    "nomorPengangkut" => $dlv->DLV_TRANS,
                    "seriPengangkut" => $seriPengangkut, //Default,
                ]
            ];
        } else {
            $pengangkut = [
                [
                    "namaPengangkut" => $dlv->MSTTRANS_TYPE,
                    "nomorPengangkut" => $dlv->DLV_TRANS,
                    "seriPengangkut" => $seriPengangkut, //Default,
                    "kodeCaraAngkut" => '3'
                ]
            ];
        }

        $kemasan = [
            [
                "jumlahKemasan" => (int) $jumlahKemasan,
                "kodeJenisKemasan" => "BX",
                // Default
                "merkKemasan" => "-",
                "seriKemasan" => 1
            ]
        ];

        if ($bc === '27') {
            return $this->sendingBC27(
                $dlv,
                $txid,
                $cif,
                $hargaPenyerahan,
                $netto,
                $entitas,
                $dokumen,
                $pengangkut,
                $kemasan,
                $barangByPrice,
                $bahanBakuList
            );
        } elseif ($bc === '25') {
            return $this->sendingBC25(
                $dlv,
                $txid,
                $cif,
                $hargaPenyerahan,
                $netto,
                $entitas,
                $dokumen,
                $pengangkut,
                $kemasan,
                $barangByPrice,
                $bahanBakuList
            );
        } else {
            return $this->sendingBC41(
                $dlv,
                $txid,
                $cif,
                $hargaPenyerahan,
                $netto,
                $entitas,
                $dokumen,
                $pengangkut,
                $kemasan,
                $barangByPrice,
                $bahanBakuList
            );
        }
    }


    public function checkDLV($txid)
    {
        $select = [
            'DLV_ZJENIS_TPB_ASAL',
            'DLV_DESTOFFICE',
            'DLV_PURPOSE',
            'DLV_ZJENIS_TPB_TUJUAN',
            'DLVH_PEMBERITAHU',
            'DLV_ZNOMOR_AJU',
            'DLV_BCDATE',
            'MDEL_ADDRCUSTOMS',
            'MDEL_ZTAX',
            'DLV_INVNO',
            'DLV_INVDT',
            'DLV_BCDATE',
            'DLV_ID',
            'DLV_TRANS',
            'MSTTRANS_TYPE',
            'MEXRATE_VAL',
            'MDEL_ZNAMA',
            'MDEL_ZSKEP',
            'MDEL_ZSKEP_DATE',
            'MDEL_NIB'
        ];

        $dlv = DLVMaster::select(
            array_merge(
                $select,
                [
                    DB::raw('RTRIM(ISNULL(MCUS_CURCD, MSUP_SUPCR)) AS MCUS_CURCD')
                ]
            )
        )
            ->where('DLV_ID', base64_decode($txid))
            ->leftjoin('MCUS_TBL', 'DLV_CUSTCD', 'MCUS_CUSCD')
            ->leftjoin('MSUP_TBL', 'DLV_CUSTCD', 'MSUP_SUPCD')
            ->leftjoin('MDEL_TBL', 'DLV_CONSIGN', 'MDEL_DELCD')
            ->leftjoin('DLVH_TBL', 'DLV_ID', 'DLVH_ID')
            ->leftjoin('MSTTRANS_TBL', 'DLV_TRANS', 'MSTTRANS_ID')
            ->join('MEXRATE_TBL', function ($j) {
                $j->on('MEXRATE_DT', 'DLV_BCDATE');
                $j->on('MEXRATE_CURR', DB::raw('ISNULL(MCUS_CURCD, MSUP_SUPCR)'));
            })
            ->groupBy(
                array_merge(
                    $select,
                    [
                        DB::raw('ISNULL(MCUS_CURCD, MSUP_SUPCR)')
                    ]
                )
            )
            ->first();

        return $dlv;
    }

    public function getBrutoHeader($txid)
    {
        $_DLV = DB::table("DLV_TBL")->select('DLV_ITMCD')
            ->where('DLV_ID', $txid)->first();

        if (str_contains($_DLV->DLV_ITMCD ?? '', 'SCRAP')) {
            $data = DB::table("DLV_PKG_TBL")->select("DLV_PKG_NWG", "DLV_PKG_GWG")
                ->where('DLV_PKG_DOC', $txid)->first();
            $hasil = $data->DLV_PKG_GWG ?? 1000;
        } else {
            $data = DB::select(DB::raw('exec SP_PACKINGLIST_BY_DONO @dono = "' . $txid . '"'));

            $hasil = 0;
            foreach ($data as $key => $value) {
                $hasil += $value->MITM_GWG;
            }
        }

        return round($hasil, 4);
    }

    public function getDelivery($txid, $sel = [], $group = [])
    {
        $data = new DLVMaster;

        if (count($sel) > 0) {
            $data->select($sel);

            if (count($group) > 0) {
                $data->groupBy($group);
            }
        }

        return $data;
    }

    function statusPengajuan(Request $request)
    {
        try {
            $sendData = $this->apiPointData(
                'openapi/status/' . $request->id,
                'GET',
                [],
                'nle',
                true
            );
            return $sendData;
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'message' => $th->getMessage()
            ];
        }
    }

    public function getNopen($noAju, $nib = '010850188055000')
    {
        try {
            $DeliveryTBL = DB::connection('sqlsrv2')
                ->table('DLV_TBL')
                ->where('DLV_ZNOMOR_AJU', $noAju)
                ->first('DLV_BCTYPE');

            $sendData = $DeliveryTBL->DLV_BCTYPE == '27' ? $this->apiPointData(
                'status/' . $noAju,
                'GET',
                [],
                'openapi',
                true
            ) : $this->apiPointData(
                'browse/dokumen-pabean-portal?nomorIdentitas=' . $nib . '&kodeJalur=&namaPerusahaan=&namaPpjk=&kodeKantor=&nomorAju=' . $noAju . '&nomorDaftar=&page=0&size=10&idUser=3e158b35-16e4-4aaa-af76-d9c382284e6c',
                'GET',
                [],
                'browse-service',
                true
            );

            if ($sendData['status'] == 'Failed') {
                $sendData = $this->apiPointData(
                    'browse/dokumen-pabean-portal?nomorIdentitas=' . $nib . '&kodeJalur=&namaPerusahaan=&namaPpjk=&kodeKantor=&nomorAju=' . $noAju . '&nomorDaftar=&page=0&size=10&idUser=3e158b35-16e4-4aaa-af76-d9c382284e6c',
                    'GET',
                    [],
                    'browse-service',
                    true
                );
            }

            return $sendData;
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'message' => $th->getMessage(),
                'dataOri' => ['message' => $th->getMessage()]
            ];
        }
    }

    public function getKursCheck($date, $kurs = '', $failedTimes = 0)
    {
        return [
            'a' => $this->getAmount($this->getKurs($date, $kurs, $failedTimes)),
            'data' => $this->getKurs($date, $kurs, $failedTimes)
        ];
    }

    public function getKurs($date, $kurs = '', $failedTimes = 0)
    {
        set_time_limit(3000);
        $guzz = new \GuzzleHttp\Client(['timeout' => 600, 'connect_timeout' => 600]);

        $res = $guzz->request('GET', "https://fiskal.kemenkeu.go.id/informasi-publik/kurs-pajak?date=" . date('Y-m-d', strtotime($date)), [
            'verify' => false,
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Connection' => 'Close'
            ],
        ]);

        // $res = $guzz->request('POST', "https://www.beacukai.go.id/kurs.html?tglKurs=" . date('d-m-Y', strtotime($date)) . "&content=browseKurs", [
        //     'verify' => false,
        //     'headers' => [
        //         'Content-Type' => 'application/json',
        //         'Accept' => 'application/json',
        //         'Connection' => 'Close'
        //     ],
        // ]);

        try {
            logger('start crawling for date ' . $date . ' and kurs ' . $kurs . ' ' . $failedTimes);
            // logger($res->getBody());
            $bodynya = $res->getBody();
            // return $bodynya;
            $crawler = new Crawler((string) $bodynya);

            $listItem = $crawler->filterXPath('//*[@class="table table-bordered table-striped"]/tbody/tr/td')->each(function ($value) use ($kurs) {
                $fmt = new \NumberFormatter('de_DE', \NumberFormatter::CURRENCY);
                $curr = 'EUR';
                // return $value->extract(['_text']);
                $getText = $value->extract(['_text'])[0];

                $getText = trim(preg_replace('/\s\s+/', ' ', $getText));
                return $getText;
                // $hasil = explode(' ', $getText)[2];

                if (!empty($kurs)) {
                    if (str_contains($getText, $kurs)) {
                        return strtr($getText, ".,", ",.");
                    }
                } else {
                    return strtr($getText, ".,", ",.");
                }
            });

            $hasil = [];
            $count = $keyMaster = 0;
            foreach ($listItem as $key => $value) {
                switch ($count) {
                    case 0:
                        $keyString = 'no';
                        break;

                    case 1:
                        $keyString = 'mata_uang';
                        break;

                    case 2:
                        $keyString = 'nilai';
                        break;

                    default:
                        $keyString = 'perubahan';
                        break;
                }

                $hasil[$keyMaster][$keyString] = $value;

                $count++;
                if ($count === 4) {
                    $count = 0;
                    $keyMaster++;
                }
            }

            $searchKurs = array_values(array_filter($hasil, function ($f) use ($kurs) {
                return str_contains($f['mata_uang'], $kurs);
            }))[0];

            return strtr($searchKurs['nilai'], ".,", ",.");
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            logger('Failed ' . $failedTimes);
            if ($failedTimes <= 5) {
                return $this->getKurs($date, $kurs, $failedTimes + 1);
            }

            logger($e);
            return $e->getResponse()->getBody()->getContents();
        }
    }

    public function getAmount($moneys)
    {
        $money = substr_replace($moneys, "", -2);
        $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
        $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

        $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

        $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
        $removedThousandSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '', $stringWithCommaOrDot);

        return (float) str_replace(',', '.', $removedThousandSeparator);
    }
}
