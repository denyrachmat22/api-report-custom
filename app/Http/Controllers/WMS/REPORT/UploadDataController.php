<?php

namespace App\Http\Controllers\WMS\REPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ScrapUploadRequest;
use App\Imports\UploadScrapData;
use App\Imports\UploadMesinData;
use Maatwebsite\Excel\Facades\Excel;

class UploadDataController extends Controller
{
    public function UploadScrap(ScrapUploadRequest $req)
    {
        $nama_file = uniqid('upscrap_') . rand() . '.' . $req->file->extension();

        $req->file->storeAs('/public/upload_scrap', $nama_file);

        try {
            $importer = new UploadScrapData;
            Excel::import($importer, public_path('/storage/upload_scrap/' . $nama_file));
            if ($importer->data == 'item_not_found') {
                return 'Item tidak ditemukan';
            }

            return 'Upload Sukses ' . $nama_file;
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();

            foreach ($failures as $failure) {
                $failure->row(); // row that went wrong
                $failure->attribute(); // either heading key (if using heading row concern) or column index
                $failure->errors(); // Actual error messages from Laravel validator
                $failure->values(); // The values of the row that has failed.
            }

            return response()->json($failure,422);
        }
    }

    public function UploadMesin(Request $req)
    {
        $nama_file = uniqid('upmesin_') . rand() . '.' . $req->file->extension();

        $req->file->storeAs('/public/upload_mesin', $nama_file);

        try {
            $importer = new UploadMesinData;
            Excel::import($importer, public_path('/storage/upload_mesin/' . $nama_file));
            if ($importer->data == 'item_not_found') {
                return 'Item tidak ditemukan';
            }

            return 'Upload Sukses ' . $nama_file;
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();

            foreach ($failures as $failure) {
                $failure->row(); // row that went wrong
                $failure->attribute(); // either heading key (if using heading row concern) or column index
                $failure->errors(); // Actual error messages from Laravel validator
                $failure->values(); // The values of the row that has failed.
            }

            return response()->json($failure,422);
        }
    }

    public function DownloadTemplateScrap()
    {
        return 'public/storage/template_upload/upload_template_scrap.xlsx';
    }

    public function DownloadTemplateMesin()
    {
        return 'public/storage/template_upload/upload_template_mesin_peralatan.xlsx';
    }
}
