<?php

namespace App\Http\Controllers\WMS\REPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\RPCUST\UserLogs;
use Illuminate\Support\Facades\DB;
use App\Helpers\CustomFunctionHelper;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UserLogsExport;
use App\Model\WMS\API\WMSUserLogs;
use App\Exports\WMSUserLogsExport;

class UserLogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return UserLogs::create([
            'username' => $request->username,
            'type_log' => $request->type_log,
            'log_desc' => $request->log_desc,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return WMSUserLogs::select('*', DB::raw('ROW_NUMBER() OVER(ORDER BY CSMLOG_LINE ASC) as no'))->where('CSMLOG_DOCNO', base64_decode($id))->join('MSTEMP_TBL', 'MSTEMP_ID', 'CSMLOG_CREATED_BY')->get()->toArray();
    }

    public function showWMSLogs(Request $request, $id)
    {
        $rows = $request->has('pagination') ? $request['pagination']['rowsPerPage'] : 20;
        $page = $request->has('pagination') ? $request['pagination']['page'] : 1;

        $data = $this->parseRequest($request, $id, 'wms');

        $filterData = collect($data);

        $rows = $request->has('pagination') ? $request['pagination']['rowsPerPage'] : 20;
        $page = $request->has('pagination') ? $request['pagination']['page'] : 1;
        $rowsNum = $request->has('pagination') ? $request->pagination['rowsNumber'] : $filterData->count();

        if ($id == 'table') {
            // file_put_contents('logs.txt', $filterData->toSql().json_encode($filterData->getBindings()).PHP_EOL , FILE_APPEND | LOCK_EX);
            if ($rows == 0) {
                $cekall = $filterData->count();
                return $filterData->paginate($cekall);
                return $filterData->paginate($cekall, ['*'], 'page', $page);
            } else {
                // Log::info($filterData->paginate($rows));
                return $filterData->paginate($rows, $rowsNum, $page);
                return $filterData->paginate($rows, ['*'], 'page', $page);
            }
        } elseif ($id == 'data') {
            return $filterData->all();
        } else {
            // $data = $filterData->get();
            return $this->ExportToExcell($request, 'wms');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rows = $request->has('pagination') ? $request['pagination']['rowsPerPage'] : 20;
        $page = $request->has('pagination') ? $request['pagination']['page'] : 1;

        $data = $this->parseRequest($request, $id);

        $filterData = collect($data);

        $rows = $request->has('pagination') ? $request['pagination']['rowsPerPage'] : 20;
        $page = $request->has('pagination') ? $request['pagination']['page'] : 1;
        $rowsNum = $request->has('pagination') ? $request->pagination['rowsNumber'] : $filterData->count();

        if ($id == 'table') {
            // file_put_contents('logs.txt', $filterData->toSql().json_encode($filterData->getBindings()).PHP_EOL , FILE_APPEND | LOCK_EX);
            if ($rows == 0) {
                $cekall = $filterData->count();
                return $filterData->paginate($cekall);
                return $filterData->paginate($cekall, ['*'], 'page', $page);
            } else {
                // Log::info($filterData->paginate($rows));
                return $filterData->paginate($rows, $rowsNum, $page);
                return $filterData->paginate($rows, ['*'], 'page', $page);
            }
        } elseif ($id == 'data') {
            return $filterData->all();
        } else {
            // $data = $filterData->get();
            return $this->ExportToExcell($request);
        }
    }

    public function parseRequest($request, $met, $logs = 'inv')
    {
        $changeDate = $met === 'export' ? $request->param : $request->form;
        $formParse = [];
        foreach ($changeDate as $keyDet => $valueDet) {
            if ($valueDet['field']['field'] === 'created_at' || $valueDet['field']['field'] === 'CSMLOG_CREATED_AT') {
                $valueDet['val'] = $valueDet['val'] . ' 00:00:00';
                $valueDet['val2'] = empty($valueDet['val2']) ? date('Y-m-d H:i:s') : $valueDet['val2'] . ' 23:59:59';
            }

            $formParse[] = $valueDet;
        }

        $data = $logs === 'inv' ? $this->data($formParse, $request->header('userinput')) : $this->dataWMS($formParse, $request->header('userinput'));

        return $data;
    }

    public function data($filteropt, $users = '')
    {
        $getData = UserLogs::select(
            'username',
            DB::raw("CONCAT(MSTEMP_FNM, ' ', MSTEMP_LNM) as full_name"),
            'type_log',
            'log_desc',
            'created_at'
        )->leftjoin('PSI_WMS.dbo.MSTEMP_TBL', 'MSTEMP_ID', 'username');

        $data = CustomFunctionHelper::filtering($getData, $filteropt);

        $data = $users === 'bcsmt' || $users === 'bcsmttest' ? $data->whereIn('username', ['bcsmt', 'gusti'])->get()->toArray() : $data->get()->toArray();

        return $data;
        // return empty($users) ? $data->get()->toArray() : $data->where('username', $users)->get()->toArray();
    }

    public function dataWMS($filteropt, $id = '')
    {
        $getData = WMSUserLogs::select(
            '*',
            DB::raw("CONCAT(MSTEMP_FNM, ' ', MSTEMP_LNM) AS FULLNAME"),
            DB::raw('ROW_NUMBER() OVER(ORDER BY CSMLOG_LINE ASC) as no')
        )->join('MSTEMP_TBL', 'MSTEMP_ID', 'CSMLOG_CREATED_BY')
        ->where('CSMLOG_DESC', 'NOT LIKE', '%change price%');

        $data = CustomFunctionHelper::filtering($getData, $filteropt);

        return $data->get()->toArray();

        $data = !empty($id) ? $data->where('MSTEMP_ID', $id)->get()->toArray() : $data->get()->toArray();

        return $data;
        // return empty($users) ? $data->get()->toArray() : $data->where('username', $users)->get()->toArray();
    }

    public function ExportToExcell($req, $logs = 'inv')
    {
        $data = $this->parseRequest($req, 'export', $logs);
        $cekDateRange = array_filter($req->param, function ($f) use ($logs) {
            return $f['field']['field'] === ($logs == 'inv' ? 'created_at' : 'CSMLOG_CREATED_AT');
        })[0];

        $datePeriod = [date('d M Y', strtotime($cekDateRange['val'])), date('d M Y', strtotime($cekDateRange['val2']))];

        if ($req->format == 'excel') {
            if ($logs === 'inv') {
                Excel::store(new UserLogsExport($data, $datePeriod, $req->format), 'UserLogs_' . date('Y_m_d') . '.xlsx', 'public');
            } else {
                Excel::store(new WMSUserLogsExport($data, $datePeriod, $req->format), 'UserLogs_' . date('Y_m_d') . '.xlsx', 'public');
            }

            return 'storage/app/public/UserLogs_' . date('Y_m_d') . '.xlsx';
        } elseif ($req->format == 'pdf') {
            if ($logs === 'inv') {
                Excel::store(new UserLogsExport($data, $datePeriod, $req->format), 'UserLogs_' . date('Y_m_d') . '.pdf', 'public', \Maatwebsite\Excel\Excel::DOMPDF);
            } else {
                Excel::store(new WMSUserLogsExport($data, $datePeriod, $req->format), 'UserLogs_' . date('Y_m_d') . '.pdf', 'public', \Maatwebsite\Excel\Excel::DOMPDF);
            }

            return 'storage/app/public/UserLogs_' . date('Y_m_d') . '.pdf';
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
