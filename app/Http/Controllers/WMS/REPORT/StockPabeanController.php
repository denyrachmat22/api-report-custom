<?php

namespace App\Http\Controllers\WMS\REPORT;

use App\Http\Controllers\Controller;
use App\Model\RPCUST\DetailStock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\WMS\API\RCVSCN;
use App\Model\WMS\API\InvRM;
use GuzzleHttp\Client;
use App\Jobs\WMSFinishGoodCountingJobs;
use App\Model\WMS\API\StockSMT;
use App\Model\RPCUST\StockExBC;
use App\Model\WMS\REPORT\MutasiStok;
use Illuminate\Support\Facades\Storage;
use App\Jobs\dataMigrationSMTToBC;
use App\Model\MASTER\DLVMaster;
use App\Model\RPCUST\MutasiDokPabean;
use App\Model\RPCUST\scrapHist;

use App\Jobs\exBCCountingJobs;
use App\Jobs\testingSocket;

class StockPabeanController extends Controller
{
    public function sendExBCQueue(Request $r)
    {
        $sendQueue = (new exBCCountingJobs(
            $r->item_num,
            $r->tujuan,
            $r->date_out,
            $r->lot,
            $r->bc,
            $r->doc,
            $r->kontrak,
            $r->qty,
            $r->fgdo,
            $r->fgitem,
            $r->fgprice
        ));

        dispatch($sendQueue)->onQueue('exBC');

        return 'Queue Ex-BC successfully inserted';
    }

    public function testingSocket()
    {
        $sendQueue = new testingSocket;

        dispatch($sendQueue)->onQueue('test');

        return 'berhasil';
    }

    public function testingArray()
    {
        $arr = [];

        $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        foreach ($arr as $key => $value) {
            $param = [
                'item_num' => $value[1],
                'qty' => $value[2],
                'do' => $value[0],
                'adj' => false,
                'doc' => 'CLOSINGDO'
            ];

            $res = $guzz->request('POST', $endpoint, ['query' => $param]);

            $content[$key]['PARAM'] = $param;
            $content[$key]['CURL'] = json_decode($res->getBody(), true);
        }

        return $content;
    }

    public function sendingParam($item_num, $qty = 0, $do = null, $doc = null, $adj = false, $lot = null, $tujuan = null, $bc = null)
    {
        $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item_num,
            'qty' => $qty,
            'do' => $do,
            'adj' => $adj,
            'doc' => $doc,
            'lot' => $lot,
            'tujuan' => $tujuan,
            'bc' => $bc
        ];

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content;
    }

    public function sendParam2($param)
    {
        $endpoint = 'http://192.168.0.29:8081/api-report-custom/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content;
    }

    public function sendAdj($item_num, $qty)
    {
        $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item_num,
            'qty' => $qty,
            'adj' => true
        ];

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content;
    }

    public function findRCVScan($item, $lot = null, $adj = false)
    {
        if (empty($lot)) {
            $getRCVSCN = RCVSCN::select(
                'RCVSCN_DONO',
                'RCV_BCDATE',
                'RCV_ITMCD_REFF',
                'RCV_DONO_REFF',
                'RCV_KPPBC',
                'RCV_HSCD',
                'RCV_ZNOURUT',
                'RCV_PRPRC',
                'RCV_BM',
                'RCVSCN_ITMCD',
                'RCV_ZSTSRCV',
                DB::raw('(
                    SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                    FROM [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                    WHERE rb.RPSTOCK_DOC = RCVSCN_DONO
                    and rb.RPSTOCK_ITMNUM = RCVSCN_ITMCD
                    and rb.RPSTOCK_BCDATE = RCV_BCDATE
                ) as STOCK'),
                // DB::raw('COALESCE(SUM(CAST(RCVSCN_QTY as BIGINT)), 0) AS RCVSTOCK')
            )
                ->where('RCVSCN_ITMCD', $item)
                ->join('RCV_TBL', function ($j) {
                    $j->on('RCVSCN_DONO', '=', 'RCV_DONO');
                    $j->on('RCVSCN_ITMCD', '=', 'RCV_ITMCD');
                })
                // ->leftjoin('PSI_RPCUST.dbo.RPSAL_BCSTOCK', function ($j2) {
                //     $j2->on('RCVSCN_DONO', '=', 'RPSTOCK_DOC');
                //     $j2->on('RCVSCN_ITMCD', '=', 'RPSTOCK_ITMNUM');
                // })
                ->orderBy('RCV_BCDATE', $adj ? 'desc' : 'asc')
                ->groupBy(
                    'RCVSCN_DONO',
                    'RCV_BCDATE',
                    'RCV_ITMCD_REFF',
                    'RCV_DONO_REFF',
                    'RCV_KPPBC',
                    'RCV_HSCD',
                    'RCV_ZNOURUT',
                    'RCV_PRPRC',
                    'RCV_BM',
                    'RCVSCN_ITMCD',
                    'RCV_ZSTSRCV'
                );

            if (!$adj) {
                $getRCVSCN->havingRaw('(
                    SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                    FROM [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                    WHERE rb.RPSTOCK_DOC = RCVSCN_DONO
                    and rb.RPSTOCK_ITMNUM = RCVSCN_ITMCD
                    and rb.RPSTOCK_BCDATE = RCV_BCDATE
                ) > 0');
            }
        } else {
            $getRCVSCN = RCVSCN::select(
                'RCVSCN_DONO',
                'RCV_BCDATE',
                'RCV_ITMCD_REFF',
                'RCV_DONO_REFF',
                'RCV_KPPBC',
                'RCV_HSCD',
                'RCV_ZNOURUT',
                'RCV_PRPRC',
                'RCV_BM',
                'RCVSCN_LOTNO',
                'RCVSCN_ITMCD',
                'RCV_ZSTSRCV',
                // DB::raw('COALESCE(SUM(CAST(RPSTOCK_QTY as BIGINT)), 0) AS STOCK'),
                DB::raw('(
                    SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                    FROM [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                    WHERE rb.RPSTOCK_DOC = RCVSCN_DONO
                    and rb.RPSTOCK_ITMNUM = RCVSCN_ITMCD
                    and rb.RPSTOCK_BCDATE = RCV_BCDATE
                ) as STOCK'),
                // DB::raw('COALESCE(SUM(CAST(RCVSCN_QTY as BIGINT)), 0) AS RCVSTOCK')
            )
                ->where('RCVSCN_ITMCD', $item)
                ->where('RCVSCN_LOTNO', $lot)
                ->join('RCV_TBL', function ($j) {
                    $j->on('RCVSCN_DONO', '=', 'RCV_DONO');
                    $j->on('RCVSCN_ITMCD', '=', 'RCV_ITMCD');
                })
                // ->leftjoin('PSI_RPCUST.dbo.RPSAL_BCSTOCK', function ($j2) {
                //     $j2->on('RCVSCN_DONO', '=', 'RPSTOCK_DOC');
                //     $j2->on('RCVSCN_ITMCD', '=', 'RPSTOCK_ITMNUM');
                // })
                ->orderBy('RCV_BCDATE', $adj ? 'desc' : 'asc')
                ->groupBy(
                    'RCVSCN_DONO',
                    'RCV_BCDATE',
                    'RCV_ITMCD_REFF',
                    'RCV_DONO_REFF',
                    'RCV_KPPBC',
                    'RCV_HSCD',
                    'RCV_ZNOURUT',
                    'RCV_PRPRC',
                    'RCV_BM',
                    'RCVSCN_LOTNO',
                    'RCVSCN_ITMCD',
                    'RCV_ZSTSRCV'
                );

            if (!$adj) {
                $getRCVSCN->havingRaw('(
                    SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                    FROM [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                    WHERE rb.RPSTOCK_DOC = RCVSCN_DONO
                    and rb.RPSTOCK_ITMNUM = RCVSCN_ITMCD
                    and rb.RPSTOCK_BCDATE = RCV_BCDATE
                ) > 0');
            }
        }

        return $getRCVSCN;
    }

    public function testingFindRCVScan($item, $lot = null, $qty = 0)
    {
        $cekData = $this->findRCVScan($item, $lot)->count();

        $getRCVSCN = $cekData === 0
            ? clone $this->findRCVScan($item)
            : ($this->findRCVScan($item, $lot)->first()['STOCK'] < $qty
                ? clone $this->findRCVScan($item)
                : clone $this->findRCVScan($item, $lot));

        return $getRCVSCN->get();
    }

    public function arrayParsedExBC(Request $r)
    {
        ini_set('max_execution_time', 7200);
        $hasil = [];

        foreach ($r->item_num as $key => $value) {
            $req = new Request([
                'item_num' => $value,
                'tujuan' => $r->tujuan,
                'date_out' => $r->has('date_out') ? $r->date_out : '',
                'lot' => $r->has('lot') ? $r->lot[$key] : '',
                'bc' => $r->bc,
                'doc' => $r->doc,
                'kontrak' => $r->has('kontrak') || !empty($r->kontrak) ? $r->kontrak : '',
                'qty' => $r->qty[$key],
                'adj' => $r->adj,
                'isNotSaved' => $r->isNotSaved,
                'do' => $r->has('do') ? $r->do[$key] : '',
                'loc' => $r->loc
            ]);

            // $hasil[] = $r->has('adj') && $r->adj === 'true';
            // $hasil[] = $this->showavailablestock($req)->original;
            $hasil[] = $r->has('adj') && $r->adj === 'true' ? $this->reviseStock($req) : $this->calculateEXBC($req);
        }

        return $hasil;
    }

    public function reviseStock(Request $r)
    {
        $data = DB::connection('PSI_RPCUST')->table('RPSAL_BCSTOCK AS aa')->select(
            'aa.id',
            'aa.RPSTOCK_BCTYPE',
            'aa.RPSTOCK_BCNUM',
            'aa.RPSTOCK_BCDATE',
            'aa.RPSTOCK_DOC',
            'aa.RPSTOCK_ITMNUM',
            'aa.RPSTOCK_NOAJU',
            'aa.RPSTOCK_LOT',
            'aa.RPSTOCK_REMARK',
            'aa.RPSTOCK_BCDATEOUT',
            DB::raw('aa.RPSTOCK_QTY + (
                SELECT COALESCE(SUM(bb.RPSTOCK_QTY),0) FROM RPSAL_BCSTOCK bb
                WHERE CAST(bb.RPSTOCK_LOC AS INT) = aa.id
                AND bb.RPSTOCK_REMARK = RPSTOCK_REMARK
            ) AS RPSTOCK_QTY'),
            DB::raw('SUM(aa.RPSTOCK_QTY + (
                SELECT COALESCE(SUM(bb.RPSTOCK_QTY),0) FROM RPSAL_BCSTOCK bb
                WHERE CAST(bb.RPSTOCK_LOC AS INT) = aa.id
                AND bb.RPSTOCK_REMARK = RPSTOCK_REMARK
            )) OVER () AS TOTAL')
        )
            ->where('RPSTOCK_TYPE', 'OUT')
            ->where(DB::raw('aa.RPSTOCK_QTY + (
            SELECT COALESCE(SUM(bb.RPSTOCK_QTY),0) FROM RPSAL_BCSTOCK bb
            WHERE CAST(bb.RPSTOCK_LOC AS INT) = aa.id
            AND bb.RPSTOCK_REMARK = RPSTOCK_REMARK
        )'), '<', 0);

        if ($r->has('item_num') && !empty($r->item_num)) {
            $data->where('RPSTOCK_ITMNUM', $r->item_num);
        } else {
            return 'failed';
        }

        if ($r->has('doc') && !empty($r->doc)) {
            $data->where('RPSTOCK_REMARK', $r->doc);
        }

        if ($r->has('date_out') && !empty($r->date_out)) {
            $data->where('RPSTOCK_BCDATEOUT', $r->date_out);
        }

        $dataFinal = $data->orderBy('id', 'desc')->get();

        $dataFinal = json_decode(json_encode($dataFinal), true);

        if (count($dataFinal) === 0) {
            $status = false;
            $message = 'No data found !!';
            $datanya = [];
        } else {
            if ((int)$r->qty > (int)$dataFinal[0]['TOTAL'] * -1) {
                $status = false;
                $message = 'Total qty is less than revised qty, only (' . ((int)$dataFinal[0]['TOTAL'] * -1) . ') !!';
                $datanya = [];
            } else {
                if ((int)$r->qty === (int)$dataFinal[0]['TOTAL'] * -1) {
                    $status = false;
                    $message = 'Total qty is already same !!';
                    $datanya = [];
                } else {
                    $status = true;
                    $message = 'Testing';
                    $datanya = $this->storeReviseData(
                        $dataFinal,
                        $r->qty,
                        $r->has('isNotSaved') && $r->isNotSaved === 'true' ? false : true
                    );
                }
            }
        }

        return [
            'status' => $status,
            'item_num' => $r->item_num,
            'data' => $datanya,
            'message' => $message
        ];
    }

    public function storeReviseData($data, $qty, $isSaved, $hasil = [])
    {
        $nowData = current($data);
        if (!empty($nowData)) {
            $total = $qty - ($nowData['RPSTOCK_QTY'] * -1);

            $savedQty = $total > 0
                ? ($nowData['RPSTOCK_QTY'] * -1)
                : $qty;

            $dataTotal = [
                'RCV_BCTYPE' => $nowData['RPSTOCK_BCTYPE'],
                'RCV_BCNO' => $nowData['RPSTOCK_BCNUM'],
                'RCV_BCDATE' => $nowData['RPSTOCK_BCDATE'],
                'RCVSCN_DONO' => $nowData['RPSTOCK_DOC'],
                'RCVSCN_ITMCD' => $nowData['RPSTOCK_ITMNUM'],
                'RCV_RPNO' => $nowData['RPSTOCK_NOAJU'],
                'RCVSCN_LOTNO' => $nowData['RPSTOCK_LOT'],
                'SAVED_QTY' => $savedQty,
                'IS_SAVED' => $isSaved
            ];

            $hasil[] = $dataTotal;

            if ($isSaved) {
                $this->insertTransaction(
                    $dataTotal,
                    $savedQty,
                    'OUT',
                    $nowData['RPSTOCK_REMARK'],
                    $nowData['id'],
                    $nowData['RPSTOCK_BCDATEOUT']
                );
            }
            if ($total > 0) {
                next($data);
                return $this->storeReviseData($data, $total, $isSaved, $hasil);
            } else {
                return $hasil;
            }
        } else {
            return $hasil;
        }
    }

    public function reviseScrapOver()
    {
        $dataItem = [
            "201270800",
            "201328400",
            "2016542-6",
            "201874500",
            "202126600",
            "202499300",
            "202566900",
            "202621400",
            "202952800",
            "203032900",
            "203045200",
            "203049600",
            "203107200",
            "203526700",
            "203845600",
            "2039838-2",
            "2040389-0",
            "2040414-5",
            "204170300",
            "204578100",
            "205198100",
            "205198200",
            "205199600",
            "205200100",
            "205200300",
            "205302700",
            "206085600",
            "206101300",
            "206327400",
            "206478200",
            "206493100",
            "206909100",
            "206909700",
            "207097900",
            "207109100",
            "207155200",
            "207331700",
            "2075550-9",
            "207645500",
            "207646800",
            "207685600",
            "207886900",
            "208241400",
            "208284400",
            "208364000",
            "208369000",
            "208369200",
            "2083783-1",
            "208386400",
            "208445500",
            "208489700",
            "208500200",
            "208526900",
            "208725700",
            "208726900",
            "208730900",
            "208742600",
            "208789800",
            "208799200",
            "208800300",
            "208800600",
            "208803600",
            "208811500",
            "208867800",
            "208868000",
            "208868100",
            "208868200",
            "208887600",
            "208905400",
            "208991700",
            "209081300",
            "209131200",
            "209139900",
            "209140300",
            "209271900",
            "209272300",
            "209272700",
            "209272900",
            "209273000",
            "209274600",
            "209342300",
            "209349200",
            "209349500",
            "209349900",
            "209350000",
            "209351300",
            "209351600",
            "209352000",
            "209359300",
            "209367200",
            "209367300",
            "209367800",
            "209393700",
            "209401301",
            "209402400",
            "209427200",
            "209433900",
            "209440900",
            "210022500",
            "210026900",
            "210029000",
            "210033600",
            "210033800",
            "210034500",
            "210035500",
            "210035600",
            "210037800",
            "210050900",
            "210058700",
            "210066400",
            "210068700",
            "210080800",
            "210086600",
            "210091900",
            "210131700",
            "210133600",
            "210133800",
            "210289300",
            "210289500",
            "210290100",
            "210290200",
            "210290300",
            "210290400",
            "210291900",
            "210295200",
            "210295900",
            "210296000",
            "210323700",
            "210328200",
            "210437500",
            "210446800",
            "210489000",
            "210491100",
            "210514800",
            "210575000",
            "210630500",
            "210661900",
            "210687700",
            "210695400",
            "210730600",
            "210767900",
            "210808800",
            "210815600",
            "210856800",
            "210903100",
            "210971600",
            "210976300",
            "211021700",
            "211089800",
            "211101000",
            "211171700",
            "211197200",
            "211252600",
            "211256002",
            "211328500",
            "211386900",
            "211387200",
            "211387700",
            "211389200",
            "211393300",
            "211433200",
            "211463300",
            "211465300",
            "211465700",
            "211466000",
            "211466300",
            "211468000",
            "211469900",
            "211475100",
            "211480200",
            "211480900",
            "211482400",
            "211483200",
            "211484000",
            "211484800",
            "211492200",
            "211534300",
            "211534900",
            "211700200",
            "211903900",
            "211910300",
            "211913300",
            "211914500",
            "211959900",
            "211962200",
            "211962300",
            "211962400",
            "211990100",
            "212024500",
            "212034900",
            "212075700",
            "212099100",
            "212126200",
            "212159900",
            "212293200",
            "212330600",
            "212330700",
            "212330900",
            "212331000",
            "212385700",
            "212431200",
            "212488200",
            "212567300",
            "212578800",
            "212623700",
            "212641600",
            "212658300",
            "212696700",
            "212813000",
            "212879000",
            "212899200",
            "212901200",
            "212921300",
            "213026200",
            "213033701",
            "213039500",
            "213055100",
            "213071700",
            "213088100",
            "213126400",
            "213178300",
            "213193500",
            "213209600",
            "213209800",
            "213220600",
            "213253000",
            "213301700",
            "213341600",
            "213341700",
            "213348900",
            "213354400",
            "213441300",
            "213468300",
            "213548100",
            "213578400",
            "213627200",
            "213687800",
            "213907900",
            "213911400",
            "213933200",
            "213935900",
            "213961700",
            "213962200",
            "213968500",
            "214033300",
            "214034500",
            "214236700",
            "214247700",
            "214251400",
            "214397200",
            "214397700",
            "214456400",
            "214521300",
            "214535200",
            "214571100",
            "214608400",
            "214672000",
            "214781200",
            "214781300",
            "214817600",
            "214857300",
            "214927400",
            "214982800",
            "214989400",
            "215014800",
            "215075801",
            "215150400",
            "215273500",
            "215349000",
            "215367500",
            "215386100",
            "215480201",
            "215480301",
            "215480601",
            "215481001",
            "215491400",
            "215494300",
            "215495500",
            "215496100",
            "215582400",
            "215723200",
            "215765300",
            "215793800",
            "215824500",
            "215825300",
            "215827700",
            "215884600",
            "215945600",
            "216044800",
            "216044900",
            "216045000",
            "216252500",
            "216261500",
            "216272500",
            "216276200",
            "216309400",
            "216309500",
            "216310200",
            "216313700",
            "216320900",
            "216332200",
            "216429200",
            "216434900",
            "216486200",
            "216511800",
            "216528900",
            "216624700",
            "216674700",
            "216701600",
            "216707200",
            "216717400",
            "216717500",
            "216730100",
            "216759900",
            "216843500",
            "216942400",
            "216942600",
            "216942700",
            "216975800",
            "216975900",
            "216976100",
            "216976200",
            "216976500",
            "216976700",
            "216976800",
            "216988600",
            "217103200",
            "217103400",
            "217103500",
            "217103800",
            "217115700",
            "217117200",
            "217137400",
            "217137500",
            "217142800",
            "217146200",
            "217246600",
            "217304200",
            "217408900",
            "217412700",
            "217440300",
            "217485700",
            "217527400",
            "217550900",
            "217568900",
            "217575500",
            "217637200",
            "217637300",
            "217653600",
            "217681700",
            "217719900",
            "217745000",
            "217889800",
            "217943400",
            "217959800",
            "217972200",
            "217972400",
            "217999600",
            "218052100",
            "218088600",
            "218131400",
            "218145900",
            "218235300",
            "218264800",
            "218296800",
            "218302900",
            "218472500",
            "2185992-8",
            "218600400",
            "218612000",
            "218625900",
            "218727400",
            "218727700",
            "218727800",
            "218727900",
            "218728000",
            "218728100",
            "218728200",
            "218980700",
            "218987100",
            "218995500",
            "219007700",
            "219134600",
            "219134800",
            "219137600",
            "219138500",
            "219138600",
            "219138800",
            "219138900",
            "219139200",
            "219139300",
            "219237500",
            "219238300",
            "219240500",
            "219241500",
            "219244900",
            "219245200",
            "219286900",
            "219323500",
            "219327300",
            "219327400",
            "219332500",
            "219358300",
            "219500300",
            "219500400",
            "219500500",
            "219500700",
            "219500800",
            "219500900",
            "219501000",
            "219501200",
            "219501400",
            "219501500",
            "219501700",
            "219502100",
            "219641700",
            "219641800",
            "219642000",
            "219819900",
            "219831000",
            "219831100",
            "219858300",
            "219858400",
            "219858500",
            "219858700",
            "219861300",
            "219955700",
            "219957300",
            "220179600",
            "221204100",
            "221307800",
            "221446601",
            "2255552-3",
            "2288555-8",
            "3014072-3",
            "3038466-5",
            "3058728-0",
            "5337103-0",
            "9539981-3",
            "E05072",
            "E06412",
            "E06653",
            "E13718",
            "E18060-04",
            "E18324-04",
            "M81417-05",
            "M81422-06",
            "M89024-02"
        ];

        $dataQty = [
            3101,
            248,
            2097,
            113645,
            4599,
            16504,
            44385,
            4033,
            87039,
            5488,
            8586,
            37245,
            6192,
            11043,
            4288,
            5453,
            2272,
            18478,
            5927,
            1759,
            37254,
            4324,
            4627,
            5277,
            6757,
            238,
            2406,
            5291,
            3501,
            5356,
            6360,
            86344,
            5828,
            12769,
            5680,
            55900,
            5808,
            1855,
            52342,
            12278,
            4355,
            77480,
            91290,
            5574,
            4614,
            1598,
            5087,
            5817,
            1070,
            27516,
            4842,
            30051,
            532,
            3489,
            40646,
            6153,
            8784,
            5589,
            3799,
            339660,
            11094,
            20864,
            106215,
            5821,
            73968,
            49108,
            116406,
            255730,
            18585,
            1691235,
            1014550,
            14642,
            3357612,
            13220,
            13377,
            18975,
            55778,
            17427,
            49770,
            19888,
            56672,
            278405,
            19649,
            42934,
            16296,
            99750,
            11371,
            11247,
            106479,
            13746,
            12332,
            59972,
            3210,
            9600,
            1239469,
            50512,
            599928,
            162029,
            16103,
            13820,
            44178,
            10100,
            14367,
            11851,
            45190,
            281685,
            12727,
            14767,
            45442,
            15722,
            15476,
            10923,
            15230,
            11330,
            12362,
            13678,
            59366,
            5309,
            22226,
            14613,
            52840,
            14176,
            15555,
            898956,
            36453,
            59570,
            274275,
            268011,
            34818,
            2462,
            13723,
            4312,
            1088,
            16879,
            54368,
            1673,
            18813,
            86190,
            29682,
            3337,
            52966,
            1685,
            526163,
            11999,
            3744,
            29922,
            6977,
            16998,
            3603,
            1168,
            52708,
            23510,
            4878,
            1350,
            10848,
            5973,
            6715,
            7068,
            6460,
            6944,
            7193,
            51968,
            11227,
            16228,
            10733,
            13560,
            17618,
            12001,
            13457,
            11194,
            27549,
            15489,
            10715,
            16519,
            43526,
            14540,
            3315,
            7545,
            11016,
            9591,
            46046,
            11463,
            5174,
            6747,
            40742,
            12497,
            14409,
            130644,
            10584,
            22244,
            185380,
            4120,
            11567,
            19764,
            13536,
            1291983,
            16080,
            9286110,
            2084012,
            6917,
            5350,
            25948,
            40980,
            29775,
            165856,
            13700,
            64,
            6688,
            6553,
            769,
            2249,
            11079,
            14648,
            899649,
            533,
            17977,
            41526,
            26314,
            4818,
            4349,
            32646,
            17992,
            55972,
            51000,
            62603,
            3849,
            15908,
            4077,
            13770,
            2623280,
            5625,
            19623,
            5355,
            3578,
            11926,
            656704,
            18612,
            2166,
            4351,
            2648,
            151438,
            17840,
            12052,
            1097,
            4360,
            1148,
            5317,
            537,
            14408,
            35169,
            13360,
            535797,
            5167,
            5747,
            87385,
            403110,
            7712,
            87530,
            4194,
            4261,
            4156,
            4785,
            47424,
            9938,
            5529,
            354,
            1222,
            57768,
            8476,
            3959,
            1166,
            4262,
            1477,
            1359,
            19680,
            1238,
            4078,
            15276,
            283685,
            3160,
            12328,
            8620,
            11570,
            18204,
            3089,
            19244,
            8737,
            1276,
            3903,
            4125,
            4287,
            426528,
            7309,
            2847,
            5844,
            6501,
            7466,
            6880,
            6046,
            7030,
            5727,
            4872,
            6832,
            1471,
            4665,
            11575,
            703,
            52078,
            10998,
            3999,
            3571,
            3040,
            2928,
            10168,
            4617,
            10537,
            5745,
            4833,
            5746,
            9378,
            8637,
            7344,
            6793,
            6576,
            5291,
            8212,
            3428,
            6370,
            10086,
            19446,
            2726,
            2016,
            12457,
            12113,
            4186,
            1901,
            97581,
            1335,
            8400,
            5155,
            46,
            2374,
            1237,
            3746,
            69879,
            434,
            3966,
            1985,
            15936,
            12680,
            4736,
            12564,
            26446,
            2525,
            2106,
            8792,
            2034,
            1950,
            2580,
            1685,
            76344,
            146,
            147312,
            1057,
            4293,
            10328,
            2860,
            130,
            24195,
            28287,
            19902,
            76908,
            11542,
            18510,
            2949,
            4456200,
            18302,
            22594,
            21774,
            20721,
            1741,
            1732,
            164472,
            1300596,
            187540,
            205093,
            5075,
            6189,
            6949,
            5128,
            22509,
            18700,
            200039,
            3735,
            3474,
            18761,
            289840,
            1934,
            4165,
            56500,
            2746,
            5450,
            19726,
            5654,
            4882,
            1561,
            2101,
            5264,
            7052,
            11793,
            1809,
            4886,
            2085,
            1658,
            1545,
            52258,
            14721,
            272675,
            4074,
            1258521,
            874620,
            43598,
            2019598,
            2356950,
            26534,
            55718,
            11437,
            4679,
            4939,
            16956,
            77,
            270,
            840,
            396,
            7001,
            20538,
            1353,
            1098,
            974,
            22630,
            5866,
            2886,
            13562,
            152,
            418,
            376,
            376,
            1720
        ];

        $data = scrapHist::whereIn('ITEMNUM', $dataItem)->where('ID_TRANS', '21/12/04/0003')->get();

        $hasil = [];
        foreach ($data as $key => $value) {
            if ($value['QTY'] <> $dataQty[$key]) {
                $hasil[] = [
                    'status' => false,
                    'item' => $value['ITEMNUM'],
                    'qty_scrap' => $value['QTY'],
                    'needed_dispose' => $dataQty[$key]
                ];
            } else {
                $hasil[] = [
                    'status' => true,
                    'item' => $value['ITEMNUM'],
                    'qty_scrap' => $value['QTY'],
                    'needed_dispose' => $dataQty[$key]
                ];
            }
        }

        return $hasil;
    }

    public function showavailablestock(Request $r)
    {
        $select = [
            'RPSTOCK_ITMNUM',
            'RPSTOCK_BCTYPE',
            'RPSTOCK_BCNUM',
            'RPSTOCK_BCDATE',
            'RPSTOCK_NOAJU',
            'RPSTOCK_DOC',
            'RPSTOCK_LOT',
            'RCV_KPPBC',
            'RCV_HSCD',
            'RCV_ZNOURUT',
            'RCV_PRPRC',
            'RCV_BM',
            'MITM_ITMD1',
            'MITM_STKUOM'
        ];

        if (!$r->has('adj') || empty($r->adj)) {
            if (!$r->has('lot') || empty($r->lot)) {
                $getRCVSCN = $this->findRCVScan($r->item_num);
            } else {
                $cekData = $this->findRCVScan($r->item_num, $r->lot)->count();

                $getRCVSCN = $cekData === 0
                    ? clone $this->findRCVScan($r->item_num)
                    : ($this->findRCVScan($r->item_num, $r->lot)->first()['STOCK'] < $r->qty
                        ? clone $this->findRCVScan($r->item_num)
                        : clone $this->findRCVScan($r->item_num, $r->lot));
            }

            if (($r->has('tujuan') && !empty($r->tujuan)) && ($r->has('bc') && !empty($r->tujuan))) {
                if ($r->bc === '27') {
                    if ($r->tujuan == 6) {
                        $getRCVSCN->where('RCV_ZSTSRCV', '2');
                    } else {
                        $getRCVSCN->where(function ($w) {
                            $w->where('RCV_ZSTSRCV', '<>', '2');
                            $w->orWhereNull('RCV_ZSTSRCV');
                        });
                    }
                }
            }

            if ($r->bc == '41') {
                if ($r->has('kontrak') || !empty($r->kontrak)) {
                    $getRCVSCN->where('RCV_CONA', $r->kontrak);
                } else {
                    $getRCVSCN->where('RCV_CONA', '<>', 'NULL');
                }
            }

            if ($r->has('do') || !empty($r->do)) {
                $getRCVSCN->where('RCVSCN_DONO', $r->do);
            }
        } else {
            $getRCVSCN = $this->findRCVScan($r->item_num, null, true);
            if ($r->has('do') || !empty($r->do)) {
                $getRCVSCN->where('RCVSCN_DONO', $r->do);
            }

            if ($r->has('item_num') || !empty($r->item_num)) {
                $getRCVSCN->where('RCVSCN_ITMCD', $r->item_num);
            }

            if ($r->has('less_than') || !empty($r->less_than)) {
                $getRCVSCN->where('RCVSCN_LUPDT', '<', $r->less_than);
            }
        }

        $dataAwal = $getRCVSCN->get();

        // return $dataAwal;

        $test = DetailStock::select(array_merge($select, [
            DB::raw('SUM(RPSTOCK_QTY) RPSTOCK_QTY'),
            DB::raw('(
                SELECT
                SUM(rt.RCVSCN_QTY)
                FROM PSI_WMS.dbo.RCVSCN_TBL rt
                WHERE rt.RCVSCN_SAVED = 1
                AND rt.RCVSCN_ITMCD = RPSTOCK_ITMNUM
                AND rt.RCVSCN_DONO = RPSTOCK_DOC
            ) as STOCK_RCVSCN')
        ]))
            ->join('PSI_WMS.dbo.RCV_TBL', function ($f) {
                $f->on('RPSTOCK_ITMNUM', 'RCV_ITMCD');
                $f->on('RPSTOCK_DOC', 'RCV_DONO');
                $f->on('RPSTOCK_BCNUM', 'RCV_BCNO');
                $f->on('RPSTOCK_NOAJU', 'RCV_RPNO');
            })
            ->join('PSI_WMS.dbo.XMITM_V', 'MITM_ITMCD', '=', 'RPSTOCK_ITMNUM');

        $hasil = [];
        foreach ($dataAwal as $key => $value) {
            $do = !empty($value['RCV_DONO_REFF']) ? $value['RCV_DONO_REFF'] : $value['RCVSCN_DONO'];
            $item = !empty($value['RCV_ITMCD_REFF']) ? $value['RCV_ITMCD_REFF'] : $value['RCVSCN_ITMCD'];

            $hasil[] = ['do' => $do, 'item' => $item];
            if ($key === 0) {
                $test->where(function ($w) use ($do, $item) {
                    $w->where('RPSTOCK_DOC', $do);
                    $w->where('RPSTOCK_ITMNUM', $item);
                });
            } else {
                $test->orwhere(function ($w) use ($do, $item) {
                    $w->where('RPSTOCK_DOC', $do);
                    $w->where('RPSTOCK_ITMNUM', $item);
                });
            }
        }

        // if ($r->has('date_out') && !empty($r->date_out)) {
        //     $test->whereBetween('created_at', [$r->date_out.'00:00:00', $r->date_out.'23:59:59']);
        // }

        // Cek tanggal terlama dari BC
        if ($r->has('adj') && $r->adj == true) {
            $cek = $test->orderBy('RPSTOCK_BCDATE', 'DESC')
                ->orderBy('RPSTOCK_QTY', 'DESC')
                ->groupBy($select)
                ->get()
                ->toArray();
        } else {
            $cek = $test->havingRaw('SUM(RPSTOCK_QTY) > 0')
                ->orderBy('RPSTOCK_BCDATE', 'ASC')
                ->orderBy('RPSTOCK_QTY', 'DESC')
                ->groupBy($select)
                ->get()
                ->toArray();
        }

        // return $cek;

        // return $test->havingRaw('SUM(RPSTOCK_QTY) > 0')
        // ->orderBy('RPSTOCK_BCDATE', 'ASC')
        // ->orderBy('RPSTOCK_QTY', 'DESC')
        // ->groupBy($select)
        // ->toSql();

        $totQty = 0;
        if ($r->has('adj') || !empty($r->adj)) {
            foreach ($cek as $keyData => $valueData) {
                $totQty += $valueData['RPSTOCK_QTY'];
            }
        }

        if ($r->has('remark')) {
            $remark = $r->remark;
        } else {
            $remark = null;
        }

        if ($r->has('adj') && $r->adj == true) {
            $typeoutincsrc = ['OUT-ADJ', NULL];
            $locoutincsrc = ['WH-RM', NULL];
        } else {
            if ($r->has('scrap') && $r->scrap == true) {
                $typeoutincsrc = ['OUT', 'INC-SCR'];
                $locoutincsrc = ['WH-RM', 'WH-SCR-RM'];
            } else {
                // Old
                // $typeoutincsrc = ['OUT', 'INC-DO'];
                // $locoutincsrc = ['WH-RM', 'WH-DO'];

                // New
                $typeoutincsrc = ['OUT', NULL];
                $locoutincsrc = ['WH-RM', NULL];

                $remark = [$r->doc];
            }
        }

        if ($r->has('revise') && $r->revise === true) {
            $rev = true;
        } else {
            $rev = false;
        }

        // return $cek;

        // Start compiling to recursive
        if (!empty($cek) && count($hasil) > 0) {
            $qty = !$r->has('adj') || empty($r->adj) || $r->adj === false
                ? $r->qty
                : ($r->qty < 0
                    ? $r->qty
                    : $totQty - $r->qty);
            $hasil = $this->comparedata(
                $r->doc,
                $r->bc,
                $cek,
                $qty,
                $r->item_num,
                [],
                $r->lot,
                $typeoutincsrc,
                $remark,
                $locoutincsrc,
                $rev,
                $r->has('adj') || $r->adj == true,
                $r->has('date_out') && !empty($r->date_out) ? $r->date_out : date('Y-m-d')
            );

            // return $hasil;
            if (count($hasil) == 0) {
                return response()->json([
                    'status' => 'failed',
                    'data' => [],
                    'message' => 'no item found with enough stock!!'
                ]);
            }

            // return $hasil;

            //Tester

            $hasil_final = [];
            foreach ($hasil as $key => $value) {
                $hasil_final[] = [
                    'BC_TYPE' => $value['RPSTOCK_BCTYPE'],
                    'BC_NUM' => $value['RPSTOCK_BCNUM'],
                    'BC_AJU' => $value['RPSTOCK_NOAJU'],
                    'BC_DATE' => $value['RPSTOCK_BCDATE'],
                    'BC_QTY' => $value['RPSTOCK_QTY'],
                    'BC_DO' => $value['DO_ASAL'],
                    'BC_ITEM' => strtoupper(trim($value['RPSTOCK_ITMNUM'])),
                    'QTY_SISA' => $value['QTY_SISA'],
                    'RCV_KPPBC' => $value['RCV_KPPBC'],
                    'RCV_HSCD' => $value['RCV_HSCD'],
                    'RCV_ZNOURUT' => $value['RCV_ZNOURUT'],
                    'RCV_PRPRC' => $value['RCV_PRPRC'],
                    'RCV_BM' => $value['RCV_BM'],
                    'MITM_ITMD1' => trim($value['MITM_ITMD1']),
                    'MITM_STKUOM' => trim($value['MITM_STKUOM']),
                    'OUT_BCDATE' => $value['RPSTOCK_BCDATEOUT']
                ];
            }

            return response()->json([
                'status' => 'success',
                'data' => $hasil_final
            ]);
        } else {
            return response()->json([
                'status' => 'failed',
                'data' => [],
                'message' => 'no item found on RCVSCN!!'
            ]);
        }

        return $cek;
    }

    public function comparedata($doc, $bcout, $arr, $qty, $item, $currentarr = [], $lot = '', $inoutarr = [], $remark = [''], $loc = [''], $rev = false, $adj = false, $dateOut = '')
    {
        $totalarray = $currentarr;
        $cekdatanow = current($arr);

        if (!empty($cekdatanow)) {
            $tot = $adj ? round($qty) + $cekdatanow['STOCK_RCVSCN'] : round($qty) - $cekdatanow['RPSTOCK_QTY'];
            $hasil = $adj
                ? ($qty < 0
                    ? ($qty + (int)$cekdatanow['RPSTOCK_QTY'] < 0
                        ? ((int)$cekdatanow['STOCK_RCVSCN'] > $qty * -1
                            ? $qty
                            : (int)$cekdatanow['STOCK_RCVSCN'] * -1 + (int)$cekdatanow['RPSTOCK_QTY'])
                        : $qty + (int)$cekdatanow['RPSTOCK_QTY'])
                    : ($qty > $cekdatanow['RPSTOCK_QTY'] ? $cekdatanow['RPSTOCK_QTY'] : round($qty)))
                : ($qty > $cekdatanow['RPSTOCK_QTY'] ? $cekdatanow['RPSTOCK_QTY'] : round($qty));

            // return [$hasil, $tot];
            // return [$hasil, $qty + (int)$cekdatanow['RPSTOCK_QTY'] > 0, $qty, $cekdatanow['RPSTOCK_QTY'], $cekdatanow['STOCK_RCVSCN'], $qty + (int)$cekdatanow['RPSTOCK_QTY']];

            if ($qty !== 0) {
                // Deducted stock incoming
                DetailStock::create([
                    'RPSTOCK_BCTYPE' => $cekdatanow['RPSTOCK_BCTYPE'],
                    'RPSTOCK_BCNUM' => $cekdatanow['RPSTOCK_BCNUM'],
                    'RPSTOCK_BCDATE' => $cekdatanow['RPSTOCK_BCDATE'],
                    'RPSTOCK_QTY' => $rev === true ? $hasil : $hasil * -1,
                    'RPSTOCK_DOC' => $cekdatanow['RPSTOCK_DOC'],
                    'RPSTOCK_TYPE' => $inoutarr[0],
                    'RPSTOCK_ITMNUM' => $cekdatanow['RPSTOCK_ITMNUM'],
                    'RPSTOCK_NOAJU' =>  $cekdatanow['RPSTOCK_NOAJU'],
                    'RPSTOCK_LOT' =>  $cekdatanow['RPSTOCK_LOT'],
                    'RPSTOCK_DOINC' => $cekdatanow['RPSTOCK_DOC'],
                    'RPSTOCK_REMARK' => is_array($remark) ? $remark[0] : '',
                    'RPSTOCK_LOC' => $loc[0],
                    'RPSTOCK_BCDATEOUT' => $dateOut
                ]);

                // IF not adjustment add to doc location
                if ($inoutarr[1]) {
                    DetailStock::create([
                        'RPSTOCK_BCTYPE' => $bcout,
                        'RPSTOCK_BCNUM' => $cekdatanow['RPSTOCK_BCNUM'],
                        'RPSTOCK_BCDATE' => $cekdatanow['RPSTOCK_BCDATE'],
                        'RPSTOCK_QTY' => $rev === true ? $hasil * -1 : $hasil,
                        'RPSTOCK_DOC' => $doc,
                        'RPSTOCK_TYPE' => $inoutarr[1],
                        'RPSTOCK_ITMNUM' => $cekdatanow['RPSTOCK_ITMNUM'],
                        'RPSTOCK_NOAJU' =>  $cekdatanow['RPSTOCK_NOAJU'],
                        'RPSTOCK_LOT' =>  $lot,
                        'RPSTOCK_DOINC' => $cekdatanow['RPSTOCK_DOC'],
                        'RPSTOCK_REMARK' => is_array($remark) ? $remark[1] : $remark,
                        'RPSTOCK_LOC' => $loc[0],
                        'RPSTOCK_BCDATEOUT' => $dateOut
                    ]);
                }
            }

            $arrhasil = [
                'RPSTOCK_BCTYPE' => $cekdatanow['RPSTOCK_BCTYPE'],
                'RPSTOCK_BCNUM' => $cekdatanow['RPSTOCK_BCNUM'],
                'RPSTOCK_BCDATE' => $cekdatanow['RPSTOCK_BCDATE'],
                'RPSTOCK_QTY' => $hasil,
                'RPSTOCK_DOC' => $doc,
                'RPSTOCK_TYPE' => $inoutarr[0],
                'RPSTOCK_ITMNUM' => $cekdatanow['RPSTOCK_ITMNUM'],
                'RPSTOCK_NOAJU' =>  $cekdatanow['RPSTOCK_NOAJU'],
                'RPSTOCK_LOT' =>  $lot,
                'DO_ASAL' => $cekdatanow['RPSTOCK_DOC'],
                'QTY_SISA' => $cekdatanow['RPSTOCK_QTY'] - $hasil,
                'RCV_KPPBC' => $cekdatanow['RCV_KPPBC'],
                'RCV_HSCD' => $cekdatanow['RCV_HSCD'],
                'RCV_ZNOURUT' => $cekdatanow['RCV_ZNOURUT'],
                'RCV_PRPRC' => $cekdatanow['RCV_PRPRC'],
                'RCV_BM' => $cekdatanow['RCV_BM'],
                'MITM_ITMD1' => trim($cekdatanow['MITM_ITMD1']),
                'MITM_STKUOM' => trim($cekdatanow['MITM_STKUOM']),
                'RPSTOCK_BCDATEOUT' => $dateOut
            ];

            if ($adj) {
                if ($qty < 0) {
                    next($arr);
                    return $this->comparedata($doc, $bcout, $arr, $tot, $item, count($totalarray) > 0 ? array_merge($totalarray, [$arrhasil]) : [$arrhasil], $lot, $inoutarr, $remark, $loc, $rev, $adj, $dateOut);
                } else {
                    return count($totalarray) > 0 ? array_merge($totalarray, [$arrhasil]) : [$arrhasil];
                }
            } else {
                if ($cekdatanow['RPSTOCK_QTY'] < $qty) {
                    next($arr);
                    return $this->comparedata($doc, $bcout, $arr, $tot, $item, count($totalarray) > 0 ? array_merge($totalarray, [$arrhasil]) : [$arrhasil], $lot, $inoutarr, $remark, $loc, $rev, $adj, $dateOut);
                } else {
                    return count($totalarray) > 0 ? array_merge($totalarray, [$arrhasil]) : [$arrhasil];
                }
            }
        } else {
            return $totalarray;
        }
    }

    public function cancelDoc($doOut, $loc = '')
    {
        ini_set('max_execution_time', 7200);

        // NEW
        $cekData = DetailStock::where('RPSTOCK_REMARK', base64_decode($doOut))
            ->where('RPSTOCK_TYPE', 'OUT');

        if (!empty($loc)) {
            $cekData->where('RPSTOCK_LOC', base64_decode($loc));
        }

        $deleteExBC = $cekData->delete();
        $deleteMutasi = MutasiDokPabean::where('RPBEA_NUMSJL', base64_decode($doOut))->delete();

        return response()->json([
            'status' => 'success',
            'data' => [
                'EXBC' => $deleteExBC,
                'Mutasi' => $deleteMutasi
            ],
            'message' => 'DO Stock successfully revised'
        ]);
        // return $cekData->get()->toArray();

        if (count($cekData->get()->toArray()) === 0) {
            return response()->json([
                'status' => 'failed',
                'data' => [],
                'message' => 'No DO reference found !!'
            ]);
        } else {
            $hasil = [];
            foreach ($cekData->get()->toArray() as $key => $value) {
                $hasil[] = DetailStock::where('RPSTOCK_DOINC', $value['RPSTOCK_DOINC'])
                    ->where('RPSTOCK_NOAJU', $value['RPSTOCK_NOAJU'])
                    ->where('RPSTOCK_ITMNUM', $value['RPSTOCK_ITMNUM'])
                    ->where('RPSTOCK_REMARK', $value['RPSTOCK_REMARK'])
                    ->where('RPSTOCK_TYPE', 'OUT')
                    ->delete();

                // Cek DO
                $cekDO = DLVMaster::where('DLV_ID', base64_decode($doOut))->first();
                if ($cekDO) {
                    MutasiDokPabean::where('RPBEA_NUMSJL', base64_decode($doOut))->delete();
                }

                // OLD
                $delete = DetailStock::where('RPSTOCK_DOC', base64_decode($doOut))
                    ->where('RPSTOCK_BCTYPE', $value['RPSTOCK_BCTYPE'])
                    ->where('RPSTOCK_BCNUM', $value['RPSTOCK_BCNUM'])
                    ->where('RPSTOCK_LOT', $value['RPSTOCK_LOT'])
                    ->where('RPSTOCK_DOINC', $value['RPSTOCK_DOINC'])
                    ->where('RPSTOCK_TYPE', 'INC-DO');

                if (!empty($loc)) {
                    $delete->where('RPSTOCK_LOC', base64_decode($loc));
                }

                $hasil[] = $delete->delete();
            }

            return response()->json([
                'status' => 'success',
                'data' => $hasil,
                'message' => 'DO Stock successfully revised'
            ]);
        }
    }

    public function exBCItemAdjustment($item, $qty)
    {
        ini_set('max_execution_time', -1);
        $qtynya = base64_decode($qty);
        $getInv = InvRM::select([
            'cPartCode',
            DB::raw('SUM(cQty) AS QTY')
        ])
            // ->where('cPartCode', base64_decode($item))
            ->groupBy('cPartCode');

        $dataAwal = $getInv->get();

        $hasil = [];
        foreach ($dataAwal as $key => $value) {
            $cekStock = DetailStock::select([
                'RPSTOCK_ITMNUM',
                DB::raw('SUM(RPSTOCK_QTY) as QTY_STOCK')
            ])
                ->where('RPSTOCK_ITMNUM', $value['cPartCode'])
                ->where('RPSTOCK_TYPE', '<>', 'INC-DO')
                ->groupBy('RPSTOCK_ITMNUM')
                ->first();

            $hasil[] = [
                'item_num' => $value['cPartCode'],
                'stock_taking' => $value['QTY'],
                'stock' => $cekStock['QTY_STOCK'],
                'api' => [
                    'status' => $value['QTY'] - $cekStock['QTY_STOCK'] === 0
                        ? 'Already Balanced'
                        : ($value['QTY'] > $cekStock['QTY_STOCK']
                            ? 'Another Stock is on ICS'
                            : 'Stock has been updated'),
                    'result' => !is_null($cekStock['QTY_STOCK'])
                        ? ($value['QTY'] - $cekStock['QTY_STOCK'] !== 0
                            ? $this->sendingParam($value['cPartCode'], $value['QTY'], null, 'STOCK_TAKING_ADJ', true)
                            : 'Already Balanced')
                        : 'No Data on Stock'
                ]
            ];
        }

        return $hasil;
    }

    public function findStokItem($item)
    {
        $cekStock = DetailStock::select([
            'RPSTOCK_ITMNUM',
            'RPSTOCK_BCNUM',
            'RPSTOCK_BCTYPE',
            'RPSTOCK_BCDATE',
            'MITM_ITMD1',
            'MITM_ITMD2',
            'MITM_SPTNO',
            DB::raw('SUM(RPSTOCK_QTY) as QTY_STOCK')
        ])
            ->where('RPSTOCK_ITMNUM', 'like', '%' . $item . '%')
            ->where('RPSTOCK_TYPE', '<>', 'INC-DO')
            ->join('PSI_WMS.dbo.XMITM_V', 'MITM_ITMCD', '=', 'RPSTOCK_ITMNUM')
            ->groupBy(
                'RPSTOCK_BCNUM',
                'RPSTOCK_ITMNUM',
                'RPSTOCK_LOT',
                'RPSTOCK_BCTYPE',
                'RPSTOCK_BCDATE',
                'MITM_ITMD1',
                'MITM_ITMD2',
                'MITM_SPTNO'
            )
            ->havingRaw('SUM(RPSTOCK_QTY) > 0')
            ->orderBy('RPSTOCK_BCDATE', 'asc')
            ->get()
            ->toArray();

        $hasil = [];
        foreach ($cekStock as $key => $value) {
            $hasil[] = [
                'ITEM_NUM' => $value['RPSTOCK_ITMNUM'],
                'ITEM_DESC' => trim($value['MITM_ITMD1']) . ' ' . trim($value['MITM_ITMD2']) . '(' . trim($value['MITM_SPTNO']) . ')',
                'BC_TYPE' => $value['RPSTOCK_BCTYPE'],
                'BC_NO_PEN' => $value['RPSTOCK_BCNUM'],
                'BC_DATE' => $value['RPSTOCK_BCDATE'],
                'QTY_STOCK' => $value['QTY_STOCK']
            ];
        }

        return $hasil;

        return $cekStock;
    }

    public function prepMigration($item = null)
    {
        set_time_limit(3600);
        $dataMigrate1 = '(SELECT COALESCE(SUM(Quantity), 0) as qty from migrasi202103 where ItemCode = SERD2_TBL.SERD2_ITMCD) as STOCK_EXC';
        $dataMigrate2 = '(SELECT COALESCE(SUM(Quantity), 0) as qty from migrasiOMC202103 where ItemCode = SERD2_TBL.SERD2_ITMCD) as STOCK_EXC';
        $dataMigrate3 = '(SELECT COALESCE(SUM(Quantity), 0) as qty from migrasiOMI202103 where ItemCode = SERD2_TBL.SERD2_ITMCD) as STOCK_EXC';
        $dataMigrate4 = '(SELECT COALESCE(SUM(Quantity), 0) as qty from migrasiIEI202103 where ItemCode = SERD2_TBL.SERD2_ITMCD) as STOCK_EXC';
        $getData2 = DB::query()
            ->fromRaw("(select ITH_SER,sum(ITH_QTY) stkqty from ITH_TBL where ITH_WH='AFWH3' OR ITH_WH='ARSHP' group by ITH_SER, ITH_WH having sum(ITH_QTY)>0) vserstock")
            ->select(
                DB::raw("
                SERD2_TBL.SERD2_ITMCD,
                SUM(SERD2_QTY) AS SERD2,
                (SELECT COALESCE(SUM(Quantity), 0) as qty from migrasi202103 where ItemCode = SERD2_TBL.SERD2_ITMCD) as STOCK_EXC,
                $dataMigrate4,
                (SELECT SUM(RPSTOCK_QTY) FROM PSI_RPCUST.dbo.RPSAL_BCSTOCK where RPSTOCK_ITMNUM = SERD2_TBL.SERD2_ITMCD) AS TOT_BCSTOCK,
                SUM(SERD2_QTY) + (SELECT COALESCE(SUM(Quantity), 0) as qty from migrasi202103 where ItemCode = SERD2_TBL.SERD2_ITMCD)AS TOT_ADJ
            ")
            )
            ->leftJoin('SERD2_TBL', 'ITH_SER', 'SERD2_SER')
            ->groupBy('SERD2_TBL.SERD2_ITMCD');

        if (empty($item)) {
            $getData = $getData2->get()->toArray();
        } else {
            $getData = $getData2->where('SERD2_ITMCD', $item)->get()->toArray();
        }

        // return $getData;

        $hasil = [];
        foreach ($getData as $key => $value) {
            if ($value->SERD2 > $value->TOT_BCSTOCK) {
                $hasil[] = [
                    'status' => 'failed',
                    'msg' => 'SERD2 > STOCK BC'
                ];
            } else {
                $hasil[] = [
                    'status' => 'success',
                    'msg' => 'Stock adjusted',
                    'data' => $this->sendingParam($value->SERD2_ITMCD, $value->TOT_ADJ, null, null, true)
                ];
            }
        }

        return $hasil;
    }

    public function calculate_raw_material_resume(Request $req)
    {
        $queueInsert = (new WMSFinishGoodCountingJobs($req->inunique, $req->inunique_qty, $req->inunique_job));

        dispatch($queueInsert)->onQueue('WMSFGCounting');

        return 'Process queue success !';
    }

    public function adjustmentStockWithWMS()
    {
        set_time_limit(14400);
        $cekDataSMT = StockSMT::select(
            'ITH_ITMCD',
            DB::raw('SUM(STKQTY) AS STKQTY')
        )
            ->groupBy('ITH_ITMCD')->get()->toArray();

        foreach ($cekDataSMT as $key => $value) {
            dataMigrationSMTToBC::dispatch($value['ITH_ITMCD'], $value['STKQTY'], '2021-03-28')->onQueue('migrationsStock');
        }

        return 'Berhasil';
    }

    // New calculate ExBC
    public function calculateEXBC(Request $r)
    {
        // ini_set('memory_limit', '2G');

        // logger($r);

        if (!$r->has('adj') || empty($r->adj)) {
            if ($r->has('kontrak') && !empty($r->kontrak)) {
                if (!$r->has('lot') || empty($r->lot)) {
                    if ($r->has('usedEXBC') && $r->has('usedEXBC') == 'true') {
                        $getRCVSCN = $this->findRCVScan2WithoutLot($r->item_num, null, null, $r->kontrak, true);
                    } else {
                        $getRCVSCN = $this->findRCVScan2WithoutLot($r->item_num, null, null, $r->kontrak);
                    }
                } else {
                    if ($r->has('usedEXBC') && $r->has('usedEXBC') == 'true') {
                        $getRCVSCN = $this->findRCVScan2($r->item_num, $r->lot, null, $r->kontrak, true);
                    } else {
                        $cekData = $this->findRCVScan2($r->item_num, $r->lot, null, $r->kontrak)->count();
                        $cekHasilWithLot = $this->findRCVScan2($r->item_num, $r->lot, null, $r->kontrak)->first();

                        if (!empty($cekHasilWithLot['RCV_ITMCD_REFF'])) {
                            $getRCVSCN = $cekData === 0
                                ? clone $this->findRCVScan2($r->item_num, null, null, $r->kontrak)
                                : ($cekHasilWithLot['STOCK_REF'] < $r->qty
                                    ? clone $this->findRCVScan2($r->item_num, null, null, $r->kontrak)->orderByRaw("CASE WHEN RCVSCN_LOTNO = '" . $r->lot . "' THEN 1 ELSE 2 END")
                                    : clone $this->findRCVScan2($r->item_num, $r->lot, null, $r->kontrak));
                        } else {
                            $getRCVSCN = $cekData === 0
                                ? clone $this->findRCVScan2($r->item_num, null, null, $r->kontrak)
                                : ($cekHasilWithLot['RCVSCN_QTY'] < $r->qty
                                    ? clone $this->findRCVScan2($r->item_num, null, null, $r->kontrak)->orderByRaw("CASE WHEN RCVSCN_LOTNO = '" . $r->lot . "' THEN 1 ELSE 2 END")
                                    : clone $this->findRCVScan2($r->item_num, $r->lot, null, $r->kontrak));
                        }
                    }
                }
            } else {
                if (!$r->has('lot') || empty($r->lot)) {
                    $getRCVSCN = $this->findRCVScan2WithoutLot($r->item_num);
                } else {
                    $cekData = $this->findRCVScan2($r->item_num, $r->lot)->count();
                    $cekHasilWithLot = $this->findRCVScan2($r->item_num, $r->lot, null, $r->kontrak)->first();

                    if (!empty($cekHasilWithLot['RCV_ITMCD_REFF'])) {
                        $getRCVSCN = $cekData === 0
                            ? clone $this->findRCVScan2($r->item_num)
                            : ($this->findRCVScan2($r->item_num, $r->lot)->first()['STOCK_REF'] < $r->qty
                                ? clone $this->findRCVScan2($r->item_num)->orderByRaw("CASE WHEN RCVSCN_LOTNO = '" . $r->lot . "' THEN 1 ELSE 2 END")
                                : clone $this->findRCVScan2($r->item_num, $r->lot));
                    } else {
                        $getRCVSCN = $cekData === 0
                            ? clone $this->findRCVScan2($r->item_num)
                            : ($this->findRCVScan2($r->item_num, $r->lot)->first()['RCVSCN_QTY'] < $r->qty
                                ? clone $this->findRCVScan2($r->item_num)->orderByRaw("CASE WHEN RCVSCN_LOTNO = '" . $r->lot . "' THEN 1 ELSE 2 END")
                                : clone $this->findRCVScan2($r->item_num, $r->lot));
                    }
                }
            }

            if (($r->has('tujuan') && !empty($r->tujuan)) && ($r->has('bc') && !empty($r->bc))) {
                if ($r->bc === '27') {
                    if ($r->tujuan == 6) {
                        $getRCVSCN->where('RCV_ZSTSRCV', '2');
                    } else {
                        $getRCVSCN->where(function ($w) {
                            $w->where('RCV_ZSTSRCV', '<>', '2');
                            $w->orWhereNull('RCV_ZSTSRCV');
                        });
                    }
                }
            }

            if (!$r->has('lot') || empty($r->lot)) {
                if ($r->has('do') && !empty($r->do)) {
                    $getRCVSCN->where('RCV_DONO', $r->do);
                }
            } else {
                if ($r->has('do') && !empty($r->do)) {
                    $getRCVSCN->where('RCVSCN_DONO', $r->do);
                }
            }
        } else {
            $getRCVSCN = $this->findRCVScan2WithoutLot($r->item_num, null, null, null, true);

            if ($r->has('do') && !empty($r->do)) {
                $getRCVSCN->where('RCVSCN_DONO', $r->do);
            }

            if ($r->has('less_than') && !empty($r->less_than)) {
                $getRCVSCN->where('RCV_BCDATE', '<', $r->less_than);
            }
        }

        $dataListDO = $getRCVSCN->orderBy('RCV_BCDATE', 'ASC')->get()->toArray();

        // return $dataListDO;

        $cekStockTotal = 0;
        foreach ($dataListDO as $keyTot => $valueTot) {
            $cekStockTotal += $valueTot['RCVSCN_QTY'];
        }

        if ($cekStockTotal < $r->qty) {
            $status = false;
            $data = [
                'data'  => $dataListDO,
                'query' => (clone $getRCVSCN)->orderBy('RCV_BCDATE', 'ASC')->toSql()
            ];
            $message = 'Stock only ' . $cekStockTotal . ', not enough !!';
        } else {
            if (count($dataListDO) > 0) {
                $isSaved = ($r->has('isNotSaved') && $r->isNotSaved == 'true') ? false : true;
                $insertDO = $this->checkStockDO(
                    $dataListDO,
                    $r->qty,
                    $r->doc,
                    $r->date_out,
                    [],
                    $r->adj,
                    false,
                    $isSaved,
                    $r->has('loc') && !empty($r->loc) ? $r->loc : ''
                );

                // return $insertDO;
                $status = true;
                $data = $this->convertReturn($insertDO, $r->date_out);
                $message = 'Ex-BC Found.';
            } else {
                $status = false;
                $data = [];
                $message = 'no item found on RCVSCN!!';
            }
        }

        return [
            'status' => $status,
            'data' => $data,
            'message' => $message
        ];
    }

    public function checkStockDO($data, $qty, $docOut, $dateOut, $currentarr = [], $adj = false, $isExceeded = false, $isSaved = true, $loc = '')
    {
        $cekdatanow = current($data);

        if ($adj) {
            $qtynya = 0;
            foreach ($this->findRCVScan2($cekdatanow['RCVSCN_ITMCD'])->get()->toArray() as $keyQtyTot => $valueQtyTot) {
                $qtynya = $qtynya + (int)$valueQtyTot['RCVSCN_QTY'];
            }

            $qtyInit = $qtynya - $qty;
        } else {
            $qtyInit = $qty;
        }

        if (!empty($cekdatanow['RCV_DONO_REFF'])) {
            $cekParent = $this->findRCVScan2($cekdatanow['RCV_ITMCD_REFF'], null, $cekdatanow['RCV_DONO_REFF'])->first();
            $getDOData = $cekParent ? $cekParent->toArray() : null;
        } else {
            $getDOData = $cekdatanow;
        }

        if ($cekdatanow && !empty($getDOData)) {
            $totalReqQty = $getDOData['RCVSCN_QTY'] < $qtyInit
                ? $qtyInit - $getDOData['RCVSCN_QTY']
                : 0;

            $totalStockQty = $getDOData['RCVSCN_QTY'] < $qtyInit
                ? 0
                : $getDOData['RCVSCN_QTY'] - $qtyInit;

            $totalSavedQty = $getDOData['RCVSCN_QTY'] < $qtyInit
                ? $getDOData['RCVSCN_QTY']
                : $qtyInit;

            if ($totalSavedQty > 0) {
                $currentarr[] = array_merge(
                    $getDOData,
                    [
                        'IS_EXCEEDED' => $isExceeded,
                        'REQ_REMAIN' => $totalReqQty,
                        'STOCK_REMAIN' => $totalStockQty,
                        'SAVED_QTY' => $totalSavedQty
                    ]
                );
            }

            if ($isSaved && $totalSavedQty > 0) {
                $this->insertTransaction(
                    $getDOData,
                    $totalSavedQty * -1,
                    'OUT',
                    $docOut,
                    $loc,
                    $dateOut
                );
            }

            if ($getDOData['RCVSCN_QTY'] < $qtyInit) {
                next($data);
                $hasil = $this->checkStockDO($data, $totalReqQty, $docOut, $dateOut, $currentarr, false, false, $isSaved, $loc);
            } else {
                $hasil = $currentarr;
            }
        } else {
            if ($adj === false) {
                if ($qtyInit > 0) {
                    // logger('masuk sini');
                    end($data);
                    $hasil = $this->checkStockDO($data, $qtyInit, $docOut, $dateOut, $currentarr, false, true, $isSaved, $loc);
                } else {
                    $hasil = $currentarr;
                }
            } else {
                $hasil = $currentarr;
            }
        }

        return $hasil;
    }

    public function insertTransaction($data, $qty, $type, $remark, $loc, $dateOut)
    {
        return DetailStock::create([
            'RPSTOCK_BCTYPE' => $data['RCV_BCTYPE'],
            'RPSTOCK_BCNUM' => $data['RCV_BCNO'],
            'RPSTOCK_BCDATE' => $data['RCV_BCDATE'],
            'RPSTOCK_QTY' => $qty,
            'RPSTOCK_DOC' => $data['RCVSCN_DONO'],
            'RPSTOCK_TYPE' => $type,
            'RPSTOCK_ITMNUM' => $data['RCVSCN_ITMCD'],
            'RPSTOCK_NOAJU' =>  $data['RCV_RPNO'],
            'RPSTOCK_LOT' =>  $data['RCVSCN_LOTNO'],
            'RPSTOCK_DOINC' => $data['RCVSCN_DONO'],
            'RPSTOCK_REMARK' => $remark,
            'RPSTOCK_LOC' => $loc,
            'RPSTOCK_BCDATEOUT' => $dateOut
        ]);
    }

    public function findRCVScan2($item, $lot = null, $do = null, $kontrak = null, $showUsed = false)
    {
        $select = [
            'RCV_BCTYPE',
            'RCV_BCNO',
            'RCV_RPNO',
            'RCV_BCDATE',
            'RCVSCN_DONO',
            'RCVSCN_LOTNO',
            'RCV_ITMCD_REFF',
            'RCV_DONO_REFF',
            'RCVSCN_ITMCD',
            'MITM_ITMD1',
            'MITM_STKUOM',
            'MITM_SPTNO',
            'RCV_PRPRC',
            'RCV_KPPBC',
            'RCV_HSCD',
            'RCV_ZNOURUT',
            'RCV_BM',
            'RCV_ZSTSRCV',
            'RCV_CONA',
            'RCV_PPH',
            'RCV_PPN',
            'RCV_PRPRC'
        ];

        $getRCVSCN = RCVSCN::select(
            array_merge(
                $select,
                [
                    DB::raw("
                    (
                        SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                        FROM [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE rb.RPSTOCK_DOC = RCVSCN_DONO
                        and rb.RPSTOCK_ITMNUM = RCVSCN_ITMCD
                        and rb.RPSTOCK_BCDATE = RCV_BCDATE
                        and rb.deleted_at IS NULL
                    ) as STOCK"),
                    DB::raw("
                    (
                        SELECT
                            COALESCE(SUM(RPSTOCK_QTY),
                            0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCV_DONO_REFF
                            and rb.RPSTOCK_ITMNUM = RCV_ITMCD_REFF
                            and rb.RPSTOCK_BCDATE = RCV_BCDATE
                            and rb.deleted_at IS NULL
                    ) as STOCK_REF"),
                    DB::raw("COALESCE(SUM(CAST(RCVSCN_QTY as INT)), 0)
                    + (
                        SELECT
                            COALESCE(SUM(RPSTOCK_QTY),
                            0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCVSCN_DONO
                            and rb.RPSTOCK_ITMNUM = RCVSCN_ITMCD
                            and rb.RPSTOCK_BCDATE = RCV_BCDATE
                            and ISNULL(rb.RPSTOCK_LOT, 'LOT0') = ISNULL(RCVSCN_LOTNO, 'LOT0')
                            and rb.deleted_at IS NULL
                            and rb.RPSTOCK_TYPE = 'OUT'
                    ) AS RCVSCN_QTY ")
                ]
            )
        )
            ->where('RCVSCN_ITMCD', $item)
            ->join(DB::raw('(
            SELECT
                RCV_ITMCD,
                RCV_BCTYPE,
                RCV_BCNO,
                RCV_DONO,
                RCV_RPNO,
                RCV_BCDATE,
                RCV_ITMCD_REFF,
                RCV_DONO_REFF,
                MAX(RCV_KPPBC) AS RCV_KPPBC,
                MAX(RCV_HSCD) AS RCV_HSCD,
                MAX(RCV_ZNOURUT) AS RCV_ZNOURUT,
                MAX(RCV_PRPRC) AS RCV_PRPRC,
                MAX(RCV_BM) AS RCV_BM,
                MAX(RCV_ZSTSRCV) AS RCV_ZSTSRCV,
                MAX(RCV_CONA) AS RCV_CONA,
                MAX(RCV_PPH) AS RCV_PPH,
                MAX(RCV_PPN) AS RCV_PPN,
                sum(RCV_QTY) AS RCV_QTY
            FROM
                PSI_WMS.dbo.RCV_TBL
            GROUP BY
                RCV_ITMCD,
                RCV_BCTYPE,
                RCV_BCNO,
                RCV_DONO,
                RCV_RPNO,
                RCV_BCDATE,
                RCV_ITMCD_REFF,
                RCV_DONO_REFF
            ) a'), function ($j) {
                $j->on('RCVSCN_DONO', '=', 'RCV_DONO');
                $j->on('RCVSCN_ITMCD', '=', 'RCV_ITMCD');
            })
            ->join('PSI_WMS.dbo.MITM_TBL', 'MITM_ITMCD', '=', 'RCVSCN_ITMCD')
            ->groupBy($select);

        if (!$showUsed) {
            $getRCVSCN->havingRaw("
            (
                CASE
                    WHEN
                        RCV_ITMCD_REFF IS NOT NULL
                    THEN
                    (
                        SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCV_DONO_REFF
                        and rb.RPSTOCK_ITMNUM = RCV_ITMCD_REFF
                        and rb.RPSTOCK_BCDATE = RCV_BCDATE
                        and rb.deleted_at IS NULL
                    )
                    ELSE
                    (
                        SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCVSCN_DONO
                        and rb.RPSTOCK_ITMNUM = RCVSCN_ITMCD
                        and rb.RPSTOCK_BCDATE = RCV_BCDATE
                        and rb.deleted_at IS NULL
                    )
                    END
            ) > 0");
        }

        if (!empty($lot)) {
            $getRCVSCN->where('RCVSCN_LOTNO', $lot);
        }

        if (!empty($do)) {
            $getRCVSCN->where('RCVSCN_DONO', trim($do));
        }

        if (!empty($kontrak)) {
            $getRCVSCN->where('RCV_CONA', trim($kontrak));
        }

        return $getRCVSCN;
    }

    public function findRCVScan2WithoutLot($item, $lot = null, $do = null, $kontrak = null, $showUsed = false)
    {
        $select = [
            'RCV_BCTYPE',
            'RCV_BCNO',
            'RCV_RPNO',
            'RCV_BCDATE',
            'RCV_ITMCD_REFF',
            'RCV_DONO_REFF',
            'MITM_ITMD1',
            'MITM_STKUOM',
            'MITM_SPTNO'
        ];

        $getRCVSCN = MutasiStok::select(
            array_merge(
                $select,
                [
                    DB::raw("NULL AS RCVSCN_LOTNO"),
                    DB::raw("RCV_DONO AS RCVSCN_DONO"),
                    DB::raw("RCV_ITMCD AS RCVSCN_ITMCD"),
                    DB::raw('MAX(RCV_KPPBC) AS RCV_KPPBC'),
                    DB::raw('MAX(RCV_HSCD) AS RCV_HSCD'),
                    DB::raw('MAX(RCV_ZNOURUT) AS RCV_ZNOURUT'),
                    DB::raw('MAX(RCV_PRPRC) AS RCV_PRPRC'),
                    DB::raw('MAX(RCV_BM) AS RCV_BM'),
                    DB::raw('MAX(RCV_ZSTSRCV) AS RCV_ZSTSRCV'),
                    DB::raw('MAX(RCV_CONA) AS RCV_CONA'),
                    DB::raw('MAX(RCV_PPH) AS RCV_PPH'),
                    DB::raw('MAX(RCV_PPN) AS RCV_PPN')
                ],
                [
                    DB::raw("
                    (
                        SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                        FROM [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE rb.RPSTOCK_DOC = RCV_DONO
                        and rb.RPSTOCK_ITMNUM = RCV_ITMCD
                        and rb.RPSTOCK_BCDATE = RCV_BCDATE
                        and rb.RPSTOCK_NOAJU = RCV_RPNO
                        and rb.deleted_at IS NULL
                    ) as STOCK"),
                    DB::raw("
                    (
                        SELECT
                            COALESCE(SUM(RPSTOCK_QTY),
                            0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCV_DONO_REFF
                            and rb.RPSTOCK_ITMNUM = RCV_ITMCD_REFF
                            and rb.RPSTOCK_BCDATE = RCV_BCDATE
                            and rb.RPSTOCK_NOAJU = RCV_RPNO
                            and rb.deleted_at IS NULL
                    ) as STOCK_REF"),
                    DB::raw("COALESCE(SUM(CAST(RCV_QTY as INT)), 0)
                    + (
                        SELECT
                            COALESCE(SUM(RPSTOCK_QTY),
                            0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCV_DONO
                            and rb.RPSTOCK_ITMNUM = RCV_ITMCD
                            and rb.RPSTOCK_BCDATE = RCV_BCDATE
                            and rb.RPSTOCK_NOAJU = RCV_RPNO
                            --and ISNULL(rb.RPSTOCK_LOT, 'LOT0') = ISNULL(RCVSCN_LOTNO, 'LOT0')
                            and rb.deleted_at IS NULL
                            and rb.RPSTOCK_TYPE = 'OUT'
                            and rb.RPSTOCK_LOT is null
                    )AS RCVSCN_QTY "),
                    DB::raw("COALESCE(SUM(CAST(RCV_QTY as INT)), 0) as test_1"),
                    DB::raw("(
                        SELECT
                            COALESCE(SUM(RPSTOCK_QTY),
                            0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCV_DONO
                            and rb.RPSTOCK_ITMNUM = RCV_ITMCD
                            and rb.RPSTOCK_BCDATE = RCV_BCDATE
                            and rb.RPSTOCK_NOAJU = RCV_RPNO
                            --and ISNULL(rb.RPSTOCK_LOT, 'LOT0') = ISNULL(RCVSCN_LOTNO, 'LOT0')
                            and rb.deleted_at IS NULL
                            and rb.RPSTOCK_TYPE = 'OUT'
                    ) AS test_2 "),
                ]
            )
        )->where('RCV_ITMCD', $item)
            ->join('PSI_WMS.dbo.MITM_TBL', 'MITM_ITMCD', '=', 'RCV_ITMCD')
            ->groupBy(array_merge(
                [
                    'RCV_DONO',
                    'RCV_ITMCD'
                ],
                $select
            ));

        if (!$showUsed) {
            $getRCVSCN->havingRaw("
            (
                CASE
                    WHEN
                        RCV_ITMCD_REFF IS NOT NULL
                    THEN
                    (
                        SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCV_DONO_REFF
                        and rb.RPSTOCK_ITMNUM = RCV_ITMCD_REFF
                        and rb.RPSTOCK_BCDATE = RCV_BCDATE
                        and rb.RPSTOCK_NOAJU = RCV_RPNO
                        and rb.deleted_at IS NULL
                    )
                    ELSE
                    (
                        SELECT COALESCE(SUM(RPSTOCK_QTY), 0)
                        FROM
                            [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
                        WHERE
                            rb.RPSTOCK_DOC = RCV_DONO
                        and rb.RPSTOCK_ITMNUM = RCV_ITMCD
                        and rb.RPSTOCK_BCDATE = RCV_BCDATE
                        and rb.RPSTOCK_NOAJU = RCV_RPNO
                        and rb.deleted_at IS NULL
                    )
                    END
            ) > 0");
            // ->havingRaw("(
            //     COALESCE(SUM(CAST(RCV_QTY as INT)), 0) + (
            //         SELECT
            //             COALESCE(SUM(RPSTOCK_QTY),
            //             0)
            //         FROM
            //             [PSI_RPCUST].[dbo].[RPSAL_BCSTOCK] rb
            //         WHERE
            //             rb.RPSTOCK_DOC = RCV_DONO
            //             and rb.RPSTOCK_ITMNUM = RCV_ITMCD
            //             and rb.RPSTOCK_BCDATE = RCV_BCDATE
            //             and rb.RPSTOCK_NOAJU = RCV_RPNO
            //             and rb.deleted_at IS NULL
            //             and rb.RPSTOCK_TYPE = 'OUT'
            //     )
            // ) > 0");
        }

        if (!empty($do)) {
            $getRCVSCN->where('RCV_DONO', trim($do));
        }

        if (!empty($kontrak)) {
            $getRCVSCN->where('RCV_CONA', trim($kontrak));
        }

        return $getRCVSCN;
    }

    public function convertReturn($dataInc, $dateOut)
    {
        // return count($dataInc);
        if (count($dataInc) > 0) {
            $data = [];
            foreach ($dataInc as $key => $value) {
                if (isset($value['RCV_BCTYPE'])) {
                    $data[] = [
                        "BC_TYPE" => $value['RCV_BCTYPE'],
                        "BC_NUM" => $value['RCV_BCNO'],
                        "BC_AJU" => $value['RCV_RPNO'],
                        "BC_DATE" => $value['RCV_BCDATE'],
                        "BC_QTY" => $value['SAVED_QTY'],
                        "BC_DO" => trim($value['RCVSCN_DONO']),
                        "BC_ITEM" => strtoupper(trim($value['RCVSCN_ITMCD'])),
                        "QTY_SISA" => $value['STOCK_REMAIN'],
                        "RCV_KPPBC" => $value['RCV_KPPBC'],
                        "RCV_HSCD" => $value['RCV_HSCD'],
                        "RCV_ZNOURUT" => $value['RCV_ZNOURUT'],
                        "RCV_ZSTSRCV" => $value['RCV_ZSTSRCV'],
                        "RCV_PRPRC" => $value['RCV_PRPRC'],
                        "RCV_BM" => $value['RCV_BM'],
                        "RCV_PPH" => $value['RCV_PPH'],
                        "RCV_PPN" => $value['RCV_PPN'],
                        "MITM_ITMD1" => trim($value['MITM_ITMD1']),
                        "MITM_STKUOM" => trim($value['MITM_STKUOM']),
                        "MITM_SPTNO" => trim($value['MITM_SPTNO']),
                        "OUT_BCDATE" => $dateOut,
                        "LOT" => $value['RCVSCN_LOTNO'],
                        "STOCK" => $value['STOCK'],
                        "REQ_REMAIN" => $value['REQ_REMAIN'],
                        "STOCK_REMAIN" => $value['STOCK_REMAIN'],
                        "SAVED_QTY" => $value['SAVED_QTY'],
                        "IS_EXCEEDED" => $value['IS_EXCEEDED']
                    ];
                }
            }

            return $data;
        } else {
            return [];
        }
    }


    public function spCalculationEXBC(Request $r)
    {
        $query = "DECLARE @tempTableEXBC table (
            BC_TYPE varchar(10),
            BC_NUM varchar(20),
            BC_AJU varchar(50),
            BC_DATE DATE,
            BC_QTY INT,
            BC_DO varchar(100),
            BC_ITEM varchar(100),
            RCV_KPPBC varchar(50),
            RCV_HSCD varchar(50),
            RCV_ZNOURUT varchar(50),
            RCV_PRPRC varchar(50),
            RCV_BM varchar(50),
            OUT_BCDATE DATE,
            LOT varchar(50)
        );";

        // if (is_array($r->item_num)) {
        //     foreach ($r->item_num as $key => $value) {
        //         $query .= $this->setupQuery($value);
        //     }
        // }
    }

    public function setupQuery($item, $doc, $dateOut, $qty)
    {
        return "
            insert into @tempTableEXBC
            EXEC PSI_RPCUST.dbo.sp_calculation_exbc
                @item_num = '" . $item . "',
                @doc_out = '" . $doc . "',
                @date_out = '" . $dateOut . "',
                @qty = " . $qty . ";
        ";
    }
}
