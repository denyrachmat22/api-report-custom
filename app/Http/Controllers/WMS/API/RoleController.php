<?php

namespace App\Http\Controllers\WMS\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\WMS\API\RoleMaster;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\WMS\REPORT\RoleRequest;
use App\Model\WMS\API\GroupMaster;

class RoleController extends Controller
{
    public function index()
    {
        $cek = RoleMaster::select([
            'EMPACCESS_GRPID',
            'EMPACCESS_MENUID'
        ])
        ->where('EMPACCESS_GRPID','LIKE', 'RPCUST%')
        ->ORwhere('EMPACCESS_GRPID','LIKE', 'MBL%')
            ->with('menu')
            ->with('group')
            ->get();

        $hasil = [];
        foreach ($cek as $key => $value) {
            $hasil[$value['EMPACCESS_GRPID']]['DESC'] = $value->group['MSTGRP_NM'];
            $hasil[$value['EMPACCESS_GRPID']]['DATA'][] = $value;
        }

        return $hasil;
    }

    public function cekroleid($id)
    {
        return RoleMaster::select([
            'EMPACCESS_GRPID',
            'EMPACCESS_MENUID'
        ])
        ->where('EMPACCESS_GRPID','LIKE', 'RPCUST%')
        ->ORwhere('EMPACCESS_GRPID','LIKE', 'MBL%')
            ->where('EMPACCESS_GRPID', $id)
            ->with('menu')
            ->with('group')
            ->get();
    }

    public function store(RoleRequest $req, $met)
    {
        if ($met == 'new') {
            $id = substr($req->role_id,0,7) !== 'RPCUST_'
                ? (substr($req->role_id,0,4) === 'MBL_' 
                   ? $req->role_id 
                   : 'RPCUST_'.$req->role_id )
                : $req->role_id;
            foreach ($req->selected as $key => $value) {
                RoleMaster::insert([
                    'EMPACCESS_GRPID' => $id,
                    'EMPACCESS_MENUID' => $value
                ]);
            }

            GroupMaster::insert([
                'MSTGRP_ID' => $id,
                'MSTGRP_NM' => $req->role_name
            ]);

            return 'success';
        } elseif ($met == 'update') {
            $id = $req->role_id;
            foreach ($req->selected as $key => $value) {
                $cekid = RoleMaster::where('EMPACCESS_GRPID', $id)->where('EMPACCESS_MENUID', $value)->first();

                if (!empty($cekid)) {
                    RoleMaster::where('EMPACCESS_GRPID', $id)->where('EMPACCESS_MENUID', $value)->delete();
                    RoleMaster::insert([
                        'EMPACCESS_GRPID' => $id,
                        'EMPACCESS_MENUID' => $value
                    ]);
                    // RoleMaster::where('EMPACCESS_GRPID', $id)->where('EMPACCESS_MENUID', $value)
                    //     ->update([
                    //         'EMPACCESS_GRPID' => $id,
                    //         'EMPACCESS_MENUID' => $value
                    //     ]);
                } else {
                    RoleMaster::insert([
                        'EMPACCESS_GRPID' => $id,
                        'EMPACCESS_MENUID' => $value
                    ]);
                }
            }


            $cekdeleted = RoleMaster::where('EMPACCESS_GRPID', $id)->whereNotIn('EMPACCESS_MENUID', $req->selected)->delete();

            return $cekdeleted;
        } else {
            $cek = RoleMaster::where('EMPACCESS_GRPID', $req->role_id)
                ->doesnthave('user');

            if (!empty($cek->first())) {
                RoleMaster::where('EMPACCESS_GRPID', $req->role_id)->delete();
                GroupMaster::where('MSTGRP_ID', $req->role_id)->delete();
                return 'success';
            } else {
                return response([
                    'message' => "The given data was invalid.",
                    'errors' => [
                        'menu_id' => ['Data has been used somewhere!!']
                    ]
                ], 422);
            }
        }
    }
}
