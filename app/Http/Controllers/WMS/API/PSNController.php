<?php

namespace App\Http\Controllers\WMS\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\WMS\API\SPLMaster;
use Illuminate\Support\Facades\DB;
use App\Model\MASTER\ITHMaster;
use App\Model\WMS\API\SPLSCN;

class PSNController extends Controller
{
    public function getPSNData(Request $req)
    {
        $select = [
            'SPL_DOC',
            'SPL_CAT',
            'SPL_LINE',
            'SPL_FEDR',
            'SPL_ORDERNO',
            'SPL_ITMCD',
            'MITM_ITMCD', 
            'MITM_ITMD1', 
            'MITM_ITMD2',
            'MITM_STKUOM', 
            'MITM_SPTNO',
            'ITMLOC_LOC'
        ];

        $getspl = SPLMaster::select(array_merge($select, [DB::raw('SUM(SPL_QTYREQ) as SPL_QTYREQ')]))
            ->leftjoin('ITMLOC_TBL', 'SPL_ITMCD', 'ITMLOC_ITM')
            ->leftjoin('MITM_TBL', 'SPL_ITMCD', 'MITM_ITMCD')
            ->where('SPL_DOC', $req->Spno)
            ->where('SPL_CAT', $req->Cat)
            ->where('SPL_LINE', $req->Line)
            ->where('SPL_FEDR', $req->Feeder)
            ->groupBy($select)
            ->orderBy('ITMLOC_LOC','asc')
            ->get()->toArray();
        
        $hasil = [];

        foreach ($getspl as $key => $value) {
            $newdoc = $req->Spno.'|'.$req->Line.'|'.$req->Cat.'|'.$req->Feeder;
            $cekith = ITHMaster::where('ITH_DOC',$newdoc)
                ->where('ITH_ITMCD',$value['SPL_ITMCD'])
                ->where('ITH_WH','ARPRD1')
                ->where('ITH_FORM','INC-PRD-RM')
                ->get()
                ->toArray();
            $ceksplscan = SPLSCN::where('SPLSCN_DOC',$req->Spno)
                ->where('SPLSCN_CAT',$req->Cat)
                ->where('SPLSCN_LINE',$req->Line)
                ->where('SPLSCN_FEDR',$req->Feeder)
                ->where('SPLSCN_ITMCD',$value['SPL_ITMCD'])
                ->where('SPLSCN_SAVED','<>', 1)
                ->get()
                ->toArray();

            $joinarray = array_merge($value,['ith' => $cekith, 'splscn' => $ceksplscan]);

            $hasil[] = $joinarray;
        }

        return $hasil;
    }
}
