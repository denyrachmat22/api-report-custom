<?php

namespace App\Http\Controllers\WMS\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\WMS\API\MenuMaster;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\WMS\REPORT\MenuRequest;
use Facade\FlareClient\Http\Response;

class MenuController extends Controller
{
    public function index($cek)
    {
        if ($cek == 'parent') {
            return MenuMaster::where('MENU_ID','LIKE','RPCUST%')->orwhere('MENU_ID','LIKE','MBL%')->with('child.parent')->where('MENU_PRNT','0')->get();
        } else {
            return MenuMaster::where('MENU_ID','LIKE','RPCUST%')->orwhere('MENU_ID','LIKE','MBL%')->with('child.parent')->get();
        }
        
    }

    public function show($cat)
    {
        $getMenu = new MenuMaster;
        $catHasil = $cat == 'custom' ? $getMenu::where(DB::raw('LEFT(MENU_ID,6)'),'RPCUST') : $getMenu::where(DB::raw('LEFT(MENU_ID,3)'),'MBL');

        return $catHasil->get();
    }

    public function cekmenuid($id)
    {
        return MenuMaster::where('MENU_ID',$id)->first();
    }

    public function store(MenuRequest $req, $met)
    {
        if ($met == 'new') {            
            $id = substr($req->menu_id,0,7) !== 'RPCUST_'
                ? (substr($req->menu_id,0,4) === 'MBL_' 
                   ? $req->menu_id 
                   : 'RPCUST_'.$req->menu_id )
                : $req->menu_id;
            MenuMaster::insert([
                'MENU_ID' => $id,
                'MENU_NAME' => $req->menu_nm,
                'MENU_DSCRPTN' => $req->menu_desc,
                'MENU_PRNT' => $req->menu_parent,
                'MENU_URL' => $req->menu_url,
                'MENU_ICON' => $req->menu_icon
            ]);

            return 'success';
        } elseif ($met == 'update') {
            $id = substr($req->menu_id,0,7) !== 'RPCUST_'
                ? (substr($req->menu_id,0,4) === 'MBL_' 
                   ? $req->menu_id 
                   : 'RPCUST_'.$req->menu_id )
                : $req->menu_id;
                
            MenuMaster::where('MENU_ID', $req->menu_id)->update([
                'MENU_ID' => $id,
                'MENU_NAME' => $req->menu_nm,
                'MENU_DSCRPTN' => $req->menu_desc,
                'MENU_PRNT' => $req->menu_parent,
                'MENU_URL' => $req->menu_url,
                'MENU_ICON' => $req->menu_icon
            ]);

            return 'success';
        } else {
            $cek = MenuMaster::where('MENU_ID', $req->menu_id)->doesnthave('child')->doesnthave('role');
            if (!empty($cek->first())) {
                MenuMaster::where('MENU_ID', $req->menu_id)->delete();
                return 'success';
            } else {
                return response([
                    'message' => "The given data was invalid.",
                    'errors' => [
                        'menu_id' => ['Data has been used somewhere!!']
                    ]
                    ],422);
            }
        }
    }
}
