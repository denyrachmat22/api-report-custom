<?php

namespace App\Http\Controllers\WMS\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MASTER\LocationMaster;
class HomeController extends Controller
{
    public function index()
    {
        return LocationMaster::get();
    }
}
