<?php

namespace App\Http\Controllers\WMS\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\WMS\REPORT\MutasiStok;
use App\Model\WMS\API\ITMLOCTblo;
use App\Model\WMS\API\RCVSCN;
use Illuminate\Support\Facades\DB;
use App\Model\MASTER\ITHMaster;
use App\Helpers\RouteDefine;

class ReceivingController extends Controller
{
    public function getdata($do)
    {
        $cekdo = MutasiStok::where('RCV_DONO', $do)->get();

        $hasil = [];
        foreach ($cekdo as $key => $value) {
            $select = [
                'RCV_DONO',
                'RCV_ITMCD',
                'RCV_WH',
                'ITMLOC_LOC',
                'ITMLOC_ITM'
            ];

            $hasil[] = MutasiStok::select(array_merge($select, [DB::raw('SUM(RCV_QTY) AS RCV_QTY')]))->where('RCV_DONO', $do)
                ->where('RCV_ITMCD', $value['RCV_ITMCD'])
                ->with(['ItemMaster'])
                ->with(['ith' => function ($qith) use ($value){
                    $qith->where('ITH_ITMCD', '=', $value['RCV_ITMCD']);
                }])
                ->with(['rcvscn' => function ($q) use ($value) {
                    $q->where('RCVSCN_ITMCD', '=', $value['RCV_ITMCD']);
                    // $q->where('RCVSCN_STATUS', '<>', 1);
                }])
                ->leftjoin('ITMLOC_TBL', 'RCV_ITMCD', 'ITMLOC_ITM')
                ->groupBy($select)
                ->first();
        }

        return $hasil;
    }

    public function getitembyloc($loc, $item)
    {
        $cekloc = ITMLOCTblo::where('ITMLOC_LOC', $loc);

        if (!empty($cekloc->first())) {
            $hasil = $cekloc->where('ITMLOC_ITM', $item)->first();
        } else {
            return [];
        }

        return $hasil;
    }

    public function store(Request $req)
    {
        $get = [];
        foreach ($req->scanned_data as $key => $value) {
            $tot = 0;
            if (isset($value['rcvscn'])) {
                foreach ($value['rcvscn'] as $key_det => $value_det) {
                    $tot += $value_det['RCVSCN_QTY'];

                    $ceklastidscan = RCVSCN::where(DB::raw('LEFT(RCVSCN_ID, 8)'), date('Ymd'))
                        ->orderBy('RCVSCN_ID','DESC')
                        ->first();

                    $id = empty($ceklastidscan) ? date('Ymd'). 1 : date('Ymd').(intval(substr($ceklastidscan['RCVSCN_ID'],8)) + 1);

                    $cekscan = RCVSCN::where('RCVSCN_ITMCD', trim($value['RCV_ITMCD']))
                        ->where('RCVSCN_DONO', $req->do);
                    
                    if (empty($cekscan->first())) {
                        RCVSCN::insert([
                            'RCVSCN_ID' => $id,
                            'RCVSCN_DONO' => $req->do,
                            'RCVSCN_LOCID' => $value['ITMLOC_LOC'],
                            'RCVSCN_ITMCD' => trim($value['RCV_ITMCD']),
                            'RCVSCN_LOTNO' => $value_det['RCVSCN_LOTNO'],
                            'RCVSCN_QTY' => $value_det['RCVSCN_QTY'],
                            'RCVSCN_SAVED' => 1,
                            'RCVSCN_LUPDT' => date('Y-m-d H:i:s'),
                            'RCVSCN_USRID' => $req->user,
                            'RCVSCN_STATUS' => 1,
                        ]);
                    } else {
                        $cekscan->update([
                            'RCVSCN_DONO' => $req->do,
                            'RCVSCN_LOCID' => $value['ITMLOC_LOC'],
                            'RCVSCN_ITMCD' => trim($value['RCV_ITMCD']),
                            'RCVSCN_LOTNO' => $value_det['RCVSCN_LOTNO'],
                            'RCVSCN_QTY' => $value_det['RCVSCN_QTY'],
                            'RCVSCN_SAVED' => 1,
                            'RCVSCN_LUPDT' => date('Y-m-d H:i:s'),
                            'RCVSCN_USRID' => $req->user,
                            'RCVSCN_STATUS' => 1,
                        ]);
                    }
                }
            }

            $cekitemdo = ITHMaster::where('ITH_ITMCD', trim($value['RCV_ITMCD']))
                ->where('ITH_DOC', $req->do)
                ->where('ITH_DATE', date('Y-m-d'));
            
            $cekroute = RouteDefine::index('RECEIVING-RM-WH',$value['RCV_WH']);

            if ($tot !== 0) {
                if (!empty($cekitemdo->first())) {
                    $cekitemdo->update([
                        'ITH_QTY' => trim($tot)
                    ]);
                } else {
                    ITHMaster::insert([
                        'ITH_ITMCD' => trim($value['RCV_ITMCD']),
                        'ITH_DATE' => date('Y-m-d'),
                        'ITH_FORM' => $cekroute->TXROUTE_WHINC,
                        'ITH_DOC' => $req->do,
                        'ITH_QTY' => $tot,
                        'ITH_WH' => $value['RCV_WH'],
                        'ITH_LOC' => $value['ITMLOC_LOC'],
                        'ITH_LUPDT' => date('Y-m-d H:i:s'),
                        'ITH_USRID' => $req->user,
                    ]);
                }
            }

            $get[$value['ITMLOC_ITM']]['TOTAL'] = $tot;
        }

        return 'success';
    }
}
