<?php

namespace App\Http\Controllers\DesignProcess;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\WMS\DESIGNPROC\masterProcess;
use App\Http\Requests\DesignProcess\DesignProcessRequest;
use App\Model\WMS\DESIGNPROC\detailProcess;
use App\Imports\uploadDesignProcessMaster;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\designProcessExport;

class DesignProcessController extends Controller
{
    public function getMasterProcess($mfg)
    {
        $data = DB::table('PROCMSTR_TBL')
            ->select(
                'id',
                DB::raw('ROW_NUMBER() OVER(ORDER BY id) AS no'),
                'PROCMSTR_PROCD',
                'PROCMSTR_REMARK',
                'PROCMSTR_ISLINE',
                'PROCMSTR_ISCT'
            )
            ->where('PROCMSTR_MFGCD', $mfg)
            ->orderBy('PROCMSTR_HEADSEQ')
            ->get();

        return $data;
    }

    public function masterStore(DesignProcessRequest $req)
    {
        return masterProcess::create($req->all());
    }

    public function masterUpdate(DesignProcessRequest $req, $id)
    {
        return masterProcess::where('id', $id)->update($req->all());
    }

    public function masterDelete($id)
    {
        return masterProcess::where('id', $id)->delete();
    }

    public function storeProcessItem(Request $request)
    {
        $hasil = [];
        foreach ($request->data as $key => $value) {

            $getLatestData = detailProcess::select('PROCDET_ITER_ID')->whereBetween('created_at', [date('Y-m-01'), date('Y-m-t')])->orderBy('id', 'desc')->first();

            if (empty($getLatestData)) {
                $iter = 'PROC-DS-' . date('Y') . '-' . date('m') . '-0001';
            } else {
                $iter = 'PROC-DS-' . date('Y') . '-' . date('m') . '-' . sprintf('%04d', (int)substr($getLatestData->PROCDET_ITER_ID, -4) + 1);
            }

            if (isset($value['PROCDET_ITER_ID'])) {
                $iter = $value['PROCDET_ITER_ID'];

                detailProcess::where('PROCDET_ITER_ID', $iter)->delete();
            }

            foreach ($value['processUpdate'] as $keyDet => $valueDet) {
                $hasil[] = detailProcess::create([
                    'PROCMSTR_ID' => $valueDet['id'],
                    'PROCDET_SEQ' => $keyDet + 1,
                    'PROCDET_MDLCD' => $value['PROCDET_MDLCD'],
                    'PROCDET_CD' => $value['PROCDET_CD'],
                    'PROCDET_LINE' => isset($valueDet['PROCDET_LINE']) ? $valueDet['PROCDET_LINE'] : null,
                    'PROCDET_CT' => isset($valueDet['PROCDET_CT']) ? $valueDet['PROCDET_CT'] : null,
                    'PROCDET_AKM' => $valueDet['PROCMSTR_PROCD'] === 'AKM' ? 1 : NULL,
                    'PROCDET_PSSS' => $valueDet['PROCMSTR_PROCD'] === 'PSSS' ? 1 : NULL,
                    'PROCDET_ITER_ID' => $iter,
                ]);
            }
        }

        return $hasil;
    }

    public function uploadProcess(Request $req)
    {
        $nama_file = uniqid('upprocess_') . rand() . '.' . $req->file->extension();

        // return $nama_file;
        $req->file->storeAs('/public/upload_process', $nama_file);

        try {
            $importer = new uploadDesignProcessMaster(['mfg' => $req->mfg]);
            Excel::import($importer, '/public/upload_process/' . $nama_file);
            // if ($importer->data == 'item_not_found') {
            //     return 'Item tidak ditemukan';
            // }

            return 'Upload Sukses ' . $nama_file;
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();

            foreach ($failures as $failure) {
                $failure->row(); // row that went wrong
                $failure->attribute(); // either heading key (if using heading row concern) or column index
                $failure->errors(); // Actual error messages from Laravel validator
                $failure->values(); // The values of the row that has failed.
            }

            return response()->json($failure, 422);
        }
    }

    public function data($mfg, $filter)
    {
        $data = detailProcess::join('PROCMSTR_TBL', 'PROCMSTR_TBL.id', 'PROCMSTR_ID')
            ->join('MITM_TBL', 'PROCDET_MDLCD', 'MITM_ITMCD')
            ->where('PROCMSTR_MFGCD', $mfg);

        if (!empty($filter)) {
            $data->where('PROCDET_MDLCD', 'like', $filter . '%');
        }

        $hasil = [];

        $keys = $keysDet = 0;
        $datanya = $data->get();
        foreach ($datanya as $key => $value) {
            if ($key === 0) {
                $hasil[$keys] = [
                    'no' => $keys + 1,
                    'PROCDET_CD' => $value['PROCDET_CD'],
                    'PROCDET_MDLCD' => $value['PROCDET_MDLCD'],
                    'MITM_ITMD1' => $value['MITM_ITMD1'],
                    'MITM_SPTNO' => $value['MITM_SPTNO'],
                    'PROCDET_ITER_ID' => $value['PROCDET_ITER_ID'],
                    'processUpdate' => []
                ];
            }

            if ($key > 0 && $value['PROCDET_ITER_ID'] !== $datanya[$key - 1]['PROCDET_ITER_ID']) {
                $keysDet = 0;
                $keys++;

                $hasil[$keys] = [
                    'no' => $keys + 1,
                    'PROCDET_CD' => $value['PROCDET_CD'],
                    'PROCDET_MDLCD' => $value['PROCDET_MDLCD'],
                    'MITM_ITMD1' => $value['MITM_ITMD1'],
                    'MITM_SPTNO' => $value['MITM_SPTNO'],
                    'PROCDET_ITER_ID' => $value['PROCDET_ITER_ID'],
                    'processUpdate' => []
                ];
            }

            $hasil[$keys]['processUpdate'][$keysDet] = [
                'PROCDET_CT' => $value['PROCDET_CT'],
                'PROCDET_LINE' => $value['PROCDET_LINE'],
                'PROCMSTR_ISCT' => $value['PROCMSTR_ISCT'],
                'PROCMSTR_ISLINE' => $value['PROCMSTR_ISLINE'],
                'PROCMSTR_PROCD' => $value['PROCMSTR_PROCD'],
                'PROCMSTR_REMARK' => $value['PROCMSTR_REMARK'],
                'PROCMSTR_ISPROCESS' => $value['PROCMSTR_ISPROCESS'],
                'id' => (string)$value['id']
            ];

            $keysDet++;
        }

        return $hasil;
    }

    public function getDataContentAll(Request $request)
    {
        $rows = $request->has('pagination') ? $request['pagination']['rowsPerPage'] : 20;
        $page = $request->has('pagination') ? $request['pagination']['page'] : 1;

        $hasil = $this->data($request->mfg, $request->filter);
        // return $hasil;
        $totalCount = count($hasil);
        $hasil = collect(array_values($hasil));

        return clone $hasil->paginate($rows, $totalCount, $page);
    }

    public function deleteContent($iter_id)
    {
        return detailProcess::where('PROCDET_ITER_ID', $iter_id)->delete();
    }

    public function ExportToExcell(Request $req, $withData = false)
    {
        // $data = $this->data($req->param);
        $data = $withData ? $this->data($req->mfg, $req->filter) : [];

        Excel::store(new designProcessExport($data, $req->mfg), 'DesignProcessSummaryExport.xls', 'public');

        return 'storage/DesignProcessSummaryExport.xls';
    }
}
