<?php

namespace App\Http\Controllers\Scrap;

use App\Http\Controllers\Controller;
use App\Jobs\scrapLossQueue;
use App\Model\RPCUST\DetailStock;
use App\Model\RPCUST\scrapDNList;
use App\Model\RPCUST\scrapDNScan;
use App\Model\RPCUST\scrapTransDet;
use App\Model\WMS\API\RETSCN;
use App\Model\WMS\API\UserMaster;
use App\Model\WMS\REPORT\MutasiStok;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\File;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

// Models
use App\Model\MASTER\ITHMaster;
use App\Model\MASTER\ItemMaster;
use App\Model\WMS\API\RCVSCN;
use App\Model\RPCUST\processMstr;
use App\Model\MASTER\MEGAEMS\pis2Table;
use App\Model\RPCUST\processMapDet;
use App\Model\RPCUST\scrapHist;
use App\Model\RPCUST\processMapMstr;
use App\Model\RPCUST\scrapHistDet;
use App\Model\WMS\API\RETFG;
use App\Model\MASTER\MITMGRP;
use App\Model\RPCUST\dispHist;
use App\Model\WMS\API\C3LCTBL;

// Traits
use App\Traits\traitNewInsertScrap;

use App\Jobs\stockExBCInsert;
use App\Jobs\scrapFixForDisposeQueue;

use App\Exports\scrapDisposeExport;
use App\Exports\scrapLossBefSubmitExport;

use App\Imports\UploadDraftDispose;

class ScrapController extends Controller
{
    use traitNewInsertScrap;

    public function getListProcess()
    {
        return processMstr::get();
    }

    public function findRef(Request $req)
    {
        if (isset($req->form) && !empty($req->form)) {
            if (is_array($req->from)) {
                $whereWH = "AND ITH_WH IN ('" . implode("','", $req->from) . "')";
            } else {
                $whereWH = "AND ITH_WH = '$req->from'";
            }
        } else {
            $whereWH = NULL;
        }

        $hasil = DB::connection('PSI_RPCUST')->select(DB::raw("SELECT * FROM v_scrap_check_ref_stock WHERE ITH_SER = '$req->ref_no' $whereWH"));

        return $hasil;
    }

    public function checkViewRef($ref)
    {
        // $data = DB::connection('PSI_RPCUST')->table('v_scrap_check_ref_stock')->where('ITH_SER',  $ref)->get();

        $data = DB::connection('PSI_RPCUST')->select(DB::raw("SELECT * FROM v_scrap_check_ref_stock WHERE ITH_SER = '$ref'"));
        return $data;
    }

    public function findItem(Request $req)
    {
        $itemInit = ItemMaster::select(
            'MITM_ITMCD',
            'MITM_ITMD1',
            'MITM_MODEL',
            DB::raw('coalesce(CAST(SUM(ITH_QTY) AS INT), 0) AS ITH_QTY'),
            DB::raw("CONCAT(MITM_ITMD1, '(', MITM_ITMCD ,')') AS MODEL2")
        )->where('MITM_ITMCD', 'LIKE', $req->filter . '%')
            ->orWhere('MITM_ITMD1', 'LIKE', $req->filter . '%')
            ->join('ITH_TBL', function ($j) {
                $j->on('MITM_ITMCD', '=', 'ITH_ITMCD')
                    ->whereRaw("LEFT([ITH_WH], 4) IN ('ARWH', 'AFWH')");
            })
            ->groupBy(
                'MITM_ITMCD',
                'MITM_ITMD1',
                'MITM_MODEL'
            );

        if ($req->has('model') && !empty($req->model)) {
            return $itemInit->where('MITM_MODEL', (string) $req->model)->get();
        }

        return $itemInit->get();
    }

    public function findItemItemOnly($query, $model, $useValue = false, $top = 'all')
    {
        $itemInit = ItemMaster::select(
            DB::raw("CONCAT(RTRIM(MITM_ITMCD), '|', RTRIM(MITM_ITMD1)) AS MITM_ITMCD"),
            DB::raw("RTRIM(MITM_ITMCD) AS MITM_ITMCD_REAL"),
            'MITM_ITMD1',
            'MITM_MODEL',
            DB::raw('coalesce(CAST(SUM(ITH_QTY) AS INT), 0) AS ITH_QTY'),
            DB::raw("CONCAT(MITM_ITMD1, '(', MITM_ITMCD ,')') AS MODEL2")
        )->where(function ($w) use ($query) {
            $w->where('MITM_ITMCD', 'LIKE', base64_decode($query) . '%')
                ->orWhere('MITM_ITMD1', 'LIKE', '%' . base64_decode($query) . '%');
        })
            ->whereIn('MITM_MODEL', ['0', '1', '6'])
            ->join('ITH_TBL', function ($j) {
                $j->on('MITM_ITMCD', '=', 'ITH_ITMCD');
            })
            ->groupBy(
                'MITM_ITMCD',
                'MITM_ITMD1',
                'MITM_MODEL'
            )
            ->havingRaw('SUM(ITH_QTY) > 0');

        if ($useValue) {
            if ($top == 'all') {
                $data = (clone $itemInit)->get();
            } else {
                $data = (clone $itemInit)->paginate(20, ['*'], 'page', $top)->items();
            }

            $hasil = [];
            foreach ($data as $key => $value) {
                $hasil[] = [
                    'label' => $value->MITM_ITMCD,
                    'value' => $value->MITM_ITMCD_REAL,
                ];
            }

            return $hasil;
        }
        return (clone $itemInit)->get()->pluck('MITM_ITMCD');
    }

    public function findLot(Request $req)
    {
        $selHeader = [
            'RCVSCN_DONO',
            'RCVSCN_ITMCD',
            'RCVSCN_LOTNO'
            // 'SERD2_ITMCD',
            // 'SERD2_LOTNO'
        ];

        $DOSel = RCVSCN::select(
            array_merge(
                $selHeader,
                [DB::raw("CONCAT(RCVSCN_DONO, '-', RCVSCN_LOTNO) as KEY_LOT")],
                [DB::raw('CAST(SUM(RCVSCN_QTY) AS INT) AS QTY_TOT')],
                [
                    DB::raw("(
                    SELECT SUM(ITH_QTY) FROM PSI_WMS.dbo.ITH_TBL IT
                    WHERE IT.ITH_ITMCD = RCVSCN_TBL.RCVSCN_ITMCD
                    AND IT.ITH_WH = 'ARWH9SC'
                ) AS QTY")
                ]
                // [DB::raw('CAST(SUM(SERD2_QTY) AS INT) AS QTY_FG')]
            )
        )
            ->join('ITH_TBL', function ($j) {
                $j->on('RCVSCN_ITMCD', '=', 'ITH_ITMCD')
                    ->where('ITH_WH', 'LIKE', 'ARWH%');
            })
            ->where('RCVSCN_DONO', '<>', '')
            ->where('RCVSCN_LOTNO', '<>', '')
            ->where('RCVSCN_ITMCD', $req->item)
            ->groupBy(
                $selHeader
            );

        if ($req->has('filter')) {
            $DOSel->where(function ($q) use ($req) {
                $q->where('RCVSCN_DONO', 'LIKE', '%' . $req->filter . '%')
                    ->orWhere('RCVSCN_LOTNO', 'LIKE', '%' . $req->filter . '%');
            });
        }

        return $DOSel->get();
    }

    public function findJobFun($item)
    {
        $cekRCVSCN = DB::connection('PSI_RPCUST')->table('v_list_rcvscn')->select(
            'RCVSCN_ITMCD as PIS2_MDLCD',
            DB::raw('MAX(RCVSCN_DONO) as PIS2_WONO'),
            'RCVSCN_LOTNO',
            DB::raw("'0' AS IS_RETURN"),
            DB::raw("'1' AS IS_MATERIAL"),
            // DB::raw('(
            //     SUM(RCVSCN_QTY) - (
            //         SELECT
            //             COALESCE(SUM(QTY),
            //             0)
            //         FROM
            //             PSI_RPCUST.dbo.RPSCRAP_HIST
            //         WHERE
            //             DOC_NO = RCVSCN_DONO
            //             AND deleted_at IS NULL
            //             AND ITEMNUM = RCVSCN_ITMCD
            //     )
            // ) AS TOTAL_QTY'),
            DB::raw('SUM(RCVSCN_QTY) AS TOTAL_QTY'),
            DB::raw('SUM(RCVSCN_QTY) AS RCVSCN_QTY')
        )
            ->where('RCVSCN_ITMCD', $item)
            ->groupBy(
                'RCVSCN_ITMCD',
                'RCVSCN_LOTNO'
            )
            ->orderBy(DB::raw('SUM(RCVSCN_QTY)'), 'desc');

        return $cekRCVSCN;
    }
    public function findJob(Request $req)
    {
        $cekItemModel = itemMaster::where('MITM_ITMCD', $req->data['item'])->first();

        if ($cekItemModel->MITM_MODEL == 0) {
            $cekRCVSCNFIl = $this->findJobFun($req->data['item']);
            if (!empty($req->data['filter'])) {
                $cekRCVSCNFIl->where('RCVSCN_LOTNO', $req->data['filter']);
            }

            $hasil = $cekRCVSCNFIl->first();

            // return $hasil;

            if (!empty($hasil)) {
                return [$hasil];
            } else {
                $cekRCVSCNWFIl = $this->findJobFun($req->data['item']);
                $hasil = $cekRCVSCNWFIl->first();

                // return $hasil;
                if (!empty($hasil->RCVSCN_LOTNO)) {
                    return [$hasil];
                }

                return [];
            }
        }

        $searchJob = collect(DB::select(DB::raw("exec sr_sp_checkJobTotalScrapQty @model = '" . $req->data['item'] . "', @jobSearch = '" . $req->data['filter'] . "'")));

        return $searchJob;
    }

    public function findStockItemMch($item, $po = '', $ser = '', $datePO = '')
    {
        $item = base64_decode($item);
        // return $item;
        $cekStock = MutasiStok::select(
            DB::raw('SUM(RCV_QTY) AS RCV_QTY'),
            'RCV_ITMCD',
            'RCV_PO',
            'RCVD_SERNNUM AS ITH_SER',
            'MITM_ITMD1',
            'it.ITH_QTY',
            'RCV_BCDATE',
            DB::raw("CONCAT(RCV_PO, '|', RCV_BCDATE) AS PO_DATE")
        )
            ->join('MITM_TBL', 'MITM_ITMCD', 'RCV_ITMCD')
            ->leftjoin(DB::raw("(
                SELECT
                    ITH_ITMCD,
                    ITH_DOC,
                    ITH_SER,
                    SUM(ITH_QTY) AS ITH_QTY
                FROM ITH_TBL
                WHERE ITH_WH IN ('PSIEQUIP', 'ENGEQUIP', 'MFG1EQUIP', 'MFG2EQUIP', 'QAEQUIP', 'PPICEQUIP')
                GROUP BY
                    ITH_ITMCD,
                    ITH_SER,
                    ITH_DOC
            ) it"), function ($j) {
                $j->on('it.ITH_ITMCD', 'RCV_ITMCD');
                $j->on('it.ITH_DOC', 'RCV_DONO');
            })
            ->leftJoin(DB::raw('(
                SELECT
                    RCVD_ITMID,
                    RCVD_SERNNUM,
                    RCVD_PO,
                    RCVD_DO
                FROM RCVD_TBL
            ) rtd'), function ($j) {
                $j->on('RCV_ITMCD', 'rtd.RCVD_ITMID');
                $j->on('RCV_DONO', 'rtd.RCVD_DO');
                $j->on('RCV_PO', 'rtd.RCVD_PO');
            })
            ->where('RCV_ITMCD', $item)
            ->whereIn('MITM_MODEL', ['0', '6'])
            ->groupBy(
                'RCV_ITMCD',
                'RCV_PO',
                'RCVD_SERNNUM',
                'it.ITH_QTY',
                'RCV_BCDATE',
                'MITM_ITMD1',
            )
            ->orderBy('RCV_BCDATE', 'asc')
            ->havingRaw('SUM(ITH_QTY) > 0');

        if (!empty($po)) {
            $po = base64_decode($po);
            $cekStock->where('RCV_PO', $po);
        }

        if (!empty($ser) && base64_decode($ser) != '0') {
            $ser = base64_decode($ser);
            $cekStock->where('ITH_SER', $ser);
        }

        if (!empty($datePO)) {
            $dpo = base64_decode($datePO);
            $cekStock->where('RCV_BCDATE', $dpo);
        }

        $response = [
            'status' => false,
            'message' => '',
            'data' => $cekStock->get(),
        ];

        if (count($cekStock->get()) > 0) {
            $response['status'] = true;
            $response['message'] = 'Stock found !';
            return response()->make($response, 200);
        }

        $response['message'] = 'Stock not found !';
        $response['errors'] = [
            'stock' => ['Stock not found !']
        ];
        return response()->make($response, 422);
    }

    public function findStockItem($item)
    {
        $item = base64_decode($item);

        $select = [
            DB::raw("CONCAT(MITM_ITMCD, '|', MITM_ITMD1) AS SEL_ITMCD"),
            'TOTQT'
        ];

        $cekStock = DB::connection('PSI_RPCUST')
            ->table('v_check_stock_item_to_ith')
            ->where('MITM_ITMCD', $item)
            ->orWhere('MITM_ITMD1', 'like', $item . '%')
            ->get();

        $response = [
            'status' => false,
            'message' => '',
            'data' => $cekStock,
        ];
        if (count($cekStock) > 0) {
            $response['status'] = true;
            $response['message'] = 'Stock found !';
            return response()->make($response, 200);
        }
    }

    public function findRetFG(Request $req)
    {
        $cekITH = ITHMaster::where('ITH_ITMCD', trim($req->data['item']))->where('ITH_QTY', '>', 0);

        if (isset($req->data['filter'])) {
            $cekITH->where('ITH_DOC', $req->data['filter']);
        }

        $dataITH = $cekITH->orderBy('ITH_LUPDT', 'DESC')->first();

        // return $dataITH;
        if (!empty($dataITH)) {
            if (trim($dataITH->ITH_WH) == 'NFWH4RT' || trim($dataITH->ITH_WH) == 'AFWH3RT') {
                $getITHForITEMCek = ITHMaster::where('ITH_REMARK', $dataITH->ITH_SER)->first();
                if (!empty($getITHForITEMCek)) {
                    $getITHForITEM = $getITHForITEMCek->ITH_ITMCD;
                } else {
                    $getITHForITEM = $dataITH->ITH_ITMCD;
                }

                $getRet = RETFG::select(
                    DB::raw('RETFG_DOCNO AS PIS2_WONO'),
                    DB::raw('RETFG_ITMCD AS PIS2_MDLCD'),
                    DB::raw("((
                        SELECT SUM(ITH_QTY)
                        FROM v_ith_tblc it2 WHERE it2.ITH_SER = '" . $dataITH->ITH_SER . "'
                        AND it2.ITH_WH IN ('NFWH4RT', 'AFWH3RT')
                    ) - (
                        SELECT COALESCE(SUM(QTY), 0)
                            FROM PSI_RPCUST.dbo.RPSCRAP_HIST
                            WHERE DOC_NO = RETFG_DOCNO
                            AND ITEMNUM = RETFG_ITMCD AND deleted_at IS NULL
                    )) AS TOTAL_QTY"),
                    DB::raw('1 AS IS_RETURN')
                )->where('RETFG_ITMCD', $getITHForITEM)
                    // ->join('v_ith_tblc', function ($j) {
                    //     $j->on('RETFG_DOCNO', '=', 'ITH_DOC');
                    //     $j->on('RETFG_ITMCD', '=', 'ITH_ITMCD');
                    // })
                    ->groupBy(
                        'RETFG_DOCNO',
                        'RETFG_ITMCD'
                    );

                if (isset($req->data['filter'])) {
                    $getRet->where('RETFG_DOCNO', 'LIKE', '%' . $req->data['filter'] . '%');
                }

                if (isset($req->pagination['sortBy']) && $req->pagination['sortBy'] !== 'desc') {
                    $getRet->orderBy($req->pagination['sortBy'], $req->pagination['descending'] ? 'desc' : 'asc');
                }

                $parsednya = $getRet->get()->toArray();
                $cek = 'masuk convert';
            } else {
                $getITHForITEM = trim($req->data['item']);

                $getRet = RETFG::select(
                    DB::raw('RETFG_DOCNO AS PIS2_WONO'),
                    DB::raw('RETFG_ITMCD AS PIS2_MDLCD'),
                    DB::raw("COALESCE(SUM(ITH_QTY), 0) -
                        (
                            SELECT COALESCE(SUM(QTY), 0)
                            FROM PSI_RPCUST.dbo.RPSCRAP_HIST
                            WHERE DOC_NO = RETFG_DOCNO AND ITEMNUM = RETFG_ITMCD AND deleted_at IS NULL
                        ) AS TOTAL_QTY"),
                    DB::raw('1 AS IS_RETURN')
                )->where('RETFG_ITMCD', $getITHForITEM)
                    ->join('v_ith_tblc', function ($j) {
                        $j->on('RETFG_DOCNO', '=', 'ITH_DOC');
                        $j->on('RETFG_ITMCD', '=', 'ITH_ITMCD');
                    })
                    ->whereIn('ITH_WH', ['AFQART', 'AFQART2'])
                    ->groupBy(
                        'RETFG_DOCNO',
                        'RETFG_ITMCD'
                    );

                if (isset($req->data['filter'])) {
                    $getRet->where('RETFG_DOCNO', 'LIKE', '%' . $req->data['filter'] . '%');
                }

                if (isset($req->pagination['sortBy']) && $req->pagination['sortBy'] !== 'desc') {
                    $getRet->orderBy($req->pagination['sortBy'], $req->pagination['descending'] ? 'desc' : 'asc');
                }

                $parsednya = $getRet->get()->toArray();
                $cek = 'masuk biasa';
            }
        }

        $hasil = [
            'data' => $parsednya,
            'material' => [],
            'cek' => $dataITH,
            'cek_test' => $cek,
            'item' => $getITHForITEM
        ];

        foreach ($parsednya as $key => $value) {
            // $hasil[] = array_merge($value, ['get_component' => $this->getPartFGReturn($value['PIS2_MDLCD'], $value['PIS2_WONO'])]);
            if (count($this->getPartFGReturn($value['PIS2_MDLCD'], $value['PIS2_WONO'])) > 0) {
                $hasil['material'][] = array_merge($value, ['get_component' => $this->getPartFGReturn($value['PIS2_MDLCD'], $value['PIS2_WONO'])]);
            }
        }

        // return $hasil;

        $parsedHasil = collect($hasil);

        if ($req->has('pagination')) {
            $rows = isset($req->pagination) ? $req->pagination['rowsPerPage'] : 5;
            $page = isset($req->pagination) ? $req->pagination['page'] : 1;

            if ($rows == 0) {
                // $cekall = $parsedHasil->count();
                return $parsedHasil->paginate(count($hasil));
            } else {
                return $parsedHasil->paginate($rows);
            }
        } else {
            return $hasil;
        }
    }

    public function findMchEq(Request $req)
    {
        $data = ITHMaster::select(
            DB::raw('ITH_DOC AS PIS2_WONO'),
            DB::raw('ITH_ITMCD AS PIS2_MDLCD'),
            DB::raw('CAST(COALESCE(SUM(ITH_QTY),0) - (SELECT COALESCE(SUM(QTY), 0) FROM PSI_RPCUST.dbo.RPSCRAP_HIST WHERE DOC_NO = ITH_DOC AND ITEMNUM = ITH_ITMCD AND deleted_at IS NULL) AS INT) AS TOTAL_QTY'),
            DB::raw('1 AS IS_MCH')
        )
            ->where('ITH_ITMCD', trim($req->data['item']))
            ->groupBy(
                'ITH_DOC',
                'ITH_ITMCD'
            );

        if (isset($req->data['filter'])) {
            $data->where('ITH_DOC', 'LIKE', '%' . $req->data['filter'] . '%');
        }

        $hasil = [
            'data' => $data->get(),
            'material' => []
        ];

        return $hasil;
    }

    public function getPartFGReturn($item, $doc)
    {
        $getRet = RETFG::select(
            'RETFG_DOCNO',
            'RETFG_ITMCD',
            'SERRC_BOMPN',
            'SERRC_BOMPNQTY',
            'SERRC_SER',
            'SERRC_SERX',
            'SERRC_SERXQTY',
            'SERRC_LOTNO'
        )
            ->join('SER_TBL', function ($f) {
                $f->on('SER_DOC', 'RETFG_DOCNO');
                $f->on('SER_ITMID', 'RETFG_ITMCD');
                $f->on('SER_PRDLINE', 'RETFG_LINE');
            })
            ->join('SERRC_TBL', function ($f) {
                $f->on('SERRC_SER', 'SER_ID');
            })
            ->where('RETFG_ITMCD', trim($item))
            ->where('RETFG_DOCNO', trim($doc))
            ->where('SERRC_BOMPN', '<>', '')
            ->where('SERRC_BOMPNQTY', '<>', '')
            ->where('SERRC_LOTNO', '<>', '')
            ->groupBy(
                'RETFG_DOCNO',
                'RETFG_ITMCD',
                'SERRC_BOMPN',
                'SERRC_BOMPNQTY',
                'SERRC_SER',
                'SERRC_SERX',
                'SERRC_SERXQTY',
                'SERRC_LOTNO'
            )
            ->get()
            ->toArray();

        return $getRet;
    }

    public function directSaveScrap(Request $req, $type)
    {
        $data = $req->data;

        $this->type = $type;
        $this->user = $req->user;
        $this->dept = $req->dept;
        $this->mainreason = $req->mainreason;
        $this->menu = $type;
        $this->other = $req->otherComp;
        $this->responsible = $req->reponsible;

        // return $req;

        switch ($type) {
            case 'wip':
                return $this->wipTransaction($data);
                break;
            case 'pending':
                return $this->pendingTransaction($data);
                break;
            case 'warehouse':
                return $this->warehouseTransaction($data);
                break;
            case 'return':
                return $this->FGReturnTransaction($data);
                break;
            case 'mchequip':
                $hasil = [];
                foreach ($data as $key => $value) {
                    $hasil[] = [
                        'MCH' => $value['part_of_qty'],
                        'MCH_PART' => isset($value['part_qty']) ? $value['part_qty'] : '',
                        'job_no' => $value['po'],
                        'reason' => $value['reason'],
                        'assy_no' => $value['part_of'],
                        'assy_no_part' => isset($value['item_code']) ? $value['item_code'] : '',
                        'date' => '',
                        'ref_no' => $value['ser_no'],
                        'partState' => $req->partState,
                        'purc_date' => $value['purc_date']
                    ];
                }
                return $this->MCHEquipmentTransaction($hasil);
                break;
            default:
                return [
                    'status' => false,
                    'message' => 'No template type found !',
                    'data' => []
                ];
                break;
        }

        return $req;
    }

    public function test()
    {
        $arr = json_decode('[{"RPDSGN_ITEMCD":"221432103","RPDSGN_PROCD":"SMT-B","RPDSGN_PRO_ID":"#1","TEST":"MFG1","MBO2_BOMRV":"1.50","PDPP_WONO":"22-1y20-221432103","PDPP_BOMRV":"1.50"},{"RPDSGN_ITEMCD":"221432103","RPDSGN_PROCD":"SMT-A","RPDSGN_PRO_ID":"#2","TEST":"MFG1","MBO2_BOMRV":"1.50","PDPP_WONO":"22-1y20-221432103","PDPP_BOMRV":"1.50"},{"RPDSGN_ITEMCD":"221432103","RPDSGN_PROCD":"SMT-HW","RPDSGN_PRO_ID":"SMT-HW","TEST":"MFG1","MBO2_BOMRV":"1.50","PDPP_WONO":"22-1y20-221432103","PDPP_BOMRV":"1.50"}]');

        $cek1 = current($arr);
        $cek2 = next($arr);
        $cek3 = current($arr);
        $cek4 = next($arr);
        $cek5 = current($arr);
        $cek6 = next($arr);
        $cek7 = current($arr);

        return [
            $cek1,
            $cek2,
            $cek3,
            $cek4,
            $cek5,
            $cek6,
            $cek7,
        ];
    }

    public function resubmitScrapDet($id_trans, $id = '')
    {
        set_time_limit(3600);
        $cekHist = scrapHist::where('ID_TRANS', base64_decode($id_trans))->where('IS_DELETE', '<>', 2)->withTrashed();

        if (!empty($id)) {
            $cekHist->where('id', base64_decode($id));
        }

        // return $cekHist->get()->toArray();

        $cekHist->restore();
        $data = clone $cekHist;
        // return $cekHist->get()->toArray();

        $checkMap = [];
        foreach ($data->get()->toArray() as $key => $value) {
            scrapHistDet::where('SCR_HIST_ID', $value['id'])->delete();

            $this->idTransaction = base64_decode($id_trans);
            $this->user = $value['USERNAME'];
            $this->dept = $value['DEPT'];
            $this->mainreason = $value['MAIN_REASON'];

            // return [
            //     base64_encode($value['ITEMNUM']),
            //     'NULL',
            //     base64_encode($value['DOC_NO']),
            //     null
            // ];

            if ($value['MENU_ID'] === 'fg_return') {
                $checkMapnya = [
                    'process_det' => [
                        [
                            'RPDSGN_PROCD' => 'RETURN',
                            'RPDSGN_PRO_ID' => 'RETURN'
                        ]
                    ]
                ];
            } else {
                if ($value['MDL_FLAG'] == 1) {
                    $checkMapnya = $this->getListMapProcess(
                        base64_encode($value['ITEMNUM']),
                        'NULL',
                        base64_encode($value['DOC_NO']),
                        null
                    )[0];
                } else {
                    $checkMapnya = [
                        'process_det' => [
                            [
                                'RPDSGN_PROCD' => 'MATERIAL',
                                'RPDSGN_PRO_ID' => 'MATERIAL'
                            ]
                        ]
                    ];
                }
            }

            // return $checkMapnya;

            // $checkMap[] = $checkMapnya;

            $transact = [];
            $parsingData = [];
            $parsingData['mapping_det'] = $checkMapnya['process_det'];
            $parsingData['job_no'] = $value['DOC_NO'];
            $parsingData['reason'] = $value['REASON'];
            $parsingData['assy_no'] = $value['ITEMNUM'];
            $parsingData['date'] = $value['DATE_OUT'];
            $parsingData['ref_no'] = $value['REF_NO'];

            $totalQty = 0;
            foreach ($checkMapnya['process_det'] as $keyDet => $valueDet) {
                if ($value['SCR_PROCD'] === 'SMT-HW') {
                    if ($valueDet['RPDSGN_PRO_ID'] == $value['SCR_PROCD']) {
                        $parsingData[$valueDet['RPDSGN_PRO_ID']] = $value['QTY'];
                        $totalQty += $value['QTY'];
                    } else {
                        $parsingData[$valueDet['RPDSGN_PRO_ID']] = 0;
                    }
                } else {
                    if ($valueDet['RPDSGN_PROCD'] == $value['SCR_PROCD']) {
                        // return ['ada loh', $value['QTY'], $value];
                        $parsingData[$valueDet['RPDSGN_PRO_ID']] = (int) $value['QTY'];
                        $totalQty += (int) $value['QTY'];
                    } else {
                        $parsingData[$valueDet['RPDSGN_PRO_ID']] = 0;
                    }
                }
            }

            $parsingData['tot_qty'] = $totalQty;

            // return [$parsingData, $checkMapnya['process_det']];

            // return $cek;
            scrapHist::where('id', base64_decode($id))->update([
                'IS_DELETE' => 2
            ]);

            if ($value['MENU_ID'] === 'fg_return') {
                $this->menu = 'fg_return';
                $cek = $this->storeDataFGReturn(
                    $parsingData
                );
            } elseif ($value['MENU_ID'] === 'pending' && $value['MDL_FLAG'] == '1') {
                $this->menu = 'pending';
                $cek = $this->storeDataFinishGood(
                    $checkMapnya['process_det'],
                    $parsingData,
                    $value['MDL_FLAG'] === 1
                    ? ['QAFG']
                    : ['ARWH0PD']
                );
            } elseif ($value['MENU_ID'] === 'pending' && $value['MDL_FLAG'] == '0') {
                $this->menu = 'pending';
                $cek = $this->storeDataMaterial(
                    $parsingData,
                    $value['MDL_FLAG'] === 1
                    ? ['QAFG']
                    : ['ARWH0PD']
                );
            } else {
                if ($value['MENU_ID'] === 'warehouse') {
                    $this->menu = 'warehouse';
                    if ($value['MDL_FLAG'] == 1) {
                        $cek = $this->storeDataFinishGood(
                            $checkMapnya['process_det'],
                            $parsingData,
                            $value['MDL_FLAG'] == 1
                            ? ['AFWH3']
                            : ['ARWH1', 'ARWH2', 'NRWH2']
                        );
                    } else {
                        $cek = $this->storeDataMaterial(
                            $parsingData,
                            ['ARWH1', 'ARWH2', 'NRWH2']
                        );
                    }
                } else {
                    $this->menu = 'wip';
                    $cek = $this->storeDataFinishGood(
                        $checkMapnya['process_det'],
                        $parsingData,
                        ['ARQA1']
                    );
                }
            }
            $bcDel = $this->cancelEXBC($value['ID_TRANS'], base64_decode($id));

            ITHMaster::where('ITH_REMARK', (string) $value['id'])->where('ITH_USRID', 'SCR_RPT')->delete();
            scrapHist::where('id', $value['id'])->delete();

            $checkMap[] = $cek;
        }

        return $checkMap;

        return $cekHist->get()->toArray();
    }

    public function getListMapProcess($assyCode = '', $processCode = '', $jobs = '', $id = '')
    {
        $processCode = $processCode === 'NULL' ? '' : $processCode;

        $assyCodeParse = base64_decode($assyCode);
        $processCodeParse = base64_decode($processCode);
        $processJob = base64_decode($jobs);

        $parseHasil = processMapMstr::with([
            'processDet' => function ($q) use ($assyCode, $processCode, $assyCodeParse, $processCodeParse, $id) {
                $q->select(
                    DB::raw('MAX(id) as id'),
                    'RPDSGN_PRO_ID',
                    'RPDSGN_ITEMCD',
                    'RPDSGN_MSTR_ID',
                    DB::raw('MAX(RPDSGN_PROCD) as RPDSGN_PROCD')
                );

                if (!empty($assyCode)) {
                    if (strpos($assyCodeParse, 'ASP') !== false || strpos($assyCodeParse, 'KD') !== false || strpos($assyCodeParse, 'KDASP') !== false) {
                        $newAssyCode = str_replace(['ASP', 'KD', 'KDASP'], '', $assyCodeParse);
                        $q->where('RPDSGN_ITEMCD', '=', $newAssyCode);
                    } else {
                        $q->where('RPDSGN_ITEMCD', '=', $assyCodeParse);
                    }
                }

                if (!empty($processCode)) {
                    logger($processCodeParse);
                    $q->where(
                        function ($w) use ($processCodeParse) {
                            $w->where('RPDSGN_PROCD', trim($processCodeParse));
                            $w->orWhere('RPDSGN_PROCD', 'ALL');
                        }
                    );
                }

                if (!empty($id) && is_numeric($id)) {
                    $q->where('id', $id);
                }

                $q->where('RPDSGN_PROCD', '<>', '');

                $q->with('processMstr')->orderBy('RPDSGN_ITEMCD')->orderBy('RPDSGN_PRO_ID');

                $q->groupBy(
                    'RPDSGN_MSTR_ID',
                    'RPDSGN_ITEMCD',
                    'RPDSGN_PRO_ID'
                );
            }
        ]);
        // return 'bnla';

        // return $parseHasil->get()->toArray();

        // logger('cek sebelum fetch');
        // logger((clone $parseHasil)->get()->toArray());

        $hasil = [];
        foreach ($parseHasil->get()->toArray() as $key => $value) {
            $hasilDet = [];
            foreach ($value['process_det'] as $keyProcess => $valueProcess) {
                $hasilDet[] = array_merge($valueProcess, [
                    'last_process_stat' => $valueProcess['RPDSGN_PRO_ID'] === $value['process_det'][count($value['process_det']) - 1]['RPDSGN_PRO_ID']
                        ? true
                        : false
                ]);
            }

            // logger('cek mapping');
            // logger($value['process_det']);
            // logger($hasilDet);
            // return $hasilDet;

            if (count($hasilDet) === 0) {
                // $whereJob = empty($jobs) ? NULL : "AND PDPP_WONO = '" . $processJob . "'";

                // $expID = explode('-', $id);
                // $getSeq = empty($id) ? NULL : "AND MBO2_SEQNO = " . $expID[count($expID) - 1];
                // $processJobs = DB::select("
                //     select * FROM PSI_RPCUST.dbo.v_cims_check_list
                //     WHERE MBO2_MDLCD = '" . $assyCodeParse . "'
                //     $whereJob
                //     $getSeq
                //     ORDER BY MBO2_SEQNO
                // ");

                $assyCD = $assyCodeParse;
                $whereAssy = empty($assyCD) || (strpos($assyCD, 'KD') || strpos($assyCD, 'ES')) !== false ? NULL : "AND MBO2_MDLCD = '" . $assyCD . "'";

                $expID = explode('-', $id);
                $getSeq = empty($id) ? NULL : "AND MBO2_SEQNO = " . $expID[count($expID) - 1];

                $processJobs = DB::connection('PSI_RPCUST')->select("
                    select * FROM PSI_RPCUST.dbo.v_cims_check_list
                    WHERE PDPP_WONO = '" . $processJob . "'
                    $whereAssy
                    $getSeq
                    ORDER BY MBO2_SEQNO
                ");

                // return $getSeq;

                $processJobs = array_map(function ($value) {
                    return (array) $value;
                }, $processJobs);

                // return $processJobs;
                $cekDataMGF = array_filter($processJobs, function ($f) use ($value) {
                    return $f['TEST'] === $value['RPDSGN_CODE'];
                });

                // return $cekDataMGF;
                // $hasilDet = $processJobs;

                // logger($processJobs);

                if (count($cekDataMGF) > 0) {
                    $cekSMTAB = array_filter($processJobs, function ($f) {
                        return $f['RPDSGN_PROCD'] === 'SMT-AB';
                    });

                    // return $cekSMTAB;
                    if (count($cekSMTAB) > 0) {
                        if (count($cekSMTAB) > 1) {
                            $hasil[] = array_merge($value, ['process_det' => $this->removeProcess1($processJobs)]);
                        } else {
                            array_merge([
                                'MBO2_BOMRV' => $cekSMTAB[0]['MBO2_BOMRV'],
                                'PDPP_BOMRV' => $cekSMTAB[0]['PDPP_BOMRV'],
                                'PDPP_WONO' => $cekSMTAB[0]['PDPP_WONO'],
                                'RPDSGN_ITEMCD' => $cekSMTAB[0]['RPDSGN_ITEMCD'],
                                'RPDSGN_PROCD' => $cekSMTAB[0]['RPDSGN_PROCD'],
                                'RPDSGN_PRO_ID' => '#2',
                                'TEST' => $cekSMTAB[0]['TEST'],
                            ], $processJobs);

                            // return $processJobs;

                            $hasil[] = array_merge($value, ['process_det' => $this->removeProcess1($processJobs)]);
                        }
                    } else {
                        $hasil[] = array_merge($value, ['process_det' => $this->removeProcess1($processJobs)]);
                    }
                }
            } else {
                $hasil[] = array_merge($value, ['process_det' => $this->removeProcess1($hasilDet)]);
            }
        }

        return $hasil;
    }

    public function removeProcess1($data)
    {
        $cekABProcess = array_filter($data, function ($f) {
            return $f['RPDSGN_PROCD'] === 'SMT-AB';
        });

        if (count($cekABProcess) > 1) {
            $hilangkanPro1 = array_values(array_filter($data, function ($f) {
                return $f['RPDSGN_PRO_ID'] !== '#1';
            }));

            return $hilangkanPro1;
        } else {
            return $data;
        }
    }

    public function resubmitITH($id)
    {
        $cekHist = scrapHist::where('id', base64_decode($id))
            ->leftJoin(DB::raw('(
                SELECT
                    ITH_DOC,
                    ITH_WH,
                    ITH_REMARK,
                    ITH_DATEC,
                    SUM(ITH_QTY) AS ITH_QTY
                FROM PSI_WMS.dbo.v_ith_tblc
                GROUP BY
                    ITH_DOC,
                    ITH_WH,
                    ITH_REMARK,
                    ITH_DATEC
            ) ith'), 'ith.ITH_REMARK', DB::raw('CAST(id AS VARCHAR(100))'))
            ->whereNull('ith.ITH_REMARK')
            ->first();

        if (!empty($cekHist)) {
            // If Finish good
            if ($cekHist->MDL_FLAG == 1) {
                // IF From Pending
                if ($cekHist->QTY_SCRAP <> 0 || $cekHist->MENU_ID === 'pending') {
                    if (!empty($cekHist->REF_NO)) {
                        $cekHistITH = ITHMaster::select(DB::raw('SUM(ITH_QTY) AS QTY'))->where('ITH_SER', $cekHist->REF_NO)->where('ITH_WH', 'QAFG')->first();

                        // return $cekHistITH;
                        if (!empty($cekHistITH->QTY) && $cekHistITH->QTY > 0) {
                            $cekHistDet = ITHMaster::where('ITH_SER', $cekHist->REF_NO)->where('ITH_WH', 'QAFG')->orderBy('ITH_LUPDT', 'desc')->first();
                            // return $cekHistDet;

                            $issue = $this->insertToITH(
                                trim($cekHist->ITEMNUM),
                                date('Y-m-d', strtotime($cekHist->DATE_OUT)),
                                'OUT-PEN-FG',
                                $cekHistDet->ITH_DOC,
                                round($cekHistDet->ITH_QTY) * -1,
                                $cekHistDet->ITH_WH,
                                $cekHist->REF_NO,
                                $cekHist->id,
                                'SCR_RPT'
                            );

                            // Receive
                            $receive = $this->insertToITH(
                                trim($cekHist->ITEMNUM),
                                date('Y-m-d', strtotime($cekHist->DATE_OUT)),
                                'INC-SCR-FG',
                                $cekHist->ID_TRANS,
                                round($cekHistDet->ITH_QTY),
                                'AFWH9SC',
                                $cekHist->REF_NO,
                                $cekHist->id,
                                'SCR_RPT'
                            );

                            return [
                                'status' => true,
                                'issue' => $issue,
                                'receive' => $receive,
                                'message' => 'Stock berhasil di pindah !'
                            ];
                        } else {
                            return [
                                'status' => false,
                                'message' => 'Stock already 0 !'
                            ];
                        }
                    } else {
                        return [
                            'status' => false,
                            'message' => 'Pending scrap must has a Ref Code !'
                        ];
                    }
                } elseif ($cekHist->IS_RETURN <> 0 || $cekHist->MENU_ID == 'fg_return') { // If From Return FG
                    $cekHistITH = ITHMaster::select(DB::raw('SUM(ITH_QTY) AS QTY'))->where('ITH_SER', $cekHist->REF_NO)->whereIn('ITH_WH', ['AFQART', 'AFQART2', 'NFWH4RT'])->first();

                    if (!empty($cekHistITH->QTY) && $cekHistITH->QTY > 0) {
                        $cekHistDet = ITHMaster::where('ITH_SER', $cekHist->REF_NO)->whereIn('ITH_WH', ['AFQART', 'AFQART2', 'NFWH4RT'])->orderBy('ITH_LUPDT', 'desc')->first();
                        $issue = $this->insertToITH(
                            trim($cekHist->ITEMNUM),
                            date('Y-m-d', strtotime($cekHist->DATE_OUT)),
                            'OUT-RTN-SCR-FG',
                            $cekHistDet->ITH_DOC,
                            round($cekHist->QTY) * -1,
                            $cekHistDet->ITH_WH,
                            $cekHist->REF_NO,
                            $cekHist->id,
                            'SCR_RPT'
                        );

                        // Receive
                        $receive = $this->insertToITH(
                            trim($cekHist->ITEMNUM),
                            date('Y-m-d', strtotime($cekHist->DATE_OUT)),
                            'INC-SCR-FG',
                            $cekHist->ID_TRANS,
                            round($cekHist->QTY),
                            'AFWH9SC',
                            $cekHist->REF_NO,
                            $cekHist->id,
                            'SCR_RPT'
                        );

                        return [
                            'status' => true,
                            'issue' => $issue,
                            'receive' => $receive,
                            'message' => 'Stock berhasil di pindah !'
                        ];
                    }

                    return [
                        'status' => false,
                        'message' => 'FG Return with ref : ' . $cekHist->REF_NO . ' already 0 !',
                        'data_os' => $cekHistITH
                    ];
                } else { // If from WIP / Warehouse
                    if ($cekHist->MENU_ID === 'wip') {
                        $cekHistITH = ITHMaster::select(DB::raw('SUM(ITH_QTY) AS QTY'))->where('ITH_SER', $cekHist->REF_NO)->whereIn('ITH_WH', ['ARQA1'])->first();
                        // return $cekHistITH;

                        // If WIP Using Ref Code
                        if (!empty($cekHistITH->QTY) && $cekHistITH->QTY > 0) {
                            $cekHistDet = ITHMaster::where('ITH_SER', $cekHist->REF_NO)->whereIn('ITH_WH', ['ARQA1'])->orderBy('ITH_LUPDT', 'desc')->first();
                            // return $cekHistDet;

                            $issue = $this->insertToITH(
                                trim($cekHist->ITEMNUM),
                                date('Y-m-d', strtotime($cekHist->DATE_OUT)),
                                'OUT-WH-SCR-FG',
                                $cekHistDet->ITH_DOC,
                                round($cekHistDet->ITH_QTY) * -1,
                                $cekHistDet->ITH_WH,
                                $cekHist->REF_NO,
                                $cekHist->id,
                                'SCR_RPT'
                            );

                            // Receive
                            $receive = $this->insertToITH(
                                trim($cekHist->ITEMNUM),
                                date('Y-m-d', strtotime($cekHist->DATE_OUT)),
                                'INC-SCR-FG',
                                $cekHist->ID_TRANS,
                                round($cekHistDet->ITH_QTY),
                                'AFWH9SC-',
                                $cekHist->REF_NO,
                                $cekHist->id,
                                'SCR_RPT'
                            );

                            return [
                                'status' => true,
                                'issue' => $issue,
                                'receive' => $receive,
                                'message' => 'Stock berhasil di pindah !'
                            ];
                        } else {
                            $cekOsITHData = DB::table('v_ith_tblc')->select(
                                'ITH_SER',
                                'ITH_WH',
                                'ITH_DOC',
                                'ITH_DATEC',
                                DB::raw('SUM(ITH_QTY) AS QTY')
                            )
                                ->where('ITH_DOC', $cekHist->DOC_NO)
                                ->whereIn('ITH_WH', ['ARQA1'])
                                ->groupBy(
                                    'ITH_SER',
                                    'ITH_WH',
                                    'ITH_DOC',
                                    'ITH_DATEC'
                                )
                                ->havingRaw('SUM(ITH_QTY) >= ' . $cekHist->QTY)
                                ->orderBy('ITH_DATEC', 'ASC')
                                ->first();

                            if (!empty($cekOsITHData->QTY) && $cekOsITHData->QTY > 0) {
                                $issue = $this->insertToITH(
                                    trim($cekHist->ITEMNUM),
                                    date('Y-m-d', strtotime($cekHist->DATE_OUT)),
                                    'OUT-WH-SCR-FG',
                                    $cekOsITHData->ITH_DOC,
                                    round($cekHist->QTY) * -1,
                                    $cekOsITHData->ITH_WH,
                                    $cekOsITHData->ITH_SER,
                                    $cekHist->id,
                                    'SCR_RPT'
                                );

                                // Receive
                                $receive = $this->insertToITH(
                                    trim($cekHist->ITEMNUM),
                                    date('Y-m-d', strtotime($cekHist->DATE_OUT)),
                                    'INC-SCR-FG',
                                    $cekHist->ID_TRANS,
                                    round($cekHist->QTY),
                                    'AFWH9SC',
                                    $cekOsITHData->ITH_SER,
                                    $cekHist->id,
                                    'SCR_RPT'
                                );

                                return [
                                    'status' => true,
                                    'issue' => $issue,
                                    'receive' => $receive,
                                    'data' => $cekOsITHData,
                                    'message' => 'Stock berhasil di pindah !'
                                ];
                            }
                            return [
                                'status' => false,
                                'message' => 'WIP Stock already 0 !',
                                'data_os' => $cekOsITHData
                            ];
                        }
                    } elseif ($cekHist->MENU_ID === 'warehouse') {
                        $cekHistITH = ITHMaster::select(DB::raw('SUM(ITH_QTY) AS QTY'))->where('ITH_SER', $cekHist->REF_NO)->whereIn('ITH_WH', $cekHist->MDL_FLAG == 1 ? ['AFWH3'] : ['ARWH1', 'ARWH2', 'NRWH2', 'AIWH1'])->first();
                        // return $cekHistITH;
                        if (!empty($cekHistITH->QTY) && $cekHistITH->QTY > 0) {
                            $cekHistDet = ITHMaster::where('ITH_SER', $cekHist->REF_NO)->whereIn('ITH_WH', $cekHist->MDL_FLAG == 1 ? ['AFWH3'] : ['ARWH1', 'ARWH2', 'NRWH2', 'AIWH1'])->orderBy('ITH_LUPDT', 'desc')->first();
                            // return $cekHistDet;

                            $issue = $this->insertToITH(
                                trim($cekHist->ITEMNUM),
                                date('Y-m-d', strtotime($cekHist->DATE_OUT)),
                                'OUT-WH-SCR-FG',
                                $cekHistDet->ITH_DOC,
                                round($cekHistDet->ITH_QTY) * -1,
                                $cekHistDet->ITH_WH,
                                $cekHist->REF_NO,
                                $cekHist->id,
                                'SCR_RPT'
                            );

                            // Receive
                            $receive = $this->insertToITH(
                                trim($cekHist->ITEMNUM),
                                date('Y-m-d', strtotime($cekHist->DATE_OUT)),
                                'INC-SCR-FG',
                                $cekHist->ID_TRANS,
                                round($cekHistDet->ITH_QTY),
                                'AFWH9SC',
                                $cekHist->REF_NO,
                                $cekHist->id,
                                'SCR_RPT'
                            );

                            return [
                                'status' => true,
                                'issue' => $issue,
                                'receive' => $receive,
                                'message' => 'Stock berhasil di pindah !'
                            ];
                        } else {
                            return [
                                'status' => false,
                                'message' => 'Stock already 0 !'
                            ];
                        }

                        return 'Warehouse';
                    } else {
                        if ($cekHist->QTY_SCRAP == 0 && $cekHist->IS_RETURN == 0) {
                            $cekHistITH = ITHMaster::select(
                                'ITH_SER',
                                'ITH_DOC',
                                'ITH_WH',
                                'ITH_DATE',
                                DB::raw('SUM(ITH_QTY) AS QTY')
                            )
                                ->where('ITH_DOC', $cekHist->DOC_NO)
                                ->where('ITH_ITMCD', $cekHist->ITEMNUM)
                                ->where('ITH_DATE', '<=', $cekHist->DATE_OUT)
                                ->whereIn('ITH_WH', ['ARQA1'])
                                ->groupBy('ITH_SER', 'ITH_DATE', 'ITH_WH', 'ITH_DOC')
                                ->havingRaw('SUM(ITH_QTY) = ' . round($cekHist->QTY))
                                ->orderBy('ITH_DATE')
                                ->first();

                            // return $cekHistITH;

                            if (!empty($cekHistITH->QTY) && $cekHistITH->QTY > 0) {
                                $issue = $this->insertToITH(
                                    trim($cekHist->ITEMNUM),
                                    date('Y-m-d', strtotime($cekHist->DATE_OUT)),
                                    'OUT-WH-SCR-FG',
                                    $cekHistITH->ITH_DOC,
                                    round($cekHist->QTY) * -1,
                                    $cekHistITH->ITH_WH,
                                    $cekHistITH->ITH_SER,
                                    $cekHistITH->id,
                                    'SCR_RPT'
                                );

                                // Receive
                                $receive = $this->insertToITH(
                                    trim($cekHist->ITEMNUM),
                                    date('Y-m-d', strtotime($cekHist->DATE_OUT)),
                                    'INC-SCR-FG',
                                    $cekHist->ID_TRANS,
                                    round($cekHist->QTY),
                                    'AFWH9SC',
                                    $cekHistITH->ITH_SER,
                                    $cekHist->id,
                                    'SCR_RPT'
                                );

                                return [
                                    'issue' => $issue,
                                    'receive' => $receive,
                                    'status' => true,
                                    'message' => 'Stock berhasil di pindah !'
                                ];
                            } else {
                                return [
                                    'status' => false,
                                    'data' => $cekHistITH,
                                    'message' => 'Stock already 0 !'
                                ];
                            }
                        }

                        return [
                            'status' => false,
                            'message' => 'Except WIP, Warehouse or Pending, Not identified !!'
                        ];
                    }
                }
            } else { // If Material
                return [
                    'status' => false,
                    'message' => 'Material Resubmit ITH not implemented yet !'
                ];
            }
        } else {
            return [
                'status' => false,
                'message' => 'ID Scrap not found or already submited ITH!'
            ];
        }
    }

    public function resubmitBCStock($id)
    {
        $cekHist = scrapHist::where('id', base64_decode($id))->first();
        $cekHistDet = scrapHistDet::where('SCR_HIST_ID', base64_decode($id))->get();

        $item = [];
        $qty = [];
        foreach ($cekHistDet as $key => $value) {
            $item[] = $value['SCR_ITMCD'];
            $qty[] = $value['SCR_QTY'];
        }

        // return [
        //     $item,
        //     $cekHist->DATE_OUT,
        //     null,
        //     $qty,
        //     $cekHist->ID_TRANS,
        //     null,
        //     $cekHist->id
        // ];

        $removeExbc = $this->cancelEXBC($cekHist->ID_TRANS, base64_decode($id));
        $insertExBC = $this->sendingParamBCStock(
            $item,
            $cekHist->DATE_OUT,
            null,
            $qty,
            $cekHist->ID_TRANS,
            null,
            $cekHist->id
        );

        return [
            'REMOVE_EXBC_STAT' => $removeExbc,
            'SEND_EXBC_STAT' => $insertExBC
        ];
    }

    public function sendingParamBCStock($item_num, $date_out = '', $lot = null, $qty = 0, $doc = null, $bc = null, $loc = '', $isArray = true)
    {
        if ($isArray) {
            $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/getStockBCArray';
            $localendpoint = 'http://localhost/api-report-custom/api/inventory/getStockBCArray';
        } else {
            $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/getStockBC';
            $localendpoint = 'http://localhost/api-report-custom/api/inventory/getStockBC';
        }

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item_num,
            'date_out' => empty($date_out) ? date('Y-m-d') : $date_out,
            // 'lot' => $lot,
            'qty' => $qty,
            'doc' => $doc,
            'bc' => $bc,
            'loc' => $loc
            // 'scrap' => true,
            // 'remark' => $id,
            // 'revise' => $rev
        ];

        $res = $guzz->request('POST', $endpoint, ['form_params' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content['CURL'];
    }

    public function cancelEXBC($doc, $loc = '')
    {
        $endpoint = empty($loc) ? 'http://192.168.0.29:8080/api_inventory/api/inventory/cancelDO/' . base64_encode($doc) : 'http://192.168.0.29:8080/api_inventory/api/inventory/cancelDO/' . base64_encode($doc) . '/' . base64_encode($loc);
        $guzz = new \GuzzleHttp\Client();

        $res = $guzz->request('GET', $endpoint);

        $content['CURL'] = json_decode($res->getBody(), true);

        return $content['CURL'];
    }

    public function reviseScrap($id, $methods = 'doc')
    {
        set_time_limit(3600);
        if ($methods === 'doc') {
            $getScrapMaster = scrapHist::where('ID_TRANS', base64_decode($id))->get()->toArray();

            if (empty($getScrapMaster)) {
                return 'not_found';
            }

            $cancel = $this->cancelBCStock(base64_decode($id));

            $hasil = [];
            if ($cancel['status'] !== 'failed') {
                foreach ($getScrapMaster as $key => $value) {
                    scrapHist::where('id', $value['id'])->update([
                        'IS_DELETE' => 1
                    ]);

                    $cekITH = ITHMaster::where('ITH_ITMCD', trim($value['ITEMNUM']))
                        ->where('ITH_REMARK', DB::raw('CAST(' . $value['id'] . ' AS VARCHAR(100))'))
                        ->where('ITH_USRID', 'SCR_RPT')
                        ->delete();

                    $hasil[] = scrapHistDet::where('SCR_HIST_ID', $value['id'])->delete();
                }

                scrapHist::where('ID_TRANS', base64_decode($id))->delete();
            } else {
                foreach ($getScrapMaster as $key => $value) {
                    scrapHist::where('id', $value['id'])->update([
                        'IS_DELETE' => 2
                    ]);

                    scrapHist::where('id', $value['id'])->delete();
                }
            }

            return [
                'DATA' => $hasil,
                'bcstock' => $cancel
            ];
        } else {
            $getScrapMaster = scrapHist::whereIn('id', json_decode(base64_decode($id)))->get()->toArray();
            if (empty($getScrapMaster)) {
                return 'not_found';
            }

            $hasil = [];
            foreach ($getScrapMaster as $key => $value) {
                $hasil[] = scrapHistDet::where('SCR_HIST_ID', $value['id'])->delete();

                $cancel = $this->cancelBCStock($value['ID_TRANS'], $value['id']);
                scrapHist::where('id', $value['id'])->update([
                    'IS_DELETE' => 2
                ]);

                if ($cancel['status'] !== 'failed') {
                    ITHMaster::where('ITH_ITMCD', trim($value['ITEMNUM']))
                        ->where('ITH_REMARK', DB::raw('CAST(' . $value['id'] . ' AS VARCHAR(100))'))
                        ->where('ITH_USRID', 'SCR_RPT')
                        ->delete();

                    scrapHist::where('id', $value['id'])->delete();
                    // $cekDet = scrapHistDet::where('SCR_HIST_ID', $value['id'])->get();

                    // foreach ($cekDet as $keyDet => $valueDet) {
                    //     // $cekRcvScnForPrice = $this->cekLatestHarga($valueDet['SCR_ITMCD'], $valueDet['SCR_LOTNO']);

                    //     // $hasil[] = $this->sendingParamBCStock(
                    //     //     $valueDet['SCR_ITMCD'],
                    //     //     $value['DATE_OUT'],
                    //     //     $valueDet['SCR_LOTNO'],
                    //     //     round($valueDet['SCR_QTY']) * -1,
                    //     //     base64_decode($id),
                    //     //     $cekRcvScnForPrice['RCV_BCTYPE']
                    //     // );

                    //     // $cekITH = ITHMaster::where('ITH_ITMCD', $valueDet['SCR_ITMCD'])
                    //     //     ->where('ITH_SER', $valueDet['id'])
                    //     //     ->where('ITH_USRID', 'SCR_RPT')
                    //     //     ->get();

                    //     // foreach ($cekITH as $keyITH => $valueITH) {
                    //     //     $this->insertToITH(
                    //     //         trim($valueDet['SCR_ITMCD']),
                    //     //         $valueITH['ITH_DATE'],
                    //     //         $valueITH['ITH_FORM'],
                    //     //         $valueITH['ITH_DOC'],
                    //     //         $valueITH['ITH_QTY'] * -1,
                    //     //         $valueITH['ITH_WH'],
                    //     //         $valueDet['id'],
                    //     //         'SCR_REV',
                    //     //         'SCR_RPT'
                    //     //     );
                    //     // }
                    // }
                } else {
                    scrapHist::where('id', $value['id'])->update([
                        'IS_DELETE' => 0
                    ]);
                }
            }

            scrapHist::whereIn('id', json_decode(base64_decode($id)))->delete();

            return [
                'DATA' => $hasil
            ];
        }
    }

    public function cancelBCStock($id, $loc = '')
    {
        if (empty($loc)) {
            $endpoint = 'http://192.168.0.29:8080/api_inventory/api/inventory/cancelDO/' . base64_encode($id);
        } else {
            $endpoint = 'http://192.168.0.29:8080/api_inventory/api/inventory/cancelDO/' . base64_encode($id) . '/' . base64_encode($loc);
        }

        $content = [];
        $guzz = new \GuzzleHttp\Client();

        $res = $guzz->request('GET', $endpoint);

        $content['CURL'] = json_decode($res->getBody(), true);

        return $content['CURL'];
    }

    public function reportTransaction(Request $req)
    {
        $scrap = DB::connection('PSI_RPCUST')->table('v_scrap_report');
        if ($req->has('filter') && !empty($req->filter)) {
            $scrap->where('ID_TRANS', 'like', '%' . $req->filter . '%');
        }

        $findGRP = UserMaster::where('MSTEMP_ID', '=', $req->user)->first();

        $sc = $scrap->where('MSTEMP_GRP', '=', $findGRP->MSTEMP_GRP)->orderBy('ID_TRANS', 'DESC');

        $hasil = $sc->paginate(($req->rowsPerPage === 0 ? $sc->count() : $req->rowsPerPage), ['*'], 'page', $req->page);

        // return $hasil;
        $hasil->getCollection()->transform(function ($value) {
            $setReq = new Request([
                'rowsPerPage' => 10,
                'page' => 1
            ]);

            return array_merge(
                (array) $value,
                ['DET' => []]
            );
        });

        $custom = collect(['loading' => false]);

        return $custom->merge($hasil);
    }

    public function reportTransactionDetail(Request $req, $id, $summaryItem = false)
    {
        set_time_limit(3600);

        if ($summaryItem) {
            $sel = [
                'RPSCRAP_HIST.ITEMNUM',
                'RPSCRAP_HIST.ID_TRANS',
                'MITM_ITMD1',
                'RPSCRAP_HIST.MDL_FLAG',
                DB::raw('SUM(RPSCRAP_HIST.QTY) + COALESCE(td.TOT_QT_TD, 0) AS QTY'),
                DB::raw('SUM(RPSCRAP_HIST.QTY) AS QTYORI')
            ];
        } else {
            $sel = [
                'RPSCRAP_HIST.*',
                DB::raw('CAST(COALESCE(det_c.det_qty, 0) AS INT) AS det_sum'),
                DB::raw("COALESCE((
                    SELECT CAST(SUM(vfse.SCR_QTY) AS INT) FROM v_failed_scrap_exbc vfse WHERE vfse.SCR_HIST_ID = RPSCRAP_HIST.id
                ), 0) AS exbc_sum"),
                DB::raw('COALESCE(det_ith.det_qty, 0) AS ith_sum')
            ];
        }

        $scrapHist = scrapHist::select($sel)
            ->where('IS_DELETE', '<>', 2)
            ->leftJoin(DB::raw('(
                SELECT
                    det.SCR_HIST_ID,
                    SUM(SCR_QTY) AS det_qty
                FROM RPSCRAP_HIST_DET det
                WHERE det.deleted_at IS NULL
                GROUP BY
                    det.SCR_HIST_ID
            ) det_c'), 'det_c.SCR_HIST_ID', 'RPSCRAP_HIST.id')
            ->leftJoin(DB::raw('(
                SELECT
                    ITH_DOC,
                    ITH_REMARK,
                    SUM(CAST(ITH_QTY AS BIGINT)) as det_qty
                FROM PSI_WMS.dbo.ITH_TBL
                GROUP BY
                    ITH_DOC,
                    ITH_REMARK
            ) det_ith'), function ($j) {
                $j->on('det_ith.ITH_DOC', 'RPSCRAP_HIST.ID_TRANS');
                $j->on('det_ith.ITH_REMARK', DB::raw('CAST(RPSCRAP_HIST.id AS VARCHAR(50))'));
            })
            ->join('PSI_WMS.dbo.MITM_TBL', 'ITEMNUM', 'MITM_ITMCD')
            ->leftJoin(DB::raw('(
                select
                    SUM(rtd.STD_QTY) AS TOT_QT_TD,
                    rh.ID_TRANS AS ID_TRANS_TD,
                    rh.ITEMNUM AS ITEMNUM_TD
                from
                    PSI_RPCUST.dbo.RPSCRAP_TRANS_DET rtd
                INNER JOIN PSI_RPCUST.dbo.RPSCRAP_HIST rh on rtd.SH_ID = rh.id
                GROUP BY
                    rh.ID_TRANS,
                    rh.ITEMNUM
            ) td'), function ($j) {
                $j->on('td.ID_TRANS_TD', 'RPSCRAP_HIST.ID_TRANS');
                $j->on('td.ITEMNUM_TD', 'RPSCRAP_HIST.ITEMNUM');
            })
            ->withTrashed();

        if ($req->has('listID') && count($req->listID) > 0) {
            $scrapHist->whereIn('ID_TRANS', $req->listID);
        } else {
            $scrapHist->where('ID_TRANS', base64_decode($id));
        }

        if (!empty($req->filter)) {
            $scrapHist = $scrapHist
                ->where('ITEMNUM', 'like', '%' . $req->filter . '%');
        }

        if ($summaryItem) {
            $scrapHist->groupBy(
                'RPSCRAP_HIST.MDL_FLAG',
                'RPSCRAP_HIST.ID_TRANS',
                'RPSCRAP_HIST.ITEMNUM',
                'MITM_ITMD1',
                'td.TOT_QT_TD'
            )
                ->havingRaw('SUM(RPSCRAP_HIST.QTY) + COALESCE(td.TOT_QT_TD, 0) > 0');
        }

        $hasil = $scrapHist->orderBy('ID_TRANS')->orderBy('ITEMNUM')->paginate($req->rowsPerPage === 0 ? $scrapHist->count() : $req->rowsPerPage, ['*'], 'page', $req->page);

        $custom = collect(['filter' => $req->filter]);

        return $custom->merge($hasil);
    }

    public function fixEmptyMapping($scrapDoc, $job = '')
    {
        $cekData = scrapHist::where('ID_TRANS', base64_decode($scrapDoc));

        if (!empty($job)) {
            $cekData->where('DOC_NO', $job);
        }

        $hasil = $cekData->get();

        $hasilRet = [];
        foreach ($hasil as $key => $value) {
            if (strpos($value['DSGN_MAP_ID'], '-') === false) {
                $checkDesign = processMapDet::where('id', $value['DSGN_MAP_ID'])->count();
                if ($checkDesign === 0) {
                    $changeID = DB::connection('PSI_RPCUST')->table('v_cims_check_list')->where('PDPP_WONO', $value['DOC_NO'])->where('RPDSGN_PROCD', $value['SCR_PROCD'])->first();

                    $updateData = scrapHist::where('id', $value['id'])->update([
                        'DSGN_MAP_ID' => $value['DOC_NO'] . '-' . $changeID->MBO2_SEQNO
                    ]);

                    $hasilRet[] = $changeID;
                }
            }
        }

        return $hasilRet;
    }

    public function fifoPartScrap($item, $qty, $idTrans = '')
    {
        set_time_limit(3600);
        $data = DB::select("SELECT * FROM PSI_RPCUST.dbo.f_fifo_scrap_item('" . base64_decode($item) . "', " . $qty . ", '" . $idTrans . "')");

        $data = array_map(function ($value) {
            return (array) $value;
        }, $data);

        $exbc = [
            'status' => true,
            'message' => "EX-BC found on all item !",
            'data' => []
        ];

        $ith = [
            'status' => true,
            'message' => "Transaction WMS Found !",
            'data' => []
        ];

        $stockPrice = [
            'status' => true,
            'message' => 'All item found !',
            'data' => $data,
        ];

        if (count($data) > 0) {
            $checkEmptyPrice = array_values(array_filter($data, function ($f) {
                return $f['item_tot_price'] == 0;
            }));

            $checkEmptyExBC = array_values(array_filter($data, function ($f) {
                return $f['item_exbc_qty'] == 0;
            }));

            $checkEmptyITH = array_values(array_filter($data, function ($f) {
                return $f['item_ith_qty'] == 0;
            }));

            if (count($checkEmptyPrice) > 0) {
                $stockPrice = [
                    'status' => false,
                    'message' => "Some item doesn't get price !",
                    'data' => $checkEmptyPrice
                ];
            }

            if (count($checkEmptyExBC) > 0) {
                $exbc = [
                    'status' => false,
                    'message' => "Some item doesn't get EX-BC !",
                    'data' => $checkEmptyExBC
                ];
            }

            if (count($checkEmptyITH) > 0) {
                $ith = [
                    'status' => false,
                    'message' => "Some item doesn't submited to ITH !",
                    'data' => $checkEmptyITH
                ];
            }
        } else {
            $stockPrice = [
                'status' => false,
                'data' => [],
                'message' => 'No item found !'
            ];

            $ith = [
                'status' => false,
                'message' => "Transaction WMS not checked because item not found !",
                'data' => []
            ];

            $exbc = [
                'status' => false,
                'message' => "EX-BC not checked because item not found !",
                'data' => []
            ];
        }

        return [
            'stockPrice' => $stockPrice,
            'exbc' => $exbc,
            'ith' => $ith,
            'allData' => $data
        ];
    }

    public function confirmScrap($id, $isPPC = false, $date = '')
    {
        $parseID = base64_decode($id);


        $update = scrapHist::where('ID_TRANS', $parseID)->update(!$isPPC
            ? [
                'IS_CONFIRMED' => empty($date) ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s', strtotime($date)),
            ] : [
                'IS_CONFIRMED_PPC' => empty($date) ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s', strtotime($date))
            ]);

        if (!$isPPC) {
            $MasterScrap = scrapHist::where('ID_TRANS', $parseID)->get();
            // Fix Ex BC

            foreach ($MasterScrap as $key => $valueMaster) {
                if (empty($valueMaster->DATE_OUT) || $valueMaster->DATE_OUT === '1970-01-01' || $valueMaster->DATE_OUT === '1900-01-01') {
                    scrapHist::where('id', $valueMaster->id)->update([
                        'DATE_OUT' => date('Y-m-d', strtotime($date))
                    ]);
                }

                $hasilDet = scrapHistDet::where('SCR_HIST_ID', $valueMaster->id)
                    ->join('PSI_WMS.dbo.MITM_TBL', 'SCR_ITMCD', 'MITM_ITMCD')
                    ->where('MITM_MODEL', 0)
                    ->where('SCR_OTHER', 0)
                    ->get();

                // return $hasilDet;
                $hasilDetailForPost = [];
                foreach ($hasilDet as $keyScrDet => $valueScrDet) {
                    $hasilDetailForPost[] = [
                        'item_code' => $valueScrDet['SCR_ITMCD'],
                        'item_lot' => $valueScrDet['SCR_LOTNO'],
                        'item_qty' => $valueScrDet['SCR_QTY'],
                        'item_price' => $valueScrDet['SCR_ITMPRC'],
                        'id_det' => $valueScrDet['id'],
                        'item_scr_id' => $valueMaster['ID_TRANS'],
                        'item_scr_date' => $valueMaster['DATE_OUT'],
                        'item_sisa_qty' => $valueScrDet['SCR_QTY'],
                        'type' => 'exbc_fifo_confirm'
                    ];
                }

                // $paramPost = new Request([
                //     'exbc' => $hasilDetailForPost
                // ]);

                // Fix EX-BC
                // $this->fixScrap($paramPost);

                scrapFixForDisposeQueue::dispatch(
                    'ith',
                    '',
                    '',
                    $valueMaster->id,
                    ''
                )->onQueue('fix_scrap_ith');
            }
        }
        return [
            'status' => true,
            'message' => "This scrap ID is confirmed !",
            'data' => $update,
            'param' => $parseID
        ];
    }

    public function fixScrap(Request $req)
    {
        // return $req;
        // Fix the price
        $updateDetail = [];
        if ($req->has('scrapPrice')) {
            foreach ($req->scrapPrice as $key => $value) {
                $updateDetail[] = scrapFixForDisposeQueue::dispatch(
                    'scrapPrice',
                    $value['item_code'],
                    $value['item_lot'],
                    '',
                    $value['id_det']
                )->onQueue('fix_scrap_price');
            }
        }

        // Fix ExBC
        $updateBC = [];
        if ($req->has('exbc')) {
            foreach ($req->exbc as $keyExBC => $valueExBC) {
                $cekRCV = MutasiStok::where('RCV_ITMCD', $valueExBC['item_code'])
                    ->leftjoin('RCVSCN_TBL', function ($j) {
                        $j->on('RCV_DONO', 'RCVSCN_DONO');
                        $j->on('RCV_ITMCD', 'RCVSCN_ITMCD');
                    });
                //->where('RCVSCN_SAVED', 1);


                // return $cekRCV->first();
                if ($valueExBC['item_lot']) {
                    $cekRCV->where('RCVSCN_LOTNO', $valueExBC['item_lot']);
                }
                if (!empty($cekRCV->first())) {
                    $updateBC['batchInsert'] = stockExBCInsert::dispatch(
                        $valueExBC['item_scr_id'],
                        $valueExBC['id_det'],
                        trim($valueExBC['item_code']),
                        $valueExBC['item_scr_date'],
                        $valueExBC['item_lot'],
                        $valueExBC['item_sisa_qty'],
                        27,
                        false,
                        true,
                        isset($valueExBC['type']) || !empty($valueExBC['type']) ? $valueExBC['type'] : '',
                        true
                    )->onQueue('exBCQueue');

                    // $updateBC['cancel'] = $this->cancelEXBC($valueExBC['item_scr_id'], $valueExBC['id_det']);
                    // $updateBC['submit'] = $this->sendingParamBCStock(
                    //     trim($valueExBC['item_code']),
                    //     $valueExBC['item_scr_date'],
                    //     $valueExBC['item_lot'],
                    //     $valueExBC['item_sisa_qty'],
                    //     $valueExBC['item_scr_id'],
                    //     27,
                    //     $valueExBC['id_det'],
                    //     false
                    // );

                    // $item_num, $date_out = '', $lot = null, $qty = 0, $doc = null, $bc = null, $loc = '', $isArray = true
                } else {
                    $updateBC[] = $cekRCV->first();
                }
            }
        }

        // Fix ITH
        $checkITH = [];
        if ($req->has('ith')) {
            foreach ($req->ith as $keyITH => $valueITH) {
                // $checkITH[] = $this->resubmitITH(base64_encode($valueITH['id_mstr']));
                $updateDetail[] = scrapFixForDisposeQueue::dispatch(
                    'ith',
                    '',
                    '',
                    $valueITH['id_mstr'],
                    ''
                )->onQueue('fix_scrap_ith');
            }
        }

        return [
            'fixPrice' => $updateDetail,
            'fixExBC' => $updateBC,
            'fixITH' => $checkITH
        ];
    }

    public function fixFailedEXBC(Request $req)
    {
        $selectData = DB::connection('PSI_RPCUST')->table('v_failed_scrap_exbc')
            ->where('ID_TRANS', $req->id)
            ->whereNull('tot_exbc')
            ->get()
            ->toArray();

        // return $selectData;
        $hasil = [];
        foreach ($selectData as $key => $value) {
            $hasil[] = stockExBCInsert::dispatch(
                $value->ID_TRANS,
                $value->id,
                trim($value->SCR_ITMCD),
                $value->IS_CONFIRMED,
                $value->SCR_LOTNO,
                $value->SCR_QTY,
                27,
                false,
                true,
                '',
            )->onQueue('exBCQueue');
        }

        return $hasil;
    }

    public function getDataForDispose(Request $req)
    {
        $data = DB::connection('PSI_RPCUST')->table('v_scrap_report_dispose');

        if (($req->has('f_date') && $req->has('l_date')) && ($req->f_date && $req->l_date)) {
            $data->whereBetween('IS_CONFIRMED', [date('Y-m-d', strtotime($req->f_date)), date('Y-m-d', strtotime($req->l_date))]);
        }

        if ($req->has('scrapID') && !empty($req->scrapID)) {
            $data->where('ID_TRANS', $req->scrapID);
        }

        if ($req->has('item_type')) {
            if ($req->item_type == 'all') {
                $data->whereIn('MDL_FLAG', [0, 1, 6]);
            } else {
                $data->where('MDL_FLAG', $req->item_type);
            }
        }

        $hasil = $data->orderBy('ID_TRANS')->get();

        // return $hasil;
        $hasilFinal = [];
        foreach (json_decode(json_encode($hasil), true) as $key => $value) {
            // $getITH = ITHMaster::select(DB::raw('COALESCE(SUM(ITH_QTY), 0) AS ITH_QTY'))
            //     ->where('ITH_DOC', $value['ID_TRANS'])
            //     ->whereIn('ITH_FORM', ['INC-SCR-FG', 'INC-SCR-RM'])
            //     ->first();
            $hasilFinal[] = [
                'ID_TRANS' => $value['ID_TRANS'],
                'MENU_ID' => $value['MENU_ID'],
                'detSum' => $value['det_sum'],
                'totalScrap' => $value['REAL_QTY'],
                'exBCQty' => (int) $value['exbc_sum'],
                'totalSubmitedItem' => $value['det_count'],
                'itemPriceSetCountQty' => $value['price_set_det_sum'],
                // 'totITH' => (int) $getITH->ITH_QTY,
                'totITH' => (int) $value['ith_sum'],
                'discExBC' => $value['det_sum'] == $value['exbc_sum'],
                'discPrice' => $value['price_set_det_sum'] == $value['det_count'],
                // 'discITH' => $value['REAL_QTY'] == $getITH->ITH_QTY,
                'discITH' => $value['REAL_QTY'] == $value['ith_sum'],
                'dispose' => $value['disp_sum'],
                'item_type' => $value['TYPE_ITEM'],
                'username' => $value['MSTEMP_FNM'] . (empty($value['MSTEMP_LNM']) ? null : ' ' . $value['MSTEMP_LNM'])
            ];
        }

        return $hasilFinal;
    }

    public function fixBeforeDispose(Request $req)
    {
        $data = [];
        // All selected data
        foreach ($req->data as $key => $value) {
            $getIDTransaction = scrapHist::where('ID_TRANS', $value['ID_TRANS'])->get();
            // if ($req->has('fix') && in_array('exbc', $req->fix)) {
            //     $this->cancelEXBC($value['ID_TRANS']);
            // }
            // return $getIDTransaction;
            // Spliting by id of transaction
            foreach ($getIDTransaction as $keyScrMstr => $valueScrMstr) {
                if ($req->has('fix') && (in_array('price', $req->fix) || in_array('priceAll', $req->fix))) {

                    if (in_array('priceAll', $req->fix)) {
                        scrapHistDet::where('SCR_HIST_ID', $valueScrMstr->id)
                            ->update([
                                'SCR_ITMPRC' => 0
                            ]);
                    }

                    $hasilDet = scrapHistDet::where('SCR_HIST_ID', $valueScrMstr->id)
                        ->where('SCR_OTHER', 0)
                        ->where('SCR_ITMPRC', 0)
                        ->get();

                    $hasilDetailForPost = [];
                    foreach ($hasilDet as $keyScrDet => $valueScrDet) {
                        // scrapHistDet::where('id', $loc)->update([
                        //     'SCR_FIXFLG' => 1
                        // ]);

                        $hasilDetailForPost[] = [
                            'item_code' => $valueScrDet['SCR_ITMCD'],
                            'item_lot' => $valueScrDet['SCR_LOTNO'],
                            'item_qty' => $valueScrDet['SCR_QTY'],
                            'item_price' => $valueScrDet['SCR_ITMPRC'],
                            'id_det' => $valueScrDet['id'],
                        ];
                    }

                    $paramPost = new Request([
                        'scrapPrice' => $hasilDetailForPost
                    ]);

                    $data[$value['ID_TRANS']][$keyScrMstr] = $this->fixScrap($paramPost);
                    $data[$value['ID_TRANS']][$keyScrMstr]['DOC_NO'] = $valueScrMstr->DOC_NO;
                    $data[$value['ID_TRANS']][$keyScrMstr]['SCR_DET'] = $hasilDetailForPost;
                    $data[$value['ID_TRANS']][$keyScrMstr]['DATA_POST'] = $hasilDet;
                }

                if ($req->has('fix') && (in_array('exbc', $req->fix) || in_array('exbcAll', $req->fix))) {
                    if (in_array('exbcAll', $req->fix)) {
                        $this->cancelEXBC($value['ID_TRANS']);

                        $hasilDetHead = scrapHistDet::select(
                            DB::raw('RPSCRAP_HIST_DET.id as id_det'),
                            DB::raw('RPSCRAP_HIST_DET.*'),
                            DB::raw('RPSCRAP_HIST.*')
                        )
                            ->where('SCR_HIST_ID', $valueScrMstr->id)
                            ->join('PSI_WMS.dbo.MITM_TBL', 'SCR_ITMCD', 'MITM_ITMCD')
                            ->join('RPSCRAP_HIST', 'SCR_HIST_ID', 'RPSCRAP_HIST.id');
                    } else {
                        $hasilDetHead = scrapHistDet::select(
                            DB::raw('RPSCRAP_HIST_DET.id as id_det'),
                            DB::raw('RPSCRAP_HIST_DET.*'),
                            DB::raw('RPSCRAP_HIST.*'),
                            DB::raw('rb.RPSTOCK_LOC'),
                            DB::raw('rb.RPSTOCK_QTY')
                        )
                            ->where('SCR_HIST_ID', $valueScrMstr->id)
                            ->join('PSI_WMS.dbo.MITM_TBL', 'SCR_ITMCD', 'MITM_ITMCD')
                            ->join('RPSCRAP_HIST', 'SCR_HIST_ID', 'RPSCRAP_HIST.id')
                            ->leftjoin(DB::raw('(
                                SELECT rbb.RPSTOCK_LOC, rbb.RPSTOCK_QTY, rbb.deleted_at as RPSTOCK_delstock FROM RPSAL_BCSTOCK rbb
                                WHERE rbb.deleted_at is null
                            ) rb'), 'RPSCRAP_HIST_DET.id', 'rb.RPSTOCK_LOC')
                            ->whereNull('rb.RPSTOCK_LOC');
                    }

                    $hasilDet = $hasilDetHead
                        ->where('SCR_OTHER', 0)
                        ->get();

                    // return $hasilDet;

                    $hasilDetailForPost = [];
                    foreach ($hasilDet as $keyScrDet => $valueScrDet) {
                        $hasilDetailForPost[] = [
                            'item_code' => $valueScrDet['SCR_ITMCD'],
                            'item_lot' => $valueScrDet['MENU_ID'] == 'fg_return' ? '' : $valueScrDet['SCR_LOTNO'],
                            'item_qty' => $valueScrDet['SCR_QTY'],
                            'item_price' => $valueScrDet['SCR_ITMPRC'],
                            'id_det' => $valueScrDet['id_det'],
                            'item_scr_id' => $value['ID_TRANS'],
                            'item_scr_date' => $valueScrMstr['DATE_OUT'],
                            'item_sisa_qty' => $valueScrDet['SCR_QTY'],
                        ];
                    }
                    // return $hasilDetailForPost;

                    $paramPost = new Request([
                        'exbc' => $hasilDetailForPost
                    ]);

                    $data[$value['ID_TRANS']][$keyScrMstr] = $this->fixScrap($paramPost);
                    $data[$value['ID_TRANS']][$keyScrMstr]['DOC_NO'] = $valueScrMstr->DOC_NO;
                    $data[$value['ID_TRANS']][$keyScrMstr]['SCR_DET'] = $hasilDetailForPost;
                }

                if ($req->has('fix') && (in_array('ith', $req->fix) || in_array('ithAll', $req->fix))) {
                    // $hasilDetailForPost = [];

                    $listID = scrapHist::where('ID_TRANS', $value['ID_TRANS'])->withTrashed()->get();
                    $deleted = [];

                    if (in_array('ithAll', $req->fix)) {
                        foreach ($listID as $keyID => $valueID) {
                            $deleted[] = ITHMaster::where('ITH_REMARK', DB::raw('CAST(' . $valueID['id'] . ' AS varchar(100))'))->where('ITH_USRID', 'SCR_RPT')->delete();
                        }
                    }

                    $data[$value['ID_TRANS']][$keyScrMstr]['TEST'] = scrapFixForDisposeQueue::dispatch(
                        'ith',
                        '',
                        '',
                        $valueScrMstr->id,
                        ''
                    )->onQueue('fix_scrap_ith');

                    $data[$value['ID_TRANS']][$keyScrMstr]['DOC_NO'] = $valueScrMstr->DOC_NO;
                    $data[$value['ID_TRANS']][$keyScrMstr]['ID'] = $valueScrMstr->id;
                    $data[$value['ID_TRANS']][$keyScrMstr]['ITH_DEL'] = $deleted;
                }
            }
        }

        return $data;
    }

    public function submitDispose(Request $req)
    {
        $data = [];
        // All selected data
        $cek = scrapHist::whereBetween('DISPOSED_DATE', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59'])->orderBy('DISPOSED_DOC', 'desc')->withTrashed()->first();
        $idDispose = 'DISP/' . date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->DISPOSED_DOC, -3) + 1));

        if ($req->has('items') && count($req->items) > 0) {
            foreach ($req->items as $keyItem => $valueItem) {
                $dataCek = scrapHist::where('ID_TRANS', $valueItem['ID_TRANS'])
                    ->where('ITEMNUM', $valueItem['ITEMNUM'])
                    ->orderBy('id')
                    ->get();

                // return $dataCek;

                $totalItem = 0;
                foreach ($dataCek as $keyFifo => $valueFifo) {

                    $cekTotal = $valueFifo->QTY - ($valueItem['QTY'] - $totalItem);
                    if ($cekTotal < 0) {
                        scrapTransDet::create([
                            'SH_ID' => $valueFifo->id,
                            'STD_QTY' => $valueFifo->QTY * -1,
                            'STD_TYPE' => 'OUT-DISPOSE',
                        ]);

                        $totalItem = $totalItem + $valueFifo->QTY;

                        scrapHist::where('id', $valueFifo->id)->update([
                            'DISPOSED_DOC' => $idDispose,
                            'DISPOSED_DATE' => date('Y-m-d H:i:s'),
                        ]);
                    } else {
                        scrapHist::where('id', $valueFifo->id)->update([
                            'DISPOSED_DOC' => $idDispose,
                            'DISPOSED_DATE' => date('Y-m-d H:i:s'),
                        ]);

                        scrapTransDet::create([
                            'SH_ID' => $valueFifo->id,
                            'STD_QTY' => $totalItem > 0 ? $cekTotal * -1 : $valueItem['QTY'] * -1,
                            'STD_TYPE' => 'OUT-DISPOSE',
                        ]);

                        break;
                    }
                }
            }
        } else {
            foreach ($req->data as $key => $value) {
                $dataCek = scrapHist::where('ID_TRANS', $value['ID_TRANS'])->get();
                $data[] = $dataCek;

                scrapHist::where('ID_TRANS', $value['ID_TRANS'])->update([
                    'DISPOSED_DOC' => $idDispose,
                    'DISPOSED_DATE' => date('Y-m-d H:i:s'),
                ]);
            }
        }

        $disposeHist = dispHist::create([
            'RPDISP_ID' => $idDispose,
            'RPDISP_UNAME' => $req->user,
            'RPDISP_LDT' => $req->l_disp,
            'RPDISP_FDT' => $req->f_disp,
            'RPDISP_REMARK' => $req->remark,
            'RPDISP_DEPT' => $req->dept,
        ]);

        $cekModel = scrapHist::select('MDL_FLAG')->where('DISPOSED_DOC', $disposeHist->RPDISP_ID)->groupBy('MDL_FLAG')->get()->pluck('MDL_FLAG');

        return [
            'status' => true,
            'data' => $disposeHist,
            'model' => $cekModel
        ];
    }

    public function disposeReport(Request $req)
    {
        set_time_limit(3600);
        ini_set('memory_limit', '4096M');
        $hasil = [
            'FG' => [],
            'MATERIAL' => []
        ];

        $return = [
            'FG' => '',
            'MATERIAL' => ''
        ];

        $data = DB::connection('PSI_RPCUST')->table('v_dispose_print')
            ->where('DISPOSED_DOC', $req->data)
            ->where('MDL_FLAG', $req->model == 'fg' ? '1' : '0')
            ->orderBy('ID_TRANS')
            ->orderBy('DOC_NO')
            ->orderBy('id')
            ->orderBy('ITEMNUM')
            ->orderBy('rhd_id')
            ->orderBy('rb_id')
            ->get()
            ->toArray();

        $data = array_map(function ($value) {
            return (array) $value;
        }, $data);

        // return $data;
        $keyFG = $keyMtr = 0;
        foreach (json_decode(json_encode($data), true) as $key => $value) {
            if ($req->model == 'fg') {
                $hasil['FG'][$keyFG] = $value;

                $keyFG++;
            } else {
                $hasil['MATERIAL'][$keyMtr] = $value;

                $keyMtr++;
            }
        }

        if ($req->has('met') && $req->met === 'data') {
            return $hasil;
        }

        $getIDTrans = [];
        foreach (json_decode(json_encode($data), true) as $keyIDTrns => $valueIDTrns) {
            $getIDTrans[$valueIDTrns['ID_TRANS']] = $valueIDTrns['ID_TRANS'];
        }

        if (count($hasil['FG']) > 0) {
            $fileName = 'DISPOSE_FG' . date('Ymd') . '_' . date('his') . '.xlsx';
            $datanya = array_values($hasil['FG']);
            Excel::store(new scrapDisposeExport($datanya, $datanya[0]['DISPOSED_DOC'], 1, array_values($getIDTrans)), $fileName, 'public');

            $return['FG'] = 'public/storage/' . $fileName;
        }

        if (count($hasil['MATERIAL']) > 0) {
            $fileName = 'DISPOSE_MATERIAL' . date('Ymd') . '_' . date('his') . '.xlsx';
            $datanya = array_values($hasil['MATERIAL']);

            // return $datanya;
            Excel::store(new scrapDisposeExport($datanya, $datanya[0]['DISPOSED_DOC'], 0, array_values($getIDTrans)), $fileName, 'public');

            $return['MATERIAL'] = 'public/storage/' . $fileName;
        }

        return $return;
    }

    public function disposeReportApproval($id, $model = 'all')
    {
        $data = DB::connection('PSI_RPCUST')->table('v_dispose_print_approval')
            ->where('RPDISP_ID', $id);

        if ($model !== 'all') {
            $data->where('SCRH_TYPMDL', $model);
        }

        return $data->get();
    }

    public function listAllDisposeDoc()
    {
        $dataDispose = dispHist::select(
            'RPDISP_ID',
            DB::raw("CONCAT(DATENAME(month, RPDISP_FDT), ' ', YEAR(RPDISP_FDT), ' - ',DATENAME(month, RPDISP_LDT), ' ', YEAR(RPDISP_LDT)) AS PERIOD"),
            'RPDISP_FDT',
            'RPDISP_LDT',
            DB::raw('MAX(RPDISP_HIST.created_at) AS created_at'),
            DB::raw('td.TOT_QT_MAT AS NG_PROD'),
            DB::raw('td.TOT_QT_MDL AS NG_CUST'),
            'RPDISP_ISCONF',
            'RPDISP_ISDISPDN',
            'SCAN_QTY',
            DB::raw("
                CASE WHEN RPDISP_ISDISPDN = 1
                    THEN (td.TOT_QT_MAT + td.TOT_QT_MDL) - SCAN_QTY
                    ELSE (td.TOT_QT_MAT + td.TOT_QT_MDL)
                END AS TOT_OUTS_QTY
            "),
            'EXBC_STOCK',
        )->leftjoin('PSI_RPCUST.dbo.RPSCRAP_HIST', 'DISPOSED_DOC', 'RPDISP_ID')
            ->leftJoin(DB::raw('PSI_RPCUST.dbo.v_dispose_list td'), function ($j) {
                $j->on('td.DISPOSED_DOC', 'RPDISP_ID');
            })
            ->groupBy([
                'RPDISP_ID',
                'RPDISP_FDT',
                'RPDISP_LDT',
                'EXBC_STOCK',
                // 'RPDISP_HIST.created_at',
                'RPDISP_ISCONF',
                'td.TOT_QT_MAT',
                'td.TOT_QT_MDL',
                'RPDISP_ISDISPDN',
                'SCAN_QTY'
            ])
            ->orderBy('RPDISP_ID', 'desc')
            // ->orderBy('MAX(RPDISP_HIST.created_at)', 'desc')
            ->get()
        ;

        $dataDispose->transform(function ($value) {
            $value->DET = [];
            $value->loading = false;
            $value->filter = '';
            return $value;
        });

        return $dataDispose;
    }

    public function listDetailData(Request $request, $idDisp)
    {
        $data = DB::connection('PSI_RPCUST')->table('v_dispose_list_det')->where('DISPOSED_DOC', base64_decode($idDisp));

        if ($request->has('pagination')) {
            if (!empty($request->filter)) {
                $data->where('ITEMNUM_TD', 'like', $request->filter . '%');
            }

            return (clone $data)->paginate(($request->pagination['rowsPerPage'] === 0 ? (clone $data)->count() : $request->pagination['rowsPerPage']), ['*'], 'page', $request->pagination['page']);
        } else {
            return (clone $data)->get();
        }
    }

    public function reprintDispose($idDispose, $model = 'all', $export = 'pdf')
    {
        $report = $this->disposeReportApproval(base64_decode($idDispose), $model);

        $getTotalAmount = $getTotalQty = 0;
        foreach ($report as $key => $value) {
            $getTotalAmount += $value->SCRD_TOTAMNT;
            $getTotalQty += $value->SCRH_QTY;
        }

        $dataHasil = [
            'data' => $report,
            'totQty' => $getTotalQty,
            'totAmount' => $getTotalAmount,
            'header' => dispHist::where('RPDISP_ID', base64_decode($idDispose))
                ->leftjoin('PSI_WMS.dbo.MSTEMP_TBL', 'MSTEMP_ID', 'RPDISP_UNAME')
                ->first()
        ];

        if ($export === 'pdf') {
            $pdf = SnappyPdf::loadview('EXPORT.disposePrint', $dataHasil);
            return $pdf->download('dispose_approval.pdf');

            // $pdf = SnappyPDF::loadview('EXPORT.scrapSign', [
            //     'data' => $data,
            //     'processmstr' => processMstr::get()->toArray(),
            //     'location' => $cekFoundScrapLocation,
            //     'foundLoc' => array_values($cekFoundAt),
            //     'time' => $this->rutime($ru, $rustart, "utime")
            // ]);
            // return $pdf->download('scrap_report.pdf');
        } elseif ($export === 'html') {
            return view('EXPORT.disposePrint', $dataHasil);
        }

        // if (empty($typePrint)) {
        //     // return view('EXPORT.scrapSign', [
        //     //     'data' => $data,
        //     //     'processmstr' => processMstr::get()->toArray(),
        //     //     'location' => $cekFoundScrapLocation,
        //     //     'foundLoc' => array_values($cekFoundAt),
        //     //     'time' => $this->rutime($ru, $rustart, "utime")
        //     // ]);
        //     $pdf = SnappyPDF::loadview('EXPORT.disposePrint', $dataHasil);
        //     return $pdf->download('scrap_report.pdf');
        // } else {
        //     if (!$excel) {
        //         $pdf = SnappyPDF::setOrientation('landscape')->loadview('EXPORT.detailScrap', [
        //             'data' => $data,
        //             'processmstr' => processMstr::get()->toArray()
        //         ]);
        //         return $pdf->download('component_scrap_report.pdf');
        //     } else {
        //         return Excel::download(new scrapReportCompFrontExport($data), 'scrapReportComponentExport.xlsx');

        //         return 'public/storage/scrapReportComponentExport.xlsx';
        //     }
        // }
        // return $report;
    }

    public function reprintDisposeByDoc($idDispose)
    {
        $dataDispose = scrapHist::select('ID_TRANS')->where('ID_TRANS', base64_decode($idDispose))->groupBy('ID_TRANS')->get();

        $IDData = [];
        foreach ($dataDispose as $key => $value) {
            $IDData[] = $value['ID_TRANS'];
        }

        $req = new Request([
            'data' => $IDData
        ]);

        $report = $this->disposeReport($req);

        return $report;
    }

    public function disposeCancel($idDispose, $item = null)
    {
        $dataDisposeHead = scrapHist::where('DISPOSED_DOC', base64_decode($idDispose));

        if (!empty($item)) {
            $dataDisposeHead->where('ITEMNUM', base64_decode($item));
        }

        $dataDispose = $dataDisposeHead->get();

        $updateScrap = scrapHist::where('DISPOSED_DOC', base64_decode($idDispose));

        if (!empty($item)) {
            $updateScrap->where('ITEMNUM', base64_decode($item));
        }

        $updateScrap->update([
            'DISPOSED_DOC' => '',
            'DISPOSED_DATE' => null
        ]);

        $histTrans = scrapTransDet::whereIn('SH_ID', $dataDispose->pluck('id'))->delete();

        $cekDisposeRemain = scrapHist::where('DISPOSED_DOC', base64_decode($idDispose));

        if (!empty($item)) {
            $cekDisposeRemain->where('ITEMNUM', base64_decode($item));
        }

        if (count($cekDisposeRemain->get()) === 0) {
            $deleteHeader = dispHist::where('RPDISP_ID', base64_decode($idDispose))->delete();
        }

        return [
            'update_scrap_hist' => $dataDispose,
            'delete_header' => $deleteHeader,
            'trans' => $histTrans
        ];
    }

    public function listFailedComp(Request $req, $param)
    {
        $listSelectedIDTrans = [];
        foreach ($req->data as $key => $value) {
            $listSelectedIDTrans[] = "'" . $value['ID_TRANS'] . "'";
        }

        $cek = DB::connection('PSI_RPCUST')
            ->table('v_scrap_comp_failed')
            ->whereRaw('SCRAP_ID in (' . implode(',', $listSelectedIDTrans) . ')');

        if (empty($param)) {
            return [
                'status' => false,
                'message' => 'param must filled !',
                'data' => []
            ];
        }

        $hasil = [];
        if ($param === 'price') {
            $hasil = (clone $cek)->where('SCRAP_ITMPRC', 0);
        } elseif ($param === 'exbc') {
            $hasil = (clone $cek)->whereRaw('SCRAP_EXBCQT <> SCRAP_ITMQT');
        } elseif ($param === 'ith') {
            $hasil = (clone $cek)->where('SCRAP_ITHQTY', 0);
            // $cek = DB::table('v_ith_tblc')
            //     ->select()
            //     ->whereIn('ITH_DOC', $listSelectedIDScrap)
            //     ->get();
        }

        return [
            'status' => count($hasil->get()) > 0,
            'message' => 'Data found !',
            'data' => $hasil->get(),
            'param' => (clone $cek)->where('SCRAP_EXBCQT', 0)->toSql()
        ];
    }

    public function confirmDispose($uname, $id, $dn)
    {
        $idDisp = base64_decode($id);
        $dnDisp = base64_decode($dn);
        $getIDTransaction = scrapHist::where('DISPOSED_DOC', $idDisp)->get();

        foreach ($getIDTransaction as $key => $value) {
            scrapFixForDisposeQueue::dispatch(
                'dispose',
                '',
                '',
                $value->id,
                ''
            )->onQueue('fix_scrap_ith');
        }

        $cekDisposeDN = scrapDNList::where('RDL_PICK_ID', $idDisp)->get();

        if (!empty($cekDisposeDN)) {
            foreach ($cekDisposeDN as $key => $value) {
                $cekScan = scrapDNScan::where('RDL_ID', $value->id)->where('RDS_ISSAVED', 1)->get();
                foreach ($cekScan as $keyScan => $valueScan) {
                    if (!empty($cekScan)) {
                        scrapFixForDisposeQueue::dispatch(
                            'dispose',
                            '',
                            '',
                            $valueScan->id,
                            '',
                            true
                        )->onQueue('fix_scrap_ith');
                    }
                }
            }
        }

        dispHist::where('RPDISP_ID', $idDisp)->update([
            'RPDISP_UNAME_CONF' => $uname,
            'RPDISP_DN' => $dnDisp,
            'RPDISP_ISCONF' => date('Y-m-d H:i:s')
        ]);

        return [
            'status' => true,
            'message' => 'Dispose ID ' . $idDisp . ' approval has been sent !'
        ];
    }

    public function cancelApproval($id)
    {
        $idDisp = base64_decode($id);

        dispHist::where('RPDISP_ID', $idDisp)->update([
            'RPDISP_UNAME_CONF' => '',
            'RPDISP_DN' => '',
            'RPDISP_ISCONF' => NULL
        ]);

        // $dataScrap = scrapHist::where('DISPOSED_DOC', $idDisp)->get()->pluck('ID_TRANS');

        // foreach ($dataScrap as $key => $value) {
        //     // ITHMaster::where('ITH_DOC', $idDisp)->whereIn('ITH_FORM', ['OUT-DISPOSE', 'OUT-SCR-DISPOSE-DN', 'INC-SCR-DISPOSE-DN'])->delete();
        // }

        ITHMaster::where('ITH_DOC', $idDisp)->whereIn('ITH_FORM', ['OUT-DISPOSE', 'OUT-DISPOSE-DN', 'OUT-SCR-DISPOSE-DN', 'INC-SCR-DISPOSE-DN'])->delete();

        return [
            'status' => true,
            'message' => 'Dispose successfully canceled !'
        ];
    }

    public function updateQtyDispose(Request $request)
    {
        $cekScrap = scrapHist::where('ID_TRANS', $request->idScrap)
            ->where('DISPOSED_DOC', $request->idDisp)
            ->where('ITEMNUM', $request->item)
            ->get();

        $listID = scrapTransDet::whereIn('SH_ID', $cekScrap->pluck('id'))->get();
        foreach ($listID as $keyTrans => $valueTrans) {
            scrapTransDet::create([
                'SH_ID' => $valueTrans->SH_ID,
                'STD_QTY' => $valueTrans->STD_QTY * -1,
                'STD_TYPE' => 'INC-REV-DISPOSE',
            ]);
        }

        $totalItem = 0;
        foreach ($cekScrap as $keyFifo => $valueFifo) {
            $cekTotal = $valueFifo->QTY - ($request->qty - $totalItem);
            if ($cekTotal < 0) {
                scrapTransDet::create([
                    'SH_ID' => $valueFifo->id,
                    'STD_QTY' => $valueFifo->QTY * -1,
                    'STD_TYPE' => 'OUT-DISPOSE',
                ]);

                $totalItem = $totalItem + $valueFifo->QTY;

                scrapHist::where('id', $valueFifo->id)->update([
                    'DISPOSED_DOC' => $request->idDisp,
                    'DISPOSED_DATE' => date('Y-m-d H:i:s'),
                ]);
            } else {
                scrapHist::where('id', $valueFifo->id)->update([
                    'DISPOSED_DOC' => $request->idDisp,
                    'DISPOSED_DATE' => date('Y-m-d H:i:s'),
                ]);

                scrapTransDet::create([
                    'SH_ID' => $valueFifo->id,
                    'STD_QTY' => $totalItem > 0 ? $cekTotal * -1 : $request->qty * -1,
                    'STD_TYPE' => 'OUT-DISPOSE',
                ]);

                break;
            }
        }
    }

    public function saveDNDispose(Request $request)
    {
        $dataDNSplit = [];
        foreach ($request->data as $keyDebitTo => $valueDebitToSplit) {
            $dataDNSplit[$valueDebitToSplit['debit_to']][] = $valueDebitToSplit;
        }

        // return $dataDNSplit;

        $data = [];
        foreach ($dataDNSplit as $keyDebitTo => $valueDebitTo) {
            foreach ($valueDebitTo as $keyDebitToSplit => $valueDebitToSplit) {
                $cek = scrapDNList::whereBetween('created_at', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59'])->orderBy('RDL_PICK_ID', 'desc')->first();
                $idDisposeDN = 'DNDISP/' . date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->RDL_PICK_ID, -3) + 1));

                if ($request->has('id') && !empty($request->id)) {
                    $idDisposeDN = $request->id;
                }

                foreach ($request->data as $key => $value) {
                    $data[] = scrapDNList::updateOrCreate([
                        'RDL_PICK_ID' => $idDisposeDN,
                        'RDL_ITMCD' => $value['item_code'],
                    ], [
                        'RDL_PICK_ID' => $idDisposeDN,
                        'RDL_ITMCD' => $value['item_code'],
                        'RDL_QTY' => $value['part_of_qty'],
                        'RDL_SUPCD' => $value['supplier'],
                        'RDL_DEBTO' => $valueDebitToSplit['debit_to'],
                        'RDL_BG' => $request->bg,
                    ]);
                }

                dispHist::create([
                    'RPDISP_ID' => $idDisposeDN,
                    'RPDISP_FDT' => date('Y-m-01'),
                    'RPDISP_LDT' => date('Y-m-t'),
                    'RPDISP_UNAME' => $request->userid,
                    'RPDISP_REMARK' => 'DISPOSE DN ' . date('d M Y h:i:s'),
                    'RPDISP_DEPT' => 'PPIC',
                    'RPDISP_ISDISPDN' => 1,
                ]);
            }
        }

        return [
            'status' => true,
            'data' => $data,
            'message' => 'Picking List Dispose DN created !'
        ];
    }

    public function getDataPickingList(Request $request, $id = '', $model = '0')
    {
        $select = [
            'rdl2.RDL_PICK_ID',
            'rdl2.RDL_DEBTO',
            'rdl2.RDL_BG',
            'mtt.MCUS_CUSNM',
            DB::raw('COALESCE(SUM(RDL_QTY),0) AS RDL_QTY'),
            DB::raw('(
                SELECT COALESCE(SUM(RDS_QTY), 0)
                FROM RPSCRAP_DN_SCAN rds
                INNER JOIN RPSCRAP_DN_LIST rdl ON rds.RDL_ID = rdl.id
                WHERE rdl.RDL_PICK_ID = rdl2.RDL_PICK_ID
                AND rds.RDS_ISSAVED = 0
            ) as RDS_QTY'),
            DB::raw('(
                SELECT COALESCE(SUM(RDS_QTY), 0)
                FROM RPSCRAP_DN_SCAN rds
                INNER JOIN RPSCRAP_DN_LIST rdl ON rds.RDL_ID = rdl.id
                WHERE rdl.RDL_PICK_ID = rdl2.RDL_PICK_ID
                AND rds.RDS_ISSAVED = 1
            ) as RDS_QTY_SAVED')
        ];

        if (!empty($id)) {
            $select = array_merge($select, [
                'rdl2.id',
                'rdl2.RDL_ITMCD',
                'mt.MITM_MODEL',
                'rdl2.RDL_BG',
                'mtt.MCUS_CUSNM',
                DB::raw('(
                SELECT COALESCE(SUM(RDS_QTY), 0)
                FROM RPSCRAP_DN_SCAN rds
                INNER JOIN RPSCRAP_DN_LIST rdl ON rds.RDL_ID = rdl.id
                WHERE rdl.RDL_PICK_ID = rdl2.RDL_PICK_ID
                AND rds.RDS_ISSAVED = 0
                AND rds.RDL_ID = rdl2.id
            ) as RDS_QTY'),
                DB::raw('(
                SELECT COALESCE(SUM(RDS_QTY), 0)
                FROM RPSCRAP_DN_SCAN rds
                INNER JOIN RPSCRAP_DN_LIST rdl ON rds.RDL_ID = rdl.id
                WHERE rdl.RDL_PICK_ID = rdl2.RDL_PICK_ID
                AND rds.RDS_ISSAVED = 1
                AND rds.RDL_ID = rdl2.id
            ) as RDS_QTY_SAVED')
            ]);
        }

        $getData = scrapDNList::from('RPSCRAP_DN_LIST as rdl2')->select($select);

        if (!empty($id)) {
            $getData
                ->where('rdl2.RDL_PICK_ID', base64_decode($id))
                ->join(DB::raw('PSI_WMS.dbo.MITM_TBL mt'), 'MITM_ITMCD', 'rdl2.RDL_ITMCD')
                ->leftjoin(DB::raw('PSI_WMS.dbo.MCUS_TBL mtt'), 'mtt.MCUS_CUSCD', 'rdl2.RDL_DEBTO')
                ->groupby(
                    'rdl2.RDL_PICK_ID',
                    'rdl2.RDL_ITMCD',
                    'mt.MITM_MODEL',
                    'rdl2.id'
                );

            if ($model === '2') {
                $getData->whereIn('mt.MITM_MODEL', ['0', '1']);
            } else {
                $getData->where('mt.MITM_MODEL', $model);
            }

        } else {
            $getData
                ->leftjoin(DB::raw('PSI_WMS.dbo.MCUS_TBL mtt'), 'mtt.MCUS_CUSCD', 'rdl2.RDL_DEBTO')
                ->groupby(
                    'rdl2.RDL_PICK_ID',
                    'rdl2.RDL_BG',
                    'rdl2.RDL_DEBTO',
                    'mtt.MCUS_CUSNM',
                );
        }

        if ($request->has('pagination')) {
            return (clone $getData)->orderBy('RDL_PICK_ID', 'desc')->paginate(($request->pagination['rowsPerPage'] === 0 ? (clone $getData)->count() : $request->pagination['rowsPerPage']), ['*'], 'page', $request->pagination['page']);
        } else {
            return (clone $getData)->orderBy('RDL_PICK_ID', 'desc')->get();
        }
    }

    public function scanScrapDNSave(Request $request)
    {
        $query = "
            SET NOCOUNT ON;
            EXEC PSI_RPCUST.dbo.sp_save_scan_disp_dn
            @picklist_id = '" . $request->picklist_id . "',
            @item_num = '" . $request->itemnum . "',
            @lotOrJob = '" . $request->lot . "',
            @qty = '" . $request->qty . "',
        ";

        if ($request->has("ref") && !empty($request->ref)) {
            $query .= "@ref = '" . $request->ref . "',";
        }

        $query .= "@saved = 0";

        $submit = DB::connection('PSI_RPCUST')->select($query);
        return $submit;
    }

    public function scanScrapDNSaveAll($id, $user, $model)
    {
        $query = "
            SET NOCOUNT ON;
            EXEC PSI_RPCUST.dbo.sp_save_scanAll_disp_dn
            @picklist_id = '" . base64_decode($id) . "',
            @username = '" . base64_decode($user) . "',
            @issaved = " . ($model == 0 ? 1 : 0) . "
        ";

        $submit = DB::connection('PSI_RPCUST')->select($query);

        $submit = array_map(function ($value) {
            return (array) $value;
        }, $submit);

        // return $submit;

        if ($model == 1) {
            $hasil = [];
            foreach ($submit as $key => $value) {
                if ($value['ITM_MODEL'] == 1) {
                    $getListCalculation = DB::connection('PSI_RPCUST')->select("
                        SET NOCOUNT ON;
                        EXEC PSI_RPCUST.dbo.sp_scrap_fg_breakdown
                        @job = '" . $value['RDS_LOTJOB'] . "',
                        @ref = '" . $value['RDS_REF'] . "',
                        @qty = " . $value['RDS_QTY'] . "
                    ");

                    $getListCalculation = array_map(function ($valueDe) {
                        return (array) $valueDe;
                    }, $getListCalculation);

                    foreach ($getListCalculation as $keySerd => $valueSerd) {
                        $exBCSubmit = DB::connection('PSI_RPCUST')->select("
                            SET NOCOUNT ON;
                            exec PSI_RPCUST.dbo.sp_calculation_exbc
                            @item_num='" . $valueSerd['FGMAT_ITMCD'] . "',
                            @qty=" . $valueSerd['FGMAT_QTY'] . ",
                            @doc_out = '" . base64_decode($id) . "',
                            @loc = '" . $value['RDS_ID'] . "',
                            @is_saved=1,
                            @is_result=1
                        ");

                        $exBCSubmit = array_map(function ($valueDe2) {
                            return (array) $valueDe2;
                        }, $exBCSubmit);

                        foreach ($exBCSubmit as $keyDetExBC => $valueDetExBC) {
                            scrapHistDet::create([
                                'SCR_HIST_ID' => $value['RDS_ID'],
                                'SCR_PSN' => $valueDetExBC['BC_AJU'],
                                'SCR_CAT' => $valueDetExBC['BC_NUM'],
                                'SCR_LINE' => '',
                                'SCR_FR' => '',
                                'SCR_MC' => $valueSerd['FGMAT_MC'],
                                'SCR_MCZ' => $valueSerd['FGMAT_MCZ'],
                                'SCR_ITMCD' => $valueDetExBC['BC_ITEM'],
                                'SCR_QTPER' => $valueSerd['FGMAT_QTPER'],
                                'SCR_QTY' => $valueSerd['FGMAT_QTY'],
                                'SCR_LOTNO' => $valueSerd['FGMAT_LOT'],
                                'SCR_ITMPRC' => $valueDetExBC['RCV_PRPRC'],
                                'SCR_PROCD' => $valueSerd['FGMAT_PROCESS'],
                                'SCR_IS_DISPDN' => 1,
                            ]);

                            $hasil[] = [
                                'BC_NOAJU' => $valueDetExBC['BC_AJU'],
                                'BC_NODAFTAR' => $valueDetExBC['BC_NUM'],
                                'BC_PRPRC' => $valueDetExBC['RCV_PRPRC'],
                                'BC_QTY' => $valueDetExBC['BC_QTY'],
                                'BC_TGLDAFTAR' => $valueDetExBC['BC_DATE'],
                                'ITM_MODEL' => $valueDetExBC['BC_ITEM'],
                                'RDL_ITMCD' => $value['RDL_ITMCD'],
                                'RDS_ID' => $value['RDS_ID'],
                                'RDS_ITMCD' => $value['RDS_ITMCD'],
                                'RDS_LOTJOB' => $value['RDS_LOTJOB'],
                                'RDS_QTY' => $value['RDS_QTY'],
                                'RDS_REF' => $value['RDS_REF'],
                            ];
                        }
                    }

                    scrapDNScan::where('id', $value['RDS_ID'])->update([
                        'RDS_ISSAVED' => 1
                    ]);
                } else {
                    $hasil[] = $value;
                }
            }

            return $hasil;
        } else {
            return $submit;
        }
    }

    public function printDisposeDN($id, $model)
    {
        $data = scrapDNList::where('RDL_PICK_ID', base64_decode($id))
            ->where('MITM_MODEL', $model)
            ->join('PSI_WMS.dbo.MITM_TBL', 'MITM_ITMCD', 'RDL_ITMCD')
            ->get();

        $hasil = [
            'RDL_PICK_ID' => $data[0]['RDL_PICK_ID'],
        ];

        foreach ($data as $key => $value) {
            // $hasil[$value['MITM_MODEL']]['RDL_PICK_ID'] = $value['RDL_PICK_ID'];
            $hasil[$value['MITM_MODEL'] == 0 ? 'MATERIAL' : 'FINGOOD'][] = $value;
        }

        // return $hasil;

        if (count($data) === 0) {
            return response([
                'stock' => [
                    'No data found for ' . $model == 0 ? 'material' : 'finish good'
                ]
            ], 422);
        }

        $pdf = SnappyPdf::loadview('EXPORT.disposeDNPickingPrint', ['data' => $hasil]);

        return $pdf->download('dispose_dn_list.pdf');
    }

    public function deleteDisposeList($id)
    {
        $data = scrapDNList::where('RDL_PICK_ID', base64_decode($id))->get();

        $hasil = [];
        foreach ($data as $key => $value) {
            $hasil[$key] = scrapDNScan::where('RDL_ID', $value->id)->delete();
        }

        dispHist::where('RPDISP_ID', base64_decode($id))->delete();
        $hapusMaster = scrapDNList::where('RDL_PICK_ID', base64_decode($id))->delete();
        return [
            'status' => true,
            'message' => 'Data berhasil di hapus !!',
            'data' => [
                'master' => $hapusMaster,
                'det' => $hasil
            ]
        ];
    }

    public function cancelSaveDisposeDNList($id)
    {
        $data = scrapDNList::where('RDL_PICK_ID', base64_decode($id))->get();
        // DetailStock::where('RPSTOCK_REMARK', base64_decode($id))->delete();
        $this->cancelEXBC(base64_decode($id));
        // dispHist::where('RPDISP_ID', base64_decode($id))->delete();

        $hasil = [];
        foreach ($data as $key => $value) {
            $dataRDS = scrapDNScan::where('RDL_ID', $value['id'])->get();

            ITHMaster::where('ITH_REMARK', (string) $value['id'])
                ->where('ITH_USRID', 'SCR_RPT')
                ->delete();
            foreach ($dataRDS as $key => $valueRDS) {
                scrapHistDet::where('SCR_HIST_ID', $valueRDS['id'])->delete();
                $hasil[$key] = scrapDNScan::where('id', $valueRDS['id'])->update([
                    'RDS_ISSAVED' => 0
                ]);
            }
        }
        return [
            'status' => true,
            'message' => 'Data berhasil di hapus !!',
            'data' => [
                'det' => $hasil
            ]
        ];
    }

    public function onUpdateData(Request $request)
    {
        $hasil = [];
        foreach ($request->data as $key => $value) {
            if (isset($value['IS_DELETE'])) {
                scrapDNList::where('id', $value['id'])->delete();
            } else {
                if (isset($value['id'])) {
                    scrapDNList::updateOrCreate([
                        'RDL_PICK_ID' => $value['RDL_PICK_ID'],
                        'id' => $value['id'],
                    ], [
                        'RDL_PICK_ID' => $value['RDL_PICK_ID'],
                        'RDL_ITMCD' => $value['RDL_ITMCD'],
                        'RDL_QTY' => $value['RDL_QTY'],
                    ]);
                } else {
                    scrapDNList::create([
                        'RDL_PICK_ID' => $value['RDL_PICK_ID'],
                        'RDL_ITMCD' => $value['RDL_ITMCD'],
                        'RDL_QTY' => $value['RDL_QTY'],
                    ]);
                }
            }
        }

        return [
            'status' => true,
            'message' => 'Data Updated !',
            'data' => $request->data
        ];
    }

    public function viewScanData(Request $request)
    {
        $getData = scrapDNScan::where('RDL_ID', $request->scan_id)
            ->join('PSI_WMS.dbo.MITM_TBL', 'MITM_ITMCD', 'RDS_ITMCD');

        if ($request->has('pagination')) {
            return (clone $getData)->paginate(($request->pagination['rowsPerPage'] === 0 ? (clone $getData)->count() : $request->pagination['rowsPerPage']), ['*'], 'page', $request->pagination['page']);
        } else {
            return (clone $getData)->get();
        }
    }

    public function deleteScanData($id, $idScan = '')
    {
        $del = scrapDNScan::where('RDL_ID', $id);

        if (!empty($idScan)) {
            $hasil = (clone $del)->where('id', $idScan)->delete();
        } else {
            $hasil = (clone $del)->delete();
        }

        return ['status' => true, 'message' => 'Data deleted', 'data' => $hasil];
    }

    public function findLoss(Request $request, $usePSN = false): array
    {
        $whereItem = $request->has('item') ? "AND PPSN2_SUBPN IN ('" . implode("','", $request->item) . "')" : null;
        $getData = DB::connection('PSI_RPCUST')->select(DB::raw("SELECT
            RTRIM(PPSN2_SUBPN) AS PPSN2_SUBPN,
            RTRIM(MITM_ITMD1) AS MITM_ITMD1,
            " . ($usePSN ? 'RTRIM(PPSN1_PSNNO) as PPSN1_PSNNO, RTRIM(PPSN2_PROCD) as PPSN2_PROCD, RTRIM(PPSN2_MC) as PPSN2_MC, RTRIM(PPSN2_MCZ) as PPSN2_MCZ, PPSN2_QTPER,' : null) . "
            CAST(SUM(PPSN2_REQQT) AS INT) AS TOT_REQQT,
            CAST(SUM(KIT_QTY) AS INT) AS TOT_KITQTY,
            CAST(SUM(KIT_QTY) - SUM(PPSN2_REQQT) AS INT) as LOG_RET_QTY,
            CAST(SUM(RET_QTY) AS INT) AS ACT_RETQTY,
            (
                SELECT
                    COALESCE(SUM(SERD_QTY), 0)
                FROM PSI_WMS.dbo.SERD_TBL
                WHERE SERD_ITMCD = PPSN2_SUBPN
                --AND SERD_LUPDT BETWEEN '" . date('Y-m-d', strtotime($request->fdate)) . "' AND '" . date('Y-m-d', strtotime($request->ldate)) . "'
            ) AS TOT_SERD,
            CAST(COALESCE(SUM(SCR_QTY), 0) AS INT) AS TOT_SCR
        FROM v_scrap_lost_list
        WHERE RETSCN_CNFRMDT
            BETWEEN '" . date('Y-m-d', strtotime($request->fdate)) . "' AND '" . date('Y-m-d', strtotime($request->ldate)) . "'
        AND RET_QTY < (KIT_QTY - PPSN2_REQQT)
        AND PPSN1_BSGRP = '" . $request->bg . "'
        " . $whereItem . "
        GROUP BY PPSN2_SUBPN, MITM_ITMD1" . ($usePSN ? ', PPSN1_PSNNO, PPSN2_PROCD, PPSN2_MC, PPSN2_MCZ, PPSN2_QTPER' : null) . "
        HAVING CAST(SUM(KIT_QTY) - SUM(PPSN2_REQQT) AS INT) - (CAST(SUM(RET_QTY) AS INT) + CAST(COALESCE(SUM(SCR_QTY), 0) AS INT)) > 0
        "));

        $data = json_decode(json_encode($getData), true);

        if ($usePSN) {
            return $data;
        }

        return [
            'status' => true,
            'data' => $data,
            'next_id' => ''
        ];
    }

    public function SubmitLoss(Request $request): array
    {
        $hasil = [];

        $getItem = [];
        foreach ($request->data as $key => $value) {
            $getItem[] = $value['PPSN2_SUBPN'];
        }

        $getData = array_merge($request->all(), [
            'item' => $getItem
        ]);

        $data = $this->findLoss(new Request($getData), true);
        $cek = scrapHist::whereBetween('created_at', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59'])->orderBy('ID_TRANS', 'desc')->withTrashed()->first();
        $idScrap = date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->ID_TRANS, -3) + 1));
        foreach ($data as $key => $value) {
            scrapLossQueue::dispatch(
                $value,
                $request->username,
                $idScrap,
                'PPIC',
                $request->mainreason
            )->onQueue('lossScrap');
        }

        return [
            'status' => true,
            'data' => $hasil,
            'message' => 'Success added to part loss queue !'
        ];
    }

    public function exportResultLossCandidate(Request $request)
    {
        $data = $this->findLoss(new Request($request->all()));

        $fileName = 'LOSS_CANDIDATE_' . date('Ymd') . '_' . date('his') . '.xlsx';
        Excel::store(new scrapLossBefSubmitExport($data['data']), $fileName, 'public');

        return 'public/storage/' . $fileName;
        // scrapLossBefSubmitExport
    }

    public function listBG()
    {
        $data = DB::table('SRVMEGA.PSI_MEGAEMS.dbo.MBSG_TBL')->where('MBSG_BFSTK', '0')->get();

        $hasil = [];
        foreach ($data as $key => $value) {
            $hasil[] = [
                'value' => $value->MBSG_BSGRP,
                'label' => $value->MBSG_BSGRP . ' (' . trim($value->MBSG_DESC) . ')'
            ];
        }

        return $hasil;
    }

    public function listSupp()
    {
        $data = DB::table('XMSUP')->get();

        $hasil = [];
        foreach ($data as $key => $value) {
            $hasil[] = [
                'value' => trim($value->MSUP_SUPCD),
                'label' => trim($value->MSUP_SUPCD) . ' (' . trim($value->MSUP_SUPNM) . ')'
            ];
        }

        return $hasil;
    }

    public function listCust()
    {
        $data = DB::table('MCUS_TBL')->get();

        $hasil = [];
        foreach ($data as $key => $value) {
            $hasil[] = [
                'value' => trim($value->MCUS_CUSCD),
                'label' => trim($value->MCUS_CUSCD) . ' (' . trim($value->MCUS_CUSNM) . ')'
            ];
        }

        return $hasil;
    }

    public function uploadDisposeDNDraft(Request $req)
    {
        ini_set('max_execution_time', '12000');
        $nama_file = $req->file->hashName();

        // return $nama_file;
        $file = new File($req->file);
        $extNya = $req->file('file')->getClientOriginalExtension();

        $fileHash = str_replace('.' . $file->extension(), '', $file->hashName());
        $nama_file = $fileHash . '.' . $extNya;

        // return $extNya;
        $oriFileName = $req->file('file')->getClientOriginalName();

        if ($extNya == 'xls' || $extNya == 'xlsx') {
            $splitString = intval(preg_replace('/[^0-9]+/', '', $oriFileName), 10);

            $req->file->storeAs('/public/upload_dispose_draftdn/', $nama_file);

            if ($extNya == 'xls') {
                $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
                $writer = new Xlsx($spreadsheet);
                $nama_file = $fileHash . '.xlsx';
                $writer->save('/public/upload_dispose_draftdn/' . $nama_file);
            }
            $cek = scrapDNList::whereBetween('created_at', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59'])->orderBy('RDL_PICK_ID', 'desc')->first();
            $idDisposeDN = 'DNDISP/' . date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->RDL_PICK_ID, -3) + 1));

            $importer = new UploadDraftDispose($req->username, $idDisposeDN);

            Excel::import($importer, public_path('/storage/upload_dispose_draftdn/' . $nama_file));

            return 'Upload Sukses ' . $nama_file;
        } else {
            return "File name doesn't right! please check again !";
        }
    }

    public function fixExbcOnDraft(Request $req)
    {
        $data = DB::connection('PSI_RPCUST')->table('v_dispose_print')
            ->select(
                'id',
                'ID_TRANS',
                'ITEMNUM',
                'QTY',
                'CHECKSUMEXBC',
                DB::raw('QTY - CHECKSUMEXBC as TOTDISC')
            )
            ->where('DISPOSED_DOC', $req->data['RPDISP_ID'])
            ->where(DB::raw('QTY - CHECKSUMEXBC'), '>', 0)
            ->groupBy(
                'id',
                'ID_TRANS',
                'ITEMNUM',
                'QTY',
                'CHECKSUMEXBC'
            )
            ->get()
            ->toArray();

        $hasil = [];
        foreach ($data as $key => $value) {
            $hasil[] = [
                'item' => $value->ITEMNUM,
                'qty' => $value->QTY,
                'exbc' => DB::connection('PSI_RPCUST')->select("
                    SET NOCOUNT ON;
                    exec PSI_RPCUST.dbo.sp_calculation_exbc
                    @item_num='" . $value->ITEMNUM . "',
                    @qty=" . $value->TOTDISC . ",
                    @doc_out = '" . $req->data['RPDISP_ID'] . "',
                    @loc = '" . $value->id . "',
                    @is_saved=1,
                    @is_result=1
                ")
            ];
        }

        return [
            'status' => true,
            'message' => 'EX-BC fixed on draft',
            'data' => $hasil
        ];
    }
}
