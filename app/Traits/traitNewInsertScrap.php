<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

// Models
use App\Model\RPCUST\processMstr;
use App\Model\RPCUST\processMapDet;
use App\Model\RPCUST\scrapHist;
use App\Model\WMS\API\SERDTBL;
use App\Model\MASTER\ItemMaster;
use App\Model\WMS\API\RCVSCN;
use App\Model\MASTER\ITHMaster;
use App\Model\RPCUST\scrapHistDet;
use App\Model\WMS\REPORT\MutasiStok;
use App\Model\RPCUST\scrapHistDetPartOf;

// Jobs
use App\Jobs\scrapDetailInserts;
use App\Jobs\scrapDetailMaterial;
use App\Jobs\scrapMasterInsert;
use App\Model\WMS\API\RETFG;

trait traitNewInsertScrap
{
    public function __construct()
    {
        $cek = scrapHist::whereBetween('created_at', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59'])->orderBy('ID_TRANS', 'desc')->withTrashed()->first();

        $this->process = processMstr::get()->toArray();
        $this->idTransaction = date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->ID_TRANS, -3) + 1));
        $this->user = '';
        $this->dept = '';
        $this->mainreason = '';
        $this->type = false;
        $this->menu = '';
        $this->other = [];
        $this->responsible = '';
    }

    public function wipTransaction($data)
    {
        set_time_limit(3600);
        $this->menu = 'wip';
        $dataAddedProcess = [];

        // return $data;
        foreach ($data as $keyData => $valueData) {
            $checkMappingItem = $valueData['mapping_det'];
            // $dataAddedProcess[$valueData['assy_no']][$valueData['job_no']] = $this->storeDataFinishGood($checkMappingItem, $valueData, ['ARQA1', 'ARPRD1']);

            $dataAddedProcess[] = scrapMasterInsert::dispatch(
                $checkMappingItem,
                $valueData,
                ['ARQA1', 'ARPRD1'],
                $this->process,
                $this->idTransaction,
                $this->user,
                $this->dept,
                $this->mainreason,
                $this->type,
                $this->menu,
                $this->other,
            )->onQueue('scrapReportMaster');
        }

        // return $dataAddedProcess;

        $returnNotif = [];
        // foreach ($dataAddedProcess as $keyItem => $valueItem) {
        //     foreach ($valueItem as $keyJob => $valueJob) {
        //         foreach ($valueJob as $keyProc => $valueProc) {
        //             $returnNotif[] = [
        //                 'status' => $valueProc['status'],
        //                 'message' => "Item: " . ($keyItem) . ", " . $valueProc['message'],
        //                 'data' => $valueProc['data'],
        //             ];
        //         }
        //     }
        // }

        $returnNotif[] = [
            'status' => true,
            'message' => 'Scrap has been submited !, please wait all scrap submited until done.',
            'data' => $data,
        ];

        return $returnNotif;
    }

    public function pendingTransaction($data)
    {
        set_time_limit(3600);
        $this->menu = 'pending';
        $dataAddedProcess = [];
        foreach ($data as $keyData => $valueData) {
            if (!$valueData['is_material']) {
                $checkMappingItem = $valueData['mapping_det'];
                $dataAddedProcess[$valueData['assy_no']][$valueData['job_no']] = $this->storeDataFinishGood($checkMappingItem, $valueData, ['QAFG']);
            } else {
                $dataAddedProcess[trim($valueData['assy_no'])][$valueData['job_no']]['MATERIAL'] = $this->storeDataMaterial($valueData, ['ARWH0PD', 'QA'], true);
            }
        }

        // return $dataAddedProcess;

        logger(json_encode($dataAddedProcess));

        $returnNotif = [];
        foreach ($dataAddedProcess as $keyItem => $valueItem) {
            foreach ($valueItem as $keyJob => $valueJob) {
                foreach ($valueJob as $keyProc => $valueProc) {
                    $returnNotif[] = [
                        'status' => $valueProc['status'],
                        'message' => "Item: " . ($keyItem) . ", " . $valueProc['message'],
                        'data' => $valueProc['data'],
                    ];
                }
            }
        }

        return $returnNotif;
    }

    public function warehouseTransaction($data)
    {
        set_time_limit(3600);
        $this->menu = 'warehouse';
        $dataAddedProcess = [];
        foreach ($data as $keyData => $valueData) {
            if (!$valueData['is_material']) {
                $checkMappingItem = $valueData['mapping_det'];
                $dataAddedProcess[$valueData['assy_no']][$valueData['job_no']] = $this->storeDataFinishGood($checkMappingItem, $valueData, ['AFWH3', 'AWIP1']);
            } else {
                $dataAddedProcess[$valueData['assy_no']][$valueData['job_no']]['MATERIAL'] = /* $this->storeDataMaterial($valueData, ['ARWH1', 'ARWH2', 'NRWH2']); */ $this->storeDataMaterial($valueData, ['ARWH0PD']);
            }
        }

        $returnNotif = [];
        foreach ($dataAddedProcess as $keyItem => $valueItem) {
            foreach ($valueItem as $keyJob => $valueJob) {
                foreach ($valueJob as $keyProc => $valueProc) {
                    $returnNotif[] = [
                        'status' => $valueProc['status'],
                        'message' => "Item: " . ($keyItem) . ", " . $valueProc['message'],
                        'data' => $valueProc['data'],
                    ];
                }
            }
        }

        return $returnNotif;
    }

    public function FGReturnTransaction($data)
    {
        set_time_limit(3600);
        $this->menu = 'fg_return';
        $dataAddedProcess = [];
        foreach ($data as $keyData => $valueData) {
            // logger(json_encode($checkMappingItem));
            $dataAddedProcess[$valueData['assy_no']][$valueData['job_no']] = $this->storeDataFGReturn($valueData);
        }

        // return $dataAddedProcess;

        $returnNotif = [];
        foreach ($dataAddedProcess as $keyItem => $valueItem) {
            foreach ($valueItem as $keyJob => $valueJob) {
                foreach ($valueJob as $keyProc => $valueProc) {
                    $returnNotif[] = [
                        'status' => $valueProc['status'],
                        'message' => "Item: " . ($keyItem) . ", " . $valueProc['message'],
                        'data' => $valueProc['data'],
                    ];
                }
            }
        }

        return $returnNotif;
    }

    public function MCHEquipmentTransaction($data)
    {
        set_time_limit(3600);
        $this->menu = 'mchequip';
        $dataAddedProcess = [];
        foreach ($data as $keyData => $valueData) {
            // logger(json_encode($checkMappingItem));
            $dataAddedProcess[$valueData['assy_no']][$valueData['job_no']] = $this->storeDataMCHEquipment($valueData);
        }

        // return $dataAddedProcess;

        $returnNotif = [];
        foreach ($dataAddedProcess as $keyItem => $valueItem) {
            foreach ($valueItem as $keyJob => $valueJob) {
                foreach ($valueJob as $keyProc => $valueProc) {
                    $returnNotif[] = [
                        'status' => $valueProc['status'],
                        'message' => "Item: " . ($keyItem) . ", " . $valueProc['message'],
                        'data' => $valueProc['data'],
                    ];
                }
            }
        }

        return $returnNotif;
    }

    public function newMCHEquipmentTransaction($data)
    {
        $hasil = [];
        foreach ($data as $key => $value) {
            $insert = $this->insertMasterScrap(
                '',
                (int) $value['MCH'],
                false,
                $data['job_no'],
                $data['reason'],
                'MCH',
                1,
                $data['assy_no'],
                $data['date'],
                $data['ref_no'],
                true,
                '',
                $value['purc_date']
            );
        }
    }

    public function getMappingItem($item, $process = null)
    {
        $data = processMapDet::where('RPDSGN_ITEMCD', $item);

        if (!empty($process)) {
            $data->where('RPDSGN_PRO_ID', $process);
        }

        return $data->get()->toArray();
    }

    public function getRCVSCNData($assy, $lot = '')
    {

        $cekRCVSCN = DB::connection('PSI_RPCUST')->table('v_list_rcvscn')->select(
            'RCVSCN_DONO',
            'RCVSCN_ITMCD',
            'RCVSCN_LOTNO',
            // DB::raw('(SUM(RCVSCN_QTY)) - COALESCE(SUM(RPSCRAP_HIST_DET.SCR_QTY), 0) AS RCVSCN_QTY'),
            DB::raw('(SUM(RCVSCN_QTY)) AS RCVSCN_QTY'),
            DB::raw('COALESCE(SUM(RPSCRAP_HIST.QTY), 0) AS SCRAP_QTY'),
            DB::raw('COALESCE(SUM(RPSCRAP_HIST_DET.SCR_QTY), 0) AS SCRAP_DET_QTY'),
            DB::raw('COALESCE(SUM(SERD_QTY), 0) as calc_fg')
        )
            ->where('RCVSCN_ITMCD', trim($assy));

        if (!empty($lot)) {
            $cekRCVSCN->where('RCVSCN_LOTNO', $lot);
        }

        $cekRCVSCN = $cekRCVSCN
            ->where('RCVSCN_SAVED', 1)
            ->leftJoin('PSI_RPCUST.dbo.RPSCRAP_HIST', function ($j) {
                $j->on('ITEMNUM', '=', 'RCVSCN_ITMCD');
                $j->on('DOC_NO', '=', 'RCVSCN_DONO');
                $j->whereNull('RPSCRAP_HIST.deleted_at');
            })
            ->leftjoin('PSI_RPCUST.dbo.RPSCRAP_HIST_DET', function ($j) {
                $j->on('RPSCRAP_HIST.id', '=', 'RPSCRAP_HIST_DET.SCR_HIST_ID');
                $j->on('RCVSCN_LOTNO', '=', 'RPSCRAP_HIST_DET.SCR_LOTNO');
                $j->whereNull('RPSCRAP_HIST_DET.deleted_at');
            })
            ->leftJoin('PSI_WMS.dbo.SERD_TBL', function ($j) {
                $j->on('SERD_ITMCD', '=', 'RCVSCN_ITMCD');
                $j->on('SERD_LOTNO', '=', 'RPSCRAP_HIST_DET.SCR_LOTNO');
            })
            ->groupBy(
                'RCVSCN_DONO',
                'RCVSCN_ITMCD',
                'RCVSCN_LOTNO'
            )
            ->orderBy(DB::raw('min(RCVSCN_LUPDT)'), 'ASC')
            // ->havingRaw('SUM(RCVSCN_QTY) - COALESCE(SUM(RPSCRAP_HIST_DET.SCR_QTY), 0) > 0')
            // ->whereNotNull('RCVSCN_DONO')
            ->get()
            ->toArray();

        return $cekRCVSCN;
    }
    public function storeDataMaterial($data, $wh, $pend = false, $hasil = [])
    {

        $cekRCVSCN = $this->getRCVSCNData($data['assy_no'], $data['job_no']);

        if (empty($cekRCVSCN)) {
            logger('pending with lot is empty ' . $data['assy_no'] . ' & '.$data['job_no']);
            $cekRCVSCN = $this->getRCVSCNData($data['assy_no']);
        }

        // logger(json_encode($cekRCVSCN));

        $totalQtyRCVscn = 0;
        foreach ($cekRCVSCN as $keyTotal => $valueTotal) {
            // if (!empty($valueTotal->RCVSCN_DONO)) {
            //     $totalQtyRCVscn += $valueTotal->RCVSCN_QTY;
            // }

            $totalQtyRCVscn += $valueTotal->RCVSCN_QTY;
        }

        if ((int) $data['MATERIAL'] > $totalQtyRCVscn) {
            return [
                'status' => false,
                'message' => 'Stock ' . $data['job_no'] . ' on RCVSCN only ' . $totalQtyRCVscn . ', please check again.',
                'data' => $cekRCVSCN
            ];
        }

        $totalSubmited = 0;
        $listIDScrap = [];
        $returnMessage = [];
        $idTrans = '';
        foreach ($cekRCVSCN as $key => $valueRCVSCN) {
            $total = 0;
            if ((int) $valueRCVSCN->RCVSCN_QTY <= (int) ($data['MATERIAL'] - $totalSubmited)) {
                $total = (int) $valueRCVSCN->RCVSCN_QTY;
            } else {
                $total = (int) ($data['MATERIAL'] - $totalSubmited);
            }

            // if (($totalSubmited + $valueRCVSCN->RCVSCN_QTY) > (int) $data['MATERIAL']) {
            //     $total = (int) $valueRCVSCN->RCVSCN_QTY - ((int) $data['MATERIAL']- $totalSubmited);
            // }

            $totalSubmited += $total;

            logger(json_encode(['check insert pending', $data['assy_no'], $totalSubmited, $data['MATERIAL'], $total, $totalQtyRCVscn]));

            $cekRCVSCNDO = [];
            if (empty($valueRCVSCN->RCVSCN_DONO)) {
                $cekRCVSCNDO = $this->getRCVSCNData($data['assy_no']);
            }

            $cekDODataItem = array_values(array_filter($cekRCVSCNDO, function($f){
                return !empty($f->RCVSCN_DONO);
            }));

            $insert = $this->insertMasterScrap(
                '',
                $total,
                $this->type === 'pending' ? true : false,
                empty($valueRCVSCN->RCVSCN_DONO) ? $cekDODataItem[0]->RCVSCN_DONO : $valueRCVSCN->RCVSCN_DONO,
                $data['reason'],
                'MATERIAL',
                0,
                $data['assy_no'],
                $data['date'],
                $data['ref_no'],
                false
            );
            $idTrans = $insert->ID_TRANS;
            $IDHeader = $insert->id;
            // logger($this->other);

            if (count($cekRCVSCN) === 0) {
                scrapHist::where('id', $IDHeader)->update([
                    'failed_reason' => 'Item & LOT not found in Scan Receiving !! 1',
                    'IS_DELETE' => 1
                ]);

                scrapHist::where('id', $IDHeader)->delete();

                return [
                    'status' => false,
                    'message' => 'Item & LOT not found in Scan Receiving !! 1',
                    'data' => $cekRCVSCN
                ];
            } else {
                // $total = 0;
                // foreach ($cekRCVSCN as $key => $value) {
                //     $total += $value->RCVSCN_QTY;
                // }

                // if ($data['MATERIAL'] > $total) {
                // scrapHist::where('id', $IDHeader)->update([
                //     'failed_reason' => 'Stock scan receiving not enough its only ' . $total . ' !!',
                //     'IS_DELETE' => 1
                // ]);

                // scrapHist::where('id', $IDHeader)->delete();

                // return [
                //     'status' => false,
                //     'message' => 'Stock scan receiving not enough its only ' . $total . ' !!',
                //     'data' => $cekRCVSCN
                // ];
                // } else {

                // }

                $checkNotEmptyLot = array_values(array_filter($cekRCVSCN, function ($f) {
                    return !empty($f->RCVSCN_LOTNO);
                }));

                scrapDetailMaterial::dispatch(
                    $data['assy_no'],
                    $IDHeader,
                    $total,
                    'pending',
                    empty($data['date']) || $data['date'] === '1970-01-01' ? date('Y-m-d') : date('Y-m-d', strtotime(str_replace('/', '-', $data['date']))),
                    $this->idTransaction,
                    // $data['job_no'],
                    empty($valueRCVSCN->RCVSCN_DONO) ? $cekDODataItem[0]->RCVSCN_LOTNO : $valueRCVSCN->RCVSCN_LOTNO, // Lot No New
                    // count($checkNotEmptyLot) > 0
                    // ? $checkNotEmptyLot[0]->RCVSCN_LOTNO
                    // : $cekRCVSCN[0]->RCVSCN_LOTNO, // Lot No
                    empty($valueRCVSCN->RCVSCN_DONO) ? $cekDODataItem[0]->RCVSCN_DONO : $valueRCVSCN->RCVSCN_DONO, // DO New
                    // count($checkNotEmptyLot) > 0
                    // ? $checkNotEmptyLot[0]->RCVSCN_DONO
                    // : $cekRCVSCN[0]->RCVSCN_DONO, // DO
                    false,
                    $wh,
                    $pend,
                    $data['ref_no']
                )->onQueue('scrapReportMaterial');

                $listIDScrap[] = $IDHeader;

                if (($totalSubmited) >= (int) $data['MATERIAL']) {
                    $returnMessage =  [
                        'status' => true,
                        'message' => 'Material scrap submited !',
                        'data' => $valueRCVSCN
                    ];

                    break ;
                }
            }
        }


        if (count($this->other) > 0 && !empty($returnMessage)) {
            $cekScrapHistID = scrapHist::where('ID_TRANS', $idTrans)->get()->pluck('id');

            $cekOthersonDet = scrapHistDet::whereIn('SCR_HIST_ID', $cekScrapHistID)->where('SCR_OTHER', 1)->first();

            if (empty($cekOthersonDet)) {
                foreach ($this->other as $key => $val) {
                    scrapHistDet::create([
                        'SCR_HIST_ID' => $listIDScrap[count($listIDScrap) - 1],
                        'SCR_ITMCD' => $val['desc'],
                        'SCR_QTPER' => 1,
                        'SCR_QTY' => $val['value'],
                        'SCR_ITMPRC' => $val['price'],
                        'SCR_OTHER' => 1
                    ]);
                }
            }

            return $returnMessage;
        }
    }

    public function storeDataFinishGood($dataMapping, $data, $wh, $hasil = [], $seq = 1)
    {
        set_time_limit(3600);
        // logger($dataMapping);
        if (isset($dataMapping[$seq - 1])) {
            $nowData = $dataMapping[$seq - 1];
            $cekNowDataHWADD = $nowData['RPDSGN_PRO_ID'] == 'SMT-HWADD' ? 'SMT-HWADD' : $nowData['RPDSGN_PRO_ID'];
            logger($data);
            logger($dataMapping);
            logger($cekNowDataHWADD);
            $processNm = strpos($nowData['RPDSGN_PROCD'], 'SMT') !== false
                ? $nowData['RPDSGN_PROCD']
                : $cekNowDataHWADD;

            $insert = [];
            if ($data[$cekNowDataHWADD] > 0) {
                $insert = $this->insertMasterScrap(
                    isset($nowData['id'])
                    ? $nowData['id']
                    : $data['job_no'] . '-' . $seq,
                    $data[$cekNowDataHWADD],
                    $this->type === 'pending' ? true : false,
                    $data['job_no'],
                    $data['reason'],
                    $processNm,
                    1,
                    $data['assy_no'],
                    $data['date'],
                    $data['ref_no'],
                    false
                );

                $IDHeader = $insert->id;

                if (count($this->other) > 0) {
                    foreach ($this->other as $key => $val) {
                        scrapHistDet::create([
                            'SCR_HIST_ID' => $IDHeader,
                            'SCR_ITMCD' => $val['desc'],
                            'SCR_QTPER' => 1,
                            'SCR_QTY' => $val['value'],
                            'SCR_ITMPRC' => $val['price'],
                            'SCR_OTHER' => 1
                        ]);
                    }
                }

                $listProcess = [];

                $dataMapping = $this->defineMappingScrapArray($data['mapping_det']);
                foreach ($dataMapping as $keyProcess => $valueProcess) {
                    $listProcess[] = $valueProcess['RPDSGN_PROCD'];
                    if ($valueProcess['RPDSGN_PROCD'] === $processNm) {
                        break;
                    }
                }

                logger('cek proses start');
                logger(json_encode($listProcess));
                logger('cek proses end');

                $allProcess = [];
                foreach ($dataMapping as $keyProcess => $valueProcess) {
                    $allProcess[] = $valueProcess['RPDSGN_PROCD'];
                }

                if (count($allProcess) > 0 && $allProcess[0] === 'COMBINE') {
                    $listJOBCombine = DB::connection('PSI_RPCUST')->table('v_serd_tbl_opt')
                        ->select('SERD_JOB_REAL')
                        ->where('SERD_JOB', $data['job_no'])
                        ->groupBy('SERD_JOB_REAL')
                        ->get()
                        ->pluck('SERD_JOB_REAL')
                        ->all();

                    $dataByJoblist = [];
                    foreach ($listJOBCombine as $keyJob => $valueJob) {
                        foreach ($this->getSERDTable($valueJob) as $key => $value) {
                            $dataByJoblist[] = $value;
                        }
                    }
                } else {
                    $dataByJoblist = $this->getSERDTable($data['job_no']);
                }

                logger('cek joblist start');
                logger($dataByJoblist);
                logger('cek joblist end');

                if ($cekNowDataHWADD === 'COMBINE') {
                    $listJobnya = [];
                    foreach ($dataByJoblist as $key => $value) {
                        $listJobnya[$value['PPSN1_PROCD']] = $value['PPSN1_PROCD'];
                    }

                    $listProcess = array_values($listJobnya);
                }

                $cekSERD = $this->checkSERDTable(
                    $dataByJoblist,
                    $data['job_no'],
                    $cekNowDataHWADD === 'COMBINE' ? 'SMT-HW' : $cekNowDataHWADD,
                    (int) $data[$cekNowDataHWADD],
                    $listProcess,
                    $allProcess
                );
                // logger('cek serd start');
                logger(json_encode($cekSERD));
                // logger('cek serd end');
            } else {
                $cekSERD = [];
            }

            $cekSERDTBL = SERDTBL::select('SERD_PROCD')->where('SERD_JOB', $data['job_no'])->groupby('SERD_PROCD')->get()->toArray();

            // logger([count($dataMapping), count($cekSERDTBL)]);
            if ((count($dataMapping) === count($cekSERDTBL)) || (isset($dataMapping[0]['MBO2_PROCD']) && $dataMapping[0]['MBO2_PROCD'] === 'COMBINE')) {
                if (count($cekSERD) > 0) {
                    if ($data[$cekNowDataHWADD] > 0) {
                        // $findByProcess = $this->checkReverseProcess($cekSERD);
                        $findByProcess = $cekSERD;

                        $getAngkaKoma = array_filter($findByProcess, function ($f) {
                            return $this->is_decimal($f['PPSN2_QTPER']);
                        });

                        $cekQtyKoma = $this->cekAngkaKoma($getAngkaKoma, (int) $data['tot_qty']);

                        if (!$cekQtyKoma['status']) {
                            $hasil[$processNm] = [
                                'status' => false,
                                'message' => 'QTY will be decimal, please check it !!',
                                'data' => $cekQtyKoma
                            ];

                            if (!empty($IDHeader)) {
                                scrapHist::where('id', $IDHeader)->update([
                                    'failed_reason' => 'QTY will be decimal, please check it !!',
                                    'IS_DELETE' => 1
                                ]);

                                scrapHist::where('id', $IDHeader)->delete();
                            }
                        } else {
                            $cekItem = ItemMaster::where('MITM_ITMCD', $data['assy_no'])->first()->toArray();
                            // logger($wh);

                            scrapDetailInserts::dispatch(
                                $findByProcess,
                                $data[$cekNowDataHWADD],
                                $IDHeader,
                                $cekItem,
                                $this->idTransaction,
                                $data['job_no'],
                                0,
                                empty($data['date']) ? date('Y-m-d') : date('Y-m-d', strtotime(str_replace('/', '-', $data['date']))),
                                false,
                                $wh
                            )->onQueue('scrapReport');

                            $hasil[$processNm] = [
                                'status' => true,
                                'message' => 'Job found in calculation finish good !!',
                                'data' => $findByProcess,
                                'inserted_data' => $insert
                            ];
                        }

                        $seq = $seq + 1;
                        if (isset($dataMapping[$seq - 1])) {
                            return $this->storeDataFinishGood($dataMapping, $data, $wh, $hasil, $seq);
                        } else {
                            return $hasil;
                        }
                    }
                } else {
                    $cekNowDataHWADD = $nowData['RPDSGN_PRO_ID'] == 'SMT-HWADD' ? 'SMT-HWADD' : $nowData['RPDSGN_PRO_ID'];
                    if ($data[$cekNowDataHWADD] > 0) {
                        $hasil[$processNm] = [
                            'status' => false,
                            'message' => 'This job ' . $data['job_no'] . ' not found in calculate finish good, please calculate first !!',
                            'data' => $cekSERD
                        ];

                        scrapHist::where('id', $IDHeader)->update([
                            'failed_reason' => 'This job ' . $data['job_no'] . ' not found in calculate finish good, please calculate first !!',
                            'IS_DELETE' => 1
                        ]);

                        scrapHist::where('id', $IDHeader)->delete();
                    }

                    $seq = $seq + 1;
                    return $this->storeDataFinishGood($dataMapping, $data, $wh, $hasil, $seq);
                }
            } else {
                // logger([count($dataMapping), count($cekSERDTBL)]);
                // logger(json_encode($dataMapping));
                // logger(json_encode($cekSERDTBL));
                if ($data[$cekNowDataHWADD] > 0) {
                    $hasil[$processNm] = [
                        'status' => false,
                        'message' => 'This job ' . $data['job_no'] . ' calculate finish good is not complete, please wait the calculate is done !!',
                        'data' => $cekSERD,
                        'dataMapping' => $dataMapping,
                        'countMapping' => count($dataMapping),
                        'countSERD' => count($cekSERDTBL)
                    ];

                    scrapHist::where('id', $IDHeader)->update([
                        'failed_reason' => 'This job ' . $data['job_no'] . ' calculate finish good is not complete, please wait the calculate is done !!',
                        'IS_DELETE' => 1
                    ]);

                    scrapHist::where('id', $IDHeader)->delete();
                }

                $seq = $seq + 1;
                return $this->storeDataFinishGood($dataMapping, $data, $wh, $hasil, $seq);
            }
            // return $this->storeDataFinishGood($dataMapping, $data, $hasil, $wh);
        } else {
            return $hasil;
        }
    }

    public function storeDataFGReturn($data, $hasil = [])
    {
        $insert = $this->insertMasterScrap(
            '',
            (int) $data['RETURN'],
            false,
            $data['job_no'],
            $data['reason'],
            'RETURN',
            1,
            $data['assy_no'],
            $data['date'],
            $data['ref_no'],
            true
        );

        $IDHeader = $insert->id;

        if (count($this->other) > 0) {
            foreach ($this->other as $key => $val) {
                scrapHistDet::create([
                    'SCR_HIST_ID' => $IDHeader,
                    'SCR_ITMCD' => $val['desc'],
                    'SCR_QTPER' => 1,
                    'SCR_QTY' => $val['value'],
                    'SCR_ITMPRC' => $val['price'],
                    'SCR_OTHER' => 1
                ]);
            }
        }

        $cekITH = ITHMaster::where('ITH_SER', $data['ref_no'])->whereIn('ITH_WH', ['AFQART', 'AFQART2', 'NFWH4RT', 'AFWH3RT'])->first();

        $issue = $this->insertToITH(
            $cekITH->ITH_WH == 'NFWH4RT' || $cekITH->ITH_WH == 'AFWH3RT' ? $cekITH->ITH_ITMCD : trim($data['assy_no']),
            date('Y-m-d', strtotime($data['date'])) === '1900-01-01' || date('Y-m-d', strtotime($data['date'])) === '1970-01-01' || empty($data['date']) ? date('Y-m-d') : date('Y-m-d', strtotime(str_replace('/', '-', $data['date']))),
            'OUT-SCR-RTN',
            $data['job_no'],
            round((int) $data['RETURN']) * -1,
            $cekITH->ITH_WH,
            $data['ref_no'],
            $IDHeader,
            'SCR_RPT'
        );

        // Receive
        $receive = $this->insertToITH(
            $cekITH->ITH_WH == 'NFWH4RT' || $cekITH->ITH_WH == 'AFWH3RT' ? $cekITH->ITH_ITMCD : trim($data['assy_no']),
            date('Y-m-d', strtotime($data['date'])) === '1900-01-01' || date('Y-m-d', strtotime($data['date'])) === '1970-01-01' || empty($data['date']) ? date('Y-m-d') : date('Y-m-d', strtotime(str_replace('/', '-', $data['date']))),
            'INC-SCR-FG',
            $this->idTransaction,
            round((int) $data['RETURN']),
            'NFWH9SC',
            $data['ref_no'],
            $IDHeader,
            'SCR_RPT'
        );

        $hasil['RETURN'] = [
            'status' => true,
            'data' => [
                'fgreturn' => $insert,
                'material' => [],
            ],
            'message' => 'FG Return scrap submited !'
        ];

        if (trim($cekITH->ITH_WH) == 'NFWH4RT' || trim($cekITH->ITH_WH) == 'AFWH3RT') {
            $cekITH2 = ITHMaster::where('ITH_REMARK', $data['ref_no'])->whereIn('ITH_WH', ['AFQART', 'AFQART2', 'NFWH4RT', 'AFWH3RT'])->first();

            if (empty($cekITH2)) {
                $cekITH2 = ITHMaster::where('ITH_SER', $data['ref_no'])->whereIn('ITH_WH', ['AFQART', 'AFQART2', 'NFWH4RT', 'AFWH3RT'])->first();
            }

            $item = $cekITH2->ITH_ITMCD;
        } else {
            $item = $data['assy_no'];
        }

        $cekRcvScnForPrice = MutasiStok::where('RCV_INVNO', $data['job_no'])->where('RCV_ITMCD', $item)->first();

        // return [$cekRcvScnForPrice, $item, $data['job_no'], $cekITH->ITH_WH, $data['assy_no']];

        $insertDet = scrapHistDet::insertGetId([
            'SCR_HIST_ID' => $IDHeader,
            'SCR_PSN' => null,
            'SCR_CAT' => null,
            'SCR_LINE' => null,
            'SCR_FR' => null,
            'SCR_MC' => null,
            'SCR_MCZ' => null,
            'SCR_ITMCD' => $data['assy_no'],
            'SCR_QTPER' => 1,
            'SCR_QTY' => round((int) $data['RETURN']),
            'SCR_LOTNO' => $data['job_no'],
            'SCR_ITMPRC' => empty($cekRcvScnForPrice) ? 0 : $cekRcvScnForPrice['RCV_PRPRC']
        ]);

        if (isset($data['material_list']) && is_array($data['material_list']) && count($data['material_list']) > 0) {
            foreach ($data['material_list'] as $key => $value) {
                $cekRCVSCN = DB::connection('PSI_RPCUST')->table('v_list_rcvscn')->select(
                    'RCVSCN_DONO',
                    'RCVSCN_ITMCD',
                    'RCVSCN_LOTNO',
                    DB::raw('SUM(RCVSCN_QTY) - COALESCE(SUM(RPSCRAP_HIST.QTY), 0) AS RCVSCN_QTY'),
                    DB::raw('COALESCE(SUM(RPSCRAP_HIST.QTY), 0) AS SCRAP_QTY')
                )
                    ->where('RCVSCN_ITMCD', $value['SERRC_BOMPN'])
                    ->where('RCVSCN_LOTNO', $value['SERRC_LOTNO'])
                    ->where('RCVSCN_SAVED', 1)
                    ->leftJoin('PSI_RPCUST.dbo.RPSCRAP_HIST', function ($j) {
                        $j->on('ITEMNUM', '=', 'RCVSCN_ITMCD');
                        $j->on('DOC_NO', '=', 'RCVSCN_DONO');
                    })
                    ->groupBy(
                        'RCVSCN_DONO',
                        'RCVSCN_ITMCD',
                        'RCVSCN_LOTNO'
                    )
                    ->havingRaw('SUM(RCVSCN_QTY) - COALESCE(SUM(RPSCRAP_HIST.QTY), 0) > 0')
                    ->get()
                    ->toArray();

                if (count($cekRCVSCN) > 0) {
                    $datas = scrapDetailMaterial::dispatch(
                        $value['SERRC_BOMPN'],
                        $IDHeader,
                        $data['RETURN'],
                        'pending',
                        empty($data['date']) || $data['date'] === '1970-01-01' ? date('Y-m-d') : date('Y-m-d', strtotime(str_replace('/', '-', $data['date']))),
                        $this->idTransaction,
                        $value['SERRC_LOTNO'],
                        $cekRCVSCN[0]->RCVSCN_DONO,
                        false,
                        ['AFQART'],
                        false,
                        $data['ref_no']
                    )->onQueue('scrapReportMaterial');

                    $hasil['material'][$value['SERRC_BOMPN']] = [
                        'status' => true,
                        'message' => 'Material scrap submited !',
                        'data' => isset($datas) ? $datas : ''
                    ];
                } else {
                    $hasil['material'][$value['SERRC_BOMPN']] = [
                        'status' => false,
                        'message' => 'Item ' . $value['SERRC_BOMPN'] . ' or lot ' . $value['SERRC_LOTNO'] . ' not found !!',
                        'data' => []
                    ];
                }
            }

            scrapHist::where('id', $IDHeader)->update([
                'IS_DONE' => 1
            ]);

            return $hasil;
        } else {
            // $getDoc = RETFG::where('RETFG_DOCNO', $data['job_no'])->where();
            scrapHist::where('id', $IDHeader)->update([
                'IS_DONE' => 1
            ]);

            return $hasil;

            scrapHist::where('id', $IDHeader)->update([
                'failed_reason' => 'Material list not found !!',
                'IS_DELETE' => 1
            ]);

            scrapHist::where('id', $IDHeader)->delete();

            return [
                'RETURN' => [
                    'status' => false,
                    'message' => 'Material list not found !!',
                    'data' => []
                ]
            ];
        }
    }

    public function storeDataMCHEquipment($data, $hasil = [])
    {
        $insert = $this->insertMasterScrap(
            '',
            (int) $data['MCH'],
            false,
            $data['job_no'],
            $data['reason'],
            'MCH',
            6,
            $data['assy_no'],
            $data['date'],
            $data['ref_no'],
            true,
            '',
            $data['purc_date']
        );

        $IDHeader = $insert->id;

        if (count($this->other) > 0) {
            foreach ($this->other as $key => $val) {
                scrapHistDet::create([
                    'SCR_HIST_ID' => $IDHeader,
                    'SCR_ITMCD' => $val['desc'],
                    'SCR_QTPER' => 1,
                    'SCR_QTY' => $val['value'],
                    'SCR_ITMPRC' => $val['price'],
                    'SCR_OTHER' => 1
                ]);
            }
        }

        $hasil['RETURN'] = [
            'status' => true,
            'data' => [
                'fgreturn' => $insert,
                'material' => [],
            ],
            'message' => 'Machine and Equipment scrap submited !'
        ];

        $cekRcvScnForPrice = MutasiStok::where('RCV_PO', $data['job_no'])->where('RCV_ITMCD', $data['assy_no'])->first();

        $insertDet = scrapHistDet::insertGetId([
            'SCR_HIST_ID' => $IDHeader,
            'SCR_PSN' => null,
            'SCR_CAT' => null,
            'SCR_LINE' => null,
            'SCR_FR' => null,
            'SCR_MC' => null,
            'SCR_MCZ' => null,
            'SCR_ITMCD' => $data['assy_no'],
            'SCR_QTPER' => 1,
            'SCR_QTY' => round((int) $data['MCH']),
            'SCR_LOTNO' => $data['job_no'],
            'SCR_ITMPRC' => empty($cekRcvScnForPrice) ? 0 : $cekRcvScnForPrice['RCV_PRPRC']
        ]);

        if ($data['partState'] === 'part_of') {
            scrapHistDetPartOf::create([
                'SCRAP_HIST_ID' => $IDHeader,
                'PRTOF_ITMCD' => $data['assy_no_part'],
                'PRTOF_QTY' => $data['MCH_PART']
            ]);
        }

        scrapHist::where('id', $IDHeader)->update([
            'IS_DONE' => 1
        ]);

        return $hasil;
    }

    public function checkSERDTable($data, $job, $code, $qty, $procd = [], $allProcd = [])
    {
        if (!empty($procd) || count($procd) > 0) {
            $getData = array_values(array_filter($data, function ($f) use ($procd) {
                if (in_array('COMBINE', $procd)) {
                    return $f;
                }

                return in_array($f['PPSN1_PROCD'], $procd);
            }));

            $cekPCB = array_values(array_filter($getData, function ($f) use ($procd) {
                return trim($f['PPSN2_ITMCAT']) == 'PCB';
            }));


            if (count($cekPCB) === 0) {
                $getDataPCB = array_values(array_filter($data, function ($f) use ($allProcd) {
                    return trim($f['PPSN2_ITMCAT']) == 'PCB';
                }));
                // logger('cek pcb');
                // logger($getDataPCB);

                $getData = array_merge($getData, $getDataPCB);
            }
        }

        $dataFinal = [];
        foreach ($getData as $key => $value) {
            $dataFinal[] = array_merge($value, ['RPDSGN_CODE' => $code]);
        }

        $dataGrouping = [];
        $blackList = [];
        $groupingCount = 0;
        $blCount = 0;

        // logger('start check dataFinal');
        // logger($dataFinal);
        // logger('end check dataFinal');
        // logger('start check main sub');
        foreach ($dataFinal as $keyData => $valueData) {
            // Check Main Sub Part
            $getModel = explode('-', $job);
            // logger('check itemSub start for item: '.trim($valueData['PPSN2_SUBPN']));
            $cekItemSub = $this->checkItemSub(trim($valueData['PPSN2_SUBPN']), $getModel[count($getModel) - 1], $job);

            // logger('check itemSub end for item: '.trim($valueData['PPSN2_SUBPN']));
            if (count($cekItemSub) > 0) {
                $cekBlacklist = array_filter($blackList, function ($f) use ($valueData) {
                    return $f['PPSN2_SUBPN'] == $valueData['PPSN2_SUBPN'] &&
                        $f['SERD_LOTNO'] == $valueData['SERD_LOTNO'] &&
                        $f['PPSN1_PROCD'] == $valueData['PPSN1_PROCD'] &&
                        $f['SERD_FR'] == $valueData['SERD_FR'] &&
                        $f['SERD_MC'] == $valueData['SERD_MC'] &&
                        $f['SERD_MCZ'] == $valueData['SERD_MCZ'];
                });

                if (count($cekBlacklist) === 0) {
                    $dataGrouping[$valueData['PPSN2_SUBPN'] . '-' . $valueData['PPSN1_PROCD'] . '-' . $valueData['SERD_FR'] . '-' . $valueData['SERD_MC'] . '-' . $valueData['SERD_MCZ']][$groupingCount] = array_merge(
                        $valueData,
                        ['STATUS' => 'PUNYA SUB']
                    );
                    $groupingCount = $groupingCount + 1;

                    foreach ($cekItemSub as $keyUsedSub => $valueUsedSub) {
                        $hasilnya = array_filter($dataFinal, function ($f) use ($valueUsedSub, $valueData) {
                            return trim($f['PPSN2_SUBPN']) == trim($valueUsedSub->CMI_ITMSUB) &&
                                trim($f['PPSN1_PROCD']) == $valueData['PPSN1_PROCD'] &&
                                trim($f['SERD_FR']) == trim($valueData['SERD_FR']) &&
                                trim($f['SERD_MC']) == trim($valueData['SERD_MC']) &&
                                trim($f['SERD_MCZ']) == trim($valueData['SERD_MCZ']);
                        });

                        if (count($hasilnya) > 0) {
                            foreach ($hasilnya as $keySum => $valueSum) {
                                $cekBlacklist2 = array_filter($blackList, function ($f) use ($valueSum) {
                                    return $f['PPSN2_SUBPN'] == $valueSum['PPSN2_SUBPN'] &&
                                        $f['SERD_LOTNO'] == $valueSum['SERD_LOTNO'] &&
                                        $f['PPSN1_PROCD'] == $valueSum['PPSN1_PROCD'] &&
                                        $f['SERD_FR'] == $valueSum['SERD_FR'] &&
                                        $f['SERD_MC'] == $valueSum['SERD_MC'] &&
                                        $f['SERD_MCZ'] == $valueSum['SERD_MCZ'];
                                });

                                $blackList[$blCount] = $valueSum;

                                if (
                                    count($cekBlacklist2) === 0
                                ) {
                                    $dataGrouping[$valueData['PPSN2_SUBPN'] . '-' . $valueData['PPSN1_PROCD'] . '-' . $valueData['SERD_FR'] . '-' . $valueData['SERD_MC'] . '-' . $valueData['SERD_MCZ']][$groupingCount] = array_merge(
                                        $valueSum,
                                        ['STATUS' => 'PUNYA SUB & ADA DI SERD ITEM']
                                    );

                                    unset($dataGrouping[$valueSum['PPSN2_SUBPN'] . '-' . $valueData['PPSN1_PROCD'] . '-' . $valueData['SERD_FR'] . '-' . $valueData['SERD_MC'] . '-' . $valueData['SERD_MCZ']]);

                                    $groupingCount = $groupingCount + 1;
                                }

                                $blCount++;
                            }
                        }
                    }
                }
            } else {
                $cekBlacklist = array_filter($blackList, function ($f) use ($valueData) {
                    return $f['PPSN2_SUBPN'] == $valueData['PPSN2_SUBPN'] &&
                        $f['SERD_LOTNO'] == $valueData['SERD_LOTNO'] &&
                        $f['PPSN1_PROCD'] == $valueData['PPSN1_PROCD'] &&
                        $f['SERD_FR'] == $valueData['SERD_FR'] &&
                        $f['SERD_MC'] == $valueData['SERD_MC'] &&
                        $f['SERD_MCZ'] == $valueData['SERD_MCZ'];
                });

                if (count($cekBlacklist) === 0) {
                    $dataGrouping[$valueData['PPSN2_SUBPN'] . '-' . $valueData['PPSN1_PROCD'] . '-' . $valueData['SERD_FR'] . '-' . $valueData['SERD_MC'] . '-' . $valueData['SERD_MCZ']][$groupingCount] = array_merge(
                        $valueData,
                        ['STATUS' => 'TIDAK PUNYA SUB']
                    );
                }
            }

            $groupingCount++;
        }

        // logger('end check main sub');
        logger(json_encode($dataGrouping));

        // return $dataGrouping;

        $hasil = [];
        $countHasil = 0;

        // logger('start all FIFOSERDLOT');
        foreach ($dataGrouping as $keyGroup => $valueGroup) {
            // logger('start one FIFOSERDLOT key: '.$keyGroup);
            $fifoData = $this->FIFOSERDLOT(array_values($valueGroup), $qty);
            if ($fifoData && count($fifoData) > 0) {
                foreach ($fifoData as $keyFiTa => $valueFiTa) {
                    $hasil[$countHasil] = $valueFiTa;

                    $countHasil++;
                }
            }
            // logger('end one FIFOSERDLOT key: '.$keyGroup);
        }
        // logger('end all FIFOSERDLOT');

        // logger($hasil);

        return $hasil;
    }

    public function FIFOSERDLOT($data, $qty, $hasil = [], $totalQtyNeed = 0)
    {
        $now = current($data);
        if (!empty($now)) {
            // if ($now['PPSN2_SUBPN'] == '213371500') {
            //     logger(json_encode($now));
            // }
            if ((int) $now['SERD_QTY'] > 0) {

                $totalPer = 0;
                $totalQty = 0;
                $cekInsider = [];

                if (empty($now['REGIST_PER'])) {
                    if (!empty($now['PPSN2_QTPER_OLD'])) {
                        foreach ($data as $keyPer => $valuePer) {
                            if ($valuePer['PPSN1_PROCD'] === $now['PPSN1_PROCD']) {
                                $cekInsider[] = $valuePer;
                                $totalPer += round($valuePer['PPSN2_QTPER_OLD'], 2);
                                $totalQty += round($valuePer['SERD_QTY'], 2);
                            }

                            if ((($totalPer < 1 && $totalPer > 0) || $totalPer > 1 && fmod($totalPer, 1) !== 0.00) && count($data) === 1) {
                                $totalPer = $now['SERD_QTPER'];
                            }
                        }
                    } else {
                        $totalPer = round($now['SERD_QTPER'], 2);
                        foreach ($data as $keyPer => $valuePer) {
                            if ($valuePer['PPSN1_PROCD'] === $now['PPSN1_PROCD']) {
                                $cekInsider[] = $valuePer;
                                $totalQty += round($valuePer['SERD_QTY'], 2);
                            }
                        }
                    }
                } else {
                    foreach ($data as $keyPer => $valuePer) {
                        if ($valuePer['PPSN1_PROCD'] === $now['PPSN1_PROCD'] && $valuePer['SERD_MC'] === $now['SERD_MC'] && $valuePer['SERD_MCZ'] === $now['SERD_MCZ']) {
                            $cekInsider[] = $valuePer;
                            $totalPer += $now['PPSN2_QTPER'] == $now['REGIST_PER'] ? round($valuePer['REAL_PER'], 4) : round($valuePer['REAL_PER']);
                            $totalQty += round($valuePer['SERD_QTY'], 2);
                        }
                    }

                    if (is_float($totalPer)) {
                        if (is_float(floatval($now['SERD_QTPER']))) {
                            $totalPer = round(floatval($now['SERD_QTPER']), 1);
                            // $totalPer = 1;
                        } else {
                            $totalPer = (int) $now['SERD_QTPER'];
                        }
                    }
                }

                // if (count($hasil) === 0) {
                //     // $total = ((int)round($now['SERD_QTY'])) - $totalQty;
                //     $total = ((int)round($now['SERD_QTY'])) - ($qty * $totalPer);
                // } else {
                //     $total = ((int)round($now['SERD_QTY'])) - $qty;
                // }
                $total = ((int) round($now['SERD_QTY'])) - (count($hasil) > 0 ? $qty : $qty * $totalPer);
                if ($total >= 0) { //Jika SERD_QTY mencukupi / lebih
                    if (count($hasil) === 0) {
                        if (is_float(floatval($now['SERD_QTPER']))) {
                            // $qtPer = floatval($now['SERD_QTPER']);
                            $qtPer = 1;
                        } else {
                            $qtPer = round($totalPer);
                        }
                    } else {
                        $qtPer = round(floatval((($qty / $totalQtyNeed) * $totalPer)), 2);
                    }

                    // $qtPer = ($now['SERD_QTY'] / $totalQty) * $totalPer;
                    // $qtPer = round($now['PPSN2_QTPER'], 2);
                    $totalQtySubmited = $qty;
                    foreach ($hasil as $keyHasil => $valueHasil) {
                        $totalQtySubmited = $totalQtySubmited + $valueHasil['SERD_QTY'];
                    }

                    $cekTotalxNowqtper = $totalQtySubmited * $qtPer;
                    if ($qtPer > 0) {
                        if (round($cekTotalxNowqtper) !== round($now['SERD_QTY'])) {
                            $now['PPSN2_QTPER'] = round($now['REAL_PER'], 4);
                            $now['TOTAL_PER'] = round($now['REAL_PER'], 4);
                        } else {
                            $now['PPSN2_QTPER'] = round($qtPer);
                            $now['TOTAL_PER'] = round($qtPer, 4);
                        }

                        if (count($hasil) > 0) {
                            $now['SERD_QTY'] = round($qty);
                            $now['PPSN2_QTPER'] = round($qty) / $totalQty;
                        } else {
                            $now['SERD_QTY'] = $qty * $qtPer;
                            if (is_float(floatval($now['SERD_QTPER']))) {
                                $now['SERD_QTY'] = $qty * round(floatval($now['SERD_QTPER']), 2);
                            }

                            $now['PPSN2_QTPER'] = $qtPer;
                        }

                        $pembagian = (($totalQtyNeed > 0 ? $totalQtyNeed : ($qty * $totalPer)));

                        logger(json_encode([$pembagian, $totalPer, $now['PPSN2_QTPER']]));

                        if ($pembagian > 0 && $totalPer > 0) {
                            $now['PPSN2_QTPER'] = round($now['SERD_QTY'] / $pembagian * $totalPer, 4);
                        }

                        $now['TOTAL_PER'] = $totalPer;
                    }

                    logger(json_encode([
                        'logs' => 'checkDataRecurs',
                        'item' => $now['PPSN2_SUBPN'],
                        'qty' => $qty,
                        'qtyNeed' => $totalQtyNeed,
                        'per' => $now['PPSN2_QTPER'],
                        'serd_per' => floatval($now['SERD_QTPER']),
                        'is_float_serd' => is_float(floatval($now['SERD_QTPER'])),
                        'totalPer' => $totalPer,
                        'totalQty' => $totalQty,
                        'count' => count($hasil),
                        'is_dec' => $this->is_decimal($totalPer),
                        'rounding' => round($totalPer),
                        'total' => $total,
                        'data' => $hasil,
                        'nowData' => $now,
                        'perData' => $cekInsider,
                        'cekCompare' => $cekTotalxNowqtper,
                        'status' => 'Complete Qty'
                    ]));


                    $hasil[] = $now;
                } else { // Jika masih ada sisa qty
                    $totalnya = $total * -1;
                    $qtPer = round(floatval((((int) round($now['SERD_QTY'])) / ($qty * $totalPer)) * $totalPer), 4);

                    $totalQtySubmited = $qty;
                    foreach ($hasil as $keyHasil => $valueHasil) {
                        $totalQtySubmited = $totalQtySubmited + $valueHasil['SERD_QTY'];
                    }

                    if ($qtPer > 0) {
                        $cekTotalxNowqtper = $totalQtySubmited * $qtPer;
                        if (round($cekTotalxNowqtper) !== round($now['SERD_QTY'])) {
                            $now['PPSN2_QTPER'] = round($now['REAL_PER'], 4);
                            $now['TOTAL_PER'] = round($now['REAL_PER'], 4);
                        } else {
                            $now['PPSN2_QTPER'] = round($qtPer, 2);
                            $now['TOTAL_PER'] = round($qtPer, 4);
                        }

                        // $now['PPSN2_QTPER'] = round(($qty * $qtPer)) / $totalQty;
                        $now['SERD_QTY'] = round(($qty * $qtPer));
                        $now['PPSN2_QTPER'] = round(($now['SERD_QTY'] / ($qty * $totalPer)) * $totalPer, 4);

                        $now['TOTAL_PER'] = $totalPer;
                    }

                    logger(json_encode([
                        'logs' => 'checkDataRecurs',
                        'item' => $now['PPSN2_SUBPN'],
                        'qty' => $qty,
                        'per' => round($qtPer, 4),
                        'per2' => round($qtPer, 2),
                        'qtyNeed' => $totalQtyNeed,
                        'totalPer' => $totalPer,
                        'totalQty' => $totalQty,
                        'count' => count($hasil),
                        'is_dec' => $this->is_decimal($totalPer),
                        'rounding' => round($totalPer),
                        'total' => $total,
                        'data' => $hasil,
                        'nowData' => $now,
                        'perData' => $cekInsider,
                        'cekCompare' => $cekTotalxNowqtper,
                        'status' => 'Uncomplete Qty'
                    ]));


                    $hasil[] = $now;
                    next($data);
                    return $this->FIFOSERDLOT($data, $totalnya, $hasil, ($qty * $totalPer));
                }

                return $hasil;
            } else {
                next($data);
                return $this->FIFOSERDLOT($data, $qty, $hasil);
            }
        } else {
            return $hasil;
        }
    }

    public function is_decimal($val)
    {
        return is_numeric($val) && floor($val) != $val;
    }

    public function getSERDTable($job)
    {
        $getData = SERDTBL::select(
            DB::raw('MAX(SERD_PSNNO) AS PPSN1_PSNNO'),
            DB::raw('SERD_LINENO AS PPSN1_LINENO'),
            DB::raw('SERD_ITMCD AS PPSN2_SUBPN'),
            DB::raw('SERD_PROCD AS PPSN1_PROCD'),
            DB::raw('CASE WHEN xp.QTPER IS NULL
                THEN CAST(SUM(st.SERD_QTY) / (
                    SELECT
                        MAX(st2.PPSN1_SIMQT)
                    FROM
                    PSI_WMS.dbo.XPPSN1 st2
                    WHERE
                        st2.PPSN1_WONO = st.SERD_JOB
                        -- AND st2.PPSN1_PSNNO = st.SERD_PSNNO
                        AND st2.PPSN1_LINENO = st.SERD_LINENO
                ) AS DECIMAL(10,3))
                ELSE MAX(xp.QTPER)
                END AS PPSN2_QTPER'),
            // DB::raw('CAST(SUM(st.SERD_QTY) / (
            //     SELECT
            //         MAX(st2.PPSN1_SIMQT)
            //     FROM
            //         XPPSN1 st2
            //     WHERE
            //         st2.PPSN1_WONO = st.SERD_JOB
            //         -- AND st2.PPSN1_PSNNO = st.SERD_PSNNO
            //         AND st2.PPSN1_LINENO = st.SERD_LINENO
            // ) AS DECIMAL(10,2)) as PPSN2_QTPER'),
            DB::raw("
                CAST(SUM(st.SERD_QTY) / (
                SELECT
                    MAX(st2.PPSN1_SIMQT)
                FROM
                PSI_WMS.dbo.XPPSN1 st2
                WHERE
                    st2.PPSN1_WONO = st.SERD_JOB
                    -- AND st2.PPSN1_PSNNO = st.SERD_PSNNO
                    AND st2.PPSN1_LINENO = st.SERD_LINENO
            ) AS DECIMAL(10,3)) AS PPSN2_QTPER_OLD"),
            DB::raw("SUM(st.SERD_QTY) / (
                SELECT MAX(st2.PPSN1_SIMQT) FROM PSI_WMS.dbo.XPPSN1 st2
                WHERE st2.PPSN1_WONO = st.SERD_JOB
                -- AND st2.PPSN1_PSNNO = st.SERD_PSNNO
                AND st2.PPSN1_LINENO = st.SERD_LINENO
            ) AS REAL_PER"),
            DB::raw("SUM(xp.QTPER) AS REGIST_PER"),
            'SERD_JOB',
            'SERD_LOTNO',
            DB::raw("SUM(st.SERD_QTY) AS SERD_QTY"),
            'MITM_ITMD1',
            'MITM_ITMD2',
            'SERD_FR',
            'SERD_MC',
            'SERD_MCZ',
            'xp2.PPSN2_ITMCAT',
            'SERD_QTPER'
        )->from('v_serd_tbl_opt as st')
            ->where('SERD_JOB', $job)
            ->join('PSI_WMS.dbo.MITM_TBL', 'SERD_ITMCD', 'MITM_ITMCD')
            ->leftjoin(DB::raw('(
                    SELECT
                        DOC_NO,
                        SCR_ITMCD,
                        SCR_LOTNO,
                        SCR_MC,
                        SCR_MCZ,
                        SUM(rhd.SCR_QTY) as QTYSCR
                    FROM PSI_RPCUST.dbo.RPSCRAP_HIST rh
                    INNER JOIN PSI_RPCUST.dbo.RPSCRAP_HIST_DET rhd ON rh.id = rhd.SCR_HIST_ID
                    -- INNER JOIN PSI_WMS.dbo.v_ith_tblc vit
                    --  ON rh.ID_TRANS = vit.ITH_DOC
                    --  AND CAST(rh.id AS VARCHAR(100)) = vit.ITH_REMARK
                    WHERE rh.deleted_at IS NULL
                    AND rhd.deleted_at IS NULL
                    GROUP BY
                        DOC_NO,
                        SCR_ITMCD,
                        SCR_LOTNO,
                        SCR_MC,
                        SCR_MCZ
                ) scrdet'), function ($j) {
                $j->on('scrdet.DOC_NO', 'SERD_JOB');
                $j->on('scrdet.SCR_ITMCD', 'SERD_ITMCD');
                $j->on('scrdet.SCR_LOTNO', 'SERD_LOTNO');
                $j->on('scrdet.SCR_MC', 'SERD_MC');
                $j->on('scrdet.SCR_MCZ', 'SERD_MCZ');
            })
            ->leftjoin(DB::raw('(
                SELECT
                    PIS3_ITMCD,
                    PIS3_WONO,
                    PIS3_PROCD,
                    CASE
                        WHEN PDPP_WORQT != SIMQT THEN
                            (SUM(PIS3_REQQT)+(SUM(PIS3_REQQT)/ SIMQT * (PDPP_WORQT-SIMQT))) / PDPP_WORQT
                        else SUM(PIS3_REQQT)/ SIMQT
                    END AS QTPER,
                    RTRIM(PIS3_MC) PIS3_MC,
                    RTRIM(PIS3_MCZ) PIS3_MCZ
                FROM PSI_WMS.dbo.XPIS3
                left JOIN PSI_WMS.dbo.XWO ON
                    PIS3_WONO = PDPP_WONO
                LEFT JOIN (
                    SELECT
                        PPSN1_WONO,
                        PPSN1_DOCNO,
                        MAX(PPSN1_SIMQT) SIMQT,
                        RTRIM(PPSN1_MDLCD) PPSN1_MDLCD
                    FROM
                    PSI_WMS.dbo.XPPSN1
                    GROUP BY
                        PPSN1_WONO,
                        PPSN1_MDLCD,
                        PPSN1_DOCNO
                ) VPPSN1 ON
                    PPSN1_WONO = PIS3_WONO
                WHERE PIS3_DOCNO = VPPSN1.PPSN1_DOCNO
                GROUP BY
                    PIS3_WONO,
                    PDPP_WORQT,
                    PIS3_PROCD,
                    VPPSN1.SIMQT,
                    PIS3_ITMCD,
                    PIS3_MC,
                    PIS3_MCZ
            ) xp'), function ($j) {
                $j->on('xp.PIS3_WONO', 'SERD_JOB');
                $j->on('xp.PIS3_ITMCD', 'SERD_ITMCD');
                $j->on('xp.PIS3_PROCD', 'SERD_PROCD');
                $j->on('xp.PIS3_MC', 'SERD_MC');
                $j->on('xp.PIS3_MCZ', 'SERD_MCZ');
            })
            ->leftjoin(DB::raw('(
                SELECT
                    xp2.PPSN2_PSNNO,
                    xp2.PPSN2_SUBPN,
                    xp2.PPSN2_ITMCAT,
                    xp2.PPSN2_MC,
                    xp2.PPSN2_MCZ
                FROM PSI_WMS.dbo.XPPSN2 xp2
                WHERE xp2.PPSN2_DOCNO IS NOT NULL
                GROUP BY
                    xp2.PPSN2_PSNNO,
                    xp2.PPSN2_SUBPN,
                    xp2.PPSN2_ITMCAT,
                    xp2.PPSN2_MC,
                    xp2.PPSN2_MCZ
            ) xp2'), function ($j) {
                $j->on('xp2.PPSN2_PSNNO', 'st.SERD_PSNNO');
                $j->on('xp2.PPSN2_SUBPN', 'st.SERD_ITMCD');
                $j->on('xp2.PPSN2_MC', 'st.SERD_MC');
                $j->on('xp2.PPSN2_MCZ', 'st.SERD_MCZ');
            })
            ->groupBy(
                'SERD_ITMCD',
                'SERD_LINENO',
                'SERD_PROCD',
                'SERD_LOTNO',
                'SERD_JOB',
                'SERD_FR',
                // 'SERD_MCZ',
                'MITM_ITMD1',
                'MITM_ITMD2',
                'xp.QTPER',
                'SERD_MC',
                'SERD_MCZ',
                'xp2.PPSN2_ITMCAT',
                'SERD_QTPER',
                // 'SERD_MSCANTM'
            )
            // ->orderBy('SERD_MSCANTM')
            ->get()
            ->toArray();

        return $getData;
    }

    public function checkItemSub($item_mtr, $item_model, $job)
    {
        $searchJob = DB::connection('PSI_RPCUST')->select(DB::raw("SET NOCOUNT ON;EXEC PSI_RPCUST.dbo.sp_check_mapping_item @item_material = ?, @item_model = ?, @job = ?"), [
            $item_mtr,
            $item_model,
            $job
        ]);

        return $searchJob;
    }

    public function checkReverseProcess($data)
    {
        $parsed = [];

        $searchPCBPer = array_values(array_filter($data, function ($f) {
            // logger(json_encode($f));
            $itemD1 = trim($f['MITM_ITMD1']);
            return
                trim($f['PPSN1_PROCD']) === 'SMT-AB'
                && (strpos($itemD1, 'PRINT CIRCUIT BOARD') !== false ||
                    strpos($itemD1, 'PCB') !== false ||
                    strpos($itemD1, 'P.C.B') !== false ||
                    strpos($itemD1, 'P,C,B') !== false);
        }));

        // logger($searchPCBPer);

        foreach ($data as $key => $value) {
            if (!empty($value['PPSN1_PROCD'])) {
                if (trim($value['PPSN1_PROCD']) === 'SMT-AB') {
                    // Jika komponen bukan PCB maka di bagi 2
                    if (
                        (strpos($value['MITM_ITMD1'], 'PRINT CIRCUIT BOARD') !== false ||
                            strpos($value['MITM_ITMD1'], 'PCB') === false ||
                            strpos($value['MITM_ITMD1'], 'P.C.B') === false ||
                            strpos($value['MITM_ITMD1'], 'P,C,B') === false)
                    ) {
                        $getPCBPer = $searchPCBPer[0]['PPSN2_QTPER'];

                        $parsed[] = [
                            "MITM_ITMD1" => $value['MITM_ITMD1'],
                            "MITM_ITMD2" => $value['MITM_ITMD2'],
                            "PPSN1_LINENO" => $value['PPSN1_LINENO'],
                            "PPSN1_PSNNO" => $value['PPSN1_PSNNO'],
                            "PPSN2_QTPER" => (0.5 / $getPCBPer) * $value['PPSN2_QTPER'],
                            "PPSN2_SUBPN" => $value['PPSN2_SUBPN'],
                            "PPSN1_PROCD" => $value['PPSN1_PROCD'],
                            "SERD_JOB" => $value['SERD_JOB'],
                            "RPDSGN_CODE" => $value['RPDSGN_CODE'],
                            "REAL_PER" => $value['REAL_PER']
                        ];
                    } else {
                        $parsed[] = $value;
                    }
                } else {
                    $parsed[] = $value;
                }
            }
        }

        return $parsed;
    }

    public function cekAngkaKoma($data, $qty)
    {
        $hasil = [];
        foreach ($data as $key => $value) {
            $hasil[trim($value['PPSN1_PROCD']) . '|' . trim($value['SERD_MC']) . '|' . trim($value['SERD_MCZ'])][$key] = [
                'dataItem' => $value,
                'qtper' => $value['PPSN2_QTPER'],
                'qty' => $qty,
                'jumlah' => fmod($qty * $value['PPSN2_QTPER'], 1) !== 0.00 ? $qty * $value['REAL_PER'] : $qty * $value['PPSN2_QTPER'],
                'process' => $value['PPSN1_PROCD'],
                'real_per' => $value['REAL_PER'],
                'serd_per' => $value['SERD_QTPER']
            ];
        }

        // return [
        //     'status' => false,
        //     'data' => $hasil
        // ];

        $cekData = [];
        $hasilSum = 0;
        foreach ($hasil as $keyJum => $valueJum) {
            $sumnya1 = 0;
            $sumnya2 = 0;
            $sumnya3 = 0;
            $sumnya4 = 0;
            foreach (array_values($valueJum) as $keyJum2 => $valueJum2) {
                $sumnya1 += $valueJum2['jumlah'];
                $sumnya2 += $valueJum2['qty'] * $valueJum2['qtper'];
                $sumnya3 += $valueJum2['qty'] * $valueJum2['real_per'];
                $sumnya4 += $valueJum2['qty'] * $valueJum2['serd_per'];
            }

            $cekData[] = [
                'ITEM' => $keyJum,
                'Jumlah' => round($sumnya1, 2),
                'sumnya1' => round($sumnya2, 2),
                'sumnya2' => round($sumnya3, 2),
                'sumnya3' => round($sumnya4, 2),
                'DATA' => $valueJum
            ];

            $hasilSum += $sumnya1;
        }

        // logger($cekData);

        $cek = array_filter(array_values($hasil), function ($f) {
            $sumnya = 0;
            foreach (array_values($f) as $key => $value) {
                $sumnya += $value['qty'] * $value['qtper'];
            }

            $sumnya2 = 0;
            foreach (array_values($f) as $key => $value) {
                $sumnya2 += $value['qty'] * $value['real_per'];
            }

            $sumnya3 = 0;
            foreach (array_values($f) as $key => $value) {
                $sumnya3 += $value['qty'] * $value['serd_per'];
            }

            return $this->is_decimal(round($sumnya3, 2)) && $this->is_decimal(round($sumnya, 2)) && $this->is_decimal(round($sumnya2, 2));
        });

        $proc = [];
        foreach ($cek as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $proc[] = $value2['process'];
            }
        }

        if (count($cek) > 0) {
            return [
                'status' => false,
                'process' => implode(",", $proc),
                'data' => $cekData,
                'total' => $hasilSum,
                'dataCek' => $cek
            ];
        } else {
            return [
                'status' => true,
                'process' => implode(",", $proc),
                'data' => $cekData
            ];
        }
    }

    public function insertMasterScrap($idMapping, $qty, $isPending, $doc, $remark, $process, $model, $item, $date, $ref = '', $is_ret = false, $menu_id = '', $purch_date = '')
    {
        return scrapHist::create([
            'DSGN_MAP_ID' => $idMapping,
            'USERNAME' => $this->user,
            'DEPT' => $this->dept,
            'QTY' => $qty,
            'QTY_SCRAP' => $isPending,
            'ID_TRANS' => $this->idTransaction,
            'TYPE_TRANS' => 'NORMAL',
            'DOC_NO' => $doc,
            'REASON' => $remark,
            'SCR_PROCD' => $process,
            'MDL_FLAG' => $model,
            'ITEMNUM' => $item,
            'DATE_OUT' => empty($date) || $date === '1970-01-01' ? '' : date('Y-m-d', strtotime(str_replace('/', '-', $date))),
            'REF_NO' => $ref,
            'IS_RETURN' => $is_ret,
            'MAIN_REASON' => $this->mainreason,
            'MENU_ID' => $this->menu,
            'PURCH_DATE' => empty($purch_date) ? null : $purch_date,
            'SCRAP_UNDER_REMARK' => empty($this->responsible) ? '' : $this->responsible
        ]);
    }

    public function defineMappingScrapArray($data)
    {
        $datanya = [];
        foreach ($data as $key => $f) {
            if ($f['RPDSGN_PRO_ID'] == 'SMT-HW' && !empty($f['RPDSGN_PROCD'])) {
                $datanya[] = array_merge(
                    $f,
                    ['RPDSGN_PROCD' => 'SMT-HW']
                );
            } elseif ($f['RPDSGN_PRO_ID'] == 'SMT-HWADD' && !empty($f['RPDSGN_PROCD'])) {
                $datanya[] = array_merge(
                    $f,
                    ['RPDSGN_PROCD' => 'SMT-HWAD']
                );
            } elseif ($f['RPDSGN_PRO_ID'] == 'SMT-SP' && !empty($f['RPDSGN_PROCD'])) {
                $datanya[] = array_merge(
                    $f,
                    ['RPDSGN_PROCD' => 'SMT-SP']
                );
            } else {
                if ($f['RPDSGN_PROCD'] == 'SMT-AV') {
                    $datanya[] = array_merge(
                        $f,
                        ['RPDSGN_PROCD' => 'SMT-AX']
                    );
                } elseif ($f['RPDSGN_PROCD'] == 'SMT-RD') {
                    $datanya[] = array_merge(
                        $f,
                        ['RPDSGN_PROCD' => 'SMT-RG']
                    );
                } else {
                    $datanya[] = $f;
                }
            }
        }

        return $datanya;
    }

    public function insertToITH($item, $date, $form, $doc, $qty, $wh, $ser, $remark, $user)
    {
        return ITHMaster::insert([
            'ITH_ITMCD' => $item,
            'ITH_DATE' => $date,
            'ITH_FORM' => $form,
            'ITH_DOC' => $doc,
            'ITH_QTY' => $qty,
            'ITH_WH' => $wh,
            'ITH_LOC' => '',
            'ITH_SER' => $ser,
            'ITH_REMARK' => $remark,
            'ITH_LINE' => NULL,
            'ITH_LUPDT' => $date . ' ' . date('H:i:s'),
            'ITH_USRID' => $user,
        ]);
    }

    public function sendingParamBCStock($item_num, $date_out = '', $lot = null, $qty = 0, $doc = null, $bc = null, $loc = '')
    {
        $endpoint = 'http://192.168.0.29:8080/api_inventory/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item_num,
            'date_out' => empty($date_out) ? date('Y-m-d') : $date_out,
            'lot' => $lot,
            'qty' => $qty,
            'doc' => $doc,
            'bc' => $bc,
            'loc' => $loc
            // 'scrap' => true,
            // 'remark' => $id,
            // 'revise' => $rev
        ];

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content['CURL'];
    }

    public function checkerDesignMapping()
    {
        # code...
    }
}


