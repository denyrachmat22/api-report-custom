<?php

namespace App\Traits;
use Illuminate\Support\Facades\DB;

trait traitKKALogic
{
    public function getKKAData($mutasi, $period, $fdate, $ldate, $item = '')
    {
        ini_set('memory_limit', '2G');
        $data = DB::table('MITM_TBL')->leftjoin('v_ith_tblc', 'MITM_ITMCD', 'ITH_ITMCD');

        if (!empty($item)) {
            $data->where('MITM_ITMCD', $item);
        }

        $dataSetup = DB::connection('PSI_RPCUST')->table('RPKKA_SETUP')->where('KKA_MUTASI', $mutasi);
        $periodSetup = DB::connection('PSI_RPCUST')
            ->table("f_kka_date(".$period.",'".$fdate."','".$ldate."')")
            ->orderBy('period_iter')
            ->get()
            ->all();

        if (count($dataSetup->get()->all()) > 0) {
            $listModel = with(clone $dataSetup)->where('KKA_CODE', 'model')->get()->pluck('KKA_VALUE')->all();
            $listWH = with(clone $dataSetup)->where('KKA_CODE', 'wh')->get()->pluck('KKA_VALUE')->all();
            
            $dataSaldoAwal = with(clone $data)->select(
                DB::raw('rtrim(MITM_ITMCD) as MITM_ITMCD'),
                DB::raw('rtrim(MITM_ITMD1) as MITM_ITMD1'),
                DB::raw('rtrim(MITM_STKUOM) as MITM_STKUOM'),
                DB::raw('CAST(sum(ITH_QTY) AS decimal(10,2)) as begin_stock'),
            )->whereIn('MITM_MODEL', $listModel)
            ->where('ITH_DATEC', '<', $fdate)
            ->whereIn('ITH_WH',  $listWH)
            ->groupBy(
                'MITM_ITMCD',
                'MITM_ITMD1',
                'MITM_STKUOM'
            )
            ->havingRaw('sum(ITH_QTY) > 0')
            ->get()
            ->all();

            $dataSaldoBetweenTanggal = with(clone $data)->select(
                DB::raw('rtrim(MITM_ITMCD) as MITM_ITMCD'),
                DB::raw('rtrim(MITM_ITMD1) as MITM_ITMD1'),
                DB::raw('rtrim(MITM_STKUOM) as MITM_STKUOM'),
                'ITH_WH',
                'ITH_FORM',
                DB::raw('CAST((CASE WHEN sum(ITH_QTY) < 0
                    THEN sum(ITH_QTY) * -1
                    ELSE sum(ITH_QTY)
                END) AS decimal(10,2)) as ITH_QTY'),
                'ITH_DATEC'
            )->whereIn('MITM_MODEL', $listModel)
            ->whereBetween('ITH_DATEC', [$fdate, $ldate])
            ->groupBy(
                'MITM_ITMCD',
                'MITM_ITMD1',
                'MITM_STKUOM',
                'ITH_WH',
                'ITH_FORM',
                'ITH_DATEC'
            )
            ->havingRaw('CAST((CASE WHEN sum(ITH_QTY) < 0
                    THEN sum(ITH_QTY) * -1
                    ELSE sum(ITH_QTY)
                END) AS INT) > 0')
            ->orderBy('MITM_ITMCD')
            ->get()
            ->all();
            
            $wh = $this->getKKASetup('bahan_baku', 'wh');

            $incWh = $this->getKKASetup('bahan_baku', 'inc_wh');
            $incForm = $this->getKKASetup('bahan_baku', 'inc_form');

            $incWhOpt1 = $this->getKKASetup('bahan_baku', 'inc_wh_opt_1');
            $incFormOpt1 = $this->getKKASetup('bahan_baku', 'inc_form_opt_1');
            
            $incWhOpt2 = $this->getKKASetup('bahan_baku', 'inc_wh_opt_2');
            $incFormOpt2 = $this->getKKASetup('bahan_baku', 'inc_form_opt_2');
            
            $incWhOpt3 = $this->getKKASetup('bahan_baku', 'inc_wh_opt_3');
            $incFormOpt3 = $this->getKKASetup('bahan_baku', 'inc_form_opt_3');
            
            $outWh = $this->getKKASetup('bahan_baku', 'out_wh');
            $outForm = $this->getKKASetup('bahan_baku', 'out_form');
            
            $outWhOpt1 = $this->getKKASetup('bahan_baku', 'out_wh_opt_1');
            $outFormOpt1 = $this->getKKASetup('bahan_baku', 'out_form_opt_1');

            $outWhOpt2 = $this->getKKASetup('bahan_baku', 'out_wh_opt_2');
            $outFormOpt2 = $this->getKKASetup('bahan_baku', 'out_form_opt_2');
            
            $outWhOpt3 = $this->getKKASetup('bahan_baku', 'out_wh_opt_3');
            $outFormOpt3 = $this->getKKASetup('bahan_baku', 'out_form_opt_3');

            $outWhOpt4 = $this->getKKASetup('bahan_baku', 'out_wh_opt_4');
            $outFormOpt4 = $this->getKKASetup('bahan_baku', 'out_form_opt_4');
            
            $dataInventory = $this->getSummaryInventory(
                $wh
            );

            // return [$outWhOpt1, $outFormOpt1];
            $hasil = [];
            foreach ($dataSaldoAwal as $key => $value) { 
                foreach ($periodSetup as $keyPeriod => $valuePeriod) {
                    $opn_stock = $this->invGetData(
                        $value->MITM_ITMCD,
                        $dataInventory,
                        'INV_QTY',
                        date('Y', strtotime($valuePeriod->first_date)),
                        date('n', strtotime($valuePeriod->first_date))
                    );

                    $opn_stock_date = $this->invGetData(
                        $value->MITM_ITMCD,
                        $dataInventory,
                        'INV_DATE',
                        date('Y', strtotime($valuePeriod->first_date)),
                        date('n', strtotime($valuePeriod->first_date))
                    );

                    $phy_stock = $this->invGetData(
                        $value->MITM_ITMCD,
                        $dataInventory,
                        'INV_PHY_QTY',
                        date('Y', strtotime($valuePeriod->first_date)),
                        date('n', strtotime($valuePeriod->first_date))
                    );

                    $phy_stock_date = $this->invGetData(
                        $value->MITM_ITMCD,
                        $dataInventory,
                        'INV_PHY_DATE',
                        date('Y', strtotime($valuePeriod->first_date)),
                        date('n', strtotime($valuePeriod->first_date))
                    );

                    $hasil[] = (object) array_merge(
                        (array) $valuePeriod, 
                        (array) $value,
                        [
                            'inc_stock' => count($incWh) === 0
                            ? '0' 
                            : $this->getSummaryBetweenDate(
                                (array) $dataSaldoBetweenTanggal,
                                $value->MITM_ITMCD,
                                $incWh,
                                $incForm,
                                $valuePeriod->first_date,
                                empty($phy_stock_date) ? $opn_stock_date : $phy_stock_date
                            ),
                            'inc_opt_1_stock' => count($incWhOpt1) === 0
                            ? '0' 
                            : $this->getSummaryBetweenDate(
                                (array) $dataSaldoBetweenTanggal,
                                $value->MITM_ITMCD,
                                $incWhOpt1,
                                $incFormOpt1,
                                $valuePeriod->first_date,
                                empty($phy_stock_date) ? $opn_stock_date : $phy_stock_date
                            ),
                            'inc_opt_2_stock' => count($incWhOpt2) === 0
                            ? '0' 
                            : $this->getSummaryBetweenDate(
                                (array) $dataSaldoBetweenTanggal,
                                $value->MITM_ITMCD,
                                $incWhOpt2,
                                $incFormOpt2,
                                $valuePeriod->first_date,
                                empty($phy_stock_date) ? $opn_stock_date : $phy_stock_date
                            ),
                            'inc_opt_3_stock' => count($incWhOpt3) === 0
                            ? '0' 
                            : $this->getSummaryBetweenDate(
                                (array) $dataSaldoBetweenTanggal,
                                $value->MITM_ITMCD,
                                $incWhOpt3,
                                $incFormOpt3,
                                $valuePeriod->first_date,
                                empty($phy_stock_date) ? $opn_stock_date : $phy_stock_date
                            ),
                            'out_stock' => count($outWh) === 0
                            ? '0' 
                            : $this->getSummaryBetweenDate(
                                (array) $dataSaldoBetweenTanggal,
                                $value->MITM_ITMCD,
                                $outWh,
                                $outForm,
                                $valuePeriod->first_date,
                                empty($phy_stock_date) ? $opn_stock_date : $phy_stock_date
                            ),
                            'out_opt_1_stock' => count($outWhOpt1) === 0
                            ? '0' 
                            : $this->getSummaryBetweenDate(
                                (array) $dataSaldoBetweenTanggal,
                                $value->MITM_ITMCD,
                                $outWhOpt1,
                                $outFormOpt1,
                                $valuePeriod->first_date,
                                empty($phy_stock_date) ? $opn_stock_date : $phy_stock_date
                            ),
                            'out_opt_2_stock' => count($outWhOpt2) === 0
                            ? '0' 
                            : $this->getSummaryBetweenDate(
                                (array) $dataSaldoBetweenTanggal,
                                $value->MITM_ITMCD,
                                $outWhOpt2,
                                $outFormOpt2,
                                $valuePeriod->first_date,
                                empty($phy_stock_date) ? $opn_stock_date : $phy_stock_date
                            ),
                            'out_opt_3_stock' => count($outWhOpt3) === 0
                            ? '0' 
                            : $this->getSummaryBetweenDate(
                                (array) $dataSaldoBetweenTanggal,
                                $value->MITM_ITMCD,
                                $outWhOpt3,
                                $outFormOpt3,
                                $valuePeriod->first_date,
                                empty($phy_stock_date) ? $opn_stock_date : $phy_stock_date
                            ),
                            'out_opt_4_stock' => count($outWhOpt4) === 0
                            ? '0' 
                            : $this->getSummaryBetweenDate(
                                (array) $dataSaldoBetweenTanggal,
                                $value->MITM_ITMCD,
                                $outWhOpt4,
                                $outFormOpt4,
                                $valuePeriod->first_date,
                                empty($phy_stock_date) ? $opn_stock_date : $phy_stock_date
                            ),
                            'disc_book_stock' => ($value->begin_stock + $this->getSummaryBetweenDate(
                                (array) $dataSaldoBetweenTanggal,
                                $value->MITM_ITMCD,
                                $incWh,
                                $incForm,
                                $valuePeriod->first_date,
                                empty($phy_stock_date) ? $opn_stock_date : $phy_stock_date
                            )) - $this->getSummaryBetweenDate(
                                (array) $dataSaldoBetweenTanggal,
                                $value->MITM_ITMCD,
                                $outWh,
                                $outForm,
                                $valuePeriod->first_date,
                                empty($phy_stock_date) ? $opn_stock_date : $phy_stock_date
                            ),
                            'opn_stock' => $opn_stock,
                            'opn_stock_date' => $opn_stock_date,
                            'phy_stock' => empty($phy_stock_date) ? $opn_stock : $phy_stock,
                            'phy_stock_date' => empty($phy_stock_date) ? $opn_stock_date : $phy_stock_date,
                            'tarik_mundur_inc' => count($incWh) === 0 || empty($phy_stock_date)
                            ? '0' 
                            : $this->getSummaryBetweenDate(
                                (array) $dataSaldoBetweenTanggal,
                                $value->MITM_ITMCD,
                                $incWh,
                                $incForm,
                                $phy_stock_date,
                                date('Y-m-t', strtotime($phy_stock_date)),
                                true
                            ),
                            'tarik_mundur_out' => count($incWh) === 0 || empty($phy_stock_date)
                            ? '0' 
                            : $this->getSummaryBetweenDate(
                                (array) $dataSaldoBetweenTanggal,
                                $value->MITM_ITMCD,
                                $outWh,
                                $outForm,
                                $phy_stock_date,
                                date('Y-m-t', strtotime($phy_stock_date)),
                                true
                            ),
                            'tot_saldo_phy' => (
                                (
                                    // (Saldo Awal + Incoming) - Outgoing
                                    (
                                        (
                                            $value->begin_stock + $this->getSummaryBetweenDate(
                                                (array) $dataSaldoBetweenTanggal,
                                                $value->MITM_ITMCD,
                                                $incWh,
                                                $incForm,
                                                $valuePeriod->first_date,
                                                empty($phy_stock_date) ? $opn_stock_date : $phy_stock_date
                                            )
                                        ) - $this->getSummaryBetweenDate(
                                            (array) $dataSaldoBetweenTanggal,
                                            $value->MITM_ITMCD,
                                            $outWh,
                                            $outForm,
                                            $valuePeriod->first_date,
                                            empty($phy_stock_date) ? $opn_stock_date : $phy_stock_date
                                        )
                                    ) + 
                                // Tarik Mundur Inc
                                $this->getSummaryBetweenDate(
                                    (array) $dataSaldoBetweenTanggal,
                                    $value->MITM_ITMCD,
                                    $incWh,
                                    $incForm,
                                    $phy_stock_date,
                                    date('Y-m-t', strtotime($phy_stock_date)),
                                    true
                                )) - 
                                // Tarik Mundur Out
                                $this->getSummaryBetweenDate(
                                    (array) $dataSaldoBetweenTanggal,
                                    $value->MITM_ITMCD,
                                    $outWh,
                                    $outForm,
                                    $phy_stock_date,
                                    date('Y-m-t', strtotime($phy_stock_date)),
                                    true
                                )
                            ),
                            'total_stock' => 
                            // Saldo Buku
                            (
                                (
                                    // (Saldo Awal + Incoming) - Outgoing
                                    (
                                        (
                                            $value->begin_stock + $this->getSummaryBetweenDate(
                                                (array) $dataSaldoBetweenTanggal,
                                                $value->MITM_ITMCD,
                                                $incWh,
                                                $incForm,
                                                $valuePeriod->first_date,
                                                empty($phy_stock_date) ? $opn_stock_date : $phy_stock_date
                                            )
                                        ) - $this->getSummaryBetweenDate(
                                            (array) $dataSaldoBetweenTanggal,
                                            $value->MITM_ITMCD,
                                            $outWh,
                                            $outForm,
                                            $valuePeriod->first_date,
                                            empty($phy_stock_date) ? $opn_stock_date : $phy_stock_date
                                        )
                                    ) + 
                                // Tarik Mundur Inc
                                $this->getSummaryBetweenDate(
                                    (array) $dataSaldoBetweenTanggal,
                                    $value->MITM_ITMCD,
                                    $incWh,
                                    $incForm,
                                    $phy_stock_date,
                                    date('Y-m-t', strtotime($phy_stock_date)),
                                    true
                                )) - 
                                // Tarik Mundur Out
                                $this->getSummaryBetweenDate(
                                    (array) $dataSaldoBetweenTanggal,
                                    $value->MITM_ITMCD,
                                    $outWh,
                                    $outForm,
                                    $phy_stock_date,
                                    date('Y-m-t', strtotime($phy_stock_date)),
                                    true
                                )
                            ) - $opn_stock
                        ]
                    );
                }              
            }
            
            return json_decode(json_encode($hasil), true);
        } else {
            return [
                'status' => false,
                'messages' => 'Report not supported yet !',
                'data' => []
            ];
        }
    }

    public function getKKASetup($mutasi, $code, $pluck = 'KKA_VALUE')
    {
        $dataSetup = DB::connection('PSI_RPCUST')->table('RPKKA_SETUP')->where('KKA_MUTASI', $mutasi);
        $list = with(clone $dataSetup)->where('KKA_CODE', $code)->get()->pluck($pluck)->all();

        return $list;
    }

    public function getSummaryBetweenDate($dataArr, $item, $listWH, $listEvent, $fdate, $ldate, $isTarikMundur = false)
    {
        $result = 0;
        $filterData = array_values(array_filter($dataArr, function ($f) use ($item, $listWH, $listEvent, $fdate, $ldate, $isTarikMundur) {
            if (
                $f->MITM_ITMCD == $item && 
                in_array($f->ITH_WH, $listWH) && 
                in_array($f->ITH_FORM, $listEvent) && 
                (
                    $isTarikMundur === true
                    ? (date('Y-m-d', strtotime($f->ITH_DATEC)) > $fdate && date('Y-m-d', strtotime($f->ITH_DATEC)) <= $ldate)
                    : (date('Y-m-d', strtotime($f->ITH_DATEC)) >= $fdate && date('Y-m-d', strtotime($f->ITH_DATEC)) <= $ldate)
                )
            ) {
                return $f->ITH_QTY;
            }
        }));

        foreach (json_decode(json_encode($filterData), true) as $key => $value) {
            $result += (int)$value['ITH_QTY'];
        }

        return $result;
    }

    public function getSummaryInventory($wh)
    {
        $list = DB::connection('PSI_RPCUST')->table('RPSAL_INVENTORY')
            // ->where('INV_YEAR', $year)
            // ->where('INV_MONTH', $month)
            ->whereIn('INV_WH', $wh)
            ->get()
            ->all();

        return $list;
    }

    public function invGetData($item, $data, $return, $year, $month)
    {
        $result = 0;
        $filterData = array_values(array_filter($data, function ($f) use ($item, $return, $year, $month) {
            if (
                $f->INV_ITMNUM == $item &&
                $f->INV_YEAR == $year &&
                $f->INV_MONTH == $month
            ) {
                if ($return === 'INV_QTY') {
                    return $f->INV_QTY;
                } else {
                    if (!empty($f->{$return})) {
                        return $f->{$return};
                    }
                }
            }
        }));

        if (!str_contains($return, 'QTY')) {
            $dataHasil = json_decode(json_encode($filterData), true);
            return count($dataHasil) > 0 ? $dataHasil[0][$return] : '';
        }

        foreach (json_decode(json_encode($filterData), true) as $key => $value) {
            $result += (int)$value[$return];
        }

        return $result;
    }
}