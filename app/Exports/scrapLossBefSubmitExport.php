<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;

class scrapLossBefSubmitExport implements FromCollection, WithHeadings, WithStartRow, WithEvents
{
    private $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function headings(): array
    {
        return [
            ['List Loss Candidate'],
            [],
            [
                'No',
                'Item Code',
                'Item Desc',
                'Total Req Qty',
                'Total Kitting Qty',
                'Logical Return Qty',
                'Actual Return Qty',
                'Total Loss Qty'
            ]
        ];
    }

    public function startRow(): int
    {
        return 4;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $hasil = [];
        foreach ($this->data as $key => $value) {
            $hasil[] = [
                $key + 1,
                (string) $value['PPSN2_SUBPN'],
                $value['MITM_ITMD1'],
                $value['TOT_REQQT'],
                $value['TOT_KITQTY'],
                $value['LOG_RET_QTY'],
                $value['ACT_RETQTY'],
                $value['LOG_RET_QTY'] - $value['ACT_RETQTY']
            ];
        }

        return collect($hasil);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // $event->sheet->getActiveSheet()->setPrintGridlines(false);
                $event->sheet->getDelegate()->getPageSetup()
                    ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE)
                    ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A3);

                $event->sheet->getStyle('A1:A1')->applyFromArray([
                    'font' => [
                        'size' => '15',
                        'bold' => true
                    ]
                ]);

                $event->sheet->getStyle('A5:F5')
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('ffffff');

                $highestRow = $event->sheet->getHighestRow();
                $highestColumn = $event->sheet->getHighestColumn();

                foreach(range('A', $highestColumn) as $columnID) {
                    $event->sheet->getColumnDimension($columnID)->setAutoSize(true) ;
                }

                $event->sheet->styleCells(
                    'A3:'.$highestColumn.$highestRow,
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],
                        ]
                    ]
                );

                // $event->sheet->getStyle('A1:A1')->getAlignment()->setHorizontal('center');
                $event->sheet->getStyle('A3:'.$highestColumn.'3')->getAlignment()->setHorizontal('center');
            }
        ];
    }

}