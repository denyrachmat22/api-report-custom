<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;

use App\Model\RPCUST\processMstr;

class scrapPendingUploadTemplateExport implements FromCollection, WithHeadings, WithStartRow, WithEvents, WithTitle
{
    use Exportable;
    use RegistersEventListeners;

    private $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function title(): string
    {
        return 'From Pending Scrap Upload Template';
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $hasil = [];
        foreach ($this->data as $key => $value) {
            $hasil[] = [
                $key + 1,
                (string) $value['assy_no'],
                $value['job_no'],
                count($value['mapping']) > 0 ? $value['mapping'][0]['RPDSGN_CODE'] : '',
                $value['reason'],
                isset($value['date']) ? $value['date'] : date('d/m/Y')
            ];
        }

        return collect($hasil);
    }

    public function headings(): array
    {
        $dataMapping = $this->getDataMapping('RPDSGN_DESC');

        return [
            ['MFG Repair Center'],
            ['FROM PENDING DOC'],
            ['PERIODE :'],
            [],
            [
                'No',
                'PCB Identity',
                '',
                '',
                '',
                '',
                'Process'
            ],
            array_merge(
                [
                    '',
                    'Assy No',
                    'Job No',
                    'MFG Choose',
                    'Cause of MRB',
                    'Scrap Date'
                ],
                $dataMapping
            )
        ];
    }

    public function startRow(): int
    {
        return 5;
    }

    public function toAlpha($num)
    {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return $this->toAlpha($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }

    public function getDataMapping($ret)
    {
        $getProcessList = processMstr::get();

        $procResult = [];
        foreach ($getProcessList as $key => $value) {
            $procResult[] = $value[$ret];
        }

        logger(array_merge($procResult, [count($getProcessList) => $ret === 'RPDSGN_CODE' ? 'MATERIAL' : 'Material']));

        return array_merge($procResult, [count($getProcessList) => $ret === 'RPDSGN_CODE' ? 'MATERIAL' : 'Material']);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // Start Protect Template
                $event->sheet->getProtection()->setPassword('PSI2021');
                $event->sheet->getProtection()->setSheet(true);
                // End Protect Template

                // Styling Template
                $highestRow = $event->sheet->getHighestRow();
                $highestColumn = $event->sheet->getHighestColumn();

                $event->sheet->getDelegate()->getPageSetup()
                    ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE)
                    ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A3);

                $event->sheet->getStyle('A1:' . $highestColumn . '3')->applyFromArray([
                    'font' => [
                        'size' => '15',
                        'bold' => true
                    ]
                ]);

                $event->sheet->getDelegate()->mergeCells('A1:D1');
                $event->sheet->getDelegate()->mergeCells('A2:D2');
                $event->sheet->getDelegate()->mergeCells('A3:B3');

                $event->sheet->getDelegate()->setCellValue($highestColumn . '1', 'PENDING');
                $event->sheet->getStyle($highestColumn . '1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $event->sheet->getDelegate()->mergeCells('A5:A6');
                $event->sheet->getDelegate()->mergeCells('B5:F5');
                $event->sheet->getDelegate()->mergeCells('G5:' . $highestColumn . '5');

                $event->sheet->getStyle('A5:' . $highestColumn . '6')->applyFromArray([
                    'font' => [
                        'size' => '12',
                        'bold' => true
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);

                $event->sheet->styleCells(
                    'A5:' . $highestColumn . $highestRow,
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],
                        ]
                    ]
                );

                foreach (range('A', $highestColumn) as $columnID) {
                    $event->sheet->getColumnDimension($columnID)->setAutoSize(true);
                }

                // Color Cells
                $event->sheet->getStyle('B5:F6')
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('ffbf91');

                $event->sheet->getStyle('G5:' . $highestColumn . '6')
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('91b4ff');

                // Can Edit Cell

                // Period Cell
                $event->sheet->getStyle('C3')->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
                $event->sheet->getStyle('A7:M1000')->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);

                $startDataRow = 7;
                foreach ($this->data as $key => $value) {
                    $getMFG = [];
                    $getProcessCode = [];
                    foreach ($value['mapping'] as $ketMFG => $valueMFG) {
                        $getMFG[] = $valueMFG['RPDSGN_CODE'];

                        foreach ($valueMFG['process_det'] as $keyMapDet => $valueMapDet) {
                            $getProcessCode[] = $valueMapDet['RPDSGN_PRO_ID'];
                        }
                    }

                    $startProcess = 6;
                    $startStock = 2;
                    $getActiveMapping = [];

                    $cellStockFormula = $this->toAlpha(15) . ($startDataRow + $key);
                    foreach ($this->getDataMapping('RPDSGN_CODE') as $keyMapCollect => $valueMapCollect) {
                        $checkIfMappingExist = array_filter($getProcessCode, function ($f) use ($valueMapCollect) {
                            return $f === $valueMapCollect;
                        });

                        $nowCells = $this->toAlpha($startProcess + $keyMapCollect) . ($startDataRow + $key);
                        if (isset($value[$valueMapCollect]) && $value[$valueMapCollect] > 0) {
                            $event->sheet->getDelegate()->setCellValue($nowCells, $value[$valueMapCollect]);
                        }

                        $getCellRange = $this->toAlpha($startProcess + $keyMapCollect) . ($startDataRow + $key) . ':' . $highestColumn . ($startDataRow + $key);

                        $event->sheet->getDelegate()->setCellValue($cellStockFormula, '=IF(SUM(' . $this->toAlpha($startProcess) . ($startDataRow + $key) . ':' . $highestColumn . ($startDataRow + $key) . ') <= Worksheet!B' . ($startStock + $key) . ', "OK", "NOK")');

                        if (!empty($value['assy_no']) && count($checkIfMappingExist) === 0) {
                            $this->changeCellColors($event->sheet, $getCellRange, 'c4c4c4');

                            $this->lockUnlockCell($event->sheet, $getCellRange);
                        } else {
                            $openCells = $this->toAlpha($startProcess + $keyMapCollect) . ($startDataRow + $key);

                            $getActiveMapping[] = $openCells;
                        }
                    }

                    // Close cells if not the latest mapping
                    foreach ($getActiveMapping as $keyOpenCells => $valueOpenCells) {
                        if ($valueOpenCells !== $getActiveMapping[count($getActiveMapping) - 1]) {
                            $this->changeCellColors($event->sheet, $valueOpenCells, 'c4c4c4');
                            $this->lockUnlockCell($event->sheet, $valueOpenCells);
                        } else {
                            $this->changeCellColors($event->sheet, $valueOpenCells, 'ffffff');
                            $this->lockUnlockCell($event->sheet, $valueOpenCells, false);
                            $validation = $this->setValidation($event->sheet, $valueOpenCells, \PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_WHOLE, 'Please input number only !');

                            // $validation = $event->sheet->getCell($valueOpenCells)->getDataValidation();
                            $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_CUSTOM);
                            $validation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP);
                            $validation->setAllowBlank(true);
                            $validation->setShowInputMessage(true);
                            $validation->setShowErrorMessage(true);
                            $validation->setErrorTitle('Requested Stock Over');
                            $validation->setError('Your Requested qty is more than stock, please change your qty.');
                            $validation->setFormula1('=IF(' . $cellStockFormula . ' = "OK", TRUE, FALSE)');
                        }
                    }

                    if ($value['assy_no'] && $value['job_no'] && $value['is_material'] == true) {
                        $this->changeCellColors($event->sheet, 'N' . ($startDataRow + $key), 'ffffff');
                        $this->lockUnlockCell($event->sheet, 'N' . ($startDataRow + $key), false);

                        $event->sheet->getDelegate()->setCellValue('N' . ($startDataRow + $key), $value['MATERIAL']);

                        $validation = $this->setValidation($event->sheet, 'N' . ($startDataRow + $key), \PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_WHOLE, 'Please input number only !');

                        // $validation = $event->sheet->getCell($valueOpenCells)->getDataValidation();
                        $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_CUSTOM);
                        $validation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP);
                        $validation->setAllowBlank(true);
                        $validation->setShowInputMessage(true);
                        $validation->setShowErrorMessage(true);
                        $validation->setErrorTitle('Requested Stock Over');
                        $validation->setError('Your Requested qty is more than stock, please change your qty.');
                        $validation->setFormula1('=IF(' . $cellStockFormula . ' = "OK", TRUE, FALSE)');
                    }

                    if (!empty($value['ref_no'])) {
                        $event->sheet->getDelegate()->setCellValue($this->toAlpha(14). ($startDataRow + $key), $value['ref_no']);
                    }

                    $this->listBuilder(
                        $event->sheet,
                        $this->toAlpha(3) . ($startDataRow + $key),
                        implode(',', $getMFG)
                    );
                }
            }
        ];
    }

    // ETC

    public function listBuilder($fun, $cell, $data)
    {
        $objValidation = $fun->getCell($cell)->getDataValidation();
        $objValidation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
        $objValidation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
        $objValidation->setAllowBlank(false);
        $objValidation->setShowInputMessage(true);
        $objValidation->setShowErrorMessage(true);
        $objValidation->setShowDropDown(true);
        $objValidation->setErrorTitle('Input error');
        $objValidation->setError('Value is not in list.');
        $objValidation->setPromptTitle('Pick from list');
        $objValidation->setPrompt('Please pick a value from the drop-down list.');
        $objValidation->setFormula1('"' . $data . '"');

        return $objValidation;
    }

    public function changeCellColors($fun, $cell, $color)
    {
        return $fun->getStyle($cell)
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB($color);
    }

    public function lockUnlockCell($fun, $cell, $lock = true)
    {
        return $lock
            ? $fun->getStyle($cell)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_PROTECTED)
            : $fun->getStyle($cell)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
    }

    public function setValidation($fun, $cells, $type, $msg)
    {
        $validation = $fun->getCell($cells)->getDataValidation();
        $validation->setType($type);
        $validation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP);
        $validation->setAllowBlank(true);
        $validation->setShowInputMessage(true);
        $validation->setShowErrorMessage(true);
        $validation->setErrorTitle('Input error');
        $validation->setError($msg);
        $validation->setPromptTitle('Allowed input');

        return $validation;
    }
}
