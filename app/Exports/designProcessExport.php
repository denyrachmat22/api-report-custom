<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use App\Model\WMS\DESIGNPROC\masterProcess;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;

class designProcessExport implements FromCollection, WithHeadings, WithEvents, WithStartRow
{
    use Exportable;
    use RegistersEventListeners;

    private $data;
    private $mfg;

    /**
     * @return \Illuminate\Support\Collection
     */
    public function __construct($data, $mfg)
    {
        $this->data = $data;
        $this->mfg = $mfg;
        $this->headerList = masterProcess::where('PROCMSTR_MFGCD', $this->mfg)->orderBy('PROCMSTR_HEADSEQ')->get()->toArray();
    }

    // public function title(): string
    // {
    //     return 'Summary Design Process ' . $this->mfg;
    // }

    public function headings(): array
    {
        $getHeader = $this->headerList;

        $dataHeader = [];
        $dataLineCT = [];
        foreach ($getHeader as $key => $value) {
            $dataHeader[] = $value['PROCMSTR_PROCD'];
            if ($value['PROCMSTR_ISLINE'] == 1 && $value['PROCMSTR_ISCT'] == 1) {
                $dataHeader[] = '';

                $dataLineCT[] = 'Line';
                $dataLineCT[] = 'CT';
            } else {
                $dataLineCT[] = '';
            }
        }

        $listProcess = [];
        $headProcess = [];
        for ($i = 0; $i < 4; $i++) {
            if ($i === 0) {
                $headProcess[] = 'PROCESS';
            } else {
                $headProcess[] = '';
            }

            $listProcess[] = '#' . ($i + 1);
        }

        return [
            [],
            ['Summary Design Process ' . $this->mfg],
            ['ID', $this->mfg],
            [],
            [],
            [],
            ['PT. SMT INDONESIA'],
            array_merge(
                [
                    'S/N',
                    'MODEL',
                    'ASSY CODE',
                    'CODE'
                ],
                $headProcess,
                $dataHeader
            ),
            array_merge(
                [
                    '',
                    '',
                    '',
                    ''
                ],
                $listProcess,
                $dataLineCT
            )
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $hasil = [];
        for ($i = 0; $i < 9; $i++) {
            $hasil[] = [''];
        }

        foreach ($this->data as $key => $value) {
            // Get Process 1 - 4
            $filterProcess = array_filter($value['processUpdate'], function ($f) {
                return $f['PROCMSTR_ISPROCESS'] == 1;
            });

            $proc = [];
            foreach ($filterProcess as $keyProc => $valueProc) {
                $proc['PROCESS' . ($keyProc)] = $valueProc['PROCMSTR_PROCD'];
            }

            for ($i = 0; $i < 4 - count($filterProcess); $i++) {
                $proc['PROCESS' . (count($filterProcess) + $i)] = '';
            }

            // Fetch based on line & CT


            $hasil[] = array_merge(
                [
                    'no' => $value['no'],
                    'PROCDET_MDLCD' => $value['PROCDET_MDLCD'],
                    'MITM_ITMD1' => $value['MITM_ITMD1'],
                    'PROCDET_CD' => $value['PROCDET_CD'],
                ],
                $proc,
                $this->dataProcDet($value['processUpdate'])
            );
        }

        logger($hasil);

        return collect($hasil);
    }

    public function startRow(): int
    {
        return 10;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getDelegate()->getPageSetup()
                    ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE)
                    ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A3);

                $event->sheet->getStyle('A1:A3')->applyFromArray([
                    'font' => [
                        'size' => '18',
                        'bold' => true
                    ]
                ]);

                $highestRow = $event->sheet->getHighestRow();
                $highestColumn = $event->sheet->getHighestColumn();

                $event->sheet->getStyle('A8:'.$highestColumn.'9')->applyFromArray([
                    'font' => [
                        'size' => '12',
                        'bold' => true
                    ]
                ]);

                $event->sheet->styleCells(
                    'A8:' . $highestColumn . $highestRow,
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],
                        ]
                    ]
                );

                $event->sheet->getStyle('A8:' . $highestColumn . '9')
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('ffffff');

                foreach (range('A', $highestColumn) as $columnID) {
                    $event->sheet->getColumnDimension($columnID)->setAutoSize(true);
                }

                $event->sheet->getStyle('A8:' . $highestColumn . '9')->getAlignment()->setHorizontal('center');

                $event->sheet->getDelegate()->mergeCells('A8:A9');
                $event->sheet->getDelegate()->mergeCells('B8:B9');
                $event->sheet->getDelegate()->mergeCells('C8:C9');
                $event->sheet->getDelegate()->mergeCells('D8:D9');

                $event->sheet->getDelegate()->mergeCells('E8:H8');

                $startCount = 8;
                foreach ($this->headerList as $keyHeader => $valueHeader) {
                    if ($valueHeader['PROCMSTR_ISLINE'] == 1 && $valueHeader['PROCMSTR_ISCT'] == 1) {
                        $event->sheet->getDelegate()->mergeCells($this->toAlpha($startCount + $keyHeader) . '8:' . $this->toAlpha($startCount + $keyHeader + 1) . '8');
                        $startCount = $startCount + 1;
                    } else {
                        $event->sheet->getDelegate()->mergeCells($this->toAlpha($startCount + $keyHeader) . '8:' . $this->toAlpha($startCount + $keyHeader) . '9');
                    }
                }


                // $total = 0;
                // for ($i=0; $i < 4; $i++) { 
                //     $event->sheet->getDelegate()->mergeCells($this->toAlpha(5 + $i).'8:D9');
                // }
            }
        ];
    }

    public function toAlpha($num)
    {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return $this->toAlpha($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }

    public function dataProcDet($processUpdate)
    {
        $contentProc = [];
        foreach ($this->headerList as $keyHeader => $valueHeader) {
            $filterContent = array_values(array_filter($processUpdate, function ($f2) use ($valueHeader) {
                return $f2['PROCMSTR_PROCD'] === $valueHeader['PROCMSTR_PROCD'];
            }));

            if ($valueHeader['PROCMSTR_ISLINE'] == 1 && $valueHeader['PROCMSTR_ISCT'] == 1) {
                if (count($filterContent) > 0) {
                    $contentProc[$valueHeader['PROCMSTR_PROCD'] . '_LINE'] = $filterContent[0]['PROCDET_LINE'];
                    $contentProc[$valueHeader['PROCMSTR_PROCD'] . '_CT'] = $filterContent[0]['PROCDET_CT'];
                } else {
                    $contentProc[$valueHeader['PROCMSTR_PROCD'] . '_LINE'] = '';
                    $contentProc[$valueHeader['PROCMSTR_PROCD'] . '_CT'] = '';
                }
            } else {
                if (count($filterContent) > 0) {
                    $contentProc[$valueHeader['PROCMSTR_PROCD'] . '_LINE'] = $filterContent[0]['PROCDET_LINE'];
                } else {
                    $contentProc[$valueHeader['PROCMSTR_PROCD'] . '_LINE'] = '';
                }
            }
        }

        return $contentProc;
    }
}
