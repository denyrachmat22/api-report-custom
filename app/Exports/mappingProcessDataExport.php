<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
// use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Model\RPCUST\processMstr;

class mappingProcessDataExport implements WithHeadings, WithEvents
{
    use Exportable;
    use RegistersEventListeners;
    /**
     * @return \Illuminate\Support\Collection
     */
    public function headings(): array
    {
        $processnya = processMstr::get()->toArray();
        $isikosong = [];

        for ($i = 0; $i < count($processnya); $i++) {
            $isikosong[] = [];
        }

        $isi = [];
        foreach ($processnya as $key => $value) {
            $isi[] = $value['RPDSGN_CODE'];
        }

        $headerCombine = array_merge([
            'NO',
            'MODEL',
            'ASSY CODE',
            'CODE',
            'PROCESS'
        ], $isikosong);

        return [
            ['PT SMT INDONESIA'],
            ['Process Name :'],
            [],
            [],
            [],
            [],
            [],
            $headerCombine,
            array_merge([
                '',
                '',
                '',
                ''
            ], $isi)
        ];
    }

    public function toAlpha($num)
    {
        return chr(substr("000" . ($num + 65), -3));
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // $event->sheet->getActiveSheet()->setPrintGridlines(false);
                $event->sheet->getDelegate()->getPageSetup()
                    ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE)
                    ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A3);

                $event->sheet->getStyle('A1')->applyFromArray([
                    'font' => [
                        'size' => '15',
                        'bold' => true
                    ]
                ]);

                $highestRow = $event->sheet->getHighestRow();
                $highestColumn = $event->sheet->getHighestColumn();

                $event->sheet->styleCells(
                    'A8:' . $highestColumn . $highestRow,
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],
                        ]
                    ]
                );

                $event->sheet->styleCells(
                    'C2',
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],
                        ]
                    ]
                );

                $event->sheet->getStyle('A8:'.$this->toAlpha(processMstr::count() + 3).'9')->getAlignment()->setHorizontal('center');

                $event->sheet->getDelegate()->mergeCells('A1:D1');
                $event->sheet->getDelegate()->mergeCells('A2:B2');
                $event->sheet->getDelegate()->mergeCells('A8:A9');
                $event->sheet->getDelegate()->mergeCells('B8:B9');
                $event->sheet->getDelegate()->mergeCells('C8:C9');
                $event->sheet->getDelegate()->mergeCells('D8:D9');
                $event->sheet->getDelegate()->mergeCells('E8:'.$this->toAlpha(processMstr::count() + 3).'8');
            },
        ];
    }
}
