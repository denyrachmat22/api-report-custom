<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\scrapReportComponentExport;
use App\Exports\scrapReportMegaTemplateExport;
class scrapReportCompFrontExport implements WithMultipleSheets
{
    private $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function sheets(): array
    {
        if ($this->data[0]['IS_CONFIRMED'] == 1) {
            return [
                'Scrap Component Detail' => new scrapReportComponentExport($this->data),
                'Upload Mega Template' => new scrapReportMegaTemplateExport($this->data),
            ];
        } else {
            return [
                'Scrap Component Detail' => new scrapReportComponentExport($this->data),
            ];}
    }
}
