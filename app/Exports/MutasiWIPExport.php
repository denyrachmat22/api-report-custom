<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MutasiWIPExport implements FromCollection, WithHeadings, WithEvents, WithStartRow
{
    use Exportable;
    use RegistersEventListeners;

    private $data;
    private $period;
    private $format;
    public function __construct($data,$period,$format)
    {
        $this->data = $data;
        $this->period = $period;
        $this->format = $format;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $hasil = [];
        foreach ($this->data as $key => $value) {
            $hasil[] = (object) array_merge(
                ['NO' => $key+1],
                ['RPWIP_ITMCOD' => $value['RPWIP_ITMCOD']],
                ['MITM_ITMD1' => $value['MITM_ITMD1']],
                ['MITM_STKUOM' => $value['MITM_STKUOM']],
                ['QTY' => number_format($value['QTY'])]
            );
        }
        return collect($hasil);
    }

    public function headings(): array
    {
        return [
            ['LAPORAN SALDO WIP'],
            ['PT SMT INDONESIA'],
            ['PERIODE S.D '.$this->period[1]],
            [],
            [
                'NO',
                'KODE BARANG',
                'NAMA BARANG',
                'SATUAN',
                'SALDO AKHIR'
            ]
         ];
    }

    public function startRow() : int
    {
        return 6;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // $event->sheet->getActiveSheet()->setPrintGridlines(false);
                $event->sheet->getDelegate()->getPageSetup()
                    ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE)
                    ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A3);

                $event->sheet->getStyle('A1:A3')->applyFromArray([
                    'font' => [
                        'size' => '15',
                        'bold' => true
                    ]
                ]);

                $highestRow = $event->sheet->getHighestRow();
                $highestColumn = $event->sheet->getHighestColumn();

                if ($this->format === 'excel') {
                    foreach(range('A', $highestColumn) as $columnID) {
                        $event->sheet->getColumnDimension($columnID)->setAutoSize(true) ;
                    }
                }

                $event->sheet->styleCells(
                    'A5:'.$highestColumn.$highestRow,
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],
                        ]
                    ]
                );

                $event->sheet->getStyle('A5:E5')
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('ffffff');

                $event->sheet->getStyle('A5:E5')->getAlignment()->setHorizontal('center');

                $event->sheet->getDelegate()->mergeCells('A1:E1');
                $event->sheet->getDelegate()->mergeCells('A2:E2');
                $event->sheet->getDelegate()->mergeCells('A3:E3');
                $event->sheet->getDelegate()->mergeCells('A4:E4');

                $event->sheet->getStyle('B5:B'.$highestRow)->getAlignment()->setHorizontal('left');
                $event->sheet->getStyle('C5:C'.$highestRow)->getAlignment()->setHorizontal('left');
                $event->sheet->getStyle('E5:E'.$highestRow)->getAlignment()->setHorizontal('right');
            },
        ];
    }
}
