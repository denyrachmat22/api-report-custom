<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ITInventoryCheckerExport implements FromCollection, WithHeadings, WithEvents, WithStartRow
{
    use Exportable;
    use RegistersEventListeners;

    private $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            'Item Number',
            'Item Desc',
            'Warehouse',
            'Saldo Awal',
            'Pemasukan',
            'Pengeluaran',
            'Saldo Buku',
            'Stock Opname',
            'Selisih'
        ];
    }
}
