<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;

class scrapReportMegaTemplateExport implements FromCollection, WithTitle
{
    private $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function title(): string
    {
        return 'Upload Mega Template';
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $hasil = [
            [
                'Item Code',
                'Transfer Qty',
            ]
        ];

        $start = 1;

        $temp = [];
        $tot = 0;
        foreach ($this->data as $key => $value) {
            foreach ($value['hist_det'] as $keyDet => $valueDet) {
                $temp[trim($valueDet['SCR_ITMCD'])][$tot] = $valueDet['SCR_QTY'];
                $tot++;
            }
        }

        foreach ($temp as $keyData => $valueData) {
            $total = 0;
            foreach ($valueData as $keyTot => $valueTot) {
                $total += $valueTot;
            }

            $hasil[$start] = [
                $keyData,
                $total
            ];

            $start++;
        }

        return collect($hasil);
    }
}
