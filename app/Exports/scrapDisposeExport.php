<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class scrapDisposeExport implements FromCollection, WithHeadings, WithStartRow, WithEvents, WithTitle
{
    private $data, $id, $type, $listTransID;
    public function __construct($data, $id, $type, $listTransID)
    {
        $this->data = $data;
        $this->id = $id;
        $this->type = $type;
        $this->listTransID = $listTransID;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $hasil = [];

        $no = 0;
        foreach ($this->data as $key => $value) {
            if ($this->type === 1) {
                $hasil[] = [
                    $key > 0 && $value['DOC_NO'] === $this->data[$key - 1]['DOC_NO'] ? '' : $no + 1,
                    // $key > 0 && $value['DOC_NO'] === $this->data[$key - 1]['DOC_NO'] ? '' : $value['DOC_NO'],
                    $key > 0 && $value['id'] === $this->data[$key - 1]['id'] ? '' : $value['ITEMNUM'],
                    $key > 0 && $value['id'] === $this->data[$key - 1]['id'] ? '' : $value['MITM_ITMD1'],
                    $key > 0 && $value['id'] === $this->data[$key - 1]['id'] ? '' : (int)$value['QTY'],
                    $key > 0 && $value['id'] === $this->data[$key - 1]['id'] ? '' : $value['MITM_STKUOM'],
                    $key > 0 && $value['id'] === $this->data[$key - 1]['id'] ? '' : $value['TOTNW'],
                    $key > 0 && $value['id'] === $this->data[$key - 1]['id'] ? '' : $value['QTY'] * $value['TOTNW'],
                    '',
                    $value['RPSTOCK_BCTYPE'],
                    $value['RPSTOCK_NOAJU'],
                    $value['RPSTOCK_BCNUM'],
                    $value['RPSTOCK_BCDATE'],
                    $value['RCV_ZNOURUT'],
                    $value['SCR_ITMCD'],
                    trim($value['MITM_ITMD12']),
                    (int) $value['SCR_QTY'],
                    $value['MITM_STKUOM2'],
                    $value['RCV_PRPRC'],
                    $value['RPSTOCK_QTY'] * $value['RCV_PRPRC'],
                    // $value['SCR_QTPER'],
                    // // Date::dateTimeToExcel(date_create($value['RPSTOCK_BCDATE'])),
                    // // Date::PHPToExcel(\DateTime::createFromFormat('Y/m/d h:i A', $value['RPSTOCK_BCDATE'])),
                    // $value['RCV_HSCD'],
                    // $value['RCV_BM'],
                    // $value['RCV_PPN'],
                    // $value['RCV_PPH'],
                ];

                if ($key > 0 && ($value['DOC_NO'] !== $this->data[$key - 1]['DOC_NO'])) {
                    $no++;
                }
            } else {
                $hasil[] = [
                    $no + 1,
                    // $value['DOC_NO'],
                    $value['MITM_ITMD1'],
                    $value['ITEMNUM'],
                    empty($value['RPSTOCK_NOAJU']) ? $value['QTY'] : $value['RPSTOCK_QTY'],
                    $value['MITM_STKUOM'],
                    $value['RCV_HSCD'],
                    $value['RCV_BM'],
                    $value['RCV_PPN'],
                    $value['RCV_PPH'],
                    $value['RCV_PRNW'],
                    $value['RPSTOCK_NOAJU'],
                    $value['RPSTOCK_BCTYPE'],
                    $value['RPSTOCK_BCDATE'],
                    $value['RPSTOCK_BCNUM'],
                    $value['RCV_ZNOURUT'],
                    $value['RCV_PRPRC'],
                    $value['RPSTOCK_QTY'] * $value['RCV_PRPRC'],
                ];
                if ($key > 0 && $value['ITEMNUM'] !== $this->data[$key - 1]['ITEMNUM']) {
                    $no++;
                }
            }

            // $no++;
        }

        return collect($hasil);
    }

    public function title(): string
    {
        return $this->type === 1 ? 'FINISH GOOD NG PRODUKSI' : 'BAHAN BAKU NG';
    }

    public function headings(): array
    {
        if ($this->type === 1) {
            $header = [
                'No',
                'Kode Barang',
                'Nama Barang',
                'Qty',
                'Satuan',
                'Berat',
                'Berat (Kg)',
                'No Scan EX BC',
                'BC Type',
                'No Aju',
                'Nopen',
                'Tanggal',
                'No Barang',
                'Part Code',
                'Part Name',
                'Qty',
                'Satuan',
                'Price',
                'Total Price',
            ];
        } else {
            $header = [
                'No',
                'Nama Barang',
                'Kode Barang',
                'Qty',
                'Satuan',
                'HS Code',
                'BM',
                'PPN',
                'PPH',
                'Berat (Gram)',
                'EX BC (AJU)',
                'Type BC',
                'Tanggal BC',
                'Dokumen Asal',
                'No Urut',
                'Price',
                'Total Price',
                // 'Remarks',
            ];
        }

        return [
            [$this->type === 1 ? 'FINISH GOOD NG PRODUKSI' : 'BAHAN BAKU NG'],
            ['Dispose Doc', '', ': ' . $this->id],
            ['Scrap Doc', '', ': ' . implode(', ', $this->listTransID)],
            $header
        ];
    }

    public function startRow(): int
    {
        return 5;
    }

    public function toAlpha($num)
    {
        $tot = $num + 65;

        return chr(substr('000' . $tot, -3));
    }

    public function columnFormats(): array
    {
        return [
            'M' => NumberFormat::FORMAT_DATE_YYYYMMDDSLASH,
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // $event->sheet->getActiveSheet()->setPrintGridlines(false);
                $event->sheet->getDelegate()->getPageSetup()
                    ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE)
                    ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A3);

                $event->sheet->getStyle('A1:A1')->applyFromArray([
                    'font' => [
                        'size' => '15',
                        'bold' => true
                    ]
                ]);

                $event->sheet->getStyle('A5:F5')
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('ffffff');

                $highestRow = $event->sheet->getHighestRow();
                $highestColumn = $event->sheet->getHighestColumn();

                // $event->sheet->getStyle('L7:Q'.$highestRow)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);

                // if ($this->typeExp === 'excel') {
                //     foreach(range('A', $highestColumn) as $columnID) {
                //         $event->sheet->getColumnDimension($columnID)->setAutoSize(true) ;
                //     }
                // }


                $event->sheet->getDelegate()->mergeCells('A1:' . $highestColumn . '1');
                $event->sheet->getDelegate()->mergeCells('A2:B2');
                $event->sheet->getDelegate()->mergeCells('C2:' . $highestColumn . '2');
                $event->sheet->getDelegate()->mergeCells('A3:B3');
                $event->sheet->getDelegate()->mergeCells('C3:' . $highestColumn . '3');

                foreach (range('A', $highestColumn) as $columnID) {
                    $event->sheet->getColumnDimension($columnID)->setAutoSize(true);
                }

                $event->sheet->styleCells(
                    'A4:' . $highestColumn . $highestRow,
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],
                        ]
                    ]
                );

                // $event->sheet->styleCells(
                //     'F7:F'.$highestRow,
                //     [
                //         'numberFormat' => [
                //             'formatCode' => \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2
                //         ]
                //     ]
                // );

                $event->sheet->getStyle('A1:A1')->getAlignment()->setHorizontal('center');
                $event->sheet->getStyle('A4:' . $highestColumn . '4')->getAlignment()->setHorizontal('center');
            },
        ];
    }
}
