<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class WMSUserLogsExport implements FromCollection,WithStartRow,WithHeadings,WithEvents
{
    use Exportable;
    use RegistersEventListeners;

    private $data;
    private $period;
    private $typeExp;

    public function __construct($data,$period, $typeExp = 'excel')
    {
        $this->data = $data;
        $this->period = $period;
        $this->typeExp = $typeExp;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $hasil = [];
        foreach ($this->data as $key => $value) {
            $hasil[] = [
                'no' => $key + 1,
                'MSTEMP_ID'=> $value['MSTEMP_ID'],
                'FULLNAME'=> $value['FULLNAME'],
                'CSMLOG_DOCNO'=> $value['CSMLOG_DOCNO'],
                'CSMLOG_TYPE'=> $value['CSMLOG_TYPE'],
                'CSMLOG_DESC'=> $value['CSMLOG_DESC'],
                'CSMLOG_CREATED_AT'=> $value['CSMLOG_CREATED_AT']
            ];
        }
        return collect($hasil);
    }

    public function headings(): array
    {
        return [
            ['USER LOGS REPORT'],
            ['PT SMT INDONESIA'],
            ['PERIODE '.$this->period[0].' S.D '.$this->period[1]],
            [],
            [
                'NO',
                'USERNAME',
                'FULL NAME',
                'DOCUMENT ID',
                'TYPE',
                'ACTIVITY',
                'DATE'
            ]
         ];
    }

    public function startRow() : int
    {
        return 7;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // $event->sheet->getActiveSheet()->setPrintGridlines(false);
                $event->sheet->getDelegate()->getPageSetup()
                    ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE)
                    ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A3);

                $event->sheet->getStyle('A1:A3')->applyFromArray([
                    'font' => [
                        'size' => '15',
                        'bold' => true
                    ]
                ]);

                $event->sheet->getStyle('A5:G5')
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('ffffff');

                $highestRow = $event->sheet->getHighestRow();
                $highestColumn = $event->sheet->getHighestColumn();

                // $event->sheet->getStyle('L7:Q'.$highestRow)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);

                if ($this->typeExp === 'excel') {
                    foreach(range('A', $highestColumn) as $columnID) {
                        $event->sheet->getColumnDimension($columnID)->setAutoSize(true) ;
                    }
                }

                $event->sheet->styleCells(
                    'A5:'.$highestColumn.$highestRow,
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],
                        ]
                    ]
                );

                // $event->sheet->styleCells(
                //     'F7:F'.$highestRow,
                //     [
                //         'numberFormat' => [
                //             'formatCode' => \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2
                //         ]
                //     ]
                // );

                $event->sheet->getStyle('A5:G'.$highestRow)->getAlignment()->setHorizontal('center');
            },
        ];
    }
}
