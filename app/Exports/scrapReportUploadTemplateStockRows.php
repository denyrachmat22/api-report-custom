<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStartRow;

class scrapReportUploadTemplateStockRows implements FromCollection, WithHeadings, WithStartRow
{
    private $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $hasil = [];
        foreach ($this->data as $key => $value) {
            $hasil[] = [
                $key + 1,
                $value['tot_qty'],
                $value['ref_no']
            ];
        }

        return collect($hasil);
    }

    public function startRow(): int
    {
        return 2;
    }

    public function headings(): array
    {
        return [
            [
                'No',
                'Tot Qty',
                'Ref no'
            ]
        ];
    }
}
