<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;

class fourMDetailList implements WithTitle, FromCollection, WithHeadings, WithStartRow, WithEvents
{
    use Exportable;
    use RegistersEventListeners;

    private $data;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function title(): string
    {
        return 'Discrepancy BOM MEGA vs CIMS';
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $hasil = [];
        foreach ($this->data as $key => $value) {
            $hasil[] = [
                'NO' => $key + 1,
                'MITM_ITMCD' => $value->MITM_ITMCD,
                'MITM_ITMD1' => $value->MITM_ITMD1,
                'REV_CIMS' => $value->REV_CIMS,
                'REV_MEGA' => $value->REV_MEGA,
                'LINE_PROD' => $value->LINE_PROD
            ];
        }

        return collect($hasil);
    }

    public function headings(): array
    {
        return [
            ['BOM Rev Comparation'],
            ['PT SMT INDONESIA'],
            [],
            [
                'NO',
                'ITEM CODE',
                'ITEM DESC',
                'CIMS REV',
                'MEGA REV',
                'LINE PROD'
            ]
        ];
    }

    public function startRow() : int
    {
        return 5;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $highestRow = $event->sheet->getHighestRow();
                $highestColumn = $event->sheet->getHighestColumn();
                
                $event->sheet->getStyle('A1:A2')->applyFromArray([
                    'font' => [
                        'size' => '15',
                        'bold' => true
                    ]
                ]);

                $event->sheet->getStyle('A4:'.$highestColumn.'4')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                
                $event->sheet->styleCells(
                    'A4:'.$highestColumn.'4',
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],
                        ]
                    ]
                );

                $event->sheet->styleCells(
                    'A5:'.$highestColumn.$highestRow,
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],
                        ]
                    ]
                );

                $event->sheet->getStyle('A1:H4')->getAlignment()->setHorizontal('center');

                $event->sheet->getDelegate()->mergeCells('A1:'.$highestColumn.'1');
                $event->sheet->getDelegate()->mergeCells('A2:'.$highestColumn.'2');
            },
        ];
    }
}
