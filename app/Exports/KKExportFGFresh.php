<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

use Illuminate\Support\Facades\DB;
use App\Model\MASTER\ItemMaster;

class KKExportFGFresh implements FromCollection, WithHeadings, WithEvents
{
    use Exportable;
    use RegistersEventListeners;

    private $mutasi;
    private $fdate;
    private $ldate;
    private $period;
    private $item;

    public function __construct($mutasi, $fdate, $ldate, $period, $item = '')
    {
        $this->mutasi = $mutasi;
        $this->fdate = $fdate;
        $this->ldate = $ldate;
        $this->period = $period;
        $this->item = $item;

        $time_start_kka = microtime(true);
        $this->dataKKA = $this->getData($fdate, $ldate);
        // $this->dataKKA = [];
        $time_end_kka = microtime(true);
        $execution_time_kka = ($time_end_kka - $time_start_kka) / 60;

        logger('<b>Total Execution Time KKA:</b> ' . $execution_time_kka . ' Mins');

        $time_start_kka_date = microtime(true);
        $this->dataForheader = $this->getDateData();
        $time_end_kka_date = microtime(true);
        $execution_time_kka_date = ($time_end_kka_date - $time_start_kka_date) / 60;

        logger('<b>Total Execution Time KKA Date:</b> ' . $execution_time_kka_date . ' Mins');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // return collect([]);
        $hasil = [];
        $hasilTotal = [];
        foreach ($this->dataForheader as $key => $value) {
            $searchData = array_filter($this->dataKKA, function ($f) use ($value) {
                if ($f['period_iter'] === $value['period_iter']) {
                    return $f;
                }
            });

            if (count($this->dataForheader) === 1) {
                foreach ($searchData as $keySearch => $valueSearch) {
                    $hasil[] = [
                        trim($valueSearch['MITM_ITMCD']),
                        trim($valueSearch['MITM_ITMD1']),
                        $valueSearch['MITM_STKUOM'],
                        !empty($valueSearch['begin_stock']) ? $valueSearch['begin_stock'] : 0,
                        !empty($valueSearch['inc_opt_1_stock']) ? $valueSearch['inc_opt_1_stock'] : 0,
                        !empty($valueSearch['inc_opt_2_stock']) ? $valueSearch['inc_opt_2_stock'] : 0,
                        !empty($valueSearch['inc_stock']) ? $valueSearch['inc_stock'] : 0,
                        !empty($valueSearch['out_opt_1_stock']) ? $valueSearch['out_opt_1_stock'] : 0,
                        !empty($valueSearch['out_opt_2_stock']) ? $valueSearch['out_opt_2_stock'] : 0,
                        !empty($valueSearch['out_opt_4_stock']) ? $valueSearch['out_opt_4_stock'] : 0,
                        !empty($valueSearch['out_stock']) ? $valueSearch['out_stock'] : 0,
                        !empty($valueSearch['disc_book_stock']) ? $valueSearch['disc_book_stock'] : 0,
                        !empty($valueSearch['phy_stock']) ? $valueSearch['phy_stock'] : 0,
                        !empty($valueSearch['tarik_mundur_inc']) ? $valueSearch['tarik_mundur_inc'] : 0,
                        !empty($valueSearch['tarik_mundur_out']) ? $valueSearch['tarik_mundur_out'] : 0,
                        !empty($valueSearch['tot_saldo_phy']) ? $valueSearch['tot_saldo_phy'] : 0,
                        !empty($valueSearch['opn_stock']) ? $valueSearch['opn_stock'] : 0,
                        !empty($valueSearch['total_stock']) ? $valueSearch['total_stock'] : 0,
                        (!empty($valueSearch['total_stock']) ? $valueSearch['total_stock'] : 0) != 0 ? 'SELISIH' : 'SESUAI',
                    ];
                }
            } else {
                // If Tanggal periode pertama
                if ($key === 0) {
                    if (count($searchData) > 0) {
                        foreach ($searchData as $keySearch => $valueSearch) {
                            $hasil[] = [
                                trim($valueSearch['MITM_ITMCD']),
                                trim($valueSearch['MITM_ITMD1']),
                                $valueSearch['MITM_STKUOM'],
                                !empty($valueSearch['begin_stock']) ? $valueSearch['begin_stock'] : 0,
                                !empty($valueSearch['inc_opt_1_stock']) ? $valueSearch['inc_opt_1_stock'] : 0,
                                !empty($valueSearch['inc_opt_2_stock']) ? $valueSearch['inc_opt_2_stock'] : 0,
                                !empty($valueSearch['inc_stock']) ? $valueSearch['inc_stock'] : 0,
                                !empty($valueSearch['out_opt_1_stock']) ? $valueSearch['out_opt_1_stock'] : 0,
                                !empty($valueSearch['out_opt_2_stock']) ? $valueSearch['out_opt_2_stock'] : 0,
                                !empty($valueSearch['out_opt_4_stock']) ? $valueSearch['out_opt_4_stock'] : 0,
                                !empty($valueSearch['out_stock']) ? $valueSearch['out_stock'] : 0,
                                // !empty($valueSearch['disc_book_stock']) ? $valueSearch['disc_book_stock'] : 0,
                                // !empty($valueSearch['opn_stock']) ? $valueSearch['opn_stock'] : 0,
                                // !empty($valueSearch['tarik_mundur_inc']) ? $valueSearch['tarik_mundur_inc'] : 0,
                                // !empty($valueSearch['tarik_mundur_out']) ? $valueSearch['tarik_mundur_out'] : 0,
                                // 0,
                                // !empty($valueSearch['total_stock']) ? $valueSearch['total_stock'] : 0
                            ];

                            $hasilTotal[] = [
                                'item' => trim($valueSearch['MITM_ITMCD']),
                                'inc_stock' => $valueSearch['inc_stock'],
                                'out_stock' => $valueSearch['out_stock'],
                                'opn_stock' => $valueSearch['opn_stock'],
                                'stock_awal' => !empty($valueSearch['begin_stock']) ? $valueSearch['begin_stock'] : 0
                            ];
                        }
                    }
                } elseif ($key === count($this->dataForheader) - 1) { // If Tanggal periode terakhir
                    $firstData = $hasil;
                    if (count($searchData) > 0) {
                        foreach ($searchData as $keySearch => $valueSearch) {
                            foreach ($firstData as $keyFD => $valueFD) {
                                if (trim($valueFD[0]) === trim($valueSearch['MITM_ITMCD'])) {
                                    $totalBookStock = ((int)$hasilTotal[$keyFD]['stock_awal'] + (int)$hasilTotal[$keyFD]['inc_stock'] + (!empty($valueSearch['inc_stock']) ? $valueSearch['inc_stock'] : 0)) - ((int)$hasilTotal[$keyFD]['out_stock'] + (!empty($valueSearch['out_stock']) ? $valueSearch['out_stock'] : 0));

                                    $hasil[$keyFD] = array_merge(
                                        $hasil[$keyFD],
                                        [
                                            !empty($valueSearch['begin_stock']) ? $valueSearch['begin_stock'] : 0,
                                            !empty($valueSearch['inc_opt_1_stock']) ? $valueSearch['inc_opt_1_stock'] : 0,
                                            !empty($valueSearch['inc_opt_2_stock']) ? $valueSearch['inc_opt_2_stock'] : 0,
                                            !empty($valueSearch['inc_stock']) ? $valueSearch['inc_stock'] : 0,
                                            !empty($valueSearch['out_opt_1_stock']) ? $valueSearch['out_opt_1_stock'] : 0,
                                            !empty($valueSearch['out_opt_2_stock']) ? $valueSearch['out_opt_2_stock'] : 0,
                                            !empty($valueSearch['out_opt_4_stock']) ? $valueSearch['out_opt_4_stock'] : 0,
                                            !empty($valueSearch['out_stock']) ? $valueSearch['out_stock'] : 0,
                                            (int)$totalBookStock,
                                            !empty($valueSearch['opn_stock']) ? $valueSearch['opn_stock'] : $hasilTotal[$keyFD]['opn_stock'],
                                            !empty($valueSearch['tarik_mundur_inc']) ? $valueSearch['tarik_mundur_inc'] : 0,
                                            !empty($valueSearch['tarik_mundur_out']) ? $valueSearch['tarik_mundur_out'] : 0,
                                            !empty($valueSearch['tot_saldo_phy']) ? $valueSearch['tot_saldo_phy'] : 0,
                                            !empty($valueSearch['opn_stock']) ? $valueSearch['opn_stock'] : 0,
                                            !empty($valueSearch['total_stock']) ? $valueSearch['total_stock'] : 0,
                                            (!empty($valueSearch['total_stock']) ? $valueSearch['total_stock'] : 0) != 0 ? 'SELISIH' : 'SESUAI',
                                        ]
                                    );
                                }
                            }
                        }
                    }
                } else { // If Tanggal periode tengah
                    $firstData = $hasil;
                    if (count($searchData) > 0) {
                        foreach ($searchData as $keySearch => $valueSearch) {
                            foreach ($firstData as $keyFD => $valueFD) {
                                if (trim($valueFD[0]) === trim($valueSearch['MITM_ITMCD'])) {
                                    $hasil[$keyFD] = array_merge(
                                        $hasil[$keyFD],
                                        [
                                            !empty($valueSearch['begin_stock']) ? $valueSearch['begin_stock'] : 0,
                                            !empty($valueSearch['inc_opt_1_stock']) ? $valueSearch['inc_opt_1_stock'] : 0,
                                            !empty($valueSearch['inc_opt_2_stock']) ? $valueSearch['inc_opt_2_stock'] : 0,
                                            !empty($valueSearch['inc_stock']) ? $valueSearch['inc_stock'] : 0,
                                            !empty($valueSearch['out_opt_1_stock']) ? $valueSearch['out_opt_1_stock'] : 0,
                                            !empty($valueSearch['out_opt_2_stock']) ? $valueSearch['out_opt_2_stock'] : 0,
                                            !empty($valueSearch['out_opt_4_stock']) ? $valueSearch['out_opt_4_stock'] : 0,
                                            !empty($valueSearch['out_stock']) ? $valueSearch['out_stock'] : 0
                                        ]
                                    );

                                    $hasilTotal[$keyFD]['inc_stock'] = (int)$hasilTotal[$keyFD]['inc_stock'] + (!empty($valueSearch['inc_stock']) ? $valueSearch['inc_stock'] : 0);
                                    $hasilTotal[$keyFD]['out_stock'] = (int)$hasilTotal[$keyFD]['out_stock'] + (!empty($valueSearch['out_stock']) ? $valueSearch['out_stock'] : 0);

                                    if (!empty($valueSearch['opn_stock'])) {
                                        $hasilTotal[$keyFD]['opn_stock'] = (int)$valueSearch['opn_stock'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // logger($hasil);

        return collect($hasil);
    }

    public function headings(): array
    {
        $headernya = [];
        $countHeader = 0;
        foreach ($this->dataForheader as $key => $value) {
            for ($i = 0; $i < 8; $i++) {
                if ($i === 0) {
                    $headernya[($key + 1) * $countHeader] = 'Periode Bulan ' . $value['f_month'] . ' (' . $value['f_year'] . ') - ' . $value['l_month'] . ' (' . $value['l_year'] . ')';
                } else {
                    $headernya[($key + 1) * $countHeader] = '';
                }

                $countHeader++;
            }
        }

        $headerMutasi = ['', '', ''];
        foreach ($this->dataForheader as $key2 => $value2) {
            array_push(
                $headerMutasi,
                'Saldo Awal',
                'Pemasukan',
                'Adj In',
                'Total Pemasukan',
                'Pengeluaran',
                'Adj Out',
                'Pemusnahan',
                'Total Pengeluaran'
            );
        }

        $hasil = [
            array_merge(
                [
                    'Item Code',
                    'Item Desc',
                    'Satuan',
                ],
                $headernya,
                [
                    'Saldo Buku',
                    'Saldo Fisik',
                    'Tarik Mundur',
                    '',
                    'Stock Fisik',
                    'Stock Opname',
                    'Selisih',
                    'Keterangan'
                ]
            ),
            array_merge(
                $headerMutasi,
                [
                    '',
                    '',
                    'In',
                    'Out'
                ]
            )
        ];

        // logger(json_encode($hasil));

        return $hasil;
    }

    public function registerEvents(): array
    {
        ini_set('memory_limit', '-1');
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // $event->sheet->getActiveSheet()->setPrintGridlines(false);
                $event->sheet->getDelegate()->getPageSetup()
                    ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE)
                    ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A3);

                $highestRow = $event->sheet->getHighestRow();
                $highestColumn = $event->sheet->getHighestColumn();

                $event->sheet->getStyle('A1:' . $highestColumn . '2')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);

                foreach (range('A', $highestColumn) as $columnID) {
                    $event->sheet->getColumnDimension($columnID)->setAutoSize(true);
                }

                $event->sheet->styleCells(
                    'A1:' . $highestColumn . $highestRow,
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],
                        ]
                    ]
                );

                $event->sheet->getStyle('A1:' . $highestColumn . '2')->getAlignment()->setHorizontal('center');

                $event->sheet->getDelegate()->mergeCells('A1:A2');
                $event->sheet->getDelegate()->mergeCells('B1:B2');
                $event->sheet->getDelegate()->mergeCells('C1:C2');

                $countHeader = 4;
                foreach ($this->dataForheader as $key => $value) {
                    for ($i = 0; $i < 8; $i++) {
                        if ($i === 0) {
                            logger([$this->toAlpha($countHeader), $this->toAlpha($countHeader + 7)]);
                            $event->sheet->getDelegate()->mergeCells($this->toAlpha($countHeader) . '1:' . $this->toAlpha($countHeader + 7) . '1');
                        }

                        if ($i > 0 && $i <= 3) {
                            $event->sheet->getStyle($this->toAlpha($countHeader) . '2')
                                ->getFill()
                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                ->getStartColor()
                                ->setRGB('75B6FF');
                        } elseif ($i > 3 && $i <= 7) {
                            $event->sheet->getStyle($this->toAlpha($countHeader) . '2')
                                ->getFill()
                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                ->getStartColor()
                                ->setRGB('FF9494');
                        }

                        $countHeader++;
                    }
                }

                $event->sheet->getDelegate()->mergeCells(($this->toAlpha($countHeader + 0)) . '1:' . ($this->toAlpha($countHeader + 0)) . '2');
                $event->sheet->getDelegate()->mergeCells(($this->toAlpha($countHeader + 1)) . '1:' . ($this->toAlpha($countHeader + 1)) . '2');
                $event->sheet->getDelegate()->mergeCells(($this->toAlpha($countHeader + 2)) . '1:' . ($this->toAlpha($countHeader + 3)) . '1');
                $event->sheet->getDelegate()->mergeCells(($this->toAlpha($countHeader + 4)) . '1:' . ($this->toAlpha($countHeader + 4)) . '2');
                $event->sheet->getDelegate()->mergeCells(($this->toAlpha($countHeader + 5)) . '1:' . ($this->toAlpha($countHeader + 5)) . '2');
                $event->sheet->getDelegate()->mergeCells(($this->toAlpha($countHeader + 6)) . '1:' . ($this->toAlpha($countHeader + 6)) . '2');
            },
        ];
    }

    // Data
    public function toAlpha($c)
    {
        $c = intval($c);
        if ($c <= 0) return '';

        $letter = '';

        while ($c != 0) {
            $p = ($c - 1) % 26;
            $c = intval(($c - $p) / 26);
            $letter = chr(65 + $p) . $letter;
        }

        return $letter;
    }

    public function getData($fdate, $ldate)
    {
        $queryString = "
            SELECT
                MITM_ITMCD,
                MITM_ITMD1,
                MITM_STKUOM,
                MD.period_iter,
                MD.first_date,
                MD.last_date,
                MD.begin_stock,
                MD.inc_stock,
                MD.inc_opt_1_stock,
                MD.inc_opt_2_stock,
                MD.inc_opt_3_stock,
                MD.out_stock,
                MD.out_opt_1_stock,
                MD.out_opt_2_stock,
                MD.out_opt_3_stock,
                MD.out_opt_4_stock,
                MD.disc_book_stock,
                MD.opn_stock,
                MD.opn_stock_date,
                MD.phy_stock,
                MD.phy_stock_date,
                MD.total_stock,
                MD.tarik_mundur_inc,
                MD.tarik_mundur_out,
                MD.tot_saldo_phy
            FROM PSI_WMS.dbo.MITM_TBL mt
            LEFT JOIN (
                SELECT * FROM PSI_RPCUST.dbo.f_kka_generator_2('" . $this->mutasi . "', " . $this->period . ", '" . $fdate . "', '" . $ldate . "', '".$this->item."')
            ) MD ON MD.item_code = MT.MITM_ITMCD
            WHERE MITM_MODEL = 1
            AND MD.period_iter IS NOT NULL
            GROUP BY
                MITM_ITMCD,
                MITM_ITMD1,
                MITM_STKUOM,
                MD.period_iter,
                MD.first_date,
                MD.last_date,
                MD.begin_stock,
                MD.inc_stock,
                MD.inc_opt_1_stock,
                MD.inc_opt_2_stock,
                MD.inc_opt_3_stock,
                MD.out_stock,
                MD.out_opt_1_stock,
                MD.out_opt_2_stock,
                MD.out_opt_3_stock,
                MD.out_opt_4_stock,
                MD.disc_book_stock,
                MD.opn_stock,
                MD.opn_stock_date,
                MD.phy_stock,
                MD.phy_stock_date,
                MD.total_stock,
                MD.tarik_mundur_inc,
                MD.tarik_mundur_out,
                MD.tot_saldo_phy
        ";
        // $queryString = "SELECT * FROM PSI_RPCUST.dbo.f_kka_generator('" . $this->mutasi . "', " . $this->period . ", '" . $fdate . "', '" . $ldate . "')";

        logger($queryString);
        $result = array_map(function ($value) {
            return (array)$value;
        }, DB::select($queryString));

        return $result;
    }

    public function getDateData()
    {
        $dataForheader = DB::select("
            SELECT
                md.period_iter,
                DATENAME(month, MIN(mdd.first_date)) AS f_month,
                MIN(mdd.year_desc) AS f_year,
                DATENAME(month, MAX(mdd.first_date)) AS l_month,
                MAX(mdd.year_desc) AS l_year,
                MIN(mdd.first_date) AS first_date,
                MAX(mdd.last_date) AS last_date
            from PSI_RPCUST.dbo.f_kka_date_logic(" . $this->period . ", '" . $this->fdate . "', '" . $this->ldate . "', 1, 1) md
            inner join (
                SELECT mdd2.* FROM PSI_RPCUST.dbo.f_kka_date_logic(" . $this->period . ", '" . $this->fdate . "', '" . $this->ldate . "', 1, 1) mdd2
            ) mdd ON mdd.period_iter = md.period_iter
            GROUP BY
                md.period_iter
            ORDER BY
                md.period_iter
        ");

        $dataForheader = array_map(function ($value) {
            return (array)$value;
        }, $dataForheader);

        return $dataForheader;
    }

    public function listItem()
    {
        if ($this->mutasi === 'bahan_baku') {
            return ItemMaster::select(
                'MITM_ITMCD',
                'MITM_ITMD1',
                'MITM_STKUOM'
            )->where('MITM_MODEL', 0)
                ->groupBy(
                    'MITM_ITMCD',
                    'MITM_ITMD1',
                    'MITM_STKUOM'
                )->get()->toArray();
        } elseif ($this->mutasi === 'finish_good') {
            return ItemMaster::select(
                'MITM_ITMCD',
                'MITM_ITMD1',
                'MITM_STKUOM'
            )->where('MITM_MODEL', 1)
                ->groupBy(
                    'MITM_ITMCD',
                    'MITM_ITMD1',
                    'MITM_STKUOM'
                )->get()->toArray();
        }

        return ItemMaster::select(
            'MITM_ITMCD',
            'MITM_ITMD1',
            'MITM_STKUOM'
        )->groupBy(
            'MITM_ITMCD',
            'MITM_ITMD1',
            'MITM_STKUOM'
        )->get()->toArray();
    }
}
