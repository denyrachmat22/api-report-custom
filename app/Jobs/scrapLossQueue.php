<?php

namespace App\Jobs;

use App\Model\WMS\API\SPLSCN;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use LRedis;
use App\Jobs\scrapFixForDisposeQueue;

use App\Model\RPCUST\scrapHist;
use App\Model\RPCUST\scrapHistDet;

class scrapLossQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data, $username, $idScrap, $dept, $mainreason;

    public function __construct(Array $data, $username, $idScrap, $dept, $mainreason) {
        $this->dept = $dept;
        $this->data = $data;
        $this->username = $username;
        $this->idScrap = $idScrap;
        $this->mainreason = $mainreason;
    }

    public function handle()
    {
        $cekLot = SPLSCN::select(
            DB::raw('(
                SELECT 
                    COALESCE(SUM(SERD_QTY), 0)
                FROM SERD_TBL
                WHERE SERD_ITMCD = SPLSCN_ITMCD
                AND SERD_LOTNO = SPLSCN_LOTNO
            ) AS TOT_FG_QTY'),
            DB::raw('(
                SELECT
                    COALESCE(SUM(SCR_QTY), 0)
                FROM PSI_RPCUST.dbo.RPSCRAP_HIST_DET
                WHERE SCR_ITMCD = SPLSCN_ITMCD
                AND SCR_LOTNO = SPLSCN_LOTNO
                AND deleted_at is null
            ) as TOT_SCR_QTY'),
            DB::raw('SUM(SPLSCN_QTY) AS SPLSCN_QTY'),
            'SPLSCN_ITMCD',
            'SPLSCN_LOTNO',
            'RCV_PRPRC',
            'RCV_BCTYPE'
        )
            ->leftJoin(DB::raw('(
                SELECT * FROM RCVSCN_TBL rt 
                INNER JOIN RCV_TBL rt2 ON rt.RCVSCN_ITMCD = rt2.RCV_ITMCD
                    AND rt.RCVSCN_DONO = rt2.RCV_DONO 
            ) rt'), function($j) {
                $j->on('RCVSCN_LOTNO', 'SPLSCN_LOTNO');
                $j->on('RCVSCN_ITMCD', 'SPLSCN_ITMCD');
            })
            ->where('SPLSCN_DOC', $this->data['PPSN1_PSNNO'])
            ->where('SPLSCN_ITMCD', $this->data['PPSN2_SUBPN'])
            ->groupBy(
                'SPLSCN_ITMCD',
                'SPLSCN_LOTNO',
                'RCV_PRPRC',
                'RCV_BCTYPE'
            )
            ->havingRaw('
                SUM(SPLSCN_QTY) - ((
                    SELECT
                        COALESCE(SUM(SCR_QTY), 0)
                    FROM PSI_RPCUST.dbo.RPSCRAP_HIST_DET
                    WHERE SCR_ITMCD = SPLSCN_ITMCD
                    AND SCR_LOTNO = SPLSCN_LOTNO
                )) > 0
            ')
            ->first();

        if($cekLot !== null) {
            $lossQty = $this->data['LOG_RET_QTY'] - $this->data['ACT_RETQTY'];

            $masterInsert = scrapHist::create([
                'USERNAME' => $this->username,
                'DEPT' => $this->dept,
                'QTY' => $lossQty,
                'ID_TRANS' => $this->idScrap,
                'TYPE_TRANS' => 'LOSS',
                'DOC_NO' => $this->data['PPSN1_PSNNO'],
                'REASON' => 'LOSS PART',
                'MDL_FLAG' => 0,
                'ITEMNUM' => $this->data['PPSN2_SUBPN'],
                'DATE_OUT' => date('Y-m-d'),
                'MAIN_REASON' => $this->mainreason,
                'MENU_ID' => 'loss_part'
            ]);

            $insertDet = scrapHistDet::create([
                'SCR_HIST_ID' => $masterInsert->id,
                'SCR_PSN' => $this->data['PPSN1_PSNNO'],
                'SCR_MC' => $this->data['PPSN2_MC'],
                'SCR_MCZ' => $this->data['PPSN2_MCZ'],
                'SCR_ITMCD' => $this->data['PPSN2_SUBPN'],
                'SCR_QTPER' => $this->data['PPSN2_QTPER'],
                'SCR_QTY' => $this->data['LOG_RET_QTY'] - $this->data['ACT_RETQTY'],
                'SCR_LOTNO' => $cekLot->SPLSCN_LOTNO,
                'SCR_ITMPRC' =>  $cekLot->RCV_PRPRC,
                'SCR_PROCD' => $this->data['PPSN2_PROCD']
            ]);

            // Insert scrap to Ex-BC
            $insertingStock = $this->sendingParamBCStock(
                $this->data['PPSN2_SUBPN'],
                intVal($lossQty),
                $masterInsert->id,
                $cekLot->SPLSCN_LOTNO,
                $cekLot->RCV_BCTYPE,
                $insertDet->id
            );

            if ($insertingStock['status'] == true) {
                $redis = LRedis::connection();
                $redis->publish('message', json_encode([
                    'app' => 'scrap_report',
                    'data' => $this->data,
                    'status' => 'positive',
                    'message' => $insertingStock['message'],
                ]));
                // scrapFixForDisposeQueue::dispatch(
                //     'ith',
                //     '',
                //     '',
                //     $masterInsert->id,
                //     ''
                // )->onQueue('fix_scrap_ith');
            } else {
                $redis = LRedis::connection();
                $redis->publish('message', json_encode([
                    'app' => 'scrap_report',
                    'data' => $this->data,
                    'status' => 'negative',
                    'message' => $insertingStock['message'],
                ]));
            }
        } else {
            $redis = LRedis::connection();
            $redis->publish('message', json_encode([
                'app' => 'scrap_report',
                'data' => $this->data,
                'cekLot' => $cekLot,
                'status' => 'negative',
                'message' => 'Data not found on SPL Scan !!',
            ]));
        }
    }

    public function sendingParamBCStock($item_num, $qty = 0, $doc = null, $lot = null, $bc = null, $id = '', $rev = false)
    {
        $endpoint = 'http://192.168.0.29:8081/api-report-custom/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item_num,
            'qty' => $qty,
            'doc' => $doc,
            'lot' => $lot,
            'bc' => $bc,
            'scrap' => true,
            'remark' => $id,
            'revise' => $rev
        ];

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content['CURL'];
    }

}