<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

use App\Model\MASTER\ITHMaster;
use Illuminate\Support\Facades\DB;
use App\Model\RPCUST\SaldoWIP;

class SaldoStokWIP implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 3;
    public $timeout = 120;

    protected $date;

    public function __construct($date = null)
    {
        $this->date = $date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->date) {            
            $cekwip = $this->masterQuery()
            ->with('ret.retdet')
            ->whereBetween('ITH_DATE', [date($this->date, strtotime('-30 days')), $this->date])
            ->where(function ($q) {
                $q->where('ITH_FORM', 'INC-PRD-RM');
            })
            ->get()->toArray();

            $cekfingood = $this->masterQuery()
                ->where('ITH_FORM', 'INC-PRD-FG')
                ->where('ITH_DATE',$this->date)
                ->get()->toArray();
        }

        // return $cekfingood;
        $hasil = [];
        foreach ($cekwip as $key => $value) {
            $res = [];
            $totfg = 0;
            $totreq = 0;

            // Cek item di finish good query
            foreach ($cekfingood as $key_det => $value_det) {
                $get = [];
                foreach ($value_det['spl'] as $key_deeper => $value_deeper) {
                    $parsesplmode = explode('|',$value['ITH_DOC']);

                    if (
                        $value_deeper['SPL_DOC'] == $parsesplmode[0] &&
                        $value_deeper['SPL_CAT'] == $parsesplmode[1] &&
                        $value_deeper['SPL_LINE'] == $parsesplmode[2] &&
                        $value_deeper['SPL_FEDR'] == $parsesplmode[3] &&
                        $value_deeper['SPL_ITMCD'] == $value['ITH_ITMCD']
                    ) {
                        $get[trim($value_deeper['SPL_ITMCD'])] = true;
                        $totfg += $value_det['ITH_QTY'];
                        $totreq += $value_deeper['SPL_QTYREQ'];
                    }
                }

                $res[] = $get;
            }

            $cekdata = array_filter(
                $res,
                function ($c) {
                    if (count($c) > 0)
                        return $c;
                }
            );            

            $detret = 0;
            foreach ($value['ret'] as $key_splscan_deeper => $value_splscan_deeper) {
                if ($value['ITH_ITMCD'] == $value_splscan_deeper['ITH_ITMCD']) {
                    $detret += $value_splscan_deeper['ITH_QTY'];
                }
            }

            $hasil[] = [
                'item_code' => $value['ITH_ITMCD'],
                'date' => $value['ITH_DATE'],
                'qty' => $value['ITH_QTY'],
                'status_fingood' => count($cekdata),
                'total_fg' => $totfg,
                'total_return' => $detret,
                'total_req' => $totreq
            ];
        }

        foreach ($hasil as $key_hasil => $value_hasil) {
            $query = SaldoWIP::where('RPWIP_ITMCOD', $value_hasil['item_code'])->first();

            if ($value_hasil['status_fingood'] > 0) {
                if (empty($query)) {
                    SaldoWIP::create([
                        'RPWIP_DATEIS' => $value_hasil['date'],
                        'RPWIP_ITMCOD' => $value_hasil['item_code'],
                        'RPWIP_UNITMS' => NULL,
                        'RPWIP_QTYTOT' => ((int)$value_hasil['qty'] - (int)$value_hasil['total_req']) - (int)$value_hasil['total_return'],
                    ]);
                } else {
                    $query->update([
                        'RPWIP_DATEIS' => $value_hasil['date'],
                        'RPWIP_ITMCOD' => $value_hasil['item_code'],
                        'RPWIP_UNITMS' => NULL,
                        'RPWIP_QTYTOT' => ((int)$value_hasil['qty'] - (int)$value_hasil['total_req']) - (int)$value_hasil['total_return'],
                    ]);
                }
            } else {
                if (empty($query)) {
                    SaldoWIP::create([
                        'RPWIP_DATEIS' => $value_hasil['date'],
                        'RPWIP_ITMCOD' => $value_hasil['item_code'],
                        'RPWIP_UNITMS' => NULL,
                        'RPWIP_QTYTOT' => (int) $value_hasil['qty'],
                    ]);
                } else {
                    $query->update([
                        'RPWIP_DATEIS' => $value_hasil['date'],
                        'RPWIP_ITMCOD' => $value_hasil['item_code'],
                        'RPWIP_UNITMS' => NULL,
                        'RPWIP_QTYTOT' => (int) $value_hasil['qty'],
                    ]);
                }
            }
        }
    }

    public function masterQuery()
    {
        $select = [
            'ITH_ITMCD',
            'ITH_DATE',
            'ITH_DOC'
        ];

        $masterITH = ITHMaster::select(array_merge($select, [DB::raw('SUM(ITH_QTY) AS ITH_QTY')]))
            ->with('spl.splscn.returnspl')
            ->groupBy($select);

        return $masterITH;
    }
}
