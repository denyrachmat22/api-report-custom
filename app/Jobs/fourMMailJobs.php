<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\fourMStatusNotif;
use Illuminate\Support\Facades\Mail;

class fourMMailJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;
    protected $user;
    protected $file;

    public function __construct($user, $data, $file)
    {
        $this->user = $user;
        $this->data = $data;
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to('deny-rachmat@sumitronics.co.jp')
            ->cc('rexon-julianto@sumitronics.co.jp')
            ->send(new fourMStatusNotif($this->user, $this->data, $this->file));
    }
}
