<?php

namespace App\Jobs;

use App\Model\RPCUST\scrapDNScan;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use LRedis;
use Illuminate\Support\Facades\DB;

use App\Model\WMS\API\RCVSCN;
use App\Model\RPCUST\scrapHistDet;
use App\Model\RPCUST\scrapHist;
use App\Model\WMS\REPORT\MutasiStok;
use App\Model\MASTER\ITHMaster;
use App\Model\MASTER\MITMGRP;
use App\Model\RPCUST\scrapDNList;

class scrapFixForDisposeQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $fix;
    protected $item_code;
    protected $item_lot;
    protected $id;
    protected $id_det;
    protected $isdisposedn;
    public function __construct($fix, $item_code, $item_lot, $id, $id_det, $isdisposedn = false)
    {
        $this->fix = $fix;
        $this->item_code = $item_code;
        $this->item_lot = $item_lot;
        $this->id = $id;
        $this->id_det = $id_det;
        $this->isdisposedn = $isdisposedn;
    }

    public function handle()
    {
        $redis = LRedis::connection();
        if ($this->fix === 'scrapPrice') {
            $cekRCV = RCVSCN::select(
                DB::raw('RCVSCN_TBL.*'),
                DB::raw('(
                    CASE WHEN rt2.RCV_PRPRC IS NOT NULL
                        THEN rt2.RCV_PRPRC
                        ELSE rt.RCV_PRPRC
                    END
                ) AS RCV_PRPRC')
            )->where('RCVSCN_ITMCD', $this->item_code)
                ->join(DB::raw('RCV_TBL as rt'), function ($j) {
                    $j->on('rt.RCV_DONO', 'RCVSCN_DONO');
                    $j->on('rt.RCV_ITMCD', 'RCVSCN_ITMCD');
                })
                ->leftJoin('RCV_TBL as rt2', function ($j) {
                    $j->on('rt2.RCV_DONO', 'rt.RCV_DONO_REFF');
                    $j->on('rt2.RCV_ITMCD', 'rt.RCV_ITMCD_REFF');
                })
                ->where('RCVSCN_SAVED', 1);

            $cekRCVWithLot = $cekRCV->where('RCVSCN_LOTNO', $this->item_lot)->first();

            // return $cekRCVWithLot;
            // Jika ada update harga dari RCV_TBL
            if (!empty($cekRCVWithLot)) {
                scrapHistDet::where('id', $this->id_det)->update([
                    'SCR_ITMPRC' => $cekRCVWithLot->RCV_PRPRC
                ]);

                $cekDataDet = scrapHistDet::where('id', $this->id_det)->first();

                if (!empty($cekDataDet)) {
                    $cekHasilMaster = scrapHist::where('RPSCRAP_HIST.id', $cekDataDet->SCR_HIST_ID)
                        ->join('RPSCRAP_HIST_DET', 'SCR_HIST_ID', 'RPSCRAP_HIST.id')
                        ->first();

                    $totalFixedPrice = scrapHist::where('ID_TRANS', $cekHasilMaster->ID_TRANS)
                        ->join('RPSCRAP_HIST_DET', 'SCR_HIST_ID', 'RPSCRAP_HIST.id')
                        ->where('SCR_ITMPRC', '>', 0)
                        ->count();

                    $redis->publish('message', json_encode([
                        'app' => 'fix_scrap_price',
                        'result' => [
                            'status' => true,
                            'data' => $cekRCVWithLot,
                            'total_fix' => $totalFixedPrice,
                            'message' => $this->item_code . ' get price $ ' . $cekRCVWithLot->RCV_PRPRC . ' from DO :' . $cekRCVWithLot->RCV_DONO . ' and Lot :' . $cekRCVWithLot->RCVSCN_LOTNO,
                        ],
                        'payload' => [
                            'fix' => $this->fix,
                            'item_code' => $this->item_code,
                            'item_lot' => $this->item_lot,
                            'id' => $this->id,
                            'id_det' => $this->id_det,
                            'doc' => scrapHist::where('id', $cekDataDet->SCR_HIST_ID)->first()->ID_TRANS
                        ]
                    ]));
                }
            } else { // Jika tidak cari lot lain yang ada
                $cekRCVWithoutLot = RCVSCN::select(
                    'RCVSCN_ITMCD',
                    'RCV_PRPRC',
                    'RCVSCN_LOTNO',
                    DB::raw('SUM(RCVSCN_QTY) - SUM(SERD_QTY) AS TOTAL'),
                )
                    ->where('RCVSCN_ITMCD', $this->item_code)
                    ->join('RCV_TBL', function ($j) {
                        $j->on('RCV_DONO', 'RCVSCN_DONO');
                        $j->on('RCV_ITMCD', 'RCVSCN_ITMCD');
                    })
                    ->join('SERD_TBL', function ($j) {
                        $j->on('SERD_ITMCD', 'RCVSCN_ITMCD');
                        $j->on('SERD_LOTNO', 'RCVSCN_LOTNO');
                    })
                    ->where('RCVSCN_SAVED', 1)
                    ->orderBy(DB::raw('MIN([RCVSCN_LUPDT])'))
                    ->havingRaw('SUM(RCVSCN_QTY) - SUM(SERD_QTY) > 0')
                    ->groupBy(
                        'RCVSCN_ITMCD',
                        'RCV_PRPRC',
                        'RCVSCN_LOTNO'
                    )
                    ->first();

                if (!empty($cekRCVWithoutLot)) {
                    scrapHistDet::where('id', $this->id_det)->update([
                        'SCR_LOTNO' => $cekRCVWithoutLot->RCVSCN_LOTNO,
                        'SCR_ITMPRC' => $cekRCVWithoutLot->RCV_PRPRC
                    ]);

                    $cekDataDet = scrapHistDet::where('id', $this->id_det)->first();
                    if (!empty($cekDataDet)) {
                        $cekHasilMaster = scrapHist::where('RPSCRAP_HIST.id', $cekDataDet->SCR_HIST_ID)
                            ->join('RPSCRAP_HIST_DET', 'SCR_HIST_ID', 'RPSCRAP_HIST.id')
                            ->first();

                        $totalFixedPrice = scrapHist::where('ID_TRANS', $cekHasilMaster->ID_TRANS)
                            ->join('RPSCRAP_HIST_DET', 'SCR_HIST_ID', 'RPSCRAP_HIST.id')
                            ->where('SCR_ITMPRC', '>', 0)
                            ->count();
                        $redis->publish('message', json_encode([
                            'app' => 'fix_scrap_price',
                            'result' => [
                                'status' => true,
                                'data' => $cekRCVWithoutLot,
                                'total_fix' => $totalFixedPrice,
                                'message' => $this->item_code . ' get price $ ' . $cekRCVWithoutLot->RCV_PRPRC . ' from DO :' . $cekRCVWithoutLot->RCV_DONO . ' and Lot :' . $cekRCVWithoutLot->RCVSCN_LOTNO,
                            ],
                            'payload' => [
                                'fix' => $this->fix,
                                'item_code' => $this->item_code,
                                'item_lot' => $this->item_lot,
                                'id' => $this->id,
                                'id_det' => $this->id_det,
                                'doc' => scrapHist::where('id', $cekDataDet->SCR_HIST_ID)->first()->ID_TRANS
                            ]
                        ]));
                    }
                } else {
                    $cekPriceToRCV = MutasiStok::where('RCV_ITMCD', $this->item_code)->orderBy('RCV_LUPDT', 'DESC')->first();

                    if (!empty($cekPriceToRCV)) {
                        scrapHistDet::where('id', $this->id_det)->update([
                            'SCR_LOTNO' => '',
                            'SCR_ITMPRC' => $cekPriceToRCV->RCV_PRPRC
                        ]);

                        $cekDataDet = scrapHistDet::where('id', $this->id_det)->first();
                        if (!empty($cekDataDet)) {
                            $cekHasilMaster = scrapHist::where('RPSCRAP_HIST.id', $cekDataDet->SCR_HIST_ID)
                                ->join('RPSCRAP_HIST_DET', 'SCR_HIST_ID', 'RPSCRAP_HIST.id')
                                ->first();

                            $totalFixedPrice = scrapHist::where('ID_TRANS', $cekHasilMaster->ID_TRANS)
                                ->join('RPSCRAP_HIST_DET', 'SCR_HIST_ID', 'RPSCRAP_HIST.id')
                                ->where('SCR_ITMPRC', '>', 0)
                                ->count();
                            $redis->publish('message', json_encode([
                                'app' => 'fix_scrap_price',
                                'result' => [
                                    'status' => true,
                                    'data' => $cekPriceToRCV,
                                    'total_fix' => $totalFixedPrice,
                                    'message' => $this->item_code . ' get price $ ' . $cekPriceToRCV->RCV_PRPRC . ' from DO : ' . $cekPriceToRCV->RCV_DONO,
                                ],
                                'payload' => [
                                    'fix' => $this->fix,
                                    'item_code' => $this->item_code,
                                    'item_lot' => $this->item_lot,
                                    'id' => $this->id,
                                    'id_det' => $this->id_det,
                                    'doc' => scrapHist::where('id', $cekDataDet->SCR_HIST_ID)->first()->ID_TRANS
                                ]
                            ]));
                        }
                    } else {
                        $redis->publish('message', json_encode([
                            'app' => 'fix_scrap_price',
                            'result' => [
                                'status' => false,
                                'data' => [],
                                'message' => $this->item_code . ' no data found in incoming GRN',
                            ],
                            'payload' => [
                                'fix' => $this->fix,
                                'item_code' => $this->item_code,
                                'item_lot' => $this->item_lot,
                                'id' => $this->id,
                                'id_det' => $this->id_det
                            ]
                        ]));
                    }
                }
            }
        } elseif ($this->fix === 'ith') {
            $this->resubmitITH(base64_encode($this->id));
        } elseif ($this->fix === 'dispose') {
            $this->resubmitITH(base64_encode($this->id), true);
        }
    }

    public function resubmitITH($id, $dispose = false)
    {
        $redis = LRedis::connection();

        if ($dispose) {
            if ($this->isdisposedn) {
                $cekDispDN = scrapDNScan::where('RPSCRAP_DN_SCAN.id', base64_decode($id))
                    ->join('PSI_WMS.dbo.MITM_TBL', 'RDS_ITMCD', 'MITM_ITMCD')
                    ->join('RPSCRAP_DN_LIST', 'RDL_ID', 'RPSCRAP_DN_LIST.id')
                    ->where('RDS_ISSAVED', 1)
                    ->get();

                // logger($cekDispDN);
                // logger(base64_decode($id));
                if (!empty($cekDispDN)) {
                    foreach ($cekDispDN as $key => $value) {
                        $ithMaster = ITHMaster::select(
                            'ITH_WH',
                            'ITH_SER',
                            DB::raw('SUM(ITH_QTY) AS QTY')
                        )
                            ->whereIn('ITH_WH', ['QAFG', 'ARWH0PD']);

                        if (!empty($value->RDS_REF) && $value->MITM_MODEL == 1) {
                            $ithMaster->where('ITH_SER', $value->RDS_REF)
                                ->where('ITH_DOC', $value->RDS_LOT);
                        } else {
                            $ithMaster->where('ITH_LINE', base64_decode($id));
                        }

                        $ithMaster
                            ->groupBy('ITH_WH', 'ITH_SER')
                            ->havingRaw('SUM(ITH_QTY) > 0');

                        $cekHistITH = (clone $ithMaster)->first();
                        $getTimeCreated = str_split($value->created_at)[1];

                        logger('dispose dn ada');
                        logger($cekHistITH);
                        if (!empty($cekHistITH)) {
                            // Pindahin ke scrap
                            // Issue Scrap
                            $issue = $this->insertToITH(
                                trim($value->RDS_ITMCD),
                                date('Y-m-d'),
                                'OUT-SCR-DISPOSE-DN',
                                $value->RDL_PICK_ID,
                                round($value->RDS_QTY) * -1,
                                $cekHistITH->ITH_WH,
                                $value->MITM_MODEL == 1 ? $value->RDS_REF : '',
                                $value->id,
                                'SCR_RPT',
                                $getTimeCreated
                            );

                            // Receive Scrap
                            $issue = $this->insertToITH(
                                trim($value->RDS_ITMCD),
                                date('Y-m-d'),
                                'INC-SCR-DISPOSE-DN',
                                $value->RDL_PICK_ID,
                                round($value->RDS_QTY),
                                $value->MITM_MODEL == 1 ? 'AFWH9SC' : 'ARWH9SC',
                                $value->MITM_MODEL == 1 ? $value->RDS_REF : '',
                                $value->id,
                                'SCR_RPT',
                                $getTimeCreated
                            );

                            // Issue Scrap to dispose
                            $issue = $this->insertToITH(
                                trim($value->RDS_ITMCD),
                                date('Y-m-d'),
                                'OUT-DISPOSE-DN',
                                $value->RDL_PICK_ID,
                                round($value->RDS_QTY) * -1,
                                $value->MITM_MODEL == 1 ? 'AFWH9SC' : 'ARWH9SC',
                                $value->MITM_MODEL == 1 ? $value->RDS_REF : '',
                                $value->id,
                                'SCR_RPT',
                                $getTimeCreated
                            );
                        } else {
                            $redis->publish('message', json_encode([
                                'app' => 'scrap_dispose',
                                'result' => [
                                    'status' => false,
                                    'data' => [],
                                    'menu' => '',
                                    'message' => $value->RDS_REF . ' ID Scrap not found or stock already 0!',
                                    'query' => (clone $ithMaster)->toSql()
                                ],
                                'payload' => [
                                    'fix' => $this->fix,
                                    'item_code' => $this->item_code,
                                    'item_lot' => $this->item_lot,
                                    'id' => $this->id,
                                    'id_det' => $this->id_det,
                                    'doc' => ''
                                ]
                            ]));
                        }
                    }
                }
            } else {
                $cekHist = scrapHist::where('id', base64_decode($id))
                    ->join(DB::raw('(
                        SELECT
                            ITH_DOC,
                            ITH_WH,
                            ITH_REMARK,
                            ITH_DATEC,
                            SUM(ITH_QTY) AS ITH_QTY
                        FROM PSI_WMS.dbo.v_ith_tblc
                        GROUP BY
                            ITH_DOC,
                            ITH_WH,
                            ITH_REMARK,
                            ITH_DATEC
                    ) ith'), 'ith.ITH_REMARK', DB::raw('CAST(id AS VARCHAR(100))'))
                    ->first();

                if (!empty($cekHist)) {
                    $ithMaster = ITHMaster::select(
                        'ITH_WH',
                        'ITH_SER',
                        DB::raw('SUM(ITH_QTY) AS QTY')
                    )
                        ->where('ITH_DOC', $cekHist->ID_TRANS)
                        ->where('ITH_REMARK', base64_decode($id))
                        ->whereIn('ITH_WH', ['AFWH9SC', 'ARWH9SC', 'NFWH9SC']);

                    if (!empty($cekHist->REF_NO) && $cekHist->MDL_FLAG === 1) {
                        $ithMaster->where('ITH_SER', $cekHist->REF_NO);
                    }

                    $ithMaster
                        ->groupBy('ITH_WH', 'ITH_SER')
                        ->havingRaw('SUM(ITH_QTY) > 0');

                    $cekHistITH = $ithMaster->first();
                    $getTimeCreated = str_split($cekHist->created_at)[1];

                    if (!empty($cekHistITH)) {
                        $issue = $this->insertToITH(
                            trim($cekHist->ITEMNUM),
                            date('Y-m-d', strtotime($cekHist->DISPOSED_DATE)),
                            'OUT-DISPOSE',
                            $cekHist->ID_TRANS,
                            round($cekHist->QTY) * -1,
                            $cekHistITH->ITH_WH,
                            $cekHist->MDL_FLAG === 1 ? $cekHist->REF_NO : $cekHistITH->ITH_SER,
                            $cekHist->id,
                            'SCR_RPT',
                            $getTimeCreated
                        );

                        $redis->publish('message', json_encode([
                            'app' => 'fix_scrap_ith',
                            'result' => [
                                'status' => true,
                                'data' => [
                                    'ISSUE' => [
                                        'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                        'ITH_DATE' => date('Y-m-d', strtotime($cekHist->DISPOSED_DATE)),
                                        'ITH_FORM' => 'OUT-DISPOSE',
                                        'ITH_DOC' => $cekHistITH->ITH_DOC,
                                        'ITH_QTY' => round($cekHistITH->ITH_QTY) * -1,
                                        'ITH_WH' => $cekHistITH->ITH_WH,
                                        'ITH_SER' => $cekHist->REF_NO,
                                        'ITH_REMARK' => $cekHist->id,
                                        'ITH_USRID' => 'SCR_RPT',
                                    ],
                                    'RECEIVE' => []
                                ],
                                'menu' => $cekHist->MENU_ID,
                                'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . ' has been moved to Dispose !',
                            ],
                            'payload' => [
                                'fix' => $this->fix,
                                'item_code' => $this->item_code,
                                'item_lot' => $this->item_lot,
                                'id' => $this->id,
                                'id_det' => $this->id_det,
                                'doc' => $cekHist->ID_TRANS
                            ]
                        ]));
                    } else {
                        $redis->publish('message', json_encode([
                            'app' => 'scrap_dispose',
                            'result' => [
                                'status' => false,
                                'data' => [],
                                'menu' => '',
                                'message' => $this->id . ' ID Scrap not found or not submit to Scrap yet!',
                            ],
                            'payload' => [
                                'fix' => $this->fix,
                                'item_code' => $this->item_code,
                                'item_lot' => $this->item_lot,
                                'id' => $this->id,
                                'id_det' => $this->id_det,
                                'doc' => ''
                            ]
                        ]));
                    }
                } else {
                    $redis->publish('message', json_encode([
                        'app' => 'scrap_dispose',
                        'result' => [
                            'status' => false,
                            'data' => [],
                            'menu' => '',
                            'message' => $this->id . ' ID Scrap not found or not submit to Scrap yet!',
                        ],
                        'payload' => [
                            'fix' => $this->fix,
                            'item_code' => $this->item_code,
                            'item_lot' => $this->item_lot,
                            'id' => $this->id,
                            'id_det' => $this->id_det,
                            'doc' => $cekHist->ID_TRANS
                        ]
                    ]));
                }
            }
        } else {
            $cekHist = scrapHist::where('id', base64_decode($id))
                ->leftJoin(DB::raw('(
                    SELECT
                        ITH_DOC,
                        ITH_WH,
                        ITH_REMARK,
                        ITH_DATEC,
                        ITH_SER,
                        SUM(ITH_QTY) AS ITH_QTY
                    FROM PSI_WMS.dbo.v_ith_tblc
                    GROUP BY
                        ITH_DOC,
                        ITH_WH,
                        ITH_REMARK,
                        ITH_SER,
                        ITH_DATEC
                ) ith'), 'ith.ITH_REMARK', DB::raw('CAST(id AS VARCHAR(100))'))
                ->whereNull('ith.ITH_REMARK')
                ->first();

            if (!empty($cekHist)) {
                if (empty($cekHist->IS_CONFIRMED)) {
                    $redis->publish('message', json_encode([
                        'app' => 'fix_scrap_ith',
                        'result' => [
                            'status' => false,
                            'data' => [],
                            'menu' => $cekHist->MENU_ID,
                            'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . ' Scrap not confirmed yet !',
                        ],
                        'payload' => [
                            'fix' => $this->fix,
                            'item_code' => $this->item_code,
                            'item_lot' => $this->item_lot,
                            'id' => $this->id,
                            'id_det' => $this->id_det,
                            'doc' => $cekHist->ID_TRANS
                        ]
                    ]));
                } else {
                    $getTimeCreated = str_split($cekHist->created_at)[1];
                    // If Finish good
                    if ($cekHist->MDL_FLAG == 1) {
                        // IF From Pending
                        if ($cekHist->QTY_SCRAP <> 0 || $cekHist->MENU_ID === 'pending') {
                            if (!empty($cekHist->REF_NO)) {
                                $cekHistITH = ITHMaster::select(DB::raw('SUM(ITH_QTY) AS QTY'))->where('ITH_SER', $cekHist->REF_NO)->where('ITH_WH', 'QAFG')->first();

                                // return $cekHistITH;
                                if (!empty($cekHistITH->QTY) && $cekHistITH->QTY > 0) {
                                    $cekHistDet = ITHMaster::where('ITH_SER', $cekHist->REF_NO)->where('ITH_WH', 'QAFG')->orderBy('ITH_LUPDT', 'desc')->first();
                                    // return $cekHistDet;

                                    $issue = $this->insertToITH(
                                        trim($cekHist->ITEMNUM),
                                        date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                        'OUT-PEN-FG',
                                        $cekHistDet->ITH_DOC,
                                        round($cekHistDet->ITH_QTY) * -1,
                                        $cekHistDet->ITH_WH,
                                        $cekHist->REF_NO,
                                        $cekHist->id,
                                        'SCR_RPT',
                                        $getTimeCreated
                                    );

                                    // Receive
                                    $receive = $this->insertToITH(
                                        trim($cekHist->ITEMNUM),
                                        date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                        'INC-SCR-FG',
                                        $cekHist->ID_TRANS,
                                        round($cekHistDet->ITH_QTY),
                                        'AFWH9SC',
                                        $cekHist->REF_NO,
                                        $cekHist->id,
                                        'SCR_RPT',
                                        $getTimeCreated
                                    );

                                    $redis->publish('message', json_encode([
                                        'app' => 'fix_scrap_ith',
                                        'result' => [
                                            'status' => true,
                                            'data' => [
                                                'ISSUE' => [
                                                    'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                    'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                    'ITH_FORM' => 'OUT-PEN-FG',
                                                    'ITH_DOC' => $cekHistDet->ITH_DOC,
                                                    'ITH_QTY' => round($cekHistDet->ITH_QTY) * -1,
                                                    'ITH_WH' => $cekHistDet->ITH_WH,
                                                    'ITH_SER' => $cekHist->REF_NO,
                                                    'ITH_REMARK' => $cekHist->id,
                                                    'ITH_USRID' => 'SCR_RPT',
                                                ],
                                                'RECEIVE' => [
                                                    'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                    'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                    'ITH_FORM' => 'INC-SCR-FG',
                                                    'ITH_DOC' => $cekHist->ID_TRANS,
                                                    'ITH_QTY' => round($cekHistDet->ITH_QTY),
                                                    'ITH_WH' => 'AFWH9SC',
                                                    'ITH_SER' => $cekHist->REF_NO,
                                                    'ITH_REMARK' => $cekHist->id,
                                                    'ITH_USRID' => 'SCR_RPT'
                                                ]
                                            ],
                                            'menu' => $cekHist->MENU_ID,
                                            'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . 'has been moved to Scrap !',
                                        ],
                                        'payload' => [
                                            'fix' => $this->fix,
                                            'item_code' => $this->item_code,
                                            'item_lot' => $this->item_lot,
                                            'id' => $this->id,
                                            'id_det' => $this->id_det,
                                            'doc' => $cekHist->ID_TRANS
                                        ]
                                    ]));
                                } else {
                                    $redis->publish('message', json_encode([
                                        'app' => 'fix_scrap_ith',
                                        'result' => [
                                            'status' => false,
                                            'data' => [],
                                            'menu' => $cekHist->MENU_ID,
                                            'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . ' Stock already 0 ! !',
                                        ],
                                        'payload' => [
                                            'fix' => $this->fix,
                                            'item_code' => $this->item_code,
                                            'item_lot' => $this->item_lot,
                                            'id' => $this->id,
                                            'id_det' => $this->id_det
                                        ]
                                    ]));
                                }
                            } else {
                                $redis->publish('message', json_encode([
                                    'app' => 'fix_scrap_ith',
                                    'result' => [
                                        'status' => false,
                                        'data' => [],
                                        'menu' => $cekHist->MENU_ID,
                                        'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . ' Pending scrap must has a Ref Code !',
                                    ],
                                    'payload' => [
                                        'fix' => $this->fix,
                                        'item_code' => $this->item_code,
                                        'item_lot' => $this->item_lot,
                                        'id' => $this->id,
                                        'id_det' => $this->id_det
                                    ]
                                ]));
                            }
                        } elseif ($cekHist->IS_RETURN <> 0 || $cekHist->MENU_ID == 'fg_return') { // If From Return FG
                            if (!empty($cekHist->REF_NO)) {
                                $cekITH = ITHMaster::where('ITH_SER', $cekHist->REF_NO)
                                    ->whereIn('ITH_WH', ['AFQART', 'AFQART2', 'NFWH4RT', 'AFWH3RT'])
                                    ->orderBy('ITH_LUPDT', 'desc')
                                    ->first();

                                $issue = $this->insertToITH(
                                    trim($cekHist->ITEMNUM),
                                    date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                    'OUT-SCR-RTN',
                                    $cekITH->ITH_DOC,
                                    round($cekHist->QTY) * -1,
                                    $cekITH->ITH_WH,
                                    $cekHist->REF_NO,
                                    $cekHist->id,
                                    'SCR_RPT',
                                    $getTimeCreated
                                );

                                // Receive
                                $receive = $this->insertToITH(
                                    trim($cekHist->ITEMNUM),
                                    date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                    'INC-SCR-FG',
                                    $cekHist->ID_TRANS,
                                    round($cekHist->QTY),
                                    'NFWH9SC',
                                    $cekHist->REF_NO,
                                    $cekHist->id,
                                    'SCR_RPT',
                                    $getTimeCreated
                                );

                                $redis->publish('message', json_encode([
                                    'app' => 'fix_scrap_ith',
                                    'result' => [
                                        'status' => true,
                                        'data' => [
                                            'ISSUE' => [
                                                'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                'ITH_FORM' => 'OUT-SCR-RTN',
                                                'ITH_DOC' => $cekITH->ITH_DOC,
                                                'ITH_QTY' => round($cekHist->QTY) * -1,
                                                'ITH_WH' => $cekITH->ITH_WH,
                                                'ITH_SER' => $cekHist->REF_NO,
                                                'ITH_REMARK' => $cekHist->id,
                                                'ITH_USRID' => 'SCR_RPT',
                                            ],
                                            'RECEIVE' => [
                                                'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                'ITH_FORM' => 'INC-SCR-FG',
                                                'ITH_DOC' => $cekHist->ID_TRANS,
                                                'ITH_QTY' => round($cekHist->QTY),
                                                'ITH_WH' => 'NFWH9SC',
                                                'ITH_SER' => $cekHist->REF_NO,
                                                'ITH_REMARK' => $cekHist->id,
                                                'ITH_USRID' => 'SCR_RPT'
                                            ]
                                        ],
                                        'menu' => $cekHist->MENU_ID,
                                        'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . ' has been moved to Scrap !',
                                    ],
                                    'payload' => [
                                        'fix' => $this->fix,
                                        'item_code' => $this->item_code,
                                        'item_lot' => $this->item_lot,
                                        'id' => $this->id,
                                        'id_det' => $this->id_det,
                                        'doc' => $cekHist->ID_TRANS
                                    ]
                                ]));
                            } else {
                                $redis->publish('message', json_encode([
                                    'app' => 'fix_scrap_ith',
                                    'result' => [
                                        'status' => false,
                                        'data' => [],
                                        'menu' => $cekHist->MENU_ID,
                                        'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . ' Resubmit ITH return FG need REF NO !',
                                    ],
                                    'payload' => [
                                        'fix' => $this->fix,
                                        'item_code' => $this->item_code,
                                        'item_lot' => $this->item_lot,
                                        'id' => $this->id,
                                        'id_det' => $this->id_det
                                    ]
                                ]));
                            }
                        } else { // If from WIP / Warehouse
                            if ($cekHist->MENU_ID === 'wip') {
                                $cekHistITH = ITHMaster::select(DB::raw('SUM(ITH_QTY) AS QTY'))->where('ITH_SER', $cekHist->REF_NO)->whereIn('ITH_WH', ['ARQA1', 'ARPRD1'])->first();
                                // return $cekHistITH;

                                // If WIP Using Ref Code
                                if (!empty($cekHist->REF_NO) && !empty($cekHistITH->QTY) && $cekHistITH->QTY > 0) {
                                    $cekHistDet = ITHMaster::where('ITH_SER', $cekHist->REF_NO)->whereIn('ITH_WH', ['ARQA1', 'ARPRD1'])->orderBy('ITH_LUPDT', 'desc')->first();
                                    // return $cekHistDet;

                                    $issue = $this->insertToITH(
                                        trim($cekHist->ITEMNUM),
                                        date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                        'OUT-WH-SCR-FG',
                                        $cekHistDet->ITH_DOC,
                                        round($cekHistDet->ITH_QTY) * -1,
                                        $cekHistDet->ITH_WH,
                                        $cekHist->REF_NO,
                                        $cekHist->id,
                                        'SCR_RPT',
                                        $getTimeCreated
                                    );

                                    // Receive
                                    $receive = $this->insertToITH(
                                        trim($cekHist->ITEMNUM),
                                        date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                        'INC-SCR-FG',
                                        $cekHist->ID_TRANS,
                                        round($cekHistDet->ITH_QTY),
                                        'AFWH9SC-',
                                        $cekHist->REF_NO,
                                        $cekHist->id,
                                        'SCR_RPT',
                                        $getTimeCreated
                                    );

                                    $redis->publish('message', json_encode([
                                        'app' => 'fix_scrap_ith',
                                        'result' => [
                                            'status' => true,
                                            'data' => [
                                                'ISSUE' => [
                                                    'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                    'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                    'ITH_FORM' => 'OUT-WH-SCR-FG',
                                                    'ITH_DOC' => $cekHistDet->ITH_DOC,
                                                    'ITH_QTY' => round($cekHistDet->ITH_QTY) * -1,
                                                    'ITH_WH' => $cekHistDet->ITH_WH,
                                                    'ITH_SER' => $cekHist->REF_NO,
                                                    'ITH_REMARK' => $cekHist->id,
                                                    'ITH_USRID' => 'SCR_RPT'
                                                ],
                                                'RECEIVE' => [
                                                    'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                    'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                    'ITH_FORM' => 'INC-SCR-FG',
                                                    'ITH_DOC' => $cekHist->ID_TRANS,
                                                    'ITH_QTY' => round($cekHistDet->ITH_QTY),
                                                    'ITH_WH' => 'AFWH9SC',
                                                    'ITH_SER' => $cekHist->REF_NO,
                                                    'ITH_REMARK' => $cekHist->id,
                                                    'ITH_USRID' => 'SCR_RPT'
                                                ]
                                            ],
                                            'menu' => $cekHist->MENU_ID,
                                            'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . 'has been moved to Scrap !',
                                        ],
                                        'payload' => [
                                            'fix' => $this->fix,
                                            'item_code' => $this->item_code,
                                            'item_lot' => $this->item_lot,
                                            'id' => $this->id,
                                            'id_det' => $this->id_det,
                                            'doc' => $cekHist->ID_TRANS
                                        ]
                                    ]));
                                } else {
                                    $cekOsITHData = DB::table('v_ith_tblc')->select(
                                        'ITH_SER',
                                        'ITH_WH',
                                        'ITH_DOC',
                                        'ITH_DATEC',
                                        DB::raw('SUM(ITH_QTY) AS QTY')
                                    )
                                        ->where('ITH_DOC', $cekHist->DOC_NO)
                                        ->whereIn('ITH_WH', ['ARQA1', 'ARPRD1'])
                                        ->groupBy(
                                            'ITH_SER',
                                            'ITH_WH',
                                            'ITH_DOC',
                                            'ITH_DATEC'
                                        )
                                        ->havingRaw('SUM(ITH_QTY) >= ' . $cekHist->QTY)
                                        ->orderBy('ITH_DATEC', 'ASC')
                                        ->first();

                                    if (!empty($cekOsITHData->QTY) && $cekOsITHData->QTY > 0) {
                                        $issue = $this->insertToITH(
                                            trim($cekHist->ITEMNUM),
                                            date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                            'OUT-WH-SCR-FG',
                                            $cekOsITHData->ITH_DOC,
                                            round($cekHist->QTY) * -1,
                                            $cekOsITHData->ITH_WH,
                                            $cekOsITHData->ITH_SER,
                                            $cekHist->id,
                                            'SCR_RPT',
                                            $getTimeCreated
                                        );

                                        // Receive
                                        $receive = $this->insertToITH(
                                            trim($cekHist->ITEMNUM),
                                            date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                            'INC-SCR-FG',
                                            $cekHist->ID_TRANS,
                                            round($cekHist->QTY),
                                            'AFWH9SC-',
                                            $cekOsITHData->ITH_SER,
                                            $cekHist->id,
                                            'SCR_RPT',
                                            $getTimeCreated
                                        );

                                        $redis->publish('message', json_encode([
                                            'app' => 'fix_scrap_ith',
                                            'result' => [
                                                'status' => true,
                                                'data' => [
                                                    'ISSUE' => [
                                                        'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                        'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                        'ITH_FORM' => 'OUT-WH-SCR-FG',
                                                        'ITH_DOC' => $cekOsITHData->ITH_DOC,
                                                        'ITH_QTY' => round($cekHist->QTY) * -1,
                                                        'ITH_WH' => $cekOsITHData->ITH_WH,
                                                        'ITH_SER' => $cekOsITHData->ITH_SER,
                                                        'ITH_REMARK' => $cekHist->id,
                                                        'ITH_USRID' => 'SCR_RPT'
                                                    ],
                                                    'RECEIVE' => [
                                                        'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                        'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                        'ITH_FORM' => 'INC-SCR-FG',
                                                        'ITH_DOC' => $cekHist->ID_TRANS,
                                                        'ITH_QTY' => round($cekHist->QTY),
                                                        'ITH_WH' => 'AFWH9SC',
                                                        'ITH_SER' => $cekOsITHData->ITH_SER,
                                                        'ITH_REMARK' => $cekHist->id,
                                                        'ITH_USRID' => 'SCR_RPT'
                                                    ]
                                                ],
                                                'menu' => $cekHist->MENU_ID,
                                                'message' => $cekHist->ID_TRANS . ' ref ' . $cekOsITHData->ITH_SER . 'has been moved to Scrap !',
                                                'fulldata' => $cekHist,
                                                'fullDataOS' => $cekOsITHData
                                            ],
                                            'payload' => [
                                                'fix' => $this->fix,
                                                'item_code' => $this->item_code,
                                                'item_lot' => $this->item_lot,
                                                'id' => $this->id,
                                                'id_det' => $this->id_det,
                                                'doc' => $cekHist->ID_TRANS
                                            ]
                                        ]));
                                    } else {
                                        $cekOsITHDataOnPrd = DB::table('v_ith_tblc')->select(
                                            'ITH_SER',
                                            'ITH_WH',
                                            'ITH_DOC',
                                            'ITH_DATEC',
                                            DB::raw('SUM(ITH_QTY) AS QTY')
                                        )
                                            ->where('ITH_DOC', $cekHist->DOC_NO)
                                            ->whereIn('ITH_WH', ['ARPRD1'])
                                            ->groupBy(
                                                'ITH_SER',
                                                'ITH_WH',
                                                'ITH_DOC',
                                                'ITH_DATEC'
                                            )
                                            ->havingRaw('SUM(ITH_QTY) >= ' . $cekHist->QTY)
                                            ->orderBy('ITH_DATEC', 'ASC')
                                            ->first();

                                        if (!empty($cekOsITHDataOnPrd)) {
                                            $redis->publish('message', json_encode([
                                                'app' => 'fix_scrap_ith',
                                                'result' => [
                                                    'status' => false,
                                                    'data' => [],
                                                    'menu' => $cekHist->MENU_ID,
                                                    'message' => $cekHist->ID_TRANS . ' Doc ' . $cekHist->DOC_NO . ' stock still exists on ARPRD1 ' . $cekOsITHDataOnPrd->QTY . ', please transfer to ARQA1 !!',
                                                ],
                                                'payload' => [
                                                    'fix' => $this->fix,
                                                    'item_code' => $this->item_code,
                                                    'item_lot' => $this->item_lot,
                                                    'id' => $this->id,
                                                    'id_det' => $this->id_det
                                                ]
                                            ]));
                                        } else {
                                            $redis->publish('message', json_encode([
                                                'app' => 'fix_scrap_ith',
                                                'result' => [
                                                    'status' => false,
                                                    'data' => [],
                                                    'menu' => $cekHist->MENU_ID,
                                                    'message' => $cekHist->ID_TRANS . ' Doc ' . $cekHist->DOC_NO . ' ITH Stock already 0 ! !',
                                                ],
                                                'payload' => [
                                                    'fix' => $this->fix,
                                                    'item_code' => $this->item_code,
                                                    'item_lot' => $this->item_lot,
                                                    'id' => $this->id,
                                                    'id_det' => $this->id_det
                                                ]
                                            ]));
                                        }
                                    }
                                }

                                // Kurangin stok produksi
                                $cekHistDet = scrapHistDet::where('SCR_HIST_ID', base64_decode($id))->get();
                                foreach ($cekHistDet as $keyDet => $valueDet) {
                                    $cekHistITHDet = ITHMaster::select(
                                        DB::raw('SUM(ITH_QTY) AS QTY'),
                                        'ITH_DOC',
                                        'ITH_WH'
                                    )
                                        ->where('ITH_ITMCD', $valueDet->SCR_ITMCD)
                                        ->where('ITH_DOC', 'LIKE', $valueDet->SCR_PSN . '%')
                                        ->whereIn('ITH_WH', ['PLANT1', 'PLANT2', 'PLANT_NA'])
                                        ->groupBy(
                                            'ITH_DOC',
                                            'ITH_WH'
                                        )
                                        ->first();

                                    if (!empty($cekHistITHDet->QTY) && $cekHistITHDet->QTY > 0) {
                                        $issue = $this->insertToITH(
                                            trim($valueDet->SCR_ITMCD),
                                            date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                            'OUT-WIP-SCR-RM',
                                            $cekHistITHDet->ITH_DOC,
                                            round($valueDet->SCR_QTY) * -1,
                                            $cekHistITHDet->ITH_WH,
                                            $cekHist->REF_NO,
                                            base64_decode($id),
                                            'SCR_RPT',
                                            $getTimeCreated,
                                            $valueDet->id
                                        );

                                        // Receive
                                        $receive = $this->insertToITH(
                                            trim($valueDet->SCR_ITMCD),
                                            date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                            'INC-SCR-RM',
                                            $cekHist->ID_TRANS,
                                            round($valueDet->SCR_QTY),
                                            'ARWH9SC',
                                            $cekHist->REF_NO,
                                            base64_decode($id),
                                            'SCR_RPT',
                                            $getTimeCreated,
                                            $valueDet->id
                                        );

                                        $redis->publish('message', json_encode([
                                            'app' => 'fix_scrap_ith',
                                            'result' => [
                                                'status' => true,
                                                'data' => [
                                                    'ISSUE' => [
                                                        'ITH_ITMCD' => trim($valueDet->SCR_ITMCD),
                                                        'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                        'ITH_FORM' => 'OUT-WIP-SCR-RM',
                                                        'ITH_DOC' => $cekHistITHDet->ITH_DOC,
                                                        'ITH_QTY' => round($cekHistITHDet->ITH_QTY) * -1,
                                                        'ITH_WH' => $cekHistITHDet->ITH_WH,
                                                        'ITH_SER' => $cekHist->REF_NO,
                                                        'ITH_REMARK' => $valueDet->id,
                                                        'ITH_USRID' => 'SCR_RPT',
                                                    ],
                                                    'RECEIVE' => [
                                                        'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                        'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                        'ITH_FORM' => 'INC-SCR-RM',
                                                        'ITH_DOC' => $cekHist->ID_TRANS,
                                                        'ITH_QTY' => round($cekHistITHDet->ITH_QTY),
                                                        'ITH_WH' => 'ARWH9SC',
                                                        'ITH_SER' => $cekHist->REF_NO,
                                                        'ITH_REMARK' => $valueDet->id,
                                                        'ITH_USRID' => 'SCR_RPT',
                                                    ]
                                                ],
                                                'menu' => $cekHist->MENU_ID,
                                                'message' => $cekHist->ID_TRANS . ' item ' . trim($cekHist->ITEMNUM) . ' for WIP, has been moved to Scrap !',
                                            ],
                                            'payload' => [
                                                'fix' => $this->fix,
                                                'item_code' => $this->item_code,
                                                'item_lot' => $this->item_lot,
                                                'id' => $this->id,
                                                'id_det' => $this->id_det,
                                                'doc' => $cekHist->ID_TRANS
                                            ]
                                        ]));
                                    } else {
                                        $redis->publish('message', json_encode([
                                            'app' => 'fix_scrap_ith',
                                            'result' => [
                                                'status' => false,
                                                'data' => [],
                                                'menu' => $cekHist->MENU_ID,
                                                'message' => $cekHist->ITEMNUM . ' Scrap WIP Stock not found at PLANT1, PLANT2, PLANT_NA !',
                                            ],
                                            'payload' => [
                                                'fix' => $this->fix,
                                                'item_code' => $this->item_code,
                                                'item_lot' => $this->item_lot,
                                                'id' => $this->id,
                                                'id_det' => $this->id_det,
                                                'doc' => $cekHist->ID_TRANS
                                            ]
                                        ]));
                                    }
                                }
                            } elseif ($cekHist->MENU_ID === 'warehouse') {
                                $cekHistITH = ITHMaster::select(
                                    'ITH_WH',
                                    'ITH_DOC',
                                    'ITH_LOC',
                                    DB::raw('CAST(ITH_LUPDT AS date) AS ITH_LUPDT'),
                                    DB::raw('SUM(ITH_QTY) AS ITH_QTY')
                                )
                                    ->where('ITH_SER', $cekHist->REF_NO)
                                    ->whereIn('ITH_WH', $cekHist->MDL_FLAG == 1 ? ['AFWH3', 'AWIP1'] : ['ARWH1', 'ARWH2', 'NRWH2', 'AIWH1'])
                                    ->groupBy(
                                        'ITH_WH',
                                        'ITH_DOC',
                                        'ITH_LOC',
                                        DB::raw('CAST(ITH_LUPDT AS date)')
                                    )
                                    ->havingRaw('SUM(ITH_QTY) > 0')
                                    ->orderBy(DB::raw('CAST(ITH_LUPDT AS date)'), 'DESC')
                                    ->first();
                                // return $cekHistITH;
                                if (!empty($cekHistITH->ITH_QTY) && $cekHistITH->ITH_QTY > 0) {
                                    $cekHistDet = $cekHistITH;
                                    // return $cekHistDet;

                                    $issue = $this->insertToITH(
                                        trim($cekHist->ITEMNUM),
                                        date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                        'OUT-WH-SCR-FG',
                                        $cekHistDet->ITH_DOC,
                                        round($cekHistDet->ITH_QTY) * -1,
                                        $cekHistDet->ITH_WH,
                                        $cekHist->REF_NO,
                                        $cekHist->id,
                                        'SCR_RPT',
                                        $getTimeCreated,
                                        $cekHistDet->ITH_LOC,
                                    );

                                    // Receive
                                    $receive = $this->insertToITH(
                                        trim($cekHist->ITEMNUM),
                                        date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                        'INC-SCR-FG',
                                        $cekHist->ID_TRANS,
                                        round($cekHistDet->ITH_QTY),
                                        'AFWH9SC',
                                        $cekHist->REF_NO,
                                        $cekHist->id,
                                        'SCR_RPT',
                                        $getTimeCreated
                                    );

                                    $redis->publish('message', json_encode([
                                        'app' => 'fix_scrap_ith',
                                        'result' => [
                                            'status' => true,
                                            'data' => [
                                                'ISSUE' => [
                                                    'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                    'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                    'ITH_FORM' => 'OUT-WH-SCR-FG',
                                                    'ITH_DOC' => $cekHistDet->ITH_DOC,
                                                    'ITH_QTY' => round($cekHistDet->ITH_QTY) * -1,
                                                    'ITH_WH' => $cekHistDet->ITH_WH,
                                                    'ITH_SER' => $cekHist->REF_NO,
                                                    'ITH_REMARK' => $cekHist->id,
                                                    'ITH_LOC' => $cekHist->ITH_LOC,
                                                    'ITH_USRID' => 'SCR_RPT'
                                                ],
                                                'RECEIVE' => [
                                                    'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                    'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                    'ITH_FORM' => 'INC-SCR-FG',
                                                    'ITH_DOC' => $cekHist->ID_TRANS,
                                                    'ITH_QTY' => round($cekHistDet->ITH_QTY),
                                                    'ITH_WH' => 'AFWH9SC',
                                                    'ITH_SER' => $cekHist->REF_NO,
                                                    'ITH_REMARK' => $cekHist->id,
                                                    'ITH_USRID' => 'SCR_RPT'
                                                ],
                                            ],
                                            'data_ori' => ITHMaster::select(
                                                'ITH_WH',
                                                'ITH_DOC',
                                                DB::raw('SUM(ITH_QTY) AS QTY')
                                            )
                                                ->where('ITH_SER', $cekHist->REF_NO)
                                                ->whereIn('ITH_WH', $cekHist->MDL_FLAG == 1 ? ['AFWH3', 'AWIP1'] : ['ARWH1', 'ARWH2', 'NRWH2', 'AIWH1'])
                                                ->groupBy(
                                                    'ITH_WH',
                                                    'ITH_DOC'
                                                )
                                                ->havingRaw('SUM(ITH_QTY) > 0')
                                                ->get(),
                                            'menu' => $cekHist->MENU_ID,
                                            'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . 'has been moved to Scrap !',
                                        ],
                                        'payload' => [
                                            'fix' => $this->fix,
                                            'item_code' => $this->item_code,
                                            'item_lot' => $this->item_lot,
                                            'id' => $this->id,
                                            'id_det' => $this->id_det,
                                            'doc' => $cekHist->ID_TRANS
                                        ]
                                    ]));
                                } else {
                                    $redis->publish('message', json_encode([
                                        'app' => 'fix_scrap_ith',
                                        'result' => [
                                            'status' => false,
                                            'data' => $cekHistITH,
                                            'menu' => $cekHist->MENU_ID,
                                            'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . ' Stock already 0 ! !',
                                        ],
                                        'payload' => [
                                            'fix' => $this->fix,
                                            'item_code' => $this->item_code,
                                            'item_lot' => $this->item_lot,
                                            'id' => $this->id,
                                            'id_det' => $this->id_det,
                                            'doc' => $cekHist->ID_TRANS
                                        ]
                                    ]));
                                }
                            } else {
                                if ($cekHist->QTY_SCRAP == 0 && $cekHist->IS_RETURN == 0) {
                                    $cekHistITH = ITHMaster::select(
                                        'ITH_SER',
                                        'ITH_DOC',
                                        'ITH_WH',
                                        'ITH_DATE',
                                        DB::raw('SUM(ITH_QTY) AS QTY')
                                    )
                                        ->where('ITH_DOC', $cekHist->DOC_NO)
                                        ->where('ITH_ITMCD', $cekHist->ITEMNUM)
                                        ->where('ITH_DATE', '<=', $cekHist->IS_CONFIRMED)
                                        ->whereIn('ITH_WH', ['ARQA1'])
                                        ->groupBy('ITH_SER', 'ITH_DATE', 'ITH_WH', 'ITH_DOC')
                                        ->havingRaw('SUM(ITH_QTY) = ' . round($cekHist->QTY))
                                        ->orderBy('ITH_DATE')
                                        ->first();

                                    // return $cekHistITH;

                                    if (!empty($cekHistITH->QTY) && $cekHistITH->QTY > 0) {
                                        $issue = $this->insertToITH(
                                            trim($cekHist->ITEMNUM),
                                            date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                            'OUT-WH-SCR-FG',
                                            $cekHistITH->ITH_DOC,
                                            round($cekHist->QTY) * -1,
                                            $cekHistITH->ITH_WH,
                                            $cekHistITH->ITH_SER,
                                            $cekHistITH->id,
                                            'SCR_RPT',
                                            $getTimeCreated
                                        );

                                        // Receive
                                        $receive = $this->insertToITH(
                                            trim($cekHist->ITEMNUM),
                                            date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                            'INC-SCR-FG',
                                            $cekHist->ID_TRANS,
                                            round($cekHist->QTY),
                                            'AFWH9SC',
                                            $cekHistITH->ITH_SER,
                                            $cekHist->id,
                                            'SCR_RPT',
                                            $getTimeCreated
                                        );

                                        $redis->publish('message', json_encode([
                                            'app' => 'fix_scrap_ith',
                                            'result' => [
                                                'status' => true,
                                                'data' => [
                                                    'ISSUE' => [
                                                        'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                        'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                        'ITH_FORM' => 'OUT-WH-SCR-FG',
                                                        'ITH_DOC' => $cekHistITH->ITH_DOC,
                                                        'ITH_QTY' => round($cekHist->QTY) * -1,
                                                        'ITH_WH' => $cekHistITH->ITH_WH,
                                                        'ITH_SER' => $cekHistITH->ITH_SER,
                                                        'ITH_REMARK' => $cekHistITH->id,
                                                        'ITH_USRID' => 'SCR_RPT'
                                                    ],
                                                    'RECEIVE' => [
                                                        'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                        'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                        'ITH_FORM' => 'INC-SCR-FG',
                                                        'ITH_DOC' => $cekHist->ID_TRANS,
                                                        'ITH_QTY' => round($cekHist->QTY),
                                                        'ITH_WH' => 'AFWH9SC',
                                                        'ITH_SER' => $cekHistITH->ITH_SER,
                                                        'ITH_REMARK' => $cekHist->id,
                                                        'ITH_USRID' => 'SCR_RPT'
                                                    ]
                                                ],
                                                'menu' => $cekHist->MENU_ID,
                                                'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . 'has been moved to Scrap !',
                                            ],
                                            'payload' => [
                                                'fix' => $this->fix,
                                                'item_code' => $this->item_code,
                                                'item_lot' => $this->item_lot,
                                                'id' => $this->id,
                                                'id_det' => $this->id_det,
                                                'doc' => $cekHist->ID_TRANS
                                            ]
                                        ]));

                                        return [
                                            'issue' => $issue,
                                            'receive' => $receive,
                                            'status' => true,
                                            'message' => 'Stock berhasil di pindah !'
                                        ];
                                    } else {
                                        $redis->publish('message', json_encode([
                                            'app' => 'fix_scrap_ith',
                                            'result' => [
                                                'status' => false,
                                                'data' => [],
                                                'menu' => $cekHist->MENU_ID,
                                                'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . ' Stock already 0 ! !',
                                            ],
                                            'payload' => [
                                                'fix' => $this->fix,
                                                'item_code' => $this->item_code,
                                                'item_lot' => $this->item_lot,
                                                'id' => $this->id,
                                                'id_det' => $this->id_det,
                                                'doc' => $cekHist->ID_TRANS
                                            ]
                                        ]));
                                    }
                                } else {
                                    $redis->publish('message', json_encode([
                                        'app' => 'fix_scrap_ith',
                                        'result' => [
                                            'status' => false,
                                            'data' => [],
                                            'menu' => $cekHist->MENU_ID,
                                            'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . ' Except WIP, Warehouse or Pending, Not identified !!',
                                        ],
                                        'payload' => [
                                            'fix' => $this->fix,
                                            'item_code' => $this->item_code,
                                            'item_lot' => $this->item_lot,
                                            'id' => $this->id,
                                            'id_det' => $this->id_det,
                                            'doc' => $cekHist->ID_TRANS
                                        ]
                                    ]));
                                }
                            }
                        }
                    } else if ($cekHist->MDL_FLAG == 0) { // If Material
                        if ($cekHist->MENU_ID === 'pending') {
                            if (!empty($cekHist->REF_NO)) {
                                $cekHistITH = ITHMaster::select(DB::raw('SUM(ITH_QTY) AS QTY'))->where('ITH_DOC', $cekHist->REF_NO)->whereIn('ITH_WH', ['QA', 'ARWH0PD'])->first();

                                // return $cekHistITH;
                                if (!empty($cekHistITH->QTY) && $cekHistITH->QTY > 0) {
                                    $cekHistDet = ITHMaster::where('ITH_DOC', $cekHist->REF_NO)->whereIn('ITH_WH', ['QA', 'ARWH0PD'])->orderBy('ITH_LUPDT', 'desc')->first();

                                    $issue = $this->insertToITH(
                                        trim($cekHist->ITEMNUM),
                                        date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                        'OUT-PEN-SCR-RM',
                                        $cekHistDet->ITH_DOC,
                                        round($cekHist->QTY) * -1,
                                        $cekHistDet->ITH_WH,
                                        $cekHist->REF_NO,
                                        $cekHist->id,
                                        'SCR_RPT',
                                        $getTimeCreated
                                    );

                                    // Receive
                                    $receive = $this->insertToITH(
                                        trim($cekHist->ITEMNUM),
                                        date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                        'INC-SCR-RM',
                                        $cekHist->ID_TRANS,
                                        round($cekHist->QTY),
                                        'ARWH9SC',
                                        $cekHist->REF_NO,
                                        $cekHist->id,
                                        'SCR_RPT',
                                        $getTimeCreated
                                    );

                                    $redis->publish('message', json_encode([
                                        'app' => 'fix_scrap_ith',
                                        'result' => [
                                            'status' => true,
                                            'data' => [
                                                'ISSUE' => [
                                                    'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                    'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                    'ITH_FORM' => 'OUT-PEN-FG',
                                                    'ITH_DOC' => $cekHistDet->ITH_DOC,
                                                    'ITH_QTY' => round($cekHist->QTY) * -1,
                                                    'ITH_WH' => $cekHistDet->ITH_WH,
                                                    'ITH_SER' => $cekHist->REF_NO,
                                                    'ITH_REMARK' => $cekHist->id,
                                                    'ITH_USRID' => 'SCR_RPT',
                                                ],
                                                'RECEIVE' => [
                                                    'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                    'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                    'ITH_FORM' => 'INC-SCR-FG',
                                                    'ITH_DOC' => $cekHist->ID_TRANS,
                                                    'ITH_QTY' => round($cekHist->QTY),
                                                    'ITH_WH' => 'AFWH9SC',
                                                    'ITH_SER' => $cekHist->REF_NO,
                                                    'ITH_REMARK' => $cekHist->id,
                                                    'ITH_USRID' => 'SCR_RPT'
                                                ]
                                            ],
                                            'menu' => $cekHist->MENU_ID,
                                            'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . ' QTY => ' . $cekHist->QTY . ' has been moved to Scrap !',
                                        ],
                                        'payload' => [
                                            'fix' => $this->fix,
                                            'item_code' => $this->item_code,
                                            'item_lot' => $this->item_lot,
                                            'id' => $this->id,
                                            'id_det' => $this->id_det,
                                            'doc' => $cekHist->ID_TRANS
                                        ]
                                    ]));
                                } else {
                                    $redis->publish('message', json_encode([
                                        'app' => 'fix_scrap_ith',
                                        'result' => [
                                            'status' => false,
                                            'data' => [],
                                            'menu' => $cekHist->MENU_ID,
                                            'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . ' Ref not found !',
                                        ],
                                        'payload' => [
                                            'fix' => $this->fix,
                                            'item_code' => $this->item_code,
                                            'item_lot' => $this->item_lot,
                                            'id' => $this->id,
                                            'id_det' => $this->id_det,
                                            'doc' => $cekHist->ID_TRANS
                                        ]
                                    ]));
                                }
                            } else {
                                $cekHistITHItem = ITHMaster::select(DB::raw('SUM(ITH_QTY) AS QTY'))
                                    ->where('ITH_ITMCD', $cekHist->ITEMNUM)
                                    ->whereIn('ITH_WH', ['QA', 'ARWH0PD'])
                                    ->first();

                                if (!empty($cekHistITHItem) && $cekHistITHItem >= $cekHist->QTY) {
                                    $cekHistITHItemList = DB::table('v_ith_tblc')
                                        ->select(
                                            'ITH_ITMCD',
                                            'ITH_WH',
                                            'ITH_DATEC',
                                            'ITH_DOC',
                                            DB::raw('SUM(ITH_QTY) AS ITH_QTY')
                                        )->where('ITH_ITMCD', $cekHist->ITEMNUM)
                                        ->whereIn('ITH_WH', ['QA', 'ARWH0PD'])
                                        ->orderBy('ITH_DATEC', 'desc')
                                        ->groupBy(
                                            'ITH_ITMCD',
                                            'ITH_WH',
                                            'ITH_DATEC',
                                            'ITH_DOC'
                                        )
                                        ->get();

                                    $getData = $this->ITHRecursQTY($cekHistITHItemList->all(), $cekHist->QTY);

                                    foreach ($getData as $keyRecITH => $valueRecITH) {
                                        $issue = $this->insertToITH(
                                            trim($cekHist->ITEMNUM),
                                            date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                            'OUT-PEN-SCR-RM',
                                            $valueRecITH->ITH_DOC,
                                            round($valueRecITH->ITH_QTY) * -1,
                                            $valueRecITH->ITH_WH,
                                            $cekHist->REF_NO,
                                            $cekHist->id,
                                            'SCR_RPT',
                                            $getTimeCreated
                                        );

                                        // Receive
                                        $receive = $this->insertToITH(
                                            trim($cekHist->ITEMNUM),
                                            date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                            'INC-SCR-RM',
                                            $cekHist->ID_TRANS,
                                            round($valueRecITH->ITH_QTY),
                                            'ARWH9SC',
                                            $cekHist->REF_NO,
                                            $cekHist->id,
                                            'SCR_RPT',
                                            $getTimeCreated
                                        );

                                        $redis->publish('message', json_encode([
                                            'app' => 'fix_scrap_ith',
                                            'result' => [
                                                'status' => true,
                                                'data' => [
                                                    'ISSUE' => [
                                                        'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                        'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                        'ITH_FORM' => 'OUT-PEN-SCR-RM',
                                                        'ITH_DOC' => $valueRecITH->ITH_DOC,
                                                        'ITH_QTY' => round($valueRecITH->ITH_QTY) * -1,
                                                        'ITH_WH' => $valueRecITH->ITH_WH,
                                                        'ITH_SER' => $cekHist->REF_NO,
                                                        'ITH_REMARK' => $cekHist->id,
                                                        'ITH_USRID' => 'SCR_RPT',
                                                    ],
                                                    'RECEIVE' => [
                                                        'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                        'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                        'ITH_FORM' => 'INC-SCR-RM',
                                                        'ITH_DOC' => $cekHist->ID_TRANS,
                                                        'ITH_QTY' => round($valueRecITH->ITH_QTY),
                                                        'ITH_WH' => 'ARWH9SC',
                                                        'ITH_SER' => $cekHist->REF_NO,
                                                        'ITH_REMARK' => $cekHist->id,
                                                        'ITH_USRID' => 'SCR_RPT'
                                                    ]
                                                ],
                                                'menu' => $cekHist->MENU_ID,
                                                'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . 'has been moved to Scrap !',
                                            ],
                                            'payload' => [
                                                'fix' => $this->fix,
                                                'item_code' => $this->item_code,
                                                'item_lot' => $this->item_lot,
                                                'id' => $this->id,
                                                'id_det' => $this->id_det,
                                                'doc' => $cekHist->ID_TRANS
                                            ]
                                        ]));
                                    }
                                } else {
                                    $redis->publish('message', json_encode([
                                        'app' => 'fix_scrap_ith',
                                        'result' => [
                                            'status' => false,
                                            'data' => [],
                                            'menu' => $cekHist->MENU_ID,
                                            'message' => $cekHist->ID_TRANS . ' item ' . $cekHist->ITEMNUM . ' This item on Pending location only ' . $cekHistITHItem->QTY . ' with requirement ' . $cekHist->QTY . ' !',
                                        ],
                                        'payload' => [
                                            'fix' => $this->fix,
                                            'item_code' => $this->item_code,
                                            'item_lot' => $this->item_lot,
                                            'id' => $this->id,
                                            'id_det' => $this->id_det,
                                            'doc' => $cekHist->ID_TRANS
                                        ]
                                    ]));
                                }
                            }
                        } elseif ($cekHist->MENU_ID === 'warehouse') {
                            $cekHistITH = ITHMaster::select(
                                DB::raw('SUM(ITH_QTY) AS QTY')
                            )
                                ->where('ITH_DOC', $cekHist->DOC_NO)
                                ->where('ITH_ITMCD', $cekHist->ITEMNUM)
                                ->whereIn('ITH_WH', ['ARWH1', 'ARWH2', 'AIWH1', 'NRWH2'])
                                ->havingRaw('SUM(ITH_QTY) >= ' . $cekHist->QTY)
                                ->first();

                            if (!empty($cekHistITH)) {
                                $cekHistDet = ITHMaster::where('ITH_SER', $cekHist->REF_NO)
                                    ->where('ITH_DOC', $cekHist->DOC_NO)
                                    ->where('ITH_ITMCD', $cekHist->ITEMNUM)
                                    ->whereIn('ITH_WH', ['ARWH1', 'ARWH2', 'AIWH1', 'NRWH2'])
                                    ->orderBy('ITH_LUPDT', 'desc')
                                    ->first();

                                $issue = $this->insertToITH(
                                    trim($cekHist->ITEMNUM),
                                    date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                    'OUT-WH-SCR-RM',
                                    $cekHistDet->ITH_DOC,
                                    round($cekHist->QTY) * -1,
                                    $cekHistDet->ITH_WH,
                                    $cekHist->REF_NO,
                                    $cekHist->id,
                                    'SCR_RPT',
                                    $getTimeCreated
                                );

                                // Receive
                                $receive = $this->insertToITH(
                                    trim($cekHist->ITEMNUM),
                                    date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                    'INC-SCR-RM',
                                    $cekHist->ID_TRANS,
                                    round($cekHist->QTY),
                                    'ARWH9SC',
                                    $cekHist->REF_NO,
                                    $cekHist->id,
                                    'SCR_RPT',
                                    $getTimeCreated
                                );

                                $redis->publish('message', json_encode([
                                    'app' => 'fix_scrap_ith',
                                    'result' => [
                                        'status' => true,
                                        'data' => [
                                            'ISSUE' => [
                                                'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                'ITH_FORM' => 'OUT-WH-SCR-RM',
                                                'ITH_DOC' => $cekHistDet->ITH_DOC,
                                                'ITH_QTY' => round($cekHist->QTY) * -1,
                                                'ITH_WH' => $cekHistDet->ITH_WH,
                                                'ITH_SER' => $cekHist->REF_NO,
                                                'ITH_REMARK' => $cekHist->id,
                                                'ITH_USRID' => 'SCR_RPT'
                                            ],
                                            'RECEIVE' => [
                                                'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                'ITH_FORM' => 'INC-SCR-RM',
                                                'ITH_DOC' => $cekHist->ID_TRANS,
                                                'ITH_QTY' => round($cekHist->QTY),
                                                'ITH_WH' => 'ARWH9SC',
                                                'ITH_SER' => $cekHist->REF_NO,
                                                'ITH_REMARK' => $cekHist->id,
                                                'ITH_USRID' => 'SCR_RPT'
                                            ]
                                        ],
                                        'menu' => $cekHist->MENU_ID,
                                        'message' => $cekHist->ID_TRANS . ' DOC ' . $cekHistDet->ITH_DOC . 'has been moved to Scrap !',
                                    ],
                                    'payload' => [
                                        'fix' => $this->fix,
                                        'item_code' => $this->item_code,
                                        'item_lot' => $this->item_lot,
                                        'id' => $this->id,
                                        'id_det' => $this->id_det,
                                        'doc' => $cekHist->ID_TRANS
                                    ]
                                ]));
                            } else {
                                $cekHistITHItemOnly = ITHMaster::select(
                                    DB::raw('SUM(ITH_QTY) AS QTY')
                                )
                                    ->where('ITH_ITMCD', $cekHist->ITEMNUM)
                                    ->whereIn('ITH_WH', ['ARWH1', 'ARWH2', 'AIWH1', 'NRWH2'])
                                    ->havingRaw('SUM(ITH_QTY) >= ' . $cekHist->QTY)
                                    ->first();

                                if (!empty($cekHistITHItemOnly)) {
                                    $cekHistDet = ITHMaster::where('ITH_SER', $cekHist->REF_NO)
                                        ->where('ITH_ITMCD', $cekHist->ITEMNUM)
                                        ->whereIn('ITH_WH', ['ARWH1', 'ARWH2', 'AIWH1', 'NRWH2'])
                                        ->orderBy('ITH_LUPDT', 'desc')
                                        ->first();

                                    $issue = $this->insertToITH(
                                        trim($cekHist->ITEMNUM),
                                        date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                        'OUT-WH-SCR-RM',
                                        $cekHistDet->ITH_DOC,
                                        round($cekHist->QTY) * -1,
                                        $cekHistDet->ITH_WH,
                                        $cekHist->REF_NO,
                                        $cekHist->id,
                                        'SCR_RPT',
                                        $getTimeCreated
                                    );

                                    // Receive
                                    $receive = $this->insertToITH(
                                        trim($cekHist->ITEMNUM),
                                        date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                        'INC-SCR-RM',
                                        $cekHist->ID_TRANS,
                                        round($cekHist->QTY),
                                        'ARWH9SC',
                                        $cekHist->REF_NO,
                                        $cekHist->id,
                                        'SCR_RPT',
                                        $getTimeCreated
                                    );

                                    $redis->publish('message', json_encode([
                                        'app' => 'fix_scrap_ith',
                                        'result' => [
                                            'status' => true,
                                            'data' => [
                                                'ISSUE' => [
                                                    'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                    'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                    'ITH_FORM' => 'OUT-WH-SCR-RM',
                                                    'ITH_DOC' => $cekHistDet->ITH_DOC,
                                                    'ITH_QTY' => round($cekHist->QTY) * -1,
                                                    'ITH_WH' => $cekHistDet->ITH_WH,
                                                    'ITH_SER' => $cekHist->REF_NO,
                                                    'ITH_REMARK' => $cekHist->id,
                                                    'ITH_USRID' => 'SCR_RPT'
                                                ],
                                                'RECEIVE' => [
                                                    'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                    'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                    'ITH_FORM' => 'INC-SCR-RM',
                                                    'ITH_DOC' => $cekHist->ID_TRANS,
                                                    'ITH_QTY' => round($cekHist->QTY),
                                                    'ITH_WH' => 'ARWH9SC',
                                                    'ITH_SER' => $cekHist->REF_NO,
                                                    'ITH_REMARK' => $cekHist->id,
                                                    'ITH_USRID' => 'SCR_RPT'
                                                ]
                                            ],
                                            'menu' => $cekHist->MENU_ID,
                                            'message' => $cekHist->ID_TRANS . ' DOC ' . $cekHistDet->ITH_DOC . 'has been moved to Scrap !',
                                        ],
                                        'payload' => [
                                            'fix' => $this->fix,
                                            'item_code' => $this->item_code,
                                            'item_lot' => $this->item_lot,
                                            'id' => $this->id,
                                            'id_det' => $this->id_det,
                                            'doc' => $cekHist->ID_TRANS
                                        ]
                                    ]));
                                } else {
                                    $redis->publish('message', json_encode([
                                        'app' => 'fix_scrap_ith',
                                        'result' => [
                                            'status' => false,
                                            'data' => [],
                                            'menu' => $cekHist->MENU_ID,
                                            'message' => $cekHist->ID_TRANS . ' doc ' . $cekHist->DOC_NO . ' Material Resubmit ITH Stock not found !',
                                        ],
                                        'payload' => [
                                            'fix' => $this->fix,
                                            'item_code' => $this->item_code,
                                            'item_lot' => $this->item_lot,
                                            'id' => $this->id,
                                            'id_det' => $this->id_det,
                                            'doc' => $cekHist->ID_TRANS
                                        ]
                                    ]));
                                }
                            }
                        } elseif ($cekHist->MENU_ID === 'loss_part') {
                            $cekHistITH = ITHMaster::select(
                                'ITH_DOC',
                                'ITH_WH',
                                DB::raw('SUM(ITH_QTY) AS QTY')
                            )
                                ->where('ITH_DOC', 'LIKE', $cekHist->DOC_NO . '%')
                                ->where('ITH_ITMCD', $cekHist->ITEMNUM)
                                ->whereIn('ITH_WH', ['PLANT1', 'PLANT2', 'PLANT_NA'])
                                ->havingRaw('SUM(ITH_QTY) >= ' . $cekHist->QTY)
                                ->groupBy('ITH_DOC', 'ITH_WH')
                                ->first();

                            if (!empty($cekHistITH)) {
                                $issue = $this->insertToITH(
                                    trim($cekHist->ITEMNUM),
                                    date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                    'OUT-LOSS-SCR-RM',
                                    $cekHistITH->ITH_DOC,
                                    round($cekHist->QTY) * -1,
                                    $cekHistITH->ITH_WH,
                                    $cekHist->REF_NO,
                                    $cekHist->id,
                                    'SCR_RPT',
                                    $getTimeCreated
                                );

                                // Receive
                                $receive = $this->insertToITH(
                                    trim($cekHist->ITEMNUM),
                                    date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                    'INC-SCR-RM',
                                    $cekHist->ID_TRANS,
                                    round($cekHist->QTY),
                                    'ARWH9SC',
                                    $cekHist->REF_NO,
                                    $cekHist->id,
                                    'SCR_RPT',
                                    $getTimeCreated
                                );

                                $redis->publish('message', json_encode([
                                    'app' => 'fix_scrap_ith',
                                    'result' => [
                                        'status' => true,
                                        'data' => [
                                            'ISSUE' => [
                                                'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                'ITH_FORM' => 'OUT-WH-SCR-RM',
                                                'ITH_DOC' => $cekHistITH->ITH_DOC,
                                                'ITH_QTY' => round($cekHist->QTY) * -1,
                                                'ITH_WH' => $cekHistITH->ITH_WH,
                                                'ITH_SER' => $cekHist->REF_NO,
                                                'ITH_REMARK' => $cekHist->id,
                                                'ITH_USRID' => 'SCR_RPT'
                                            ],
                                            'RECEIVE' => [
                                                'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                'ITH_FORM' => 'INC-SCR-RM',
                                                'ITH_DOC' => $cekHist->ID_TRANS,
                                                'ITH_QTY' => round($cekHist->QTY),
                                                'ITH_WH' => 'ARWH9SC',
                                                'ITH_SER' => $cekHist->REF_NO,
                                                'ITH_REMARK' => $cekHist->id,
                                                'ITH_USRID' => 'SCR_RPT'
                                            ]
                                        ],
                                        'menu' => $cekHist->MENU_ID,
                                        'message' => $cekHist->ID_TRANS . ' DOC ' . $cekHistITH->ITH_DOC . 'has been moved to Scrap !',
                                    ],
                                    'payload' => [
                                        'fix' => $this->fix,
                                        'item_code' => $this->item_code,
                                        'item_lot' => $this->item_lot,
                                        'id' => $this->id,
                                        'id_det' => $this->id_det,
                                        'doc' => $cekHist->ID_TRANS
                                    ]
                                ]));
                            } else {
                                $redis->publish('message', json_encode([
                                    'app' => 'fix_scrap_ith',
                                    'result' => [
                                        'status' => true,
                                        'data' => [],
                                        'menu' => $cekHist->MENU_ID,
                                        'message' => $cekHist->ID_TRANS . ' DOC ' . $cekHistITH->ITH_DOC . ' stock not found on PLANT1 / PLANT2 Please check again !',
                                    ],
                                    'payload' => [
                                        'fix' => $this->fix,
                                        'item_code' => $this->item_code,
                                        'item_lot' => $this->item_lot,
                                        'id' => $this->id,
                                        'id_det' => $this->id_det,
                                        'doc' => $cekHist->ID_TRANS
                                    ]
                                ]));
                            }
                        }
                    } else {
                        if ($cekHist->MENU_ID === 'mchequip') {
                            $cekHistITH = ITHMaster::select(
                                'ITH_DOC',
                                'ITH_FORM',
                                'ITH_WH',
                                DB::raw('SUM(ITH_QTY) AS QTY')
                            )->where('ITH_ITMCD', $cekHist->ITEMNUM)
                                ->whereIn('ITH_WH', ['PSIEQUIP'])
                                ->havingRaw('SUM(ITH_QTY) > 0')
                                ->groupBy(
                                    'ITH_DOC',
                                    'ITH_FORM',
                                    'ITH_WH',
                                )
                                ->first();

                            if (!empty($cekHistITH) && $cekHistITH->QTY > 0) {
                                $issue = $this->insertToITH(
                                    trim($cekHist->ITEMNUM),
                                    date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                    'OUT-WH-SCR-MCH',
                                    $cekHistITH->ITH_DOC,
                                    round($cekHist->QTY) * -1,
                                    $cekHistITH->ITH_WH,
                                    '',
                                    $cekHist->id,
                                    'SCR_RPT',
                                    $getTimeCreated
                                );

                                // Receive
                                $receive = $this->insertToITH(
                                    trim($cekHist->ITEMNUM),
                                    date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                    'INC-SCR-RM',
                                    $cekHist->ID_TRANS,
                                    round($cekHist->QTY),
                                    'NRWH9SC',
                                    $cekHist->REF_NO,
                                    $cekHist->id,
                                    'SCR_RPT',
                                    $getTimeCreated
                                );

                                $redis->publish('message', json_encode([
                                    'app' => 'fix_scrap_ith',
                                    'result' => [
                                        'status' => true,
                                        'data' => [
                                            'ISSUE' => [
                                                'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                'ITH_FORM' => 'OUT-PEN-FG',
                                                'ITH_DOC' => $cekHistITH->ITH_DOC,
                                                'ITH_QTY' => round($cekHist->QTY) * -1,
                                                'ITH_WH' => $cekHistITH->ITH_WH,
                                                'ITH_SER' => '',
                                                'ITH_REMARK' => $cekHist->id,
                                                'ITH_USRID' => 'SCR_RPT',
                                            ],
                                            'RECEIVE' => [
                                                'ITH_ITMCD' => trim($cekHist->ITEMNUM),
                                                'ITH_DATE' => date('Y-m-d', strtotime($cekHist->IS_CONFIRMED)),
                                                'ITH_FORM' => 'INC-SCR-FG',
                                                'ITH_DOC' => $cekHist->ID_TRANS,
                                                'ITH_QTY' => round($cekHist->QTY),
                                                'ITH_WH' => 'AFWH9SC',
                                                'ITH_SER' => $cekHist->REF_NO,
                                                'ITH_REMARK' => $cekHist->id,
                                                'ITH_USRID' => 'SCR_RPT'
                                            ]
                                        ],
                                        'menu' => $cekHist->MENU_ID,
                                        'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . ' QTY => ' . $cekHist->QTY . ' has been moved to Scrap !',
                                    ],
                                    'payload' => [
                                        'fix' => $this->fix,
                                        'item_code' => $this->item_code,
                                        'item_lot' => $this->item_lot,
                                        'id' => $this->id,
                                        'id_det' => $this->id_det,
                                        'doc' => $cekHist->ID_TRANS
                                    ]
                                ]));
                            } else {
                                $redis->publish('message', json_encode([
                                    'app' => 'fix_scrap_ith',
                                    'result' => [
                                        'status' => false,
                                        'data' => [],
                                        'menu' => $cekHist->MENU_ID,
                                        'message' => $cekHist->ITEMNUM . ' Scrap machine & equipment Stock not found at PSIEQUIP !',
                                    ],
                                    'payload' => [
                                        'fix' => $this->fix,
                                        'item_code' => $this->item_code,
                                        'item_lot' => $this->item_lot,
                                        'id' => $this->id,
                                        'id_det' => $this->id_det,
                                        'doc' => $cekHist->ID_TRANS
                                    ]
                                ]));
                            }
                        } else {
                            $redis->publish('message', json_encode([
                                'app' => 'fix_scrap_ith',
                                'result' => [
                                    'status' => false,
                                    'data' => [],
                                    'menu' => $cekHist->MENU_ID,
                                    'message' => $cekHist->ID_TRANS . ' ref ' . $cekHist->REF_NO . ' Menu & item not defined !',
                                ],
                                'payload' => [
                                    'fix' => $this->fix,
                                    'item_code' => $this->item_code,
                                    'item_lot' => $this->item_lot,
                                    'id' => $this->id,
                                    'id_det' => $this->id_det,
                                    'doc' => $cekHist->ID_TRANS
                                ]
                            ]));
                        }
                    }
                }
            } else {
                $redis->publish('message', json_encode([
                    'app' => 'fix_scrap_ith',
                    'result' => [
                        'status' => false,
                        'data' => [],
                        'menu' => '',
                        'message' => base64_decode($id). ' ID Scrap not found or already submited ITH !',
                    ],
                    'payload' => [
                        'fix' => $this->fix,
                        'item_code' => $this->item_code,
                        'item_lot' => $this->item_lot,
                        'id' => $this->id,
                        'id_det' => $this->id_det,
                        'doc' => ''
                    ]
                ]));
            }
        }
    }

    public function insertToITH($item, $date, $form, $doc, $qty, $wh, $ser, $remark, $user, $timeLupdt = '', $loc = '')
    {
        return ITHMaster::insert([
            'ITH_ITMCD' => $item,
            'ITH_DATE' => $date,
            'ITH_FORM' => $form,
            'ITH_DOC' => $doc,
            'ITH_QTY' => $qty,
            'ITH_WH' => $wh,
            'ITH_LOC' => $loc,
            'ITH_SER' => $ser,
            'ITH_REMARK' => $remark,
            'ITH_LINE' => NULL,
            'ITH_LUPDT' => $date . ' ' . (empty($timeLupdt) ? date('H:i:s') : date('H:i:s', strtotime($timeLupdt))),
            'ITH_USRID' => $user,
        ]);
    }

    public function ITHRecursQTY($data, $qtySisa, $hasil = [])
    {
        // logger($data);
        $nowData = current($data);
        logger(json_encode($hasil));
        if (isset($nowData->ITH_QTY) && !empty($nowData->ITH_QTY)) {
            $total = $qtySisa - $nowData->ITH_QTY;
            if ($total >= 0) {
                $hasil[] = $nowData;
                next($data);
                return $this->ITHRecursQTY($data, $total, $hasil);
            } else {
                $nowData['ITH_QTY'] = $qtySisa;
                $hasil[] = $nowData;
            }
        }

        return $hasil;
    }
}
