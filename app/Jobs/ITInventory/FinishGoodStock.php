<?php

namespace App\Jobs\ITInventory;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Model\RPCUST\MutasiFinishGood;

class FinishGoodStock implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        MutasiFinishGood::updateOrCreate([
            'RPGOOD_REF' => $this->data['RPGOOD_REF'],
            'RPGOOD_ITMCOD' => $this->data['RPGOOD_ITMCOD']
        ],[
            'RPGOOD_UNITMS' => $this->data['RPGOOD_UNITMS'],
            'RPGOOD_QTYTOT' => $this->data['RPGOOD_QTYTOT'],
            'RPGOOD_QTYOUT' => $this->data['RPGOOD_QTYOUT'],
            'RPGOOD_QTYOPN' => $this->data['RPGOOD_QTYOPN'],
            'RPGOOD_QTYINC' => $this->data['RPGOOD_QTYINC'],
            'RPGOOD_QTYADJ' => $this->data['RPGOOD_QTYADJ'],
            'RPGOOD_KET' => $this->data['RPGOOD_KET'],
            'RPGOOD_ITMCOD' => $this->data['RPGOOD_ITMCOD'],
            'RPGOOD_DATEIS' => $this->data['RPGOOD_DATEIS'],
            'RPGOOD_SA' => $this->data['RPGOOD_SA'],
            'RPGOOD_REF' => $this->data['RPGOOD_REF']
        ]);
    }
}
