<?php

namespace App\Jobs\ITInventory;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Model\RPCUST\MutasiMesin;

class MesinPeralatanStock implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        MutasiMesin::updateOrCreate([
            'RPMCH_REF' => $this->data['RPMCH_REF'],
            'RPMCH_ITMCOD' => $this->data['RPMCH_ITMCOD']
        ],[
            'RPMCH_UNITMS' => $this->data['RPMCH_UNITMS'],
            'RPMCH_QTYTOT' => $this->data['RPMCH_QTYTOT'],
            'RPMCH_QTYOUT' => $this->data['RPMCH_QTYOUT'],
            'RPMCH_QTYOPN' => $this->data['RPMCH_QTYOPN'],
            'RPMCH_QTYINC' => $this->data['RPMCH_QTYINC'],
            'RPMCH_QTYADJ' => $this->data['RPMCH_QTYADJ'],
            'RPMCH_KET' => $this->data['RPMCH_KET'],
            'RPMCH_ITMCOD' => $this->data['RPMCH_ITMCOD'],
            'RPMCH_DATEIS' => $this->data['RPMCH_DATEIS'],
            'RPMCH_SA' => $this->data['RPMCH_SA'],
            'RPMCH_REF' => $this->data['RPMCH_REF']
        ]);
    }
}
