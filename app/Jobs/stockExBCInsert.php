<?php

namespace App\Jobs;

use App\Model\RPCUST\scrapHistDet;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use LRedis;

use App\Model\RPCUST\scrapHist;

class stockExBCInsert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $doc;
    protected $loc;
    protected $item;
    protected $date;
    protected $lot;
    protected $qty;
    protected $bc;
    protected $isArray;
    protected $isScrap;
    protected $type;
    protected $isReset;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        $doc,
        $loc,
        $item,
        $date,
        $lot,
        $qty,
        $bc,
        $isArray,
        $isScrap,
        $type,
        $isReset
    )
    {
        $this->doc = $doc;
        $this->loc = $loc;
        $this->item = $item;
        $this->date = $date;
        $this->lot = $lot;
        $this->qty = $qty;
        $this->bc = $bc;
        $this->isArray = $isArray;
        $this->isScrap = $isScrap;
        $this->type = empty($type) ? 'exbc_fifo' : $type;
        $this->isReset = $isReset;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->isReset) {
            $this->cancelEXBC($this->doc, $this->loc);
        }
        
        $this->sendingParamBCStock(
            trim($this->item),
            $this->date,
            $this->lot,
            $this->qty,
            $this->doc,
            $this->bc,
            $this->loc,
            $this->isArray
        );
    }

    public function sendingParamBCStock($item_num, $date_out = '', $lot = null, $qty = 0, $doc = null, $bc = null, $loc = '', $isArray = true)
    {
        if ($isArray) {
            $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/getStockBCArray';
            $localendpoint = 'http://localhost/api-report-custom/api/inventory/getStockBCArray';
        } else {
            $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/getStockBC';
            $localendpoint = 'http://localhost/api-report-custom/api/inventory/getStockBC';
        }

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item_num,
            'date_out' => empty($date_out) ? date('Y-m-d') : $date_out,
            // 'lot' => $lot,
            'qty' => $qty,
            'doc' => $doc,
            'bc' => $bc,
            'loc' => $loc
            // 'scrap' => true,
            // 'remark' => $id,
            // 'revise' => $rev
        ];

        $res = $guzz->request('POST', $endpoint, ['form_params' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        // if ($this->isScrap) {
        //     scrapHistDet::where('id', $loc)->update([
        //         'SCR_FIXFLG' => 0
        //     ]);
        // }

        
        $redis = LRedis::connection();
        $redis->publish('message', json_encode([
            'app' => $this->type,
            'result' => $content['CURL'],
            'payload' => [
                'item' => trim($this->item),
                'date' => $this->date,
                'lot' => $this->lot,
                'qty' => $this->qty,
                'doc' => $this->doc,
                'bc' => $this->bc,
                'loc' => $this->loc,
                'isArray' => $this->isArray
            ]
        ]));
    }

    public function cancelEXBC($doc, $loc = '')
    {
        $endpoint = empty($loc) ? 'http://192.168.0.29:8081/api_inventory/api/inventory/cancelDO/' . base64_encode($doc) : 'http://192.168.0.29:8081/api_inventory/api/inventory/cancelDO/' . base64_encode($doc) . '/' . base64_encode($loc);
        $guzz = new \GuzzleHttp\Client();

        $res = $guzz->request('GET', $endpoint);

        $content['CURL'] = json_decode($res->getBody(), true);

        $redis = LRedis::connection();
        $redis->publish('message', json_encode([
            'app' => 'exbc_cancel',
            'result' => $content['CURL'],
            'payload' => [
                'doc' => $this->doc,
                'loc' => $this->loc
            ]
        ]));
    }
}