<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use LRedis;

use App\Jobs\ITInventory\BahanBakuStock;
use App\Jobs\ITInventory\FinishGoodStock;
use App\Jobs\ITInventory\ScrapStock;
use App\Jobs\ITInventory\MesinPeralatanStock;

use App\Model\RPCUST\MutasiRaw;
use App\Model\RPCUST\MutasiFinishGood;
use App\Model\RPCUST\MutasiScrap;
use App\Model\RPCUST\MutasiMesin;
class MutasiInterfaceJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $mutasi, $date_from, $date_to, $is_stored, $period_det;
    public function __construct($mutasi, $date_from, $date_to, $is_stored, $period_det)
    {
        $this->mutasi = $mutasi;
        $this->date_from = $date_from;
        $this->date_to = $date_to;
        $this->is_stored = $is_stored;
        $this->period_det = $period_det;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $ref = 'ITF-'.date('Ymt', strtotime($this->date_to));

        $redis = LRedis::connection();            

        $cek = 0;
        if ($this->mutasi === 'bahan_baku') {
            $cek = MutasiRaw::where('RPRAW_REF', $ref)->count();
        } elseif ($this->mutasi === 'finish_good') {
            $cek = MutasiFinishGood::where('RPGOOD_REF', $ref)->count();
        } elseif ($this->mutasi === 'scrap') {
            $cek = MutasiScrap::where('RPREJECT_REF', $ref)->count();
        } elseif ($this->mutasi === 'mesin') {
            $cek = MutasiMesin::where('RPMCH_REF', $ref)->count();
        }

        if ($cek === 0) {
            $queryString = "SET NOCOUNT ON ;
                EXEC PSI_RPCUST.dbo.sp_mutasi_inv_dev @mutasi = '".$this->mutasi."',
                @date_from = '".$this->date_from."',
                @date_to = '".$this->date_to."'
            ";
    
            $result = array_map(function ($value) {
                return (array)$value;
            }, DB::select($queryString));
    
            $hasilNG = [];
            $hasilOK = [];
            foreach ($result as $key => $value) {
                if ($value['SALDO_SLS'] != 0) {
                    $hasilNG[] = $value;
                }
            }
    
            if (count($hasilNG) > 0) {
                $redis->publish('message', json_encode([
                    'app' => 'interface_mutasi',
                    'status' => false,
                    'mutasi' => $this->mutasi,
                    'period' => $this->period_det,
                    'data' => $hasilNG,
                    'exists' => false
                ]));
            } else {     
                $redis->publish('message', json_encode([
                    'app' => 'interface_mutasi',
                    'status' => true,
                    'mutasi' => $this->mutasi,
                    'period' => $this->period_det,
                    'exists' => false
                ]));
    
                foreach ($result as $key => $value2) {                
                    if ($this->is_stored === true) {
                        if ($this->mutasi === 'bahan_baku') {
                            $cek = MutasiRaw::where('RPRAW_REF', $ref)->where('RPRAW_ITMCOD', $value2['RPRAW_ITMCOD'])->count();
    
                            if ($cek === 0) {
                                BahanBakuStock::dispatch([
                                    'RPRAW_ITMCOD' => $value2['RPRAW_ITMCOD'],
                                    'RPRAW_UNITMS' => $value2['RPRAW_UNITMS'],
                                    'RPRAW_DATEIS' => date('Y-m-t', strtotime($this->date_to)),
                                    'RPRAW_QTYINC' => $value2['SALDO_MASUK'],
                                    'RPRAW_QTYOUT' => $value2['SALDO_KELUAR'],
                                    'RPRAW_QTYADJ' => 0,
                                    'RPRAW_QTYTOT' => $value2['SALDO_MASUK'] + $value2['SALDO_KELUAR'],
                                    'RPRAW_QTYOPN' => $value2['SALDO_OPN'],
                                    'RPRAW_KET' => 'INTERFACE_MUTASI',
                                    'RPRAW_SA' => $value2['SALDO_AWAL'],
                                    'RPRAW_REF' => $ref,
                                ])->onQueue('mutasi_interface');
                            }
                        } elseif ($this->mutasi === 'finish_good') {
                            $cek = MutasiFinishGood::where('RPGOOD_REF', $ref)->where('RPGOOD_ITMCOD', $value2['RPGOOD_ITMCOD'])->count();
    
                            if ($cek === 0) {
                                FinishGoodStock::dispatch([
                                    'RPGOOD_ITMCOD' => $value2['RPGOOD_ITMCOD'],
                                    'RPGOOD_UNITMS' => $value2['RPGOOD_UNITMS'],
                                    'RPGOOD_DATEIS' => $this->date_to,
                                    'RPGOOD_QTYINC' => $value2['SALDO_MASUK'],
                                    'RPGOOD_QTYOUT' => $value2['SALDO_KELUAR'],
                                    'RPGOOD_QTYADJ' => 0,
                                    'RPGOOD_QTYTOT' => $value2['SALDO_MASUK'] + $value2['SALDO_KELUAR'],
                                    'RPGOOD_QTYOPN' => $value2['SALDO_OPN'],
                                    'RPGOOD_KET' => 'INTERFACE_MUTASI',
                                    'RPGOOD_SA' => $value2['SALDO_AWAL'],
                                    'RPGOOD_REF' => 'ITF-'.date('Ymt', strtotime($this->date_to)),
                                ])->onQueue('mutasi_interface');
                            }
                        } elseif ($this->mutasi === 'scrap') {
                            $cek = MutasiScrap::where('RPREJECT_REF', $ref)->where('RPREJECT_ITMCOD', $value2['RPREJECT_ITMCOD'])->count();
    
                            if ($cek === 0) {
                                ScrapStock::dispatch([
                                    'RPREJECT_ITMCOD' => $value2['RPREJECT_ITMCOD'],
                                    'RPREJECT_UNITMS' => $value2['RPREJECT_UNITMS'],
                                    'RPREJECT_DATEIS' => $this->date_to,
                                    'RPREJECT_QTYINC' => $value2['SALDO_MASUK'],
                                    'RPREJECT_QTYOUT' => $value2['SALDO_KELUAR'],
                                    'RPREJECT_QTYADJ' => 0,
                                    'RPREJECT_QTYTOT' => $value2['SALDO_MASUK'] + $value2['SALDO_KELUAR'],
                                    'RPREJECT_QTYOPN' => $value2['SALDO_OPN'],
                                    'RPREJECT_KET' => 'INTERFACE_MUTASI',
                                    'RPREJECT_SA' => $value2['SALDO_AWAL'],
                                    'RPREJECT_REF' => 'ITF-'.date('Ymt', strtotime($this->date_to)),
                                ])->onQueue('mutasi_interface');
                            }
                        } elseif ($this->mutasi === 'mesin') {
                            $cek = MutasiMesin::where('RPMCH_REF', $ref)->where('RPMCH_ITMCOD', $value2['RPMCH_ITMCOD'])->count();
    
                            if ($cek === 0) {
                                MesinPeralatanStock::dispatch([
                                    'RPMCH_ITMCOD' => $value2['RPMCH_ITMCOD'],
                                    'RPMCH_UNITMS' => $value2['RPMCH_UNITMS'],
                                    'RPMCH_DATEIS' => $this->date_to,
                                    'RPMCH_QTYINC' => $value2['SALDO_MASUK'],
                                    'RPMCH_QTYOUT' => $value2['SALDO_KELUAR'],
                                    'RPMCH_QTYADJ' => 0,
                                    'RPMCH_QTYTOT' => $value2['SALDO_MASUK'] + $value2['SALDO_KELUAR'],
                                    'RPMCH_QTYOPN' => $value2['SALDO_OPN'],
                                    'RPMCH_KET' => 'INTERFACE_MUTASI',
                                    'RPMCH_SA' => $value2['SALDO_AWAL'],
                                    'RPMCH_REF' => 'ITF-'.date('Ymt', strtotime($this->date_to)),
                                ])->onQueue('mutasi_interface');
                            }
                        } 
                    } 
                }
            }
        } else {
            $redis->publish('message', json_encode([
                'app' => 'interface_mutasi',
                'status' => true,
                'mutasi' => $this->mutasi,
                'period' => $this->period_det,
                'exists' => true
            ]));
        }
    }
}
