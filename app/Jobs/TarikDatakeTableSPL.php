<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Model\WMS\API\ViewSPL;
use App\Model\WMS\API\SPLMaster;
use App\Model\MASTER\MEGAEMS\ppsn1Table;
use App\Helpers\CustomFunctionHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TarikDatakeTableSPL implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $tries = 3;
    public $timeout = 120;
    protected $date;
    protected $psn;
    protected $cat;
    protected $line;
    protected $feeder;

    public function __construct($date, $psn = null, $cat = null, $line = null, $feeder = null)
    {
        $this->date = $date;
        $this->psn = $psn;
        $this->cat = $cat;
        $this->line = $line;
        $this->feeder = $feeder;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $select = [
            'PPSN2_DOCNO',
            'PPSN1_PSNNO', //SPL_DOC
            'PPSN2_ITMCAT', //SPL_CAT
            'PPSN1_LINENO', //SPL_LINE
            'PPSN1_FR', //SPL_FEDR
            'PPSN1_WONO', //SPL_JOBNO
            'PPSN1_MDLCD', //SPL_FG
            'PIS3_REQQT', //SPL_QTYREQ
            'PIS3_MCZ', //SPL_ORDERNO
            'PPSN1_SIMQT', //SPL_QTYREQ
            'PPSN2_MCZ', //SPL_ORDERNO
            'ITMLOC_LOC', //SPL_RACKNO
            'PPSN2_SUBPN', //SPL_ITMCOD
            'PPSN2_QTPER', //SPL_QTYUSE
            'PPSN2_MSFLG', //SPL_MS
            'PPSN1_SIMQT', //SPL_LOTSZ
            'PDPP_CUSCD', //SPL_CUSTCD
            'PPSN2_PSNQT',
            'PPSN2_REQQT',
            'PPSNA_NREQQT',
            'MSIM_SUBPN'
        ];

        $dataCheck = ppsn1Table::select(array_merge(
            $select,
            [
                DB::raw('(SELECT COUNT(*) 
                    FROM [SRVMEGA].[PSI_MEGAEMS].[dbo].[PIS3_TBL] 
                    WHERE PIS3_DOCNO = PPSN2_DOCNO
                    AND PIS3_MCZ = PPSN2_MCZ
                    AND PIS3_ITMCD = PPSN2_SUBPN
                ) AS NAONSIH')
            ]
        ))
            ->join('SRVMEGA.PSI_MEGAEMS.dbo.PPSN2_TBL', function ($q1) {
                $q1->on('PPSN1_PSNNO', 'PPSN2_PSNNO');
                $q1->on('PPSN1_LINENO', 'PPSN2_LINENO');
                $q1->on('PPSN1_FR', 'PPSN2_FR');
                $q1->on('PPSN1_DOCNO', 'PPSN2_DOCNO');
                $q1->on('PPSN1_BSGRP', 'PPSN2_BSGRP');
            })
            ->join('SRVMEGA.PSI_MEGAEMS.dbo.PDPP_TBL', function ($q3) {
                $q3->on('PDPP_WONO', 'PPSN1_WONO');
            })
            ->leftjoin('SRVMEGA.PSI_MEGAEMS.dbo.PIS3_TBL', function ($q5) {
                $q5->on('PIS3_DOCNO', 'PPSN2_DOCNO');
                $q5->on('PIS3_MCZ', 'PPSN2_MCZ');
                $q5->on('PIS3_PROCD', 'PPSN2_PROCD');
                $q5->on('PIS3_LINENO', 'PPSN1_LINENO');
                $q5->on('PIS3_WONO', 'PPSN1_WONO');
                $q5->on('PIS3_BSGRP', 'PPSN1_BSGRP');
                $q5->on('PIS3_ITMCD', 'PPSN2_SUBPN');
                // $q5->on('PIS3_MC', 'PPSN2_MC');
            })
            ->leftjoin('SRVMEGA.PSI_MEGAEMS.dbo.PIS2_TBL', function ($q5) {
                $q5->on('PIS2_DOCNO', 'PPSN2_DOCNO');
                $q5->on('PIS2_MCZ', 'PPSN2_MCZ');
                $q5->on('PIS2_PROCD', 'PPSN2_PROCD');
                $q5->on('PIS2_LINENO', 'PPSN1_LINENO');
                $q5->on('PIS2_WONO', 'PPSN1_WONO');
                $q5->on('PIS2_BSGRP', 'PPSN1_BSGRP');
                $q5->on('PIS2_ITMCD', 'PPSN2_SUBPN');
                // $q5->on('PIS3_MC', 'PPSN2_MC');
            })
            ->leftjoin('ITMLOC_TBL', 'ITMLOC_ITM', 'PPSN2_SUBPN')
            ->leftjoin('SRVMEGA.PSI_MEGAEMS.dbo.MSIM_TBL', function ($q6) {
                $q6->on('MSIM_MDLCD', 'PPSN1_MDLCD');
                $q6->on('MSIM_SUBPN', 'PPSN2_SUBPN');
            })
            ->leftjoin('SRVMEGA.PSI_MEGAEMS.dbo.PPSNA_TBL', function ($q7) {
                $q7->on('PPSNA_PSNNO', 'PPSN1_PSNNO');
                $q7->on('PPSNA_LINENO', 'PPSN1_LINENO');
                $q7->on('PPSNA_FR', 'PPSN1_FR');
                $q7->on('PPSNA_LINENO', 'PPSN1_LINENO');
                $q7->on('PPSNA_MCZ', 'PPSN2_MCZ');
                $q7->on('PPSNA_SUBPN', 'PPSNA_SUBPN');
            });

        if (
            !empty($this->psn) &&
            !empty($this->cat) &&
            !empty($this->line) &&
            !empty($this->feeder)
        ) {
            $data = $dataCheck
                ->where('PPSN1_PSNNO', $this->psn)
                //->where('PPSN2_ITMCAT', $this->cat)
                //->where('PPSN1_LINENO', $this->line)
                //->where('PPSN1_FR', $this->feeder)
                ->orderBy('PPSN2_MCZ')
                ->groupby($select)
                ->get();
        } else {
            $data = $dataCheck
                ->where('PPSN1_ISUDT', $this->date)
                ->orderBy('PPSN2_MCZ')
                ->groupby($select)
                ->get();
        }
        
        foreach ($data as $key => $value) {
            $totalreqperwo = empty($value['PIS3_REQQT']) ? $value['PPSN2_QTPER'] : $value['PIS3_REQQT'] / $value['PPSN1_SIMQT'];

            $arraynya = [
                'SPL_DOC' => $value['PPSN1_PSNNO'],
                'SPL_CAT' => $value['PPSN2_ITMCAT'],
                'SPL_LINE' => $value['PPSN1_LINENO'],
                'SPL_FEDR' => $value['PPSN1_FR'],
                // 'SPL_JOBNO' => $value['PPSN1_WONO'],
                'SPL_JOBNO' => $value['PPSN1_WONO'],
                'SPL_FG' => $value['PPSN1_MDLCD'],
                // 'SPL_ORDERNO' => $value['PPSN2_MCZ'],
                'SPL_ORDERNO' => $value['PPSN2_MCZ'],
                'SPL_RACKNO' => $value['ITMLOC_LOC'],
                // 'SPL_ITMCD' => $value['PPSN2_SUBPN'],
                'SPL_ITMCD' => $value['PPSN2_SUBPN'],
                // 'SPL_QTYUSE' => $value['PPSN2_QTPER'],
                'SPL_QTYUSE' => $totalreqperwo,
                'SPL_QTYREQ' => (empty($value['PPSNA_NREQQT']))
                ? ((empty($value['PIS3_REQQT']))
                  ? (($value['NAONSIH'] > 0) 
                    ? 0
                    : $value['PPSN1_SIMQT'] * $value['PPSN2_QTPER']
                    )
                  : $value['PIS3_REQQT']
                  )
                : ((empty($value['PIS3_REQQT'])) 
                  ? (($value['NAONSIH'] > 0) 
                    ? 0
                    : $value['PPSN1_SIMQT'] * $value['PPSN2_QTPER']
                    )
                  : (is_float($value['PIS3_REQQT'] / $value['PPSN1_SIMQT'])
                    ? $value['PPSN1_SIMQT'] * $value['PPSN2_QTPER']
                    : $value['PIS3_REQQT']
                    )
                  ),
                'SPL_MS' => $value['PPSN2_MSFLG'],
                'SPL_LOTSZ' => $value['PPSN1_SIMQT'],
                'SPL_CUSCD' => $value['PDPP_CUSCD'],
                'SPL_LUPDT' => date('Y-m-d h:i:s'),
                'SPL_USRID' => 'SCD',
            ];

            $cekrecord = SPLMaster::where('SPL_DOC', $value['PPSN1_PSNNO'])
                ->where('SPL_CAT', $value['PPSN2_ITMCAT'])
                ->where('SPL_LINE', $value['PPSN1_LINENO'])
                ->where('SPL_FEDR', $value['PPSN1_FR'])
                ->where('SPL_JOBNO', $value['PPSN1_WONO'])
                ->where('SPL_ORDERNO', $value['PPSN2_MCZ'])
                ->where('SPL_ITMCD', $value['PPSN2_SUBPN'])
                // ->where('SPL_QTYUSE', $value['PPSN2_QTPER'])
                ->first();

            if (!empty($cekrecord)) {
                $cekrecord->delete();
            }

            SPLMaster::updateOrCreate([
                'SPL_DOC' => $value['PPSN1_PSNNO'],
                'SPL_CAT' => $value['PPSN2_ITMCAT'],
                'SPL_LINE' => $value['PPSN1_LINENO'],
                'SPL_FEDR' => $value['PPSN1_FR'],
                // 'SPL_JOBNO' => $value['PPSN1_WONO'],
                'SPL_JOBNO' => $value['PPSN1_WONO'],
                // 'SPL_ORDERNO' => $value['PPSN2_MCZ'],
                'SPL_ORDERNO' => $value['PPSN2_MCZ'],
                // 'SPL_ITMCD' => $value['PPSN2_SUBPN'],
                'SPL_ITMCD' => $value['PPSN2_SUBPN'],
                // 'SPL_QTYUSE' => $totalreqperwo,
            ], $arraynya);
        }
    }
}
