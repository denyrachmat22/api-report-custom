<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use LRedis;
use Illuminate\Support\Facades\DB;

use App\Model\RPCUST\scrapHist;
use App\Model\RPCUST\scrapHistDet;
use App\Model\WMS\API\SPLSCN;

use App\Model\MASTER\ITHMaster;
use App\Model\MASTER\ItemMaster;
use App\Model\RPCUST\DetailStock;

use App\Jobs\ITInventory\ScrapStock;
use App\Model\WMS\API\SERDTBL;

class scrapDetailInserts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $process, $qty, $id, $item, $idTrans, $job, $scrap_instruction, $date_out, $insertStock, $whSource;
    public function __construct($process, $qty, $id, $item, $idTrans, $job, $scrap_instruction, $date_out = '', $insertStock = false, $whSource = [])
    {
        $this->process = $process;
        $this->qty = $qty;
        $this->id = $id;
        $this->item = $item;
        $this->idTrans = $idTrans;
        $this->job = $job;
        $this->scrap_instruction = $scrap_instruction;
        $this->date_out = empty($date_out) || $date_out === '1970-01-01' ? date('Y-m-d') : $date_out;
        $this->insertStock = $insertStock;
        $this->whSource = $whSource;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        set_time_limit(3600);
        // Fetch All PSN by process and job number

        // logger(json_encode($this->process));

        $getAngkaKoma = array_filter($this->process, function ($f) {
            return $this->is_decimal(round($f['PPSN2_QTPER'], 2));
        });

        // logger(json_encode($getAngkaKoma));

        $cekKoma = $this->cekAngkaKoma($getAngkaKoma, $this->qty);

        // logger($cekKoma);
        if ($cekKoma['status'] === true) {
            $listStoredItem = [];
            scrapHistDet::where('SCR_HIST_ID', $this->id)->delete();
            foreach ($this->process as $key => $value) {

                logger(json_encode([
                    'qty' => $this->qty,
                    'qtper' => $value['PPSN2_QTPER'],
                    'data' => $value,
                ]));

                $totalQty = $this->qty * $value['TOTAL_PER'];

                $insertDet = scrapHistDet::insertGetId([
                    'SCR_HIST_ID' => $this->id,
                    'SCR_PSN' => $value['PPSN1_PSNNO'],
                    'SCR_CAT' => $value['PPSN2_ITMCAT'],
                    'SCR_LINE' => $value['PPSN1_LINENO'],
                    'SCR_FR' => '',
                    'SCR_MC' => $value['SERD_MC'],
                    'SCR_MCZ' => $value['SERD_MCZ'],
                    'SCR_ITMCD' => trim($value['PPSN2_SUBPN']),
                    // 'SCR_QTPER' => $value['PPSN2_QTPER'],
                    // 'SCR_QTPER' => isset($value['TOTAL_PER']) && (round($value['TOTAL_PER'], 2) !== round($value['PPSN2_QTPER'], 2) || round($valueFifoLot['SERD_QTY'], 2) !== round($value['SERD_QTY'], 2))
                    //     ? ($valueFifoLot['QTY_USED'] / $value['SERD_QTY']) * $value['PPSN2_QTPER']
                    //     : $value['PPSN2_QTPER'],
                    'SCR_QTPER' => $this->is_decimal($value['SERD_QTPER']) ? round($value['PPSN2_QTPER'], 2) : round($value['PPSN2_QTPER']),
                    // 'SCR_QTY' => fmod($valueFifoLot['QTY_USED'], 1) !== 0.00 ? round($valueFifoLot['QTY_USED']) : round($valueFifoLot['QTY_USED']),
                    'SCR_QTY' => $this->is_decimal($value['SERD_QTPER']) ? round($value['SERD_QTY'], 2) : round($value['SERD_QTY']),
                    'SCR_LOTNO' => $value['SERD_LOTNO'],
                    'SCR_ITMPRC' => 0,
                    'SCR_PROCD' => $value['PPSN1_PROCD'],
                    'SCR_PROID' => $value['RPDSGN_CODE']
                ]);

                // logger('check inserStock nih');
                // logger($this->insertStock);
                if ($insertDet) {
                    // logger($insertDet);
                    $listStoredItem[trim($value['PPSN2_SUBPN'])][$key] = array_merge($value, [
                        'QTY' => $this->qty,
                        'PPSN2_QTPER' => $value['PPSN2_QTPER'],
                        'ID_DET' => $insertDet,
                        'ID_MSTR' => $this->id
                    ]);

                    // Insert scrap to stock
                    $hasilBCStock = $this->sendingParamBCStock(
                        trim($value['PPSN2_SUBPN']),
                        $this->date_out,
                        $value['SERD_LOTNO'],
                        round($totalQty),
                        $this->idTrans,
                        null,
                        $this->id
                    );

                    $redis = LRedis::connection();

                    if (count($hasilBCStock['data']) > 1) {
                        $redis->publish('message', json_encode([
                            'app' => 'scrap_report_checker',
                            'hasil_exbc' => $hasilBCStock,
                            'SCR_ITMPRC' => isset($hasilBCStock['data'][count($hasilBCStock['data']) - 1])
                                ? $hasilBCStock['data'][count($hasilBCStock['data']) - 1]['RCV_PRPRC']
                                : 0,
                            'SCR_ITMPRC_STAT' => count($hasilBCStock['data']) > 1,
                            'TEST' => $hasilBCStock['data']
                        ]));
                    }

                    if ($hasilBCStock['status']) {
                        scrapHistDet::where('id', $insertDet)->update([
                            'SCR_ITMPRC' => count($hasilBCStock['data']) > 0
                                ? $hasilBCStock['data'][count($hasilBCStock['data']) - 1]['RCV_PRPRC']
                                : 0
                        ]);
                    }
                }
                // If Exists delete first
                // foreach ($getFIFOLotNo as $keyFifoLot => $valueFifoLot) {
                //     // if (isset($value['TOTAL_PER'])) {
                //     //     if ($value['TOTAL_PER'] / $valueFifoLot['QTY_USED'] == 1) {
                //     //         $qtPer = $value['TOTAL_PER'];
                //     //     } else {
                //     //         $qtPer = round($valueFifoLot['QTY_USED'] / $value['TOTAL_PER'], 2);
                //     //     }
                //     // } else {
                //     //     if ($value['PPSN2_QTPER'] / $valueFifoLot['QTY_USED'] == 1) {
                //     //         $qtPer = $value['PPSN2_QTPER'];
                //     //     } else {
                //     //         $qtPer = round($valueFifoLot['QTY_USED'] / $value['PPSN2_QTPER'], 2);
                //     //     }
                //     // }

                //     if (round($valueFifoLot['SERD_QTY'], 2) !== round($value['SERD_QTY'], 2)) {
                //         // $qtPer = ($valueFifoLot['QTY_USED'] / $value['SERD_QTY']) * $value['PPSN2_QTPER'];
                //         $qtPer = ($valueFifoLot['QTY_USED'] / $this->qty);
                //     } else {
                //         $qtPer = $value['PPSN2_QTPER'];
                //     }

                //     $insertDet = scrapHistDet::insertGetId([
                //         'SCR_HIST_ID' => $this->id,
                //         'SCR_PSN' => $value['PPSN1_PSNNO'],
                //         'SCR_CAT' => $value['SERD_CAT'],
                //         'SCR_LINE' => $value['PPSN1_LINENO'],
                //         'SCR_FR' => $value['SERD_FR'],
                //         'SCR_MC' => $value['SERD_MC'],
                //         'SCR_MCZ' => $value['SERD_MCZ'],
                //         'SCR_ITMCD' => trim($value['PPSN2_SUBPN']),
                //         // 'SCR_QTPER' => $value['PPSN2_QTPER'],
                //         // 'SCR_QTPER' => isset($value['TOTAL_PER']) && (round($value['TOTAL_PER'], 2) !== round($value['PPSN2_QTPER'], 2) || round($valueFifoLot['SERD_QTY'], 2) !== round($value['SERD_QTY'], 2))
                //         //     ? ($valueFifoLot['QTY_USED'] / $value['SERD_QTY']) * $value['PPSN2_QTPER']
                //         //     : $value['PPSN2_QTPER'],
                //         'SCR_QTPER' => round($value['PPSN2_QTPER'], 4),
                //         // 'SCR_QTY' => fmod($valueFifoLot['QTY_USED'], 1) !== 0.00 ? round($valueFifoLot['QTY_USED']) : round($valueFifoLot['QTY_USED']),
                //         'SCR_QTY' => round($value['SERD_QTY']),
                //         'SCR_LOTNO' => $value['SERD_LOTNO'],
                //         'SCR_ITMPRC' => empty($valueFifoLot['PRICE']) ? 0 : $valueFifoLot['PRICE'],
                //         'SCR_PROCD' => $value['PPSN1_PROCD'],
                //         'SCR_PROID' => $value['RPDSGN_CODE']
                //     ]);

                //     // logger('check inserStock nih');
                //     // logger($this->insertStock);
                //     if ($insertDet) {
                //         // logger($insertDet);
                //         $listStoredItem[trim($value['PPSN2_SUBPN'])][$key] = array_merge($valueFifoLot, [
                //             'QTY' => $this->qty,
                //             'PPSN2_QTPER' => $value['PPSN2_QTPER'],
                //             'ID_DET' => $insertDet,
                //             'ID_MSTR' => $this->id
                //         ]);

                //         // Insert scrap to stock
                //         $hasilBCStock = $this->sendingParamBCStock(
                //             trim($value['PPSN2_SUBPN']),
                //             $this->date_out,
                //             $valueFifoLot['SERD_LOTNO'],
                //             round($totalQty),
                //             $this->idTrans,
                //             null,
                //             $this->id
                //         );

                //         // logger('checl data bc');
                //         // logger($hasilBCStock);
                //         // logger($hasilBCStock['data']);

                //         // $cek = scrapFixForDisposeQueue::dispatch(
                //         //     'scrapPrice',
                //         //     trim($value['PPSN2_SUBPN']),
                //         //     $value['SERD_LOTNO'],
                //         //     '',
                //         //     $insertDet
                //         // )->onQueue('fix_scrap_price');

                //         $redis = LRedis::connection();
                //         $redis->publish('message', json_encode([
                //             'app' => 'scrap_report_checker',
                //             'hasil_exbc' => $hasilBCStock
                //         ]));

                //         if ($hasilBCStock['status']) {
                //             scrapHistDet::where('id', $insertDet)->update([
                //                 'SCR_ITMPRC' => count($hasilBCStock['data']) > 1
                //                 ? $hasilBCStock['data'][count($hasilBCStock['data']) - 1]['RCV_PRPRC']
                //                 : 0
                //             ]);
                //         }
                //     }
                // }
            }

            // logger(json_encode($listStoredItem));

            if (count($listStoredItem) > 0) {
                $sumItem = [];
                foreach ($listStoredItem as $keyStock => $valueStock) {

                    $tot_qty = 0;
                    foreach ($valueStock as $keyTot => $valueTot) {
                        $tot_qty += $valueTot['QTY'] * $valueTot['PPSN2_QTPER'];
                    }

                    $sumItem[] = array_merge(array_values($valueStock)[0], ['QTY' => round($tot_qty)]);
                }
            }

            if ($this->insertStock) {
                $cekJob = scrapHist::where('id', $this->id)->first();

                $cekStockITH = ITHMaster::select(
                    'ITH_DOC',
                    'ITH_SER',
                    'ITH_DATE',
                    'ITH_WH'
                );
                if (isset($this->whSource)) {
                    $cekStockITH->whereIn('ITH_WH', $this->whSource);
                }

                if (!empty($cekJob->REF_NO)) {
                    $cekStockITH->where('ITH_SER', $cekJob->REF_NO);
                }

                $hasilITH = $cekStockITH->groupBy(
                    'ITH_DOC',
                    'ITH_SER',
                    'ITH_DATE',
                    'ITH_WH'
                )
                    ->orderBy('ITH_DATE', 'DESC')
                    ->havingRaw('SUM(ITH_QTY) > 0')
                    ->first();

                if (!empty($hasilITH)) {
                    // Issue
                    // logger(json_encode($hasilITH));
                    $this->insertToITH(
                        trim($cekJob->ITEMNUM),
                        $this->date_out,
                        in_array('ARQA1', $this->whSource)
                        ? 'OUT-QA-SCR-FG'
                        : (
                            in_array('ARPRD1', $this->whSource)
                            ? 'OUT-PRD-SCR-FG'
                            : (
                                in_array('QA', $this->whSource) || in_array('QAFG', $this->whSource)
                                ? 'OUT-PEN-SCR-FG'
                                : 'OUT-WH-SCR-FG'
                            )
                        ),
                        $hasilITH->ITH_DOC,
                        round($cekJob->QTY) * -1,
                        $hasilITH->ITH_WH,
                        $hasilITH->ITH_SER,
                        $this->id,
                        'SCR_RPT'
                    );

                    // Receive
                    $this->insertToITH(
                        trim($cekJob->ITEMNUM),
                        $this->date_out,
                        'INC-SCR-FG',
                        $this->idTrans,
                        round($cekJob->QTY),
                        in_array('ARQA1', $this->whSource) || in_array('ARPRD1', $this->whSource) ? 'AFWH9SC-' : 'AFWH9SC',
                        $hasilITH->ITH_SER,
                        $this->id,
                        'SCR_RPT'
                    );
                }
            }

            try {
                $cekJob = scrapHist::where('id', $this->id)->first();
                $redis = LRedis::connection();

                $redis->publish('message', json_encode([
                    'app' => 'scrap_report',
                    'id' => $this->idTrans,
                    'id_transaction' => $this->id,
                    'item' => $this->item['MITM_ITMCD'],
                    'item_desc' => $this->item['MITM_ITMD1'],
                    'doc' => $cekJob->DOC_NO,
                    'status' => 'positive',
                    'message' => 'This ' . $cekJob->DOC_NO . ' detail is successfully inserted',
                    'user' => $cekJob->USERNAME,
                    'stat_print' => false
                ]));

                scrapHist::where('id', $this->id)->update(['IS_DONE' => 1]);

                $cekOk = scrapHist::where('ID_TRANS', $this->idTrans)->get()->toArray();

                $cekData = array_values(array_filter($cekOk, function ($f) {
                    return $f['IS_DONE'] == 1;
                }));

                // $cekData = $cekOk;

                if (count($cekData) === scrapHist::where('ID_TRANS', $this->idTrans)->count()) {
                    $redis->publish('message', json_encode([
                        'app' => 'scrap_report',
                        'id' => $this->idTrans,
                        'id_transaction' => $this->id,
                        'item' => $this->item['MITM_ITMCD'],
                        'item_desc' => $this->item['MITM_ITMD1'],
                        'doc' => $cekJob->DOC_NO,
                        'status' => 'positive',
                        'message' => 'This ' . $this->idTrans . ' already finished, you can print now.',
                        'user' => $cekJob->USERNAME,
                        'stat_print' => true
                    ]));
                } else {
                    logger('gak masuk');
                }
            } catch (\Predis\Connection\ConnectionException $e) {
                return response('error connection redis');
            }
        } else {
            $cekJob = scrapHist::where('id', $this->id)->first();
            $redis = LRedis::connection();
            $redis->publish('message', json_encode([
                'app' => 'scrap_report',
                'id' => $this->idTrans,
                'id_transaction' => $this->id,
                'item' => $this->item['MITM_ITMCD'],
                'item_desc' => $this->item['MITM_ITMD1'],
                'doc' => $cekJob->DOC_NO,
                'status' => 'negative',
                'message' => 'Your input qty will be decimal please put right amount of scrap !!',
                'user' => $cekJob->USERNAME,
                'stat_print' => false,
                'hasil_dec' => $cekKoma['data']
            ]));
            scrapHist::where('id', $this->id)->update(['failed_reason' => 'Your input qty will be decimal please put right amount of scrap !!']);
            ITHMaster::where('ITH_SER', (string) $this->id)->where('ITH_USRID', 'SCR_RPT')->delete();

            scrapHist::where('id', $this->id)->delete();
            scrapHistDet::where('SCR_HIST_ID', $this->id)->delete();

            DetailStock::where('RPSTOCK_REMARK', $this->idTrans)->where('RPSTOCK_LOC', $this->id)->delete();
        }
    }

    public function failed(\Throwable $exception)
    {
        try {
            $cekJob = scrapHist::where('id', $this->id)->first();
            $redis = LRedis::connection();

            $redis->publish('message', json_encode([
                'app' => 'scrap_report',
                'id' => $this->idTrans,
                'id_transaction' => $this->id,
                'item' => $this->item['MITM_ITMCD'],
                'item_desc' => $this->item['MITM_ITMD1'],
                'doc' => $cekJob->DOC_NO,
                'status' => 'negative',
                'message' => 'There is an error on server, please contact administrator !',
                'user' => $cekJob->USERNAME,
                'stat_print' => false
            ]));

            scrapHist::where('id', $this->id)->update(['failed_reason' => $exception]);
            ITHMaster::where('ITH_SER', (string) $this->id)->where('ITH_USRID', 'SCR_RPT')->delete();

            scrapHist::where('id', $this->id)->delete();
            scrapHistDet::where('SCR_HIST_ID', $this->id)->delete();

            DetailStock::where('RPSTOCK_REMARK', $this->idTrans)->where('RPSTOCK_LOC', $this->id)->delete();
        } catch (\Predis\Connection\ConnectionException $e) {
            return response('error connection redis');
        }
    }

    // ETC Function
    public function convertifABProcess($data)
    {
        $cekAB = array_filter($data, function ($f) {
            return $f['PPSN1_PROCD'] === 'SMT-AB';
        });

        if (count($cekAB) > 0) {
            $hasil = [];
            foreach ($cekAB as $key => $value) {
                if ($value['PPSN2_ITMCAT'] !== 'PCB') {
                    $hasil = array_merge($value, ['PPSN2_QTPER' => $value['PPSN2_QTPER'] * 0.5]);
                }
            }
        }
    }

    public function cekAngkaKoma($data, $qty)
    {
        $hasil = [];
        foreach ($data as $key => $value) {
            $hasil[trim($value['PPSN2_SUBPN'])][$key] = [
                'qtper' => $value['PPSN2_QTPER'],
                'qty' => $qty,
                'jumlah' => fmod($qty * $value['PPSN2_QTPER'], 1) !== 0.00 ? $qty * $value['REAL_PER'] : $qty * $value['PPSN2_QTPER'],
                'process' => $value['PPSN1_PROCD'],
                'real_per' => $value['REAL_PER'],
                'serd_per' => $value['SERD_QTPER']
            ];
        }

        // logger(json_encode($hasil));

        $cekData = [];
        foreach ($hasil as $keyJum => $valueJum) {
            $sumnya1 = 0;
            foreach (array_values($valueJum) as $keyJum2 => $valueJum2) {
                $sumnya1 += $valueJum2['jumlah'];
            }
            $cekData[] = [
                'ITEM' => $keyJum,
                'TOTAL' => round($sumnya1, 2)
            ];
        }

        $cek = array_filter(array_values($hasil), function ($f) {
            $sumnya = 0;
            foreach (array_values($f) as $key => $value) {
                $sumnya += $value['qty'] * $value['qtper'];
            }

            $sumnya2 = 0;
            foreach (array_values($f) as $key => $value) {
                $sumnya2 += $value['qty'] * $value['real_per'];
            }

            $sumnya3 = 0;
            foreach (array_values($f) as $key => $value) {
                $sumnya3 += $value['qty'] * $value['serd_per'];
            }

            return $this->is_decimal(round($sumnya3, 2)) && $this->is_decimal(round($sumnya, 2)) && $this->is_decimal(round($sumnya2, 2));
        });

        $proc = [];
        foreach ($cek as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $proc[] = $value2['process'];
            }
        }

        if (count($cek) > 0) {
            return [
                'status' => false,
                'process' => implode(",", $proc),
                'data' => $cekData
            ];
        } else {
            return [
                'status' => true,
                'process' => implode(",", $proc),
                'data' => $cekData
            ];
        }
    }

    public function is_decimal($val)
    {
        return is_numeric($val) && floor($val) != $val;
    }

    public function getDataSPLSCN($psn, $line, $fr, $item)
    {
        $data = SPLSCN::select([
            'SPLSCN_DOC',
            'SPLSCN_CAT',
            'SPLSCN_LINE',
            'SPLSCN_FEDR',
            'SPLSCN_ORDERNO',
            'SPLSCN_ITMCD',
            'SPLSCN_LOTNO',
            'SPLSCN_LUPDT',
            DB::raw('(SUM(SPLSCN_QTY) - COALESCE(SUM(RETSCN_QTYAFT), 0)) - COALESCE(SUM(SCR_QTY), 0) AS SPLSCN_QTY'),
            DB::raw('SUM(RETSCN_QTYAFT) AS RETURN_QTY'),
            DB::raw('COALESCE(SUM(SCR_QTY), 0) AS SCRAP_QTY'),
            DB::raw('SUM(SPLSCN_QTY) AS REAL_SPLSCN_QTY'),
            // DB::raw('(SELECT TOP 1
            //     RCV_PRPRC
            //     FROM RCVSCN_TBL
            //     INNER JOIN RCV_TBL ON RCVSCN_DONO = RCV_DONO
            //         AND RCVSCN_ITMCD = RCV_ITMCD
            //     WHERE RCVSCN_ITMCD = SPLSCN_ITMCD
            //     AND RCVSCN_LOTNO = SPLSCN_LOTNO
            //     ORDER BY RCV_RPDATE DESC) as PRICE'),
            DB::raw('0 as PRICE'),
            // DB::raw('(SELECT TOP 1
            //     RCV_BCTYPE
            //     FROM RCVSCN_TBL
            //     INNER JOIN RCV_TBL ON RCVSCN_DONO = RCV_DONO
            //         AND RCVSCN_ITMCD = RCV_ITMCD
            //     WHERE RCVSCN_ITMCD = SPLSCN_ITMCD
            //     AND RCVSCN_LOTNO = SPLSCN_LOTNO
            //     ORDER BY RCV_RPDATE DESC) as RCV_BCTYPE')
        ])
            ->leftjoin('RETSCN_TBL', function ($f) {
                $f->on('RETSCN_ITMCD', '=', 'SPLSCN_ITMCD');
                $f->on('RETSCN_SPLDOC', '=', 'SPLSCN_DOC');
                $f->on('RETSCN_CAT', '=', 'SPLSCN_CAT');
                $f->on('RETSCN_LINE', '=', 'SPLSCN_LINE');
                $f->on('RETSCN_FEDR', '=', 'SPLSCN_FEDR');
                $f->on('RETSCN_ORDERNO', '=', 'SPLSCN_ORDERNO');
                $f->on('RETSCN_LOT', '=', 'SPLSCN_LOTNO');
            })
            ->leftjoin('PSI_RPCUST.dbo.RPSCRAP_HIST_DET', function ($f2) {
                $f2->on('SCR_PSN', '=', 'SPLSCN_DOC');
                $f2->on('SCR_CAT', '=', 'SPLSCN_CAT');
                $f2->on('SCR_LINE', '=', 'SPLSCN_LINE');
                $f2->on('SCR_FR', '=', 'SPLSCN_FEDR');
                $f2->on('SCR_MCZ', '=', 'SPLSCN_ORDERNO');
                $f2->on('SCR_ITMCD', '=', 'SPLSCN_ITMCD');
            })
            ->where('SPLSCN_DOC', $psn)
            // ->where('SPLSCN_CAT', $cat)
            ->where('SPLSCN_LINE', $line)
            ->where('SPLSCN_FEDR', $fr)
            // ->where('SPLSCN_ORDERNO', $orderNo)
            ->where('SPLSCN_ITMCD', $item)
            ->groupBy(
                [
                    'SPLSCN_DOC',
                    'SPLSCN_CAT',
                    'SPLSCN_LINE',
                    'SPLSCN_FEDR',
                    'SPLSCN_ORDERNO',
                    'SPLSCN_ITMCD',
                    'SPLSCN_LOTNO',
                    'SPLSCN_LUPDT'
                ]
            )
            ->orderBy('SPLSCN_LOTNO', 'DESC')
            ->get()
            ->toArray();

        return $data;
    }

    public function getListLotFromSERD($psn, $line, $item, $job)
    {
        $getData = SERDTBL::select(
            '*',
            // DB::raw(
            //     '(SELECT TOP 1
            //         RCV_PRPRC
            //         FROM PSI_WMS.dbo.RCVSCN_TBL
            //         INNER JOIN PSI_WMS.dbo.RCV_TBL ON RCVSCN_DONO = RCV_DONO
            //             AND RCVSCN_ITMCD = RCV_ITMCD
            //         WHERE RCVSCN_ITMCD = SERD_ITMCD
            //         AND RCVSCN_LOTNO = SERD_LOTNO
            //         ORDER BY RCV_RPDATE DESC) as PRICE'
            // ),
            DB::raw(
                '0 as PRICE'
            ),
        )
            ->where('SERD_PSNNO', $psn)
            ->where('SERD_LINENO', $line)
            ->where('SERD_ITMCD', $item)
            ->where('SERD_JOB', $job)
            ->get()
            ->toArray();

        return $getData;
    }

    public function getFIFOLOT($process, $qty, $hasil = [])
    {
        $cekProcessNow = current($process);

        if (!empty($cekProcessNow)) {
            $total = (int) $qty - (int) $cekProcessNow['SPLSCN_QTY'];
            if ($total > 0) {
                $hasil = array_merge($hasil, [array_merge($cekProcessNow, ['QTY_USED' => (int) $cekProcessNow['SPLSCN_QTY']])]);
                next($process);
                return $this->getFIFOLOT($process, $total, $hasil);
            } else {
                $hasil = array_merge($hasil, [array_merge($cekProcessNow, ['QTY_USED' => $qty])]);
                return $hasil;
            }
        } else {
            return $hasil;
        }
    }

    public function getFIFOLOTSERD($process, $qty, $hasil = [])
    {
        $cekProcessNow = current($process);

        if (!empty($cekProcessNow)) {
            $totalInserted = 0;
            // logger(json_encode($hasil));
            foreach ($hasil as $key => $value) {
                if (
                    $value['SERD_ITMCD'] == $cekProcessNow['SERD_ITMCD']
                    && $value['SERD_PROCD'] == $cekProcessNow['SERD_PROCD']
                    && $value['SERD_LOTNO'] == $cekProcessNow['SERD_LOTNO']
                ) {
                    $totalInserted += $value['QTY_USED'];
                }
            }

            $total = ((int) $qty - $totalInserted) - (int) $cekProcessNow['SERD_QTY'];
            if (((int) $qty - $totalInserted) > 0 && $total > 0) {
                $hasil = array_merge($hasil, [array_merge($cekProcessNow, ['QTY_USED' => (int) $cekProcessNow['SERD_QTY']])]);
                next($process);

                return $this->getFIFOLOTSERD($process, $total, $hasil);
            } else {
                $hasil = array_merge($hasil, [array_merge($cekProcessNow, ['QTY_USED' => $qty])]);

                return $hasil;
            }
        } else {
            return $hasil;
        }
    }

    public function sendingParamBCStock($item_num, $date_out = '', $lot = null, $qty = 0, $doc = null, $bc = null, $loc = '')
    {
        $endpoint = 'http://192.168.0.29:8081/api_inventory/api/inventory/getStockBC';

        $content = [];
        $guzz = new \GuzzleHttp\Client();
        $param = [
            'item_num' => $item_num,
            'date_out' => empty($date_out) ? date('Y-m-d') : $date_out,
            'lot' => $lot,
            'qty' => $qty,
            'doc' => $doc,
            'bc' => $bc,
            'loc' => $loc
            // 'scrap' => true,
            // 'remark' => $id,
            // 'revise' => $rev
        ];

        $res = $guzz->request('POST', $endpoint, ['query' => $param]);

        $content['PARAM'] = $param;
        $content['CURL'] = json_decode($res->getBody(), true);

        return $content['CURL'];
    }

    public function sendMigrationsSP($item, $qty, $lot, $id)
    {
        $result = DB::select(
            "SET NOCOUNT ON; EXEC PSI_RPCUST.dbo.sp_calculation_exbc
                @item_num= '" . $item . "',
                @doc_out= '" . $id . "',
                @lot = '" . $lot . "',
                @date_out= '" . date('Y-m-d') . "',
                @qty= " . (int) $qty . ",
                @is_saved= 1"
        );

        return $result;
    }

    public function sendMigrationsSPQuery($item, $qty, $lot, $id)
    {
        $result = "SET NOCOUNT ON; EXEC PSI_RPCUST.dbo.sp_calculation_exbc
                @item_num= '" . $item . "',
                @doc_out= '" . $id . "',
                @lot = '" . $lot . "',
                @date_out= '" . date('Y-m-d') . "',
                @qty= " . (int) $qty . ",
                @is_saved= 1";

        return $result;
    }

    public function insertToITH($item, $date, $form, $doc, $qty, $wh, $ser, $remark, $user)
    {
        ITHMaster::insert([
            'ITH_ITMCD' => $item,
            'ITH_DATE' => $date,
            'ITH_FORM' => $form,
            'ITH_DOC' => $doc,
            'ITH_QTY' => $qty,
            'ITH_WH' => $wh,
            'ITH_LOC' => '',
            'ITH_SER' => $ser,
            'ITH_REMARK' => $remark,
            'ITH_LINE' => NULL,
            'ITH_LUPDT' => date('Y-m-d H:i:s'),
            'ITH_USRID' => $user,
        ]);
    }
}
