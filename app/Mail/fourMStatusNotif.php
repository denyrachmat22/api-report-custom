<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class fourMStatusNotif extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $data, $file)
    {
        $this->user = $user;
        $this->data = $data;
        $this->file = $file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('EMAIL.fourMbody', ['user' => $this->user, 'data' => $this->data])
            ->subject('PSI Notification - BOM MEGA vs CIMS')
            ->attach(storage_path('app/public/'.$this->file));
    }
}
