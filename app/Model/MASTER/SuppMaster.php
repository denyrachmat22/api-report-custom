<?php

namespace App\Model\MASTER;

use Illuminate\Database\Eloquent\Model;

class SuppMaster extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'MSUP_TBL';
}
