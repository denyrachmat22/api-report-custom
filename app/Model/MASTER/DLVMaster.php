<?php

namespace App\Model\MASTER;

use Illuminate\Database\Eloquent\Model;

class DLVMaster extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'DLV_TBL';

    public function serial()
    {
        return $this->hasOne('App\Model\MASTER\SerialMaster','SER_ID','DLV_SER');
    }
}
