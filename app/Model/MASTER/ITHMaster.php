<?php

namespace App\Model\MASTER;

use Illuminate\Database\Eloquent\Model;

class ITHMaster extends Model
{
    protected $primaryKey = 'ITH_DOC';
    protected $keyType = 'string';

    protected $connection = 'sqlsrv2';
    protected $table = 'ITH_TBL';
    const UPDATED_AT = 'ITH_LUPDT';
    protected $fillable = [
        'ITH_ITMCD',
        'ITH_DATE',
        'ITH_FORM',
        'ITH_DOC',
        'ITH_QTY',
        'ITH_WH',
        'ITH_LOC',
        'ITH_SER',
        'ITH_REMARK',
        'ITH_LINE',
        'ITH_LUPDT',
        'ITH_USRID',
    ];

    public function spl()
    {
        return $this->hasMany('App\Model\MASTER\SPLMaster','SPL_JOBNO','ITH_DOC');
    }

    public function dlv()
    {
        return $this->hasMany('App\Model\MASTER\DLVMaster','DLV_ID','ITH_DOC');
    }

    public function ret()
    {
        return $this->hasMany('App\Model\MASTER\ITHMaster','ITH_DOC','ITH_DOC')->where('ITH_REMARK', '<>', '');
    }

    public function retdet()
    {
        return $this->hasMany('App\Model\WMS\API\RETSCN','RETSCN_ID','ITH_REMARK');
    }

    public function modelprice()
    {
        return $this->hasOne('App\Model\MASTER\PriceMaster','MSLSPRICE_ITMCD','ITH_ITMCD');
    }

    public function serDet()
    {
        return $this->hasMany('App\Model\MASTER\SERD2TBL','SERD2_SER','ITH_SER');
    }

    public function WIPDone()
    {
        return $this->hasOne('App\Model\RPCUST\WIPDone','line','ITH_LINE');
    }
}
