<?php

namespace App\Model\MASTER;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ItemMaster extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'MITM_TBL';

    public function ItemLoc()
    {
        return $this->hasOne('App\Model\WMS\API\ITMLOCTblo','ITMLOC_ITM','MITM_ITMCD');
    }

    public function ith()
    {
        return $this->belongsto('App\Model\MASTER\ITHMaster','MITM_ITMCD','ITH_ITMCD');
    }
}
