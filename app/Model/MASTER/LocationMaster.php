<?php

namespace App\Model\MASTER;

use Illuminate\Database\Eloquent\Model;

class LocationMaster extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'MSTLOCG_TBL';
}
