<?php

namespace App\Model\MASTER;

use Illuminate\Database\Eloquent\Model;

class MITMGRP extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'MITMGRP_TBL';
}