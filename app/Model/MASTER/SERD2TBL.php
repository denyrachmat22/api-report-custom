<?php

namespace App\Model\MASTER;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SERD2TBL extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'SERD2_TBL';
}
