<?php

namespace App\Model\MASTER;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SerialMaster extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'SER_TBL';

    public function spl()
    {
        return $this->hasMany('App\Model\MASTER\SPLMaster', 'SPL_JOBNO', 'SER_DOC');
        // return \App\Model\MASTER\SPLMaster::Where("SPL_JOBNO", 'like', trim($this->SER_DOC).'%')->get();
    }

    public function siscn()
    {
        return $this->hasOne('App\Model\MASTER\SISCNMaster', 'SISCN_SER', 'SER_ID');
    }

    public function serd2()
    {
        return $this->hasMany('App\Model\MASTER\SERD2TBL', 'SERD2_SER', 'SER_ID');
    }
}
