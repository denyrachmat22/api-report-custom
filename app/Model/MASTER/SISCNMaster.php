<?php

namespace App\Model\MASTER;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SISCNMaster extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'SISCN_TBL';

    public function si()
    {
        return $this->hasOne('App\Model\MASTER\SIMaster', 'SI_DOC', 'SISCN_DOC');
    }
}
