<?php

namespace App\Model\MASTER;

use Illuminate\Database\Eloquent\Model;

class PriceMaster extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'MSLSPRICE_TBL';
}
