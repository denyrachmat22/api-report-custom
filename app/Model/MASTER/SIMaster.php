<?php

namespace App\Model\MASTER;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SIMaster extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'SI_TBL';
}
