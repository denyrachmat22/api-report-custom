<?php

namespace App\Model\MASTER\MEGAEMS;

use Illuminate\Database\Eloquent\Model;

class pis2Table extends Model
{
    protected $table = 'SRVMEGA.PSI_MEGAEMS.dbo.PIS2_TBL';

    public function scrapDesign()
    {
        return \App\Model\RPCUST\processMapDet::Where('RPDSGN_ITEMCD', trim($this->PIS2_MDLCD))->get();
        // return $this->hasMany('App\Model\RPCUST\processMapDet', 'RPDSGN_ITEMCD', 'PIS2_MDLCD');
    }
}
