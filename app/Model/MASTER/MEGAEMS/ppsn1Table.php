<?php

namespace App\Model\MASTER\MEGAEMS;

use Awobaz\Compoships\Database\Eloquent\Model;

class ppsn1Table extends Model
{
    protected $table = 'SRVMEGA.PSI_MEGAEMS.dbo.PPSN1_TBL';

    public function splscn()
    {
        return $this->hasMany('App\Model\WMS\API\SPLSCN', [
            'SPLSCN_DOC',
            'SPLSCN_LINE',
            'SPLSCN_FEDR'
        ], [
            'PPSN1_PSNNO',
            'PPSN1_LINENO',
            'PPSN1_FR',
        ]);
    }
}
