<?php

namespace App\Model\MASTER\MEGAEMS;

use Illuminate\Database\Eloquent\Model;

class pis3Table extends Model
{
    protected $table = 'SRVMEGA.PSI_MEGAEMS.dbo.PIS3_TBL';

    public function psn()
    {
        return $this->hasOne('App\Model\MASTER\MEGAEMS\ppsn1Table','PPSN1_WONO','PIS3_WONO');
    }
}
