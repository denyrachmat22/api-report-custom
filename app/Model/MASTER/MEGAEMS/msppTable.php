<?php

namespace App\Model\MASTER\MEGAEMS;
use Awobaz\Compoships\Compoships;

use Illuminate\Database\Eloquent\Model;

class msppTable extends Model
{
    use Compoships;

    protected $table = 'PSI_WMS.dbo.XMSPP_HIS';

    public function parent()
    {
        return $this->hasOne('App\Model\MASTER\MEGAEMS\msppTable',['MSPP_MDLCD', 'MSPP_BOMPN'], ['MSPP_MDLCD', 'MSPP_SUBPN']);
    }

    public function child()
    {
        return $this->hasMany('App\Model\MASTER\MEGAEMS\msppTable',['MSPP_SUBPN', 'MSPP_MDLCD'], ['MSPP_BOMPN', 'MSPP_MDLCD']);
    }

    // public function parent()
    // {
    //     return $this->hasOne('App\Model\MASTER\MEGAEMS\msppTable','MSPP_BOMPN','MSPP_SUBPN');
    // }

    // public function child()
    // {
    //     return $this->hasMany('App\Model\MASTER\MEGAEMS\msppTable','MSPP_SUBPN','MSPP_BOMPN');
    // }

    public function allParentList()
    {
        return $this->parent()->with('allParentList');
    }

    public function allChildrenList()
    {
        return $this->child()->with('allChildrenList');
    }
}
