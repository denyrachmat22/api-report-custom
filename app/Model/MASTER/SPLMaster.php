<?php

namespace App\Model\MASTER;

use Illuminate\Database\Eloquent\Model;

class SPLMaster extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'SPL_TBL';

    public function splscn()
    {
        return $this->hasMany('App\Model\WMS\API\SPLSCN','SPLSCN_DOC', 'SPL_DOC');
    }
}
