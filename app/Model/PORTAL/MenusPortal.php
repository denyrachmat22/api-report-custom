<?php

namespace App\Model\PORTAL;

use Illuminate\Database\Eloquent\Model;

class MenusPortal extends Model
{
    protected $connection = 'PSI_PORTAL';
    protected $table = 'psi_portal_menu';

    public function parent()
    {
        return $this->belongsTo('App\Model\PORTAL\MenusPortal','id','menu_parent');
    }

    public function child()
    {
        return $this->hasMany('App\Model\PORTAL\MenusPortal','menu_parent','id');
    }

    public function role()
    {
        return $this->belongsTo('App\Model\PORTAL\RolesPortal','id','menu_id');
    }
}
