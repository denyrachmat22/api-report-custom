<?php

namespace App\Model\PORTAL;

use Illuminate\Database\Eloquent\Model;

class RolesPortal extends Model
{
    protected $connection = 'PSI_PORTAL';
    protected $table = 'psi_portal_role';

    public function menu()
    {
        return $this->hasOne('App\Model\PORTAL\MenusPortal','id','menu_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\PORTAL\UsersPortal','role_identifier','role_id');
    }

    public function group()
    {
        return $this->hasOne('App\Model\PORTAL\DivisisPortal','role_id','role_identifier');
    }
}
