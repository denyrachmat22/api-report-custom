<?php

namespace App\Model\PORTAL;

use Illuminate\Database\Eloquent\Model;

class UsersPortal extends Model
{
    protected $connection = 'PSI_PORTAL';
    protected $table = 'psi_portal_user';
    protected $fillable = [
        'username',
        'email',
        'token',
        'password_hash',
        'password_sha',
        'role_id',
        'status',
        'first_name',
        'last_name',
    ];

    public function role()
    {
        return $this->hasMany('App\Model\PORTAL\RolesPortal','role_identifier','role_id');
    }

    public function menu()
    {
        return $this->hasManyThrough(
            'App\Model\PORTAL\MenusPortal',
            'App\Model\PORTAL\RolesPortal',
            'role_identifier',
            'id',
            'role_id',
            'menu_id'
        );
    }
}
