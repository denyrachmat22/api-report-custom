<?php

namespace App\Model\PORTAL;

use Illuminate\Database\Eloquent\Model;

class DivisisPortal extends Model
{
    protected $connection = 'PSI_PORTAL';
    protected $table = 'psi_portal_divisi';
}
