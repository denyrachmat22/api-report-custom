<?php

namespace App\Model\CIESA;

use Illuminate\Database\Eloquent\Model;

class ciesaToken extends Model
{
    protected $connection = 'PSI_RPCUST';
    protected $table = 'CIESA_AUTH_TOKEN';
    protected $fillable = [
        'access_token',
        'refresh_token'
    ];
}
