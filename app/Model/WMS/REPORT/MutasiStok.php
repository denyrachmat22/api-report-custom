<?php

namespace App\Model\WMS\REPORT;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;

class MutasiStok extends Model
{
    use Compoships;

    protected $connection = 'sqlsrv2';
    protected $table = 'RCV_TBL';

    public function ItemMaster()
    {
        return $this->hasOne('App\Model\MASTER\ItemMaster','MITM_ITMCD','RCV_ITMCD')->select(array('MITM_ITMCD', 'MITM_ITMD1', 'MITM_ITMD2','MITM_STKUOM'));
    }

    public function SuppMaster()
    {
        return $this->hasOne('App\Model\MASTER\SuppMaster','MSUP_SUPCD','RCV_SUPID');
    }

    public function ith()
    {
        return $this->hasMany('App\Model\MASTER\ITHMaster','ITH_DOC','RCV_DONO');
    }

    public function rcvscn()
    {
        return $this->hasMany('App\Model\WMS\API\RCVSCN','RCVSCN_DONO','RCV_DONO');
    }
}
