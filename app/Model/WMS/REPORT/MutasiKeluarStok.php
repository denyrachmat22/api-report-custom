<?php

namespace App\Model\WMS\REPORT;

use Illuminate\Database\Eloquent\Model;

class MutasiKeluarStok extends Model
{
    protected $table = 'DLV_TBL';

    public function ItemMaster()
    {
        return $this->hasOne('App\Model\MASTER\ItemMaster','MITM_ITMCD','DLV_ITMCD')->select(array('MITM_ITMCD', 'MITM_ITMD1', 'MITM_ITMD2','MITM_STKUOM'));
    }

    public function SuppMaster()
    {
        return $this->hasOne('App\Model\MASTER\SuppMaster','MSUP_SUPCD','DLV_CUSTID');
    }
}
