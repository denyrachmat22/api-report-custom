<?php

namespace App\Model\WMS\DESIGNPROC;

use Illuminate\Database\Eloquent\Model;

class detailProcess extends Model
{
    protected $table = 'PROCDET_TBL';
    protected $fillable = [
        'PROCMSTR_ID',
        'PROCDET_SEQ',
        'PROCDET_MDLCD',
        'PROCDET_CD',
        'PROCDET_LINE',
        'PROCDET_CT',
        'PROCDET_AKM',
        'PROCDET_PSSS',
        'PROCDET_ITER_ID',
    ];
}