<?php

namespace App\Model\WMS\DESIGNPROC;

use Illuminate\Database\Eloquent\Model;

class masterProcess extends Model
{
    protected $table = 'PROCMSTR_TBL';
    protected $fillable = [
        'PROCMSTR_PROCD',
        'PROCMSTR_REMARK',
        'PROCMSTR_ISLINE',
        'PROCMSTR_ISCT',
        'PROCMSTR_MFGCD',
    ];
}