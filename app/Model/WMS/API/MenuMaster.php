<?php

namespace App\Model\WMS\API;

use Illuminate\Database\Eloquent\Model;

class MenuMaster extends Model
{
    use \Znck\Eloquent\Traits\BelongsToThrough;

    protected $table = 'MENU_TBL';

    public function parent()
    {
        return $this->belongsTo('App\Model\WMS\API\MenuMaster','MENU_ID','MENU_PRNT');
    }

    public function child()
    {
        return $this->hasMany('App\Model\WMS\API\MenuMaster','MENU_PRNT','MENU_ID')->orderBy('MENU_ID');
    }

    public function role()
    {
        return $this->belongsTo('App\Model\WMS\API\RoleMaster','MENU_ID','EMPACCESS_MENUID');
    }

    public function user()
    {
        return $this->BelongsToThrough(
            'App\Model\WMS\API\UserMaster',
            'App\Model\WMS\API\RoleMaster',
            null,
            '',
            [
                'App\Model\WMS\API\RoleMaster' => 'EMPACCESS_MENUID',
                'App\Model\WMS\API\UserMaster' => 'MSTEMP_GRP'
            ]
        );
    }
}
