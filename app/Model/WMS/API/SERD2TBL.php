<?php

namespace App\Model\WMS\API;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CompositeKey;

class SERD2TBL extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'SERD2_TBL';
}