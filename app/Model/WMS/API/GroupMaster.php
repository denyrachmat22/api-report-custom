<?php

namespace App\Model\WMS\API;

use Illuminate\Database\Eloquent\Model;

class GroupMaster extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.u';
    protected $table = 'MSTGRP_TBL';
}
