<?php

namespace App\Model\WMS\API;

use Awobaz\Compoships\Database\Eloquent\Model;
use App\Helpers\CompositeKey;

class SPLMaster extends Model
{
    use CompositeKey;
    
    protected $connection = 'sqlsrv2';

    public $timestamps = false;

    protected $primaryKey = [
        'SPL_DOC', 
        'SPL_CAT',
        'SPL_LINE',
        'SPL_FEDR',
        'SPL_JOBNO',
        'SPL_ORDERNO',
        'SPL_ITMCD',
        'SPL_MC',
        // 'SPL_QTYUSE',
    ];
    public $incrementing = false;
    protected $table = 'SPL_TBL';
    protected $fillable = [
        'SPL_DOC',
        'SPL_DOCNO',
        'SPL_MC',
        'SPL_CAT',
        'SPL_LINE',
        'SPL_FEDR',
        'SPL_JOBNO',
        'SPL_FG',
        'SPL_ORDERNO',
        'SPL_RACKNO',
        'SPL_ITMCD',
        'SPL_QTYUSE',
        'SPL_QTYREQ',
        'SPL_MS',
        'SPL_LOTSZ',
        'SPL_LUPDT',
        'SPL_USRID',
        'SPL_CUSCD',
        'SPL_PROCD'
    ];

    public function ItemMaster()
    {
        return $this->hasOne('App\Model\MASTER\ItemMaster',trim('MITM_ITMCD'),'SPL_ITMCD')->select(array('MITM_ITMCD', 'MITM_ITMD1', 'MITM_ITMD2','MITM_STKUOM', 'MITM_SPTNO'));
    }
}
