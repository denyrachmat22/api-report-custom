<?php

namespace App\Model\WMS\API;

use Illuminate\Database\Eloquent\Model;

class TXRoute extends Model
{
    protected $table = 'TXROUTE_TBL';
}
