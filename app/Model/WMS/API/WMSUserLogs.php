<?php

namespace App\Model\WMS\API;

use Illuminate\Database\Eloquent\Model;

class WMSUserLogs extends Model
{
    protected $table = 'CSMLOG_TBL';
}
