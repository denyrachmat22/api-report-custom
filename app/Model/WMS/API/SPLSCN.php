<?php

namespace App\Model\WMS\API;

// use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Database\Eloquent\Model;

class SPLSCN extends Model
{
    // use \Awobaz\Compoships\Compoships;
    protected $connection = 'sqlsrv2';

    protected $table = 'SPLSCN_TBL';

    public function returnspl()
    {
        return $this->hasMany('App\Model\WMS\API\RETSCN',[
            'RETSCN_SPLDOC',
            'RETSCN_CAT',
            'RETSCN_LINE',
            'RETSCN_FEDR',
            'RETSCN_ORDERNO'
        ],[
            'SPLSCN_DOC',
            'SPLSCN_CAT',
            'SPLSCN_LINE',
            'SPLSCN_FEDR',
            'SPLSCN_ORDERNO',
        ]);
    }

    public function splMaster()
    {
        return $this->hasOne('App\Model\WMS\API\SPLMaster',[
            'SPL_DOC',
            'SPL_CAT',
            'SPL_LINE',
            'SPL_FEDR',
            'SPL_ORDERNO',
            'SPL_ITMCD'
        ],[
            'SPLSCN_DOC',
            'SPLSCN_CAT',
            'SPLSCN_LINE',
            'SPLSCN_FEDR',
            'SPLSCN_ORDERNO',
            'SPLSCN_ITMCD'
        ]);
    }
}
