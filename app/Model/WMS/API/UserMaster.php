<?php

namespace App\Model\WMS\API;

use Illuminate\Database\Eloquent\Model;

class UserMaster extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'MSTEMP_TBL';

    public function role()
    {
        return $this->hasMany('App\Model\WMS\API\RoleMaster','EMPACCESS_GRPID','MSTEMP_GRP');
    }

    public function menu()
    {
        return $this->hasManyThrough(
            'App\Model\WMS\API\MenuMaster',
            'App\Model\WMS\API\RoleMaster',
            'EMPACCESS_GRPID',
            'MENU_ID',
            'MSTEMP_GRP',
            'EMPACCESS_MENUID'
        );
    }
}
