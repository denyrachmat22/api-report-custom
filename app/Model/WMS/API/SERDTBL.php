<?php

namespace App\Model\WMS\API;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CompositeKey;

class SERDTBL extends Model
{
    protected $connection = 'PSI_RPCUST';
    protected $table = 'v_serd_tbl_opt';
    // protected $connection = 'sqlsrv2';
    // protected $table = 'SERD_TBL';
}
