<?php

namespace App\Model\WMS\API;

use Illuminate\Database\Eloquent\Model;

class RCVSCN2 extends Model
{
    protected $connection = 'sqlsrv2';
    protected $table = 'RCVSCN_TBL';

    protected $primaryKey = 'RCVSCN_ID'; // or null
    public $incrementing = false;

    protected $fillable = [
        'RCVSCN_ID',
        'RCVSCN_DONO',
        'RCVSCN_LOCID',
        'RCVSCN_ITMCD',
        'RCVSCN_LOTNO',
        'RCVSCN_QTY',
        'RCVSCN_SAVED',
        'RCVSCN_LUPDT',
        'RCVSCN_USRID',
        'RCVSCN_STATUS',
    ];
}
