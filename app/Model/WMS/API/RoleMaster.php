<?php

namespace App\Model\WMS\API;

use Illuminate\Database\Eloquent\Model;

class RoleMaster extends Model
{
    public function getDateFormat()
    {
        return 'Y-m-d H:i:s.u';
    }
    protected $table = 'EMPACCESS_TBL';

    public function menu()
    {
        return $this->hasMany('App\Model\WMS\API\MenuMaster', 'MENU_ID', 'EMPACCESS_MENUID');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\WMS\API\UserMaster', 'EMPACCESS_GRPID', 'MSTEMP_GRP');
    }

    public function group()
    {
        return $this->hasOne('App\Model\WMS\API\GroupMaster', 'MSTGRP_ID', 'EMPACCESS_GRPID');
    }
}
