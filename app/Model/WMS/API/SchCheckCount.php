<?php

namespace App\Model\WMS\API;

use Illuminate\Database\Eloquent\Model;

class SchCheckCount extends Model
{
    public $timestamps = true;
    protected $table = 'scheduller_time_check';
    protected $fillable = [
        'count',
        'jobs_name',
    ];
}
