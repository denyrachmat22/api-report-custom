<?php

namespace App\Model\WMS\API;

use Illuminate\Database\Eloquent\Model;

class ViewSPL extends Model
{
    protected $table = 'SPL_VIEW';
}
