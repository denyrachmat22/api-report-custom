<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;

class StockExBC extends Model
{
    protected $connection = 'PSI_RPCUST';
    protected $table = 'v_stock_exbc';
}
