<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CompositeKey;

class MutasiDokPabean extends Model
{
    use CompositeKey;

    public $timestamps = true;
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPMUT_PABEAN';
    protected $primaryKey = [
        'RPBEA_DOC',
        'RPBEA_ITMCOD',
        'RPBEA_NUMPEN',
        'RPBEA_QTYSAT'
    ];

    protected $fillable = [
        'RPBEA_JENBEA',
        'RPBEA_NUMBEA',
        'RPBEA_TGLBEA',
        'RPBEA_NUMSJL',
        'RPBEA_TGLSJL',
        'RPBEA_NUMPEN',
        'RPBEA_TGLPEN',
        'RPBEA_CUSSUP',
        'RPBEA_ITMCOD',
        'RPBEA_QTYSAT',
        'RPBEA_QTYJUM',
        'RPBEA_UNITMS',
        'RPBEA_VALAS',
        'RPBEA_QTYTOT',
        'RPBEA_TYPE',
        'RPBEA_RCVEDT',
        'RPBEA_DOC',
        'RPBEA_CONSIGNEE'
    ];
}
