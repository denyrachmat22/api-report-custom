<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CompositeKey;

class MutasiScrap extends Model
{
    use CompositeKey;
    public $timestamps = true;
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPSAL_REJECT';
    protected $primaryKey = [
        'RPREJECT_REF',
        'RPREJECT_ITMCOD'
    ];
    protected $fillable = [
        'RPREJECT_UNITMS',
        'RPREJECT_QTYTOT',
        'RPREJECT_QTYOUT',
        'RPREJECT_SA',
        'RPREJECT_QTYOPN',
        'RPREJECT_QTYINC',
        'RPREJECT_QTYADJ',
        'RPREJECT_KET',
        'RPREJECT_ITMCOD',
        'RPREJECT_DATEIS',
        'RPREJECT_REF',
    ];
}
