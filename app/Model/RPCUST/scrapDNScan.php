<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CompositeKey;

class scrapDNScan extends Model
{
    public $timestamps = true;
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPSCRAP_DN_SCAN';

    protected $fillable = [
        'RDL_ID',
        'RDS_QTY',
        'RDS_LOT',
        'RDS_ITMCD',
        'RDS_ISSAVED'
    ];
}