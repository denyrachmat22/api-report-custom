<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CompositeKey;

class WIPDone extends Model
{
    use CompositeKey;
    
    public $timestamps = true;
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPWIP_DONE_INC';

    protected $primaryKey = [
        'line',
        'doc_num'
    ];

    protected $fillable = [
        'line',
        'doc_num'
    ];
}