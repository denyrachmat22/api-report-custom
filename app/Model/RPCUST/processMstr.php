<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;

class processMstr extends Model
{
    public $timestamps = true;
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPDSGN_PROCESS_MSTR';
    protected $primaryKey = 'RPDSGN_CODE';
    protected $keyType = 'string';
    protected $fillable = [
        'RPDSGN_CODE',
        'RPDSGN_DESC',
    ];
}
