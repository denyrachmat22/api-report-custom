<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CompositeKey;

class processMapDet extends Model
{
    use CompositeKey;

    public $timestamps = true;
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPDSGN_PROCESS_MAP_DET';
    protected $primaryKey = [
        'RPDSGN_PRO_ID',
        'RPDSGN_ITEMCD',
        // 'RPDSGN_PROCD',
        'RPDSGN_MSTR_ID'
    ];

    protected $fillable = [
        'RPDSGN_PRO_ID',
        'RPDSGN_ITEMCD',
        'RPDSGN_PROCD',
        'RPDSGN_CODE',
        'RPDSGN_MSTR_ID'
    ];

    public function processMstr()
    {
        return $this->hasOne('App\Model\RPCUST\processMstr','RPDSGN_CODE','RPDSGN_PRO_ID');
    }    

    public function items()
    {
        return $this->hasOne('App\Model\MASTER\ItemMaster','MITM_ITMCD','RPDSGN_ITEMCD');
    }

    public function dsgMaster()
    {
        return $this->belongsTo('App\Model\RPCUST\processMapMstr','RPDSGN_MSTR_ID','id');
    }
}
