<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;

class PabeanInOutLogs extends Model
{
    protected $connection = 'PSI_RPCUST';
    protected $table = 'LOGS_INTRFC_PABEAN';
    protected $fillable = [
        'LIP_TYPE_TRANS',
        'LIP_DOC_NO',
        'LIP_FLAG',
        'LIP_DESC',
    ];
}