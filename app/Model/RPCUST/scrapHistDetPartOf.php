<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class scrapHistDetPartOf extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPSCRAP_HIST_PRT_OF_DET';
    protected $fillable = [
        'SCRAP_HIST_ID',
        'PRTOF_ITMCD',
        'PRTOF_QTY',
    ];

    protected $dates = ['deleted_at'];
}
