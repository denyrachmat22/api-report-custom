<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;

class UserLogs extends Model
{
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPSAL_USERS_HIST';

    protected $fillable = [
        'username',
        'type_log',
        'log_desc',
    ];
}
