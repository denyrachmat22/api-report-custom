<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;

class MutasiMesin extends Model
{
    public $timestamps = true;
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPSAL_MCH';
    protected $primaryKey = [
        'RPMCH_REF',
        'RPMCH_ITMCOD'
    ];

    protected $fillable = [
        'RPMCH_UNITMS',
        'RPMCH_QTYTOT',
        'RPMCH_QTYOUT',
        'RPMCH_SA',
        'RPMCH_QTYOPN',
        'RPMCH_QTYINC',
        'RPMCH_QTYADJ',
        'RPMCH_KET',
        'RPMCH_ITMCOD',
        'RPMCH_DATEIS',
        'RPMCH_REF',
    ];
}
