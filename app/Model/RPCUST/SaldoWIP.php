<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CompositeKey;

class SaldoWIP extends Model
{
    use CompositeKey;

    public $timestamps = true;
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPSAL_WIP';

    protected $primaryKey = [
        'RPWIP_TYPE',
        'RPWIP_PSN',
        'RPWIP_CAT',
        'RPWIP_LINE',
        'RPWIP_FR',
        'RPWIP_JOB',
        'RPWIP_MCH',
        'RPWIP_LOT',
        'RPWIP_SER'
    ];

    protected $fillable = [
        'RPWIP_DATEIS',
        'RPWIP_ITMCOD',
        'RPWIP_UNITMS',
        'RPWIP_QTYTOT',
        'RPWIP_TYPE',
        'RPWIP_PSN',
        'RPWIP_CAT',
        'RPWIP_LINE',
        'RPWIP_FR',
        'RPWIP_JOB',
        'RPWIP_MCH',
        'RPWIP_LOT',
        'RPWIP_SER'
    ];
}
