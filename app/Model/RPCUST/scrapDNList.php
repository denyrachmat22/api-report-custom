<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CompositeKey;

class scrapDNList extends Model
{
    public $timestamps = true;
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPSCRAP_DN_LIST';

    protected $fillable = [
        'RDL_PICK_ID',
        'RDL_ITMCD',
        'RDL_QTY',
        'RDL_SUPCD',
        'RDL_DEBTO',
        'RDL_BG'
    ];
}
