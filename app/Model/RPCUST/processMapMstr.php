<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;

class processMapMstr extends Model
{
    public $timestamps = true;
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPDSGN_PROCESS_MAP_MSTR';
    protected $primaryKey = 'RPDSGN_DESC';
    protected $keyType = 'string';
    protected $fillable = [
        'RPDSGN_DESC',
    ];

    public function processDet()
    {
        return $this->hasMany('App\Model\RPCUST\processMapDet','RPDSGN_MSTR_ID','id');
    }
}
