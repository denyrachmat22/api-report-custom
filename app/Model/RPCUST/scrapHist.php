<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class scrapHist extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPSCRAP_HIST';
    protected $fillable = [
        'DSGN_MAP_ID',
        'USERNAME',
        'QTY',
        'QTY_SCRAP',
        'ID_TRANS',
        'TYPE_TRANS',
        'DOC_NO',
        'REASON',
        'SCR_PROCD',
        'DEPT',
        'MDL_FLAG',
        'ITEMNUM',
        'IS_RETURN',
        'DATE_OUT',
        'IS_DELETE',
        'REF_NO',
        'MAIN_REASON',
        'MENU_ID',
        'IS_CONFIRMED',
        'IS_CONFIRMED_PPC',
        'DISPOSED_DOC',
        'DISPOSED_DATE',
        'PURCH_DATE',
        'SCRAP_UNDER_REMARK'
    ];

    protected $dates = ['deleted_at'];

    public function designMap()
    {
        return $this->hasOne('App\Model\RPCUST\processMapDet','id', 'DSGN_MAP_ID');
    }

    public function users()
    {
        return $this->hasOne('App\Model\WMS\API\UserMaster','MSTEMP_ID','USERNAME');
    }

    public function histDet()
    {
        return $this->hasMany('App\Model\RPCUST\scrapHistDet', 'SCR_HIST_ID', 'id');
    }
}
