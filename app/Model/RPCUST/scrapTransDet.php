<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class scrapTransDet extends Model
{
    // use SoftDeletes;
    public $timestamps = true;
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPSCRAP_TRANS_DET';
    protected $fillable = [
        'SH_ID',
        'STD_QTY',
        'STD_TYPE',
    ];
}