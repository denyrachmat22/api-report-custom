<?php

namespace App\Model\RPCUST;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class dispHist extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $connection = 'PSI_RPCUST';
    protected $table = 'RPDISP_HIST';
    protected $fillable = [
        'RPDISP_UNAME_CONF',
        'RPDISP_UNAME',
        'RPDISP_LDT',
        'RPDISP_ISCONF',
        'RPDISP_ID',
        'RPDISP_FDT',
        'RPDISP_DEPT',
        'RPDISP_REMARK',
        'RPDISP_ISDISPDN'
    ];
}
