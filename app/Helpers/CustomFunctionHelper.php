<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use App\Model\WMS\API\SchCheckCount;
use Illuminate\Support\Facades\Log;

class CustomFunctionHelper
{
    public static function defineOperator($op)
    {
        if ($op == 'equals') {
            return [
                'valadd' => '',
                'operator' => '='
            ];
        } elseif ($op == 'start_at') {
            return [
                'valadd' => '%',
                'valpos' => 'f',
                'operator' => 'like'
            ];
        } elseif ($op == 'contain') {
            return [
                'valadd' => '%',
                'valpos' => 'fr',
                'operator' => 'like'
            ];
        } elseif ($op == 'more_than') {
            return [
                'valadd' => '',
                'operator' => '>'
            ];
        } elseif ($op == 'less_than') {
            return [
                'valadd' => '',
                'operator' => '<'
            ];
        } elseif ($op == 'more_than_equal') {
            return [
                'valadd' => '',
                'operator' => '>='
            ];
        } elseif ($op == 'less_than_equal') {
            return [
                'valadd' => '',
                'operator' => '<='
            ];
        } elseif ($op == 'range') {
            return [
                [
                    'valadd' => '',
                    'operator' => '>='
                ],
                [
                    'valadd' => '',
                    'operator' => '<='
                ],
            ];
        }
    }

    public static function filtering($collection, $filter)
    {
        foreach ($filter as $key => $valFilter) {
            $opr = CustomFunctionHelper::defineOperator($valFilter['op']);
            $valsel = isset($valFilter['valselect']) ? $valFilter['valselect'] : null;
            if (!empty($valFilter['val']) || !empty($valFilter['val2']) || $valsel) {
                // Range
                if (isset($opr[0])) {
                    if (!empty($valFilter['val']) || !empty($valFilter['val2'])) {
                        $collection
                            ->where($valFilter['field']['field'], $opr[0]['operator'], $valFilter['val'])
                            ->where($valFilter['field']['field'], $opr[1]['operator'], $valFilter['val2']);
                    } else {
                        $collection;
                    }
                } else {
                    if (!empty($opr['valadd'])) { //Contains
                        $collection
                            ->where(
                                $valFilter['field']['field'],
                                $opr['operator'],
                                $opr['valpos'] == 'f' ? $opr['valadd'] . $valFilter['val'] : ($opr['valpos'] == 'r' ? $valFilter['val'] . $opr['valadd'] : ($opr['valpos'] == 'fr' ? $opr['valadd'] . $valFilter['val'] . $opr['valadd'] : $valFilter['val'] . $opr['valadd']))
                            );
                    } else {
                        if ($valFilter['field']['type'] === 'select') {
                            $collection
                                ->whereIn($valFilter['field']['field'], $valFilter['valselect']);
                        } else {
                            $collection
                                ->where($valFilter['field']['field'], $opr['operator'], $valFilter['val']);
                        }
                    }
                }
            }
        }

        $arr = array();

        foreach ($filter as $key => $item) {
            $arr[] = $item['field']['field'];
        }

        $counts = array_count_values($arr);

        // file_put_contents('logs.txt', $counts['RPBEA_JENBEA'] . PHP_EOL, FILE_APPEND | LOCK_EX);
        return $collection;
    }

    public static function CheckSchedullerCount($jobs,$count)
    {        
        $cekcount = SchCheckCount::where('jobs_name','TarikDatakeTableSPL')->orderBy('id','desc')->first();
        
        if (empty($cekcount)) {
            SchCheckCount::insert([
                'count' => $count,
                'jobs_name' => $jobs
            ]);

            return ['cek'=>'kosong'];
        } else {
            $lasthasil = $cekcount->count - $count;
            if ($lasthasil !== 0) {
                SchCheckCount::insert([
                    'count' => $count,
                    'jobs_name' => $jobs
                ]);

                return ['cek'=>'beda'];
            }

            return ['cek'=>'sama'];
        }
    }
}
