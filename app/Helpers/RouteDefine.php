<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use App\Model\WMS\API\TXRoute;

class RouteDefine {

    public static function index($id, $wh)
    {
        $cek = TXRoute::where('TXROUTE_ID', $id)->where('TXROUTE_WH', $wh)->first();

        return $cek;
    }
}