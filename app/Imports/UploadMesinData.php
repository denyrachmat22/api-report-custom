<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use App\Model\RPCUST\MutasiMesin;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\Model\MASTER\ItemMaster;
use Maatwebsite\Excel\Concerns\WithValidation;

class UploadMesinData implements ToModel, WithStartRow, WithValidation
{

    public $data;

    public function startRow(): int
    {
        return 2;
    }

    /**
     * @param Model $collection
     */
    public function model(array $row)
    {
        $getitem = ItemMaster::where('MITM_ITMCD', $row[0])->where('MITM_INDIRMT',0)->first();

        return new MutasiMesin([
            'RPMCH_ITMCOD' => $row[0],
            'RPMCH_UNITMS' => trim($getitem['MITM_STKUOM']),
            'RPMCH_DATEIS' => date($row[1]),
            'RPMCH_QTYINC' => $row[2],
            'RPMCH_QTYOUT' => $row[3],
            'RPMCH_QTYTOT' => $row[2] - $row[3],
            'RPMCH_QTYOPN' => $row[4],
            'RPMCH_KET' => empty($row[5]) ? null : $row[5]
        ]);
    }

    public function rules(): array
    {
        return [
            '0' => 'required|string|exists:MITM_TBL,MITM_ITMCD,MITM_INDIRMT,0',
            '1' => 'required|string',
        ];
    }

    public function customValidationMessages()
    {
        return [
            '0.required' => 'Item tidak boleh kosong.',
            '0.string' => 'Format Item harus string.',
            '0.exists' => 'Item tidak ditemukan di Master Item atau item bukan Item mesin & Peralatan.',
            '1.required' => 'Tanggal issue tidak boleh kosong.',
            '1.string' => 'Format tanggal harus string.',
        ];
    }
}
