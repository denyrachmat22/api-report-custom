<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithMappedCells;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Model\WMS\DESIGNPROC\detailProcess;
use App\Model\WMS\DESIGNPROC\masterProcess;

class uploadDesignProcessMaster implements ToCollection, WithStartRow
{
    private $mfg;
    public function __construct($mfg)
    {
        $this->mfg = $mfg;
    }

    public function startRow(): int
    {
        return 8;
    }

    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        detailProcess::join('PROCMSTR_TBL', 'PROCMSTR_TBL.id', 'PROCMSTR_ID')
            // ->where('PROCMSTR_MFGCD', $this->mfg)
            ->delete();

        $listData = [];
        $listHeaderProcess = [];
        foreach ($rows as $key => $value) {
            if ($key > 1) {
                $listData[] = $value;
            }

            if ($key === 0) {
                foreach ($value as $keyCols => $valueCols) {
                    if ($keyCols > 8 && !empty($valueCols)) {
                        $listHeaderProcess[$keyCols] = $valueCols;
                    }
                }
            }
        }

        foreach ($listData as $keyData => $valueData) {
            $dataHasil = [];

            // In Process store
            $seq = 1;
            $keyNew = 0;
            $idnya = $this->getIterID();
            foreach ($valueData as $keyDet => $valueDet) {
                if ($keyDet >= 5 && $keyDet <= 8) {
                    if (!empty($valueDet)) {
                        $dataMaster = $this->getCTLINE($valueDet, $listHeaderProcess, $valueData);
                        if (!empty($dataMaster['Line'] && $dataMaster['CT'])) {
                            $dataHasil[$keyNew] = [
                                'PROCMSTR_ID' => $dataMaster['id'],
                                'PROCDET_SEQ' => $seq,
                                'PROCDET_MDLCD' => $valueData[3],
                                'PROCDET_CD' => $valueData[4],
                                'PROCDET_LINE' => $dataMaster['Line'],
                                'PROCDET_CT' => $dataMaster['CT'],
                                'PROCDET_AKM' => '',
                                'PROCDET_PSSS' => $dataMaster['Line'] === 'PSSS' ? true : false,
                                'PROCDET_ITER_ID' => $idnya
                                // 'HeaderList' => json_encode($listHeaderProcess)
                            ];

                            $seq++;
                            $keyNew++;
                        }
                    }
                }
            }

            // Outside Process
            foreach ($listHeaderProcess as $keyHeader => $valueHeader) {
                $dataMaster = $this->getCTLINE($valueHeader, $listHeaderProcess, $valueData, 0);

                if (!empty($dataMaster['Line'] && $dataMaster['CT'])) {
                    $dataHasil[$keyNew] = [
                        'PROCMSTR_ID' => $dataMaster['id'],
                        'PROCDET_SEQ' => $seq,
                        'PROCDET_MDLCD' => $valueData[3],
                        'PROCDET_CD' => $valueData[4],
                        'PROCDET_LINE' => $dataMaster['Line'],
                        'PROCDET_CT' => $dataMaster['CT'],
                        'PROCDET_AKM' => '',
                        'PROCDET_PSSS' => $dataMaster['Line'] === 'PSSS' ? true : false,
                        'PROCDET_ITER_ID' => $idnya
                    ];

                    $seq++;
                    $keyNew++;
                }
            }

            if (count($dataHasil) > 0) {
                foreach ($dataHasil as $keyHasil => $valueHasil) {
                    detailProcess::create($valueHasil);
                }

                logger(json_encode($dataHasil));
            }
        }
    }

    public function getIterID()
    {
        $getLatestData = detailProcess::select('PROCDET_ITER_ID')->whereBetween('created_at', [date('Y-m-01'), date('Y-m-t')])->orderBy('id', 'desc')->first();

        if (empty($getLatestData)) {
            $iter = 'PROC-DS-' . date('Y') . '-' . date('m') . '-0001';
        } else {
            $iter = 'PROC-DS-' . date('Y') . '-' . date('m') . '-' .sprintf('%04d', (int)substr($getLatestData->PROCDET_ITER_ID, -4) + 1);
        }

        return $iter;
    }

    public function getCTLINE($proc, $listHeaderProcess, $valueDataRows, $isProcess = 1)
    {
        $getDataLine = array_filter($listHeaderProcess, function ($f) use ($proc) {
            return $f === ($proc == 'SMT-RG' ? 'SMT-RD' : $proc);
        });

        $getData = masterProcess::where('PROCMSTR_PROCD', $proc == 'SMT-RG' ? 'SMT-RD' : $proc)->where('PROCMSTR_ISPROCESS', $isProcess)->first();

        if (count($getDataLine) > 0 && !empty($getData) && $getData['PROCMSTR_ISLINE'] == 1) {
            $getLine = $valueDataRows[(array_keys($getDataLine)[0])];
            // $getLine = array_keys($getDataLine);
        } else {
            $getLine = '';
        }

        if (count($getDataLine) > 0 && !empty($getData) && $getData['PROCMSTR_ISCT'] == 1) {
            $getCT = $valueDataRows[(array_keys($getDataLine)[0]) + 1];
            // $getCT = array_keys($getDataLine);
        } else {
            $getCT = '';
        }

        return [
            'id' => !empty($getData) ? $getData['id'] : '',
            'Line' => $getLine,
            'CT' => $getCT
        ];
    }
}
