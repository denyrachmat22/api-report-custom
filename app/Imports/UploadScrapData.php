<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Model\RPCUST\MutasiScrap;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\Model\MASTER\ItemMaster;
use Maatwebsite\Excel\Concerns\WithValidation;

class UploadScrapData implements ToModel, WithStartRow, WithValidation
{

    public $data;

    public function startRow(): int
    {
        return 2;
    }

    /**
     * @param Model $collection
     */
    public function model(array $row)
    {
        $getitem = ItemMaster::where('MITM_ITMCD', $row[0])->first();

        if (!empty($getitem)) {
            return new MutasiScrap([
                'RPREJECT_ITMCOD' => $row[0],
                'RPREJECT_UNITMS' => trim($getitem['MITM_STKUOM']),
                'RPREJECT_DATEIS' => date($row[1]),
                'RPREJECT_QTYINC' => $row[2],
                'RPREJECT_QTYOUT' => $row[3],
                'RPREJECT_QTYTOT' => $row[2] - $row[3],
                'RPREJECT_QTYOPN' => $row[4],
                'RPREJECT_KET' => empty($row[5]) ? null : $row[5]
            ]);
        } else {
            $data = 'item_not_found';
            $this->data = $data;
        }
    }

    public function rules(): array
    {
        return [
            '0' => 'required|string|exists:XMITM_TBL,MITM_ITMCD',
            '1' => 'required|string',
        ];
    }

    public function customValidationMessages()
    {
        return [
            '0.required' => 'Item tidak boleh kosong.',
            '0.string' => 'Format Item harus string.',
            '0.exists' => 'Item tidak ditemukan di Master Item.',
            '1.required' => 'Tanggal issue tidak boleh kosong.',
            '1.string' => 'Format tanggal harus string.',
        ];
    }
}
