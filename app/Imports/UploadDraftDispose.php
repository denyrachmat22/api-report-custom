<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use App\Model\RPCUST\MutasiMesin;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;

use App\Model\RPCUST\scrapDNList;
use App\Model\MASTER\ItemMaster;
use App\Model\RPCUST\dispHist;

class UploadDraftDispose implements ToModel, WithStartRow
{

    public $users, $idDisp;
    public function __construct($users, $idDisp) {
        $this->users = $users;
        $this->idDisp = $idDisp;
    }

    public function startRow(): int
    {
        return 2;
    }

    public function model(array $row)
    {
        // $getitem = ItemMaster::where('MITM_ITMCD', $row[0])->where('MITM_INDIRMT',0)->first();

        $cek = scrapDNList::whereBetween('created_at', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59'])->orderBy('RDL_PICK_ID', 'desc')->first();
        $idDisposeDN = 'DNDISP/' . date('y/m/d') . '/' . (empty($cek) ? '0001' : sprintf('%04d', (int) substr($cek->RDL_PICK_ID, -3) + 1));

        scrapDNList::updateOrCreate([
            'RDL_PICK_ID' => $this->idDisp,
            'RDL_ITMCD' => $row[0],
        ], [
            'RDL_PICK_ID' => $this->idDisp,
            'RDL_ITMCD' => $row[0],
            'RDL_QTY' => $row[1],
            'RDL_SUPCD' => $row[2],
            'RDL_DEBTO' => $row[3],
        ]);

        return dispHist::create([
            'RPDISP_ID' => $this->idDisp,
            'RPDISP_FDT' => date('Y-m-01'),
            'RPDISP_LDT' => date('Y-m-t'),
            'RPDISP_UNAME' => $this->users,
            'RPDISP_REMARK' => 'DISPOSE DN ' . date('d M Y h:i:s'),
            'RPDISP_DEPT' => 'PPIC',
            'RPDISP_ISDISPDN' => 1,
        ]);
    }

    // public function rules(): array
    // {
    //     return [
    //         '0' => 'required|string|exists:MITM_TBL,MITM_ITMCD,MITM_INDIRMT,0',
    //         '1' => 'required|int',
    //     ];
    // }

    // public function customValidationMessages()
    // {
    //     return [
    //         '0.required' => 'Item tidak boleh kosong.',
    //         '0.string' => 'Format Item harus string.',
    //         '0.exists' => 'Item tidak ditemukan di Master Item atau item bukan Item mesin & Peralatan.',
    //         '1.required' => 'Tanggal issue tidak boleh kosong.',
    //         '1.string' => 'Format tanggal harus string.',
    //     ];
    // }
}
