<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RPSALRAWMAT extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('PSI_RPCUST')->create('RPSAL_RAW', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('RPRAW_ITMCOD')->index()->nullable();
            $table->string('RPRAW_UNITMS')->index()->nullable();
            $table->date('RPRAW_DATEIS')->index()->nullable();
            $table->integer('RPRAW_QTYINC')->nullable();
            $table->integer('RPRAW_QTYOUT')->nullable();
            $table->integer('RPRAW_QTYADJ');
            $table->integer('RPRAW_QTYTOT');
            $table->integer('RPRAW_QTYOPN');
            $table->string('RPRAW_KET')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RPSAL_RAW');
    }
}
