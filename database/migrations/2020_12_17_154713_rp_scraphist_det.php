<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RpScraphistDet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('PSI_RPCUST')->create('RPSCRAP_HIST_DET', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('SCR_HIST_ID')->index();
            $table->string('SCR_PSN')->nullable();
            $table->string('SCR_CAT')->nullable();
            $table->string('SCR_LINE')->nullable();
            $table->string('SCR_FR')->nullable();
            $table->string('SCR_MC')->nullable();
            $table->string('SCR_MCZ')->nullable();
            $table->string('SCR_ITMCD')->nullable();
            $table->string('SCR_QTPER')->nullable();
            $table->integer('SCR_QTY')->nullable();
            $table->string('SCR_LOTNO')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('PSI_RPCUST')->dropIfExists('RPSCRAP_HIST_DET');
    }
}
