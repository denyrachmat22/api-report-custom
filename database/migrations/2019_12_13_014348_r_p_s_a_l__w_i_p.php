<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RPSALWIP extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('PSI_RPCUST')->create('RPSAL_WIP', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('RPWIP_DATEIS')->index()->nullable();
            $table->string('RPWIP_ITMCOD')->index()->nullable();
            $table->string('RPWIP_UNITMS')->index()->nullable();
            $table->integer('RPWIP_QTYTOT')->index()->nullable();
            $table->string('RPWIP_TYPE')->index()->nullable();
            $table->string('RPWIP_PSN')->index()->nullable();
            $table->string('RPWIP_CAT')->index()->nullable();
            $table->string('RPWIP_LINE')->index()->nullable();
            $table->string('RPWIP_FR')->index()->nullable();
            $table->string('RPWIP_JOB')->index()->nullable();
            $table->string('RPWIP_MCH')->index()->nullable();
            $table->string('RPWIP_LOT')->index()->nullable();
            $table->string('RPWIP_SER')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('PSI_RPCUST')->dropIfExists('RPSAL_WIP');
    }
}
