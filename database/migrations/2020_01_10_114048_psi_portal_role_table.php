<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PsiPortalRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('PSI_PORTAL')->create('psi_portal_role', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('role_identifier')->index();
            $table->string('menu_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('PSI_PORTAL')->dropIfExists('psi_portal_role');
    }
}
