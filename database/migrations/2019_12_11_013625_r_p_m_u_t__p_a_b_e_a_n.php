<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RPMUTPABEAN extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('PSI_RPCUST')->create('RPMUT_PABEAN', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('RPBEA_JENBEA')->index()->nullable();
            $table->string('RPBEA_NUMBEA')->index()->nullable();
            $table->date('RPBEA_TGLBEA')->index()->nullable();
            $table->string('RPBEA_NUMSJL')->index()->nullable();
            $table->date('RPBEA_TGLSJL')->index()->nullable();
            $table->string('RPBEA_NUMPEN')->nullable();
            $table->date('RPBEA_TGLPEN')->nullable();
            $table->string('RPBEA_CUSSUP');
            $table->string('RPBEA_ITMCOD')->index();
            $table->integer('RPBEA_QTYSAT');
            $table->integer('RPBEA_QTYJUM');
            $table->string('RPBEA_UNITMS');
            $table->string('RPBEA_VALAS');
            $table->integer('RPBEA_QTYTOT');
            $table->string('RPBEA_TYPE');
            $table->date('RPBEA_RCVEDT')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RPMUT_PABEAN');
    }
}
