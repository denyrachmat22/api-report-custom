<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RPSALFINGOOD extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('PSI_RPCUST')->create('RPSAL_FINGOOD', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('RPGOOD_ITMCOD')->index()->nullable();
            $table->string('RPGOOD_UNITMS')->index()->nullable();
            $table->date('RPGOOD_DATEIS')->index()->nullable();
            $table->integer('RPGOOD_QTYINC')->nullable();
            $table->integer('RPGOOD_QTYOUT')->nullable();
            $table->integer('RPGOOD_QTYADJ');
            $table->integer('RPGOOD_QTYTOT');
            $table->integer('RPGOOD_QTYOPN');
            $table->string('RPGOOD_KET')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RPSAL_GOOD');
    }
}
