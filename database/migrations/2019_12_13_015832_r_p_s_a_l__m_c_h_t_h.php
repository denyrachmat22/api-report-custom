<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RPSALMCHTH extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('PSI_RPCUST')->create('RPSAL_MCH', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('RPMCH_ITMCOD')->index()->nullable();
            $table->string('RPMCH_UNITMS')->index()->nullable();
            $table->date('RPMCH_DATEIS')->index()->nullable();
            $table->integer('RPMCH_QTYINC')->nullable();
            $table->integer('RPMCH_QTYOUT')->nullable();
            $table->integer('RPMCH_QTYADJ');
            $table->integer('RPMCH_QTYTOT');
            $table->integer('RPMCH_QTYOPN');
            $table->string('RPMCH_KET')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RPSAL_MCH');
    }
}
