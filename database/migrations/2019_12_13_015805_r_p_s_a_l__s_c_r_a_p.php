<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RPSALSCRAP extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('PSI_RPCUST')->create('RPSAL_REJECT', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('RPREJECT_ITMCOD')->index()->nullable();
            $table->string('RPREJECT_UNITMS')->index()->nullable();
            $table->date('RPREJECT_DATEIS')->index()->nullable();
            $table->integer('RPREJECT_QTYINC')->nullable();
            $table->integer('RPREJECT_QTYOUT')->nullable();
            $table->integer('RPREJECT_QTYADJ');
            $table->integer('RPREJECT_QTYTOT');
            $table->integer('RPREJECT_QTYOPN');
            $table->string('RPREJECT_KET')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RPSAL_REJECT');
    }
}
