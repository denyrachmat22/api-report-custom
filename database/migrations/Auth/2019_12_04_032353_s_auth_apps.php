<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SAuthApps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('PSI_AUTH')->create('s_auth_apps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('appsName');
            $table->text('appsDesc');
            $table->text('appsLink');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_auth_apps');
    }
}
