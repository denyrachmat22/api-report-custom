<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSAuthMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('PSI_AUTH')->create('s_auth_menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('menuName');
            $table->text('menuParent');
            $table->text('menuLink');
            $table->text('id_apps');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_auth_menus');
    }
}
