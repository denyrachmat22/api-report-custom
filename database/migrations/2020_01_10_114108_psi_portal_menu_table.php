<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PsiPortalMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('PSI_PORTAL')->create('psi_portal_menu', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('menu_name');
            $table->string('menu_desc');
            $table->string('menu_parent');
            $table->string('menu_url')->nullable();
            $table->string('menu_icon')->nullable();
            $table->integer('menu_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('PSI_PORTAL')->dropIfExists('psi_portal_menu');
    }
}
