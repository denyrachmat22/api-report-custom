<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Faker\Factory as Faker;
use App\Model\WMS\REPORT\MutasiStok;

class ITHTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $year = rand(2009, 2019);
        $month = rand(1, 12);
        $day = rand(1, 28);

        $date = Carbon::create($year,$month ,$day , 0, 0, 0);
        $number = rand(1,9999);

        $getData = MutasiStok::where('RCV_DONO','=',$this->getDataReceive())->get();
                   
        $faker = Faker::create();

        foreach ($getData as $key => $value) {
            $cek = DB::table('ITH_TBL')->where('ITH_DOC',$value['RCV_DONO'])->where('ITH_ITMCD',$value['RCV_ITMCD'])->first();

            if (empty($cek)) {
                DB::table('ITH_TBL')->insert([
                    'ITH_ITMCD' => $value['RCV_ITMCD'],
                    'ITH_DATE' => $value['RCV_RCVDATE'],
                    'ITH_FORM' => 'INC',
                    'ITH_DOC' => $value['RCV_DONO'],
                    'ITH_QTY' => $value['RCV_QTY'],
                    'ITH_LUPDT' => $date->format('Y-m-d H:i:s'),
                    'ITH_USRID' => substr($faker->name,0,9),
                    'ITH_CAT' => NULL,
                    'ITH_LINE' => $key+1,
                    'ITH_FEDR' => NULL,
                    'ITH_WH' => 'ARWH1',
                ]);
            }
        }
    }

    private function getDataReceive()
    {
        $receive = MutasiStok::inRandomOrder()->first();

        return $receive->RCV_DONO;
    }
}
