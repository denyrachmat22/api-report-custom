<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Faker\Factory as Faker;

class RCVTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($h = 0; $h <= 20; $h++) {
            $year = rand(2009, 2019);
            $month = rand(1, 12);
            $day = rand(1, 28);

            $date = Carbon::create($year, $month, $day, 0, 0, 0);

            $ponum = rand(1, 99);
            $inv = rand(1, 99);

            $faker = Faker::create();
            $bcType = $faker->randomElement(['BC 2.3', 'BC 2.5', 'BC 4.0']);

            $randjumline = rand(1, 15);
            $supp = $this->getRandomSupp();
            for ($i = 0; $i <= $randjumline; $i++) {

                $qty = rand(1, 9999);
                $amt = rand(1, 99);

                DB::table('RCV_TBL')->insert([
                    'RCV_PO' => $day . $month . $ponum . $year,
                    'RCV_INVNO' => 'INV-' . $day . $month . $year . $inv,
                    'RCV_INVDATE' => $date->format('Y-m-d H:i:s'),
                    'RCV_ITMCD' => $this->getRandomItemNumber(),
                    'RCV_RPNO' => NULL,
                    'RCV_RPDATE' => NULL,
                    'RCV_BCTYPE' => $bcType,
                    'RCV_BCNO' => str_pad($day, 2, '0', STR_PAD_LEFT) . str_pad($month, 2, '0', STR_PAD_LEFT),
                    'RCV_BCDATE' => $date->format('Y-m-d'),
                    'RCV_RCVDATE' => $date->format('Y-m-d'),
                    'RCV_DONO' => 'SHP-' . $year . $day . $month,
                    'RCV_FRLOCCD' => NULL,
                    'RCV_TOLOCCD' => NULL,
                    'RCV_QTY' => $qty,
                    'RCV_AMT' => $amt,
                    'RCV_SUPID' => $supp,
                    'RCV_WH' => 'ARWH1',
                    'RCV_LUPDT' => date('Y-m-d h:i:s'),
                    'RCV_USRID' => 'ane'
                ]);
            }
        }
    }

    private function getRandomItemNumber()
    {
        $item = \App\Model\MASTER\ItemMaster::inRandomOrder()->first();
        return trim($item->MITM_ITMCD);
    }

    private function getRandomSupp()
    {
        $supp = \App\Model\MASTER\SuppMaster::inRandomOrder()->first();
        return trim($supp->MSUP_SUPCD);
    }
}
