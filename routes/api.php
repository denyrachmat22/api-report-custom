<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'inventory'], function () {
    Route::get('/testgetdatalinkserver', 'API\BarcodeGeneratorController@test2');

    Route::get('/getmenuall/{prefix}', 'WMS\API\MenuController@show');
    Route::get('/getbarcode/{val}', 'API\BarcodeGeneratorController@getbarcodePDF417');
    Route::get('/getbarcodeqrcode/{val}', 'API\BarcodeGeneratorController@getbarcodeQRCODE');

    // Pemasukan Barang
    Route::post('/MutasiStock/{methode}', 'WMS\REPORT\MutasiStockController@show');
    Route::post('/MutasiStockNew/{methode}/{state?}', 'WMS\REPORT\MutasiStockController@newShow');
    Route::post('/MutasiStockExport/{methode}', 'WMS\REPORT\MutasiStockController@ExportToExcell');

    // Pengeluaran Barang
    Route::post('/MutasiStockKeluar/{methode}', 'WMS\REPORT\MutasiStockKeluarController@show');
    Route::post('/MutasiStockKeluarExport', 'WMS\REPORT\MutasiStockKeluarController@ExportToExcell');

    // Mesin & Peralatan
    Route::post('/MutasiMesinAlat/{methode}', 'WMS\REPORT\MutasiMesinAlatController@show');
    Route::post('/MutasiMesinAlatExport', 'WMS\REPORT\MutasiMesinAlatController@ExportToExcell');

    // Bahan Baku / Penolong
    Route::get('/MutasiBahanBakutest', 'WMS\REPORT\MutasiBahanBakuController@index');
    Route::post('/MutasiBahanBaku/{methode}', 'WMS\REPORT\MutasiBahanBakuController@show');
    Route::post('/MutasiBahanBakuExport', 'WMS\REPORT\MutasiBahanBakuController@ExportToExcell');

    // Finish Good
    Route::post('/MutasiFinishGood/{methode}', 'WMS\REPORT\MutasiFinishGoodController@show');
    Route::post('/MutasiFinishGoodExport', 'WMS\REPORT\MutasiFinishGoodController@ExportToExcell');

    // Scrap
    Route::post('/MutasiScrap/{methode}', 'WMS\REPORT\MutasiScrapController@show');
    Route::post('/MutasiScrapExport', 'WMS\REPORT\MutasiScrapController@ExportToExcell');

    // Upload Scrap
    Route::get('/DownloadScrapTemplate', 'WMS\REPORT\UploadDataController@DownloadTemplateScrap');
    Route::post('/UploadScrap', 'WMS\REPORT\UploadDataController@UploadScrap');

    // Upload Mesin & Peralatan
    Route::get('/DownloadMesinTemplate', 'WMS\REPORT\UploadDataController@DownloadTemplateMesin');
    Route::post('/UploadMesin', 'WMS\REPORT\UploadDataController@UploadMesin');

    // Saldo WIP
    Route::post('/MutasiWIP/{methode}', 'WMS\REPORT\MutasiWIPController@show');
    Route::post('/MutasiWIPExport', 'WMS\REPORT\MutasiWIPController@ExportToExcell');
    Route::post('/MutasiWIPNew/{methode}', 'WMS\REPORT\MutasiWIPController@show2');
    Route::post('/MutasiWIPExportNew', 'WMS\REPORT\MutasiWIPController@ExportToExcell2');

    // Mutasi Internal
    Route::post('/MutasiInternal', 'WMS\REPORT\MutasiInternalController@show');

    // Scrap Report Report Start
    Route::post('/getDataKitJob', 'WMS\REPORT\KittingJobsController@index2');
    Route::get('/getItemModel', 'WMS\REPORT\KittingJobsController@getAssyCode');
    Route::get('/getItemModel/{filter}', 'WMS\REPORT\KittingJobsController@getAssyCode');
    Route::get('/getItemModel/{filter}/{pagination}', 'WMS\REPORT\KittingJobsController@getAssyCode');
    Route::get('/getItemModel/{filter}/{pagination}/{model}', 'WMS\REPORT\KittingJobsController@getAssyCode');

    Route::post('/storeProcessMstr', 'WMS\REPORT\KittingJobsController@submiterProcessMaster');
    Route::get('/getListProcess', 'WMS\REPORT\KittingJobsController@getListProcess');

    Route::get('/getListMapMaster', 'WMS\REPORT\KittingJobsController@getListMappingMaster');
    Route::post('/getDetailMapping/{id}', 'WMS\REPORT\KittingJobsController@getDetailMapping');

    Route::get('/getListMapProcess', 'WMS\REPORT\KittingJobsController@getListMapProcess');
    Route::get('/getListMapProcess/{model}', 'WMS\REPORT\KittingJobsController@getListMapProcess');
    Route::get('/getListMapProcess/{model}/{process}', 'WMS\REPORT\KittingJobsController@getListMapProcess');
    Route::get('/getListMapProcess/{model}/{process}/{jobs}', 'WMS\REPORT\KittingJobsController@getListMapProcess');

    Route::get('/resubmitScrap/{id_trans}/{id?}', 'WMS\REPORT\KittingJobsController@resubmitScrapDet');

    Route::post('/storeProcessMapping', 'WMS\REPORT\KittingJobsController@storeProcessMap');
    Route::get('/deleteProcessMapping/{id}', 'WMS\REPORT\KittingJobsController@ondeleteProcessMap');

    Route::get('/downloadTemplateMapping', 'WMS\REPORT\KittingJobsController@downloadTemplateMapping');
    Route::post('/uploadDataMapping', 'WMS\REPORT\KittingJobsController@importMappingNew');

    Route::post('/findListWO', 'WMS\REPORT\KittingJobsController@newfindListJob');
    Route::post('/findListDO', 'WMS\REPORT\KittingJobsController@findListDO');
    Route::post('/findListRef', 'WMS\REPORT\KittingJobsController@findListRef');

    Route::post('/storeHistScrap', 'WMS\REPORT\KittingJobsController@storeHist');
    Route::post('/newStoreHistScrap', 'WMS\REPORT\KittingJobsController@newStoreHist');
    Route::get('/printScrap/{id}', 'WMS\REPORT\KittingJobsController@pdfExport');
    Route::get('/printScrap/{id}/{det}', 'WMS\REPORT\KittingJobsController@pdfExport');
    Route::get('/printScrap/{id}/{det}/{met}', 'WMS\REPORT\KittingJobsController@pdfExport');
    Route::get('/printScrap/{id}/{det}/{met}/{exc}', 'WMS\REPORT\KittingJobsController@pdfExport');

    Route::get('/getScrapData/{id}', 'WMS\REPORT\KittingJobsController@getScrapByID');
    Route::get('/reviseScrapData/{id}', 'WMS\REPORT\KittingJobsController@reviseScrap');
    Route::get('/reviseScrapData/{id}/{met}', 'WMS\REPORT\KittingJobsController@reviseScrap');

    Route::post('/getdatascrap', 'WMS\REPORT\KittingJobsController@reportTransaction');
    Route::post('/getdatascrapdet/{id}', 'WMS\REPORT\KittingJobsController@reportTransactionDetail');

    Route::get('/testingCheckItemStock/{item}/{qty}', 'WMS\REPORT\KittingJobsController@checkItemStock');
    Route::get('/checkMigrationScrap', 'WMS\REPORT\KittingJobsController@migrationsScrap');

    Route::post('/getlossjob', 'WMS\REPORT\KittingJobsController@findith');
    Route::post('/confirmlossjob', 'WMS\REPORT\KittingJobsController@confirmedLoss');
    Route::post('/devStoreHistScrap', 'WMS\REPORT\ScrapReportController@storeHist');
    Route::post('/testGetArray', 'WMS\REPORT\ScrapReportController@testingListJob');

    Route::post('/storeScrapReport', 'WMS\REPORT\ScrapReportController@newStoreQueue');
    Route::get('/testRedis', 'WMS\REPORT\ScrapReportController@testRedis');
    Route::get('/testrcvscn/{item}/{lot}/{do}', 'WMS\REPORT\ScrapReportController@cekLatestHarga');
    Route::post('/findRetFG', 'WMS\REPORT\ScrapReportController@findRetFG');

    Route::get('/testfgret/{item}/{doc}', 'WMS\REPORT\ScrapReportController@getPartFGReturn');

    Route::get('/testitemlot/{item}/{lot}', 'WMS\REPORT\ScrapReportController@getLatestHargaFromBC');
    Route::get('/testitemlot/{item}', 'WMS\REPORT\ScrapReportController@getLatestHargaFromBC');

    Route::get('/getlistitemcomp/{job}', 'WMS\REPORT\ScrapReportController@psnListByJobSerD');

    Route::post('templateScrapUpload/{type}', 'WMS\REPORT\ScrapReportController@getTemplateUpload');
    Route::post('uploadScrapHist', 'WMS\REPORT\ScrapReportController@uploadTemplate');

    Route::post('directScrapUpload/{type}', 'WMS\REPORT\ScrapReportController@processDataWithoutUpload');

    Route::get('/testRCV/{do}/{item?}', 'WMS\REPORT\ScrapReportController@testRCV');

    Route::get('/getDetailItem/{id}', 'WMS\REPORT\ScrapReportController@getDetailComponentByID');

    // Scrap Report Report End

    // Checker
    Route::get('/cekmspp/{model}/{item}', 'WMS\REPORT\KittingJobsController@cekMsppStructureBOMPN');

    // API CEISA
    Route::post('/getStockBC', 'WMS\REPORT\StockPabeanController@calculateEXBC');
    Route::get('/cancelDO/{DO}', 'WMS\REPORT\StockPabeanController@cancelDoc');
    Route::get('/cancelDO/{DO}/{loc}', 'WMS\REPORT\StockPabeanController@cancelDoc');
    Route::get('/exBCAdjustment/{item}/{qty}', 'WMS\REPORT\StockPabeanController@exBCItemAdjustment');
    Route::get('/getStockSMT', 'WMS\REPORT\StockPabeanController@adjustmentStockWithWMS');

    Route::get('/getItemStock/{item}', 'WMS\REPORT\StockPabeanController@findStokItem');
    Route::get('/getItemMigration', 'WMS\REPORT\StockPabeanController@prepMigration');
    Route::get('/getItemMigration/{item}', 'WMS\REPORT\StockPabeanController@prepMigration');

    Route::post('/calculate_raw_material_resume', 'WMS\REPORT\StockPabeanController@calculate_raw_material_resume');

    Route::post('/getStockBCArray', 'WMS\REPORT\StockPabeanController@arrayParsedExBC');

    Route::post('/sendqueueexbc', 'WMS\REPORT\StockPabeanController@sendExBCQueue');
    Route::get('/testingsocket', 'WMS\REPORT\StockPabeanController@testingSocket');

    Route::get('/testingrcvscn/{item}/{lot?}/{qty?}', 'WMS\REPORT\StockPabeanController@testingFindRCVScan');
    Route::get('/updateScrapSelisih', 'WMS\REPORT\StockPabeanController@reviseScrapOver');
    Route::post('/interfacemutasi/{isStore?}', 'WMS\REPORT\MutasiStockController@interfaceMutasiCheck');

    Route::resource('/user_log', 'WMS\REPORT\UserLogsController');
    Route::patch('/wms_user_log/{id?}', 'WMS\REPORT\UserLogsController@showWMSLogs');
});

Route::post('/login/{app}', 'WMS\API\AuthController@login');
Route::get('/userlists/{met}', 'WMS\API\AuthController@getuser');
Route::get('/rolelists', 'WMS\API\AuthController@getrole');

Route::post('/menuadd/{met}', 'WMS\API\MenuController@store');
Route::get('/getmenu/{id}', 'WMS\API\MenuController@cekmenuid');

Route::get('/getallrole', 'WMS\API\RoleController@index');
Route::get('/getrole/{id}', 'WMS\API\RoleController@cekroleid');
Route::post('/roleadd/{met}', 'WMS\API\RoleController@store');

Route::post('/register', 'WMS\API\AuthController@register');
Route::post('/updateuser/{id}', 'WMS\API\AuthController@updateuser');
Route::get('/deleteuser/{id}', 'WMS\API\AuthController@delete');

Route::group(['prefix' => 'mobile', 'middleware' => 'api'], function () {
    // Home
    Route::get('/getWHall', 'WMS\API\HomeController@index');
    Route::get('/getmenuall/{cek}', 'WMS\API\MenuController@index');

    // Receiving
    Route::get('/getbydo/{do}', 'WMS\API\ReceivingController@getdata');
    Route::get('/searchitembyloc/{loc}/{item}', 'WMS\API\ReceivingController@getitembyloc');
    Route::post('/savereceiving', 'WMS\API\ReceivingController@store');

    // Part Supply
    Route::post('/getspllist', 'WMS\API\PSNController@getPSNData');
});

Route::group([
    'namespace' => 'Auth',
    'middleware' => 'api',
    'prefix' => 'password'
], function () {
    Route::post('create', 'PasswordResetController@create');
    Route::get('find/{token}', 'PasswordResetController@find');
    Route::post('reset', 'PasswordResetController@reset');
});

Route::group(['prefix' => 'portal'], function () {
    Route::post('registeruser', 'Auth\RegisterController@create');
    Route::post('resetpassword', 'Auth\RegisterController@ResetPassword');
    Route::post('login', 'Auth\LoginController@Login');

    Route::get('verify/{username}/{token}','Auth\RegisterController@verify');
    Route::get('registereduser', 'Auth\RegisterController@GetAllUser');

    Route::get('getmenulist/{url}', 'PORTAL\MenuController@show');
    Route::get('getmenu/{id}', 'PORTAL\MenuController@cekmenuid');
    Route::get('getmenuall/{cek}', 'PORTAL\MenuController@index');
    Route::post('menuadd/{method}', 'PORTAL\MenuController@getmenu');

    Route::get('/getallrole', 'PORTAL\RoleController@index');
    Route::get('/getrole/{id}', 'PORTAL\RoleController@cekroleid');
    Route::post('/roleadd/{met}', 'PORTAL\RoleController@store');
});

Route::get('/test3', 'WMS\REPORT\StockPabeanController@testingArray');

Route::group(['prefix' => 'scheduller'], function () {
    Route::get('/MutasiPabean', 'Scheduller\TarikdataheaderController@MutasiPabeanPerDokumen');
    Route::get('/MutasiPabean/{met}', 'Scheduller\TarikdataheaderController@MutasiPabeanPerDokumen');
    Route::get('/MutasiPabean/{met}/{nosj}', 'Scheduller\TarikdataheaderController@MutasiPabeanPerDokumen');

    Route::get('/MutasiPabeanByItem/{item}', 'Scheduller\TarikdataheaderController@MutasiPabeanPerItem');

    Route::get('/MutasiFG', 'Scheduller\TarikdataFinishGoodController@index');
    Route::get('/GetdataSPL', 'Scheduller\TarikDataSPL@GetData');
    Route::get('/GetdataSPLbydate/{date}', 'Scheduller\TarikDataSPL@GetDataBydate');
    Route::get('/GetdataSPL/{psn}/{cat}/{line}/{feeder}', 'Scheduller\TarikDataSPL@GetData');
    Route::get('/GetdataSPLbySPL/{psn}/{cat?}/{line?}/{feeder?}/{job?}/{mcz?}', 'Scheduller\TarikDataSPL@GetDataSPLbySPL');
    Route::get('/GetdataScrap/{psn}/{cat}/{line}/{feeder}/{order}/{lot}', 'Scheduller\TarikdataScrapController@index');

    Route::get('/getdataWIP', 'Scheduller\TarikSaldoWIP@index');
    Route::get('/getdataWIPNew', 'Scheduller\TarikSaldoWIP@getData');
    Route::get('/getdataWIPNew/{psn}/{cat?}/{line?}/{feeder?}', 'Scheduller\TarikSaldoWIP@getData');
    Route::get('/getCompleteWIP', 'Scheduller\TarikSaldoWIP@completeWIP');

    Route::get('/ScrapItem/{item}', 'Scheduller\TarikdataScrapController@sendscheduller');

    // New Tarikan IT Inventory
    Route::get('/ResetMutasiMasuk', 'Scheduller\TarikdataheaderController@ResetMutasiMasuk');
    Route::get('/ResetBahanBakuOut', 'Scheduller\TarikdataheaderController@resetOutgoingBB');

    // 4M Status Notification
    Route::get('/FourMNotification', 'Scheduller\FourMController@index');
});

Route::group(['prefix' => 'stock'], function () {
    // WMS API
    Route::get('incomingPabean/{do}/{item?}', 'ITInventory\IncomingPabean@updateIncomingPabean');
    Route::get('deleteIncomingPabean/{do}/{aju?}', 'ITInventory\IncomingPabean@deleteDO');
    Route::get('onKitting/{item}/{psn?}/{cat?}/{line?}/{feeder?}', 'ITInventory\KittingAPIController@KittingAPI');
    Route::post('onKittingMultiItem', 'ITInventory\KittingAPIController@multipleiKittingAPI');
    Route::get('onKittingReturn/{item}/{psn?}/{cat?}/{line?}/{feeder?}/{mc?}/{lot?}', 'ITInventory\KittingReturnAPIController@returnAPI');
    Route::get('onFGInc/{sernum}', 'ITInventory\IncomingFGAPIController@incFGAPI');
    Route::get('onDelivery/{do}/{item?}', 'ITInventory\DeliveryAPIController@updateIncomingPabean');
    Route::post('onDeliveryByDate', 'ITInventory\DeliveryAPIController@updateOutgoingByDate');
    Route::post('updateOutgoingByDOArr', 'ITInventory\DeliveryAPIController@updateOutgoingByDOArr');
    Route::get('checkDataDelivery/{do}/{item?}', 'ITInventory\DeliveryAPIController@onDelivery');

    Route::get('migrateFromAPI', 'ITInventory\MigrationController@migrateFromAPI');
    Route::get('migrateFromAPI/{insert}', 'ITInventory\MigrationController@migrateFromAPI');
    Route::get('migrateFromAPI/{insert}/{status}', 'ITInventory\MigrationController@migrateFromAPI');
    Route::get('removeExBCNotExistsRCVTBL', 'ITInventory\MigrationController@removeExBCNotExistsRCVTBL');
    Route::get('deleteMinusData', 'ITInventory\MigrationController@checkingStockAju');

    Route::get('migrateFromAPINew', 'ITInventory\MigrationController@checkDiscrepancyStock');
    Route::get('migrateFromAPINew/{stat}/{isInsert?}/{item?}', 'ITInventory\MigrationController@checkDiscrepancyStock');
    Route::get('checkMigrationDouble/{remark}/{item?}', 'ITInventory\MigrationController@checkMigrationDouble');
    Route::get('migrationOutAdj', 'ITInventory\MigrationController@migrationOutAdj');
    Route::post('migrateFromAPIByItem', 'ITInventory\MigrationController@checkDiscByItem');
    Route::get('recheckStockMigration', 'ITInventory\MigrationController@recheckStockMigration');
    Route::get('compareCurentWithMigration', 'ITInventory\MigrationController@compareCurentWithMigration');
    Route::post('deleteOutAdj', 'ITInventory\MigrationController@deleteAdjOut');
    Route::get('resetAdjOut', 'ITInventory\MigrationController@resetAdj');

    // Reset Data
    Route::get('incomingPabeanAll', 'ITInventory\IncomingPabean@pushAllData');
    Route::post('incomingPabeanByDate', 'ITInventory\IncomingPabean@getDataByDate');
    Route::post('incomingPabeanByDOArray', 'ITInventory\IncomingPabean@pushByDoArray');
    Route::post('getDataByDO', 'ITInventory\IncomingPabean@cekDataDO');

    Route::get('resendKittingAll', 'ITInventory\KittingAPIController@resendKittingAll');
    Route::get('resendKittingReturnAll', 'ITInventory\KittingReturnAPIController@resendReturnAll');
    Route::get('resendFGIncoming', 'ITInventory\IncomingFGAPIController@resendReturnAll');
    Route::get('outgoingPabeanAll', 'ITInventory\DeliveryAPIController@pushAllData');
});

Route::group(['prefix' => 'scrap'], function () {
    Route::get('getListProcess', 'Scrap\ScrapController@getListProcess');
    Route::post('findRef', 'Scrap\ScrapController@findRef');
    Route::get('findRefView/{ref}', 'Scrap\ScrapController@checkViewRef');

    Route::post('findItem', 'Scrap\ScrapController@findItem');
    Route::post('findLot', 'Scrap\ScrapController@findLot');
    Route::post('findJob', 'Scrap\ScrapController@findJob');
    Route::post('findRetFG', 'Scrap\ScrapController@findRetFG');
    Route::post('findMchEq', 'Scrap\ScrapController@findMchEq');
    Route::post('directSaveScrap/{type}', 'Scrap\ScrapController@directSaveScrap');
    Route::get('resubmitScrap/{id_trans}/{id?}', 'Scrap\ScrapController@resubmitScrapDet');
    Route::get('testerscrap', 'Scrap\ScrapController@test');
    Route::get('resubmitITHScrap/{id}', 'Scrap\ScrapController@resubmitITH');
    Route::get('resubmitEXBCScrap/{id}', 'Scrap\ScrapController@resubmitBCStock');
    Route::get('reviseScrapData/{id}', 'Scrap\ScrapController@reviseScrap');
    Route::get('reviseScrapData/{id}/{met}', 'Scrap\ScrapController@reviseScrap');

    Route::post('/getdatascrap', 'Scrap\ScrapController@reportTransaction');
    Route::post('/getdatascrapdet/{id}/{sum?}', 'Scrap\ScrapController@reportTransactionDetail');

    Route::get('/fixEmptyMapping/{doc}/{job?}', 'Scrap\ScrapController@fixEmptyMapping');

    Route::get('checkDataItem/{item}/{qty}', 'Scrap\ScrapController@fifoPartScrap');

    Route::get('confirmScrap/{id}/{isPPC?}/{date?}', 'Scrap\ScrapController@confirmScrap');

    Route::post('/fixScrapForDispose', 'Scrap\ScrapController@fixScrap');
    Route::post('/getDataForDispose', 'Scrap\ScrapController@getDataForDispose');
    Route::post('/fixBeforeDispose', 'Scrap\ScrapController@fixBeforeDispose');

    Route::post('/submitDispose', 'Scrap\ScrapController@submitDispose');
    Route::post('/disposeReport', 'Scrap\ScrapController@disposeReport');
    Route::get('/listAllDisposeDoc', 'Scrap\ScrapController@listAllDisposeDoc');
    Route::post('/listDisposeDetailData/{idDispose}', 'Scrap\ScrapController@listDetailData');
    Route::get('/reprintDispose/{id}', 'Scrap\ScrapController@reprintDispose');
    Route::get('/reprintDisposeApproval/{id}/{model?}/{export?}', 'Scrap\ScrapController@reprintDispose');
    Route::get('/reprintDisposeByDoc/{id}', 'Scrap\ScrapController@reprintDisposeByDoc');
    Route::get('/confirmDispose/{uname}/{id}/{dn}', 'Scrap\ScrapController@confirmDispose');
    Route::get('/cancelApproval/{id}', 'Scrap\ScrapController@cancelApproval');
    Route::post('/updateQtyDispose', 'Scrap\ScrapController@updateQtyDispose');
    Route::get('/disposeCancel/{id}/{item?}', 'Scrap\ScrapController@disposeCancel');
    Route::post('/fixFailedEXBC', 'Scrap\ScrapController@fixFailedEXBC');
    Route::post('/saveDNDispose', 'Scrap\ScrapController@saveDNDispose');

    Route::post('/getDataPickingList', 'Scrap\ScrapController@getDataPickingList');
    Route::post('/getDataPickingList/{id}/{model}', 'Scrap\ScrapController@getDataPickingList');

    Route::get('/findItemNew/{query}/{model}/{useValue?}/{top?}', 'Scrap\ScrapController@findItemItemOnly');
    Route::get('/findStockMch/{item}/{po?}/{ser?}/{datepo?}', 'Scrap\ScrapController@findStockItemMch');
    Route::get('/findStockItem/{item}', 'Scrap\ScrapController@findStockItem');

    Route::post('/listFailedComp/{param}', 'Scrap\ScrapController@listFailedComp');
    Route::post('/scanScrapDNSave', 'Scrap\ScrapController@scanScrapDNSave');
    Route::get('/scanScrapDNSaveAll/{id}/{user}/{model}', 'Scrap\ScrapController@scanScrapDNSaveAll');
    Route::post('/uploadDisposeDNDraft', 'Scrap\ScrapController@uploadDisposeDNDraft');

    Route::get('/printDisposeDN/{id}/{model}', 'Scrap\ScrapController@printDisposeDN');
    Route::get('/deleteDisposeList/{id}', 'Scrap\ScrapController@deleteDisposeList');
    Route::get('/cancelSaveDisposeDNList/{id}', 'Scrap\ScrapController@cancelSaveDisposeDNList');

    Route::post('/updatePickingList', 'Scrap\ScrapController@onUpdateData');
    Route::post('/viewScanData', 'Scrap\ScrapController@viewScanData');
    Route::get('/deleteScanData/{idList}/{idScan?}', 'Scrap\ScrapController@deleteScanData');

    Route::post('/getlossjob', 'Scrap\ScrapController@findLoss');
    Route::post('/exportResultLossCandidate', 'Scrap\ScrapController@exportResultLossCandidate');
    Route::post('/submitLoss', 'Scrap\ScrapController@SubmitLoss');

    Route::get('getListBG', 'Scrap\ScrapController@listBG');
    Route::get('getListSupp', 'Scrap\ScrapController@listSupp');
    Route::get('getListCust', 'Scrap\ScrapController@listCust');


    Route::post('fixExbcOnDraft', 'Scrap\ScrapController@fixExbcOnDraft');
});

Route::group(['prefix' => 'designprocess'], function () {
    Route::get('getMasterProcess/{mfg}', 'DesignProcess\DesignProcessController@getMasterProcess');
    Route::post('storeMasterProcess', 'DesignProcess\DesignProcessController@masterStore');
    Route::patch('updateMasterProcess/{id}', 'DesignProcess\DesignProcessController@masterUpdate');
    Route::delete('deleteMasterProcess/{id}', 'DesignProcess\DesignProcessController@masterDelete');

    Route::post('storeProcessItem', 'DesignProcess\DesignProcessController@storeProcessItem');
    Route::post('uploadDataProcess', 'DesignProcess\DesignProcessController@uploadProcess');
    Route::post('getDataContentAll', 'DesignProcess\DesignProcessController@getDataContentAll');
    Route::delete('deleteContent/{id}', 'DesignProcess\DesignProcessController@deleteContent');

    Route::post('exportProcess/{withData?}', 'DesignProcess\DesignProcessController@ExportToExcell');
});

Route::group(['prefix' => 'KKA'], function () {
    Route::post('generateKKA', 'WMS\REPORT\KKAController@getKKA');
    Route::post('checkITInventory', 'WMS\REPORT\KKAController@checkITInventory');

    Route::post('checkEvent', 'WMS\REPORT\KKAController@checkEvent');
    Route::post('checkLedger', 'WMS\REPORT\KKAController@checkLedger');
    Route::get('findItem/{item}/{model}', 'WMS\REPORT\KKAController@findItem');
    Route::get('newKKA/{mutasi}/{period}/{fdate}/{ldate}/{item?}', 'WMS\REPORT\KKAController@newKKA');
});

Route::group(['prefix' => 'ciesafour'], function () {
    Route::get('login', 'WMS\REPORT\ciesaFourController@login');
    Route::post('sendPosting/{bc}', 'WMS\REPORT\ciesaFourController@getDataFromDLVWMS');
    Route::get('getDetailAju/{aju}', 'WMS\REPORT\ciesaFourController@getNopen');
    Route::get('status-pengajuan/{id}', 'WMS\REPORT\ciesaFourController@statusPengajuan');
    Route::get('getKurs/{date}/{kurs}', 'WMS\REPORT\ciesaFourController@getKurs');
    Route::get('getKursCheck/{date}/{kurs}', 'WMS\REPORT\ciesaFourController@getKursCheck');
    Route::get('getNIB', 'WMS\REPORT\ciesaFourController@testNIB');

    Route::get('checkDLV/{txid}', 'WMS\REPORT\ciesaFourController@checkDLV');
});

// getKKA
